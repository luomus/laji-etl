CREATE TABLE annotation (
 id				NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
 annotationid	VARCHAR2(400 char) NOT NULL UNIQUE,
 documentid		VARCHAR2(400 char) NOT NULL,
 data			CLOB NOT NULL,
 created		NUMBER(15) NOT NULL,
 loaded			DATE DEFAULT SYSDATE NOT NULL,
 inpipeentryid	NUMBER(33) NOT NULL
);
CREATE INDEX annotation_documentid ON annotation(documentid);

CREATE TABLE query_log (
 id             VARCHAR2(40 char) NOT NULL,
 timestamp		NUMBER(33) NOT NULL,
 year           NUMBER(4) NOT NULL,
 loaded			DATE DEFAULT SYSDATE NOT NULL,
 warehouse		VARCHAR2(7 char) NOT NULL,
 apiuser		VARCHAR2(400 char) NOT NULL,
 caller			VARCHAR2(400 char) NOT NULL,
 base			VARCHAR2(40 char) NOT NULL,
 useragent		VARCHAR2(4000 char),
 personid		VARCHAR2(40 char),
 personemail	VARCHAR2(400 char),
 querystring	CLOB NOT NULL,
 permissionid   VARCHAR2(400 char)
)
PARTITION BY LIST (year, apiuser) AUTOMATIC (
 PARTITION Q_LOG_INITIAL VALUES(2020, 'KE.389')
);
CREATE INDEX ix_query_log_apiuser_person ON query_log(apiuser, personid);
CREATE INDEX ix_query_log_apiuser_permis ON query_log(permissionid);

CREATE TABLE occurrence_count_log (
	log_date		DATE DEFAULT SYSDATE,
	private_count	NUMBER(33),
	public_count	NUMBER(33)
);

CREATE TABLE storage (
	key			VARCHAR2(200 char) PRIMARY KEY,
	data		CLOB NOT NULL
);

CREATE TABLE log (
	id				NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	timestamp		NUMBER(33) NOT NULL,
	source			VARCHAR2(400 char),
	phase			VARCHAR2(400 char),
	type			VARCHAR2(400 char),
	identifier		VARCHAR2(400 char),
	message			VARCHAR2(4000 char),
	full_message	CLOB
);

CREATE TABLE reprocess_temp (
 	id 				NUMBER,
 	documentid		VARCHAR2(400 char)
);

CREATE TABLE firstloaddates (
	documentid		VARCHAR2(400 char) NOT NULL,
	time			NUMBER(33) NOT NULL
);
CREATE INDEX firstloaddates_docid_time ON firstloaddates(documentid, time);

CREATE TABLE notification (
	id				NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	annotationid	VARCHAR2(400 char) NOT NULL,
	personid		VARCHAR2(400 char) NOT NULL,
	data			CLOB NOT NULL,
	reason			VARCHAR2(40 char),
	sent			NUMBER(1) NOT NULL,
	loaded			DATE DEFAULT SYSDATE NOT NULL
);
CREATE UNIQUE INDEX notification_annid_personid ON notification(annotationid, personid);
CREATE INDEX notification_sent_annid ON notification(sent, annotationid);

CREATE TABLE splitted_documentid (
	splitteddocumentid	VARCHAR2(400 char) NOT NULL,
	originaldocumentid	VARCHAR2(400 char) NOT NULL,
	originalunitid		VARCHAR2(400 char) NOT NULL,
	loaded				DATE DEFAULT SYSDATE NOT NULL
);
CREATE INDEX splitted_docid_orig ON splitted_documentid(originaldocumentid, originalunitid, splitteddocumentid);
CREATE INDEX splitted_docid_spli ON splitted_documentid(splitteddocumentid, originaldocumentid, originalunitid);

CREATE TABLE splitted_document_queue (
	id					NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	originaldocumentid	VARCHAR2(400 char) NOT NULL,
	source				VARCHAR2(400 char) NOT NULL,
	expiretime			NUMBER(33) NOT NULL,
	data				CLOB NOT NULL,
	loaded				DATE DEFAULT SYSDATE NOT NULL
);
CREATE INDEX splitted_q_orig ON splitted_document_queue(originaldocumentid);
CREATE INDEX splitted_q_exp ON splitted_document_queue(expiretime, source);
    
    
CREATE SEQUENCE seq_in INCREMENT BY 10000;
CREATE SEQUENCE seq_out INCREMENT BY 10000;

CREATE TABLE in_pipe (
	id					NUMBER PRIMARY KEY,
	source				VARCHAR2(10 char) NOT NULL,
	processed			NUMBER(1) DEFAULT 0 NOT NULL, 
	inerror				NUMBER(1) DEFAULT 0 NOT NULL,
	unrecoverableerror	NUMBER(1) DEFAULT 0 NOT NULL ,
	attemptcount		NUMBER(33) DEFAULT 0 NOT NULL,
	errormessage		VARCHAR2(4000 char),
	timestamp			DATE DEFAULT SYSDATE NOT NULL 
)
PARTITION BY LIST (source) AUTOMATIC (
   PARTITION P_INP_INITIAL VALUES('KE.3')
);
CREATE INDEX ix_inpipe ON in_pipe(processed, inerror, unrecoverableerror, attemptcount) LOCAL;

CREATE TABLE in_pipe_data (
	id					PRIMARY KEY,
    source				VARCHAR2(10 char) NOT NULL,
	contenttype			VARCHAR2(400 char) NOT NULL,
	data				CLOB NOT NULL,
    CONSTRAINT in_pipe_data_fk FOREIGN KEY (id) REFERENCES in_pipe(id) ON DELETE CASCADE
)
PARTITION BY LIST (source) AUTOMATIC (
   PARTITION P_IND_INITIAL VALUES('KE.3')
);

CREATE TABLE out_pipe (
	id					NUMBER PRIMARY KEY,
	source				VARCHAR2(10 char) NOT NULL,
	documentid			VARCHAR2(400 char) UNIQUE NOT NULL,
	collectionid		VARCHAR2(400 char),
	namedplaceid		VARCHAR2(400 char),
	inpipeid			NUMBER NOT NULL,
	deletion			NUMBER(1) NOT NULL,
	processed			NUMBER(1) DEFAULT 0 NOT NULL, 
	inerror				NUMBER(1) DEFAULT 0 NOT NULL,
	attemptcount		NUMBER(33) DEFAULT 0 NOT NULL,
	errormessage		VARCHAR2(4000 char),
	timestamp			DATE DEFAULT SYSDATE NOT NULL,
	CONSTRAINT fk_out_in FOREIGN KEY (inpipeid) REFERENCES in_pipe(id)
)
PARTITION BY LIST (source) AUTOMATIC (
   PARTITION P_OUTP_INITIAL VALUES('KE.3')
);
CREATE INDEX ix_outpipe ON out_pipe(processed, inerror, attemptcount, deletion) LOCAL;
CREATE INDEX ix_outpipe_exists ON out_pipe(documentid, id, inpipeid);
CREATE INDEX ix_outpipe_inpipeid ON out_pipe(inpipeid) LOCAL;
CREATE INDEX ix_outpipe_coll ON out_pipe(collectionid);
CREATE INDEX ix_outpipe_namedp ON out_pipe(namedplaceid);

CREATE TABLE out_pipe_data (
	id					PRIMARY KEY,
    source				VARCHAR2(10 char) NOT NULL,
	data				CLOB NOT NULL,
    CONSTRAINT out_pipe_data_fk FOREIGN KEY (id) REFERENCES out_pipe(id) ON DELETE CASCADE
)
PARTITION BY LIST (source) AUTOMATIC (
   PARTITION P_OUTD_INITIAL VALUES('KE.3')
);

CREATE TABLE apikey (
 	apikey			VARCHAR2(400 char) PRIMARY KEY,
 	requestid		VARCHAR2(400 char) NOT NULL,
 	expires			NUMBER(33) NOT NULL,
 	personid		VARCHAR2(400 char)
);
CREATE INDEX ix_apikey_person ON apikey(personid);


CREATE SEQUENCE seq_polygon INCREMENT BY 100;

CREATE TABLE polygon_search (
	id					NUMBER PRIMARY KEY,
    hash				VARCHAR2(400 char) NOT NULL,
	wkt					CLOB NOT NULL
);
CREATE INDEX ix_polygon_hash ON polygon_search(hash);

	
---- LAJIGIS ETL TABLES START -----------------------------
CREATE TABLE lajigis_etl_occurrences (
 eventId      NUMBER(33),
 occurrenceId NUMBER(33),
 json	      CLOB
);
CREATE INDEX ix_lajigis_occ ON lajigis_etl_occurrences(eventId, occurrenceId);

CREATE TABLE lajigis_etl_geo (
 occurrenceId NUMBER(33),
 wkt	      CLOB
);
CREATE INDEX ix_lajigis_geo ON lajigis_etl_geo(occurrenceId);

CREATE TABLE lajigis_etl_event_geo (
 eventId NUMBER(33),
 wkt	      CLOB
);
CREATE INDEX ix_lajigis_event_geo ON lajigis_etl_event_geo(eventId);

CREATE TABLE lajigis_etl_projects (
 eventId      NUMBER(33),
 projectName  VARCHAR2(400)
);
CREATE INDEX ix_lajigis_projects ON lajigis_etl_projects(eventId);

CREATE TABLE lajigis_etl_incoming (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);

CREATE TABLE lajigis_etl_previous (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);
 ---- LAJIGIS ETL TABLES END -----------------------------
 
 
 
 
---- TIIRA ETL TABLES START -----------------------------

CREATE TABLE tiiraatlas_etl_incoming (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);

CREATE TABLE tiiraatlas_etl_previous (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);
 ---- TIIRA ETL TABLES END -----------------------------
 
 
 ---- TAMPERE ETL TABLES START -----------------------------

CREATE TABLE tampere_etl_incoming (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);

CREATE TABLE tampere_etl_previous (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);
 ---- TAMPERE ETL TABLES END -----------------------------
 
 ---- HELSINKI ETL TABLES START -----------------------------

CREATE TABLE helsinki_etl_incoming (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);

CREATE TABLE helsinki_etl_previous (
 documentId   VARCHAR2(400) PRIMARY KEY,
 json	      CLOB NOT NULL,
 hash         VARCHAR2(400) NOT NULL
);
 ---- HELSINKI ETL TABLES END -----------------------------