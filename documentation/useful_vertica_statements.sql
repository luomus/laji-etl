-------------------------------------------------------------------------------------------
-- Deleting documents that do not exist in ETL tables

-- dimension schema
create table document_ids_to_delete (
document_id varchar(1000)
);

truncate table document_ids_to_delete;

insert into document_ids_to_delete
select d.document_id
from <PRIVATE_SCHEMA>.document d
left join all_documentids alld on alld.documentid = d.document_id
where alld.documentid is null
and d.actual_load_date < sysdate - INTERVAL '2 days'
order by actual_load_date desc
;

insert into document_ids_to_delete
select d.document_id
from <PUBLIC_SCHEMA>.document d
left join (
        select sp.splitteddocumentid as documentid
        from splitted_documentid sp
        join <PRIVATE_SCHEMA>.document d on sp.originaldocumentid = d.document_id
        UNION
        select documentid from all_documentids
) alld on alld.documentid = d.document_id
where alld.documentid is null
and d.actual_load_date < sysdate - INTERVAL '2 days'
order by actual_load_date desc
;


select count(distinct document_id) from document_ids_to_delete;
select distinct document_id from document_ids_to_delete order by document_id;

select distinct del.document_id, document.actual_load_date, source.name
from document_ids_to_delete del
left join <PRIVATE_SCHEMA>.document on del.document_id = document.document_id
left join source on document.source_key = source.key
order by source.name, del.document_id;


select * from splitted_documentid
where splitteddocumentid in (select document_id from document_ids_to_delete);

select count(1) from <PRIVATE_SCHEMA>.unit where document_id in (select document_id from document_ids_to_delete);
select count(1) from <PRIVATE_SCHEMA>.gathering where document_id in (select document_id from document_ids_to_delete);
select count(1) from <PRIVATE_SCHEMA>.document where document_id in (select document_id from document_ids_to_delete);

select count(1) from <PUBLIC_SCHEMA>.unit where document_id in (select document_id from document_ids_to_delete);
select count(1) from <PUBLIC_SCHEMA>.gathering where document_id in (select document_id from document_ids_to_delete);
select count(1) from <PUBLIC_SCHEMA>.document where document_id in (select document_id from document_ids_to_delete);

-- delete from <PRIVATE_SCHEMA>.unit where document_id in (select document_id from document_ids_to_delete);
-- delete from <PRIVATE_SCHEMA>.gathering where document_id in (select document_id from document_ids_to_delete);
-- delete from <PRIVATE_SCHEMA>.document where document_id in (select document_id from document_ids_to_delete);
-- delete from <PUBLIC_SCHEMA>.unit where document_id in (select document_id from document_ids_to_delete);
-- delete from <PUBLIC_SCHEMA>.gathering where document_id in (select document_id from document_ids_to_delete);
-- delete from <PUBLIC_SCHEMA>.document where document_id in (select document_id from document_ids_to_delete);

-------------------------------------------------------------------------------------------
-- FINDING units that are on public but not private and other way around
-- target/taxon is there because private side doesn't have occurrences for 3x species
-- see Securer: PRIVATE_SECURED_TAXA
-- MX.26825 tunturihaukka
-- MX.28987  tunturipöllö
-- MX.46542 naali 

			
select pub.document_id, pub.unit_id, pri.document_id, pri.unit_id, t.target_lowercase, t.taxon_key
from laji_public.unit pub
left join laji_private.unit pri on pub.document_id = pri.document_id and pub.unit_id = pri.unit_id
join laji_dimensions.target t on pub.target_key = t.key
where pub.document_id not like 'http://tun.fi/A.%'
and (pri.document_id is null or pri.unit_id is null)
order by pub.document_id, pub.unit_id;

-- sysdate is there to not include splitted documents still delayed in queue
select pri.document_id, pri.unit_id, pub.document_id, pub.unit_id, t.target_lowercase, t.taxon_key
from laji_private.unit pri
left join laji_public.unit pub on pub.document_id = pri.document_id and pub.unit_id = pri.unit_id
join laji_dimensions.target t on pri.target_key = t.key
where (pub.document_id is null or pub.unit_id is null)
and pri.actual_load_date < sysdate - INTERVAL '15 days'
order by pri.document_id, pri.unit_id;

-------------------------------------------------------------------------------------------
 


-- When loading data, if vertica slows down: stop loading, do query, merge ROS containers
select schema_name, projection_name, storage_type, count(1)
from STORAGE_CONTAINERS
group by schema_name, projection_name, storage_type
order by count(1) desc;

SELECT DO_TM_TASK('moveout', 'unit');
SELECT DO_TM_TASK('mergeout', 'unit');




-- Do this when ever new columns are added to tables  (nightly scheduler does this also but for random tables)
-- This also helps sometimes if queries to a certain table are slow
SELECT ANALYZE_STATISTICS('unit');



-- Find and kill running/blocking query
select * from v_monitor.query_requests where user_name = 'eskodev' and is_executing = true;
select INTERRUPT_STATEMENT('v_mustikka_node0001-24036:0xf03282', 12);   -- session_id, statement_id 



-- Käynnissä olevien kyselyiden haku
select * from QUERY_REQUESTS
where end_timestamp is null 
order by transaction_id desc
limit 5;



-- Explain plan
EXPLAIN select ...




-- Change con limit 
ALTER DATABASE mustikka SET MaxClientSessions = 100;



-- Check for data that breaks constaints (vertica is not in mode that would enforce them) 
SELECT ANALYZE_CONSTRAINTS('');



-- Drop all data of a certain data source
--SELECT DROP_PARTITION('document', 9003); -- 9003 = source_key
--SELECT DROP_PARTITION('gathering', 9003);
--SELECT DROP_PARTITION('unit', 9003);




-- If a table is updated: purge (=delete rows marked deleted) and 'defragment'
SELECT MAKE_AHM_NOW();
SELECT PURGE_TABLE('tablename'); 
SELECT DO_TM_TASK('moveout', 'tablename');
SELECT DO_TM_TASK('mergeout', 'tablename');


-- Find out tables that have lots of stuff that needs purging
select * from DELETE_VECTORS order by deleted_row_count desc;
SELECT MAKE_AHM_NOW();
SELECT PURGE_TABLE('tablename'); 



ALTER USER lajietlproduction RUNTIMECAP '120 minutes';
ALTER USER queryproduction RUNTIMECAP '10 minutes';














-----------------------------------------------------------------------------------------
-- fixing duplicate target ids

-- update units to use max target key in case of duplicate targets
-- both public and private
select 'update unit set target_key = '||max(key)||' where target_key = '||min(key)||';' 
        from target
        group by ORIGINAL_REFERENCE
        having count(1) > 1;


-- delete not-used targets
delete from target where key in (
   select key from (
        select key from target
        where key not in (
                select key
                from target
                where key in
                (select target_key from laji_public.unit UNION select target_key from laji_private.unit UNION select target_key FROM target_checklist_taxa)
        )
   ) a
);

-- check there are no target_ckeclist_taxa rows that are missing target link
select *
from target_checklist_taxa ct
left join target on ct.target_key = target.key
where target.key is null;

-- check there are no units that are missing target link
select document_id, target_key
from laji_public.unit 
left join target on unit.target_key = target.key
where target.key is null and unit.target_key is not null
UNION
select document_id, target_key
from laji_private.unit 
left join target on unit.target_key = target.key
where target.key is null and unit.target_key is not null;





-- creating target backup
CREATE TABLE TARGET_TEMP_xxxxxxxx
(
    KEY  INTEGER ,
    TARGETLOOKUP varchar(1000) NOT NULL,
    TARGET_LOWERCASE varchar(1000) NOT NULL,
    REFERENCE_KEY int,
    TAXON_KEY int,
    LOAD_DATE timestamp NOT NULL DEFAULT "sysdate"(),
    ORIGINAL_REFERENCE varchar(1000) NOT NULL,
    NOT_EXACT_TAXON_MATCH boolean
);
insert into TARGET_TEMP_xxxxxxxx
(key, targetlookup, target_lowercase, reference_key, taxon_key, load_date, original_reference, not_exact_taxon_match)
select (key, targetlookup, target_lowercase, reference_key, taxon_key, load_date, original_reference, not_exact_taxon_match)
from target;




--- manual dimension backup before doing updates


CREATE TABLE DIMENSION_BACKUP
(
TABLENAME VARCHAR(1000),
KEY INTEGER,
ID VARCHAR(1000)
);

insert into DIMENSION_BACKUP select 'agent', key, name from agent;
insert into DIMENSION_BACKUP select 'area', key, id from area;
insert into DIMENSION_BACKUP select 'collection', key, id from collection;
insert into DIMENSION_BACKUP select 'form', key, id from form;
insert into DIMENSION_BACKUP select 'geosource', key, id from geosource;
insert into DIMENSION_BACKUP select 'invasivecontrol', key, id from invasivecontrol;
insert into DIMENSION_BACKUP select 'keyword', key, id from keyword;
insert into DIMENSION_BACKUP select 'lifestage', key, id from lifestage;
insert into DIMENSION_BACKUP select 'mediatype', key, id from mediatype;
insert into DIMENSION_BACKUP select 'property', key, id from property;
insert into DIMENSION_BACKUP select 'qualityissue', key, id from qualityissue;
insert into DIMENSION_BACKUP select 'qualitysource', key, id from qualitysource;
insert into DIMENSION_BACKUP select 'recordbasis', key, id from recordbasis;
insert into DIMENSION_BACKUP select 'recordquality', key, id from recordquality;
insert into DIMENSION_BACKUP select 'reliability', key, id from reliability;
insert into DIMENSION_BACKUP select 'securelevel', key, id from securelevel;
insert into DIMENSION_BACKUP select 'securereason', key, id from securereason;
insert into DIMENSION_BACKUP select 'sex', key, id from sex;
insert into DIMENSION_BACKUP select 'source', key, id from source;
insert into DIMENSION_BACKUP select 'tag', key, id from tag;
insert into DIMENSION_BACKUP select 'taxonconfidence', key, id from taxonconfidence;
insert into DIMENSION_BACKUP select 'taxongroup', key, id from taxongroup;
insert into DIMENSION_BACKUP select 'userid', key, id from userid;

CREATE TABLE TEAM_BACKUP
(
KEY INTEGER,
TEAMLOOKUP VARCHAR(2000),
AGENTCOUNT INTEGER,
TEAM_TEXT VARCHAR(5000)
);

CREATE TABLE TARGET_BACKUP
(
KEY INTEGER,
ORIGINAL_REFERENCE VARCHAR(1000),
TARGETLOOKUP VARCHAR(1000),
TARGET_LOWERCASE VARCHAR(1000),
REFERENCE_KEY INTEGER
);

insert into TEAM_BACKUP select KEY, TEAMLOOKUP, AGENTCOUNT, TEAM_TEXT from team;
insert into TARGET_BACKUP select KEY, ORIGINAL_REFERENCE, TARGETLOOKUP, TARGET_LOWERCASE, REFERENCE_KEY from target; 




