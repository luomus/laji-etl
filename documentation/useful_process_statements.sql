-------------------------------------------------------------------------------
-- DELETING FROM ORACLE + VERTICA BY COLLECTION ID --
(when dropping full source partition is not possible)


-- Oracle:
-- Check all looks ok, how many is going to be deleted and from which sources
select source, count(1)
from out_pipe
where collectionid in (
 'http://tun.fi/HR.5012',
 'http://tun.fi/HR.5013',
 'http://tun.fi/HR.5014'
)
group by source;
 
 
truncate table del_temp;
 
insert into del_temp
select distinct inpipeid
from out_pipe
where collectionid in (
 'http://tun.fi/HR.5012',
 'http://tun.fi/HR.5013',
 'http://tun.fi/HR.5014'
);
 
delete from out_pipe_data
where id in (
select id
from out_pipe
where collectionid in (
 'http://tun.fi/HR.5012',
 'http://tun.fi/HR.5013',
 'http://tun.fi/HR.5014'
));

delete from out_pipe
where collectionid in (
 'http://tun.fi/HR.5012',
 'http://tun.fi/HR.5013',
 'http://tun.fi/HR.5014'
);

delete from in_pipe_data
where id in (select inpipeid from del_temp);

delete from in_pipe
where id in (select inpipeid from del_temp);

-- Vertica:
select key from laji_dimensions.collection
where id in (
 'http://tun.fi/HR.5012',
 'http://tun.fi/HR.5013',
 'http://tun.fi/HR.5014'
);
 
delete from laji_private.unit where collection_key in (299001,299002,299003);
delete from laji_private.gathering where collection_key in (299001,299002,299003);
delete from laji_private.document where collection_key in (299001,299002,299003);
delete from laji_public.unit where collection_key in (299001,299002,299003);
delete from laji_public.gathering where collection_key in (299001,299002,299003);
delete from laji_public.document where collection_key in (299001,299002,299003);

-------------------------------------------------------------------------------
-- TIPU
-- Re-send certain events

insert into warehouse_queue (queueid, eventid, action) 
select warehouse_queue_seq.nextval, id, 'UPDATE'
from event 
where ....;
commit;

-- Sending manual deletions
insert into warehouse_queue (queueid, eventid, action) 
select warehouse_queue_seq.nextval, id, 'DELETE'
from event 
where ....;
delete from event where ....;
commit;


-------------------------------------------------------------------------------
-- Kastikka
-- Re-send all or certain documents
-- Note: DOCUMENTURI contains Qname despide name of the field

DELETE FROM transactions_temp;
INSERT INTO transactions_temp (documenturi) 
    SELECT subjectname FROM kastikka_statement
    WHERE predicatename = 'rdf:type' 
    AND objectname = 'MY.document';
commit;
INSERT INTO transactions (documenturi)
    SELECT documenturi FROM transactions_temp;
commit;

select inerror, count(1) 
from transactions
group by inerror;


