-- dimensions
alter table taxon add newrank_key INTEGER NULL;
alter table taxon_temp add newrank_key INTEGER NULL;

-- pub ja private
CREATE OR REPLACE VIEW TAXON AS SELECT * FROM <schema>_DIMENSIONS.TAXON;
grant select on taxon to lajietlproduction|staging;


-- Vastaavat kentät ja getterit/setterit lisätään fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity

-- Muista pistää myös create_tables_vertica.sql



