-- Reprocess a batch --
-- 1.1 modify fi.laji.datawarehouse.etl.service.console.StartErrorReprocessing.java to define what documents you want to reprocess - the scrip stores documentids to reprocess_temp table 
-- (but you have to deploy a new version so option 1.2 is better for small/decent sized updates)
-- 1.2 alternatively do manual queries to vertica etc to find out a list of documentids you want to reprocess and store them to reprocess_temp table using Console UI  

-- 2. reprocess just out pipe
--  reprocessing from out pipe is enough if there is no changes to harmonization and we just want to trigger interpretation, conversion etc process
--  sql change is detected in 10 minute tasks OR to trigger immediately go to console and hit reprocess of one out-pipe entry of the source 
update out_pipe
set processed = 0
where documentid in (
	select documentid from reprocess_temp
);
commit;

-- 3. reprocess from in pipe
--  to fix harmonization process, we must reprocess the original data batches from in pipe
--  note that the batches may contain lots of more documents -> the total number of documents that will be reprocessed will be higher than the number of documents in reprocess_temp
 --  sql change is detected in 10 minute tasks OR to trigger immediately go to console and hit reprocess of one in-pipe entry of the source
update in_pipe 
set processed = 0, unrecoverableError = 0
where id in ( 
    select distinct inpipeid
    from out_pipe
    where documentid in (
        select documentid from reprocess_temp
    )
);
commit;



-- Reprocess annotations --
-- Annotations are stored under KE.388 - Lajistore
-- sql change is detected in 10 minute tasks or go to console and start manual reprocess of one entry to trigger immediate reader run
update in_pipe
set processed = 0
where source = 'KE.388';
commit;

-- Reprocess annotations ALTERNATIVE --
-- This alternative way doesn't reprocess the annotations, just the documents that have annotations
update out_pipe
set processed = 0
where documentid in (
	select distinct documentid from annotation
);
commit;


-- Reprocess staring from certain timestamp --
-- Find out pipe id that has timestamp that is sufficiently in the past; change the number 50000 bellow to larger 
select id, timestamp
from out_pipe
where id >= (select max(id) from out_pipe) - 50000
order by id asc;

-- update all records with id larger to be reprocessed
update out_pipe
set processed = 0, inerror = 0
where id >= 83689462; 



-- Browse log --
-- note time is 02 or 03 depending if you want to get winter or summer time
select 
TIMESTAMP '1970-01-01 03:00:00' + numtodsinterval(timestamp/1000, 'SECOND'),
source, phase, type, identifier, full_message
from log
order by id desc;


-- Check query log --
select apiuser, personemail, warehouse, permissionid, count(1)
from query_log
where year = 2022
and timestamp > 1654060821
group by apiuser, personemail, warehouse, permissionid
order by apiuser, warehouse;


-- list partitions ----------------------
SELECT table_name,
       partition_name,
       high_value,
       num_rows
FROM   user_tab_partitions
ORDER BY 1, 2;


-- empty partition -----------------------
DELETE FROM in_pipe partition (P_INP_INITIAL);   -- note partition name not inside ''


-- if empty partition is too slow is drop faster? -------
ALTER TABLE out_pipe DISABLE CONSTRAINT fk_out_in;
ALTER TABLE in_pipe DROP PARTITION xxx UPDATE INDEXES;
ALTER TABLE out_pipe ENABLE CONSTRAINT fk_out_in;
PURGE RECYCLEBIN; -- empty recycle bin after partition drop



--------------------------------------------------------------------------------------------------
-- Finding out document ids that are on Oracle ETL tables but not on Vertica 
-- First run document id sync using console

-- DELETE DUPLICATES (public+private ids)
DELETE FROM reprocess_temp
WHERE rowid not in
(SELECT MIN(rowid)
FROM reprocess_temp
GROUP BY documentid);
commit;

select o.id, o.inpipeid, o.source, o.documentid, o.deletion, o.processed, o.timestamp
from out_pipe o
left join reprocess_temp t on o.documentid = t.documentid
where t.documentid is null
and o.deletion = 0;





--------------------------------------------------------------------------------------------------
-- Copying data from production to test
-- as production user execute

delete from lajietl_staging.data_temp;

insert into lajietl_staging.data_temp (contenttype, data, source)
select contenttype, data, source
from in_pipe_data where id in (
	select inpipeid from out_pipe 
	where documentid IN (
        'http://id.luomus.fi/HV.85',
		...
    )
);
commit;

-- as staging user execute
DECLARE
  v_id NUMBER;
BEGIN
  FOR temp_row IN (SELECT * FROM DATA_TEMP) LOOP
    SELECT SEQ_IN.NEXTVAL INTO v_id FROM DUAL;
    
    INSERT INTO IN_PIPE (ID, SOURCE)
    VALUES (v_id, temp_row.SOURCE);

    INSERT INTO IN_PIPE_DATA (ID, SOURCE, CONTENTTYPE, DATA)
    VALUES (v_id, temp_row.SOURCE, temp_row.CONTENTTYPE, temp_row.DATA);
    
  END LOOP;
END;
COMMIT;



