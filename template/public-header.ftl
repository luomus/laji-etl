<!DOCTYPE html>
<html lang="${locale}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <title>${text["title_"+page]} | ${text.title}</title>
	<link href="${staticURL}/favicon.ico?${staticContentTimestamp}1" type="image/ico" rel="shortcut icon" />
	
    <!-- Bootstrap -->
    <link href="${staticURL}/bootstrap.min.css" rel="stylesheet">
    <link href="${staticURL}/lajifi_style.css?${staticContentTimestamp}" rel="stylesheet">
	<link href="${staticURL}/public.css?${staticContentTimestamp}" rel="stylesheet">

	<script src="${staticURL}/jquery-1.9.1.js"></script>
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand uppercase" href="https://laji.fi">laji.fi</a>
        </div>
        <div class="pull-right" id="language-bar">
            <ul>
					<#if locale != "fi"><li><a href="?locale=fi">suomeksi</a></li></#if>
                    <#if locale != "sv"><li><a href="?locale=sv">på svenska</a></li></#if>
                    <#if locale != "en"><li><a href="?locale=en">in English</a></li></#if>
            </ul>
        </div>
    </div>
</nav>