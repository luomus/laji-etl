<#include "luomus-header.ftl">

<h1>Reports</h1>

<#if successMessage?has_content>
	<div class="success">${successMessage?html}</div>
</#if>
	
<#if errorMessage?has_content>
	<div class="errorMessage">${errorMessage?html}</div>
</#if>

<h2>Start a Report</h2>
<ul>
	<li>Bird line-transect <button id="birdlinetransect">Start</button></li>
	<li>Bird point count <button id="birdpointcount">Start</button></li>
	<li>Winter bird <button id="winterbird">Start</button></li>
	<li>Water bird <button id="waterbird">Start</button></li>
	<li>SYKE Butterfly <button id="sykebutterfly">Start</button></li>
	<li>SYKE Bumblebee <button id="sykebumblebee">Start</button></li>
	<li>SYKE Pollinator <button id="sykepöly">Start</button></li>
	<li>Spring monitoring <button id="springmonitoring">Start</button></li>
	<li>Spring monitoring CURRENT YEAR <button id="springmonitoring_currentyear">Start</button></li>
	<li>Invasive prevention <button id="invasiveprevention">Start</button></li>
	<li>Complete list <button id="completelist">Start</button></li>
</ul>

<#if producedFiles?has_content>
<h2>Ready reports</h2>
<ul>
	<#list producedFiles as filename>
		<li><a href="${baseURL}/console/reports/download/${filename?html}">${filename?html}</a></li>
	</#list>
</ul>
</#if>

<script>
	$("button").on('click', function() {
		var id = $(this).attr('id');
		$('<form>', {"action": 'reports/' +id, "method": "POST"})
		.appendTo(document.body).submit();
	});
</script>

<#include "luomus-footer.ftl">
