<#include "luomus-header.ftl">

<h1>${source.toURI()} - ${(sourceDescriptions[source.toURI()].name)!"Unknown"}</h1>

<#if successMessage?has_content><div class="success">${successMessage}</div><div class="clear"></div></#if>

<#if inPipeEntries??>
<table>
	<thread>
		<tr>
			<th>Id</th>
			<th>Timestamp</th>
			<th>Unrecoverable error</th>
			<th>Error message</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<#list inPipeEntries as entry>
		<tr>
			<td><a href="${baseURL}/console/in/entry/${entry.id}">${entry.id}</td>
			<td>${entry.timestamp?string("d.M.yyyy - HH:mm:ss.SSS")}</td>
			<td><#if entry.unrecoverableError><span class="errorMessage">Yes</span></#if></td>
			<td><pre>${(entry.errorMessage!"")?html}</pre></td>
			<td>
				<a onclick="return confirm('Re-process?');" href="${baseURL}/console/reprocess/in/${entry.id}">Re-process</a> <br/>
				<#if entry.errorMessage?has_content>
					<a onclick="return confirm('Delete?');" href="${baseURL}/console/delete/in/${entry.id}">Delete</a> 
				</#if>
			</td>
		</tr>	
	</#list>
	</tbody>
</table>
</#if>

<#if outPipeEntries??>
<table>
	<thread>
		<tr>
			<th>IN-Id (received data)</th>
			<th>OUT-Id (harmonized data)</th>
			<th>DocumentId</th>
			<th>Timestamp</th>
			<th>Deletion</th>
			<th>Error message</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	<#list outPipeEntries as entry>
		<tr>
			<td><a href="${baseURL}/console/in/entry/${entry.inPipeId}">${entry.inPipeId}</a></td>
			<td><a href="${baseURL}/console/out/entry/${entry.id}">${entry.id}</td>
			<td>
				${entry.documentId.toURI()?html} &nbsp; 
				<a href="${baseURL}/console/public/view?documentId=${entry.documentId?html}">public</a>
				<a href="${baseURL}/console/private/view?documentId=${entry.documentId?html}">private</a>
				<#if entry.splittedDocumentIds?has_content>
					<p>Splitted documents</p>
					<ul>
						<#list entry.splittedDocumentIds as splitted>
							<li>
								${splitted} &nbsp; 
								<a href="${baseURL}/console/public/view?documentId=${splitted}">public</a>
							</li>
						</#list>
					</ul>
				</#if>
			</td>
			<td>${entry.timestamp?string("d.M.yyyy - HH:mm:ss.SSS")}</td>
			<td>${entry.deletion?string("yes", "no")}</td>
			<td><pre>${(entry.errorMessage!"")?html}</pre></td>
			<td>
				<a onclick="return confirm('Re-process?');" href="${baseURL}/console/reprocess/out/${entry.id}">Re-process</a><br/>
				<#if entry.errorMessage?has_content>
					<a onclick="return confirm('Delete document id from DW?');" href="${baseURL}/console/delete/out/${entry.id}">Delete</a><br/>
				</#if>
				<a onclick="return confirm('Re-process?');" href="${baseURL}/console/reprocess/in/${entry.inPipeId}">Re-process IN-entry</a> 
			</td>
		</tr>	
	</#list>
	</tbody>
</table>
</#if>

<#include "luomus-footer.ftl">
