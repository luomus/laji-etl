<#include "luomus-header.ftl">

<h1>Annotations</h1>

<table>
	<thread>
		<tr>
			<th>Id</th>
			<th>Root id</th>
			<th>Target id</th>
			<th>Deleted</th>
			<th>Added tags</th>
			<th>Removed tags</th>
			<th>Identification</th>
			<th>Notes</th>
			<th>Person</th>
			<th>System</th>
		</tr>
	</thead>
	<tbody>
	<#list annotations as a>
		<tr>
			<td>${a.id?html}</td>
			<td>${a.rootID?html}</td>
			<td>${a.targetID?html}</td>
			<td>${a.deleted?string("yes", "no")}</td>
			<td><#list a.addedTags as t>${t}</#list></td>
			<td><#list a.removedTags as t>${t}</#list></td>
			<td><#if a.identification??> ${a.identification.toString()?html} </#if></td>
			<td>${(a.notes!"")?html}</td>
			<td>${a.annotationByPerson!""}</td>
			<td>${a.annotationBySystem!""}</td>
		</tr>
	</#list>
	</tbody>
</table>

<#include "luomus-footer.ftl">

