<#list entries as e>
	<tr>
		<td>${e.datetime?string("d.M.yyyy - HH:mm:ss.SSS")}</td>
		<td>${(sourceDescriptions[e.source.toURI()].name)!"Unknown"}</td>
		<td>${e.phase!""}</td>
		<td>${e.type!""}</td>
		<td>${e.identifier!""}</td>
		<td><pre>${(e.message!"")?html}</pre></td>
	</tr>
</#list>
