<#include "public-header.ftl" />
 
<div class="container" id="digitisation">

	<h1>${text["title_"+page]}</h1>

	<input type="number" id="year" name="year" value="${year}" /> <button id="changeYear">${text.change}</button>

	<h3>${text.digistat_specimen}</h3>
		
	<#assign currentSubParentId = "">
	<#assign totalCalculator = calculator.new>
	<#assign subTotalCalculator = calculator.new>
	
	<table>
		<caption><p class="info">${text.digistat_size} ${text.digistat_from_metadata}</p></caption>
		<#list collections as stat>
			<#if stat_index == 0>
				<@header text.digistat_size />
			<#elseif stat.subParentId != currentSubParentId>
				<@total text.digistat_subtotal subTotalCalculator subTotalCalculator.sizeTotal />
				<#assign subTotalCalculator = calculator.new>
				<@header text.digistat_size />				
			</#if>
			<tr>
				<td>${stat.collection.qname.toString()?html}</td>
				<td><@name stat.collection /></td>
				<td>${stat.digitizedCount!"0"}</td>
				<td>${stat.imagedCount!"0"}</td>
				<td><@percent stat.digitizedCount!0 stat.collection.size!0 /></td>
				<td>${stat.collection.size!"--"}</td>
				<#list months as m><td>${stat.getMonth(m)}</td></#list>
			</tr>
			${subTotalCalculator.add(stat)!}
			${totalCalculator.add(stat)!}
			<#assign currentSubParentId = stat.subParentId>
		</#list>
		<@total text.digistat_subtotal subTotalCalculator subTotalCalculator.sizeTotal />
		<@header text.digistat_size />
		<@total text.digistat_total totalCalculator totalCalculator.sizeTotal />
	</table>

	<h3>${text.digistat_types}</h3>
	
	<#assign currentSubParentId = "">
	<#assign totalCalculator = calculator.new>
	<#assign subTotalCalculator = calculator.new>
	
	<table>
		<caption><p class="info">${text.digistat_typeSize} ${text.digistat_from_metadata}</p></caption>
		<#list collectionsTypes as stat>
			<#if stat_index == 0>
				<@header text.digistat_typeSize />
			<#elseif stat.subParentId != currentSubParentId>
				<@total text.digistat_subtotal subTotalCalculator subTotalCalculator.typeSizeTotal />
				<#assign subTotalCalculator = calculator.new>
				<@header text.digistat_typeSize />				
			</#if>
			<tr>
				<td>${stat.collection.qname.toString()?html}</td>
				<td><@name stat.collection /></td>
				<td>${stat.digitizedCount!"0"}</td>
				<td>${stat.imagedCount!"0"}</td>
				<td><@percent stat.digitizedCount!0 stat.collection.typeSpecimenSize!0 /></td>
				<td>${stat.collection.typeSpecimenSize!"--"}</td>
				<#list months as m><td>${stat.getMonth(m)}</td></#list>
			</tr>
			${subTotalCalculator.add(stat)!}
			${totalCalculator.add(stat)!}
			<#assign currentSubParentId = stat.subParentId>
		</#list>
		<@total text.digistat_subtotal subTotalCalculator subTotalCalculator.typeSizeTotal />
		<@header text.digistat_typeSize />
		<@total text.digistat_total totalCalculator totalCalculator.typeSizeTotal />
	</table>

	<h3>${text.digistat_specimen} ${text.digistat_by_prefix}</h3>

	<@prefixStat prefixes />
	
	<h3>${text.digistat_types} ${text.digistat_by_prefix}</h3>
	
	<@prefixStat prefixesTypes />
	
</div>

<script>
	$(function(){
		$("#changeYear").on('click', function(){
			 var url = new URL(window.location.href);
			url.searchParams.set('year', $("#year").val());
			window.location.replace(url.toString());
		});
	});
</script>

<#include "public-footer.ftl" />

<#macro prefixStat stats>
	<table>
		<tr>
			<th rowspan="2">&nbsp;</th>
			<th rowspan="2">${text.digistat_digitised}</th>
			<th rowspan="2">${text.digistat_imaged}</th>
			<th colspan="${months?size}">${text.digistat_digitised} ${year}</th>
		</tr>
		<tr>
			<#list months as m><th>${m}</th></#list>
		</tr>
		<#list stats as stat>
			<tr>
				<th>${stat.prefix?html}</th>
				<td>${stat.digitizedCount!"0"}</td>
				<td>${stat.imagedCount!"0"}</td>
				<#list months as m><td>${stat.getMonth(m)}</td></#list>
			</tr>
		</#list>
	</table>
</#macro>

<#macro header totalText>
<tr>
	<th rowspan="2">&nbsp;</th>
	<th rowspan="2">&nbsp;</th>
	<th rowspan="2">${text.digistat_digitised}</th>
	<th rowspan="2">${text.digistat_imaged}</th>
	<th rowspan="2">%</th>
	<th rowspan="2">${totalText}</th>
	<th colspan="${months?size}">${text.digistat_digitised} ${year}</th>
</tr>
<tr>
	<#list months as m><th>${m}</th></#list>	
</tr>
</#macro>

<#macro name collection>${collection.name.forLocale(locale)?html}</#macro>

<#macro percent amount total><#if total != 0>${((amount / total)*100)?string["0.#"]}%<#else>--</#if></#macro>

<#macro total typeText calculator size>
<tr class="sumRow">
	<th>&nbsp;</th>
	<th>${typeText}</th>
	<th>${calculator.digitizedCountTotal}</th>
	<th>${calculator.imagedCountTotal}</th>
	<th><@percent calculator.digitizedCountTotal size /></th>
	<th>${size}</th>
	<#list months as m><td>${calculator.getMonthCountTotal(m)}</td></#list>
</tr>
</#macro>
