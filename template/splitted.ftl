<#include "luomus-header.ftl">

<h1>Splitted document queue</h1>

<table>
	<thread>
		<tr>
			<th>Id</th>
			<th>Source</th>
			<th>Expires</th>
			<th>Data</th>
		</tr>
	</thead>
	<tbody>
	<#list splitted as s>
		<tr>
			<td>${s.id}</td>
			<td>${(sourceDescriptions[s.source.toURI()].name)!"Unknown"} - ${s.source}</td>
			<td>${s.expiryAsDateString()}</td>
			<td><pre>${s.data?html}</pre></td>
		</tr>
	</#list>
	</tbody>
</table>

<#include "luomus-footer.ftl">

