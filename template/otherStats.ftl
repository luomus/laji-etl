<div class="toolContainer">
	<table>
		<tbody>
			<tr>
				<th>Annotations</th>
				<td><a href="${baseURL}/console/annotations">${annotationCount}</a></td>
			</tr>
			<tr>
				<th>Sent notifications</th>
				<td><a href="${baseURL}/console/notifications/sent">${notificationSentCount!"0"}</a></td>
			</tr>
			<tr>
				<th>Unsent notifications</th>
				<td><a href="${baseURL}/console/notifications/unsent">${notificationUnsentCount!"0"}</a></td>
			</tr>
			<tr>
				<th>First load dates</th>
				<td>${firstLoadDateCount}</td>
			</tr>
			<tr>
				<th>Splitted document ids</th>
				<td>${splittedDocumentIdCount}</td>
			</tr>
			<tr>
				<th>Splitted document queue</th>
				<td><a href="${baseURL}/console/splitted-queue">${splittedDocumentQueueCount}</a></td>
			</tr>
		</tbody>
	</table>
</div>

<#function sum stats>
	<#assign c = 0>
	<#list stats as stat>
		<#assign c = c + stat.count>
	</#list>
	<#return c>
</#function>

<#macro printStat stat title>
<div class="toolContainer">
	<table>
		<caption>${title}</caption>
		<thead>
			<tr>
				<th>Source</th>
				<th>Count</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Sum</th>
				<th>${sum(loadStats.getStat(stat))}</th>
			</tr>
		</tfoot>
		<tbody>
			<#list loadStats.getStat(stat) as stat>
				<tr>
					<td>${stat.sourceId} - ${(sourceDescriptions[stat.sourceId.toURI()].name)!"Unknown"}</td>
					<td>${stat.count}</td>
				</tr>
			</#list>
		</tbody>
	</table>
</div>
</#macro>


<@printStat "DAY" "24 hours" />
<@printStat "WEEK" "7 days" />
<@printStat "MONTH" "Month" />

<#if unitAccumulation??>
<div class="toolContainer">
	<div id="unitAccumulationContainer" style="width:650px; height:320px;"></div>
	<script>
	$(function () { 
    	$('#unitAccumulationContainer').highcharts({
    		credits: { enabled: false },
        	chart: {
            	type: 'areaspline'
        	},
        	plotOptions: { areaspline: { marker: { enabled: false } } },
        	title: {
            	text: 'Unit accumulation (load date)'
        	},
        	xAxis: {
            	type: 'datetime',
            	title: {
                	text: 'Time'
            	}
        	},
        	yAxis: {
            	title: {
                	text: 'Units'
            	}
        	},
        	series: [{
        		showInLegend: false,
            	data: [
            		<#list unitAccumulation as day>
            			[Date.UTC(${day.day}), ${day.count}]<#if day_has_next>,</#if>
            		</#list>
            	]
        	}]
    	});
	});
	</script>
</div>
</#if>



