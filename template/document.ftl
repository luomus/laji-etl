<#include "luomus-header.ftl">

<#if inPipeEntry??>
	<a href="${baseURL}/console/in/entry/${inPipeEntry.id}">IN-Pipe Entry</a> | 
<#else>
	IN-Pipe entry no longer stored to ETL | 
</#if>

<#if outPipeEntry??>
	<a href="${baseURL}/console/out/entry/${outPipeEntry.id}">OUT-Pipe Entry</a>
<#else>
	OUT-Pipe entry no longer stored to ETL
</#if>

<#if document?has_content>

<h3><#if concealment == "PUBLIC">
		Public DW | Go to 
		<a href="${baseURL}/console/private/view?documentId=${document.documentId.toURI()?html}">private</a>
	<#else>
		Private DW | Go to
		<a href="${baseURL}/console/public/view?documentId=${document.documentId.toURI()?html}">public</a>
	</#if> 
</h3>
<h1>Document ${document.documentId.toURI()?html}</h1>
<h5>${(sourceDescriptions[document.sourceId.toURI()].name)!"Unknown"} - ${document.sourceId}</h5>
<h5>${collections[document.collectionId.toURI()].name.forLocale("fi")?html} - ${document.collectionId?html}</h5>

<div class="document">
	<h4>Document ${document.documentId?html}</h4>
	<div class="fields">
		<p><label>Collection</label> ${collections[document.collectionId.toURI()].name.forLocale("fi")?html} - ${document.collectionId?html}</p>
		<p><label>Secure level</label> ${document.secureLevel}</p>
		<p><label>Partial document</label> ${document.partial?string("Yes","No")}</p>
		<#if document.secureReasons??><p><label>Secure reason</label> <#list document.secureReasons as reason>${reason}<#if reason_has_next><br/><label></label> </#if></#list></p></#if>
		<#if document.createdDate??><p><label>Created</label>${document.createdDate?string("d.M.yyyy")}</p></#if>
		<#if document.modifiedDate??><p><label>Modified</label>${document.modifiedDate?string("d.M.yyyy")}</p></#if>
		<#if document.keywords?has_content>
			<label><h6>Keywords</h6></label>
			<#list document.keywords as keyword>
				<ul>
					<li><label></label>${keyword?html}</li>
				</ul>
			</#list>
		</#if>
		<#if document.linkings.editors?has_content>
			<label><h6>Editors</h6></label>
			<#list document.linkings.editors as person>
				<ul>
					<li>	<p><label>Userid</label>${person.userId?html}</p>
							<p><label>Id</label>${person.id!""}</p>
							<p><label>Full name</label>${(person.fullName!"")?html}</p>
					</li>
				</ul>
			</#list>
		</#if>
		<#if document.namedPlaceId??> <label>Named place ID</label> ${document.namedPlaceId?html} </#if>
		<#if document.notes??><p> <label>Notes</label> ${document.notes?html} </p></#if>
		<#list document.media as media>
			<@printMedia media />
		</#list>
	</div>
	<@facts document.facts />
	
	<#list document.gatherings as gathering>
		<h4>Gathering ${gathering.gatheringId?html}</h4>
		<div class="fields">
			<#if gathering.eventDate??>
				<p>
					<label>Time</label> 
						<#if gathering.eventDate.begin??>${gathering.eventDate.begin?string("d.M.yyyy")}</#if> - 
						<#if gathering.eventDate.end??>${gathering.eventDate.end?string("d.M.yyyy")}</#if>
						<#if gathering.timeBegin??>${gathering.timeBegin}</#if>
						<#if gathering.timeEnd??> - ${gathering.timeEnd} </#if>
				</p>
				<p>
					<label>Season</label> 
					<#if gathering.conversions.dayOfYearBegin??>${gathering.conversions.dayOfYearBegin}</#if>
					<#if gathering.conversions.dayOfYearEnd??> - ${gathering.conversions.dayOfYearEnd}</#if>
				</p>
			<#else>
				<p><label>Time</label> [not reported]</p>
			</#if>
			<#if gathering.conversions?? && gathering.conversions.wgs84??>
				<p> TODO MAP </p>
			</#if>
			<#if gathering.interpretations.sourceOfCoordinates??>
				<p> <label>Source of coordinates</label> [${gathering.interpretations.sourceOfCoordinates}] </p>
			</#if>
			<#if gathering.coordinatesVerbatim??>
				<p><label>Coordinates vertabim</label> ${gathering.coordinatesVerbatim?html}</p>
			</#if>
			<#if gathering.interpretations.coordinateAccuracy??>
				<p><label>Coordinate accuracy (m)</label> ${gathering.interpretations.coordinateAccuracy} </p> 
			</#if>
			<#if gathering.conversions?? && gathering.conversions.ykj??>
				<p><label>YKJ</label> 
					<#if gathering.conversions.ykj.latMin == gathering.conversions.ykj.latMax && gathering.conversions.ykj.lonMin == gathering.conversions.ykj.lonMax>
						${gathering.conversions.ykj.latMin}:${gathering.conversions.ykj.lonMin} 
					<#else>
						${gathering.conversions.ykj.latMin} - ${gathering.conversions.ykj.latMax} : ${gathering.conversions.ykj.lonMin} - ${gathering.conversions.ykj.lonMax}
					</#if>
				</p>
				<#if gathering.conversions.ykj4??><p><label>YKJ4</label> ${gathering.conversions.ykj4.lat}:${gathering.conversions.ykj4.lon} </p></#if>
				<#if gathering.conversions.ykj3??><p><label>YKJ3</label> ${gathering.conversions.ykj3.lat}:${gathering.conversions.ykj3.lon} </p></#if>
			</#if>
			<#if gathering.higherGeography??><p> <label>Higher Geography verbatim</label> ${gathering.higherGeography?html} </p></#if>
			<#if gathering.country??><p> <label>Country verbatim</label> ${gathering.country?html} </p></#if>
			<#if gathering.interpretations.country??>
				<p> <label>Country</label> ${areas[gathering.interpretations.country.toURI()]} - ${gathering.interpretations.country.toURI()} [${gathering.interpretations.sourceOfCountry}] </p>
			</#if>
			<#if gathering.province??><p> <label>Administrative Province verbatim</label> ${gathering.province?html} </p></#if>
			<#if gathering.biogeographicalProvince??><p> <label>Biogeographical Prov. verbatim</label> ${gathering.biogeographicalProvince?html} </p></#if>
			<#if gathering.interpretations.biogeographicalProvince??>
				<p> <label>Biogeographical Province</label> ${areas[gathering.interpretations.biogeographicalProvince.toURI()]} - ${gathering.interpretations.biogeographicalProvince.toURI()} [${gathering.interpretations.sourceOfBiogeographicalProvince}] </p>
			</#if>
			<#if gathering.municipality??><p> <label>Municipality verbatim</label> ${gathering.municipality?html} </p></#if>
			<#if gathering.interpretations.finnishMunicipality??>
				<p> <label>Finnish Municipality</label> ${areas[gathering.interpretations.finnishMunicipality.toURI()]} - ${gathering.interpretations.finnishMunicipality.toURI()} [${gathering.interpretations.sourceOfFinnishMunicipality}] </p>
			</#if>
			<#if gathering.locality??><p> <label>Locality verbatim</label> ${gathering.locality?html} </p></#if>
			<label>Team</label> <#list gathering.team as teamMember>${teamMember}<#if teamMember_has_next>, </#if></#list>
			<#if gathering.linkings.teamMembers?has_content>
				<label><h6>Known observers</h6></label>
				<#list gathering.linkings.observers as person>
					<ul>
						<li>	<p><label>Userid</label>${person.userId?html}</p>
								<p><label>Id</label>${person.id!""}</p>
								<p><label>Full name</label>${(person.fullName!"")?html}</p>
						</li>
					</ul>
				</#list>
			</#if>
			<#if gathering.notes??><p> <label>Notes</label> ${gathering.notes?html} </p></#if>
			<#list gathering.media as media>
				<@printMedia media />
			</#list>
		</div>
		<@facts gathering.facts />
		
		<#list gathering.units as unit>
			<#assign hasTaxon = false>
			<#if unit.linkings?? && unit.linkings.taxon??>
				<#assign taxon = unit.linkings.taxon />
				<#assign hasTaxon = true />
			</#if>
			<h4>Unit ${unit.unitId}</h4>
			<div class="fields">
				<#if unit.individualId??><p><label>Individual id</label> ${unit.individualId?html}</p></#if>
				<#if unit.recordBasis??><p><label>Record basis</label> ${unit.recordBasis?html}</p></#if>
				<#if unit.taxonVerbatim??> <p><label>Taxon name verbatim</label> ${unit.taxonVerbatim?html} &nbsp; [<#if !hasTaxon><span style="color: red;">UNKNOWN NAME</span><#else><span style="color: green;">KNOWN NAME</span></#if>] </p></#if>
				<#list unit.annotations as annotation>
					<p><label>Annotation</label> ${(annotation.identification!"")?html} ${(annotation.reliabilityAnnotation!"")?html} ${(annotation.notes!"")?html} </p>
				</#list>
				<#if unit.det??><p> <label>Det verbatim</label> ${unit.det?html} </p></#if>
				<#if unit.author??><p> <label>Author verbatim</label> ${unit.author?html} </p></#if>
				<#if unit.referencePublication??><p> <label>Reference</label> ${unit.referencePublication?html} </p></#if>
				<#if unit.interpretations?? && unit.interpretations.individualCount??>
					<p> <label>Individual count</label> ${unit.interpretations.individualCount} </p>
				<#elseif unit.abundanceString??>
					<p> <label>Abundance</label> ${unit.abundanceString?html} </p>
				</#if>
				<#if unit.typeSpecimen??><p><label>Type specimen</label> ${unit.typeSpecimen}</p></#if>
				<#if unit.breedingSite??><p> <label>Breeding site</label> ${unit.breedingSite} </p></#if>
				<#if unit.lifeStage??><p> <label>Life stage</label> ${unit.lifeStage} </p></#if>
				<#if unit.sex??><p> <label>Sex</label> ${unit.sex} </p></#if>
				<#if unit.notes??><p> <label>Notes</label> ${unit.notes?html} </p></#if>
				<#if hasTaxon>
					<label><h6>Taxonomy</h6></label>
					<p><label><#if taxon.taxonRank??>${taxon.taxonRank?replace("MX.", "")}</#if></label> <span style="display: inline-block; width: 250px; <#if taxon.cursiveName>font-style: italic;</#if>">${taxon.scientificName!""} &nbsp; ${taxon.scientificNameAuthorship!""}</span> ${taxon.vernacularName.forLocale("fi")!""}</p>
				</#if>
				<#list unit.media as media>
					<@printMedia media />
				</#list>
			</div>
			<@facts unit.facts />
		</#list>
		
	</#list>
	
</div>

<#macro printMedia media>
	<#if media.mediaType.name() == "IMAGE">
		<div class="image">
			<#assign thumbnailURL = media.fullURL />
			<#if media.squareThumbnailURL?has_content>
				<#assign thumbnailURL = media.squareThumbnailURL />
			<#elseif media.thumbnailURL?has_content>
				<#assign thumbnailURL = media.thumbnailURL />
			</#if>
			<a href="${media.fullURL?html}"><img src="${thumbnailURL?html}" alt="${(media.caption!"")?html}" /></a>
			<#if media.caption??> <label> Caption</label> ${media.caption?html} </br></#if>
			<#if media.author??> <label> Author</label> ${media.author?html}</#if>
		</div>
	<#else>
		<p>No support yet to show ${media.mediaType.name()}</p>
	</#if>
</#macro>

<#macro facts facts>
<#if facts?has_content>
<div class="facts">
	<h5><a href="#">Facts &#9660;</a></h5>
	<ul style="display: none;">
		<#list facts as fact>
			<li><label>${fact.fact?html}</label> ${fact.value?html} </li>
		</#list>
	</ul>
</div>
</#if>
</#macro>

<p style="clear:both;"></p>
<br />
<br />
<hr />
<br />

<h3>DW Data as JSON</h3>
<pre id="json">
</pre>

<#if inPipeEntry??>
	<h3>Original IN-Pipe Data</h3>
	<pre>${inPipeEntry.data?html}</pre>
</#if>


<script>

$(function() {
	$(".facts h5 a").on('click', function() {
		$(this).closest('.facts').find('ul').fadeIn('slow');
		return false;
	});
});

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

var data = ${json?replace("'", "")};
var str = JSON.stringify(data, undefined, 4);

$("#json").html(syntaxHighlight(str));

</script>


<#else>
	<div class="errorMessage">Document not found!</div>
</#if>


<#include "luomus-footer.ftl">