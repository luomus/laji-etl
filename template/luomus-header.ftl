<!DOCTYPE html>

<!--[if lt IE 9 ]> <body class="oldie"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="${locale}"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for supporting devices (iOS, Android etc) -->
	<meta name="viewport" content="initial-scale=1.0" />

	<title>${text["title_"+page]} | ${text.title}</title>
	<link href="${staticURL}/favicon.ico?${staticContentTimestamp}" type="image/ico" rel="shortcut icon" />
	
	<script src="${staticURL}/jquery-1.9.1.js"></script>
	
	<link href="${staticURL}/jquery-ui/css/cupertino/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" /> 
	<script src="${staticURL}/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
	
	<script src="${staticURL}/chosen/chosen.jquery.min.js?${staticContentTimestamp}"></script>
	<link href="${staticURL}/chosen/chosen.min.css?${staticContentTimestamp}" rel="stylesheet" />
	
	<script src="${staticURL}/Highcharts-4.2.4/js/highcharts.js?${staticContentTimestamp}"></script>
	
	<link href="${staticURL}/luomus.css?${staticContentTimestamp}" rel="stylesheet" />
	<link href="${staticURL}/triplestore.css?${staticContentTimestamp}" rel="stylesheet" />
	<link href="${staticURL}/console.css?${staticContentTimestamp}" rel="stylesheet" />
	
	<#include "javascript.ftl">
	
</head>

<body>

	<div id="masthead" role="banner">
		<div id="masthead-inner">
		
			<#if user??>
				<div id="userinfo">
					<ul>
             			<li><a href="${baseURL}/console/logout" class="button">Logout</a></li>
              			<li style="color: green; font-weight: bold;">ADMIN</li>
              			<li>Logged in as <span class="name">${user.fullname?html}</span> (${user.qname.toString()?html})</li>
          			</ul>
				</div>
			</#if>
			
			<div id="logo">&nbsp;</div>

			<div id="sitetitle">
				${text.title}
			</div>
			
			<#if inStagingMode>     <span class="devmode">TEST ENVIRONMENT</span></#if>	
    		<#if inDevelopmentMode> <span class="devmode">DEV ENVIRONMENT</span></#if>	
			
			
			<div id="navigation-wrap" role="navigation">
				<nav id="mainmenu" role="navigation">
					<ul class="nav-bar" role="menu">
						<#if user??>
							<li role="menuitem"><a href="${baseURL}/console">${text.title_console}</a></li>
							<li role="menuitem"><a href="${baseURL}/console/reports">Reports</a></li>
						</#if>
						<#if page == "citation-viewer">
							<li role="menuitem"><a href="?locale=fi">suomeksi</a></li>
							<li role="menuitem"><a href="?locale=sv">på svenska</a></li>
							<li role="menuitem"><a href="?locale=en">in English</a></li>
						</#if>
					</ul>
				</nav>
		    </div>
		    
		</div>
	</div>
	
	<!-- Content section -->
	<div id="main-area" role="main">
		<div id="content-wrapper">
		
		<div id="content" class="page-content">
