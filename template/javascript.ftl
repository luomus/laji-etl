<script>

$(function() {
	
	if ($("body").attr("class") === "oldie") {
		var text = 'Your browser is not supported. Please use Mozilla Firefox, Google Chrome  (both are reccomended) or newer version of IE (10,11) (not reccomended)'; 
		$("html").html(text);
		alert(text);
	}
	
	$("input[type=submit], button, .button").button();
			
});

</script>
