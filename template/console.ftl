<#include "luomus-header.ftl">

<h1>${text.title_console}</h1>

<p>
<#if minimal == "false">
	<b>Robust view</b> | Change to <a href="${baseURL}/console?minimal=true">minimal view</a>
<#else>
	<b>Minimal view</b> | Change to <a href="${baseURL}/console?minimal=false">robust view</a>
</#if>
</p>

<#if successMessage?has_content><div class="success">${successMessage}</div><div class="clear"></div></#if>

<#if debug??><div class="info">${debug}</div><div class="clear"></div></#if>

<div class="toolContainer" >
	<label>Go to document viewer</label>
	<input id="goToViewerDocId" value="" placeholder="URI or Qname"/>
	<button onclick="view('public')">Public</button>
	<button onclick="view('private')">Private</button>
</div>

<hr/>

<h4>ETL Tables</h4>
<div class="toolContainer" id="pipeContents" style="position: relative;"><@loadingSpinner /></div>

<div class="toolContainer">
	<div id="threadStatusContents" style="position: relative;"><@loadingSpinner /></div>
</div>

<#if minimal == "false">
<h4>Other stats</h4>
<div id="otherStats"><@loadingSpinner /></div>
</#if>

<h4>Log</h4>
<table>
	<thead>
		<tr>
			<th rowspan="2">Time</th>
			<th rowspan="2">Source</th>
			<th rowspan="2">Phase</th>
			<th rowspan="2">Type</th>
			<th rowspan="2">Identifier</th>
			<th style="position: relative;"><#if minimal == "false"><button id="stopLogRefresh">Stop log refresh</button></#if></th>
		</tr>
		<tr>
			<th>Message</th>
		</tr>
	</thead>
	<tbody id="logContents">
		<tr><td colspan="5"><@loadingSpinner /></td></tr>
	</tbody>
</table>


<#if minimal == "false">
<div>
	<div class="toolContainer">
		<h4>Failed download requests</h4>
		<@printRequests failedDownloadRequests />
	</div>
	<div class="toolContainer">
		<h4>Download requests today</h4>
		<@printRequests downloadRequestsToday />
	</div>
</div>
</#if>

<#macro printRequests requests>
<#if requests?has_content>
	<ul>	
		<#list requests as request>
			<li>
				${request.id?html}
				${request.downloadType?html}
				${request.warehouse?html}
				${(request.personEmail!"No email")?html} 
				${(request.filters!"")?html}
				<#if request.failed>
					<div style="color: red;">
						<pre>${request.failureMessage?html}</pre>
					</div>
				</#if>
			</li>
		</#list>
	</ul>
</#if>
</#macro>

<div class="toolContainer" id="tools">
<h4>Tools</h4>
<ul>
	<li>
		<label for="removeFromSource">Delete document</label>
		<select name="removeFromSource" id="removeFromSource" data-placeholder="Select source" class="chosen">
			<option value="">&nbsp;</option>
			<#list sourceDescriptions?keys as sourceId>
				<option value="${sourceId}">${sourceId} - ${sourceDescriptions[sourceId].name!"Unknown"}</option>
			</#list>
		</select>
		<input id="removeDocumentId" name="removeDocumentId" type="text" placeholder="Document URI" />
		<button id="deleteDocumentButton">Delete</button>
	</li>
	
	<li>
		<label for="startSource">Start a new source</label>
		<select name="startSource" id="startSource" data-placeholder="Select source" class="chosen"/>
			<option value="">&nbsp;</option>
			<#list sourceDescriptions?keys as sourceId>
				<option value="${sourceId}">${sourceId} - ${sourceDescriptions[sourceId].name!"Unknown"}</option>
			</#list>
		</select>
		<button id="startnewSourceButton">Start</button>
	</li>

	<li>
		<label>Start taxon/person linking</label> <button onclick="startLinkingReprocess();">Start</button>
	</li>

	<li>
		<label>Start named place update</label> <button onclick="startNamedPlaceUpdate();">Start</button>
	</li>
	
	<li>
		<label>Start collection update</label> <button onclick="startCollectionUpdate();">Start</button>
	</li>
	
	<li>
		<label>Add document ids to reprocess_temp</label> <a href="${baseURL}/console/add-reprocess-temp">Here</a>
	</li>
	
	<li>
		<label for="reprocessTaxonById">Re-process by taxon id</label>
		<input type="text" name="reprocessTaxonById" id="reprocessTaxonById" data-placeholder="MX." />
		<button id="reprocessTaxonByIdButton">Re-process</button>
	</li>
	
	<li>
		<label>Run currently deployed reprocess_temp script</label> <button onclick="startErrorReprocess();">Start</button>
	</li>	
	
	<li>
		<label>Sync document IDs from ETL to Vertica and Vertica to ETL for debugging</label> <button onclick="startDocumentIdSync();">Start</button>
	</li>	

	<li>
		<label>Reload all GBIF datasets</label> <button onclick="gbifReload();">Start</button>
	</li>
	
	<li>
		<label>LajiGIS sync</label> <button onclick="lajigisSync();">Start</button>
	</li>
	
	<li>
		<label>Tampere sync</label> <button onclick="tampereSync();">Start</button>
	</li>
	
	<li>
		<label>Helsinki sync</label> <button onclick="helsinkiSync();">Start</button>
	</li>
</ul>
</div>



<div class="toolContainer">
<h4>Taxon secure levels</h4>
<table>
	<#list taxaWithSecureLevels as taxon>
		<#if taxon_index % 20 == 0>
			<tr>	
				<th>Qname</th>
				<th>Scientific name</th>
				<th>Finnish name</th>
				<th>Secure level</th>
				<th>Breeding season</th>
				<th>Wintering season</th>
				<th>Nest site</th>
				<th>Natura 2000 area</th>
				<th>&nbsp;</th>
			</tr>
		</#if>
		<tr>
			<td>${taxon.qname}</td>
			<td>${taxon.scientificName!""}</td>
			<td>${taxon.vernacularName.forLocale("fi")!""}</td>
			<td>${(taxon.secureLevel!"")?replace("MX.secureLevel", "")?replace("KM", "")}</td>
			<td>${(taxon.breedingSecureLevel!"")?replace("MX.secureLevel", "")?replace("KM", "")}</td>
			<td>${(taxon.winteringSecureLevel!"")?replace("MX.secureLevel", "")?replace("KM", "")}</td>
			<td>${(taxon.nestSiteSecureLevel!"")?replace("MX.secureLevel", "")?replace("KM", "")}</td>
			<td>${(taxon.naturaAreaSecureLevel!"")?replace("MX.secureLevel", "")?replace("KM", "")}</td>
			<td><button class="reProcessTaxonButton" data-taxonId="${taxon.qname}">Re-process</td>
		</tr>
	</#list>
</table>
</div>

<#if minimal == "false">
<div class="toolContainer">
<h4>Unlinked user IDs</h4>
<table>
	<tr>
		<th>User ID</th>
		<th>Source</th>
		<th>Count</th>
	</tr>
	<#list unlinkedUserIds as data>
		<tr>
			<td>${data.userId?html}</td>
			<td>${data.sourceId} - ${(sourceDescriptions[data.sourceId.toURI()].name)!"Unknown"}</td>
			<td>${data.count}</td>
		</tr>
	</#list>
</table>
</div>

<div class="toolContainer">
<h4>Unlinked target names</h4>
<table>
	<tr>
		<th>Target name</th>
		<th>Reference</th>
		<th>Linked but not exact</th>
		<th>Taxon id (not exact)</th>
		<th>Count</th>
	</tr>
	<#list unlinkedTargetNames as target>
		<tr>
			<td>${(target.targetName!"Null")?html}</td>
			<td><#if target.reference??>${target.reference}</#if></td>
			<td><#if target.notExactMatch?? && target.notExactMatch>Yes</#if></td>
			<td>${(target.taxonId!"")?html}</td>
			<td>${target.count}</td>
		</tr>
	</#list>
</table>
</div>
</#if>


<script>
var logIntervalId = null;
$(function() {
	$(".chosen").chosen();
	loadPipeStats();
	loadOtherStats();
	loadThreadStatuses();
	loadLog();
	<#if minimal == "false">
	setInterval(loadPipeStats, 5000);
	setInterval(loadThreadStatuses, 2000);
	logIntervalId = setInterval(loadLog, 10000);
	</#if>
	$("#stopLogRefresh").on('click', function(){
		clearInterval(logIntervalId);
		$(this).fadeOut('slow');
	});
	$(".reProcessTaxonButton").on('click', function() {
		var taxonId = $(this).attr('data-taxonId');
		startReProcessTaxon(taxonId);
	});
});
	
$("#reprocessTaxonByIdButton").on('click', function() {
	var taxonId = $("#reprocessTaxonById").val();
	startReProcessTaxon(taxonId);
});
function startReProcessTaxon(taxonId) {
	if (confirm('Are you sure you want to re-process all documents containing units for taxon ' + taxonId)) {
		$.post('${baseURL}/console/start-taxon-reprocess/'+taxonId);
		$("html, body").animate({
        	scrollTop: $("#threadStatusContents").offset().top - 100
    	}, 700);
    }
}
function loadPipeStats() {
	$.get('${baseURL}/console/pipe-stats', function(data) {
       $("#pipeContents").html(data);
	});
}
function loadThreadStatuses() {
	$.get('${baseURL}/console/thread-statuses', function(data) {
       $("#threadStatusContents").html(data);
       var loaded = $('<div class="refreshed"><img src="${staticURL}/img/loading_whitebg.gif?${staticContentTimestamp}" alt="Loading.." /></div>');
       $("#threadStatusContents").append(loaded);
       loaded.fadeOut(500, function() {$(this).remove();});
	});
}
function loadLog() {
	$.get('${baseURL}/console/log-entries', function(data) {
       $("#logContents").html(data);
       var loaded = $('<div class="refreshed"><img src="${staticURL}/img/loading_whitebg.gif?${staticContentTimestamp}" alt="Loading.." /></div>');
       $("#stopLogRefresh").after(loaded);
       loaded.fadeOut(500, function() {$(this).remove();});
	});
}
function loadOtherStats() {
	$.get('${baseURL}/console/other-stats', function(data) {
       $("#otherStats").html(data);
	});
}

function startLinkingReprocess() {
	if (!confirm('Force linking reprocessing to start?')) return;
	$.post('${baseURL}/console/start-linking-reprocess');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function startNamedPlaceUpdate() {
	if (!confirm('Start named place update?')) return;
	$.post('${baseURL}/console/start-named-place-update');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function startCollectionUpdate() {
	if (!confirm('Start collection update?')) return;
	$.post('${baseURL}/console/start-collection-update');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function startErrorReprocess() {
	if (!confirm('Force various erroreous document reprocessing to start?')) return;
	$.post('${baseURL}/console/start-error-reprocess');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function startDocumentIdSync() {
	if (!confirm('Start documentId sync?')) return;
	$.post('${baseURL}/console/documentid-sync');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function gbifReload() {
	if (!confirm('Reload all GBIF datasets and occurrences?')) return;
	$.post('${baseURL}/console/gbif-reload');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function lajigisSync() {
	if (!confirm('Start LajiGIS sync?')) return;
	$.post('${baseURL}/console/lajigis-sync');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function tampereSync() {
	if (!confirm('Start Tampere sync?')) return;
	$.post('${baseURL}/console/tampere-sync');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

function helsinkiSync() {
	if (!confirm('Start Helsinki sync?')) return;
	$.post('${baseURL}/console/helsinki-sync');
	$("html, body").animate({
        scrollTop: $("#logContents").offset().top - 100
    }, 700);
}

$("#startnewSourceButton").on('click', function() {
	var source = $("#startSource").val();
	if (!source) { alert('Select source first!'); return; }
	$.post('${baseURL}/console/start-threads/'+source);
	$("html, body").animate({
        scrollTop: $("#threadStatusContents").offset().top - 100
    }, 700);
});

$("#deleteDocumentButton").on('click', function() {
	var source = $("#removeFromSource").val();
	if (!source) { alert('Select source first!'); return; }
	var documentId = $("#removeDocumentId").val();
	if (!documentId) { alert('Enter full URI to delete!'); return; }
	$.post('${baseURL}/console/delete-document?source='+source+'&documentId='+documentId);
	alert('ok');
});
		
function view(concealment) {
	var docId = $("#goToViewerDocId").val().trim();
	if (!docId) return;
	window.location.href = '${baseURL}/console/' + concealment + '/view?documentId=' + encodeURIComponent(docId);
}

</script>
<#include "luomus-footer.ftl">

<#macro loadingSpinner><@compress single_line=true>
	<div class="loadingSpinner">
    	<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>  
    	Loading &nbsp; &nbsp; <img src="${staticURL}/img/loading_whitebg.gif?${staticContentTimestamp}" alt="Loading.." /> 
    </div>
</@compress></#macro>