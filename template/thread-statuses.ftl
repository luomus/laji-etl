<table id="threadStatuses">
	<tr>
		<th>Source</th>
		<th>Thread</th>
		<th colspan="2">Status</th>
	</tr>
	<#list statuses as status>
	<tr>
		<td>${status.source} - ${(sourceDescriptions[status.source.toURI()].name)!"Unknown"}</td>
		<td>${(status.threadName!"")?html}</td>
		<td class="statusline">${(status.status!"")?html}</td>
		<td><button onclick="stopThread('${status.source}');">Stop</button></td>
	</tr>	
	</#list>
	<#if !statuses?has_content>
		<tr>
			<td colspan="4">No threads running!</td>
		</tr>
	</#if>
</table>

<script>
	$(function() {
		$("button").button();
	});
	
	function stopThread(source) {
		$.post('${baseURL}/console/stop-threads/'+source);
	}
</script>