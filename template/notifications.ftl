<#include "luomus-header.ftl">

<h1>Notifications</h1>

<table>
	<thread>
		<tr>
			<th>Id</th>
			<th>Receiver</th>
			<th>Reason</th>
			<th>Annotation id</th>
			<th>Root id</th>
			<th>Target id</th>
			<th>Deleted</th>
			<th>Valid</th>
			<th>Added tags</th>
			<th>Removed tags</th>
			<th>Identification</th>
			<th>Notes</th>
			<th>Person</th>
			<th>System</th>
		</tr>
	</thead>
	<tbody>
	<#list notifications as n>
		<#assign a = n.annotation>
		<tr>
			<td>${n.id}</td>
			<td>${n.personId}</td>
			<td>${n.notificationReason!""}</td>
			<td>${a.id?html}</td>
			<td>${a.rootID?html}</td>
			<td>${a.targetID?html}</td>
			<td>${a.deleted?string("yes", "no")}</td>
			<td>${(a.valid?string("yes", "no"))!""}</td>
			<td><#list a.addedTags as t>${t}</#list></td>
			<td><#list a.removedTags as t>${t}</#list></td>
			<td><#if a.identification??> ${a.identification.toString()?html} </#if></td>
			<td>${(a.notes!"")?html}</td>
			<td>${a.annotationByPerson!""}</td>
			<td>${a.annotationBySystem!""}</td>
		</tr>
	</#list>
	</tbody>
</table>

<#include "luomus-footer.ftl">

