<#include "luomus-header.ftl">

<#if missingFrom??>
	<div class="errorMessage">
		This document is missing from the ${missingFrom} warehouse! Showing out pipe entry instead!
	</div>
</#if>

<div id="entry">

<#if annotations??>
	<h3>Annotations</h3>
	<ul>
	<#list annotations as a>
		<li>
			${a.id?html} | ${a.rootID?html} ${a.targetID?html} | ${a.created} | ${a.annotationByPerson!""} ${a.annotationBySystem!""} | <#if a.deleted>DELETED |</#if>
			Added: <#list a.addedTags as t>${t}</#list> | Removed: <#list a.removedTags as t>${t}</#list> | <#if a.identification??> ${a.identification.toString()?html} </#if>
			<#if a.notes?has_content>| ${a.notes?html}</#if> 
		</li> 
	</#list>
	</ul>
</#if>

<#if outPipeEntry??>
	<h3>OUT-Pipe Entry</h3>
	<h4>Id</h4> ${outPipeEntry.id}
	<h4>IN-Pipe Id</h4> <a href="${baseURL}/console/in/entry/${outPipeEntry.inPipeId}">${outPipeEntry.inPipeId}</a>
	<h4>Source</h4> ${outPipeEntry.source.toURI()} - ${(sourceDescriptions[outPipeEntry.source.toURI()].name)!"Unknown"}
	<h4>DocumentId</h4> ${outPipeEntry.documentId.toURI()?html} &nbsp; 
				<a href="${baseURL}/console/public/view?documentId=${outPipeEntry.documentId?html}">public</a>
				<a href="${baseURL}/console/private/view?documentId=${outPipeEntry.documentId?html}">private</a>
				<#if outPipeEntry.splittedDocumentIds?has_content>
					<p>Splitted documents</p>
					<ul>
						<#list outPipeEntry.splittedDocumentIds as splitted>
							<li>
								${splitted} &nbsp; 
								<a href="${baseURL}/console/public/view?documentId=${splitted?html}">public</a>
							</li>
						</#list>
					</ul>
				</#if>
	<h4>Timestamp</h4> ${outPipeEntry.timestamp?string("d.M.yyyy - HH:mm:ss.SSS")}
	<h4>Error message</h4> <pre>${(outPipeEntry.errorMessage!"")?html}</pre>
	<h4>Data</h4> <pre id="data">${outPipeEntry.data?html}</pre>
	
	<br />
	<a onclick="return confirm('Re-process?');" href="${baseURL}/console/reprocess/out/${outPipeEntry.id}">Re-process</a><br/><br/>
	<#if outPipeEntry.errorMessage?has_content>
		<a onclick="return confirm('Remove?');" href="${baseURL}/console/delete/out/${outPipeEntry.id}">Remove</a><br/><br/>
	</#if>

<script>
var data = '${outPipeEntry.data?replace("'", "")?replace("\\\"", "")}';
	
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

var obj = JSON.parse(data);
var str = JSON.stringify(obj, undefined, 4);

$("#data").html(syntaxHighlight(str));
</script>
</#if>


<#if inPipeEntry??>
	<h3>IN-Pipe Entry</h3>
	<h4>Id</h4> ${inPipeEntry.id}
	<h4>Source</h4> ${inPipeEntry.source.toURI()} - ${(sourceDescriptions[inPipeEntry.source.toURI()].name)!"Unknown"}
	<h4>Content-Type</h4> ${inPipeEntry.contentType}
	<h4>Timestamp</h4> ${inPipeEntry.timestamp?string("d.M.yyyy - HH:mm:ss.SSS")}
	<#if inPipeEntry.unrecoverableError><h4>Unrecoverable error</h4><span class="errorMessage">Yes</span></#if>
	<h4>Error message</h4> <pre>${(inPipeEntry.errorMessage!"")?html}</pre>
	<h4>Data</h4> <pre>${(inPipeEntry.data!"")?html}</pre>
	
	<br />
	
	<a onclick="return confirm('Re-process?');" href="${baseURL}/console/reprocess/in/${inPipeEntry.id}">Re-process</a>
	 
</#if>


</div>

<#include "luomus-footer.ftl">