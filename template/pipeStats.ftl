<table>
	<thead>
	<tr>
		<th rowspan="2">Source</th>
		<th colspan="4">Harmonization phase (IN)</th>
		<th colspan="4">AISCS* phase (OUT)</th>
	</tr>
	<tr>
		<th>Received</th>
		<th>Processed</th>
		<th>Waiting</th>
		<th>In Error</th>
		<th>Received</th>
		<th>Processed (deletions)</th>
		<th>Waiting</th>
		<th>In Error</th>
	</tr>
	</thead>
	<#list pipeStats.sources as sourceId>
	<#assign inpipe = pipeStats.getIn(sourceId) />
	<#assign outpipe = pipeStats.getOut(sourceId) />
	<tr>
		<td>${sourceId} - ${(sourceDescriptions[sourceId.toURI()].name)!"Unknown"}</td>
		<td><a href="${baseURL}/console/in/received/${sourceId}">${inpipe.received}</a></td>
		<td><a href="${baseURL}/console/in/processed/${sourceId}">${inpipe.processed}</a></td>
		<#if (inpipe.waiting > 0)>
			<td class="hasWaiting"><a href="${baseURL}/console/in/waiting/${sourceId}">${inpipe.waiting}</a></td>
		<#else>
			<td>0</td>
		</#if>
		<#if (inpipe.inError > 0)>
			<td class="hasErrors"><a href="${baseURL}/console/in/error/${sourceId}">${inpipe.inError}</a></td>
		<#else>
			<td>0</td>
		</#if>
		<td><a href="${baseURL}/console/out/received/${sourceId}">${outpipe.received}</a></td>
		<td>
			<a href="${baseURL}/console/out/processed/${sourceId}">${outpipe.processed}</a>
			<#if (outpipe.deleted > 0)>
				(<a href="${baseURL}/console/out/deleted/${sourceId}">${outpipe.deleted}</a>)
			</#if>
		</td>
		<#if (outpipe.waiting > 0)>
			<td class="hasWaiting"><a href="${baseURL}/console/out/waiting/${sourceId}">${outpipe.waiting}</a></td>
		<#else>
			<td>0</td>
		</#if>
		<#if (outpipe.inError > 0)>
			<td class="hasErrors"><a href="${baseURL}/console/out/error/${sourceId}">${outpipe.inError}</a></td>
		<#else>
			<td>0</td>
		</#if>
	</tr>	
	</#list>
</table>
<p class="info">* = Annotate-Interpret-Secure-Convert-Store  (and Notify)</p>