<#include "luomus-header.ftl">

<h1>Add document ids to reprocess_temp table</h1>

<#if successMessage?has_content>
	<div class="success">${successMessage?html}</div>
</#if>

<form action="${baseURL}/console/add-reprocess-temp" method="POST">
	<p><label>Document ids</label></p>
	<textarea name="document_ids" rows="30" cols="90"></textarea>
	<p><input type="submit" value="Submit"/></p>
</form>

<#include "luomus-footer.ftl">

