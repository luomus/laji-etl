Tämän latauksen tunniste: http://tun.fi/REQ-ID

Lataus on tehty 10.11.2015 seuraavilla rajauksilla:
Kohde (laji): Aves
Aika: 2000/2010

Linkki hakuun:
https://laji.fi/fi/observation/map?target=Aves&time=2000%2F2010

-------------------------------------------

Käyttöehdot
-----------

Saadaksesi tämän latauksen tiedot olet hyväksynyt aineistoja koskevat ehdot.
Ehdot ovat luettavissa aineistopyyntöpalvelusta, josta latasit nämä tiedostot.

Käyttöehdot hyväksyessäsi olet sitoutunut:

a. noudattamaan erityistä varovaisuutta, jotta vältetään julkisuuslaissa 
kuvattu lajin vaarantuminen ja/tai huomioimaan muut käyttörajoituksen 
aiheuttaneet perusteet 

b. käyttämään tietoa vain ja yksinomaan siihen tarkoitukseen, jota varten 
aineisto on luovutettu 

c. olemaan jakamatta aineistoja kenellekään ulkopuoliselle

d. viittaamaan aineistoon viittausohjeiden mukaan ja huomioimaan, ettei 
sensitiivisten lajien tarkkoja sijaintitietoja saa julkaista. 

-------------------------------------------

Viittausohje
------------

Suomen Lajitietokeskus/FinBIF. http://tun.fi/FOOBAR, http://tun.fi/HR.48 
(haettu 10.11.2015). 

Jos käytät vain osaa aineistoista, on suositeltavaa, että viittaat vain niihin 
aineistoihin. 

Aineistot
---------

Luettelo aineistoista, jotka sisältyvät hakuun:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Rengastus- ja löytörekisteri (TIPU) - http://tun.fi/HR.48
Creative Commons Nimeä
Lisätietoja tämän aineiston käytöstä antaa jari.valkama@helsinki.fi


Ohjeet
------

Havaintoaineistojen käyttö paikkatietona: https://laji.fi/about/5138
Tiedoston vienti Exceliin: https://laji.fi/about/1068

===========================================

Identifier of this download: http://tun.fi/REQ-ID

This download is made on 2015-11-10 using the following filters:
Target (taxon): Aves
Time: 2000/2010

Link to search:
https://laji.fi/en/observation/map?target=Aves&time=2000%2F2010

-------------------------------------------

Terms of Use
------------

To get this data you have accepted the terms and conditions for the data.
The terms and conditions are available for review also from the page where you 
downloaded these files. 

By accepting the terms of use, you are committed to:

a. take special care to avoid endangering the species as described in the 
Publicity Act and / or taking into account other reasons that have caused the 
restriction of use. 

b. use the information solely and exclusively for the purpose for which the 
data has been released. 

c. not to distribute the material to anyone outside.

d. when using the data, refer to the data according to the citation 
instructions and note that the exact location information of sensitive species 
may not be published. 

-------------------------------------------

Citation
--------

Finnish Biodiversity Information Facility/FinBIF. http://tun.fi/FOOBAR, 
http://tun.fi/HR.48 (accessed 2015-11-10). 

If you use only some information sources included to the download, it is 
recommended to cite only those sources. 

Datasets
--------

List of datasets included in the search:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Attribution
More information about using this information source can be requested from 
jari.valkama@helsinki.fi 


Instructions
------------

Import data to GIS (in Finnish): https://laji.fi/about/5138
Import files into Excel: https://laji.fi/about/1064

===========================================

Identifierare för denna nedladdning: http://tun.fi/REQ-ID

Den här nedladdningen är gjord 10.11.2015 med följande filter:
Föremål (taxon): Aves
Tid: 2000/2010

Länk till sökning:
https://laji.fi/sv/observation/map?target=Aves&time=2000%2F2010

-------------------------------------------

Användarvillkor
---------------

Du har accepterat användarvillkoren för att få denna data. Användarvillkoren 
finns tillgängliga för granskning också från den sidan där du hämtade filerna. 

Användaren av sensitiva eller på annat sätt begränsade data förbinder åtar sig

a. att vara särskilt försiktig för att undvika att äventyra arten som beskrivs 
i publicitetslagen och/eller med hänsyn till andra skäl som har orsakat 
användningsbegränsningen. 

b. att använda informationen enbart och uteslutande för det ändamål för vilket 
informationen har släppts. 

c. att inte distribuera materialet till någon utomstående.

d. att hänvisa till data enligt instruktioner för hänvisning vid användning av 
data och notera att exakt lokaliseringsinformation för sensitiva arter inte kan 
publiceras. 

-------------------------------------------

Citat
-----

Finlands Artdatacenter/FinBIF. http://tun.fi/FOOBAR, http://tun.fi/HR.48 
(nerladdat 10.11.2015). 

Om du använder endast en del av informationskällor som ingår till nedladdningen 
rekommenderas det att bara de källor citeras. 

Dataset
-------

Lista över dataseten som ingår i sökningen:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Erkännande
Mer information om den här informationskälla kan beställas från 
jari.valkama@helsinki.fi 


Instruktioner
-------------

Importera en fil till GIS (på finska): https://laji.fi/about/5138
Lär dig att importera en fil till Excel (på engelska): 
https://laji.fi/about/1064