package fi.laji.datawarehouse.query.download;

import java.io.File;
import java.net.URL;
import java.text.ParseException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.TestDAO;
import fi.laji.datawarehouse.etl.models.TestDAO.Mode;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.model.DownloadRequestEmailGenerator;
import fi.laji.datawarehouse.query.download.model.DownloadRequestReadmeGenerator;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.CoordinatesWithOverlapRatio;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.Partition;
import fi.laji.datawarehouse.query.model.PolygonIdSearch;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class ReadmeGeneratorTests {

	private static TestDAO dao;
	private static LocalizedTextsContainer texts;

	@BeforeClass
	public static void init() throws Exception {
		dao = new TestDAO(Mode.REAL_COLLECTIONS, ReadmeGeneratorTests.class);
		texts = new LanguageFileReader("locales", "locale.", "FI","SV","EN").readUITexts();
	}

	@AfterClass
	public static void close() {
		if (dao != null) dao.close();
	}

	@Test
	public void generateFiltersText() throws Exception {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.SAMPLE, false);

		filters.setSampleType(new Qname("MF.preparationTypeTissueEcotoxicology"));
		filters.setSampleType(new Qname("MF.preparationTypeDNAExtract"));

		filters.setAdministrativeStatusId(new Qname("MX.habitatsDirectiveAnnexII"));
		filters.setAdministrativeStatusId(new Qname("MX.gameBird"));
		filters.setAdministrativeStatusId(new Qname("MX.FOOBAR"));

		filters.setArea("paikkatrue true");
		filters.setArea("paikka false");
		filters.setArea("paikka");

		filters.setBiogeographicalProvinceId(new Qname("ML.251"));

		filters.setCollectionId(new Qname("HR.209"));
		filters.setCollectionId(new Qname("HR.48"));
		filters.setCollectionId(new Qname("HR.FOOBAR"));

		filters.setCoordinates(new CoordinatesWithOverlapRatio(6666, 3333, Type.YKJ));
		filters.setCoordinates(new CoordinatesWithOverlapRatio(-23.23234234234, 63.1232342342424, Type.WGS84).setOverlapRatio(0.51234567890123456789));

		filters.setWgs84CenterPoint(new CoordinatesWithOverlapRatio(60.21312312312, 61.222, 22.0, 23.444444444444444, Type.WGS84));

		filters.setPolygonId(new PolygonIdSearch(1));

		filters.setCountryId(new Qname("ML.206"));
		filters.setCountryId(new Qname("ML.FOOBAR"));

		filters.setDayOfYear("5/9");

		filters.setDocumentId(new Qname("JX.123H"));
		filters.setDocumentId(new Qname("JX.123K"));

		filters.setEditorId(new Qname("MA.5"));
		filters.setEditorId(new Qname("MA.1"));
		filters.setEditorId(new Qname("MA.FOOBAR"));
		filters.setEditorOrObserverId(new Qname("MA.1"));
		filters.setTeamMember("Jouko");
		filters.setTeamMemberId(123L);

		filters.setFinnish(false);
		filters.setHasDocumentMedia(true);
		filters.setIncludeNonValidTaxa(true);

		filters.setInformalTaxonGroupId(new Qname("MVL.200"));
		filters.setInformalTaxonGroupId(new Qname("MVL.FOOBAR"));

		filters.setLoadedSameOrBefore(DateUtils.convertToDate("1.1.2000"));

		filters.setSex(Sex.MALE);
		filters.setSex(Sex.WORKER);

		filters.setTaxonId(new Qname("MX.123")); // not in test taxon dao
		filters.setTaxonId(new Qname("MX.26530")); // is in test taxon dao

		filters.setTarget("susi");
		filters.setTarget("MX.26530");
		filters.setTarget(new Qname("MX.26523").toURI());
		filters.setTarget("MX.123");

		filters.setTaxonRankId(new Qname("MX.genus"));
		filters.setTaxonRankId(new Qname("MX.FOOBAR"));
		filters.setHigherTaxon(true);

		filters.setTime("2000/");

		filters.setTaxonCensus(new Qname("MX.37580"));

		filters.setHasUnitAudio(true);

		filters.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.AMATEUR).setRecordQuality(RecordQuality.NEUTRAL).setRecordQuality(RecordQuality.UNCERTAIN));

		filters.setPartition(new Partition("1/5"));

		filters.setNamedPlaceTag(new Qname("MNP.tagHabitatFarmland"));
		filters.setNamedPlaceTag(new Qname("MNP.tagCensusRare"));

		filters.setTaxonSetId(new Qname("MX.taxonSetBiomonCompleteListOdonata"));

		String text = new DownloadRequestReadmeGenerator(dao, texts).generateFiltersText(filters, "fi", Concealment.PUBLIC);

		String expected = "" +
				"Taksoni: http://tun.fi/MX.123, Haliaeetus albicilla (MX.26530)\r\n" +
				"Kohde (laji): susi, Haliaeetus albicilla (MX.26530), Haliaeetus (MX.26523), http://tun.fi/MX.123\r\n" +
				"Lajiryhmä: Turskat, removed group MVL.FOOBAR\r\n" +
				"Lajin hallinnollinen rajaus: EU:n luontodirektiivin II-liite, Riistalintu (Metsästyslaki 1993/615), http://tun.fi/MX.FOOBAR\r\n" +
				"Laji on suomalainen: Ei\r\n" +
				"Taksonominen taso: suku, http://tun.fi/MX.FOOBAR\r\n" +
				"Liitetty ylemmän tason taksoniin: Kyllä\r\n" +
				"Maa: Suomi, http://tun.fi/ML.FOOBAR\r\n" +
				"Eliömaakunta: Ahvenanmaa (A)\r\n" +
				"Alueen nimi: paikkatrue true, paikka false, paikka\r\n" +
				"Nimettyn paikan tagi: Pelto, Harvoin laskettu\r\n" +
				"Aika: 2000/\r\n" +
				"Päivän järjestysnumero: 5/9\r\n" +
				"Aineisto: Luomus - Suomen suurperhosatlas, Luomus - Rengastus- ja löytörekisteri (TIPU), http://tun.fi/HR.FOOBAR\r\n" +
				"Sukupuoli: koiras, työläinen\r\n" +
				"Dokumentin tunniste: JX.123H, JX.123K\r\n" +
				"Lataus tietovarastoon, päivänä tai ennen: 2000-01-01\r\n" +
				"Koordinaatit: 6666.0 - 6667.0 N 3333.0 - 3334.0 E YKJ; -23.232342 - -23.232342 N 63.123234 - 63.123234 E WGS84 vaadittu päällekkäisyyssuhde 0.51\r\n" +
				"Polygonrajaus: POLYGON ((556320 7167999, 557885 7169572, 557598 7166987, 556320 7167999)) (id 1)\r\n" +
				"Koordinaatit (keskipiste): 60.213123 - 61.222 N 22.0 - 23.444444 E WGS84\n" +
				"Kuvia dokumentista: Kyllä\r\n" +
				"Äänite: Kyllä\r\n" +
				"Omistaja: Rajaus henkilöllä, Rajaus henkilöllä, Rajaus henkilöllä\r\n" +
				"Havaitsija tai omistaja: Rajaus henkilöllä\r\n" +
				"Havaitsijan nimi: Rajaus henkilöllä\r\n" +
				"Havaitsijan nimi: Rajaus henkilöllä\r\n" +
				"Aineiston ja havainnon laatuluokitus: Kansalaishavaintoja / ei laadunvarmistusta: Ei arvioitu, Epävarma\r\n" +
				"Täydellinen lajiluettelo: Biomon: Suomen sudenkorennot\r\n" +
				"Kattavasti kirjattu taksoni: Aves (MX.37580)\r\n" +
				"Preparaatin/näytteen tyyppi: Ympäristönäyte, DNA-eristys\r\n" +
				"Ositus/partition: 1/5\r\n\r\n" +
				"Linkki hakuun:\r\nhttps://laji.fi/fi/observation/map?taxonId=MX.123%2CMX.26530&target=susi%2CMX.26530%2Chttp%3A%2F%2Ftun.fi%2FMX.26523%2CMX.123&informalTaxonGroupId=MVL.200%2CMVL.FOOBAR&administrativeStatusId=MX.habitatsDirectiveAnnexII%2CMX.gameBird%2CMX.FOOBAR&finnish=false&taxonRankId=MX.genus%2CMX.FOOBAR&higherTaxon=true&countryId=ML.206%2CML.FOOBAR&biogeographicalProvinceId=ML.251&area=paikkatrue+true%2Cpaikka+false%2Cpaikka&namedPlaceTag=MNP.tagHabitatFarmland%2CMNP.tagCensusRare&time=2000%2F&dayOfYear=5%2F9&collectionId=HR.209%2CHR.48%2CHR.FOOBAR&sex=MALE%2CWORKER&documentId=JX.123H%2CJX.123K&loadedSameOrBefore=2000-01-01&coordinates=6666.0%3A6667.0%3A3333.0%3A3334.0%3AYKJ%3Anull%2C-23.232342%3A-23.232342%3A63.123234%3A63.123234%3AWGS84%3A0.51&polygonId=1&wgs84CenterPoint=60.213123%3A61.222%3A22.0%3A23.444444%3AWGS84%3Anull&hasDocumentMedia=true&hasUnitAudio=true&editorId=MA.5%2CMA.1%2CMA.FOOBAR&editorOrObserverId=MA.1&teamMember=Jouko&teamMemberId=123&collectionAndRecordQuality=AMATEUR%3ANEUTRAL%2CUNCERTAIN&taxonSetId=MX.taxonSetBiomonCompleteListOdonata&taxonCensus=MX.37580&sampleType=MF.preparationTypeTissueEcotoxicology%2CMF.preparationTypeDNAExtract&partition=1%2F5";
		assertEquals(expected, text);
	}

	@Test
	public void test_readme() throws Exception {
		DownloadRequest request = buildTestRequest(Concealment.PUBLIC, DownloadType.CITABLE);
		String readmeContents = new DownloadRequestReadmeGenerator(dao, texts).generateReadme(request);
		String expected = getFileContents("expected_readme.txt");
		assertEquals(expected.trim(), readmeContents.trim());
	}

	@Test
	public void test_pyha_readme() throws Exception {
		DownloadRequest request = buildTestRequest(Concealment.PRIVATE, DownloadType.APPROVED_DATA_REQUEST);
		String readmeContents = new DownloadRequestReadmeGenerator(dao, texts).generatePrivateReadme(request);
		String expected = getFileContents("expected_private_readme.txt");
		assertEquals(expected, Utils.removeWhitespaceAround(readmeContents));
	}

	@Test
	public void test_authorities_readme() throws Exception {
		DownloadRequest request = buildTestRequest(Concealment.PRIVATE, DownloadType.AUTHORITIES_FULL);
		String readmeContents = new DownloadRequestReadmeGenerator(dao, texts).generatePrivateReadme(request);
		String expected = getFileContents("expected_private_authorities_readme.txt");
		assertEquals(expected, Utils.removeWhitespaceAround(readmeContents));
	}

	private DownloadRequest buildTestRequest(Concealment concealment, DownloadType downloadType) throws ParseException {
		BaseQuery baseQuery = new BaseQueryBuilder(concealment).setApiSourceId("userid").setCaller(this.getClass()).setPersonEmail("email@email.com").setPersonId(new Qname("personId")).build();
		baseQuery.getFilters().setTarget("Aves").setTime("2000/2010");
		DownloadRequest request = new DownloadRequest(new Qname("REQ-ID"), DateUtils.convertToDate("10.11.2015"), baseQuery, downloadType, DownloadFormat.CSV_FLAT, Utils.set(DownloadInclude.UNIT_FACTS));
		request.setCollectionIds(Utils.set(new Qname("HR.48"), new Qname("FOOBAR")));
		return request;
	}

	public static String getFileContents(String filename) {
		URL url = ReadmeGeneratorTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return Utils.removeWhitespaceAround(data);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void test_email() throws Exception {
		DownloadRequest request = buildTestRequest(Concealment.PUBLIC, DownloadType.CITABLE);

		String downloadLink = request.getId().toURI();

		DownloadRequestEmailGenerator generator = new DownloadRequestEmailGenerator(new DownloadRequestReadmeGenerator(dao, texts), texts, downloadLink);
		request.setLocale("fi");
		String emailContentsFi = generator.buildEmailContent(request);
		request.setLocale("sv");
		String emailContentsSv = generator.buildEmailContent(request);
		request.setLocale("en");
		String emailContentsEn = generator.buildEmailContent(request);

		String expectedFi = getFileContents("expected_email_fi.txt");
		String expectedSv = getFileContents("expected_email_sv.txt");
		String expectedEn = getFileContents("expected_email_en.txt");

		assertEquals(expectedFi, emailContentsFi);
		assertEquals(expectedSv, emailContentsSv);
		assertEquals(expectedEn, emailContentsEn);
	}

	private void assertEquals(String expected, String actual) {
		Assert.assertEquals(expected.replace("\r", ""), actual.replace("\r", ""));
	}

}
