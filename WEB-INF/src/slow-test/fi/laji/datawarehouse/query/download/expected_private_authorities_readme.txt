Tämän latauksen tunniste: http://tun.fi/REQ-ID

Lataus on tehty 10.11.2015 seuraavilla rajauksilla:
Kohde (laji): Aves
Aika: 2000/2010

Linkki hakuun:
https://viranomaiset.laji.fi/fi/observation/map?target=Aves&time=2000%2F2010

Käyttöehdot
-----------

Tämä lataus on tuotettu viranomaisportaalissa. Päästäksesi 
viranomaisportaaliin, olet hyväksynyt aineistojen käsittelyä määrittävät 
säännöt. Et saa jakaa tiedostoja eteenpäin henkilöille pl säännöissä mainittu 
yhteiskäyttö. Muut henkilöt voivat tarvittaessa tehdä vastaavan latauksen tai 
aineistopynnön. 

Viittausohje
------------

Suomen Lajitietokeskus/FinBIF. http://tun.fi/FOOBAR, http://tun.fi/HR.48 
(haettu 10.11.2015). 

Jos käytät vain osaa aineistoista, on suositeltavaa, että viittaat vain niihin 
aineistoihin. 

Aineistot
---------

Luettelo aineistoista, jotka sisältyvät hakuun:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Rengastus- ja löytörekisteri (TIPU) - http://tun.fi/HR.48
Creative Commons Nimeä
Lisätietoja tämän aineiston käytöstä antaa jari.valkama@helsinki.fi


Ohjeet
------

Havaintoaineistojen käyttö paikkatietona: https://laji.fi/about/5138
Tiedoston vienti Exceliin: https://laji.fi/about/1068

===========================================

Identifier of this download: http://tun.fi/REQ-ID

This download is made on 2015-11-10 using the following filters:
Target (taxon): Aves
Time: 2000/2010

Link to search:
https://viranomaiset.laji.fi/en/observation/map?target=Aves&time=2000%2F2010

Terms of Use
------------

This download has been made in the Authorities' portal. To get access to the 
portal, you have accepted the terms and conditions. You may not share these 
files to persons not mentioned by the terms. Other persons may produce the same 
file in the portal themselves or make a data request. 

Citation
--------

Finnish Biodiversity Information Facility/FinBIF. http://tun.fi/FOOBAR, 
http://tun.fi/HR.48 (accessed 2015-11-10). 

If you use only some information sources included to the download, it is 
recommended to cite only those sources. 

Datasets
--------

List of datasets included in the search:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Attribution
More information about using this information source can be requested from 
jari.valkama@helsinki.fi 


Instructions
------------

Import data to GIS (in Finnish): https://laji.fi/about/5138
Import files into Excel: https://laji.fi/about/1064

===========================================

Identifierare för denna nedladdning: http://tun.fi/REQ-ID

Den här nedladdningen är gjord 10.11.2015 med följande filter:
Föremål (taxon): Aves
Tid: 2000/2010

Länk till sökning:
https://viranomaiset.laji.fi/sv/observation/map?target=Aves&time=2000%2F2010

Användarvillkor
---------------

Den här nedladdningen har gjorts i myndigheternas portal. För att få tillgång 
till portalen har du accepterat villkoren. Du får inte dela dessa filer till 
personer som inte nämns i villkoren. Andra personer kan producera samma fil i 
portalen själva eller göra en materialförfrågan. 

Citat
-----

Finlands Artdatacenter/FinBIF. http://tun.fi/FOOBAR, http://tun.fi/HR.48 
(nerladdat 10.11.2015). 

Om du använder endast en del av informationskällor som ingår till nedladdningen 
rekommenderas det att bara de källor citeras. 

Dataset
-------

Lista över dataseten som ingår i sökningen:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Erkännande
Mer information om den här informationskälla kan beställas från 
jari.valkama@helsinki.fi 


Instruktioner
-------------

Importera en fil till GIS (på finska): https://laji.fi/about/5138
Lär dig att importera en fil till Excel (på engelska): 
https://laji.fi/about/1064