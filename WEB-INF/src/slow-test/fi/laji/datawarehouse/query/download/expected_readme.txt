Tämän latauksen tietosivu ja viittausohjeet: http://tun.fi/REQ-ID

Lataus on tehty 10.11.2015 seuraavilla rajauksilla:
Kohde (laji): Aves
Aika: 2000/2010

Linkki hakuun:
https://laji.fi/fi/observation/map?target=Aves&time=2000%2F2010

Aineistot
---------

Luettelo aineistoista, jotka sisältyvät hakuun:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Rengastus- ja löytörekisteri (TIPU) - http://tun.fi/HR.48
Creative Commons Nimeä
Lisätietoja tämän aineiston käytöstä antaa jari.valkama@helsinki.fi


Ohjeet
------

Havaintoaineistojen käyttö paikkatietona: https://laji.fi/about/5138
Tiedoston vienti Exceliin: https://laji.fi/about/1068

===========================================

Information about this download and citation instructions: http://tun.fi/REQ-ID

This download is made on 2015-11-10 using the following filters:
Target (taxon): Aves
Time: 2000/2010

Link to search:
https://laji.fi/en/observation/map?target=Aves&time=2000%2F2010

Datasets
--------

List of datasets included in the search:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Attribution
More information about using this information source can be requested from 
jari.valkama@helsinki.fi 


Instructions
------------

Import data to GIS (in Finnish): https://laji.fi/about/5138
Import files into Excel: https://laji.fi/about/1064

===========================================

Information om den här nedladdningen och citat instruktioner: 
http://tun.fi/REQ-ID 

Den här nedladdningen är gjord 10.11.2015 med följande filter:
Föremål (taxon): Aves
Tid: 2000/2010

Länk till sökning:
https://laji.fi/sv/observation/map?target=Aves&time=2000%2F2010

Dataset
-------

Lista över dataseten som ingår i sökningen:

ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING! - http://tun.fi/FOOBAR

Luomus - Ringing and recovery database of birds (TIPU) - http://tun.fi/HR.48
Creative Commons Erkännande
Mer information om den här informationskälla kan beställas från 
jari.valkama@helsinki.fi 


Instruktioner
-------------

Importera en fil till GIS (på finska): https://laji.fi/about/5138
Lär dig att importera en fil till Excel (på engelska): 
https://laji.fi/about/1064