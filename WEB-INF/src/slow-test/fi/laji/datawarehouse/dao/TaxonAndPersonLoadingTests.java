package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.TestDAO;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;

public class TaxonAndPersonLoadingTests {

	private static TestDAO dao;

	@BeforeClass
	public static void init() {
		dao = new TestDAO(TaxonAndPersonLoadingTests.class);
	}

	@AfterClass
	public static void close() {
		if (dao != null) dao.close();
	}

	@Test
	public void loading_taxon_from_file() {
		Taxon t = dao.getTaxon(new Qname("MX.1"));
		assertEquals("Odonata", t.getScientificName());
		assertEquals("trollsländor", t.getVernacularName().forLocale("sv"));

		t = dao.getTaxon(new Qname("MX.272980"));
		assertEquals("Ctenophora guttata", t.getScientificName());
		assertEquals("niinijalokirsikäs", t.getVernacularName().forLocale("fi"));
		assertEquals("MKV.135432 : MKV.habitatMl : [MKV.habitatSpecificTypeV]", t.getPrimaryHabitat().toString());
		assertEquals("[MKV.135433 : MKV.habitatMk : [MKV.habitatSpecificTypeV], MKV.135434 : MKV.habitatI : [MKV.habitatSpecificTypeJ]]", t.getSecondaryHabitats().toString());

		t = dao.getTaxon(new Qname("MX.59808"));
		assertEquals("Gnorimoschema epithymella", t.getScientificName());
		assertEquals("MKV.383407 : MKV.habitatMkk : [MKV.habitatSpecificTypeH, MKV.habitatSpecificTypeP, MKV.habitatSpecificTypePAK]", t.getPrimaryHabitat().toString());

	}

	@Test
	public void taxon_loading_force_refresh() {
		dao.clearCachesStartReload();

		Taxon t = dao.getTaxon(new Qname("MX.1"));
		assertEquals("Odonata", t.getScientificName());
		assertEquals("trollsländor", t.getVernacularName().forLocale("sv"));

		t = dao.getTaxon(new Qname("MX.272980"));
		assertEquals("Ctenophora guttata", t.getScientificName());
		assertEquals("niinijalokirsikäs", t.getVernacularName().forLocale("fi"));
		assertEquals("MKV.135432 : MKV.habitatMl : [MKV.habitatSpecificTypeV]", t.getPrimaryHabitat().toString());
		assertEquals("[MKV.135433 : MKV.habitatMk : [MKV.habitatSpecificTypeV], MKV.135434 : MKV.habitatI : [MKV.habitatSpecificTypeJ]]", t.getSecondaryHabitats().toString());

		t = dao.getTaxon(new Qname("MX.59808"));
		assertEquals("Gnorimoschema epithymella", t.getScientificName());
		assertEquals("MKV.383407 : MKV.habitatMkk : [MKV.habitatSpecificTypeH, MKV.habitatSpecificTypeP, MKV.habitatSpecificTypePAK]", t.getPrimaryHabitat().toString());
	}

	@Test
	public void loading_person() {
		PersonInfo p = dao.getPerson(new Qname("MA.5"));
		assertEquals("Esko Piirainen", p.getFullName());
	}

	@Test
	public void loading_unknown_person() {
		Qname id = new Qname("MA.9393939393939393");
		PersonInfo p = dao.getPerson(id);
		assertEquals("Unknown person (" + id + ")", p.getFullName());
	}

	@Test
	public void person_loading_force_refresh() {
		dao.clearCachesStartReload();
		assertTrue(dao.getPersons().size() >= 10);
	}

}
