package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.StatelessSession;
import org.hibernate.criterion.Projections;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.oracle.QueryLogEntity;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader.Occurrence;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ConvertedCoordinates;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class DAOImpleTests {

	private static final Qname SATAKUNTA_PROVINCE = new Qname("ML.1229");
	private static final Qname VARSINAISSUOMEN_ELY = new Qname("ML.1247");

	public static class AlmostRealDao extends DAOImple {

		public AlmostRealDao(Config config, Class<?> createdBy) {
			super(config, "FOO", new ErrorReportingToSystemErr(), new ThreadStatuses(), createdBy);
		}

		@Override
		protected QueryLogStorer initQueryLogger() {
			return null;
		}

		@Override
		protected void log(Qname source, Class<?> phase, String type, String identifier, String message) {
			System.out.println(Utils.debugS("LOGGING", source, phase.getSimpleName(), type, identifier, message));
		}

		public void logForReal(Qname source, Class<?> phase, String type, String identifier, String message) {
			super.log(source, phase, type, identifier, message);
		}

		@Override
		public VerticaDimensionsDAO getVerticaDimensionsDAO() {
			return new VerticaDimensionsDAO() {

				@Override
				public void updateNameableEntityNames() {
					// Auto-generated method stub

				}

				@Override
				public void updateEntities(List<BaseEntity> entityList) {
					// Auto-generated method stub

				}

				@Override
				public void startTaxonAndPersonReprosessing() {
					// Auto-generated method stub

				}

				@Override
				public void performCleanUp() {
					// Auto-generated method stub

				}

				@Override
				public void insertEntities(List<BaseEntity> entityList) {
					// Auto-generated method stub

				}

				@Override
				public VerticaDimensionsTaxonService getTaxonService() {
					return new VerticaDimensionsTaxonService() {

						@Override
						public void switchTaxonTempToActual() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void insertToTaxonTempTable(Taxon taxon) throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void generateOccurrenceTypeLinkings() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void generateInformalGroupLinkings() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void generateHabitatLinkings() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void generateAdminStatusLinkings() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void emptyTaxonTempTable() throws Exception {
							// Auto-generated method stub

						}

						@Override
						public void close() {
							// Auto-generated method stub

						}

						@Override
						public void generateTaxonSetLinkings() throws Exception {
							// Auto-generated method stub

						}
					};
				}

				@Override
				public VerticaDimensionsPersonService getPersonSevice() {
					// Auto-generated method stub
					return null;
				}

				@Override
				public String getIdForKey(Long value, String field) {
					// Auto-generated method stub
					return null;
				}

				@Override
				public Long getEnumKey(Enum<?> enumeration) {
					// Auto-generated method stub
					return null;
				}

				@Override
				public List<NameableEntity> getEntities(Class<? extends NameableEntity> nameableEntityClass) {
					// Auto-generated method stub
					return null;
				}

				@Override
				public void storeDocumentIds(Iterator<String> iterator) {
					// Auto-generated method stub

				}

				@Override
				public void storeSplittedDocumentIds(Collection<SplittedDocumentIdEntity> values) {
					// Auto-generated method stub

				}
			};
		}
	}

	private static AlmostRealDao dao;
	private static Config config = TestConfig.getConfig();

	@BeforeClass
	public static void init() {
		dao = new AlmostRealDao(config, DAOImpleTests.class);
	}

	@AfterClass
	public static void close() {
		if (dao != null) dao.close();
	}

	@Test
	public void coordinates_by_municipality() {
		assertEquals(1, dao.resolveMunicipalitiesByName("Inkoo").size());
		Qname id = dao.resolveMunicipalitiesByName("Inkoo").get(0).getQname();
		Coordinates coords = dao.getCoordinatesByFinnishMunicipality(id);
		assertEquals(Type.WGS84, coords.getType());
		assertEquals(59.751691, coords.getLatMin(), 0.0001);
		assertEquals(23.752646, coords.getLonMin(), 0.0001);
		assertEquals(60.15011, coords.getLatMax(), 0.0001);
		assertEquals(24.316274, coords.getLonMax(), 0.0001);
	}

	@Test
	public void coordinates_by_non_existing_municipality() {
		Coordinates coords = dao.getCoordinatesByFinnishMunicipality(new Qname("ML.foo1234"));
		assertEquals(null, coords);
	}

	@Test
	public void municipalities_by_coords() throws Exception {
		Set<Qname> municipalities = dao.getFinnishMunicipalities(new Coordinates(60.0, 60.3, 25.0, 25.5, Type.WGS84));
		Qname vantaa = dao.resolveMunicipalitiesByName("Vantaa").get(0).getQname();
		assertEquals("ML.648", vantaa.toString());
		Qname sipoo =  dao.resolveMunicipalitiesByName("Sipoo").get(0).getQname();
		Qname helsinki =  dao.resolveMunicipalitiesByName("Helsinki").get(0).getQname();
		Qname porvoo =  dao.resolveMunicipalitiesByName("poRvOO").get(0).getQname();
		assertTrue(municipalities.contains(vantaa));
		assertTrue(municipalities.contains(sipoo));
		assertTrue(municipalities.contains(helsinki));
		assertTrue(municipalities.contains(porvoo));
		assertEquals(4, municipalities.size());
	}

	@Test
	public void biogeogprov_by_coords() throws Exception {
		Set<Qname> provinces = dao.getBiogeographicalProvinces(new Coordinates(60.0, 60.3, 25.0, 25.5, Type.WGS84));
		assertEquals(1, dao.resolveBiogeographicalProvincesByName("U").size());
		Qname uusimaa = dao.resolveBiogeographicalProvincesByName("U").get(0).getQname();
		assertEquals("ML.253", uusimaa.toString());
		assertEquals(1, provinces.size());
		assertEquals(uusimaa, provinces.iterator().next());
	}

	@Test
	public void natura2000AreabyCoords() throws Exception {
		Set<Qname> areas = dao.getNaturaAreas(new Coordinates(61.185706147440555, 26.894072905806993, Type.WGS84));
		assertEquals("[syke:FI0424001]", areas.toString());
	}

	@Test
	public void municipalities_by_geo() throws Exception {
		String data = "" +
				"{ \"type\": \"FeatureCollection\", \"crs\": \"WGS84\", \"features\": [ " +
				"{ \"type\": \"Feature\", \"geometry\": " +
				"{ \"type\": \"Polygon\", \"coordinates\": [ [ [ 28.855214, 61.866113 ], [ 28.854755, 61.866161 ], [ 28.854503, 61.865998 ], [ 28.85452, 61.866267 ], [ 28.854741, 61.866285 ], [ 28.854755, 61.866161 ], [ 28.855214, 61.866113 ] ] ] } " +
				"} ] }";
		// data has point 28.854755, 61.866161 twice
		Geo geo = Geo.fromGeoJSON(new JSONObject(data));
		assertEquals("[ML.446]", dao.getFinnishMunicipalities(geo).toString());
	}

	@Test
	public void store_get_download_request() {
		BaseQuery baseQuery = new BaseQueryBuilder(Concealment.PUBLIC)
				.setPersonEmail("integrationtest@etl.laji.fi")
				.setPersonId(new Qname("MA.5"))
				.setApiSourceId("KE.123")
				.setCaller(DAOImple.class).build();
		baseQuery.getFilters()
		.setArea("sastamala").setTarget("susi").setTarget("haahee")
		.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.AMATEUR).setRecordQuality(RecordQuality.NEUTRAL).setRecordQuality(RecordQuality.UNCERTAIN))
		.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.HOBBYIST).setRecordQuality(RecordQuality.NEUTRAL))
		.setTeamMember("Meikäläinen, Matti")
		.setTeamMember("Maija M*");
		Qname id = dao.getSeqNextVal("HBF");
		DownloadRequest stored =
				new DownloadRequest(
						id, new Date(), baseQuery,
						DownloadType.CITABLE, DownloadFormat.CSV_FLAT, Utils.set(DownloadInclude.DOCUMENT_EDITORS, DownloadInclude.UNIT_MEDIA))
				.setLocale("fi")
				.setApproximateResultSize(21)
				.setCollectionIds(Utils.set(new Qname("HR.1")))
				.setCreatedFile("filename")
				.setCreatedFileSize(0.5)
				.setCompleted(true)
				.setFailed(false)
				.setCreated(new Date());

		dao.storeDownloadRequest(stored);

		DownloadRequest fetched = dao.getDownloadRequest(id);
		assertEquals(stored.toString(), fetched.toString());
	}

	@Test
	public void store_get_lightweight_download_request() {
		BaseQuery baseQuery = new BaseQueryBuilder(Concealment.PUBLIC)
				.setApiSourceId("KE.123").setCaller(DAOImple.class)
				.build();
		Qname id = dao.getSeqNextVal("HBF");
		DownloadRequest stored =
				new DownloadRequest(
						id,
						new Date(),
						baseQuery,
						DownloadType.LIGHTWEIGHT, DownloadFormat.ODS_FLAT, Utils.set())
				.setLocale("fi")
				.setApproximateResultSize(21)
				.setCollectionIds(Utils.set(new Qname("HR.1"), new Qname("HR.2")))
				.setCompleted(true)
				.setCreated(new Date());

		dao.storeDownloadRequest(stored);

		DownloadRequest fetched = dao.getDownloadRequest(id);
		assertEquals(stored.toString(), fetched.toString());
	}

	@Test
	public void authoritiesDownloadRequests() {
		List<DownloadRequest> requests = dao.getAuthoritiesDownloadRequests();
		System.out.println(requests);
		assertEquals(10, requests.size()); // in dev mode only 10 are fetched
		assertEquals("HBF.5147", requests.get(9).getId().toString()); // the first authorities request ever made is last in the batch
	}

	@Test
	public void personsAuthoritiesDownloadRequests() {
		List<DownloadRequest> requests = dao.getPersonsAuthoritiesDownloadRequests(new Qname("MA.131"));
		System.out.println(requests);
		assertEquals("HBF.5207", requests.get(requests.size()-1).getId().toString()); // the first authorities request made by the user is last in the batch
	}

	@Test
	public void coordinate_conversion() throws Exception {
		Coordinates coordinates1 = new Coordinates(65, 25.31666667, Type.WGS84);
		Utils.debug(coordinates1);

		Coordinates coordinates2 = new Coordinates(64.123, 26.123, Type.WGS84);
		Utils.debug(coordinates2);

		for (int i = 0; i < 1000; i++) {
			if (i % 2 == 0) {
				ConvertedCoordinates converted = CoordinateConverter.convert(coordinates1);
				assertEquals("7212529.0 : 7212529.0 : 3420767.0 : 3420767.0 : YKJ : null", converted.getYkj().toString());
			} else {
				ConvertedCoordinates converted = CoordinateConverter.convert(coordinates2);
				assertEquals("7113993.0 : 7113993.0 : 3457448.0 : 3457448.0 : YKJ : null", converted.getYkj().toString());

			}
		}
	}

	@Test
	public void personname() {
		assertEquals("Esko Piirainen", dao.getPerson((new Qname("MA.5"))).getFullName());
		assertEquals("Unknown person (MA.Foobarbarbar)", dao.getPerson((new Qname("MA.Foobarbarbar"))).getFullName());
	}

	@Test
	public void collection_childs() {
		CollectionMetadata luomusCollections = dao.getCollections().get(new Qname("HR.128").toURI());
		assertTrue(luomusCollections.getChildren().size() > 5);
		assertEquals("Luonnontieteellisen keskusmuseon Luomuksen kokoelmat (Luomus)", luomusCollections.getName().forLocale("fi"));

		CollectionMetadata child1 = null;
		for (CollectionMetadata child : luomusCollections.getChildren()) {
			if (child.getQname().toString().equals("HR.203")) {
				child1 = child;
				break;
			}
		}
		assertNotNull(child1);
		assertEquals("HR.203", child1.getQname().toString());
		assertEquals("Luomus - Löydös havaintopalvelu", child1.getName().forLocale("fi"));
		assertEquals(1, child1.getChildren().size());
	}

	@Test
	public void collection_licenses() {
		CollectionMetadata c = dao.getCollections().get(new Qname("HR.48").toURI());
		assertEquals("MY.intellectualRightsCC-BY", c.getIntellectualRights().toString());
		assertEquals("Creative Commons Nimeä", c.getIntellectualRightsDescription().forLocale("fi"));
	}

	@Test
	public void getProperty() {
		assertEquals("Kaikki oikeudet pidätetään", dao.getLabels(new Qname("MY.intellectualRightsARR")).forLocale("fi"));
	}

	@Test
	public void sendNotification() throws Exception {
		Annotation annotation = new Annotation(new Qname("MAN.123"), new Qname("luomus:JA.123"), new Qname("luomus:JA.123#1"), new Date().getTime()/1000);
		annotation.setIdentification(new Identification("susi", new Qname("MX.1")));
		annotation.setAnnotationByPerson(new Qname("MA.1"));
		annotation.addTag(Tag.EXPERT_TAG_VERIFIED);
		dao.sendNotification(new AnnotationNotification(new Qname("MA.5"), annotation, null));
		dao.sendNotification(new AnnotationNotification(new Qname("MA.5"), annotation, NotificationReason.ANNOTATED_DOCUMENT_ANNOTATED));
		dao.sendNotification(new AnnotationNotification(new Qname("MA.5"), annotation, NotificationReason.MY_DOCUMENT_ANNOTATED));
	}

	@Test
	public void queryLog() throws Exception {
		AggregatedQuery q = new AggregatedQuery(
				new BaseQueryBuilder(Concealment.PRIVATE)
				.setApiSourceId("apiuser")
				.setCaller(DAOImpleTests.class)
				.setPersonId(new Qname("MA.5"))
				.setPersonEmail("myemail@example.com")
				.build()
				.setPermissionId(new Qname("XXX.xxx")),
				new AggregateBy().addField("document.documentId").addField("gathering.gatheringId"),
				5,
				1000)
				.setOrderBy(new OrderBy(Base.UNIT).addAscending(Const.COUNT));
		q.getFilters().setAdministrativeStatusId(new Qname("somestatus"));

		dao.persistQueryLogToLogTable(); // deletes old temp files
		long count = getQueryCount(); // get current number of log entries in db

		// log 5 queries
		dao.logQuery(q);
		Thread.sleep(10);
		dao.logQuery(new ListQuery(q, 1, 10));
		Thread.sleep(10);
		dao.logQuery(new CountQuery(q));
		Thread.sleep(10);
		dao.logQuery(new AggregatedQuery(q, q.getAggregateBy(), 1, 10));
		Thread.sleep(10);
		dao.logQuery(q); // same id, but id not defined as primary key on purpose

		File tmpFolder = new File(config.baseFolder() + config.get("TempFolder"));
		assertEquals(0, queryLogTempFiles(tmpFolder).length);

		dao.tempStoreQueryLog(); // usually we don't call this - there is a timed task, but for tests the timed task is disabled and we want the results now

		assertEquals(1, queryLogTempFiles(tmpFolder).length);
		List<String> lines = FileUtils.readLines((queryLogTempFiles(tmpFolder)[0]));
		assertEquals(5, lines.size());

		JSONObject logContent = new JSONObject(lines.get(0));
		logContent.remove("id");
		assertEquals("" +
				"{\"timestamp\":"+(q.getTimestamp()/1000)+",\"warehouse\":\"PRIVATE\",\"base\":\"UNIT\",\"apiUser\":\"apiuser\",\"caller\":\"DAOImpleTests\",\"personId\":\"MA.5\",\"personEmail\":\"myemail@example.com\",\"queryString\":\"BaseQuery [filters=[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, administrativeStatusId:[somestatus], individualCountMin:1, wild:[WILD,WILD_UNKNOWN], qualityIssues:NO_ISSUES], permissionFilters=null, approvedDataRequest=false, warehouse=PRIVATE, base=UNIT, apiSourceId=apiuser, caller=class fi.laji.datawarehouse.dao.DAOImpleTests, userAgent=null, userId=MA.5, userEmail=myemail@example.com, permissionId=XXX.xxx, crs=null, featureType=null, useCache=false] PageableBaseQuery [pageSize=1000, currentPage=5] AggregatedQuery [aggregateBy=[document.documentId, gathering.gatheringId], orderBy=[count ASC], onlyCount=true, pairCounts=false, taxonCounts=false, atlasCounts=false, exludeNulls=true, pessimisticDateRangeHandling=false]\",\"permissionId\":\"XXX.xxx\"}",
				logContent.toString());

		long countAfter = getQueryCount();
		assertEquals(count, countAfter);

		dao.persistQueryLogToLogTable(); // this is usually called nightly
		countAfter = getQueryCount();

		assertEquals(0, queryLogTempFiles(tmpFolder).length);
		assertEquals(count+5, countAfter);
	}

	private File[] queryLogTempFiles(File tmpFolder) {
		return tmpFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("query_log");
			}
		});
	}

	private long getQueryCount() {
		try (StatelessSession ses = dao.getETLDAO().getETLEntityConnection()) {
			return (long) ses.createCriteria(QueryLogEntity.class).setProjection(Projections.rowCount()).uniqueResult();
		}
	}

	@Test
	public void getImage() {
		MediaObject m = dao.getMediaObject(new Qname("MM.88811"));
		assertEquals(
				"MediaObject [id=MM.88811, mediaType=IMAGE, fullURL=https://luomus.digitarium.fi/MM.88811/GL.2315_Arachnospila_opinata_f.jpg, thumbnailURL=https://luomus.digitarium.fi/MM.88811/GL.2315_Arachnospila_opinata_f_thumb.jpg, squareThumbnailURL=https://luomus.digitarium.fi/MM.88811/GL.2315_Arachnospila_opinata_f_square.jpg, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=Malinen, Pekka, copyrightOwner=Luomus, licenseId=MZ.intellectualRightsCC-BY-SA-4.0, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null]",
				m.toString());
	}

	@Test
	public void getAudio() {
		MediaObject m = dao.getMediaObject(new Qname("MM.97876"));
		assertEquals(
				"MediaObject [id=MM.97876, mediaType=AUDIO, fullURL=https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL_spectrogram.png, thumbnailURL=https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL_spectrogram_thumb.jpg, squareThumbnailURL=null, mp3URL=https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL.mp3, wavURL=https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL.wav, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=Esko Piirainen (Luomus), copyrightOwner=Esko Piirainen (Luomus), licenseId=MZ.intellectualRightsCC0-4.0, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null]",
				m.toString());
	}

	@Test
	public void getModel() {
		MediaObject m = dao.getMediaObject(new Qname("MM.106094"));
		assertEquals(
				"MediaObject [id=MM.106094, mediaType=MODEL, fullURL=https://imagetest.laji.fi/MM.106094/Capreolus_capreolus_xray_thumbnail.jpg, thumbnailURL=https://imagetest.laji.fi/MM.106094/Capreolus_capreolus_xray_thumbnail_thumb.jpg, squareThumbnailURL=https://imagetest.laji.fi/MM.106094/Capreolus_capreolus_xray_thumbnail_square.jpg, mp3URL=null, wavURL=null, videoURL=https://imagetest.laji.fi/MM.106094/Capreolus_capreolus_xray.mp4, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=me, licenseId=MZ.intellectualRightsARR, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=true]",
				m.toString());
	}

	@Test
	public void nonExistingMedia() {
		MediaObject o = dao.getMediaObject(new Qname("JA.FOOBARxxx112"));
		assertNull(o);
	}

	@Test
	public void existingMedia() {
		MediaObject o = dao.getMediaObject(new Qname("MM.7752"));
		assertEquals("https://imagetest.laji.fi/MM.7752/tiny_full.jpg", o.getFullURL());
	}

	@Test
	public void mediaByDocumentId() {
		List<MediaObject> media = dao.getMediaObjects(new Qname("luomus:foo/blah"));
		assertFalse(media.isEmpty());
		assertEquals("[aTag]", media.get(0).getKeywords().toString());
	}

	@Test
	public void contextDefinitions() throws Exception {
		ContextDefinitions contextDefinitionsFromSchema = dao.getContextDefinitionFromSchemaWithTimeOut();
		ContextDefinitions contextDefinitionsFromLocalFallback = dao.getLocalContextDefinition();

		test(contextDefinitionsFromSchema);
		test(contextDefinitionsFromLocalFallback);

	}

	private void test(ContextDefinitions contextDefinitions) {
		assertEquals("MY.leg", contextDefinitions.getContextDefinition("leg").getProperty().toString());
		assertEquals(false, contextDefinitions.getContextDefinition("leg").isResource());

		assertEquals("MY.sourceID", contextDefinitions.getContextDefinition("sourceID").getProperty().toString());
		assertEquals(true, contextDefinitions.getContextDefinition("sourceID").isResource());

		assertEquals("MZ.editors", contextDefinitions.getContextDefinition("editors").getProperty().toString());
		assertEquals(true, contextDefinitions.getContextDefinition("sourceID").isResource());
	}

	@Test
	public void invasivePersisted() {
		dao.setInvasiveSpeciesEarlyWarningReportedUnits(Collections.emptySet());

		Set<Qname> persistedIds = dao.getInvasiveSpeciesEarlyWarningReportedUnits();
		assertEquals(0, persistedIds.size());
		persistedIds.addAll(Utils.set(new Qname("JX.3"))); // make sure set is modifiable

		Set<Qname> ids = Utils.set(new Qname("luomus:JA.1"), new Qname("JX.1"), new Qname("JX.2"));
		dao.setInvasiveSpeciesEarlyWarningReportedUnits(ids);

		persistedIds = dao.getInvasiveSpeciesEarlyWarningReportedUnits();

		assertTrue(persistedIds.containsAll(ids));
		assertTrue(ids.containsAll(persistedIds));

		persistedIds.addAll(Utils.set(new Qname("JX.3"))); // make sure set is modifiable
	}

	@Test
	public void pestPersisted() {
		dao.setPestSpeciesEarlyWarningReportedUnits(Collections.emptySet());
		assertEquals(0, dao.getPestSpeciesEarlyWarningReportedUnits().size());

		Set<Qname> ids = Utils.set(new Qname("luomus:JA.1"), new Qname("JX.1"), new Qname("JX.2"));
		dao.setPestSpeciesEarlyWarningReportedUnits(ids);

		Set<Qname> persistedIds = dao.getPestSpeciesEarlyWarningReportedUnits();

		assertTrue(persistedIds.containsAll(ids));
		assertTrue(ids.containsAll(persistedIds));
	}

	@Test
	public void taxonSearch() {
		assertEquals(0, dao.getAllTaxonMatches("foo").size());
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("MX.26944").toString()); // qname
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("http://tun.fi/MX.26944").toString()); // uri
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("Oreortyx pictus").toString()); // scientific name
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("vuoriviiriäinen").toString()); // vernacular name, fi
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("Mountain Quail").toString()); // vernacular name, en
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("Oreortyx picta").toString()); // synonym scientific name
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("MX.316762").toString()); // synonym id
		assertEquals("[MX.26944]", dao.getAllTaxonMatches("vuoriviiriäinen (Oreortyx pictus)").toString()); // vernacular + sciname
		assertEquals("[MX.70300]", dao.getAllTaxonMatches("päiväpetolintu").toString()); // aka name
	}

	@Test
	public void getNamedPlaces() {
		Collection<NamedPlaceEntity> places = dao.getNamedPlaces().values();
		assertTrue(places.size() > 10);
		Set<String> collections = new HashSet<>();

		Map<String, Set<String>> birdAreaNames = new HashMap<>();
		Map<String, Set<String>> municipalityNames = new HashMap<>();

		for (NamedPlaceEntity e : places) {
			collections.add(e.getCollectionId());

			if (!birdAreaNames.containsKey(e.getBirdAssociationAreaId())) {
				birdAreaNames.put(e.getBirdAssociationAreaId(), new HashSet<String>());
			}
			birdAreaNames.get(e.getBirdAssociationAreaId()).add(e.getBirdAssociationAreaDisplayName());

			if (!municipalityNames.containsKey(e.getMunicipalityId())) {
				municipalityNames.put(e.getMunicipalityId(), new HashSet<String>());
			}
			municipalityNames.get(e.getMunicipalityId()).add(e.getMunicipalityDisplayName());

			if (e.getId().equals("http://tun.fi/MNP.22762")) {
				assertEquals("725.0:350.0:YKJ", e.getYkj10km().toString());
				assertEquals("POLYGON ((26.996311 65.345558, 26.996298 65.435245, 27.211783 65.435097, 27.211062 65.345411, 26.996311 65.345558))",
						e.getWgs84WKT());
				assertEquals("65.390328:27.10404:WGS84", e.getWgs84CenterPoint().toString());
			}

			if (e.getId().equals("http://tun.fi/MNP.47029")) {
				assertEquals(null, e.getYkj10km());
				assertEquals("GEOMETRYCOLLECTION(POINT (24.008093 60.209743), POINT (24.000048 60.199785), POINT (23.981324 60.198677), POINT (23.986871 60.202155), POINT (23.969254 60.187278), POINT (23.968142 60.188178), POINT (23.985306 60.179674), POINT (23.98966 60.179603), POINT (24.077179 60.19221), POINT (23.953192 60.190045), POINT (23.893546 60.167409), POINT (23.941104 60.172766), POINT (23.938772 60.178634), POINT (23.920841 60.184418), POINT (23.946156 60.199731), POINT (23.919535 60.20855), POINT (23.913492 60.218473), POINT (23.878178 60.224224))",
						e.getWgs84WKT());
				assertEquals("60.195816:23.977678:WGS84", e.getWgs84CenterPoint().toString());
			}

			if (e.getId().equals("http://tun.fi/MNP.47029")) {
				assertEquals("9001", e.getAlternativeId());
			}

			if (e.getId().equals("http://tun.fi/MNP.48288")) {
				assertEquals("[MNP.tagAccessibilityModerate, MNP.tagHabitatImportant, MNP.tagHabitatMire]", e.getTags().toString());
			}
		}
		Set<String> ids = places.stream().map(n->n.getId()).collect(Collectors.toSet());
		assertTrue(ids.contains("http://tun.fi/MNP.22762"));
		assertTrue(ids.contains("http://tun.fi/MNP.47029"));
		assertTrue(ids.contains("http://tun.fi/MNP.48288"));

		birdAreaNames.remove(null);
		municipalityNames.remove(null);

		assertTrue(collections.contains("http://tun.fi/HR.39"));
		assertTrue(collections.size() > 5);

		assertTrue(municipalityNames.size() > 200);
		assertEquals("[Riihimäki]", municipalityNames.get("http://tun.fi/ML.635").toString());

		assertEquals(28, birdAreaNames.size());
		assertEquals("[Suomenselän Lintutieteellinen Yhdistys ry]", birdAreaNames.get("http://tun.fi/ML.1105").toString());

		for (Map.Entry<String, Set<String>> e : municipalityNames.entrySet()) {
			assertEquals(1, e.getValue().size());
		}
		for (Map.Entry<String, Set<String>> e : birdAreaNames.entrySet()) {
			assertEquals(1, e.getValue().size());
		}
	}

	@Test
	public void apiusers() {
		ApiUser apiUser = dao.getApiUser("asdas");
		assertFalse(apiUser.isValid());
	}

	@Test
	public void gbifDatasets() {
		Collection<Pair<Qname, Collection<URI>>> endpoints = dao.getGBIFDatasetEndpoints();
		assertTrue(endpoints.size() > 15);
		Set<String> names = new HashSet<>();
		for (Pair<Qname, Collection<URI>> e : endpoints) {
			CollectionMetadata collectionMetadata = dao.getCollections().get(e.getKey().toURI());
			System.out.println(collectionMetadata.getName().forLocale("en") + " " + collectionMetadata.getIntellectualRights());
			names.add(collectionMetadata.getName().forLocale("en"));
		}
		assertTrue(names.contains("Chronicle of Nature - Phenology of Plants of Carpathian Biosphere Reserve"));
	}

	@Test
	public void downloadLimit() {
		Qname u1 = new Qname("u1");
		Qname u2 = new Qname("u2");
		for (int i=0; i<=19; i++) {
			assertFalse(dao.exceedsDownloadLimit(u1));
		}
		assertTrue(dao.exceedsDownloadLimit(u1));

		assertFalse(dao.exceedsDownloadLimit(u2));

		dao.clearPersonDailyLimits();

		assertTrue(dao.exceedsDownloadLimit(u1));
	}

	@Test
	public void occurrenceCounts() {
		dao.oracle.logOccurrenceCounts(Utils.randomNumber(1, 1000), Utils.randomNumber(1, 1000));
	}

	@Test
	public void keyValueStorage() {
		truncateTable("storage");
		assertNull(dao.getPersisted(Utils.generateGUID()));
		dao.persist("foo", "bar");
		assertEquals("bar", dao.getPersisted("foo"));
	}

	private void truncateTable(String table) {
		try (StatelessSession session = dao.getETLDAO().getETLEntityConnection()) {
			session.getTransaction().begin();
			session.createSQLQuery("TRUNCATE TABLE " + table).executeUpdate();
			session.getTransaction().commit();
		}
	}

	@Test
	public void logOne() {
		String message = Utils.generateGUID();
		dao.logForReal(Const.LAJI_ETL_QNAME, DAOImpleTests.class, "MESSAGE", "myid", message);
		assertEquals(
				"LogEntry [source=KE.398, phase=DAOImpleTests, type=MESSAGE, identifier=myid, message=XXX]".replace("XXX", message),
				dao.getLogEntries().iterator().next().toString());
	}

	@Test
	public void logPurging() {
		// add some old log entries
		truncateTable("log");
		LogEntry entry = new LogEntry(Const.LAJI_ETL_QNAME, DAOImpleTests.class, "MESSAGE", "logpurging", "old messages");
		long month = 1000L*60*60*24*30;
		for (int i = 0; i < 10; i++) {
			long time = System.currentTimeMillis() - month;
			entry.setTimestamp(time);
			dao.oracle.log(entry);
		}
		assertEquals(10, dao.getLogEntries().size());

		// add new log entries
		for (int i = 0; i < 200; i++) {
			dao.logForReal(Const.LAJI_ETL_QNAME, DAOImpleTests.class, "MESSAGE", "logpurging", "new messages");
		}
		assertEquals(200, dao.getLogEntries().size()); // not 210
	}

	@Test
	public void municipalityElyProvince() {
		List<Qname> elyMunicipalities = new ArrayList<>(dao.getMunicipalitiesPartOfELY(VARSINAISSUOMEN_ELY));
		List<Qname> provinceMunicipalities = new ArrayList<>(dao.getMunicipalitiesPartOfProvince(SATAKUNTA_PROVINCE));
		Collections.sort(elyMunicipalities);
		Collections.sort(provinceMunicipalities);
		assertEquals("[ML.373, ML.386, ML.387, ML.391, ML.393, ML.399, ML.400, ML.401, ML.402, ML.409, ML.429, ML.432, ML.435, ML.455, ML.456, ML.457, ML.458, ML.459, ML.460, ML.461, ML.462, ML.464, ML.465, ML.466, ML.467, ML.468, ML.469, ML.472, ML.473, ML.475, ML.478, ML.481, ML.486, ML.496, ML.510, ML.533, ML.547, ML.563, ML.641, ML.647, ML.649, ML.664, ML.665]",
				elyMunicipalities.toString());
		assertEquals("[ML.399, ML.400, ML.409, ML.429, ML.432, ML.435, ML.455, ML.459, ML.464, ML.465, ML.466, ML.467, ML.510, ML.563, ML.641, ML.649]",
				provinceMunicipalities.toString());
	}

	@Test
	public void apiKeyRequests() {
		truncateTable("apikey");
		Qname id = dao.getSeqNextVal("HBF");

		BaseQueryBuilder baseQueryBuilder = new BaseQueryBuilder(Concealment.PRIVATE)
				.setBase(Base.UNIT)
				.setCaller(this.getClass())
				.setApiSourceId("KE.123")
				.setUserAgent("User-Agent")
				.setPersonEmail("integrationtest@etl.laji.fi")
				.setPersonId(new Qname("MA.5"));
		BaseQuery baseQuery = baseQueryBuilder.build();
		baseQuery.getFilters().setCollectionId(new Qname("HR.1"));
		baseQuery.getFilters().setCollectionId(new Qname("HR.2"));
		baseQuery.getFilters().setIndividualCountMin(0);

		DownloadRequest request = new DownloadRequest(id, new Date(), baseQuery, DownloadType.AUTHORITIES_API_KEY, null, null)
				.setLocale("fi")
				.setDataUsePurpose("Purpose integration test")
				.setApiKeyExpires(new Date());

		request.setCompleted(true);
		request.setCollectionIds(Utils.set(new Qname("HR.1"), new Qname("HR.2")));
		request.setCreated(new Date());

		dao.storeDownloadRequest(request);

		String apiKey = dao.generateAndStoreApiKey(request);
		assertTrue(apiKey.startsWith("pa-"));
		assertEquals(64, apiKey.length());

		DownloadRequest viaApiKey = dao.getRequestForApiKey(apiKey);
		assertEquals(
				"[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, collectionId:[HR.1,HR.2], individualCountMin:0, wild:[WILD,WILD_UNKNOWN], qualityIssues:NO_ISSUES]",
				viaApiKey.getFilters().toString());
		assertEquals(viaApiKey.getId().toString(), id.toString());

		List<DownloadRequest> allRequests = dao.getApiRequests();
		List<Pair<String, DownloadRequest>> byUserId = dao.getPersonsApiRequests(new Qname("MA.5"));
		List<Pair<String, DownloadRequest>> byFoobarUserId = dao.getPersonsApiRequests(new Qname("MA.foo"));

		assertEquals(allRequests.size(), byUserId.size());
		assertEquals(0, byFoobarUserId.size());
	}

	@Test
	public void polygonSearch() {
		Polygon p = (Polygon) Polygon.from(new double[][] {{55.0, 70.0}, {55.1, 70.0}, {55.1, 70.1}, {55.0, 70.1}});

		String wkt = p.getWKT();

		long id = dao.getPolygonSearchId(wkt);
		long idAgain = dao.getPolygonSearchId(wkt);
		assertEquals(id, idAgain);

		assertEquals(wkt, dao.getPolygonSearch(id));
		assertNull(dao.getPolygonSearch(-1));
	}

	@Test
	public void getAllDocumentIdsAsStream() {
		try (ResultStream<String> s = dao.getETLDAO().getAllDocumentIds()) {

		}
	}

	@Test
	public void testLajigisOracle() throws SQLException {
		LajiGISPullReader.DwTempTablesDAOImple lajigisDao = new LajiGISPullReader.DwTempTablesDAOImple(dao.getETLDAO(), new ThreadStatusReporter());

		truncateTable("lajigis_etl_occurrences");

		Occurrence o = new Occurrence();
		o.setEventId(1);
		o.setOccurrenceId(1);
		o.setJson("foo");

		lajigisDao.insertOccurrences(Utils.list(o));

		try (ResultStream<Occurrence> stream = lajigisDao.getOccurrences()) {
			Iterator<Occurrence> i = stream.iterator();
			assertTrue(i.hasNext());
			assertEquals("foo", i.next().getJson());
			assertFalse(i.hasNext());
			assertFalse(i.hasNext());
			try {
				i.next();
				fail();
			} catch (IllegalArgumentException e) {
				assertEquals("Last result was already reached", e.getMessage());
			}
		}
	}

}