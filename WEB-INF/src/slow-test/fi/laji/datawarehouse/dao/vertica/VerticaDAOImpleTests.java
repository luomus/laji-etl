package fi.laji.datawarehouse.dao.vertica;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.models.harmonizers.LajistoreHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizerTests;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.Utils;

public class VerticaDAOImpleTests extends VerticaDAOTestBase {

	@Test
	public void loadingResourceNames() throws Exception {
		assertEquals(0, dimensions.getEntities(SourceEntity.class).size());
		storeTestDocs(1);
		assertEquals(1, dimensions.getEntities(SourceEntity.class).size());
		String testDocSource = new Qname("KE.3").toURI();
		assertEquals(null, dimensions.getEntity(testDocSource, SourceEntity.class).getName());
		dimensions.updateNameableEntityNames();
		assertEquals("Kotka", dimensions.getEntity(testDocSource, SourceEntity.class).getName());
		assertEquals("Helsingin Seudun Lintutieteellinen Yhdistys - Tringa ry", dimensions.getEntity(new Qname("ML.1091").toURI(), AreaEntity.class).getName());
	}

	@Test
	public void save_and_get_and_query_and_delete_and_check_is_deleted() throws Exception {
		Document doc = createTestDoc(0);
		verticaDao.save(Utils.list(doc), STATUS_REPORTER); // save

		Document returnedDoc = verticaDao.getQueryDAO().get(doc.getDocumentId()); // get

		JSONObject json = ModelToJson.toJson(returnedDoc);
		assertFalse(json.getBoolean("public"));

		// query
		CountQuery countQuery = new CountQuery(privateQuery().build());
		countQuery.getFilters().setDocumentId(new Qname("A.12345_0"));
		assertEquals(4, verticaDao.getQueryDAO().getCount(countQuery).getTotal());

		Document deleteDoc = Document.createDeletedDocument(new DwRoot(new Qname("A.12345_0"), new Qname("KE.3")));
		verticaDao.save(Utils.list(deleteDoc), STATUS_REPORTER); // delete
		assertEquals(0, verticaDao.getQueryDAO().getCount(countQuery).getTotal()); // check is deleted
	}

	@Test
	public void personLoader() throws Exception {
		Document doc = new Document(Concealment.PRIVATE, new Qname("KE.999"), new Qname("1"), new Qname("HR.1"));
		doc.addEditorUserId("http://tun.fi/MA.1");
		doc.addEditorUserId("MA.2");
		doc.addEditorUserId("Öäfoobuar");
		doc.addEditorUserId("hatikka:Esko.Piirainen@hatikka.fi");

		verticaDao.save(Utils.list(doc), STATUS_REPORTER);
		Document returnedDoc = verticaDao.getQueryDAO().get(doc.getDocumentId());

		assertEquals("[http://tun.fi/MA.1, http://tun.fi/MA.2, Öäfoobuar, hatikka:Esko.Piirainen@hatikka.fi]", returnedDoc.getEditorUserIds().toString());
		assertEquals(4, returnedDoc.getLinkings().getEditors().size());

		assertEquals("http://tun.fi/MA.1", returnedDoc.getLinkings().getEditors().get(0).getId().toURI());
		assertEquals("http://tun.fi/MA.1", returnedDoc.getLinkings().getEditors().get(0).getUserId());
		assertEquals("Dare Talvitie", returnedDoc.getLinkings().getEditors().get(0).getFullName());

		assertEquals("http://tun.fi/MA.2", returnedDoc.getLinkings().getEditors().get(1).getId().toURI());
		assertEquals("http://tun.fi/MA.2", returnedDoc.getLinkings().getEditors().get(1).getUserId());
		assertEquals("Tapani Lahti", returnedDoc.getLinkings().getEditors().get(1).getFullName());

		assertEquals(null, returnedDoc.getLinkings().getEditors().get(2).getId());
		assertEquals("Öäfoobuar", returnedDoc.getLinkings().getEditors().get(2).getUserId());
		assertEquals(null, returnedDoc.getLinkings().getEditors().get(2).getFullName());

		assertEquals("http://tun.fi/MA.5", returnedDoc.getLinkings().getEditors().get(3).getId().toURI());
		assertEquals("hatikka:Esko.Piirainen@hatikka.fi", returnedDoc.getLinkings().getEditors().get(3).getUserId());
		assertEquals("Esko Piirainen", returnedDoc.getLinkings().getEditors().get(3).getFullName());

		UserIdEntity userId1 = dimensions.getUserIds().get(0); // ordered alphabetically ascending using id
		assertEquals("hatikka:Esko.Piirainen@hatikka.fi", userId1.getId());
		assertEquals(5, userId1.getPersonKey().intValue());

		assertEquals(9699, dimensions.getPersonEntity(5L).getLintuvaaraId().intValue());
		assertEquals("esko.piirainen@helsinki.fi", dimensions.getPersonEntity(5L).getEmail());
	}

	@Test
	public void taxonLoader() throws Exception {
		Document doc = new Document(Concealment.PRIVATE, new Qname("KE.999"), new Qname("1"), new Qname("HR.1"));
		doc.addGathering(new Gathering(new Qname("gid")));

		// target 1
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u1")));
		doc.getGatherings().get(0).getUnits().get(0).setTaxonVerbatim("tunturihaukka"); // vernacular name -- links to taxon

		// target 2
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u2")));
		doc.getGatherings().get(0).getUnits().get(1).setTaxonVerbatim("Ötökkö"); // some name - will not link to taxon; especially because has reference
		doc.getGatherings().get(0).getUnits().get(1).setReferencePublication(new Qname("MP.1234"));

		// target 3
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u3")));
		doc.getGatherings().get(0).getUnits().get(2).setTaxonVerbatim("talitintti"); // aka name -- links to taxon

		// target 4
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u4")));
		doc.getGatherings().get(0).getUnits().get(3).setTaxonVerbatim("Pelecanus roseus? (ruusupelikaani)"); // target name that will be stripped to "pelacanusroseusruusupelikaani" -- will link to taxon

		// target 5
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u5")));
		doc.getGatherings().get(0).getUnits().get(4).setTaxonVerbatim("käki"); // links to taxon

		// target 6
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u6")));
		doc.getGatherings().get(0).getUnits().get(5).setTaxonVerbatim("käki (harmaa värimuoto)"); // will be stripped to "käki".. will link the same as u5

		// target 7
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u7")));
		doc.getGatherings().get(0).getUnits().get(6).setTaxonVerbatim("Pelecanus  xoofoobar"); // will not match but should link to pelecanus (genus) as a non exact match

		// target 8
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u8")));
		doc.getGatherings().get(0).getUnits().get(7).setTaxonVerbatim("koivunlehti 1 cm"); // will not match  anything in master checklist; but will resolve to MR.84 (spring monitoring checklist).  (Does not match anything in master because even though this is AKA name of koivu, koivu is not loaded at all as part of limited taxon loading)

		// target 9
		doc.getGatherings().get(0).addUnit(new Unit(new Qname("u9")));
		doc.getGatherings().get(0).getUnits().get(8).setTaxonVerbatim("hiiri, myyrä"); // aka name - will match

		interpreter.interpret(doc);
		converter.convert(doc);
		verticaDao.save(Utils.list(doc), STATUS_REPORTER);

		// Test targets have been linked to taxa correctly
		testTargetLinkings();

		// Test get by document fills in taxon linkings
		Document returnedDoc = verticaDao.getQueryDAO().get(doc.getDocumentId());
		System.out.println(ModelToJson.toJson(returnedDoc));
		assertEquals("Falco rusticolus", returnedDoc.getGatherings().get(0).getUnits().get(0).getLinkings().getTaxon().getScientificName());

		// Load taxa to vertica
		dimensions.startTaxonAndPersonReprosessing();

		// Compare taxon object and TaxonEntity values
		Taxon subsp = dao.getTaxon(new Qname("MX.52882"));
		TaxonEntity subspEntity = dimensions.getTaxonEntity(52882L);
		assertEquals(subsp.getParent().getTaxonomicOrder(), subspEntity.getSpeciesTaxonomicOrder().longValue());
		assertEquals(subsp.getTaxonomicOrder(), subspEntity.getTaxonomicOrder().longValue());
		assertEquals(subsp.getTaxonRank().toURI(), subspEntity.getTaxonRank());
		assertEquals(subsp.getScientificName(), subspEntity.getScientificName());
		assertEquals(subsp.getVernacularName().forLocale("fi"), subspEntity.getNameFinnish());
		assertEquals(Const.MASTER_CHECKLIST_BIOTA_QNAME.toString(), "MX."+subspEntity.getSuperdomainId());
		assertEquals(Const.MASTER_CHECKLIST_QNAME.toURI(), subspEntity.getNameAccordingTo());
		assertEquals(Const.MASTER_CHECKLIST_QNAME, subsp.getChecklist());

		// Check spring monitoring checklist taxon is loaded in test dao correctly so that following tests pass
		Taxon springMonitoringTaxon = dao.getTaxon(new Qname("MX.53780"));
		TaxonEntity springMonitoringTaxonEntity = dimensions.getTaxonEntity(53780L);
		assertEquals("MR.84", springMonitoringTaxon.getChecklist().toString());
		assertEquals("http://tun.fi/MR.84", springMonitoringTaxonEntity.getNameAccordingTo());
		assertEquals("koivunlehti 1 cm", springMonitoringTaxonEntity.getNameFinnish());
		assertEquals(springMonitoringTaxon.getVernacularName().forLocale("fi"), springMonitoringTaxonEntity.getNameFinnish());

		// Now lets process them
		new TargetReprocessor(dimensions).reprocess();

		// Should have one linking to spring monitoring checklist
		List<TargetChecklistTaxaEntity> targetChecklistEntities = dimensions.getTargetChecklistTaxa(new Qname("MR.84"));
		assertEquals(1, targetChecklistEntities.size());
		assertEquals(53780, targetChecklistEntities.get(0).getTaxonKey().intValue());

		// Test again after reprocessing target entities that results have not changed
		testTargetLinkings();
	}

	private void testTargetLinkings() {
		List<TargetEntity> targets = dimensions.getTargets(); // Ordered by id(=original reference) alphabetically
		for (TargetEntity t : targets) {
			System.out.println(t);
		}
		assertEquals(9, targets.size());

		TargetEntity t1 = targets.get(7);
		TargetEntity t2 = targets.get(8);
		TargetEntity t3 = targets.get(6);
		TargetEntity t4 = targets.get(4);
		TargetEntity t5 = targets.get(3);
		TargetEntity t6 = targets.get(2);
		TargetEntity t7 = targets.get(5);
		TargetEntity t8 = targets.get(1);
		TargetEntity t9 = targets.get(0);

		// target 1
		assertEquals("tunturihaukka:null:null:null", t1.getId());
		assertEquals(null, t1.getReferenceKey());
		assertEquals("tunturihaukka", t1.getTargetLowercase());
		assertEquals(26825, t1.getTaxonKey().intValue());
		assertEquals(null, t1.getNotExactTaxonMatch());

		// target 2
		assertEquals("ötökkö:MP.1234:null:null", t2.getId());
		assertNotNull(t2.getReferenceKey());
		assertEquals("ötökkö", t2.getTargetLowercase());
		assertEquals(null, t2.getTaxonKey());
		assertEquals(null, t2.getNotExactTaxonMatch());

		// target 3
		assertEquals("talitintti:null:null:null", t3.getId());
		assertEquals(null, t3.getReferenceKey());
		assertEquals("talitintti", t3.getTargetLowercase());
		assertEquals(34567, t3.getTaxonKey().intValue()); // aka name of Parus major (talitiainen)
		assertEquals(null, t3.getNotExactTaxonMatch());

		// target 4
		assertEquals("pelecanus roseus ruusupelikaani:null:null:null", t4.getId());
		assertEquals(null, t4.getReferenceKey());
		assertEquals("pelecanus roseus ruusupelikaani", t4.getTargetLowercase());
		assertEquals(26018, t4.getTaxonKey().intValue());
		assertEquals(null, t4.getNotExactTaxonMatch());

		// target 5
		assertEquals("käki:null:null:null", t5.getId());
		assertEquals(28715, t5.getTaxonKey().intValue());

		// target 6
		assertEquals("käki harmaa värimuoto:null:null:null", t6.getId());
		assertEquals(null, t6.getTaxonKey());
		assertEquals(null, t6.getNotExactTaxonMatch());

		// target 7
		assertEquals("pelecanus xoofoobar:null:null:null", t7.getId());
		assertEquals(null, t7.getTaxonKey());
		assertEquals(null, t7.getNotExactTaxonMatch());

		// target 8
		assertEquals("koivunlehti 1 cm:null:null:null", t8.getId());
		assertEquals(null, t8.getTaxonKey());
		assertEquals(null, t8.getNotExactTaxonMatch());

		// target 9
		assertEquals("hiiri myyrä:null:null:null", t9.getId());
		assertEquals(50594, t9.getTaxonKey().intValue());
		assertEquals(null, t9.getNotExactTaxonMatch());
	}

	@Test
	public void testIndividualCountFilter() throws Exception {
		DwRoot root = new DwRoot(new Qname("blaablaa"), new Qname("KE.3"));
		root.setCollectionId(Qname.fromURI("http://tun.fi/HR.1"));
		Document doc = root.createPrivateDocument();
		Gathering g = new Gathering(new Qname("gid_blaablaa"));
		doc.addGathering(g);

		Unit u0 = new Unit(new Qname("u0"));
		Unit u1 = new Unit(new Qname("u1"));
		Unit u2 = new Unit(new Qname("u2"));
		Unit u3 = new Unit(new Qname("u3"));
		g.addUnit(u0).addUnit(u1).addUnit(u2).addUnit(u3);

		//u0 -> null
		u1.setAbundanceString("5");
		u2.setAbundanceString("1");
		u3.setAbundanceString("0");

		u0.setTaxonVerbatim("musthavesomething");
		u1.setTaxonVerbatim("musthavesomething");
		u2.setTaxonVerbatim("musthavesomething");
		u3.setTaxonVerbatim("musthavesomething");
		u0.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u1.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u2.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u3.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);
		verticaDao.save(Utils.list(doc), STATUS_REPORTER);

		ListQuery query = new ListQuery(
				privateQuery().build(),
				1, 1000)
				.setSelected(new Selected("unit.unitId"))
				.setOrderBy(new OrderBy("unit.unitId"));
		Filters filters = query.getFilters();

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(0);
		filters.setIndividualCountMax(0);
		List<String> unitIds = getUnitIds(query);
		assertEquals("[u3]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(0);
		filters.setIndividualCountMax(1);
		unitIds = getUnitIds(query);
		assertEquals("[u0, u2, u3]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(null);
		filters.setIndividualCountMax(1);
		unitIds = getUnitIds(query);
		assertEquals("[u0, u2, u3]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(0);
		filters.setIndividualCountMax(null);
		unitIds = getUnitIds(query);
		assertEquals("[u0, u1, u2, u3]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(1);
		filters.setIndividualCountMax(null);
		unitIds = getUnitIds(query);
		assertEquals("[u0, u1, u2]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(null);
		filters.setIndividualCountMax(null);
		unitIds = getUnitIds(query);
		assertEquals("[u0, u1, u2, u3]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(10);
		filters.setIndividualCountMax(5);
		unitIds = getUnitIds(query);
		assertEquals("[]", unitIds.toString());

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(2);
		filters.setIndividualCountMax(10);
		unitIds = getUnitIds(query);
		assertEquals("[u1]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(2);
		filters.setIndividualCountMax(5);
		unitIds = getUnitIds(query);
		assertEquals("[u1]", unitIds.toString().replace("", ""));

		// u0: null     u1: 5     u2: 1   u3: 0
		filters.setIndividualCountMin(2);
		filters.setIndividualCountMax(4);
		unitIds = getUnitIds(query);
		assertEquals("[]", unitIds.toString());
	}

	private List<String> getUnitIds(ListQuery query) {
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		List<String> unitIds = new ArrayList<>();
		for (JoinedRow row : results) {
			unitIds.add(row.getUnit().getUnitId().toString());
		}
		return unitIds;
	}

	@Test
	public void cleanup() {
		verticaDao.performCleanUp(STATUS_REPORTER);
		dimensions.performCleanUp();
		assertEquals("[]", LOGGED_EXCEPTIONS.toString());
	}

	@Test
	public void documentIdLoad() {
		List<String> ids = Utils.list("doc1", "doc2", "doc3");
		dimensions.storeDocumentIds(ids.iterator());

		List<SplittedDocumentIdEntity> splitted = new ArrayList<>();
		splitted.add(new SplittedDocumentIdEntity(new Qname("doc1"), new Qname("unit1"), new Qname("A."+Utils.generateGUID())));
		splitted.add(new SplittedDocumentIdEntity(new Qname("doc2"), new Qname("unit2"), new Qname("A."+Utils.generateGUID())));
		dimensions.storeSplittedDocumentIds(splitted);
	}

	@Test
	public void searchingInvasiveControl() throws Exception {
		DwRoot controlDoc = getDoc("invasive-control.json");
		DwRoot targetDoc = getDoc("invasive-control-target.json");

		verticaDao.save(Utils.list(controlDoc.getPrivateDocument()), STATUS_REPORTER); // our control document
		verticaDao.save(Utils.list(targetDoc.getPublicDocument()), STATUS_REPORTER); // observation that matches the control
		verticaDao.save(Utils.list(createTestDoc(0), createTestDoc(1), createTestDoc(2)), STATUS_REPORTER); // some other test docs
		verticaDao.callGeoUpdate();

		List<Annotation> unprosessedInvasiveControls = verticaDao.getQueryDAO().getCustomQueries().getInvasiveControlled();
		assertEquals(1, unprosessedInvasiveControls.size());
		Annotation annotation = unprosessedInvasiveControls.get(0);

		assertEquals(null, annotation.getId());
		assertEquals(null, annotation.getAnnotationByPerson());
		assertEquals(Const.LAJI_ETL_QNAME, annotation.getAnnotationBySystem());
		assertEquals("["+Tag.INVASIVE_PARTIAL+"]", annotation.getAddedTags().toString());
		assertEquals(null, annotation.getNotes());
		//assertEquals("[JX.161276]", annotation.getRelatesTo().toString()); TODO do we need this? -- The idea was to be able to show a link to the event that has caused this occurrence to become controlled
		assertEquals(null, annotation.getIdentification());
		assertEquals("JX.161297", annotation.getRootID().toString());
		assertEquals("JX.161297#2", annotation.getTargetID().toString());
		assertEquals(AnnotationType.INVASIVE_CONTROL, annotation.resolveType());
		assertEquals(false, annotation.isDeleted());
		assertEquals(AnnotationType.INVASIVE_CONTROL, annotation.resolveType());
	}

	@Test
	public void customQueryResultStream() {
		try (ResultStream<String> s = verticaDao.getQueryDAO().getCustomQueries().getAllDocumentIds()) {

		}
	}

	private DwRoot getDoc(String filename) throws CriticalParseFailure, UnknownHarmonizingFailure, Exception {
		JSONObject json = new JSONObject(RdfXmlHarmonizerTests.getTestData(filename));
		List<DwRoot> roots = new LajistoreHarmonizer(dao.getLocalContextDefinition(), null).harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);
		return root;
	}

}
