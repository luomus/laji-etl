package fi.laji.datawarehouse.dao.vertica;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.StatelessSession;
import org.junit.After;
import org.junit.Before;

import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.etl.models.Converter;
import fi.laji.datawarehouse.etl.models.FakeThreadStatusReporter;
import fi.laji.datawarehouse.etl.models.Interpreter;
import fi.laji.datawarehouse.etl.models.Securer;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.models.TestDAO;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class VerticaDAOTestBase {

	protected static final Qname NAMED_PLACE_ID = new Qname("HR.128/12345");
	protected static final Qname FORM_ID = new Qname("MHL.1");
	protected static final ThreadStatusReporter STATUS_REPORTER = new FakeThreadStatusReporter();
	protected static final List<String> LOGGED_EXCEPTIONS = new ArrayList<>();

	protected BaseQueryBuilder publicQuery() {
		return query(Concealment.PUBLIC);
	}

	protected BaseQueryBuilder privateQuery() {
		return query(Concealment.PRIVATE);
	}

	private BaseQueryBuilder query(Concealment warehouse) {
		return new BaseQueryBuilder(warehouse).setApiSourceId("unittests").setCaller(this.getClass());
	}

	protected static class VerticaDaoExtended extends VerticaDAOPrivate {

		public VerticaDaoExtended(VerticaDAOImpleSharedInitialization sharedResources) {
			super(sharedResources, "LAJIETL_TESTS");
		}

		public void deleteAll() {
			StatelessSession session = null;
			try {
				session = dimensions.getSession();
				session.getTransaction().begin();
				delete(dimensionsSchema, "team", session);
				delete(dimensionsSchema, "team_agent", session);
				delete(dimensionsSchema, "userid", session);
				delete(dimensionsSchema, "agent", session);
				delete(dimensionsSchema, "target", session);
				delete(dimensionsSchema, "source", session);
				delete(dimensionsSchema, "taxon_temp", session);
				delete(dimensionsSchema, "taxon", session);
				delete(dimensionsSchema, "target_checklist_taxa", session);
				session.getTransaction().commit();
			} finally {
				if (session != null) session.close();
			}
			try {
				session = getETLSession();
				session.getTransaction().begin();
				delete(dataSchema, "unit", session);
				delete(dataSchema, "gathering", session);
				delete(dataSchema, "document", session);
				delete(dataSchema, "document_userid", session);
				delete(dataSchema, "document_fact", session);
				delete(dataSchema, "document_keyword", session);
				delete(dataSchema, "document_media", session);
				delete(dataSchema, "document_securereason", session);
				delete(dataSchema, "gathering_fact", session);
				delete(dataSchema, "gathering_geometry", session);
				delete(dataSchema, "gathering_media", session);
				delete(dataSchema, "gathering_userId", session);
				delete(dataSchema, "unit_fact", session);
				delete(dataSchema, "unit_media", session);
				delete(dataSchema, "annotation", session);
				delete(dataSchema, "taxoncensus", session);
				delete(dataSchema, "sample", session);
				delete(dataSchema, "sample_fact", session);
				delete(dataSchema, "sample_keyword", session);
				delete(dataSchema, "unit_effective_tag", session);
				session.getTransaction().commit();
			} finally {
				if (session != null) session.close();
			}
		}
		private void delete(String schema, String table, StatelessSession session) {
			session.createSQLQuery("DELETE FROM " + schema + "." + table).executeUpdate();
		}
	}

	protected static DAOImple dao;
	protected static VerticaDaoExtended verticaDao;
	protected static VerticaDAOImpleDimensions dimensions;
	protected static Interpreter interpreter;
	protected static Securer securer;
	protected static Converter converter;

	public static void init() {
		dao = new DAOImple(TestConfig.getConfig(), "LAJIETL_TESTS", new ErrorReportingToSystemErr(), new ThreadStatuses(), VerticaDAOTestBase.class) {
			@Override
			public Map<String, CollectionMetadata> getCollections() {
				return TestDAO.getFakeCollections();
			}
			@Override
			protected QueryLogStorer initQueryLogger() {
				return null;
			}
			@Override
			public Map<Qname, NamedPlaceEntity> getNamedPlaces() {
				Map<Qname, NamedPlaceEntity> places = new HashMap<>();
				NamedPlaceEntity testPlace = new NamedPlaceEntity(NAMED_PLACE_ID.toURI());
				testPlace.setBirdAssociationAreaId(new Qname("ML.1234").toURI());
				testPlace.addTag(new Qname("nptag1"));
				testPlace.addTag(new Qname("nptag2"));
				testPlace.setName(Utils.generateGUID());
				places.put(NAMED_PLACE_ID, testPlace);
				return places;
			}
			@Override
			public Map<Qname, NamedPlaceEntity> getNamedPlacesForceReload() {
				return getNamedPlaces();
			}
			@Override
			public void logError(Qname source, Class<?> phase, String identifier, Throwable exception) {
				String stackTrace = LogUtils.buildStackTrace(exception);
				LOGGED_EXCEPTIONS.add(Utils.debugS(source, phase.getSimpleName(), identifier, stackTrace));
				log(source, phase, "ERROR", identifier, stackTrace);
			}
			@Override
			public void logError(Qname source, Class<?> phase, String identifier, String message, Throwable exception) {
				String stackTrace = LogUtils.buildStackTrace(exception);
				LOGGED_EXCEPTIONS.add(Utils.debugS(source, phase.getSimpleName(), identifier, message));
				log(source, phase, "ERROR", identifier, stackTrace);
			}
			@Override
			protected void log(Qname source, Class<?> phase, String type, String identifier, String message) {
				System.out.println(Utils.debugS("FakeDAO LOGGING", source, phase.getSimpleName(), type, identifier, message));
			}
			@Override
			public String getPersisted(String key) {
				if (!"obscounts".equals(key))  {
					throw new UnsupportedOperationException(key);
				}
				JSONObject json = new JSONObject();
				JSONObject counts = new JSONObject();
				counts.setInteger("count", 5);
				counts.setInteger("countFinland", 1);
				counts.setString("taxonId", new Qname("MX.28715").toURI());
				json.getArray("results").appendObject(counts);
				return json.beautify();
			}
			@Override
			public String getPolygonSearch(long id) {
				if (id == 1) return "POLYGON((333349 6663919,333400 6663114,334256 6664218,333349 6663919))";
				return null;
			}
		};
		interpreter = new Interpreter(dao);
		securer = new Securer(dao);
		converter = new Converter(dao);
		verticaDao = new VerticaDaoExtended(dao.getSharedVerticaInitialization());
		verticaDao.deleteAll();
		dimensions = (VerticaDAOImpleDimensions) dao.getVerticaDimensionsDAO();
	}

	public static void cleanUp() {
		if (verticaDao != null) verticaDao.close();
		if (dao != null) dao.close();
	}

	@Before
	public void initBefore() {
		init();
	}

	@After
	public void clandUpAfter() {
		cleanUp();
	}

	protected static Date storeTestDocs(int count) throws ParseException, Exception {
		return storeTestDocs(count, 0);
	}

	protected static Date storeTestDocs(int count, int startAt) throws ParseException, Exception {
		List<Document> docs = new ArrayList<>();
		for (int i = 1; i <= count; i++) {
			Document doc = createTestDoc(startAt + i);
			docs.add(doc);
		}
		Date loadTime = new Date();
		verticaDao.save(docs, STATUS_REPORTER);
		verticaDao.callGeoUpdate();
		return loadTime;
	}

	protected static Document createTestDoc(int i) throws ParseException, Exception {
		DwRoot root = new DwRoot(new Qname("A.12345_" + i), new Qname("KE.3"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Document doc = root.createPrivateDocument();
		doc.setNotes("my notes");
		doc.setSecureLevel(SecureLevel.KM1);
		doc.setCreatedDate(new Date());
		doc.setModifiedDate(new Date());
		doc.addSecureReason(SecureReason.DEFAULT_TAXON_CONSERVATION);
		doc.addSecureReason(SecureReason.DATA_QUARANTINE_PERIOD);
		doc.setLicenseIdUsingQname(new Qname("MY.someLicense"));

		doc.addFact("f1", "f1v");
		doc.addFact("f2", "1,5");
		doc.addFact("F1", "F1V");
		doc.addFact("email", "xxx@xxx.xxx");
		doc.addFact("http://tun.fi/MZ.phoneNumber", "123456789");
		doc.addKeyword("hatikka:mun projekti");
		doc.addKeyword("http://tun.fi/GX.1234");

		doc.addEditorUserId("extiirai");
		doc.addEditorUserId("http://tun.fi/MA.5");

		doc.setNamedPlaceIdUsingQname(NAMED_PLACE_ID);
		doc.setFormIdUsingQname(FORM_ID);
		doc.setLoadTimeNow();
		doc.setFirstLoadTime(DateUtils.getCurrentEpoch());

		doc.setDataSource("rabbit out of hat");
		doc.setSiteStatus("exists");
		doc.setSiteType("monitoring site of dung beetles");
		doc.setSiteDead(false);

		doc.setCompleteListTaxonIdUsingQname(new Qname("MX.1"));
		doc.setCompleteListTypeUsingQname(new Qname("MY.completeListTypeComplete"));

		Annotation docAnnotation = new Annotation(new Qname("docan.1"), root.getDocumentId(), root.getDocumentId(), Util.toTimestamp("2018-07-26T18:51:53+00:00")).addTag(Tag.ADMIN_MARKED_COARSE).secureAnnotation();
		doc.addAnnotation(docAnnotation);

		MediaObject m4 = new MediaObject(MediaType.AUDIO, "https://aani.net/3.wav");
		m4.setCaption("*kurr* *kurr*");
		doc.addMedia(m4);

		doc.createQuality().setIssue(new Quality(Quality.Issue.INVALID_CREATED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, "message"));

		Gathering g = new Gathering(new Qname("gid_" + i));
		doc.addGathering(g);
		g.setBiogeographicalProvince("U");
		g.setCoordinates(new Coordinates(6666, 3333, Type.YKJ));
		g.setCoordinatesVerbatim("6666:3333 yhtenäiskoordinaatisto");
		g.setCountry("Finland");
		g.setEventDate(new DateRange(DateUtils.convertToDate("5.1.2015"), DateUtils.convertToDate("10.1.2015")));

		g.addFact("f1", "1");
		g.addFact("gf1", "2");

		g.setHigherGeography("Europe");
		g.setLocality("Inkoonperä");
		g.setMunicipality("inkoo");
		g.setNotes("gnotes");
		g.setProvince("Uusimaa");
		g.setHourBegin(11);
		g.setHourEnd(12);
		g.setMinutesEnd(45);

		g.addTeamMember("Meikäläinen, Matti");
		g.addTeamMember("http://tun.fi/MA.1");
		g.addTeamMember("MA.2");
		g.addObserverUserId("MA.2");
		g.addObserverUserId("hatikka:Esko.Piirainen@hatikka.fi");

		g.createQuality().setLocationIssue(new Quality(Quality.Issue.REPORTED_UNRELIABLE, Quality.Source.ORIGINAL_DOCUMENT));
		g.addTaxonCensus(new TaxonCensus(new Qname("MX.37580"), new Qname("MZ.someType"))); // MX.37580 == Aves

		g.setGatheringSection(1);

		g.setStateLand(false);

		MediaObject m1 = new MediaObject(MediaType.IMAGE, "https://kuva.net/"+i+"_1.jpg");
		g.addMedia(m1);

		Unit u1 = new Unit(new Qname("uid1_" + i));
		u1.addFact("uf1", "f1v");
		u1.addFact("f1", "25030393475759330227847455643723829282747464564638229292984744656574748383922883744");
		u1.addFact("age", "5");
		u1.addFact(new Qname("MY.temperature").toURI(), "-5");
		u1.addFact("http://tun.fi/MY.additionalIDs", "ABC:123");
		u1.addFact("http://tun.fi/MY.keywords", "foofarbar");

		u1.setAbundanceString("4");
		u1.setAuthor("author verbatim");
		u1.setDet("det verbatim");
		u1.setReferencePublication(new Qname("MP.1234"));
		u1.setBreedingSite(true);
		u1.setLifeStage(LifeStage.ADULT);
		u1.setNotes("u notes");
		u1.setRecordBasis(RecordBasis.FOSSIL_SPECIMEN);
		u1.setPrimarySpecimen(true);
		u1.setSex(Sex.FEMALE);
		u1.setTaxonVerbatim("talitintti");
		u1.setTypeSpecimen(false);

		Annotation u1annotation1 = new Annotation(new Qname("MAN.1_"+i), doc.getDocumentId(), u1.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00")).secureAnnotation();
		u1annotation1.setAnnotationByPerson(new Qname("MA.1"));
		u1annotation1.setAnnotationBySystem(new Qname("KE.3"));
		u1annotation1.addTag(Tag.EXPERT_TAG_VERIFIED);
		u1annotation1.setIdentification(new Identification("käki"));
		u1annotation1.setNotes("notes");
		u1.addAnnotation(u1annotation1);
		u1.addFact("pairCount", "2");

		Unit u2 = new Unit(new Qname("uid2_" + i));
		u2.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u2.setTaxonVerbatim("Hippulus cyppirys");
		u2.setTypeSpecimen(true);
		u2.setReportedTaxonConfidence(TaxonConfidence.UNSURE);

		Unit u3 = new Unit(new Qname("uid3_" + i));
		u3.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u3.setTaxonVerbatim("Hippulus cyppirys");
		u3.setReferencePublication(new Qname("MP.3"));
		u3.setIndividualIdUsingQname(new Qname("KE.67/FOOBAR"));
		u3.setReportedTaxonConfidence(TaxonConfidence.UNSURE);

		Annotation u3annotation = new Annotation(new Qname("MAN.4_"+i), doc.getDocumentId(), u3.getUnitId(), Util.toTimestamp("2017-06-26T18:51:53+00:00")).secureAnnotation();
		u3annotation.addTag(Tag.CHECKED_CANNOT_VERIFY);
		u3annotation.setAnnotationBySystem(doc.getSourceId());
		u3.addAnnotation(u3annotation.secureAnnotation());

		Unit u4 = new Unit(new Qname("uid4_" + i)); // unit4 is the one that matches listQuery() test
		u4.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u4.setTaxonVerbatim("ruusupelikaani");
		u4.setSex(Sex.MALE);
		u4.setRecordBasis(RecordBasis.MACHINE_OBSERVATION_AUDIO);
		u4.setLifeStage(LifeStage.JUVENILE);
		u4.setIndividualIdUsingQname(new Qname("KE.67/A123"));
		u4.setTypeSpecimen(true);
		u4.setAbundanceString("1");
		u4.setIndividualCountFemale(3);
		u4.setWild(true);
		u4.setAlive(false);
		u4.setLocal(true);
		u4.addSourceTag(Tag.INVASIVE_FULL);
		u4.setReportedInformalTaxonGroup(new Qname("MVL.3"));
		u4.setPlantStatusCodeUsingQname(new Qname("MY.plantStatusCodeAV"));
		u4.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum65"));
		u4.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
		u4.setSamplingMethodUsingQname(new Qname("MY.samplingMethodLightTrap"));
		u4.addIdentificationBasis(new Qname("MY.identificationBasisMicroscope"));
		u4.addIdentificationBasis(new Qname("MY.identificationBasisDNA"));

		Annotation u4annotation = new Annotation(new Qname("MAN.4_"+i), doc.getDocumentId(), u3.getUnitId(), Util.toTimestamp("2012-06-26T18:51:53+00:00")).secureAnnotation();
		u4annotation.setAnnotationBySystem(doc.getSourceId());
		u4annotation.addTag(Tag.EXPERT_TAG_VERIFIED);
		u4annotation.setAtlasCode(new Qname("MY.atlasCodeEnum65"));
		u4.addAnnotation(u4annotation.secureAnnotation());

		u4.addFact("pairCount", "1");

		MediaObject m2 = new MediaObject(MediaType.IMAGE, "https://kuva.net/"+i+"_2.jpg");
		m2.setThumbnailURL("https://kuva.net/2/thumb.jpg");
		MediaObject m3 = new MediaObject(MediaType.VIDEO, "https://kuva.net/"+i+"_1.mov");
		u4.addMedia(m2);
		u4.addMedia(m3);

		g.addUnit(u1);
		g.addUnit(u2);
		g.addUnit(u3);
		g.addUnit(u4);

		Sample s1 = new Sample(new Qname("s1_" + i));
		Sample s2 = new Sample(new Qname("s2_" + i));

		s1.setTypeUsingQname(new Qname("stype"));
		s2.setTypeUsingQname(new Qname("stype"));

		s1.addFact("sf1", "sf1v");
		s1.addFact("sf2", "sf2v");
		s1.addFact("sf3", "5.0");
		s1.addFact(new Qname("MY.sf4").toURI(), new Qname("MY.sf4EnumV").toURI());

		s1.addKeyword("sk1");
		s1.addKeyword("sk2");
		s1.addKeyword("something: skpart");
		s1.setCollectionId(new Qname("HR.2"));
		s1.setMultiple(true);
		s1.setNotes("notes");
		s1.setQualityUsingQname(new Qname("q"));
		s1.setStatusUsingQname(new Qname("s"));
		s1.setMaterialUsingQname(new Qname("mat"));

		u4.addSample(s1);
		u4.addSample(s2);

		root.setPublicDocument(Document.fromRoot(Concealment.PUBLIC, root)); // not used but prevents getting "ONLY_PRIVATE" secureReason to private document in securer

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);
		return doc;
	}


}
