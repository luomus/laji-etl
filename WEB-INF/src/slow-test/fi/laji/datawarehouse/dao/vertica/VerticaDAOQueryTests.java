package fi.laji.datawarehouse.dao.vertica;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.CollectionUpdater;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.NamedPlaceUpdater;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.FakeFileCreator;
import fi.laji.datawarehouse.query.download.FakeFileCreator.FakeFile;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.model.DownloadRequestExecutor;
import fi.laji.datawarehouse.query.download.rowhandlers.FlatCSVRowHandlerImple;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.CoordinatesWithOverlapRatio;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.Filters.Operator;
import fi.laji.datawarehouse.query.model.Filters.QualityIssues;
import fi.laji.datawarehouse.query.model.Filters.Wild;
import fi.laji.datawarehouse.query.model.Partition;
import fi.laji.datawarehouse.query.model.PolygonIdSearch;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.OrderBy.Order;
import fi.laji.datawarehouse.query.model.queries.OrderBy.OrderCriteria;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.laji.datawarehouse.query.service.annotation.AnnotationListQueryAPI;
import fi.laji.datawarehouse.query.service.sample.SampleListQueryAPI;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class VerticaDAOQueryTests extends VerticaDAOTestBase {

	@BeforeClass
	public static void init() {
		VerticaDAOTestBase.init();
		try {
			storeTestDocs(5);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		new NamedPlaceUpdater(dao).update();
		new CollectionUpdater(dao).update();
		dao.getVerticaDimensionsDAO().startTaxonAndPersonReprosessing();
	}

	@AfterClass
	public static void close() {
		VerticaDAOTestBase.cleanUp();
	}

	@Override
	@Before
	public void initBefore() {
		// override super - do nothing
	}

	@Override
	@After
	public void clandUpAfter() {
		// override super - do nothing
	}

	@Test
	public void listQuery() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();

		filters.setTaxonId(new Qname("MX.26015")); // this matches --- ruusupelikaani is part of pelikaanit
		filters.setTaxonId(new Qname("MX.26813")); // just some other random taxa that exists but will not cause results
		filters.setTarget("Pelikaanit"); // matches - same as MX.26015 but using name of the taxon
		filters.setTarget("foooobarhuurbar"); // (just some other target that will not cause results)

		filters.setSex(Sex.MALE); // -- this matches
		filters.setRecordBasis(RecordBasis.MACHINE_OBSERVATION_AUDIO); // -- this matches
		filters.setRecordBasis(RecordBasis.LITERATURE); // -- no match
		filters.setRecordBasis(RecordBasis.SUBFOSSIL_SPECIMEN); // -- no match
		filters.setSuperRecordBasis(RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED); // -- this matches (MACHINE_OBSERVATION_AUDIO - > superresultbasis -> MACHINE_OBS_UNSPECIFID)

		filters.setLifeStage(LifeStage.JUVENILE); // -- this matches
		filters.setIndividualId(new Qname("KE.67/A123")); // -- this matches
		filters.setIndividualId(new Qname("foobar"));
		filters.setTypeSpecimen(true); // -- this matches

		filters.setInvasive(false);
		filters.setFinnish(false);
		filters.setTaxonRankId(new Qname("MX.species")); // this matches -- ruusupelikaani is species
		filters.setTaxonRankId(new Qname("MX.superhuper")); // just some other filter

		filters.setSourceId(new Qname("KE.3")); // -- this matches
		filters.setSourceId(new Qname("KE.1234"));
		filters.setCollectionId(new Qname("HR.1")); // -- this matches
		filters.setCollectionId(new Qname("blaablaa"));

		filters.setNamedPlaceId(NAMED_PLACE_ID); // this matches
		filters.setNamedPlaceId(new Qname("FooBar")); // some other id
		filters.setNamedPlaceTag(new Qname("nptag1")); // this matches
		filters.setNamedPlaceTag(new Qname("nptag2")); // this matches
		filters.setNamedPlaceTag(new Qname("foobar")); // some other id
		filters.setNamedPlaceTag(new Qname("-- select 1;")); // some other id

		filters.setFormId(FORM_ID); // This matches
		filters.setFormId(new Qname("FooBar"));  // some other id

		// informal group filter tests removed for test speed performance reasons -- informal groups are no longer loaded
		// filters.setInformalTaxonGroupId(new Qname("MVL.1")); // -- this matches; ruusupelikaani belongs to informal group "Linnut"
		// filters.setInformalTaxonGroupId(new Qname("huuhaa")); // just something else, no matches
		// filters.setInformalTaxonGroupId(new Qname("MVL.2")); // just something else, no matches
		//filters.setInformalTaxonGroupId(new Qname("MVL.3")); // -- this does not match
		filters.setInformalTaxonGroupIdIncludingReported(new Qname("MVL.3")); // -- this matches, a reported informal taxon group

		filters.setEditorId(new Qname("MA.5")); // -- this matches
		filters.setEditorId(new Qname("MA.1")); //

		filters.setEditorOrObserverId(new Qname("MA.5")); // -- this matches

		filters.setKeyword("hatikka:mun projekti"); // -- this matches
		filters.setKeyword("foobar");

		filters.setTime("2000/2015-11"); // -- this matches 5.-10.1.2015
		filters.setTime("1999-12-12"); // just some other date
		filters.setTime("1995/1996-12-31"); // just some other date
		filters.setTime("-7/0"); // just some other date
		filters.setTime("-9"); // just some other date

		filters.setYearMonth("2015-01/2015-03"); // this matches 1.1.2015
		filters.setYearMonth("2014-01"); // just some other range
		filters.setYearMonth("2017/2019"); // just some other range

		filters.setCountryId(Const.FINLAND); // -- this matches
		filters.setCountryId(new Qname("FOOBAR.1")); // just some other area
		filters.setBiogeographicalProvinceId(new Qname("ML.253")); // -- this matches
		filters.setBiogeographicalProvinceId(new Qname("FOOBAR.2")); // just some other area
		filters.setFinnishMunicipalityId(new Qname("ML.363"));
		filters.setFinnishMunicipalityId(new Qname("FOOBAR.3")); // just some other area
		filters.setElyCentreId(new Qname("ML.1246")); // ML.363 is part of this ELY
		filters.setElyCentreId(new Qname("ML.1247")); // some other ely
		filters.setElyCentreId(new Qname("FOOBAR.4")); // some other ely
		filters.setProvinceId(new Qname("ML.1227")); // ML.363 is part of this province
		filters.setProvinceId(new Qname("ML.1228")); // some other province
		filters.setProvinceId(new Qname("FOOBAR.5")); // some other province

		filters.setCoordinates(new CoordinatesWithOverlapRatio(6666000.0, 6667000.0, 3333000.0, 3334000.0, Type.YKJ)); // -- this matches
		filters.setCoordinates(new CoordinatesWithOverlapRatio(0.0, 30.0, 5.0, 32.0, Type.WGS84)); // just some other coordinates
		filters.setCoordinates(new CoordinatesWithOverlapRatio(6664000, 6664200, 332800, 332900, Type.EUREF)); // -- this matches also
		filters.setCoordinateAccuracyMax(100000); // -- this matches
		filters.setSourceOfCoordinates(GeoSource.REPORTED_VALUE); // matches

		filters.setCoordinates(new CoordinatesWithOverlapRatio(65.0, 69.0, 32.0, 36.0, Type.YKJ).setOverlapRatio(1.0)); // -- this matches; this huge area contains 6666:3333 entirely (ratio = 1)
		filters.setCoordinates(new CoordinatesWithOverlapRatio(69999.0, 34111.0, Type.YKJ).setOverlapRatio(1.0)); // does not match

		filters.setYkj10km(new SingleCoordinates(666, 333, Type.YKJ)); // this matches
		filters.setYkj1km(new SingleCoordinates(6666, 3333, Type.YKJ)); // this matches

		filters.setYkj1kmCenter(new SingleCoordinates(6666, 3333, Type.YKJ)); // this matches

		//60.076461	24.005504
		filters.setWgs84CenterPoint(new CoordinatesWithOverlapRatio(60.07, 60.08, 24.00, 24.1, Type.WGS84)); // this matches
		filters.setWgs84CenterPoint(new CoordinatesWithOverlapRatio(42, 42.001, -1.0, 2.0, Type.WGS84)); // does not match

		filters.setArea("inkoonPerÄ"); // -- this matches, should go to verbatim search
		filters.setArea("foobar"); // just some other area, should go to verbatim search
		filters.setArea("ML.363"); // -- this matches also, should go to municipality qname search
		filters.setArea("Espoo"); // this should go to municipality qname search, does not match

		filters.setLoadedSameOrAfter(new Date(new Date().getTime() - 100000)); // matches

		filters.setDayOfYear("1/10"); // matches    5.1.2015 - 10.1.2015
		filters.setDayOfYear("360/10"); // does not match
		filters.setDayOfYear("60/70"); // does not match

		filters.setSeason("101/110"); // matches   5.1.2015 - 10.1.2015
		filters.setSeason("1231/110"); // does not match
		filters.setSeason("110/111"); // does not match

		filters.setSecureReason(SecureReason.DEFAULT_TAXON_CONSERVATION); // -- this matches
		filters.setSecureReason(SecureReason.BREEDING_SITE_CONSERVATION);
		filters.setSecured(true); // matches

		filters.setHasMedia(true); // -- matches
		filters.setHasUnitMedia(true); // -- matches
		filters.setHasGatheringMedia(true); // -- matches
		filters.setHasDocumentMedia(true); // -- matches
		filters.setHasUnitImages(true); // -- matches
		filters.setHasUnitAudio(false); // -- matches
		filters.setHasUnitVideo(true); // -- matches

		filters.setInvasiveControlled(true); // matches
		filters.setInvasiveControl(InvasiveControl.FULL); // matches

		filters.setCollectionQuality(CollectionQuality.PROFESSIONAL); // matches
		filters.setCollectionQuality(CollectionQuality.AMATEUR); // does not match

		filters.setQualityIssues(QualityIssues.ONLY_ISSUES); // matches
		filters.setRecordQuality(RecordQuality.NEUTRAL); // matches -- is neutral because has issues
		filters.setRecordQuality(RecordQuality.COMMUNITY_VERIFIED); // does not match
		filters.setReliability(Reliability.UNDEFINED); // matches
		filters.setEffectiveTag(Tag.EXPERT_TAG_VERIFIED); // matches
		filters.setEffectiveTag(Tag.CHECK_DUPLICATE); // just some other tag, no match

		filters.setTaxonCensus(new Qname("MX.26009")); // matches -- test document has taxon census for aves and pelikaanilinnut (MX.26009) is part of aves
		filters.setTaxonCensus(new Qname("MX.37600")); // taxon census for biota - no match

		filters.setAnnotated(true);

		filters.setTeamMember("*eikäläinen* m*"); // this matches
		filters.setTeamMember("foobar"); // does not match
		filters.setTeamMemberId(dimensions.getKeyDoNoCreate(new AgentEntity("Meikäläinen, Matti"))); // this matches
		filters.setTeamMemberId(-1L); // does not match

		filters.setTypeOfOccurrenceIdNot(new Qname("foobar")); // matches, does not have this status
		filters.setTypeOfOccurrenceIdNot(new Qname("MX.typeOfOccurrenceImport")); // this matches also

		filters.setHasSample(true);

		filters.setPlantStatusCode(new Qname("foobar"));
		filters.setPlantStatusCode(new Qname("MY.plantStatusCodeAV")); // matches
		filters.setAtlasCode(new Qname("foobar")); // matches
		filters.setAtlasCode(new Qname("MY.atlasCodeEnum65")); // matches
		filters.setAtlasClass(new Qname("foobar")); // matches
		filters.setAtlasClass(new Qname("MY.atlasClassEnumC")); // matches

		filters.setAlive(false);

		filters.setCompleteListTaxonId(new Qname("MX.1")); // matches
		filters.setCompleteListTaxonId(new Qname("MX.1234")); // some other value
		filters.setCompleteListType(new Qname("MY.completeListTypeComplete")); // matches
		filters.setCompleteListType(new Qname("foobar")); // some other value

		filters.setTaxonSetId(new Qname("MX.testSet")); // contains ruusupelikaani
		filters.setTaxonSetId(new Qname("MX.foobar")); // some other value

		filters.setSamplingMethod(new Qname("MY.samplingMethodLightTrap"));
		filters.setSamplingMethod(new Qname("foobar"));
		filters.setIdentificationBasis(new Qname("MY.identificationBasisMicroscope"));
		filters.setIdentificationBasis(new Qname("foobar"));

		ListResponse results = verticaDao.getQueryDAO().getList(
				new ListQuery(baseQuery, 4, 1) // fourth of five
				.setOrderBy(new OrderBy("document.documentId")));
		assertEquals(1, results.getResults().size());
		assertEquals(5, results.getTotal()); // 5 documents with similar 4 units (5*4=20 total); one unit matches of each document -> 5 units (u4)
		JoinedRow row = results.getResults().get(0);

		// JSONObject serialized = ModelToJson.rowToJson(row);
		// System.out.println(serialized.beautify());

		assertEquals("uid4_4", row.getUnit().getUnitId().toString()); // fourth of five
		assertEquals("A.12345_4", row.getDocument().getDocumentId().toString()); // fourth of five
		assertEquals("6666000 3333000", row.getGathering().getConversions().getYkj().getLatMin().intValue() + " " + row.getGathering().getConversions().getYkj().getLonMin().intValue());
		assertEquals(1, row.getDocument().getMediaCount().intValue());
		assertEquals(1, row.getGathering().getMediaCount().intValue());
		assertEquals(2, row.getUnit().getMediaCount().intValue());
		assertEquals(2, row.getUnit().getMedia().size());
		MediaObject m1 = row.getUnit().getMedia().get(0);
		assertEquals(MediaType.IMAGE, m1.getMediaType());
		assertEquals("https://kuva.net/4_2.jpg", m1.getFullURL());
		assertEquals("Pelecanus roseus", row.getUnit().getLinkings().getTaxon().getScientificName());
		assertEquals(true, row.getUnit().getLinkings().getTaxon().isSpecies());
		assertEquals(true, row.getUnit().getLinkings().getTaxon().isCursiveName());

		assertEquals(2, row.getDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, row.getDocument().getSecureReasons().get(0));

		// check does not create queried non-existing resources to dimensions
		assertEquals(null, dimensions.getEntity(new Qname("blaablaa").toURI(), CollectionEntity.class));

		assertEquals(3, row.getUnit().getIndividualCountFemale().intValue());

		assertEquals(0, row.getGathering().getGatheringOrder());
		assertEquals(3, row.getUnit().getUnitOrder());
	}

	@Test
	public void teamFilterIsSubQueryNotJoin() {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();

		filters.setTeamMember("*eikäläinen* m*"); // this matches
		filters.setTeamMember("*Dare*"); // this matches
		filters.setDocumentId(Qname.fromURI("http://tun.fi/A.12345_1"));
		filters.setUnitId(Qname.fromURI("http://tun.fi/uid1_1"));

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(
				new ListQuery(baseQuery, 1, 100)).getResults();
		assertEquals(1, results.size());
	}

	@Test
	public void teamAggregates() throws Exception {
		testTeamAggregatesWithBase(Base.UNIT);
		testTeamAggregatesWithBase(Base.GATHERING);
	}

	private void testTeamAggregatesWithBase(Base base) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(
				privateQuery().setBase(base).build(),
				new AggregateBy("gathering.team", "gathering.team.memberId", "gathering.team.memberName"),
				1, 100);
		query.getFilters().setQualityIssues(QualityIssues.BOTH);
		query.setOrderBy(new OrderBy("gathering.team.memberName"));
		List<AggregateRow> rows = verticaDao.getQueryDAO().getAggregate(query).getResults();
		int i = 1;
		for (AggregateRow row : rows) {
			System.out.println(row.getAggregateByValues() + " = " + row.getCount());
			assertEquals("Meikäläinen, Matti; Dare Talvitie; Tapani Lahti", row.getAggregateByValues().get(0).toString());
			assertTrue(row.getAggregateByValues().get(1) instanceof Long);
			if (i == 1) assertEquals("Dare Talvitie", row.getAggregateByValues().get(2));
			if (i == 2) assertEquals("Meikäläinen, Matti", row.getAggregateByValues().get(2));
			if (i == 3) assertEquals("Tapani Lahti", row.getAggregateByValues().get(2));
			i++;
		}
		assertEquals(3, rows.size());
	}

	@Test
	public void listQuery_only_target__gets_neccesary_left_join_to_taxon() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters()
		.setQualityIssues(QualityIssues.BOTH)
		.setTarget("Pelikaanit");

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(
				new ListQuery(baseQuery, 1, 10)
				.setOrderBy(new OrderBy("document.documentId"))).getResults();
		assertEquals(5, results.size());
	}

	@Test
	public void testGivingNonExistingTaxonDoesNotCauseException() {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setTaxonId(new Qname("MX.FOO")); // does not exist
		List<JoinedRow> r = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1)).getResults();
		assertEquals(0, r.size());
	}

	@Test
	public void testSearchByNonTypeSpecimens() {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters()
		.setQualityIssues(QualityIssues.BOTH)
		.setTypeSpecimen(false);
		long count = verticaDao.getQueryDAO().getCount(new CountQuery(baseQuery)).getTotal();
		assertEquals(10, count); // 5 docs, 4 units of which 2 are not type specimens = 10
	}

	@Test
	public void testSearchByIds() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();
		filters.setDocumentId(new Qname("A.12345_2"));
		filters.setUnitId(new Qname("uid4_2"));

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000).setSelected(new Selected("unit.unitId", "document.documentId"))).getResults();
		assertEquals(1, results.size());
		JoinedRow row = results.get(0);
		assertEquals("A.12345_2", row.getDocument().getDocumentId().toString());
		assertEquals("uid4_2", row.getUnit().getUnitId().toString());
	}

	@Test
	public void getsDistinctResultsOnly() {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();
		filters.setKeyword("hatikka:mun projekti"); // -- both match, must not get unit row twice
		filters.setKeyword("http://tun.fi/GX.1234"); // -- both match, must not get unit row twice
		filters.setTarget("Ruusupelikaani"); // only one unit of each doc matches --> expecting 1 unit rows per doc as result == 5

		long count = verticaDao.getQueryDAO().getCount(new CountQuery(baseQuery)).getTotal();
		assertEquals(5, count);

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
		assertEquals(5, results.size());
	}

	@Test
	public void aggregatedQuery() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();
		filters.setKeyword("http://tun.fi/GX.1234");

		AggregateBy aggregateBy = new AggregateBy("document.collectionId", "gathering.team.memberName", "unit.taxonVerbatim");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 1000);
		aggregatedQuery.setOrderBy(new OrderBy("document.collectionId", "gathering.team.memberName", "unit.taxonVerbatim"));
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();

		Long expectedCollectionKey = dimensions.getKey(new CollectionEntity("http://tun.fi/HR.1"));
		List<String> expected = Utils.list(
				"["+expectedCollectionKey+", Dare Talvitie, Hippulus cyppirys]",
				"["+expectedCollectionKey+", Dare Talvitie, ruusupelikaani]",
				"["+expectedCollectionKey+", Dare Talvitie, talitintti]",
				"["+expectedCollectionKey+", Meikäläinen, Matti, Hippulus cyppirys]",
				"["+expectedCollectionKey+", Meikäläinen, Matti, ruusupelikaani]",
				"["+expectedCollectionKey+", Meikäläinen, Matti, talitintti]",
				"["+expectedCollectionKey+", Tapani Lahti, Hippulus cyppirys]",
				"["+expectedCollectionKey+", Tapani Lahti, ruusupelikaani]",
				"["+expectedCollectionKey+", Tapani Lahti, talitintti]" );
		List<Integer> expectedCounts = Utils.list(10, 5, 5, 10, 5, 5, 10, 5, 5);

		assertEquals(9, results.size()); // 1 collection * 3 persons * 3 distinct species = 9

		int i = 0;
		for (AggregateRow row : results) {
			assertEquals(3, row.getAggregateByValues().size());
			List<Object> result = Utils.list(row.getAggregateByValues().get(0), row.getAggregateByValues().get(1), row.getAggregateByValues().get(2));
			assertEquals(expected.get(i), result.toString());
			assertEquals(expectedCounts.get(i), row.getCount() == null ? null : row.getCount().intValue());
			i++;
		}
	}

	@Test
	public void aggregatedQueryWithNotOnlyCount() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters();
		filters.setKeyword("http://tun.fi/GX.1234");

		AggregateBy aggregateBy = new AggregateBy("gathering.municipality");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 1000)
				.setOnlyCountRequest(false) // <--- this includes individual count sum/max, occurrence date min/max, lineLengthSum
				.setPairCountRequest(true)  // <-- this includes pair count sum
				.setTaxonCountRequest(true) // <-- this includes taxon/species count
				.setAtlasCountRequest(true) // <-- this includes atlas counts
				.setGatheringCountRequest(true); // <-- this includes gathering count
		aggregatedQuery.setOrderBy(new OrderBy("gathering.municipality"));
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, results.size());
		AggregateRow row = results.get(0);
		Long recordQualityMaxKey = dimensions.getEnumKey(RecordQuality.NEUTRAL);
		assertEquals("" +
				"AggregateRow [aggregateByValues=[inkoo], count=20, individualCountSum=35, individualCountMax=4, oldestRecord=2015-01-05, newestRecord=2015-01-10, "+
				"firstLoadDateMin="+DateUtils.getCurrentDate()+", firstLoadDateMax="+DateUtils.getCurrentDate()+", "+
				"pairCountSum=15, pairCountMax=2, lineLengthSum=null, taxonCount=2, speciesCount=2, " +
				"atlasCodeMax=http://tun.fi/MY.atlasCodeEnum65, atlasClassMax=http://tun.fi/MY.atlasClassEnumC, " +
				"recordQualityMax="+recordQualityMaxKey+", redListStatusGroupMax=http://tun.fi/MX.iucnGroup1, gatheringCount=5, securedCount=20]",
				row.toString());
	}

	@Test
	public void aggregatedByNoFields() {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy();
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, results.size());
		assertEquals(20, results.get(0).getCount().intValue());
		assertEquals(0, results.get(0).getAggregateByValues().size());
	}

	@Test
	public void aggregatedByNoFields_gathering() {
		BaseQuery baseQuery = privateQuery().setBase(Base.GATHERING).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy();
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, results.size());
		assertEquals(5, results.get(0).getCount().intValue());
		assertEquals(0, results.get(0).getAggregateByValues().size());
	}

	@Test
	public void aggregatedByNoFields_document() {
		BaseQuery baseQuery = privateQuery().setBase(Base.DOCUMENT).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy();
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, results.size());
		assertEquals(5, results.get(0).getCount().intValue());
		assertEquals(0, results.get(0).getAggregateByValues().size());
	}

	@Test
	public void aggregateTaxonCensus() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy().addField("gathering.taxonCensus.taxonId").addField("gathering.taxonCensus.type");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, results.size());
		assertEquals("37580", results.get(0).getAggregateByValues().get(0).toString());
		assertEquals("http://tun.fi/MZ.someType", results.get(0).getAggregateByValues().get(1).toString());
	}

	@Test
	public void aggregateAtlasValues() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy().addField("gathering.conversions.ykj10kmCenter.lat").addField("gathering.conversions.ykj10kmCenter.lon");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5).setAtlasCountRequest(true);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		results.forEach(r->System.out.println(r));
		assertEquals(1, results.size());
		AggregateRow row = results.get(0);
		assertEquals("[666.0, 333.0]", row.getAggregateByValues().toString());
		assertEquals("http://tun.fi/MY.atlasCodeEnum65", row.getAtlasCodeMax());
		assertEquals("http://tun.fi/MY.atlasClassEnumC", row.getAtlasClassMax());
	}

	@Test
	public void aggregateMediaValues() throws Exception {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();

		AggregateBy aggregateBy = new AggregateBy().addField("unit.media.mediaType").addField("unit.media.licenseId").addField("gathering.media.mediaType").addField("document.media.mediaType");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();

		String result = results.stream().map(r -> Utils.list(
				dimensions.getIdForKey((Long)r.getAggregateByValues().get(0), "unit.media.mediaType"),
				(String)r.getAggregateByValues().get(1),
				dimensions.getIdForKey((Long)r.getAggregateByValues().get(2), "gathering.media.mediaType"),
				dimensions.getIdForKey((Long)r.getAggregateByValues().get(3), "document.media.mediaType"),
				r.getCount())
				.toString())
				.collect(Collectors.toList()).toString();

		assertEquals("" +
				"[[IMAGE, null, IMAGE, AUDIO, 5], [VIDEO, null, IMAGE, AUDIO, 5]]",
				result);
	}

	@Test
	public void aggregatedByCountNoFields() {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		AggregateBy aggregateBy = new AggregateBy();
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 5);
		long count = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getTotal();
		assertEquals(1, count);
	}

	@Test
	public void aggregatedByCount() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH).setIncludeNonValidTaxa(false);
		AggregateBy aggregateBy = new AggregateBy().addField("unit.unitId");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 250);
		long count = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getTotal();
		assertEquals(10, count);
	}

	@Test
	public void aggregatedByCount_base_gathering() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.GATHERING).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH).setIncludeNonValidTaxa(false);
		AggregateBy aggregateBy = new AggregateBy().addField("gathering.gatheringId");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 250);
		long count = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getTotal();
		assertEquals(5, count);
	}

	@Test
	public void aggregatedByCount_base_document() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.DOCUMENT).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH).setIncludeNonValidTaxa(false);
		AggregateBy aggregateBy = new AggregateBy().addField("document.documentId");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 250);
		long count = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getTotal();
		assertEquals(5, count);
	}

	@Test
	@Ignore // too many combinations -> test has become too slow; run this after adding new fields -> just make sure sql is accepted and then abort query
	// NOTE!: After running this, check and kill the query - see documentation/useful_vertica_statements.sql
	public void aggregatedByAllFields() throws Exception {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();

		AggregateBy aggregateBy = new AggregateBy(baseQuery.getBase(), false);
		OrderBy orderBy = new OrderBy(baseQuery.getBase());
		for (String field : Fields.aggregatable(baseQuery.getBase()).getAllFields()) {
			aggregateBy.addField(field);
			orderBy.addAscending(field);
		}
		for (String s : Const.aggregateFunctions(baseQuery.getBase())) {
			orderBy.addAscending(s);
		}
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 350)
				.setOrderBy(orderBy)
				.setOnlyCountRequest(false)
				.setPairCountRequest(true)
				.setTaxonCountRequest(true);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		int c = 0;
		for (AggregateRow row : results) {
			int i = 0;
			for (String field : aggregateBy.getFields()) {
				Object value = row.getAggregateByValues().get(i++);
				if (value instanceof Long) {
					String id = dimensions.getIdForKey((Long) value, field); // Test resolving id by key
					Utils.debug(field, value, id);
				}
			}
			if (c++ > 5) break;
		}
		assertEquals(350, results.size()); // same as limit
	}

	@Test
	public void aggregatedByAllFields_baseGathering() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.GATHERING).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy(Base.GATHERING, false);
		OrderBy orderBy = new OrderBy(Base.GATHERING);
		for (String field : Fields.aggregatable(baseQuery.getBase()).getAllFields()) {
			aggregateBy.addField(field);
			orderBy.addAscending(field);
		}
		for (String s : Const.aggregateFunctions(baseQuery.getBase())) {
			orderBy.addAscending(s);
		}
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 350)
				.setOrderBy(orderBy).setOnlyCountRequest(false);

		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(350, results.size()); // same as limit
	}

	@Test
	public void aggregatedByAllFields_baseDocument() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.DOCUMENT).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy(Base.DOCUMENT, false);
		OrderBy orderBy = new OrderBy(baseQuery.getBase());
		for (String field : Fields.aggregatable(baseQuery.getBase()).getAllFields()) {
			aggregateBy.addField(field);
			orderBy.addAscending(field);
		}
		for (String s : Const.aggregateFunctions(baseQuery.getBase())) {
			orderBy.addAscending(s);
		}
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 350)
				.setOrderBy(orderBy)
				.setOnlyCountRequest(false);

		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(300, results.size()); // 5 doc * 5 facts * 3 keywords * 2 secure reasons * 2 distinct fields = 300
	}

	@Test
	public void orderByAllFields() throws Exception {
		testOrderByAllFields(Base.UNIT);
		testOrderByAllFields(Base.DOCUMENT);
	}

	private void testOrderByAllFields(Base base) throws NoSuchFieldException {
		BaseQuery baseQuery = privateQuery().setBase(base).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy(base, false);
		OrderBy orderBy = new OrderBy(baseQuery.getBase());
		for (String field : Fields.sortable(base).getAllFields()) {
			orderBy.addAscending(field);
			if (!field.startsWith(Const.RANDOM)) {
				aggregateBy.addField(field);
			}
		}
		orderBy.addAscending("RANDOM:123");

		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 10).setOrderBy(orderBy);
		ListQuery listQuery = new ListQuery(baseQuery, 1, 10).setOrderBy(orderBy);

		verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		verticaDao.getQueryDAO().getList(listQuery).getResults();
	}

	@Test
	public void selectAllFields() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		Selected selected = new Selected(Base.UNIT);
		for (String field : Fields.selectable(Base.UNIT).getAllFields()) {
			selected.add(field);
		}

		ListQuery listQuery = new ListQuery(baseQuery, 1, 10).setSelected(selected);
		List<JoinedRow> rows = verticaDao.getQueryDAO().getList(listQuery).getResults();
		JoinedRow row = rows.get(0);
		assertEquals("extiirai", row.getDocument().getLinkings().getEditors().get(0).getUserId());
		assertEquals("http://tun.fi/MA.2", row.getGathering().getLinkings().getObservers().get(0).getUserId());
		assertEquals(CollectionQuality.PROFESSIONAL, row.getDocument().getLinkings().getCollectionQuality());
	}

	@Test
	public void aggregate_no_defined_order() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy("document.documentId", "unit.taxonVerbatim");
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 10);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		String data = "";
		for (AggregateRow row : results) {
			data += Utils.debugS(row.getAggregateByValues(), row.getCount()) + "\n";
		}
		// first order by count, then by each aggregate by column (asc)
		String expected = "" +
				"[http://tun.fi/A.12345_1, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_2, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_3, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_4, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_5, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_1, ruusupelikaani] : 1\n"+
				"[http://tun.fi/A.12345_1, talitintti] : 1\n"+
				"[http://tun.fi/A.12345_2, ruusupelikaani] : 1\n"+
				"[http://tun.fi/A.12345_2, talitintti] : 1\n"+
				"[http://tun.fi/A.12345_3, ruusupelikaani] : 1\n";
		assertEquals(expected, data);
	}

	@Test
	public void aggregate_defined_order() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregateBy aggregateBy = new AggregateBy("document.documentId", "unit.taxonVerbatim");
		OrderBy orderBy = new OrderBy(baseQuery.getBase());
		orderBy.addDescending("document.documentId");
		orderBy.addAscending(Const.COUNT);
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, 1, 10).setOrderBy(orderBy);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		String data = "";
		for (AggregateRow row : results) {
			data += Utils.debugS(row.getAggregateByValues(), row.getCount()) + "\n";
		}
		// first order by count, then by each aggregate by column (asc)
		String expected = "" +
				"[http://tun.fi/A.12345_5, ruusupelikaani] : 1\n"+
				"[http://tun.fi/A.12345_5, talitintti] : 1\n"+
				"[http://tun.fi/A.12345_5, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_4, ruusupelikaani] : 1\n"+
				"[http://tun.fi/A.12345_4, talitintti] : 1\n"+
				"[http://tun.fi/A.12345_4, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_3, ruusupelikaani] : 1\n"+
				"[http://tun.fi/A.12345_3, talitintti] : 1\n"+
				"[http://tun.fi/A.12345_3, Hippulus cyppirys] : 2\n"+
				"[http://tun.fi/A.12345_2, ruusupelikaani] : 1\n";
		assertEquals(expected, data);
	}

	@Test
	public void countQuery() {
		CountQuery query = new CountQuery(privateQuery().build());
		query.getFilters()
		.setQualityIssues(QualityIssues.BOTH)
		.setTime("1990-01-01/"); // missing end date replaced with 0 (today)
		long count = verticaDao.getQueryDAO().getCount(query).getTotal();
		assertEquals(20L, count);
	}

	@Test
	public void countQuery_cached() {
		CountQuery query = new CountQuery(privateQuery().setBase(Base.UNIT).setUseCache(true).build());
		query.getFilters().setQualityIssues(QualityIssues.BOTH)
		.setTime("1990-01-01/"); // missing end date replaced with 0 (today)
		long count = verticaDao.getQueryDAO().getCount(query).getTotal();
		assertEquals(20L, count);
	}

	@Test
	public void download() throws Exception {
		FakeFileCreator fileCreator = new FakeFileCreator();
		BaseQuery baseQuery = privateQuery().setPersonEmail("some@email.com").setPersonId(new Qname("MA.5")).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		baseQuery.getFilters().setActualLoadBefore(new Date(System.currentTimeMillis()+(24*60*60*1000)));
		DownloadRequest request = new DownloadRequest(new Qname("HBF.123"), new Date(), baseQuery, DownloadType.CITABLE, DownloadFormat.CSV_FLAT, allIncludes());
		DownloadRequestExecutor creator = new DownloadRequestExecutor(request, new FlatCSVRowHandlerImple(fileCreator, dao), verticaDao.getQueryDAO());
		creator.create();
		List<File> files = creator.getCreatedFiles();
		Set<String> fileNames = new TreeSet<>();
		for (File f : files) {
			FakeFile ff = (FakeFile) f;
			fileNames.add(ff.getName());
			if (ff.getName().equals("document_keywords")) {
				assertEquals(11, ff.getLines().size());
			}
			if (ff.getName().equals("rows")) {
				assertEquals(21, ff.getLines().size());
			}
		}
		Set<String> expected = new TreeSet<>(Utils.set("document_media", "unit_facts", "gathering_media", "document_facts", "gathering_facts", "unit_media", "rows", "document_keywords", "samples"));
		assertEquals(expected.toString(), fileNames.toString());
		assertEquals("[HR.1]", creator.getCollectionIds().toString());
		for (File f : fileCreator.getWrittenFiles()) {
			assertTrue(((FakeFile) f).isClosed());
		}
	}

	private Set<DownloadInclude> allIncludes() {
		Set<DownloadInclude> downloadIncludes = new HashSet<>(Utils.list(DownloadInclude.values()));
		return downloadIncludes;
	}

	@Test
	public void secure_level_filters() {
		BaseQuery baseQuery = publicQuery().build();
		Filters filters = baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		CountQuery countQuery = new CountQuery(baseQuery);
		filters.setSecured(true);
		long secCount = verticaDao.getQueryDAO().getCount(countQuery).getTotal();

		filters.setSecured(false);
		long nonSecCount = verticaDao.getQueryDAO().getCount(countQuery).getTotal();
		Utils.debug(secCount, nonSecCount);
		assertEquals(20, secCount);
		assertEquals(0, nonSecCount);
	}

	@Test
	public void secure_reason_filter() {
		BaseQuery baseQuery = publicQuery().build();
		CountQuery countQuery = new CountQuery(baseQuery);
		countQuery.getFilters().setQualityIssues(QualityIssues.BOTH)
		.setSecureReason(SecureReason.CUSTOM)
		.setSecureReason(SecureReason.DEFAULT_TAXON_CONSERVATION);
		long count = verticaDao.getQueryDAO().getCount(countQuery).getTotal();
		assertEquals(20, count);
	}

	@Test
	public void secure_reason_aggregate_by() throws Exception {
		BaseQuery baseQuery = publicQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, new AggregateBy("document.secureReasons"), 1, 10);
		aggregatedQuery.setOrderBy(new OrderBy("document.secureReasons"));
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(2, results.size());
		assertEquals(20, results.get(0).getCount().longValue());
		int i = 1;
		for (AggregateRow row : results) {
			String resrowReason = ResponseUtil.getValue(row.getAggregateByValues().get(0), "document.secureReasons", dimensions);
			System.out.println(resrowReason);
			if (i == 1) assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION.name(), resrowReason);
			if (i == 2) assertEquals(SecureReason.DATA_QUARANTINE_PERIOD.name(), resrowReason);
			i++;
		}
	}

	@Test
	public void orderby_joinable_fields() throws Exception {
		BaseQuery baseQuery = publicQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		ListQuery query = new ListQuery(baseQuery, 1, 1000);
		OrderBy orderBy = new OrderBy("gathering.team", "unit.linkings.taxon.nameFinnish", "unit.linkings.taxon.scientificName", "unit.taxonVerbatim", "gathering.hourBegin");
		query.setOrderBy(orderBy);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		assertEquals(20, results.size()); // does not do a join with all team members (which would result in 3*20 = 60 rows)
	}

	@Test
	public void selecting_partial() throws Exception {
		Document partialDoc = new Document(Concealment.PUBLIC, new Qname("KE.3"), new Qname("XXX"), new Qname("HR.1"));
		partialDoc.setPartial(true);
		partialDoc.addGathering(new Gathering(new Qname("XXX#G")));
		partialDoc.getGatherings().get(0).addUnit(new Unit(new Qname("XXX#U")));
		partialDoc.getGatherings().get(0).getUnits().get(0).setTaxonVerbatim("blaa");
		interpreter.interpret(partialDoc);
		converter.convert(partialDoc);
		verticaDao.save(Utils.list(partialDoc), STATUS_REPORTER);
		try {
			BaseQuery baseQuery = publicQuery().build();
			baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
			ListQuery query = new ListQuery(baseQuery, 1, 1000);
			Selected selected = new Selected("document.documentId", "document.partial", "document.secured");
			query.setSelected(selected);

			List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
			for (JoinedRow row : results) {
				if (row.getDocument().getDocumentId().toString().equals("XXX")) {
					assertTrue(row.getDocument().isPartial());
				} else {
					assertFalse(row.getDocument().isPartial());
				}
			}
		} finally {
			verticaDao.delete(partialDoc, STATUS_REPORTER);
		}
	}

	@Test
	public void unidentified() {
		ListQuery query = new ListQuery(publicQuery().build(), 1, 1000);
		query.getFilters().setQualityIssues(QualityIssues.BOTH);

		// unidentified
		query.getFilters().setUnidentified(true);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		assertEquals(5, results.size()); // u2 x 5

		// not unidentified
		query.getFilters().setUnidentified(false);
		results = verticaDao.getQueryDAO().getList(query).getResults();
		assertEquals(15, results.size());
	}

	@Test
	public void annotation_list() throws Exception {
		ListQuery listQuery = new ListQuery(privateQuery().setBase(Base.ANNOTATION).build(), 1, 100);

		listQuery.getFilters()
		.setAnnotatedSameOrAfter(DateUtils.convertToDate("1.1.1900"))
		.setAnnotationType(AnnotationType.USER_CHECK)
		.setAnnotationType(AnnotationType.USER_EFFECTIVE)
		.setCollectionQuality(CollectionQuality.PROFESSIONAL)
		.setIncludeSystemAnnotations(false);

		listQuery.setOrderBy(new OrderBy(listQuery.getBase()).addDescending("unit.annotations.created").addAscending("unit.annotations.annotationByPersonName"));
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(listQuery).getResults();
		Long prevCreatedTimestamp = null;
		for (JoinedRow row : results) {
			//System.out.println(ModelToJson.toSelectedJSON(row, new Selected(Base.ANNOTATION, "annotation.id", "annotation.created", "annotation.annotationByPersonName", "unit.unitId")));
			long thisCreated = row.getAnnotation().getCreatedTimestamp();
			if (prevCreatedTimestamp != null) {
				assertTrue(thisCreated <= prevCreatedTimestamp); // order by created time DESC
			}
			prevCreatedTimestamp = thisCreated;
			assertEquals("Dare Talvitie", row.getAnnotation().getAnnotationByPersonName());
		}
		assertEquals(5, results.size());
	}

	@Test
	public void taxonUsedCustomQuery() {
		assertTrue(verticaDao.getQueryDAO().getCustomQueries().isTaxonIdUsed(new Qname("MX.28715")));
	}

	@Test
	public void annotationAggregate() throws Exception {
		BaseQuery baseQuery = privateQuery().build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, new AggregateBy("unit.annotations.annotationByPerson"), 1, 10);
		List<AggregateRow> rows = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		for (AggregateRow row : rows) {
			System.out.println(row.toString());
		}
		assertEquals(1, rows.size());
	}

	@Test
	public void unitAggregate() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.UNIT).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, new AggregateBy("gathering.conversions.year"), 1, 10).setOnlyCountRequest(false).setPairCountRequest(true);

		List<AggregateRow> rows = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, rows.size());
		assertEquals(20, rows.get(0).getCount().intValue());
	}

	@Test
	public void gatheringAggregate() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.GATHERING).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, new AggregateBy("gathering.conversions.year"), 1, 10).setOnlyCountRequest(false).setPairCountRequest(true);

		List<AggregateRow> rows = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(1, rows.size());
		assertEquals(5, rows.get(0).getCount().intValue());
		assertEquals(null, rows.get(0).getLineLengthSum());
		assertEquals("2015-01-05", DateUtils.format(rows.get(0).getOldestRecord(), "yyyy-MM-dd"));
		assertEquals("2015-01-10", DateUtils.format(rows.get(0).getNewestRecord(), "yyyy-MM-dd"));
		assertEquals(DateUtils.getCurrentDate(), DateUtils.format(rows.get(0).getFirstLoadDateMin(), "yyyy-MM-dd"));
		assertEquals(DateUtils.getCurrentDate(), DateUtils.format(rows.get(0).getFirstLoadDateMax(), "yyyy-MM-dd"));
	}

	@Test
	public void pessimisticDateRangeAggregate() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.UNIT).build();
		baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);

		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, new AggregateBy("gathering.conversions.year"), 1, 10)
				.setOnlyCountRequest(false)
				.setPessimisticDateRangeHandling(true);

		List<AggregateRow> rows = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		for (AggregateRow row : rows) {
			assertEquals("2015-01-10", DateUtils.format(row.getOldestRecord(), "yyyy-MM-dd"));
			assertEquals("2015-01-05", DateUtils.format(row.getNewestRecord(), "yyyy-MM-dd")); // oldest > newest is desired behaviour
		}
	}

	@Test
	public void coordinateOverlapRatio() throws Exception {
		// Observation area is 6666:3333

		// Search area: 666:333 -> obsevation area always inside search area -> all searches match regardless of overlap ratio
		testOverlapRatio(new CoordinatesWithOverlapRatio(666, 333, Type.YKJ).setOverlapRatio(1.0), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(666, 333, Type.YKJ).setOverlapRatio(0.5), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(666, 333, Type.YKJ).setOverlapRatio(0.0), true);

		// Search area 6666:3333 is the same as observation area -> all searches match regardless of overlap ratio
		testOverlapRatio(new CoordinatesWithOverlapRatio(6666, 3333, Type.YKJ).setOverlapRatio(1.0), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6666, 3333, Type.YKJ).setOverlapRatio(0.5), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6666, 3333, Type.YKJ).setOverlapRatio(0.0), true);

		// Search area 66665-66675:33335-33345 is 1/4 inside the observation area, so overlap ratios <= 0.25 match
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(1.0), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(0.5), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(0.26), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(0.25), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(0.24), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(66665, 66675, 33335, 33345, Type.YKJ).setOverlapRatio(0.0), true);
	}

	@Test
	public void coordinateOverlapRatio_euref() throws Exception {
		// Observation area is 6663201-6664201 : 332898-333897

		// Search area 6664000-6663000 : 333897-332898 is ~0.799 inside the observation area, so overlap ratios <= 0.79 match
		testOverlapRatio(new CoordinatesWithOverlapRatio(6663000, 6664000, 332898, 333897, Type.EUREF).setOverlapRatio(1.0), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6663000, 6664000, 332898, 333897, Type.EUREF).setOverlapRatio(0.8), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6663000, 6664000, 332898, 333897, Type.EUREF).setOverlapRatio(0.7), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6663000, 6664000, 332898, 333897, Type.EUREF).setOverlapRatio(0.5), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(6663000, 6664000, 332898, 333897, Type.EUREF).setOverlapRatio(0.0), true);
	}

	@Test
	public void coordinateOverlapRatio_wgs84() throws Exception {
		// Observation area is 60.071776-60.081146 : 23.996942-24.014066

		// Search area 60.08-60.0805 : 20.0-30.0 is ~0.053 inside the observation area, so overlap ratios <= 0.05 match
		testOverlapRatio(new CoordinatesWithOverlapRatio(60.08, 60.0805, 20.0, 30.0, Type.WGS84).setOverlapRatio(1.0), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(60.08, 60.0805, 20.0, 30.0, Type.WGS84).setOverlapRatio(0.9), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(60.08, 60.0805, 20.0, 30.0, Type.WGS84).setOverlapRatio(0.06), false);
		testOverlapRatio(new CoordinatesWithOverlapRatio(60.08, 60.0805, 20.0, 30.0, Type.WGS84).setOverlapRatio(0.05), true);
		testOverlapRatio(new CoordinatesWithOverlapRatio(60.08, 60.0805, 20.0, 30.0, Type.WGS84).setOverlapRatio(0.0), true);
	}

	private void testOverlapRatio(CoordinatesWithOverlapRatio c, boolean shouldMatch) {
		ListQuery listQuery = new ListQuery(privateQuery().setBase(Base.UNIT).build(), 1, 1);
		listQuery.getFilters().setCoordinates(c).setQualityIssues(QualityIssues.BOTH);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(listQuery).getResults();
		if (shouldMatch) {
			assertEquals(1, results.size());
		} else {
			assertEquals(0, results.size());
		}
	}

	@Test
	public void polygonId() {
		PolygonIdSearch polygonId = new PolygonIdSearch(1);
		ListQuery listQuery = new ListQuery(privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build(), 1, 1);
		listQuery.getFilters().setPolygonId(polygonId);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(listQuery).getResults();
		assertEquals(1, results.size());
	}

	@Test
	public void unknownPolygonId() {
		PolygonIdSearch polygonId = new PolygonIdSearch(-1);
		ListQuery listQuery = new ListQuery(privateQuery().setBase(Base.UNIT).build(), 1, 1);
		listQuery.getFilters().setPolygonId(polygonId);
		try {
			verticaDao.getQueryDAO().getList(listQuery).getResults();
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Nothing found with polygonId -1", e.getMessage());
		}
	}

	@Test
	public void primaryHabitatAggregate() throws NoSuchFieldException {
		AggregatedQuery a = new AggregatedQuery(privateQuery().setDefaultFilters(false).build(), new AggregateBy("unit.linkings.taxon.id", "unit.linkings.taxon.primaryHabitat"), 1, 10);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(a).getResults();
		assertEquals(2, results.size());
		assertEquals("[http://tun.fi/MX.26018, null]", results.get(0).getAggregateByValues().toString());
		assertEquals(5, results.get(0).getCount().intValue());
		assertEquals("[http://tun.fi/MX.28715, MKV.habitatMk]", results.get(1).getAggregateByValues().toString());
		assertEquals(5, results.get(1).getCount().intValue());
	}

	@Test
	public void allHabitatsAggregate() throws NoSuchFieldException {
		AggregatedQuery a = new AggregatedQuery(privateQuery().setDefaultFilters(false).build(), new AggregateBy("unit.linkings.taxon.id", "unit.linkings.taxon.habitats"), 1, 10);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(a).getResults();
		assertEquals(5, results.size());
		assertEquals("[http://tun.fi/MX.26018, null]", results.get(0).getAggregateByValues().toString());
		assertEquals("[http://tun.fi/MX.28715, MKV.habitatMk]", results.get(1).getAggregateByValues().toString());
		assertEquals("[http://tun.fi/MX.28715, MKV.habitatMl]", results.get(2).getAggregateByValues().toString());
		assertEquals("[http://tun.fi/MX.28715, MKV.habitatMt]", results.get(3).getAggregateByValues().toString());
		assertEquals("[http://tun.fi/MX.28715, MKV.habitatSr]", results.get(4).getAggregateByValues().toString());
	}

	@Test
	public void birdArea() throws Exception {
		assertEquals(20, testBirdAreaWithBase(Base.UNIT));
		assertEquals(5,  testBirdAreaWithBase(Base.GATHERING));
	}

	private int testBirdAreaWithBase(Base base) throws NoSuchFieldException {
		AggregatedQuery a = new AggregatedQuery(privateQuery().setBase(base).build(), new AggregateBy("gathering.conversions.birdAssociationArea"), 1, 10);
		a.getFilters().setQualityIssues(QualityIssues.BOTH).setBirdAssociationAreaId(new Qname("ML.1091")); // Testdoc 666:333 belongs to Tringa ML.1091
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(a).getResults();
		assertEquals(1, results.size());
		assertEquals("["+dimensions.getKey(new AreaEntity(new Qname("ML.1091").toURI()))+"]", results.get(0).getAggregateByValues().toString());
		return results.get(0).getCount().intValue();
	}

	@Test
	public void joinDocumentSecureReasonFromUnit() throws Exception {
		AggregatedQuery aggregatedQuery = new AggregatedQuery(privateQuery().setDefaultFilters(false).build(),
				new AggregateBy("document.collectionId", "document.secureReasons"), 1, 10000).setExludeNulls(true);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();

		String result = results.stream().map(r->Utils.list(
				dimensions.getIdForKey((Long)r.getAggregateByValues().get(0), "document.collectionId"),
				dimensions.getIdForKey((Long)r.getAggregateByValues().get(1), "document.secureReasons"),
				r.getCount())
				.toString())
				.collect(Collectors.toList()).toString();

		String expected = "[" +
				"[http://tun.fi/HR.1, DEFAULT_TAXON_CONSERVATION, 20], "+
				"[http://tun.fi/HR.1, DATA_QUARANTINE_PERIOD, 20]]";
		assertEquals(expected, result);
	}

	@Test
	public void joinTaxonSets() throws Exception {
		AggregatedQuery aggregatedQuery = new AggregatedQuery(privateQuery().setDefaultFilters(false).build(),
				new AggregateBy("unit.linkings.taxon.taxonSets", "unit.linkings.originalTaxon.taxonSets", "unit.linkings.taxon.id", "gathering.conversions.year"), 1, 10000)
				.setExludeNulls(false);
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(aggregatedQuery).getResults();
		assertEquals(4, results.size());
		for (AggregateRow row : results) {
			if (new Qname("MX.26018").toURI().equals(row.getAggregateByValues().get(2))) {
				String set = dimensions.getIdForKey((Long)row.getAggregateByValues().get(0), "unit.linkings.taxon.taxonSets");
				assertEquals(new Qname("MX.testSet").toURI(), set);
			}
		}
	}

	@Test
	public void documentList() throws NoSuchFieldException, ParseException {
		ListQuery query = new ListQuery(privateQuery().setBase(Base.DOCUMENT).setApiSourceId(Const.LAJI_ETL_QNAME.toString()).setCaller(this.getClass()).build(), 1, 100);
		Filters filters = query.getFilters();
		query.setSelected(new Selected(Base.DOCUMENT).add("document.documentId"));
		Qname q1 = new Qname("A.12345_1");
		Qname q2 = new Qname("A.12345_4");
		filters.setDocumentId(q1);
		filters.setDocumentId(q2);
		filters.setDocumentId(new Qname("foobar"));
		String today = DateUtils.getCurrentDateTime("yyyy-MM-dd");
		filters.setLoadedSameOrBefore(DateUtils.convertToDate(today, "yyyy-MM-dd"));

		Set<Qname> existing = new HashSet<>();
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		for (JoinedRow row : results) {
			existing.add(row.getDocument().getDocumentId());
		}
		assertEquals(2, existing.size());
		assertTrue(existing.contains(q1));
		assertTrue(existing.contains(q2));
	}

	@Test
	public void dateFilters() throws ParseException {
		ListQuery query = new ListQuery(privateQuery().setBase(Base.DOCUMENT).setDefaultFilters(false).setApiSourceId(Const.LAJI_ETL_QNAME.toString()).setCaller(this.getClass()).build(), 1, 100);
		Filters filters = query.getFilters();

		String todayString = DateUtils.getCurrentDateTime("yyyy-MM-dd");
		Date today = DateUtils.convertToDate(todayString, "yyyy-MM-dd");

		Date future = new Date(new Date().getTime()+ 1000L*60*60*24);

		filters.setLoadedSameOrBefore(today);
		filters.setFirstLoadedSameOrBefore(today);
		filters.setFirstLoadedSameOrAfter(today);
		filters.setLoadedSameOrAfter(today);
		filters.setActualLoadBefore(future);

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		assertTrue(results.size() > 0);
	}

	@Test
	public void aggregateDocumentId() throws Exception {
		AggregatedQuery query = new AggregatedQuery(
				privateQuery().setBase(Base.DOCUMENT).setApiSourceId(Const.LAJI_ETL_QNAME.toString()).setCaller(this.getClass()).build(),
				new AggregateBy(Base.DOCUMENT, false).addField("document.documentId"), 1, 100);
		Filters filters = query.getFilters();
		Qname q1 = new Qname("A.12345_1");
		Qname q2 = new Qname("A.12345_4");
		filters.setDocumentId(q1);
		filters.setDocumentId(q2);
		filters.setDocumentId(new Qname("foobar"));
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(query).getResults();
		Set<Qname> existing = new HashSet<>();
		for (AggregateRow row : results) {
			existing.add(Qname.fromURI(row.getAggregateByValues().get(0).toString()));
		}
		assertEquals(2, existing.size());
		assertTrue(existing.contains(q1));
		assertTrue(existing.contains(q2));
	}

	//	@Test
	//	public void aggregateTeamMember() throws Exception {
	//		///aggregate?aggregateBy=gathering.team.memberName,gathering.team.memberId&teamMember=<hakutermi>
	//		AggregatedQuery query = new AggregatedQuery(new BaseQueryBuilder(Concealment.PRIVATE).setBase(Base.GATHERING, Const.LAJI_ETL_QNAME.toString()),
	//				new AggregateBy("document.documentId"), 1, 100);
	////		AggregatedQuery query = new AggregatedQuery(new BaseQueryBuilder(Concealment.PRIVATE).setBase(Base.UNIT, Const.LAJI_ETL_QNAME.toString()),
	////				new AggregateBy("gathering.team.memberName", "gathering.team.memberId"), 1, 100);
	//		query.getFilters().setTeamMember("*a*");
	//		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(query).getResults();
	//		for (AggregateRow row : results) {
	//			System.out.println(row.getAggregateByValues() + " " + row.getCount());
	//		}
	//	}

	@Test
	public void annotated() {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		filters.setAnnotated(true);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
		System.out.println("annotated=true: " + results.size());
		assertEquals(15, results.size());

		filters.setAnnotated(false);
		results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
		System.out.println("annotated=false: false " + results.size());
		assertEquals(5, results.size());
	}

	@Test
	public void listAndAggregateAnnotation() throws Exception {
		BaseQuery baseQuery = privateQuery().setBase(Base.ANNOTATION).build();
		Filters filters = baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		filters.setIncludeSystemAnnotations(true);
		filters.setAnnotationType(AnnotationType.USER_EFFECTIVE); // matches
		filters.setAnnotationType(AnnotationType.USER_CHECK); // matches
		filters.setAnnotationType(AnnotationType.DW_AUTO); // just some other or clause value, does not match
		filters.setAnnotatedSameOrBefore(DateUtils.convertToDate("2050-01-01", "yyyy-MM-dd"));
		filters.setAnnotatedSameOrAfter(DateUtils.convertToDate("1950-01-01", "yyyy-MM-dd"));
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000).setOrderBy(AnnotationListQueryAPI.DEFAULT_ORDER_BY)).getResults();
		assertEquals(15, results.size());
		assertEquals("" +
				"MAN.1_1",
				results.get(0).getAnnotation().getId().toString());

		List<AggregateRow> aggregateResults = verticaDao.getQueryDAO().getAggregate(new AggregatedQuery(baseQuery, new AggregateBy("unit.unitId"), 1, 1000)).getResults();
		assertEquals(15, aggregateResults.size());
	}

	@Test
	public void listUnitMedia() throws NoSuchFieldException {
		BaseQuery baseQuery = privateQuery().setBase(Base.UNIT_MEDIA).setDefaultFilters(false).build();
		baseQuery.getFilters().setTarget("Biota");
		baseQuery.getFilters().setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.PROFESSIONAL).setRecordQuality(RecordQuality.NEUTRAL).setRecordQuality(RecordQuality.UNCERTAIN));
		baseQuery.getFilters().setNamedPlaceTag(new Qname("nptag1"));
		baseQuery.getFilters().setKeyword("hatikka:mun projekti");
		baseQuery.getFilters().setDocumentId(new Qname("A.12345_1"));
		baseQuery.getFilters().setDocumentId(new Qname("A.12345_2"));
		baseQuery.getFilters().setDocumentId(new Qname("A.12345_3"));
		baseQuery.getFilters().setDocumentId(new Qname("A.12345_4"));
		baseQuery.getFilters().setDocumentId(new Qname("A.12345_5"));
		baseQuery.getFilters().setIdentificationBasis(new Qname("MY.identificationBasisDNA"));

		OrderBy orderBy = new OrderBy(Base.UNIT_MEDIA)
				.add(new OrderCriteria("unit.media.mediaType", Order.ASC))
				.add(new OrderCriteria("unit.media.fullURL", Order.ASC));
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000).setOrderBy(orderBy)).getResults();

		assertEquals(10, results.size()); // 5 units with 2 images = 10

		List<String> imageUrls = new ArrayList<>();
		for (JoinedRow row : results) {
			imageUrls.add(row.getMedia().getFullURL());
		}
		assertEquals("" +
				"[https://kuva.net/1_2.jpg, https://kuva.net/2_2.jpg, https://kuva.net/3_2.jpg, https://kuva.net/4_2.jpg, https://kuva.net/5_2.jpg, https://kuva.net/1_1.mov, https://kuva.net/2_1.mov, https://kuva.net/3_1.mov, https://kuva.net/4_1.mov, https://kuva.net/5_1.mov]",
				imageUrls.toString());
	}

	@Test
	public void customQuery_unlinkeduserids() {
		assertEquals(1, verticaDao.getQueryDAO().getCustomQueries().getUnlinkedUserIds().size());
	}

	@Test
	public void taxonFilterCombinations() {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();
		Filters filters = baseQuery.getFilters();
		filters.setTarget("aves");
		filters.setTaxonId(new Qname("MX.37580"));
		filters.setAdministrativeStatusId(new Qname("MX.finlex160_1997_appendix4"));
		filters.setFinnish(true);
		filters.setSensitive(true);
		filters.setIncludeNonValidTaxa(false);
		//filters.setInformalTaxonGroupId(new Qname("foobar")); // these can't be included in the query because no informal groups are loaded
		//filters.setInformalTaxonGroupIdNot(new Qname("foobar"));
		filters.setInvasive(true);
		filters.setRedListStatusId(new Qname("MX.iucnLC"));
		filters.setTaxonRankId(new Qname("MX.species"));
		filters.setUnidentified(true);
		filters.setIncludeSubTaxa(false);
		filters.setUseIdentificationAnnotations(false);
		filters.setOnlyNonValidTaxa(true);
		filters.setHigherTaxon(false);

		// There is no way to get this query to return any other results than 0 rows so we just test here that the query does not fail..
		verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
		verticaDao.getQueryDAO().getAggregate(new AggregatedQuery(baseQuery, new AggregateBy(), 1, 10));
	}

	@Test
	public void taxonFilterCombinations_2() {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();
		Filters filters = baseQuery.getFilters();

		filters.setTarget("MX.26018");
		filters.setIncludeSubTaxa(false);
		filters.setUseIdentificationAnnotations(false);

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
		assertEquals(5, results.size());

		List<AggregateRow> aggreResults = verticaDao.getQueryDAO().getAggregate(new AggregatedQuery(baseQuery, new AggregateBy(), 1, 10)).getResults();
		assertEquals(1, aggreResults.size());
	}

	@Test
	public void factFilters() {
		BaseQuery query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=5"); // has fact age=5
		long count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=4/5"); // has fact age=5
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=-1.2/-6.5");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(0L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=-1.2/6.5");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact(new Qname("age").toURI()+"=5");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact(new Qname("MY.temperature").toURI());
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("MY.temperature");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("foo");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(0L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("foo=");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(0L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=5");
		query.getFilters().setUnitFact("age=4"); // has fact age=5
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(0L, count);

		query = privateQuery().setBase(Base.UNIT).setDefaultFilters(false).build();
		query.getFilters().setUnitFact("age=5");
		query.getFilters().setGatheringFact("f1=1");
		query.getFilters().setDocumentFact("f1=f1v");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.GATHERING).setDefaultFilters(false).build();
		query.getFilters().setGatheringFact("f1=1");
		query.getFilters().setDocumentFact("f1=f1v");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);

		query = privateQuery().setBase(Base.DOCUMENT).setDefaultFilters(false).build();
		query.getFilters().setDocumentFact("f1=f1v");
		count = verticaDao.getQueryDAO().getCount(new CountQuery(query)).getTotal();
		assertEquals(5L, count);
	}

	@Test
	public void yearMonth() {
		BaseQuery baseQuery = privateQuery().build();
		Filters filters = baseQuery.getFilters().setQualityIssues(QualityIssues.BOTH);
		filters.setYearMonth("2000");
		filters.setYearMonth("2000/");
		filters.setYearMonth("2000-01");
		filters.setYearMonth("2000/2005");
		filters.setYearMonth("2000-01/2020-12");
		filters.setYearMonth("2000-01/2020");
		filters.setYearMonth("2020/1950-01"); // begin after end but no exception will be thrown -> this just does not return any results ever (user fault)
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();

		assertEquals(20, results.size());

		filters.setYearMonth("2000-01-01");
		try {
			verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid value. Valid formats are yyyy, yyyy-mm, yyyy/yyyy, yyyy-mm/yyyy-mm, yyyy/yyyy-mm and yyyy-mm/yyyy.", e.getMessage());
		}

		filters.getYearMonth().clear();

		filters.setYearMonth("2000-01/2000-01-05");
		try {
			verticaDao.getQueryDAO().getList(new ListQuery(baseQuery, 1, 1000)).getResults();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid value. Valid formats are yyyy, yyyy-mm, yyyy/yyyy, yyyy-mm/yyyy-mm, yyyy/yyyy-mm and yyyy-mm/yyyy.", e.getMessage());
		}
	}

	@Test
	public void habitatFilters() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setPrimaryHabitat("MKV.habitatM");
		query.getFilters().setAnyHabitat("MKV.habitatM");
		query.getFilters().setAnyHabitat("[MKV.habitatSpecificTypeJ]");
		query.getFilters().setAnyHabitat("[MKV.habitatSpecificTypeJ,MKV.foobar]");
		query.getFilters().setAnyHabitat("MKV.foobar[MKV.joojuu,MKV.juujoo]");

		long c = verticaDao.getQueryDAO().getCount(query).getTotal();
		assertEquals(5, c);
	}

	@Test
	public void occurrenceCountFilters() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setTaxonId(new Qname("MX.28715")); // käki

		assertEquals(5L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 5 units total for käki

		// käki has
		//counts.setInteger("count", 5);
		//counts.setInteger("countFinland", 1);

		query.getFilters().setOccurrenceCountMax(10);
		assertEquals(5L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 5

		query.getFilters().setOccurrenceCountMax(6);
		assertEquals(5L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 5

		query.getFilters().setOccurrenceCountMax(5);
		assertEquals(0L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 0

		query.getFilters().setOccurrenceCountMax(0);
		assertEquals(0L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 0

		query.getFilters().setOccurrenceCountMax(null);
		query.getFilters().setOccurrenceCountFinlandMax(3);
		assertEquals(5L, verticaDao.getQueryDAO().getCount(query).getTotal()); // 5

		query.getFilters().setOccurrenceCountFinlandMax(1);
		assertEquals(0L, verticaDao.getQueryDAO().getCount(query).getTotal());  // 0
	}

	@Test
	public void unitKeywordFilter() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setKeyword("ABC:123FOO");

		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setKeyword("ABC:123");
		assertEquals(5, verticaDao.getQueryDAO().getCount(query).getTotal());

		query = new CountQuery(privateQuery().setBase(Base.GATHERING).setDefaultFilters(false).build());
		query.getFilters().setKeyword("ABC:123");
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void wild() {
		// test data has 5 units, one of them marked wild=true rest are unknown   (and 4 identical documents, ie 4*5 = 20 units)
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(true).build());
		query.getFilters().setQualityIssues(QualityIssues.BOTH);
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal()); // by default search for wild and unknown

		query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setWild(Wild.WILD);
		assertEquals(5, verticaDao.getQueryDAO().getCount(query).getTotal());

		query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setWild(Wild.NON_WILD);
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void dayofYearStartingWithSlash() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setDayOfYear("/50");
		query.getFilters().setDayOfYear("1/50");
		query.getFilters().setDayOfYear("50/");
		query.getFilters().setDayOfYear("50");
		query.getFilters().setDayOfYear("/");
		query.getFilters().setSeason("/0202");
		query.getFilters().setSeason("0101/0202");
		query.getFilters().setSeason("0101/");
		query.getFilters().setSeason("0101");
		query.getFilters().setSeason("/");
		verticaDao.getQueryDAO().getCount(query).getTotal();
	}

	@Test
	public void sample_list() {
		ListQuery listQuery = new ListQuery(privateQuery().setBase(Base.SAMPLE).build(), 1, 100);
		listQuery.setOrderBy(SampleListQueryAPI.DEFAULT_ORDER_BY);
		List<JoinedRow> results = verticaDao.getQueryDAO().getList(listQuery).getResults();
		assertEquals(10, results.size()); // 5 unit * 2 samples = 10
		assertEquals("" +
				"s1_1",
				results.get(0).getSample().getSampleId().toString());
	}

	@Test
	public void sample_filters_() {
		ListQuery query = new ListQuery(privateQuery().setBase(Base.SAMPLE).build(), 1, 100);
		query.getFilters().setKeyword("skpart");
		query.getFilters().setSampleId(new Qname("s1_5"));

		query.getFilters().setSampleFact("sf1");
		query.getFilters().setSampleFact("sf1=sf1v");
		query.getFilters().setSampleFact("sf2=sf2v");
		query.getFilters().setSampleFact("sf3=3.4/"); // value is 5.0
		query.getFilters().setSampleFact("sf3=-1/20"); // value is 5.0
		query.getFilters().setSampleFact(new Qname("MY.sf4").toURI() +"="+new Qname("MY.sf4EnumV").toURI());
		query.getFilters().setSampleFact(new Qname("MY.sf4").toURI() +"="+new Qname("MY.sf4EnumV").toString());
		query.getFilters().setSampleFact(new Qname("MY.sf4").toString() +"="+new Qname("MY.sf4EnumV").toURI());
		query.getFilters().setSampleFact(new Qname("MY.sf4").toString() +"="+new Qname("MY.sf4EnumV").toString());

		query.getFilters().setSampleCollectionId(new Qname("HR.2"));
		query.getFilters().setSampleMultiple(true);
		query.getFilters().setSampleType(new Qname("stype"));
		query.getFilters().setSampleQuality(new Qname("q"));
		query.getFilters().setSampleStatus(new Qname("s"));
		query.getFilters().setSampleMaterial(new Qname("mat"));

		query.getFilters().setIdentificationBasis(new Qname("MY.identificationBasisDNA"));

		List<JoinedRow> results = verticaDao.getQueryDAO().getList(query).getResults();
		assertEquals(1, results.size());
	}

	@Test
	public void sample_count() {
		CountQuery query = new CountQuery(privateQuery().setBase(Base.SAMPLE).build());
		assertEquals(10L, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void collectionAndRecordQuality() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(true).build());
		query.getFilters().setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.PROFESSIONAL).setRecordQuality(RecordQuality.NEUTRAL).setRecordQuality(RecordQuality.UNCERTAIN));
		query.getFilters().setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.AMATEUR).setRecordQuality(RecordQuality.ERRONEOUS));
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void partition() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.getFilters().setPartition(new Partition("1/1"));
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void permissionFilters() throws NoSuchFieldException {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());

		query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.setPermissionFilters(Filters.createEmptyFilters(query.getWarehouse(), query.getBase(), false));
		query.getPermissionFilters().setDocumentId(new Qname("A.12345_1"));
		query.getPermissionFilters().setDocumentId(new Qname("A.12345_2"));
		assertEquals(8, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setDocumentId(new Qname("A.12345_2"));
		query.getFilters().setDocumentId(new Qname("A.12345_3"));
		assertEquals(4, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTime("1800/1801");
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getPermissionFilters().setTaxonId(new Qname("MX.28715"));
		query.getPermissionFilters().setUseIdentificationAnnotations(false);
		query.getPermissionFilters().setCollectionQuality(CollectionQuality.PROFESSIONAL);
		query.getPermissionFilters().setInformalTaxonGroupIdIncludingReported(new Qname("MVL.3"));
		query.getPermissionFilters().setNamedPlaceTag(new Qname("nptag1")); // this matches
		verticaDao.getQueryDAO().getCount(query).getTotal(); // test joins are added -> does not cause exception

		CountQuery annQuery = new CountQuery(privateQuery().setDefaultFilters(false).setBase(Base.ANNOTATION).build());
		annQuery.setPermissionFilters(query.getPermissionFilters());
		verticaDao.getQueryDAO().getCount(annQuery);

		AggregatedQuery annAggQuery = new AggregatedQuery(privateQuery().setDefaultFilters(false).setBase(Base.ANNOTATION).build(), new AggregateBy("unit.annotations.annotationByPerson"), 1, 10);
		annAggQuery.setPermissionFilters(query.getPermissionFilters());
		verticaDao.getQueryDAO().getAggregate(annAggQuery);

		CountQuery gatQuery = new CountQuery(privateQuery().setDefaultFilters(false).setBase(Base.GATHERING).build());
		gatQuery.setPermissionFilters(query.getPermissionFilters());
		verticaDao.getQueryDAO().getCount(gatQuery);

		CountQuery docQuery = new CountQuery(privateQuery().setDefaultFilters(false).setBase(Base.DOCUMENT).build());
		docQuery.setPermissionFilters(query.getPermissionFilters());
		verticaDao.getQueryDAO().getCount(docQuery);
	}

	@Test
	public void taxonStatusOr() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		query.setPermissionFilters(Filters.createEmptyFilters(query.getWarehouse(), query.getBase(), false));

		query.getFilters().setTime("1900/2040");
		query.getFilters().setAdministrativeStatusId(new Qname("MX.finlex160_1997_appendix4"));
		query.getFilters().setAdministrativeStatusId(new Qname("MX.finlex160_1997_appendix4_specialInterest"));
		query.getFilters().setRedListStatusId(new Qname("MX.iucnLC"));
		query.getFilters().setRedListStatusId(new Qname("MX.iucnCR"));

		query.getPermissionFilters().setCountryId(Const.FINLAND);
		query.getPermissionFilters().setAdministrativeStatusId(new Qname("MX.finlex160_1997_appendix4"));
		query.getPermissionFilters().setAdministrativeStatusId(new Qname("MX.finlex160_1997_appendix4_specialInterest"));
		query.getPermissionFilters().setRedListStatusId(new Qname("MX.iucnLC"));
		query.getPermissionFilters().setRedListStatusId(new Qname("MX.iucnCR"));

		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTaxonAdminFiltersOperator(Operator.OR);
		query.getPermissionFilters().setTaxonAdminFiltersOperator(Operator.OR);
		assertEquals(5, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getPermissionFilters().getAdministrativeStatusId().clear();
		query.getPermissionFilters().setAdministrativeStatusId(new Qname("foobar"));
		assertEquals(5, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getPermissionFilters().getRedListStatusId().clear();
		query.getPermissionFilters().setRedListStatusId(new Qname("fibar"));
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void hasValuesFilter() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		Filters filters = query.getFilters();
		filters.setHasValue("unit.plantStatusCode");
		filters.setHasValue("gathering.conversions.euref.latMax");
		filters.setHasValue("gathering.interpretations.countryDisplayname");
		filters.setHasValue("document.namedPlaceId");
		assertEquals(5, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void hasValuesFilterForAllFields() {
		Set<String> fields = new TreeSet<>();
		fields.addAll(Fields.selectable(Base.UNIT).getAllFields());
		fields.addAll(Fields.aggregatable(Base.UNIT).getAllFields());
		fields.addAll(Fields.sortable(Base.UNIT).getAllFields());

		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		Filters filters = query.getFilters();

		for (String fieldName : fields) {
			try {
				Filters.validateHasValueFilterField(fieldName, filters);
				filters.setHasValue(fieldName);
			} catch (Exception e) {
				// Do not test with non-valid field
			}
		}
		verticaDao.getQueryDAO().getCount(query).getTotal();
	}

	@Test
	public void prefixFilter() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		Filters filters = query.getFilters();
		filters.setDocumentIdPrefix("foobar");
		filters.setDocumentIdPrefix("A");
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void createdDateAggregateAndFilter() throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(privateQuery().setDefaultFilters(false).build(), new AggregateBy("document.createdDateMonth", "document.collectionId"), 1, 10);
		query.getFilters().setCreatedDateYear(Integer.valueOf(DateUtils.getCurrentDateTime("yyyy")));
		List<AggregateRow> results = verticaDao.getQueryDAO().getAggregate(query).getResults();
		results.forEach(r->System.out.println(r));
		assertEquals(1, results.size());
		AggregateRow r = results.get(0);
		assertEquals(20, r.getCount().intValue());
		assertEquals(Integer.valueOf(DateUtils.getCurrentDateTime("MM")), r.getAggregateByValues().get(0));
	}

	@Test
	public void stateLandFilter() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		Filters filters = query.getFilters();
		filters.setOnlyNonStateLands(true);
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());

		filters.setOnlyNonStateLands(false);
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void virvaFilter() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());
		Filters filters = query.getFilters();

		filters.setTaxonAdminFiltersOperator(Operator.OR);
		for (Qname adminStatus : Taxon.VIRVA_ADMIN_STATUSES) {
			filters.setAdministrativeStatusId(adminStatus);
		}
		for (Qname redListStatus : Taxon.VIRVA_RED_LIST_STATUSES) {
			filters.setRedListStatusId(redListStatus);
		}

		filters.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.PROFESSIONAL)
				.setRecordQuality(RecordQuality.EXPERT_VERIFIED)
				.setRecordQuality(RecordQuality.COMMUNITY_VERIFIED)
				.setRecordQuality(RecordQuality.NEUTRAL)
				.setRecordQuality(RecordQuality.UNCERTAIN));
		filters.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.HOBBYIST)
				.setRecordQuality(RecordQuality.EXPERT_VERIFIED)
				.setRecordQuality(RecordQuality.COMMUNITY_VERIFIED)
				.setRecordQuality(RecordQuality.NEUTRAL));
		filters.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.AMATEUR)
				.setRecordQuality(RecordQuality.EXPERT_VERIFIED)
				.setRecordQuality(RecordQuality.COMMUNITY_VERIFIED));

		System.out.println("XXX - should use taxon virva boolean");
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal()); // In loaded test data there are no virva taxa, so this test can not cause results -- this test can be used to check manually what the generated sql looks like (and it runs without exception)

		System.out.println("XXX - should not use taxon virva boolean");
		filters.setAdministrativeStatusId(new Qname("MX.otherPlantPest"));
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void taxonSearchWithSameRankMoreThanOnceInSearchParentChain() {
		// MX.4973478 == Raunucuus -group species - parent chain has two taxa with aggregate rank: MX.37908 and MX.4972569 -- above those is genus MX.37894
		// MX.4973479 == same as above, different species

		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());

		System.out.println(" XXX -- genus search: should search for all taxa with genus_key");
		query.getFilters().setTaxonId(new Qname("MX.37894"));
		verticaDao.getQueryDAO().getCount(query);

		System.out.println(" XXX -- higher of the aggregates -- should search for all taxa with aggregate_key");
		query.getFilters().getTaxonId().clear();
		query.getFilters().setTaxonId(new Qname("MX.4972569"));
		verticaDao.getQueryDAO().getCount(query);

		System.out.println(" XXX -- lower of the aggregates -- should search for all taxa with MX.37908 and all its children (two) so in total the IN statement should have 3 keys");
		query.getFilters().getTaxonId().clear();
		query.getFilters().setTaxonId(new Qname("MX.37908"));
		verticaDao.getQueryDAO().getCount(query);

		System.out.println(" XXX -- species - should search for all taxa with species_key");
		query.getFilters().getTaxonId().clear();
		query.getFilters().setTaxonId(new Qname("MX.4973478"));
		verticaDao.getQueryDAO().getCount(query);

		// This test does not return any results with any of these taxa, so we are only checking what the queries look like
	}

	@Test
	public void timeAccuracy() {
		CountQuery query = new CountQuery(privateQuery().setDefaultFilters(false).build());

		// Time in test doc is 5.1.2015 - 10.1.2015  (6 days)
		query.getFilters().setTimeAccuracy(10);
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTimeAccuracy(6);
		assertEquals(20, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTimeAccuracy(5);
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTimeAccuracy(1);
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());

		query.getFilters().setTimeAccuracy(0);
		assertEquals(0, verticaDao.getQueryDAO().getCount(query).getTotal());
	}

	@Test
	public void editorOrObserverId() {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();
		Filters filters = baseQuery.getFilters();

		filters.setEditorOrObserverId(new Qname("MA.5")); // -- this matches
		ListResponse results = verticaDao.getQueryDAO().getList(
				new ListQuery(baseQuery, 1, 100));
		assertEquals(20, results.getResults().size());
	}

	@Test
	public void editorOrObserverIdNotIsAccessibleForSourceOnPublic() {
		BaseQuery baseQuery = publicQuery().setApiSourceIdCanUseEditorOrObserverIdIsNot(true).build();
		Filters filters = baseQuery.getFilters();

		filters.setEditorOrObserverIdIsNot(new Qname("MA.5"));
		ListResponse results = verticaDao.getQueryDAO().getList(
				new ListQuery(baseQuery, 1, 100));
		assertEquals(0, results.getResults().size());
	}

	@Test
	public void hidingPersonStuffForDataRequests_list() throws NoSuchFieldException {
		BaseQuery baseQuery = privateQuery().setDefaultFilters(false).build();
		baseQuery.setApprovedDataRequest(false); // no person hiding
		ListQuery listQuery = new ListQuery(baseQuery, 1, 100);
		listQuery.setSelected(new Selected().add("document").add("gathering")); // select all fields to enable linkings

		ListResponse results = verticaDao.getQueryDAO().getList(listQuery);
		assertEquals(20, results.getResults().size());

		for (JoinedRow row : results.getResults()) {
			assertEquals("[Meikäläinen, Matti, Dare Talvitie, Tapani Lahti]", row.getGathering().getTeam().toString());
			assertEquals("[f1 : f1v, f2 : 1,5, F1 : F1V, email : xxx@xxx.xxx, http://tun.fi/MZ.phoneNumber : 123456789]", row.getDocument().getFacts().toString());
			assertEquals("[extiirai, http://tun.fi/MA.5]", row.getDocument().getEditorUserIds().toString());
			assertEquals("[Person [id=MA.2, userId=http://tun.fi/MA.2, fullName=Tapani Lahti], Person [id=MA.5, userId=hatikka:Esko.Piirainen@hatikka.fi, fullName=Esko Piirainen], Person [id=MA.1, userId=http://tun.fi/MA.1, fullName=Dare Talvitie]]",
					row.getGathering().getLinkings().getObservers().toString());
		}

		listQuery.setApprovedDataRequest(true); // hide person info
		results = verticaDao.getQueryDAO().getList(listQuery);
		assertEquals(20, results.getResults().size());

		for (JoinedRow row : results.getResults()) {
			assertEquals("[]", row.getGathering().getTeam().toString());
			assertEquals("[f1 : f1v, f2 : 1,5, F1 : F1V]", row.getDocument().getFacts().toString()); // email, phone is removed
			assertEquals("[]", row.getDocument().getEditorUserIds().toString());
			assertEquals("[]", row.getGathering().getLinkings().getObservers().toString());
		}
	}

	@Test
	public void hidingPersonStuffForDataRequests_single() {
		Document document = verticaDao.getQueryDAO().get(new Qname("A.12345_2"), false); // no person hiding

		assertEquals("[Meikäläinen, Matti, Dare Talvitie, Tapani Lahti]", document.getGatherings().get(0).getTeam().toString());
		assertEquals("[f1 : f1v, f2 : 1,5, F1 : F1V, email : xxx@xxx.xxx, http://tun.fi/MZ.phoneNumber : 123456789]", document.getFacts().toString());
		assertEquals("[extiirai, http://tun.fi/MA.5]", document.getEditorUserIds().toString());
		assertEquals("[Person [id=MA.2, userId=http://tun.fi/MA.2, fullName=Tapani Lahti], Person [id=MA.5, userId=hatikka:Esko.Piirainen@hatikka.fi, fullName=Esko Piirainen], Person [id=MA.1, userId=http://tun.fi/MA.1, fullName=Dare Talvitie]]",
				document.getGatherings().get(0).getLinkings().getObservers().toString());

		document = verticaDao.getQueryDAO().get(new Qname("A.12345_2"), true); // hide person info

		assertEquals("[]", document.getGatherings().get(0).getTeam().toString());
		assertEquals("[f1 : f1v, f2 : 1,5, F1 : F1V]", document.getFacts().toString());
		assertEquals("[]", document.getEditorUserIds().toString());
		assertEquals("[]", document.getGatherings().get(0).getLinkings().getObservers().toString());
	}

}