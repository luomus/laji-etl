package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.StatelessSession;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAOImpleTests.AlmostRealDao;
import fi.laji.datawarehouse.dao.ETLDAO.PipeSearchParams;
import fi.laji.datawarehouse.dao.oracle.InPipeDataEntity;
import fi.laji.datawarehouse.dao.oracle.InPipeMetaEntity;
import fi.laji.datawarehouse.dao.oracle.SequenceStore;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import jersey.repackaged.com.google.common.collect.Lists;

public class ETLDAOImpleTests {

	private static ETLDAO dao;
	private static AlmostRealDao topDao;

	@BeforeClass
	public static void init() {
		topDao = new AlmostRealDao(TestConfig.getConfig(), DAOImpleTests.class);
		dao = topDao.getETLDAO();
	}

	@AfterClass
	public static void close() {
		if (topDao != null) topDao.close();
	}

	@Test
	public void reprocessTemp() {
		Set<String> ids = new HashSet<>();
		while (ids.size() < 1000) {
			ids.add(Utils.generateGUID());
		}
		dao.clearReprocessDocumentIds();
		dao.storeReprocessDocumentIds(ids);
	}

	@Test
	public void annotations() throws Exception {
		Qname id = new Qname(Utils.generateGUID());
		Qname documentId = new Qname(Utils.generateGUID());
		Set<Qname> documentIds = Utils.set(documentId, new Qname("justsomeotherid"));
		Annotation a = new Annotation(id, documentId, new Qname("foo"), DateUtils.getCurrentEpoch());

		assertEquals(0, dao.getAnnotations(documentIds).size());
		assertEquals(null, dao.getExistingAnnotationInPipeId(id));

		dao.storeAnnotation(a, 6);

		assertEquals(1, dao.getAnnotations(documentIds).size());
		assertEquals(6, dao.getExistingAnnotationInPipeId(id).intValue());

		a.setDeleted(true);
		dao.storeAnnotation(a, 14);

		assertEquals(1, dao.getAnnotations(documentIds).size());
		assertEquals(14, dao.getExistingAnnotationInPipeId(id).intValue());

		dao.getTopAnnotations(10);
	}

	@Test
	public void loadTimeService() throws Exception {
		List<fi.laji.datawarehouse.etl.models.dw.Document> documents = new ArrayList<>();
		Qname id1 = createId("JA.123");
		Qname id2 = createId("JA.124");
		Qname id3 = createId("luomus:JA.123");
		documents.add(createDocument(id1));
		documents.add(createDocument(id2));
		documents.add(createDocument(id3));

		Map<Qname, Long> times = dao.getFirstLoadTimes(documents);
		assertEquals(0, times.size());

		times.put(id1, 123456L);
		dao.storeFirstLoadedTimes(times);

		times = dao.getFirstLoadTimes(documents);
		assertEquals(1, times.size());
		assertEquals(123456L, times.get(id1).longValue());

		try {
			dao.storeFirstLoadedTimes(times);
		} catch (Exception e) {
			assertEquals("Storing first loaded times {"+id1.toString()+"=123456}", e.getMessage());
		}

		times.clear();
		times.put(id3, 7890123L);
		dao.storeFirstLoadedTimes(times);

		times = dao.getFirstLoadTimes(documents);
		assertEquals(2, times.size());
		assertEquals(123456L, times.get(id1).longValue());
		assertEquals(7890123L, times.get(id3).longValue());
	}

	private Qname createId(String string) {
		return new Qname(string+"_"+DateUtils.getCurrentEpoch());
	}

	private Document createDocument(Qname id) throws CriticalParseFailure {
		return new fi.laji.datawarehouse.etl.models.dw.Document(Concealment.PRIVATE, new Qname("KE.1"), id, new Qname("HR.1"));
	}


	private void truncateTable(String table) {
		try (StatelessSession session = dao.getETLEntityConnection()) {
			session.getTransaction().begin();
			session.createSQLQuery("TRUNCATE TABLE " + table).executeUpdate();
			session.getTransaction().commit();
		}
	}

	@Test
	@SuppressWarnings("deprecation")
	public void firstLoadTimes() throws CriticalParseFailure {
		truncateTable("firstloaddates");
		Map<Qname, Long> firstLoadTimesOfDocumentIds = new HashMap<>();
		while (firstLoadTimesOfDocumentIds.size() < 1000) {
			firstLoadTimesOfDocumentIds.put(new Qname(Utils.generateGUID()), DateUtils.getCurrentEpoch());
		}
		dao.storeFirstLoadedTimes(firstLoadTimesOfDocumentIds);


		Document d = new Document();
		d.setDocumentId(firstLoadTimesOfDocumentIds.keySet().iterator().next());

		long fromDb = dao.getFirstLoadTimes(Utils.list(d)).get(d.getDocumentId());
		assertEquals(firstLoadTimesOfDocumentIds.get(d.getDocumentId()).longValue(), fromDb);

		List<Document> searchBy = new ArrayList<>();
		for (Qname documentId : firstLoadTimesOfDocumentIds.keySet()) {
			d = new Document();
			d.setDocumentId(documentId);
			searchBy.add(d);
			if (searchBy.size() >= 500) break;
		}
		while (searchBy.size() < 2000) {
			d = new Document();
			d.setDocumentId(new Qname(Utils.generateGUID()));
			searchBy.add(d);
		}
		Map<Qname, Long> batch = dao.getFirstLoadTimes(searchBy);
		assertEquals(500, batch.size());
	}

	@Test
	public void notificationQueue() throws CriticalParseFailure {
		truncateTable("notification");

		assertEquals(0, dao.getUnsentNotifications().size());

		Annotation a1 = new Annotation(new Qname("a1"), new Qname("r1"), new Qname("t1"), DateUtils.getEpoch(1, 1, 2020));
		dao.storeNotification(new AnnotationNotification(new Qname("p1"), a1, NotificationReason.MY_DOCUMENT_ANNOTATED));

		assertEquals(1, dao.getUnsentNotifications().size());
		assertEquals("[AnnotationNotification [personId=p1, annotation=Annotation [id=a1], notificationReason=MY_DOCUMENT_ANNOTATED]]",
				dao.getUnsentNotifications().toString());

		dao.storeNotification(new AnnotationNotification(new Qname("p1"), a1, NotificationReason.MY_DOCUMENT_ANNOTATED));
		assertEquals(1, dao.getUnsentNotifications().size()); // same annotation id and person id

		dao.storeNotification(new AnnotationNotification(new Qname("p2"), a1, NotificationReason.MY_DOCUMENT_ANNOTATED));
		assertEquals(2, dao.getUnsentNotifications().size());

		long id = dao.getUnsentNotifications().stream().filter(n->n.getPersonId().toString().equals("p1")).findFirst().get().getId();
		dao.markNotificationSent(id);
		dao.markNotificationSent(-1);
		assertEquals(1, dao.getUnsentNotifications().size());
		assertEquals("p2", dao.getUnsentNotifications().get(0).getPersonId().toString());

		dao.removeUnsentNotifications(new Qname("a1"));
		dao.removeUnsentNotifications(new Qname("foobar"));
		assertEquals(0, dao.getUnsentNotifications().size());

		dao.storeNotification(new AnnotationNotification(new Qname("p1"), a1, NotificationReason.MY_DOCUMENT_ANNOTATED));
		assertEquals(0, dao.getUnsentNotifications().size());
		dao.storeNotification(new AnnotationNotification(new Qname("p2"), a1, NotificationReason.MY_DOCUMENT_ANNOTATED));
		assertEquals(1, dao.getUnsentNotifications().size());

		dao.storeNotification(new AnnotationNotification(new Qname("p3"), a1, null));
		assertEquals("AnnotationNotification [personId=p3, annotation=Annotation [id=a1], notificationReason=null]", dao.getUnsentNotifications().stream().filter(n->n.getPersonId().toString().equals("p3")).findFirst().get().toString());
	}

	@Test
	public void splittedDocumentIds() {
		truncateTable("splitted_documentid");
		assertEquals("{}", dao.getSplittedDocumentIds(Utils.set(new Qname("foo"))).toString());
		assertEquals(null, dao.getOriginalIdsOfSplittedDocument(new Qname("foo")));

		SplittedDocumentIds ids = dao.getOrGenerateSplittedDocumentIds(new Qname("docid"), Utils.list(new Qname("u1"), new Qname("u2")));
		assertNull(ids.getSplittedDocumentIdFor(new Qname("foo")));
		assertNull(ids.getSplittedDocumentIdFor(new Qname("foo"), new Qname("u1")));
		assertNull(ids.getSplittedDocumentIdFor(new Qname("docid"), new Qname("foo")));
		Qname u1Splitted = ids.getSplittedDocumentIdFor(new Qname("u1"));
		Qname u2Splitted = ids.getSplittedDocumentIdFor(new Qname("docid"), new Qname("u2"));
		assertNotNull(u1Splitted);
		assertNotNull(u2Splitted);
		assertEquals(2, ids.getSplittedDocumentIdsFor(new Qname("docid")).size());
		assertEquals(0, ids.getSplittedDocumentIdsFor(new Qname("foo")).size());

		ids = dao.getOrGenerateSplittedDocumentIds(new Qname("docid"), Utils.list(new Qname("u1")));
		assertEquals(2, ids.getSplittedDocumentIdsFor(new Qname("docid")).size());
		assertEquals(u1Splitted, ids.getSplittedDocumentIdFor(new Qname("u1")));
		assertEquals(u2Splitted, ids.getSplittedDocumentIdFor(new Qname("u2")));

		assertEquals("OriginalIds [documentId=docid, unitId=u1]", dao.getOriginalIdsOfSplittedDocument(u1Splitted).toString());
		assertNull(dao.getOriginalIdsOfSplittedDocument(new Qname("foo")));
	}

	@Test
	public void splittedDocumentQueue() throws Exception {
		truncateTable("splitted_document_queue");

		assertEquals(0, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());

		dao.removeSplittedQueue(Utils.list(-1L, -2L));
		dao.removeSplittedDocumentsFromQueue(Utils.set(new Qname("foo")));

		dao.storeSplittedDocumentToQueue(new Qname("s1"), new Qname("docid1"), "data1");
		dao.storeSplittedDocumentToQueue(new Qname("s1"), new Qname("docid1"), "data2");
		dao.storeSplittedDocumentToQueue(new Qname("s1"), new Qname("docid2"), "data3");
		dao.storeSplittedDocumentToQueue(new Qname("s2"), new Qname("docid3"), "data4");
		Thread.sleep(1100);

		List<QueueData> data = dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1"));
		assertEquals(3, data.size()); // expire time is immediate in dev
		assertEquals(0, dao.getExpiredSplittedDocumentsFromQueue(new Qname("foo")).size());
		assertEquals("s1", data.get(0).getSource().toString());
		assertEquals("data1", data.get(0).getData());
		assertEquals("data2", data.get(1).getData());
		assertEquals("data3", data.get(2).getData());

		dao.removeSplittedQueue(Utils.list(data.get(0).getId()));
		assertEquals(2, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());
		dao.removeSplittedDocumentsFromQueue(Utils.set(new Qname("foo")));
		assertEquals(2, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());
		dao.removeSplittedDocumentsFromQueue(Utils.set(new Qname("foo"), new Qname("docid1")));
		assertEquals(1, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());
		dao.removeSplittedDocumentsFromQueue(Utils.set(new Qname("docid2")));
		assertEquals(0, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());

		List<String> datas = new ArrayList<>();
		while (datas.size() < 110) {
			String s = Utils.generateGUID();
			datas.add(s);
			dao.storeSplittedDocumentToQueue(new Qname("s1"), new Qname("docid1"), s);
		}
		Thread.sleep(1100);
		assertEquals(100, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s1")).size());
		assertEquals(1, dao.getExpiredSplittedDocumentsFromQueue(new Qname("s2")).size());
	}

	@Test
	public void pipeSequence() {
		// For this test to work add the following sequence to test database:
		// CREATE SEQUENCE test_seq INCREMENT BY 10;
		SequenceStore store = new SequenceStore("test_seq", 10);

		long prevFirst = 0;
		try (StatelessSession ses = dao.getETLEntityConnection()) {
			long first = store.getVal(ses); // 1
			System.out.println("ret " +first);
			prevFirst = first;
			assertTrue(first > 0);
			for (int i = 1; i<=20; i++) {
				long next = store.getVal(ses);
				System.out.println("ret " +next);
				assertEquals(i, next-first); // 2-20
			}
		}
		try (StatelessSession ses = dao.getETLEntityConnection()) {
			long first = store.getVal(ses); // 21
			System.out.println("ret " +first);
			assertEquals(21, first-prevFirst);
			for (int i = 1; i<=50; i++) {
				long next = store.getVal(ses);
				System.out.println("ret " +next);
				assertEquals(i, next-first); // 22-
				assertEquals(i+21, next-prevFirst); // 22-
			}
		}
	}

	@Test
	public void bigInData() {
		String bigData = createBigData();
		dao.storeToInPipe(new Qname("KE.1"), bigData, "text/plain");
		dao.storeToInPipe(new Qname("KE.1"), bigData, "text/plain");
		dao.storeToInPipe(new Qname("KE.1"), bigData, "text/plain");
	}

	private String createBigData() {
		StringBuilder b = new StringBuilder();
		while (b.length() < 120000) {
			b.append(Utils.generateGUID()).append(" äöåÄÖÅ ♀♂ русский алфавит ");
		}
		return b.toString();
	}

	@Test
	public void bigOutData() {
		emptyTable("out_pipe_data");
		emptyTable("out_pipe");
		emptyTable("in_pipe_data");
		emptyTable("in_pipe");

		Qname sourceId = new Qname("KE.1");
		Qname collectionId = new Qname("HR.1");

		setUpInPipeIds(sourceId);

		String bigData = createBigData();
		List<OutPipeData> data = new ArrayList<>(101);
		int i = 0;
		while (data.size() < 100) {
			data.add(new OutPipeData(-1, 1, sourceId, new Qname("JX."+(i++)), collectionId, null, bigData, false, new Date(), null));
		}
		dao.storeToOutPipe(data);
	}

	@Test
	public void inPipeStoreOnHold() {
		emptyTable("out_pipe_data");
		emptyTable("out_pipe");
		emptyTable("in_pipe_data");
		emptyTable("in_pipe");

		Qname source = new Qname("KE.1");

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());

		dao.storeToInPipeOnHold(source, Utils.list("data1", "data2"), "text/plain");
		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
		assertEquals(2, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());

		dao.releaseInPipeHold(source);
		assertEquals(2, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
	}

	@Test
	public void inPipeHandling() {
		emptyTable("splitted_document_queue");
		emptyTable("out_pipe_data");
		emptyTable("out_pipe");
		emptyTable("in_pipe_data");
		emptyTable("in_pipe");

		Qname source = new Qname("KE.1");

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(null, dao.getFromInPipe(1));
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());
		assertEquals(0, dao.getUnprosessedSources().size());

		dao.storeToInPipe(source, "data", "application/json");

		List<InPipeData> unprocessed = dao.getUnprocessedNotErroneousInOrderFromInPipe(source);
		assertEquals(1, unprocessed.size());
		InPipeData data = unprocessed.get(0);
		String expected = "InPipeData [contentType=application/json, unrecoverableError=false] PipeData [id=XXX, source=KE.1, data=data, timestamp=null, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId());
		assertEquals(expected, data.toString());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());

		InPipeData byId = dao.getFromInPipe(unprocessed.get(0).getId());
		expected = "InPipeData [contentType=application/json, unrecoverableError=false] PipeData [id=XXX, source=KE.1, data=data, timestamp=YYY, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId())
				.replace("YYY", byId.getTimestamp().toString());
		assertEquals(expected, byId.toString());
		List<InPipeData> onlyMeta = dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED);
		assertEquals(1, onlyMeta.size());
		String expectedLimited = "InPipeData [contentType=null, unrecoverableError=false] PipeData [id=XXX, source=KE.1, data=null, timestamp=YYY, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId())
				.replace("YYY", ""+onlyMeta.get(0).getTimestamp());
		assertEquals(expectedLimited, onlyMeta.get(0).toString());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());
		assertEquals("["+source+"]", dao.getUnprosessedSources().toString());

		dao.reportAttempted(Utils.list(data), "No success but will retry");

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
		List<InPipeData> failed = dao.getFailedAttemptDataFromInPipe(source);
		assertEquals(1, failed.size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());

		dao.reportPermantentyFailed(failed, "Will never succeed");
		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());

		dao.markReprocessInPipe(failed.get(0).getId());
		unprocessed = dao.getUnprocessedNotErroneousInOrderFromInPipe(source);
		assertEquals(1, unprocessed.size());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());

		dao.reportProcessed(unprocessed);
		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromInPipe(source).size());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());

		Qname otherSource = new Qname("KE.2");
		dao.storeToInPipe(otherSource, "other source data", "text/plain");
		dao.storeToInPipe(source, "other data", "application/json");

		unprocessed = dao.getUnprocessedNotErroneousInOrderFromInPipe(otherSource);
		assertEquals(1, unprocessed.size());
		data = unprocessed.get(0);
		expected = "InPipeData [contentType=text/plain, unrecoverableError=false] PipeData [id=XXX, source=KE.2, data=other source data, timestamp=null, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId());
		assertEquals(expected, data.toString());

		unprocessed = dao.getUnprocessedNotErroneousInOrderFromInPipe(source);
		data = unprocessed.get(0);

		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(2, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());
		List<Qname> sources = new ArrayList<>(dao.getUnprosessedSources());
		Collections.sort(sources);
		assertEquals("["+source+", "+otherSource+"]", sources.toString());

		dao.removeInPipe(data.getId());
		assertEquals(0, dao.getFailedAttemptDataFromInPipe(source).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromInPipeForSource(source, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromInPipeForSource(source, PipeSearchParams.IN_ERROR).size());
		assertEquals("["+otherSource+"]", dao.getUnprosessedSources().toString());

		List<String> batch = new ArrayList<>();
		for (int i = 0; i<=20; i++) {
			batch.add("data " + i);
		}
		dao.storeToInPipe(source, batch, "application/json");
		List<InPipeData> datas = dao.getUnprocessedNotErroneousInOrderFromInPipe(source);
		assertEquals(10, datas.size()); // fetches top 10

		dao.reportAttempted(datas, "all failed");
		assertEquals(1, dao.getFailedAttemptDataFromInPipe(source).size()); // fetches top 1
	}

	@Test
	public void storeToOutPipeDuplicateHandling() {
		emptyTable("out_pipe_data");
		emptyTable("out_pipe");
		emptyTable("in_pipe_data");
		emptyTable("in_pipe");

		Qname documentId = new Qname("JA.1");
		Qname sourceId = new Qname("KE.1");
		Qname collectionId = new Qname("HR.1");

		setUpInPipeIds(sourceId);

		List<OutPipeData> datas = new ArrayList<>();
		assertEquals(false, dao.storeToOutPipe(datas));


		// Store with in pipe id 1
		datas.add(new OutPipeData(-1, 1, sourceId, documentId, collectionId, null, "testdata 1", false));
		assertEquals(true, dao.storeToOutPipe(datas));

		// It exists
		OutPipeData existing = dao.getFromOutPipe(documentId);
		assertEquals(1, existing.getInPipeId());

		// Store same again
		assertEquals(true, dao.storeToOutPipe(datas));

		// Still exists
		existing = dao.getFromOutPipe(documentId);
		assertEquals(1, existing.getInPipeId());

		// Store with in pipe id 1 and 2
		datas.add(new OutPipeData(-1, 2, sourceId, documentId, collectionId, null, "testdata 2", false));
		assertEquals(true, dao.storeToOutPipe(datas));

		// Higher pipe id data has overriden
		existing = dao.getFromOutPipe(documentId);
		assertEquals(2, existing.getInPipeId());

		// Same document id in same batch with same in pipe id
		datas.add(new OutPipeData(-1, 2, sourceId, documentId, collectionId, null, "testdata 3", false));
		assertEquals(true, dao.storeToOutPipe(datas));

		// Pipe id 2 data is stored but if it is "testdata 2" or "testdata 3" is not defined: same document should not be twice in same in pipe batch and it is not possible to know which is the newer info
		existing = dao.getFromOutPipe(documentId);
		assertEquals(2, existing.getInPipeId());

		// Store with lower pipe id than existing
		datas.clear();
		datas.add(new OutPipeData(-1, 1, sourceId, documentId, collectionId, null, "testdata 4", false));
		assertEquals(false, dao.storeToOutPipe(datas));

		// Higher pipe id still exists
		existing = dao.getFromOutPipe(documentId);
		assertEquals(2, existing.getInPipeId());

	}

	private void setUpInPipeIds(Qname sourceId) {
		// In pipe has to have rows with inpipeids used in out pipe tests (1, 2) because there is foreign key outpipe.inpipeid -> inpipe.id
		try (StatelessSession con = dao.getETLEntityConnection()) {
			con.beginTransaction();
			InPipeMetaEntity meta1 = new InPipeMetaEntity();
			InPipeMetaEntity meta2 = new InPipeMetaEntity();
			meta1.setId(1);
			meta2.setId(2);
			meta1.setSource(sourceId.toString());
			meta2.setSource(sourceId.toString());
			meta1.setProcessed(1);
			meta2.setProcessed(1);
			meta1.setTimestamp(new Date());
			meta2.setTimestamp(new Date());
			con.insert(meta1);
			con.insert(meta2);
			InPipeDataEntity data1 = new InPipeDataEntity();
			InPipeDataEntity data2 = new InPipeDataEntity();
			data1.setId(meta1.getId());
			data2.setId(meta2.getId());
			data1.setSource(meta1.getSource());
			data2.setSource(meta2.getSource());
			data1.setContentType("text/plain");
			data2.setContentType("text/plain");
			data1.setData("data");
			data2.setData("data");
			con.insert(data1);
			con.insert(data2);
			con.getTransaction().commit();
		}
	}

	@Test
	public void outPipeHandling() {
		emptyTable("splitted_document_queue");
		emptyTable("out_pipe_data");
		emptyTable("out_pipe");
		emptyTable("in_pipe_data");
		emptyTable("in_pipe");

		Qname sourceId = new Qname("KE.1");
		Qname collectionId = new Qname("HR.1");
		Qname documentId = new Qname("JX.1");
		Qname namedPlaceId = new Qname("NP.1");

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId).size());
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		assertEquals(null, dao.getFromOutPipe(1));
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());
		assertEquals(0, dao.getUnprosessedSources().size());

		setUpInPipeIds(sourceId);

		OutPipeData data = new OutPipeData(-1, 1, sourceId, documentId, collectionId, namedPlaceId, "testdata 1", false);
		SplittedDocumentIds splitted = dao.getOrGenerateSplittedDocumentIds(data.getDocumentId(), Utils.list(new Qname("U.1")));
		dao.storeToOutPipe(Utils.list(data));

		List<OutPipeData> unprocessed = dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId);
		assertEquals(1, unprocessed.size());
		data = unprocessed.get(0);
		String expected = "OutPipeData [documentId=JX.1, inPipeId=0, splittedDocumentIds=null, deletion=false, collectionId=null, namedPlaceId=null] PipeData [id=XXX, source=null, data=testdata 1, timestamp=null, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId());
		assertEquals(expected, data.toString());
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		OutPipeData byId = dao.getFromOutPipe(unprocessed.get(0).getId());
		expected = "OutPipeData [documentId=JX.1, inPipeId=1, splittedDocumentIds=ZZZ, deletion=false, collectionId=HR.1, namedPlaceId=NP.1] PipeData [id=XXX, source=KE.1, data=testdata 1, timestamp=YYY, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId())
				.replace("YYY", byId.getTimestamp().toString())
				.replace("ZZZ", splitted.getSplittedDocumentIdsFor(data.getDocumentId()).toString());
		assertEquals(expected, byId.toString());
		List<OutPipeData> onlyMeta = dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED);
		assertEquals(1, onlyMeta.size());
		String expectedOnlyMeta = "OutPipeData [documentId=JX.1, inPipeId=1, splittedDocumentIds=ZZZ, deletion=false, collectionId=null, namedPlaceId=null] PipeData [id=XXX, source=KE.1, data=null, timestamp=YYY, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId())
				.replace("YYY", onlyMeta.get(0).getTimestamp().toString())
				.replace("ZZZ", splitted.getSplittedDocumentIdsFor(data.getDocumentId()).toString());
		assertEquals(expectedOnlyMeta, onlyMeta.get(0).toString());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());
		assertEquals("["+sourceId+"]", dao.getUnprosessedSources().toString());

		dao.reportAttempted(Utils.list(data), "Error message");

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId).size());
		List<OutPipeData> failed = dao.getFailedAttemptDataFromOutPipe(sourceId);
		assertEquals(1, failed.size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());

		dao.markReprocessOutPipe(failed.get(0).getId());
		unprocessed = dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId);
		assertEquals(1, unprocessed.size());
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());

		dao.reportProcessed(unprocessed);
		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId).size());
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());

		Qname otherSourceId = new Qname("KE.2");
		dao.storeToOutPipe(Utils.list(new OutPipeData(-1, 1, otherSourceId, new Qname("JX.2"), collectionId, namedPlaceId, "testdata 2", false)));
		dao.storeToOutPipe(Utils.list(new OutPipeData(-1, 1, sourceId, new Qname("JX.3"), collectionId, namedPlaceId, "testdata 3", false)));

		unprocessed = dao.getUnprocessedNotErroneousInOrderFromOutPipe(otherSourceId);
		assertEquals(1, unprocessed.size());
		data = unprocessed.get(0);
		expected = "OutPipeData [documentId=JX.2, inPipeId=0, splittedDocumentIds=null, deletion=false, collectionId=null, namedPlaceId=null] PipeData [id=XXX, source=null, data=testdata 2, timestamp=null, errorMessage=null, attemptCount=0]"
				.replace("XXX", ""+data.getId());
		assertEquals(expected, data.toString());

		unprocessed = dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId);
		data = unprocessed.get(0);

		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		assertEquals(2, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());
		List<Qname> sources = new ArrayList<>(dao.getUnprosessedSources());
		Collections.sort(sources);
		assertEquals("["+sourceId+", "+otherSourceId+"]", sources.toString());

		dao.removeOutPipe(data.getId());
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.RECEIVED).size());
		assertEquals(1, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.PROCESSED).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.WAITING).size());
		assertEquals(0, dao.getFromOutPipeForSource(sourceId, PipeSearchParams.IN_ERROR).size());
		assertEquals("["+otherSourceId+"]", dao.getUnprosessedSources().toString());

		assertEquals(0, dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId).size());
		assertEquals(1, dao.getUnprocessedNotErroneousInOrderFromOutPipe(otherSourceId).size());
		dao.markReprocessOutPipeByNamedPlaceIds(Utils.list(namedPlaceId, new Qname("foo")));
		assertEquals(1, dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId).size());
		assertEquals(1, dao.getUnprocessedNotErroneousInOrderFromOutPipe(otherSourceId).size());
		sources = new ArrayList<>(dao.getUnprosessedSources());
		Collections.sort(sources);
		assertEquals("["+sourceId+", "+otherSourceId+"]", sources.toString());

		List<OutPipeData> batch = new ArrayList<>();
		for (int i = 10; i<=130; i++) {
			boolean deletion = (i % 10 == 0);
			batch.add(new OutPipeData(-1, 1, sourceId, new Qname("JX."+i), collectionId, null, "data " + i, deletion));
		}
		for (List<OutPipeData> subBatch : Lists.partition(batch, 100)) {
			dao.storeToOutPipe(subBatch);
		}
		List<OutPipeData> datas = dao.getUnprocessedNotErroneousInOrderFromOutPipe(sourceId);
		assertEquals(100, datas.size()); // fetches top 100
		dao.reportAttempted(datas, "all failed");
		assertEquals(40, dao.getFailedAttemptDataFromOutPipe(sourceId).size()); // fetches top 1
		dao.markOutPipeErroneousForReattempt();
		assertEquals(0, dao.getFailedAttemptDataFromOutPipe(sourceId).size());

		dao.reportAttempted(datas, "all failed");
		int i = datas.size();
		for (OutPipeData d : datas) {
			if (i-- <= 5) break; // mark all except 5 for reprocessing
			dao.markReprocessOutPipe(d.getDocumentId());
		}
		assertEquals(5, dao.getFailedAttemptDataFromOutPipe(sourceId).size());

		dao.reportProcessed(datas.subList(0, 50));

		Date d = dao.getLatestTimestampFromOutPipe();
		assertEquals(DateUtils.getCurrentDate(), DateUtils.format(d, "yyyy-MM-dd"));

		String expectedStats = "PipeStats "+
				"[in={KE.1=SourcePipeStats [processed=2, waiting=0, inError=0, deleted=0], KE.2=SourcePipeStats [processed=0, waiting=0, inError=0, deleted=0]}, "+
				"out={KE.1=SourcePipeStats [processed=50, waiting=67, inError=5, deleted=6], KE.2=SourcePipeStats [processed=0, waiting=1, inError=0, deleted=0]}]";
		assertEquals(expectedStats, dao.getPipeStats(true).toString());

		assertEquals(1, dao.removeUnlinkedInPipeData(sourceId));
		assertEquals(0, dao.removeUnlinkedInPipeData(sourceId));
		dao.removeOutPipe(dao.getFromOutPipe(new Qname("JX.33")).getId());
		assertEquals(0, dao.removeUnlinkedInPipeData(sourceId)); // still referenced by JX.1--JX.120 outpipe entries
	}

	private void emptyTable(String table) {
		try (StatelessSession session = dao.getETLEntityConnection()) {
			session.getTransaction().begin();
			session.createSQLQuery("DELETE FROM " + table).executeUpdate();
			session.getTransaction().commit();
		}
	}

	@Test
	public void unprossedSources() {
		dao.getUnprosessedSources();
	}

}
