package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fi.laji.CoordinateConverterProj4jImple;
import fi.laji.DegreePoint;
import fi.laji.datawarehouse.dao.MediaDAOStub;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.harmonizers.BaseHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.Fmnh2008Harmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.Fmnh2008HarmonizerTests;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiETLJSONHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.LajistoreHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizer;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class ConverterTests {

	private static TestDAO dao;
	private static Interpreter interpreter;
	private static Securer securer;
	private static Converter converter;

	@BeforeClass
	public static void init() {
		dao = new TestDAO(ConverterTests.class);
		interpreter = new Interpreter(dao);
		securer = new Securer(dao);
		converter = new Converter(dao);
	}

	@AfterClass
	public static void close() {
		dao.close();
	}

	@Test
	public void test_coordinate_conversion() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(6666, 3333, Type.YKJ));

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		assertEquals(1000, dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinateAccuracy().intValue());

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(60.071776, conversions.getWgs84().getLatMin(), 0.00001);
		assertEquals(23.996125, conversions.getWgs84().getLonMin(), 0.00001);
		assertEquals(60.081146, conversions.getWgs84().getLatMax(), 0.00001);
		assertEquals(24.014876, conversions.getWgs84().getLonMax(), 0.00001);
		assertEquals(Type.WGS84, conversions.getWgs84().getType());
		assertEquals(1000, conversions.getWgs84().getAccuracyInMeters().intValue());

		assertEquals(6666, conversions.getYkj1km().getLat(), 0);
		assertEquals(3333, conversions.getYkj1km().getLon(), 0);
		assertEquals(666, conversions.getYkj10km().getLat(), 0);
		assertEquals(333, conversions.getYkj10km().getLon(), 0);
		assertEquals(665, conversions.getYkj50km().getLat(), 0);
		assertEquals(330, conversions.getYkj50km().getLon(), 0);
		assertEquals(66, conversions.getYkj100km().getLat(), 0);
		assertEquals(33, conversions.getYkj100km().getLon(), 0);
		assertEquals(Type.YKJ, conversions.getYkj10km().getType());
		assertEquals(Type.YKJ, conversions.getYkj1km().getType());

		assertEquals(6666, conversions.getYkj1kmCenter().getLat(), 0);
		assertEquals(3333, conversions.getYkj1kmCenter().getLon(), 0);
		assertEquals(666, conversions.getYkj10kmCenter().getLat(), 0);
		assertEquals(333, conversions.getYkj10kmCenter().getLon(), 0);
		assertEquals(665, conversions.getYkj50kmCenter().getLat(), 0);
		assertEquals(330, conversions.getYkj50kmCenter().getLon(), 0);
		assertEquals(66, conversions.getYkj100kmCenter().getLat(), 0);
		assertEquals(33, conversions.getYkj100kmCenter().getLon(), 0);
		assertEquals(Type.YKJ, conversions.getYkj10kmCenter().getType());
		assertEquals(Type.YKJ, conversions.getYkj1kmCenter().getType());

		assertEquals(6666000, conversions.getYkj().getLatMin(), 0);
		assertEquals(6667000, conversions.getYkj().getLatMax(), 0);
		assertEquals(3333000, conversions.getYkj().getLonMin(), 0);
		assertEquals(3334000, conversions.getYkj().getLonMax(), 0);
		assertEquals(1000, conversions.getYkj().getAccuracyInMeters().intValue());

		assertEquals(6663201, conversions.getEuref().getLatMin(), 0);
		assertEquals(6664201, conversions.getEuref().getLatMax(), 0);
		assertEquals(332898, conversions.getEuref().getLonMin(), 0);
		assertEquals(333897, conversions.getEuref().getLonMax(), 0);
		assertEquals(1000, conversions.getEuref().getAccuracyInMeters().intValue());

		assertEquals(60.076461, conversions.getWgs84CenterPoint().getLat(), 0.0000001);
		assertEquals(24.005503, conversions.getWgs84CenterPoint().getLon(), 0.0000001);

		assertEquals(60, conversions.getWgs84Grid1().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid1().getLon(), 0);

		assertEquals(60, conversions.getWgs84Grid05().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid05().getLon(), 0);

		assertEquals(60, conversions.getWgs84Grid01().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid01().getLon(), 0);

		assertEquals(60.05, conversions.getWgs84Grid005().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid005().getLon(), 0);

		assertEquals(1000000, conversions.getBoundingBoxAreaInSquareMeters().intValue());

		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getGeo());
		assertEquals("POLYGON ((3333000 6666000, 3333000 6667000, 3334000 6667000, 3334000 6666000, 3333000 6666000))", conversions.getYkjGeo().getWKT());
		assertEquals("POLYGON ((332898 6663201, 332898 6664201, 333897 6664201, 333897 6663201, 332898 6663201))", conversions.getEurefGeo().getWKT());
		assertEquals("POLYGON ((23.996941 60.071776, 23.996125 60.08074, 24.014065 60.081146, 24.014876 60.072183, 23.996941 60.071776))", conversions.getWgs84Geo().getWKT());

		assertEquals(
				"{\"type\":\"FeatureCollection\",\"crs\":\"EUREF\",\"accuracyInMeters\":1000,\"features\":[{\"type\":\"Feature\",\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[332898,6663201],[333897,6663201],[333897,6664201],[332898,6664201],[332898,6663201]]]}}]}",
				conversions.getEurefGeo().getGeoJSON().toString());

		assertEquals("ML.1091", conversions.getBirdAssociationArea().toString());
	}

	@Test
	public void test_coordinate_conversion__grids_for_innaccurate() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(66, 33, Type.YKJ));

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();

		assertEquals(59, conversions.getWgs84Grid1().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid1().getLon(), 0);

		assertEquals(59.5, conversions.getWgs84Grid05().getLat(), 0);
		assertEquals(24, conversions.getWgs84Grid05().getLon(), 0);

		assertEquals(null, conversions.getWgs84Grid01());
		assertEquals(null, conversions.getWgs84Grid005());

	}

	@Test
	public void test_coordinate_conversion_ykj_point() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(6665475, 3336575, Type.YKJ));

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(1, conversions.getBoundingBoxAreaInSquareMeters().intValue());
	}

	@Test
	public void grid05() {
		assertEquals(-1.5, GatheringConversions.toGrid05(-1.1), 0);
		assertEquals(-1, GatheringConversions.toGrid05(-1), 0);
		assertEquals(-1, GatheringConversions.toGrid05(-0.51), 0);
		assertEquals(-0.5, GatheringConversions.toGrid05(-0.5), 0);
		assertEquals(-0.5, GatheringConversions.toGrid05(-0.49), 0);
		assertEquals(-0.5, GatheringConversions.toGrid05(-0.3), 0);
		assertEquals(-0.5, GatheringConversions.toGrid05(-0.01), 0);
		assertEquals(0, GatheringConversions.toGrid05(-0), 0);
		assertEquals(0, GatheringConversions.toGrid05(0), 0);
		assertEquals(0, GatheringConversions.toGrid05(0.01), 0);
		assertEquals(0, GatheringConversions.toGrid05(0.3), 0);
		assertEquals(0, GatheringConversions.toGrid05(0.49), 0);
		assertEquals(0.5, GatheringConversions.toGrid05(0.5), 0);
		assertEquals(0.5, GatheringConversions.toGrid05(0.51), 0);
		assertEquals(0.5, GatheringConversions.toGrid05(0.8), 0);
		assertEquals(1, GatheringConversions.toGrid05(1), 0);
		assertEquals(1, GatheringConversions.toGrid05(1.1), 0);
	}

	@Test
	public void grid01() {
		assertEquals(0, GatheringConversions.toGrid01(0), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.0001), 0);
		assertEquals(0.1, GatheringConversions.toGrid01(0.1), 0);
		assertEquals(0.1, GatheringConversions.toGrid01(0.12), 0);
		assertEquals(0.1, GatheringConversions.toGrid01(0.15), 0);
		assertEquals(0.1, GatheringConversions.toGrid01(0.19), 0);
		assertEquals(0.2, GatheringConversions.toGrid01(0.2), 0);
		assertEquals(0.2, GatheringConversions.toGrid01(0.21), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.09), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.01), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.05), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.049), 0);
		assertEquals(0, GatheringConversions.toGrid01(0.051), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.00001), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.1), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.09), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.01), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.05), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.049), 0);
		assertEquals(-0.1, GatheringConversions.toGrid01(-0.051), 0);
	}

	@Test
	public void grid005() {
		assertEquals(0, GatheringConversions.toGrid005(0), 0);
		assertEquals(0, GatheringConversions.toGrid005(0.0001), 0);
		assertEquals(0.1, GatheringConversions.toGrid005(0.1), 0);
		assertEquals(0.1, GatheringConversions.toGrid005(0.12), 0);
		assertEquals(0.15, GatheringConversions.toGrid005(0.15), 0);
		assertEquals(0.15, GatheringConversions.toGrid005(0.19), 0);
		assertEquals(0.2, GatheringConversions.toGrid005(0.2), 0);
		assertEquals(0.2, GatheringConversions.toGrid005(0.21), 0);
		assertEquals(0.05, GatheringConversions.toGrid005(0.09), 0);
		assertEquals(0, GatheringConversions.toGrid005(0.01), 0);
		assertEquals(0.05, GatheringConversions.toGrid005(0.05), 0);
		assertEquals(0, GatheringConversions.toGrid005(0.049), 0);
		assertEquals(0.05, GatheringConversions.toGrid005(0.051), 0);
		assertEquals(-0.05, GatheringConversions.toGrid005(-0.00001), 0);
		assertEquals(-0.1, GatheringConversions.toGrid005(-0.1), 0);
		assertEquals(-0.1, GatheringConversions.toGrid005(-0.09), 0);
		assertEquals(-0.05, GatheringConversions.toGrid005(-0.01), 0);
		assertEquals(-0.05, GatheringConversions.toGrid005(-0.05), 0);
		assertEquals(-0.05, GatheringConversions.toGrid005(-0.049), 0);
		assertEquals(-0.10, GatheringConversions.toGrid005(-0.051), 0);
		assertEquals(-9.20, GatheringConversions.toGrid005(-9.151000), 0);
		assertEquals(-9.15, GatheringConversions.toGrid005(-9.150000), 0);
		assertEquals(-9.15, GatheringConversions.toGrid005(-9.133333), 0);
		assertEquals(-9.15, GatheringConversions.toGrid005(-9.1001), 0);
		assertEquals(-9.10, GatheringConversions.toGrid005(-9.1000), 0);

		assertEquals(5.50, GatheringConversions.toGrid005(5.524750), 0);
		assertEquals(5.55, GatheringConversions.toGrid005(5.566667), 0);

	}

	@Test
	public void test_foreign_place() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(-66, 0.63221, Type.WGS84));

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(-66, conversions.getWgs84().getLatMin(), 0.00000001);
		assertEquals(0.63221, conversions.getWgs84().getLonMin(), 0.00000001);
		assertEquals(-66, conversions.getWgs84().getLatMax(), 0.00000001);
		assertEquals(0.63221, conversions.getWgs84().getLonMax(), 0.00000001);
		assertEquals(Type.WGS84, conversions.getWgs84().getType());

		assertEquals(null, conversions.getYkj1km());
		assertEquals(null, conversions.getYkj10km());
		assertEquals(null, conversions.getYkj10kmCenter());
		assertEquals(null, conversions.getYkj1kmCenter());
		assertEquals(null, conversions.getYkj());

		assertEquals(-66, conversions.getWgs84CenterPoint().getLat(), 0.0000001);
		assertEquals(0.63221, conversions.getWgs84CenterPoint().getLon(), 0.0000001);

		assertEquals("POINT (0.63221 -66)", conversions.getWgs84Geo().getWKT());
		assertEquals(null, conversions.getYkjGeo());
		assertEquals(null, conversions.getEurefGeo());

		assertEquals(null, conversions.getBoundingBoxAreaInSquareMeters());
	}

	@Test
	public void converting_times() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setEventDate(new DateRange(DateUtils.convertToDate("01.05.2013"), DateUtils.convertToDate("05.05.2013")));
		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(2000, conversions.getCentury().intValue());
		assertEquals(2010, conversions.getDecade().intValue());
		assertEquals(2013, conversions.getYear().intValue());
		assertEquals(5, conversions.getMonth().intValue());
		assertEquals(null, conversions.getDay());
		assertEquals(501, conversions.getSeasonBegin().intValue());
		assertEquals(505, conversions.getSeasonEnd().intValue());
		assertEquals(121, conversions.getDayOfYearBegin().intValue());
		assertEquals(125, conversions.getDayOfYearEnd().intValue());
	}

	@Test
	public void converting_times_not_from_same_month() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setEventDate(new DateRange(DateUtils.convertToDate("01.05.2013"), DateUtils.convertToDate("05.06.2013")));
		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(2000, conversions.getCentury().intValue());
		assertEquals(2010, conversions.getDecade().intValue());
		assertEquals(2013, conversions.getYear().intValue());
		assertEquals(null, conversions.getMonth());
		assertEquals(null, conversions.getDay());
		assertEquals(501, conversions.getSeasonBegin().intValue());
		assertEquals(605, conversions.getSeasonEnd().intValue());
		assertEquals(121, conversions.getDayOfYearBegin().intValue());
		assertEquals(156, conversions.getDayOfYearEnd().intValue());
	}

	@Test
	public void converting_times_not_from_same_year_winter_overlap() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setEventDate(new DateRange(DateUtils.convertToDate("01.12.2013"), DateUtils.convertToDate("05.02.2014")));
		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(2000, conversions.getCentury().intValue());
		assertEquals(2010, conversions.getDecade().intValue());
		assertEquals(null, conversions.getYear());
		assertEquals(null, conversions.getMonth());
		assertEquals(null, conversions.getDay());
		assertEquals(1201, conversions.getSeasonBegin().intValue());
		assertEquals(205, conversions.getSeasonEnd().intValue());
		assertEquals(335, conversions.getDayOfYearBegin().intValue());
		assertEquals(36, conversions.getDayOfYearEnd().intValue());
	}

	@Test
	public void converting_times_not_from_same_year_end_overlap() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setEventDate(new DateRange(DateUtils.convertToDate("01.05.2013"), DateUtils.convertToDate("05.06.2014")));
		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(2000, conversions.getCentury().intValue());
		assertEquals(2010, conversions.getDecade().intValue());
		assertEquals(null, conversions.getYear());
		assertEquals(null, conversions.getMonth());
		assertEquals(null, conversions.getDay());
		assertEquals(null, conversions.getSeasonBegin());
		assertEquals(null, conversions.getSeasonEnd());
		assertEquals(null, conversions.getDayOfYearBegin());
		assertEquals(null, conversions.getDayOfYearEnd());
	}

	@Test
	public void converting_times_not_from_same_year_2() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setEventDate(new DateRange(DateUtils.convertToDate("01.05.2013"), DateUtils.convertToDate("05.06.2015")));
		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		GatheringConversions conversions = dwRoot.getPublicDocument().getGatherings().get(0).getConversions();
		assertEquals(2000, conversions.getCentury().intValue());
		assertEquals(2010, conversions.getDecade().intValue());
		assertEquals(null, conversions.getYear());
		assertEquals(null, conversions.getMonth());
		assertEquals(null, conversions.getDay());
		assertEquals(null, conversions.getSeasonBegin());
		assertEquals(null, conversions.getSeasonEnd());
		assertEquals(null, conversions.getDayOfYearBegin());
		assertEquals(null, conversions.getDayOfYearEnd());
	}

	@Test
	public void test_coordinate_conversion_from_municipality() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setMunicipality("Helsinki");

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		Gathering gathering = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("Helsinki", gathering.getMunicipality());
		assertEquals(new Qname("ML.660"), gathering.getInterpretations().getFinnishMunicipality());
		assertEquals("[ML.660]", gathering.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals(59.922482, gathering.getInterpretations().getCoordinates().getLatMin().doubleValue(), 0.001);
		assertEquals(24.782792, gathering.getInterpretations().getCoordinates().getLonMin().doubleValue(), 0.001);
		assertEquals("6646855.0 : 6689449.0 : 3376175.0 : 3403654.0 : YKJ : 25000", gathering.getConversions().getYkj().toString());
		assertEquals(null, gathering.getConversions().getYkj100km());
		assertEquals(null, gathering.getConversions().getYkj50km());
		assertEquals(null, gathering.getConversions().getYkj10km());
		assertEquals(null, gathering.getConversions().getYkj1km());
		assertEquals("66.0:33.0:YKJ", gathering.getConversions().getYkj100kmCenter().toString());
		assertEquals("665.0:335.0:YKJ", gathering.getConversions().getYkj50kmCenter().toString());
		assertEquals("666.0:338.0:YKJ", gathering.getConversions().getYkj10kmCenter().toString());
		assertEquals("6668.0:3389.0:YKJ", gathering.getConversions().getYkj1kmCenter().toString());
	}

	@Test
	public void test_coordinate_conversion_from_large_ykj_grid() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(6681, 6682, 3332, 3337, Type.YKJ));

		interpreter.interpret(dwRoot);
		converter.convert(dwRoot);

		Gathering gathering = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(6681, gathering.getInterpretations().getCoordinates().getLatMin().doubleValue(), 0.001);
		assertEquals(3332, gathering.getInterpretations().getCoordinates().getLonMin().doubleValue(), 0.001);
		assertEquals(6681000, gathering.getConversions().getYkj().getLatMin().intValue());
		assertEquals(6682000, gathering.getConversions().getYkj().getLatMax().intValue());
		assertEquals(3332000, gathering.getConversions().getYkj().getLonMin().intValue());
		assertEquals(3337000, gathering.getConversions().getYkj().getLonMax().intValue());
		assertEquals(66, gathering.getConversions().getYkj100km().getLat().intValue());
		assertEquals(33, gathering.getConversions().getYkj100km().getLon().intValue());
		assertEquals(665, gathering.getConversions().getYkj50km().getLat().intValue());
		assertEquals(330, gathering.getConversions().getYkj50km().getLon().intValue());
		assertEquals(668, gathering.getConversions().getYkj10km().getLat().intValue());
		assertEquals(333, gathering.getConversions().getYkj10km().getLon().intValue());
		assertEquals(null, gathering.getConversions().getYkj1km());
		assertEquals(null, gathering.getConversions().getYkj1km());
		assertEquals("66.0:33.0:YKJ", gathering.getConversions().getYkj100kmCenter().toString());
		assertEquals("665.0:330.0:YKJ", gathering.getConversions().getYkj50kmCenter().toString());
		assertEquals("668.0:333.0:YKJ", gathering.getConversions().getYkj10kmCenter().toString());
		assertEquals("6681.0:3334.0:YKJ", gathering.getConversions().getYkj1kmCenter().toString());

		assertEquals("6681000.0 : 6682000.0 : 3332000.0 : 3337000.0 : YKJ : 5000", gathering.getConversions().getYkj().toString());

		assertEquals(5000000, gathering.getConversions().getBoundingBoxAreaInSquareMeters().intValue());

		assertEquals("60.205812 : 60.216806 : 23.965813 : 24.056687 : WGS84 : 5000", gathering.getConversions().getWgs84().toString());

		assertEquals("60.2:24.0:WGS84", gathering.getConversions().getWgs84Grid005().toString());
		assertEquals("60.2:24.0:WGS84", gathering.getConversions().getWgs84Grid01().toString());
		assertEquals("60.0:24.0:WGS84", gathering.getConversions().getWgs84Grid05().toString());
		assertEquals("60.0:24.0:WGS84", gathering.getConversions().getWgs84Grid1().toString());
		assertEquals("60.211316:24.011255:WGS84", gathering.getConversions().getWgs84CenterPoint().toString());

		assertEquals("6678195.0 : 6679195.0 : 331898.0 : 336896.0 : EUREF : 5000", gathering.getConversions().getEuref().toString());

		assertEquals("[POLYGON ((3332000 6681000, 3332000 6682000, 3337000 6682000, 3337000 6681000, 3332000 6681000))]", gathering.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POLYGON ((331898 6678195, 331898 6679195, 336896 6679195, 336896 6678195, 331898 6678195))]", gathering.getConversions().getEurefGeo().getWKTList().toString());
		assertEquals("[POLYGON ((23.966641 60.205812, 23.965813 60.214774, 24.055883 60.216806, 24.056687 60.207842, 23.966641 60.205812))]", gathering.getConversions().getWgs84Geo().getWKTList().toString());
	}

	@Test
	public void test_coordinate_conversion_from_large_wgs84_grid() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(38.450719,-9.090514, Type.WGS84));
		dwRoot.getPublicDocument().setSecureLevel(SecureLevel.KM100);
		dwRoot.getPublicDocument().addSecureReason(SecureReason.CUSTOM);

		interpreter.interpret(dwRoot);
		assertEquals("38.450719 : 38.450719 : -9.090514 : -9.090514 : WGS84 : 1", dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinates().toString());
		assertEquals(1, dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinateAccuracy().intValue());

		securer.secure(dwRoot);
		assertEquals("38.0 : 39.0 : -10.0 : -9.0 : WGS84 : 100000", dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinates().toString());
		assertEquals(100000, dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinateAccuracy().intValue());

		converter.convert(dwRoot);

		Gathering gathering = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("38.0 : 39.0 : -10.0 : -9.0 : WGS84 : 100000", gathering.getInterpretations().getCoordinates().toString());
		assertEquals("38.0 : 39.0 : -10.0 : -9.0 : WGS84 : 100000", gathering.getConversions().getWgs84().toString());
		assertEquals(100000, gathering.getInterpretations().getCoordinateAccuracy().intValue());

		assertEquals(null, gathering.getConversions().getYkj());
		assertEquals(null, gathering.getConversions().getYkj100km());
		assertEquals(null, gathering.getConversions().getEuref());
		assertEquals(null, gathering.getConversions().getBoundingBoxAreaInSquareMeters());

		assertEquals(null, gathering.getConversions().getWgs84Grid005());
		assertEquals(null, gathering.getConversions().getWgs84Grid01());
		assertEquals("38.5:-9.5:WGS84", gathering.getConversions().getWgs84Grid05().toString());
		assertEquals("38.0:-10.0:WGS84", gathering.getConversions().getWgs84Grid1().toString());
		assertEquals("38.5:-9.5:WGS84", gathering.getConversions().getWgs84CenterPoint().toString());


		assertEquals(null, gathering.getConversions().getEurefGeo());
		assertEquals(null, gathering.getConversions().getYkjGeo());
		assertEquals("[POLYGON ((-10 38, -10 39, -9 39, -9 38, -10 38))]", gathering.getConversions().getWgs84Geo().getWKTList().toString());
	}


	@Test
	public void coordinate_values() throws DataValidationException {
		try {
			new Coordinates(670, 939, Type.YKJ);
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid YKJ coordinates (Too large value: 9390000): 670 : 671 : 939 : 940 : YKJ", e.getMessage());
		}

		try {
			new Coordinates(670, 39, Type.YKJ);
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid YKJ coordinates (YKJ coordinates must be reported using same precision): 670 : 671 : 39 : 40 : YKJ", e.getMessage());
		}

		new Coordinates(670, 339, Type.YKJ);
	}

	@Test
	public void convbugtesting() throws Exception {
		DwRoot root = JsonToModel.rootFromJson(new JSONObject(getTestData("etl-schema-conv-test.json")));
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		assertEquals("65.0 : 65.0 : 25.316667 : 25.316667 : WGS84 : 1000", g.getCoordinates().toString());

		interpreter.interpret(root);
		assertEquals("65.0 : 65.0 : 25.316667 : 25.316667 : WGS84 : 1000", g.getInterpretations().getCoordinates().toString());
		assertEquals(1000, g.getInterpretations().getCoordinateAccuracy().intValue());
		converter.convert(root);
		assertEquals("7212529.0 : 7212530.0 : 3420767.0 : 3420768.0 : YKJ : 1000", g.getConversions().getYkj().toString());
	}

	public static String getTestData(String filename) throws Exception {
		URL url = ConverterTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void convbugtesting2() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G.1"));
		Document d = root.createPublicDocument();
		d.addGathering(g);
		String geojson = "{\"type\":\"FeatureCollection\",\"crs\":\"WGS84\",\"features\":[{\"type\":\"Feature\",\"geometry\":{\"type\":\"Polygon\",\"coordinates\":"+
				"[[[-58.361692,-34.604742],[-58.356972,-34.601987],[-58.352251,-34.60446],[-58.350878,-34.610182],[-58.350105,-34.616893],[-58.352938,-34.619294],[-58.357487,-34.619294],[-58.361692,-34.604742]]]}}]}";
		g.setGeo(Geo.fromGeoJSON(new JSONObject(geojson)));

		interpreter.interpret(root);
		assertEquals(-34.619294, g.getInterpretations().getCoordinates().getLatMin(), 0);
		assertEquals(-34.601987, g.getInterpretations().getCoordinates().getLatMax(), 0);
		assertEquals(-58.361692, g.getInterpretations().getCoordinates().getLonMin(), 0);
		assertEquals(-58.350105, g.getInterpretations().getCoordinates().getLonMax(), 0);
		assertEquals(1000, g.getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void interpret_secure_convert_bugtesting() throws Exception {
		fi.luomus.commons.xml.Document doc = Fmnh2008HarmonizerTests.getTestData("fmnh2008-multiline.xml");
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(doc, new Qname("KE.8"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);

		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());

		long originalUnitsCount = root.getPublicDocument().getGatherings().stream().mapToInt(g -> g.getUnits().size()).sum();
		assertEquals(23, originalUnitsCount);

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering splitG = root.getPublicDocument().getGatherings().get(0);
		assertEquals(null, splitG.getCoordinates());
		String geoAsWkt = "["+
				"LINESTRING (3383131 6674050, 3383151 6673874), "+
				"LINESTRING (3383107 6673874, 3383055 6673842, 3383007 6673830, 3382971 6673794, 3382991 6673754, 3383015 6673710, 3383023 6673686, 3382939 6673702, 3382907 6673714, 3382859 6673722, 3382835 6673730), "+
				"LINESTRING (3383147 6673874, 3383111 6673870), "+
				"LINESTRING (3382831 6673730, 3382819 6673662, 3382811 6673606, 3382795 6673550, 3382787 6673506, 3382771 6673458, 3382759 6673406, 3382743 6673382, 3382711 6673342, 3382711 6673290, 3382711 6673202, 3382775 6672770, 3382835 6672666, 3382863 6672638, 3382863 6672546, 3382859 6672510), "+
				"LINESTRING (3382867 6672499, 3382943 6672407, 3382975 6672387, 3382979 6672351, 3382975 6672319, 3382939 6672319, 3382887 6672319, 3382843 6672315, 3382839 6672299, 3382795 6672259, 3382759 6672247, 3382723 6672247, 3382595 6672371, 3382527 6672423, 3382407 6672423, 3382327 6672403, 3382259 6672347, 3382167 6672315, 3382139 6672327, 3382139 6672391, 3382159 6672403, 3382159 6672435, 3382199 6672443, 3382227 6672475, 3382259 6672491, 3382307 6672495, 3382347 6672523, 3382363 6672535, 3382351 6672639, 3382271 6672999, 3382215 6673307, 3382191 6673419, 3382139 6673467, 3382119 6673575, 3382119 6673739), "+
				"POINT (3382118 6673732), "+
				"LINESTRING (3382110 6673728, 3382046 6673720, 3382002 6673692, 3381910 6673648, 3381838 6673592, 3381766 6673572, 3381702 6673568, 3381634 6673568, 3381594 6673580, 3381570 6673520, 3381566 6673480, 3381522 6673496, 3381482 6673532, 3381442 6673552, 3381390 6673480, 3381322 6673436, 3381278 6673384, 3381242 6673380, 3381174 6673456, 3381166 6673472, 3381206 6673516, 3381266 6673552, 3381290 6673588, 3381314 6673668, 3381318 6673728, 3381350 6673776, 3381406 6673784, 3381386 6673864, 3381362 6673968, 3381342 6674132, 3381318 6674272), "+
				"LINESTRING (3381314 6674266, 3381290 6674426, 3381258 6674582, 3381234 6674706, 3381222 6674822, 3381238 6674886, 3381238 6674970, 3381250 6675050, 3381278 6675138, 3381282 6675082, 3381298 6675026, 3381334 6675006, 3381382 6674974, 3381386 6674946, 3381350 6674882, 3381330 6674842, 3381326 6674750, 3381346 6674686, 3381366 6674598, 3381390 6674542, 3381414 6674486, 3381502 6674450, 3381614 6674382, 3381674 6674386, 3381734 6674378, 3381802 6674318, 3381826 6674258, 3381826 6674230, 3381830 6674190, 3381830 6674138, 3381894 6674122, 3381970 6674098, 3382030 6674090, 3382130 6674062, 3382190 6674042), "+
				"LINESTRING (3382199 6674042, 3382223 6674086, 3382271 6674102, 3382359 6674102, 3382375 6674170, 3382375 6674226, 3382391 6674282, 3382399 6674310, 3382447 6674314, 3382511 6674338, 3382543 6674358, 3382603 6674382, 3382655 6674390, 3382751 6674390, 3382851 6674358, 3382915 6674302, 3382991 6674246, 3383075 6674194, 3383127 6674098, 3383139 6674050, 3383143 6674030)]";
		assertEquals(geoAsWkt, splitG.getGeo().getWKTList().toString());
		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());

		interpreter.interpret(root);
		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());
		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("6672247.0 : 6675138.0 : 3381166.0 : 3383151.0 : YKJ : 5000", splitG.getInterpretations().getCoordinates().toString());
		assertEquals(5000, splitG.getInterpretations().getCoordinateAccuracy().intValue());

		securer.secure(root);
		assertNotNull(root.getPublicDocument());
		assertNotNull(root.getPrivateDocument());
		Document splittedDoc = root.getSplittedPublicDocuments().get(0);

		long securedUnitsCount = root.getPublicDocument().getGatherings().stream().mapToInt(g -> g.getUnits().size()).sum();
		securedUnitsCount += root.getSplittedPublicDocuments().stream().flatMap(document -> document.getGatherings().stream()).mapToInt(g -> g.getUnits().size()).sum();
		assertEquals(originalUnitsCount, securedUnitsCount);

		assertEquals(1, root.getSplittedPublicDocuments().size());

		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals(SecureLevel.NONE, root.getPrivateDocument().getSecureLevel());
		assertEquals(SecureLevel.KM10, splittedDoc.getSecureLevel()); // merikotka at Southern Finland during breeding season (25.2.) -> 10km

		assertEquals(0, root.getPublicDocument().getSecureReasons().size());
		assertEquals(Utils.list(SecureReason.BREEDING_SEASON_TAXON_CONSERVATION), root.getPrivateDocument().getSecureReasons());

		splitG = splittedDoc.getGatherings().get(0);
		assertEquals("667.0 : 668.0 : 338.0 : 339.0 : YKJ : 10000", splitG.getInterpretations().getCoordinates().toString());
		assertEquals(null, splitG.getGeo());
		assertEquals(10000, splitG.getInterpretations().getCoordinateAccuracy().intValue());

		Gathering privG = root.getPrivateDocument().getGatherings().get(0);
		assertEquals("6672247.0 : 6675138.0 : 3381166.0 : 3383151.0 : YKJ : 5000", privG.getInterpretations().getCoordinates().toString());
		assertEquals(geoAsWkt, privG.getGeo().getWKTList().toString());
		assertEquals(5000, privG.getInterpretations().getCoordinateAccuracy().intValue());

		Gathering pubG = root.getPublicDocument().getGatherings().get(0);
		assertEquals("6672247.0 : 6675138.0 : 3381166.0 : 3383151.0 : YKJ : 5000", pubG.getInterpretations().getCoordinates().toString());
		assertEquals(geoAsWkt, pubG.getGeo().getWKTList().toString());
		assertEquals(5000, pubG.getInterpretations().getCoordinateAccuracy().intValue());

		converter.convert(root);
		assertNotNull(root.getPublicDocument());
		assertNotNull(root.getPrivateDocument());

		GatheringConversions splitC = splitG.getConversions();
		GatheringConversions pubC = pubG.getConversions();
		GatheringConversions privC = privG.getConversions();

		assertEquals(null, splitC.getDay());
		assertEquals(null, splitC.getYkj1km());
		assertEquals("60.15:24.9:WGS84", splitC.getWgs84Grid005().toString());
		assertEquals("POLYGON ((3380000 6670000, 3380000 6680000, 3390000 6680000, 3390000 6670000, 3380000 6670000))", splitC.getYkjGeo().getWKT());

		assertEquals(10828, pubC.getLinelengthInMeters().intValue());
		assertEquals(10828, privC.getLinelengthInMeters().intValue());

		assertEquals(25, privC.getDay().intValue());
		assertEquals(null, privC.getYkj1km());
		assertEquals("60.15:24.85:WGS84", privC.getWgs84Grid005().toString());
		assertEquals(geoAsWkt, privC.getYkjGeo().getWKTList().toString());

		assertEquals(25, pubC.getDay().intValue());
		assertEquals(null, pubC.getYkj1km());
		assertEquals("60.15:24.85:WGS84", pubC.getWgs84Grid005().toString());
		assertEquals(geoAsWkt, pubC.getYkjGeo().getWKTList().toString());

		assertEquals(23, privG.getUnits().size());
		assertEquals(22, pubG.getUnits().size());
		assertEquals(1, splitG.getUnits().size());
	}

	@Test
	public void interpret_secure_convert_bugtesting_2() throws Exception {
		JSONObject data = new JSONObject(getTestData("etl-schema-conv-test2.json"));
		List<DwRoot> harmonized = new LajiETLJSONHarmonizer().harmonize(data, new Qname("KE.67"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);

		Gathering privG = root.getPrivateDocument().getGatherings().get(0);
		assertEquals("6845769.0 : 6845770.0 : 3641151.0 : 3641152.0 : YKJ : 1", privG.getCoordinates().toString());
		assertEquals(null, privG.getGeo());

		interpreter.interpret(root);
		assertEquals("6845769.0 : 6845770.0 : 3641151.0 : 3641152.0 : YKJ : 1", privG.getInterpretations().getCoordinates().toString());
		assertEquals(1, privG.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals("[{6845769.0, 3641151.0}]", Geo.getBoundingBox(privG.getInterpretations().getCoordinates()).toString());

		converter.convert(root);
		assertEquals("[POINT (3641151 6845769)]", privG.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POINT (640924 6842899)]", privG.getConversions().getEurefGeo().getWKTList().toString());
		assertEquals("[POINT (29.664344 61.693297)]", privG.getConversions().getWgs84Geo().getWKTList().toString());

		assertEquals("6845769.0 : 6845770.0 : 3641151.0 : 3641152.0 : YKJ : 1", privG.getConversions().getYkj().toString());
		assertEquals("6842899.0 : 6842900.0 : 640924.0 : 640925.0 : EUREF : 1", privG.getConversions().getEuref().toString());
		assertEquals("61.693297 : 61.693297 : 29.664344 : 29.664344 : WGS84 : 1", privG.getConversions().getWgs84().toString());
	}

	@Test
	public void interpret_secure_convert_bugtesting_3() throws Exception {
		JSONObject data = new JSONObject(getTestData("etl-schema-conv-test3.json"));
		List<DwRoot> harmonized = new LajiETLJSONHarmonizer().harmonize(data, new Qname("KE.383"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);

		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, g.getGeo());
		assertEquals("6730000.0 : 6739999.0 : 3340000.0 : 3349999.0 : YKJ : null", g.getCoordinates().toString());

		interpreter.interpret(root);
		assertEquals("6730000.0 : 6739999.0 : 3340000.0 : 3349999.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals(10000, g.getInterpretations().getCoordinateAccuracy().intValue());

		securer.secure(root);
		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());
		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());

		converter.convert(root);
		assertEquals("6730000.0 : 6739999.0 : 3340000.0 : 3349999.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals(10000, g.getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void wgs84_point_but_inaccurate() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g1 = new Gathering(new Qname("G.1"));
		Document d = root.createPublicDocument();
		d.addGathering(g1);
		g1.setCoordinates(new Coordinates(63.12345, 34.12345, Type.WGS84).setAccuracyInMeters(100000));

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals("63.12345 : 63.12345 : 34.12345 : 34.12345 : WGS84 : 100000", g1.getCoordinates().toString());
		assertEquals("63.12345 : 63.12345 : 34.12345 : 34.12345 : WGS84 : 100000", g1.getInterpretations().getCoordinates().toString());
		assertEquals(100000, g1.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals("[POINT (34.12345 63.12345)]", g1.getConversions().getWgs84Geo().getWKTList().toString());
		// TODO pitäisikö tässä muuttaa bounding boxia epätarkaksi?
		// TODO entä geo?
	}

	@Test
	public void ykj_euref_1m_geo_point() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g1 = new Gathering(new Qname("G.1"));
		Gathering g2 = new Gathering(new Qname("G.2"));
		Document d = root.createPublicDocument();
		d.addGathering(g1).addGathering(g2);
		g1.setCoordinates(new Coordinates(6666666, 3333333, Type.YKJ));
		g2.setCoordinates(new Coordinates(7015489, 605939, Type.EUREF));
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals("[POINT (3333333 6666666)]", g1.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POINT (333231 6663867)]", g1.getConversions().getEurefGeo().getWKTList().toString());
		assertEquals("[POINT (3606153 7018428)]", g2.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POINT (605939 7015489)]", g2.getConversions().getEurefGeo().getWKTList().toString());
	}

	@Test
	public void ykj_euref_decimals() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g1 = new Gathering(new Qname("G.1"));
		Gathering g2 = new Gathering(new Qname("G.2"));
		Document d = root.createPublicDocument();
		d.addGathering(g1).addGathering(g2);
		g1.setCoordinates(new Coordinates(6666666.01, 3333333.7, Type.YKJ));
		g2.setCoordinates(new Coordinates(7015489.1, 605939.5, Type.EUREF));
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals("[POINT (3333334 6666666)]", g1.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POINT (333232 6663867)]", g1.getConversions().getEurefGeo().getWKTList().toString());
		assertEquals("[POINT (3606154 7018428)]", g2.getConversions().getYkjGeo().getWKTList().toString());
		assertEquals("[POINT (605940 7015489)]", g2.getConversions().getEurefGeo().getWKTList().toString());
	}

	@Test
	@Ignore
	public void big_data() throws Exception {
		List<DwRoot> roots = new RdfXmlHarmonizer(null).harmonize(getTestData("big-data.xml"), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		System.out.println("Interpeting");
		interpreter.interpret(root);

		System.out.println(root.getPublicDocument().getGatherings().get(0).getInterpretations().getMunicipalityDisplayname());

		System.out.println("Securing");
		securer.secure(root);
		System.out.println("After secure there is public: " + (root.getPublicDocument() != null) + " private: " + (root.getPrivateDocument() != null) + " splitted: " + root.getSplittedPublicDocuments().size() );
		System.out.println("Converting");
		converter.convert(root);
		System.out.println("Done");
	}

	@Test
	public void very_large_area_inside_finland_that_goes_far_outside_finland() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("lajistore-bug.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		assertEquals(Quality.Issue.INVALID_GEO, g.getQuality().getLocationIssue().getIssue());
		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g.getQuality().getLocationIssue().getSource());
		assertEquals("Conversion to YKJ failed. (Too large area?)", g.getQuality().getLocationIssue().getMessage());
	}

	@Test
	public void municipality_bugtest() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("interpreter-bugtest.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		assertEquals("Urjala", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("ML.440", g.getInterpretations().getFinnishMunicipality().toString()); // Urjala
		assertEquals("[ML.440]", g.getInterpretations().getFinnishMunicipalities().toString()); // Only urjala
	}

	@Test
	public void public_private_issues_test() throws Exception {
		List<DwRoot> roots = new LajiETLJSONHarmonizer().harmonize(new JSONObject(getTestData("issues-private-public.json")), new Qname("KE.67"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		root.getPublicDocument().setSecureLevel(SecureLevel.KM100);
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		assertEquals(1, root.getPrivateDocument().getGatherings().size());

		assertEquals(SecureLevel.NONE, root.getPrivateDocument().getSecureLevel());
		assertEquals(SecureLevel.KM100, root.getPublicDocument().getSecureLevel());
		assertEquals("[CUSTOM]", root.getPublicDocument().getSecureReasons().toString());

		Gathering publicG = root.getPublicDocument().getGatherings().get(0);
		Gathering privateG = root.getPrivateDocument().getGatherings().get(0);

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, privateG.getQuality().getLocationIssue().getIssue());
		assertEquals("Coordinates are not inside old municipality [Luvia] that has been joined to [Eurajoki] they are inside [Kauhava]: 612900, 212100 WGS84-DMS", privateG.getQuality().getLocationIssue().getMessage());
		assertEquals("LUVIA", privateG.getInterpretations().getMunicipalityDisplayname());

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, publicG.getQuality().getLocationIssue().getIssue());
		assertEquals(null, publicG.getQuality().getLocationIssue().getMessage()); // Message not shown
		assertEquals(null, publicG.getInterpretations().getMunicipalityDisplayname());

	}

	@Test
	public void many_features_different_parts_of_country() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("lajistore-hugearea-bug.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(1, root.getPublicDocument().getGatherings().size());

		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, g.getCoordinates());
		assertEquals(6, g.getGeo().getWKTList().size());

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		g = root.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getCoordinates());
		assertEquals(null, g.getGeo());
		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("60.016767 : 67.723227 : 23.948121 : 27.995928 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString());
		assertEquals(null, g.getConversions().getYkj());
		assertEquals(null, g.getConversions().getYkjGeo());
		assertEquals(null, g.getConversions().getWgs84());
		assertEquals(null, g.getConversions().getWgs84Geo());

		assertEquals("Too large area 184158 km^2", g.getQuality().getLocationIssue().getMessage());
	}

	@Test
	public void large_areas() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g1 = new Gathering(new Qname("G.1"));
		Gathering g2 = new Gathering(new Qname("G.2"));
		Gathering g3 = new Gathering(new Qname("G.3"));
		Gathering g4 = new Gathering(new Qname("G.4"));
		Gathering g5 = new Gathering(new Qname("G.5"));
		Document d = root.createPublicDocument();
		d.addGathering(g1);
		d.addGathering(g2);
		d.addGathering(g3);
		d.addGathering(g4);
		d.addGathering(g5);

		g1.setCoordinates(new Coordinates(666, 333, Type.YKJ)); // ok
		g2.setCoordinates(new Coordinates(66, 33, Type.YKJ)); // ok
		g3.setCoordinates(new Coordinates(700, 780, 333, 344, Type.YKJ)); // too large
		g4.setGeo(Geo.fromWKT("GEOMETRYCOLLECTION(POINT(24.609737137801023 60.553094584162565),POINT(28.20122287136052 63.694694540260244))", Type.WGS84)); // too large
		g5.setMunicipality("Inari"); // ok (municipality bounding box)

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		g1 = root.getPublicDocument().getGatherings().get(0);
		g2 = root.getPublicDocument().getGatherings().get(1);
		g3 = root.getPublicDocument().getGatherings().get(2);
		g4 = root.getPublicDocument().getGatherings().get(3);
		g5 = root.getPublicDocument().getGatherings().get(4);

		assertEquals(100*100*10000L, g1.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals(null, g1.createQuality().getLocationIssue());

		assertEquals(1000*1000*10000L, g2.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals(null, g2.createQuality().getLocationIssue());

		assertEquals(8800*1000*10000L, g3.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals("Quality [issue=TOO_LARGE_AREA, source=AUTOMATED_FINBIF_VALIDATION, message=Too large area 88000 km^2]", g3.getQuality().getLocationIssue().toString());

		assertEquals(66365588058L, g4.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals("Quality [issue=TOO_LARGE_AREA, source=AUTOMATED_FINBIF_VALIDATION, message=Too large area 66365 km^2]", g4.getQuality().getLocationIssue().toString());

		assertEquals(32154531424L, g5.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals(null, g5.createQuality().getLocationIssue());
	}

	@Test
	public void large_areas_2() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g1 = new Gathering(new Qname("G.1"));
		Document d = root.createPublicDocument();
		d.addGathering(g1);

		g1.setCoordinates(new Coordinates(679, 349, Type.YKJ)); // 10km area but will be secured to KM100

		Unit u1 = new Unit(new Qname("U.1"));
		u1.setTaxonVerbatim("kuukkeli"); // secure 100km
		g1.addUnit(u1);

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		g1 = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, g1.createQuality().getLocationIssue());
		assertEquals(1000*1000*10000L, g1.getConversions().getBoundingBoxAreaInSquareMeters().longValue());
		assertEquals("6700000.0 : 6800000.0 : 3400000.0 : 3500000.0 : YKJ : 100000", g1.getConversions().getYkj().toString());
	}

	@Test
	public void hidden_leg_should_hide_image_capturer_from_public() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, dao).harmonize(new JSONObject(getTestData("lajistore-hidden-leg-image-capturer.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(1, root.getPublicDocument().getGatherings().size());
		assertEquals(2, root.getPublicDocument().getGatherings().get(0).getUnits().size());

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		Gathering pubG = root.getPublicDocument().getGatherings().get(0);
		Gathering priG = root.getPrivateDocument().getGatherings().get(0);

		Unit pubU1 = pubG.getUnits().get(0);
		Unit pubU2 = pubG.getUnits().get(1);
		Unit priU1 = priG.getUnits().get(0);
		Unit priU2 = priG.getUnits().get(1);

		assertEquals("[]", root.getPublicDocument().getEditorUserIds().toString());
		assertEquals("[]", pubG.getTeam().toString());
		assertEquals("[]", pubG.getObserverUserIds().toString());

		assertEquals("[http://tun.fi/MA.5]", root.getPrivateDocument().getEditorUserIds().toString());
		assertEquals("[Esko Piirainen]", priG.getTeam().toString());
		assertEquals("[http://tun.fi/MA.5]", priG.getObserverUserIds().toString());

		assertEquals(2, pubU1.getMedia().size());
		assertEquals(1, pubU2.getMedia().size());
		assertEquals(2, priU1.getMedia().size());
		assertEquals(1, priU2.getMedia().size());

		assertEquals(null, pubU1.getMedia().get(0).getAuthor());
		assertEquals(null, pubU1.getMedia().get(0).getCopyrightOwner());
		assertEquals(null, pubU2.getMedia().get(0).getAuthor());
		assertEquals(null, pubU2.getMedia().get(0).getCopyrightOwner());

		assertEquals(MediaType.IMAGE, priU1.getMedia().get(0).getMediaType());
		assertEquals(MediaType.AUDIO, priU1.getMedia().get(1).getMediaType());

		assertEquals("https://imagetest.laji.fi/MM.7751/giant_full.jpg", priU1.getMedia().get(0).getFullURL());
		assertEquals("https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL_spectrogram.png", priU1.getMedia().get(1).getFullURL());
		assertEquals("https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL_spectrogram_thumb.jpg", priU1.getMedia().get(1).getThumbnailURL());
		assertEquals("https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL.wav", priU1.getMedia().get(1).getWavURL());
		assertEquals("https://imagetest.laji.fi/MM.97876/11_CA_PE_Pnasutus_VL.mp3", priU1.getMedia().get(1).getMp3URL());

		assertEquals("capturer", priU1.getMedia().get(0).getAuthor());
		assertEquals("rightsOwner", priU1.getMedia().get(0).getCopyrightOwner());
		assertEquals("capturer", priU2.getMedia().get(0).getAuthor());
		assertEquals("rightsOwner", priU2.getMedia().get(0).getCopyrightOwner());
	}

	@Test
	public void securing_those_with_issues() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		root.createPublicDocument().addGathering(g);
		Unit u1 = new Unit(new Qname("U1"));
		Unit u2 = new Unit(new Qname("U2"));
		g.addUnit(u1);
		g.addUnit(u2);
		u1.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_HANDLED);
		u1.setTaxonVerbatim("talitiainen");
		u2.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_PHOTO);
		u2.setTaxonVerbatim("#susi"); // # causes secure

		g.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_DATE, Quality.Source.ORIGINAL_DOCUMENT, "Some issue there is"));

		interpreter.interpret(root);

		assertNull(root.getPrivateDocument());
		assertNotNull(root.getPublicDocument());
		assertEquals(false, root.hasSplittedPublicDocuments());
		assertEquals(null, root.getSplittedPublicDocuments());

		g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("Some issue there is",g .getQuality().getTimeIssue().getMessage());
		assertEquals(true, g.getQuality().hasIssues());
		Quality.hasIssues(root.getPublicDocument(), g, g.getUnits().get(0));

		securer.secure(root);
		converter.convert(root);

		assertTrue(g.getUnits().get(0).getQuality().isDocumentGatheringUnitQualityIssues());

		assertNotNull(root.getPrivateDocument());
		assertNotNull(root.getPublicDocument());
		assertTrue(root.hasSplittedPublicDocuments());
		assertEquals(1, root.getSplittedPublicDocuments().size());

		Gathering pubG = root.getPublicDocument().getGatherings().get(0);
		Gathering privG = root.getPrivateDocument().getGatherings().get(0);
		Gathering splitG = root.getSplittedPublicDocuments().get(0).getGatherings().get(0);

		assertEquals(2, privG.getUnits().size());
		assertEquals(1, pubG.getUnits().size());
		assertEquals(1, splitG.getUnits().size());

		assertEquals(Quality.Issue.INVALID_DATE, privG.getQuality().getTimeIssue().getIssue());
		assertEquals(Quality.Issue.INVALID_DATE, pubG.getQuality().getTimeIssue().getIssue());
		assertEquals(Quality.Issue.INVALID_DATE, splitG.getQuality().getTimeIssue().getIssue());

		assertEquals("Some issue there is", privG.getQuality().getTimeIssue().getMessage());
		assertEquals("Some issue there is", pubG.getQuality().getTimeIssue().getMessage());
		assertEquals(null, splitG.getQuality().getTimeIssue().getMessage()); // for secured message is removed

		for (Unit privU : privG.getUnits()) {
			assertTrue(privU.getQuality().isDocumentGatheringUnitQualityIssues());
		}
		assertTrue(pubG.getUnits().get(0).getQuality().isDocumentGatheringUnitQualityIssues());
		assertTrue(splitG.getUnits().get(0).getQuality().isDocumentGatheringUnitQualityIssues());
	}

	@Test
	public void linelength() throws Exception {
		assertEquals(1, ((Line) Line.from(new int[][] {{0, 0}, {1, 0}})).getLength(), 0);
		assertEquals(3, ((Line) Line.from(new int[][] {{0, 0}, {1, 0}, {1, 2}})).getLength(), 0);

		DwRoot root = lineLengthTestData();
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		Gathering g = root.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getConversions().getLinelengthInMeters());

		root = lineLengthTestData();
		root.getPublicDocument().getGatherings().get(0).addFact(Converter.ROUTE_LENGTH_FACT, "123");

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		g = root.getPublicDocument().getGatherings().get(0);
		assertEquals(123, g.getConversions().getLinelengthInMeters().intValue());
	}

	private DwRoot lineLengthTestData() throws CriticalParseFailure, DataValidationException {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G.1"));
		Document d = root.createPublicDocument();
		d.addGathering(g);
		g.setCoordinates(new Coordinates(679, 349, Type.YKJ));
		return root;
	}

	@Test
	public void timeparsing_issue_3() throws Exception {
		List<DwRoot> list = new RdfXmlHarmonizer(new MediaDAOStub()).harmonize(ConverterTests.getTestData("rdf-xml-datebug3.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		assertEquals(null, root.getPrivateDocument());
		root.getPublicDocument().setSecureLevel(SecureLevel.KM10);
		root.getPublicDocument().addSecureReason(SecureReason.CUSTOM);

		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering publicG = root.getPublicDocument().getGatherings().get(0);
		assertEquals("DateRange [begin=null, end=2012-09-02]", publicG.getEventDate().toString());
		assertEquals(null, publicG.getQuality());
		assertEquals("DateRange [begin=null, end=2012-09-02]", publicG.getEventDate().toString());
		assertEquals(null, publicG.getQuality());

		interpreter.interpret(root);

		publicG = root.getPublicDocument().getGatherings().get(0);
		assertEquals("DateRange [begin=null, end=2012-09-02]", publicG.getEventDate().toString());
		assertEquals(null, publicG.getQuality());

		securer.secure(root);

		Gathering privateG = root.getPrivateDocument().getGatherings().get(0);
		publicG = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, publicG.getEventDate());
		assertEquals(null, publicG.getDisplayDateTime());
		assertEquals(null, publicG.getQuality());

		assertEquals("DateRange [begin=null, end=2012-09-02]", privateG.getEventDate().toString());
		assertEquals(" - 2012-09-02", privateG.getDisplayDateTime());
		assertEquals(null, privateG.getQuality());

		converter.convert(root.getPrivateDocument());
		converter.convert(root.getPublicDocument());

		privateG = root.getPrivateDocument().getGatherings().get(0);
		publicG = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, publicG.getEventDate());
		assertEquals(null, publicG.getDisplayDateTime());
		assertEquals(null, publicG.getQuality());

		assertEquals("DateRange [begin=null, end=2012-09-02]", privateG.getEventDate().toString());
		assertEquals(" - 2012-09-02", privateG.getDisplayDateTime());
		assertEquals(null, privateG.getQuality());

	}

	@Test
	public void nonHttpsImages() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		root.createPublicDocument().addGathering(g);
		Unit u = new Unit(new Qname("U1"));
		g.addUnit(u);

		root.getPublicDocument().addMedia(new MediaObject(MediaType.IMAGE, "http://someuri.com/1.jpg"));

		MediaObject gm = new MediaObject(MediaType.AUDIO);
		try {
			gm.setWavURL("HTTPS://someuri.com/1.wav");
		} catch (DataValidationException e ) {
			assertEquals("Invalid URL: HTTPS://someuri.com/1.wav", e.getMessage());
		}
		gm.setWavURL("http://someuri.com/1.wav");
		g.addMedia(gm);

		try {
			u.addMedia(new MediaObject(MediaType.IMAGE, "noturiatall"));
			fail("should throw ex");
		} catch (DataValidationException e ) {
			assertEquals("Invalid URL: noturiatall", e.getMessage());
		}

		u.addMedia(new MediaObject(MediaType.IMAGE, "https://someuri.com/2.jpg"));

		converter.convert(root);

		assertEquals(null, root.getPublicDocument().createQuality().getIssue());
		assertEquals(null, g.createQuality().getIssue());
		assertEquals(null, u.getQuality().getIssue());

		assertEquals("https://someuri.com/1.jpg", root.getPublicDocument().getMedia().get(0).getFullURL());
		assertEquals("https://someuri.com/1.wav", g.getMedia().get(0).getFullURL());
		assertEquals("https://someuri.com/2.jpg", u.getMedia().get(0).getFullURL());
	}

	@Test
	public void label_and_carcass_and_non_public_Images() throws Exception {
		DwRoot root = new DwRoot(new Qname("D.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		root.createPublicDocument().addGathering(g);
		Unit u = new Unit(new Qname("U1"));
		u.setTaxonVerbatim("#susi"); // # causes secure
		g.addUnit(u);

		// 1 document media
		root.getPublicDocument().addMedia(new MediaObject(MediaType.IMAGE, "http://someuri.com/1.jpg"));

		// 4 unit media
		u.addMedia(new MediaObject(MediaType.IMAGE, "https://someuri.com/2.jpg").addKeyword("carcass"));
		u.addMedia(new MediaObject(MediaType.IMAGE, "https://someuri.com/2.jpg").addKeyword("non-public"));
		u.addMedia(new MediaObject(MediaType.IMAGE, "https://someuri.com/3.jpg").addKeyword("notcarcass"));
		MediaObject m4 = new MediaObject(MediaType.IMAGE, "https://someuri.com/4.jpg");
		m4.setType(new Qname("MM.typeEnumCarcass"));
		u.addMedia(m4);

		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);

		assertEquals(0, root.getPublicDocument().getMedia().size()); // hide secured doc images from public
		assertEquals(1, root.getPrivateDocument().getMedia().size()); // don't hide document images from private
		assertEquals(1, root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getMedia().size()); // hide carcass and non-public image from public
		assertEquals(1, root.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getMedia().size()); // hide carcass and non-public image from private
	}

	@Test
	public void secured_conversion_bug() throws Exception {
		List<DwRoot> list = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(ConverterTests.getTestData("lajistore-secured-conversion-bug.json")), new Qname("KE.123"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(SecureLevel.NONE, d.getSecureLevel());
		assertEquals("POINT (29.154044 61.902865)", d.getGatherings().get(0).getGeo().getWKT());

		interpreter.interpret(root);
		securer.secure(root);

		assertEquals(false, root.getPrivateDocument() == null);
		assertEquals(SecureLevel.HIGHEST, root.getPublicDocument().getSecureLevel());

		assertEquals("61.902865 : 61.902865 : 29.154044 : 29.154044 : WGS84 : 1", root.getPrivateDocument().getGatherings().get(0).getInterpretations().getCoordinates().toString());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinates());

		converter.convert(root.getPrivateDocument());
		converter.convert(root.getPublicDocument());

		assertEquals(null, root.getPrivateDocument().getGatherings().get(0).getQuality());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getQuality());

		assertEquals("POINT (613167 6865236)", root.getPrivateDocument().getGatherings().get(0).getConversions().getEurefGeo().getWKT());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getConversions().getEurefGeo());
	}

	@Test
	public void test_cases_when_in_conversion_min_gets_larger_than_max() throws DataValidationException {
		Coordinates c = new Coordinates(67.978441, 67.978747, 28.692298, 28.692315, Type.WGS84);
		Coordinates converted = CoordinateConverter.convert(c).getYkj();
		assertEquals("7544581.0 : 7544615.0 : 3571005.0 : 3571006.0 : YKJ : null", converted.toString());
		Coordinates secured = converted.conceal(SecureLevel.KM100, c.getType());
		assertEquals("75.0 : 76.0 : 35.0 : 36.0 : YKJ : 100000", secured.toString());
	}

	@Test
	public void ykjCenterPoints() throws Exception {
		DwRoot ykj100mVerbatim = ykjVerbatim("66666:33333");
		DwRoot ykj10KmVerbatim = ykjVerbatim("666:333");

		DwRoot coarsed5Km = coarse(SecureLevel.KM5, "66666:33333");
		DwRoot coarsed10Km = coarse(SecureLevel.KM10, "66666:33333");
		DwRoot coarsed50Km = coarse(SecureLevel.KM50, "66666:33333");
		DwRoot coarsed100Km = coarse(SecureLevel.KM100, "66666:33333");

		assertEquals("60.077745:24.00269:WGS84", wgs84CenterPoint(ykj100mVerbatim));
		assertEquals("MetricPoint [northing=6666650, easting=3333350]", wgs84CenterPointAsYkj(ykj100mVerbatim));

		assertEquals("60.063623:24.033619:WGS84", wgs84CenterPoint(ykj10KmVerbatim));
		assertEquals("MetricPoint [northing=6665000, easting=3335000]", wgs84CenterPointAsYkj(ykj10KmVerbatim));

		assertEquals(wgs84CenterPoint(ykj10KmVerbatim), wgs84CenterPoint(coarsed10Km));

		assertEquals("60.085017:23.986746:WGS84", wgs84CenterPoint(coarsed5Km));
		assertEquals("MetricPoint [northing=6667500, easting=3332500]", wgs84CenterPointAsYkj(coarsed5Km));

		assertEquals("60.149095:23.845764:WGS84", wgs84CenterPoint(coarsed50Km));
		assertEquals("MetricPoint [northing=6675000, easting=3325000]", wgs84CenterPointAsYkj(coarsed50Km));

		assertEquals("59.934902:24.313583:WGS84", wgs84CenterPoint(coarsed100Km));
		assertEquals("MetricPoint [northing=6650000, easting=3350000]", wgs84CenterPointAsYkj(coarsed100Km));
		assertEquals("POLYGON ((3300000 6600000, 3300000 6700000, 3400000 6700000, 3400000 6600000, 3300000 6600000))", ykjGeo(coarsed100Km));
		assertEquals("POLYGON ((23.468432 59.465735, 23.37185 60.361561, 25.182789 60.398631, 25.231261 59.501499, 23.468432 59.465735))", wgs84Geo(coarsed100Km));
	}

	private String wgs84CenterPointAsYkj(DwRoot root) {
		SingleCoordinates c = root.getPublicDocument().getGatherings().get(0).getConversions().getWgs84CenterPoint();
		return new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(c.getLat(), c.getLon())).getYkj().toString();
	}

	private String wgs84CenterPoint(DwRoot root) {
		return root.getPublicDocument().getGatherings().get(0).getConversions().getWgs84CenterPoint().toString();
	}

	private String ykjGeo(DwRoot root) {
		return root.getPublicDocument().getGatherings().get(0).getConversions().getYkjWKT();
	}

	private String wgs84Geo(DwRoot root) {
		return root.getPublicDocument().getGatherings().get(0).getConversions().getWgs84WKT();
	}

	private DwRoot coarse(SecureLevel secureLevel, String ykj) throws Exception {
		DwRoot root = ykjVerbatimNonIntSecConv(ykj);
		root.getPublicDocument().setSecureLevel(secureLevel);
		root.getPublicDocument().addSecureReason(SecureReason.CUSTOM);
		return intSecConvert(root);
	}

	private DwRoot ykjVerbatim(String ykj) throws Exception {
		DwRoot root = ykjVerbatimNonIntSecConv(ykj);
		return intSecConvert(root);
	}

	private DwRoot ykjVerbatimNonIntSecConv(String ykj) throws CriticalParseFailure, DataValidationException {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));
		Document d = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G1"));
		g.setCoordinates(BaseHarmonizer.parseYKJCoordinates(ykj));
		g.setCountry("FI");
		d.addGathering(g);
		Unit u = new Unit(new Qname("U1"));
		u.setTaxonVerbatim("something");
		g.addUnit(u);
		return root;
	}

	private DwRoot intSecConvert(DwRoot root) {
		interpreter.interpret(root);
		securer.secure(root);
		converter.convert(root);
		return root;
	}

	@Test
	public void conversionProblemsWithStrangeGeometries() throws Exception {
		String data = getTestData("etl-schema-conv-test4.json");
		DwRoot root = JsonToModel.rootFromJson(new JSONObject(data));
		intSecConvert(root);
		for (Gathering g : root.getPrivateDocument().getGatherings()) {
			assertEquals(null, g.getQuality());
		}
	}

	@Test
	public void problemsWithStrangeGeometries2() throws Exception {
		String data = getTestData("etl-schema-conv-test5.json");
		DwRoot root = JsonToModel.rootFromJson(new JSONObject(data));

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		assertEquals("["+
				"POLYGON ((22.351885 61.838862, 22.351877 61.83886, 22.351857 61.838856, 22.351846 61.838855, 22.35183 61.838861, 22.351801 61.838874, 22.351797 61.838879, 22.351791 61.83888, 22.351876 61.838883, 22.351877 61.838881, 22.351884 61.838865, 22.351885 61.838862)), "+
				"POLYGON ((22.352543 61.838905, 22.352525 61.838906, 22.352504 61.838909, 22.35257 61.838911, 22.352572 61.838911, 22.352558 61.838907, 22.352543 61.838905)), "+
				"POLYGON ((22.352609 61.838923, 22.352621 61.838923, 22.352631 61.838921, 22.352641 61.83892, 22.352656 61.838921, 22.352672 61.83892, 22.352677 61.838918, 22.35268 61.838918, 22.352688 61.838917, 22.352692 61.838917, 22.352693 61.838917, 22.352694 61.838916, 22.352572 61.838911, 22.352589 61.838917, 22.352609 61.838923)), "+
				"POLYGON ((22.351877 61.838903, 22.351884 61.838906, 22.351896 61.838909, 22.351916 61.838915, 22.35194 61.838914, 22.351966 61.838913, 22.351987 61.838913, 22.352003 61.838917, 22.352023 61.838919, 22.352045 61.838919, 22.352065 61.838918, 22.352084 61.838921, 22.352095 61.838925, 22.352114 61.83895, 22.352132 61.83895, 22.35216 61.838953, 22.352173 61.838956, 22.352194 61.838981, 22.352218 61.838988, 22.352243 61.83899, 22.352267 61.838986, 22.352289 61.838983, 22.352308 61.83898, 22.352328 61.838978, 22.352353 61.838974, 22.352374 61.83897, 22.352392 61.838963, 22.352409 61.838953, 22.352433 61.838934, 22.352449 61.838921, 22.352464 61.838915, 22.352476 61.838913, 22.352491 61.83891, 22.352504 61.838909, 22.351876 61.838883, 22.351872 61.838895, 22.351877 61.838903))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getGeo().getWKTList().toString());

		intSecConvert(root);

		assertEquals("["+
				"POLYGON ((22.351885 61.838862, 22.351877 61.83886, 22.351857 61.838856, 22.351846 61.838855, 22.35183 61.838861, 22.351801 61.838874, 22.351797 61.838879, 22.351791 61.83888, 22.351876 61.838883, 22.351877 61.838881, 22.351884 61.838865, 22.351885 61.838862)), "+
				"POLYGON ((22.352543 61.838905, 22.352525 61.838906, 22.352504 61.838909, 22.35257 61.838911, 22.352572 61.838911, 22.352558 61.838907, 22.352543 61.838905)), "+
				"POLYGON ((22.352609 61.838923, 22.352621 61.838923, 22.352631 61.838921, 22.352641 61.83892, 22.352656 61.838921, 22.352672 61.83892, 22.352677 61.838918, 22.35268 61.838918, 22.352688 61.838917, 22.352692 61.838917, 22.352693 61.838917, 22.352694 61.838916, 22.352572 61.838911, 22.352589 61.838917, 22.352609 61.838923)), "+
				"POLYGON ((22.351877 61.838903, 22.351884 61.838906, 22.351896 61.838909, 22.351916 61.838915, 22.35194 61.838914, 22.351966 61.838913, 22.351987 61.838913, 22.352003 61.838917, 22.352023 61.838919, 22.352045 61.838919, 22.352065 61.838918, 22.352084 61.838921, 22.352095 61.838925, 22.352114 61.83895, 22.352132 61.83895, 22.35216 61.838953, 22.352173 61.838956, 22.352194 61.838981, 22.352218 61.838988, 22.352243 61.83899, 22.352267 61.838986, 22.352289 61.838983, 22.352308 61.83898, 22.352328 61.838978, 22.352353 61.838974, 22.352374 61.83897, 22.352392 61.838963, 22.352409 61.838953, 22.352433 61.838934, 22.352449 61.838921, 22.352464 61.838915, 22.352476 61.838913, 22.352491 61.83891, 22.352504 61.838909, 22.351876 61.838883, 22.351872 61.838895, 22.351877 61.838903))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getWgs84Geo().getWKTList().toString());

		assertEquals("["+
				"POLYGON ((3255481 6867863, 3255480 6867863, 3255479 6867863, 3255478 6867863, 3255476 6867865, 3255476 6867866, 3255480 6867866, 3255480 6867865, 3255481 6867864, 3255481 6867863)), "+
				"LINESTRING (3255513 6867866, 3255517 6867866), "+
				"POLYGON ((3255517 6867866, 3255518 6867867, 3255523 6867867, 3255524 6867866, 3255517 6867866)), "+
				"POLYGON ((3255480 6867868, 3255481 6867868, 3255482 6867868, 3255483 6867869, 3255484 6867869, 3255485 6867869, 3255486 6867869, 3255487 6867869, 3255488 6867869, 3255489 6867869, 3255490 6867869, 3255491 6867869, 3255492 6867870, 3255493 6867872, 3255494 6867872, 3255496 6867872, 3255496 6867873, 3255498 6867875, 3255499 6867876, 3255500 6867876, 3255502 6867876, 3255503 6867875, 3255504 6867875, 3255505 6867875, 3255506 6867874, 3255507 6867873, 3255508 6867873, 3255509 6867871, 3255510 6867869, 3255511 6867868, 3255511 6867867, 3255512 6867867, 3255513 6867866, 3255480 6867866, 3255480 6867867, 3255480 6867868))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getYkjGeo().getWKTList().toString());

		assertEquals("["+
				"POLYGON ((255408 6864983, 255407 6864983, 255406 6864983, 255404 6864985, 255404 6864986, 255408 6864986, 255408 6864985, 255408 6864984, 255408 6864983)), "+
				"POLYGON ((255443 6864985, 255441 6864986, 255445 6864986, 255443 6864985)), "+
				"POLYGON ((255447 6864987, 255448 6864987, 255449 6864987, 255450 6864987, 255451 6864986, 255445 6864986, 255446 6864987, 255447 6864987)), "+
				"POLYGON ((255408 6864988, 255409 6864988, 255411 6864989, 255412 6864989, 255413 6864989, 255414 6864988, 255415 6864989, 255416 6864989, 255417 6864989, 255418 6864989, 255419 6864989, 255420 6864989, 255421 6864992, 255422 6864992, 255424 6864992, 255424 6864993, 255426 6864995, 255427 6864996, 255428 6864996, 255430 6864996, 255431 6864995, 255432 6864995, 255433 6864994, 255434 6864994, 255435 6864993, 255436 6864992, 255437 6864991, 255438 6864989, 255439 6864988, 255439 6864987, 255440 6864987, 255441 6864986, 255408 6864986, 255408 6864987, 255408 6864988))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getEurefGeo().getWKTList().toString());

		for (Gathering g : root.getPublicDocument().getGatherings()) {
			assertEquals(null, g.getQuality());
		}
	}

	@Test
	public void problemsWithStrangeGeometries3() throws Exception {
		String data = getTestData("etl-schema-conv-test6.json");
		DwRoot root = JsonToModel.rootFromJson(new JSONObject(data));

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		assertEquals("["+
				"POLYGON ((23.473115 61.473702, 23.472995 61.473632, 23.473017 61.473649, 23.473191 61.473754, 23.473184 61.473748, 23.473115 61.473702)), "+
				"POINT (23.473191 61.473754), "+
				"POLYGON ((23.473239 61.47378, 23.473236 61.473778, 23.473231 61.473776, 23.473219 61.47377, 23.473198 61.473758, 23.473278 61.473807, 23.47324 61.47378, 23.473239 61.47378))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getGeo().getWKTList().toString());

		intSecConvert(root);

		for (Gathering g : root.getPublicDocument().getGatherings()) {
			assertEquals(null, g.getQuality());
		}

		assertEquals("["+
				"POLYGON ((23.473115 61.473702, 23.472995 61.473632, 23.473017 61.473649, 23.473191 61.473754, 23.473184 61.473748, 23.473115 61.473702)), "+
				"POINT (23.473191 61.473754), "+
				"POLYGON ((23.473239 61.47378, 23.473236 61.473778, 23.473231 61.473776, 23.473219 61.47377, 23.473198 61.473758, 23.473278 61.473807, 23.47324 61.47378, 23.473239 61.47378))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getWgs84Geo().getWKTList().toString());

		assertEquals("["+
				"POLYGON ((3312252 6823496, 3312245 6823489, 3312246 6823491, 3312256 6823502, 3312256 6823501, 3312252 6823496)), "+
				"POINT (3312256 6823502), "+
				"POLYGON ((3312257 6823502, 3312259 6823505, 3312261 6823507, 3312259 6823504, 3312257 6823502))"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getYkjGeo().getWKTList().toString());

		assertEquals("["+
				"POLYGON ((312150 6820627, 312161 6820639, 312162 6820640, 312157 6820634, 312150 6820627)), "+
				"POINT (312162 6820640), "+
				"LINESTRING (312162 6820640, 312167 6820645)"+
				"]",
				root.getPublicDocument().getGatherings().get(0).getConversions().getEurefGeo().getWKTList().toString());
	}

}
