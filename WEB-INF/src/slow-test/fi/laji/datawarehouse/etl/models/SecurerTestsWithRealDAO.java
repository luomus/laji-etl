package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.DAOImpleTests;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;

public class SecurerTestsWithRealDAO {

private static DAO dao;
	
	@BeforeClass
	public static void init() {
		Config config = TestConfig.getConfig();
		dao = new DAOImpleTests.AlmostRealDao(config, SecurerTestsWithRealDAO.class);
	}

	@AfterClass
	public static void destroy() {
		if (dao != null) dao.close();
	}

	@Test
	public void withRealDao_testSecureLevelResolveByUsingSynonymName() throws Exception {
		Securer securer = new Securer(dao);
		
		String synonym = "Chlidonias nigra"; // actual = "Chlidonias niger" <-- secure level for nest site = 10 km
				
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		Document pub = dwRoot.createPublicDocument();
		Unit pubUnit = new Unit(new Qname("u1"));
		pub.addGathering(new Gathering(new Qname("g1")).addUnit(pubUnit));
		
		pubUnit.setTaxonVerbatim(synonym);
		pubUnit.setBreedingSite(true);
		
		securer.secure(dwRoot);

		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, dwRoot.getPublicDocument().getSecureReasons().get(0));
		
		assertEquals(SecureLevel.NONE, dwRoot.getPrivateDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPrivateDocument().getSecureReasons().size());
		assertEquals(SecureReason.BREEDING_SITE_CONSERVATION, dwRoot.getPrivateDocument().getSecureReasons().get(0));
	}

}
