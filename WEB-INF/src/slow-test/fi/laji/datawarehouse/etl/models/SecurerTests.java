package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class SecurerTests {

	private static final Qname RING_SFH_A12345 = new Qname("KE.123/A12345");

	private static final String TUNTURIHAUKKA = "tunturihaukka";
	private static final Qname TUNTURIHAUKKAID = new Qname("MX.26825");
	private static final String HYPPYS_HYPPYS = "hyppys hyppys";
	private static final Qname HYPPYSID = new Qname("hyppysid");
	private static final String PARUS_MAJOR = "parus major";
	private static final Qname PARUSMAJORID = new Qname("talitinttiid");
	private static final Qname NIITTYLAUKKA_ID = new Qname("niittylaukkaneilikkaid");
	private static final String NIITTYLAUKKA = "niittylaukkaneilikka";
	private static final Qname KANAGSKAARME_ID = new Qname("kangaskaarmid");
	private static final String KANAGSKAARME = "kangaskaarme";
	private static final String MERIKOTKA = "merikotka";
	private static final Qname MERIKOTKAID = new Qname("MX.26530");
	private static final Qname VALKOSELKATIKKA_ID = new Qname("MX.30438");
	private static final Qname KANAHAUKKA_SUBSPECIES_ID = new Qname("MX.5083976");
	private static final Qname KANAHAUKKA_ID = new Qname("MX.26647");
	private static final Qname MAAKOTKA_ID = new Qname("MX.26727");
	private static final Qname KUUKKELI_ID = new Qname("MX.37095");
	private static final Qname SUOSIRRI_ID = new Qname("MX.27699");

	private static final Qname COL_ID_HAS_FEW_DAYS_QUARANTINE = new Qname("fewdays");
	private static final Qname COL_ID_HAS_FOUR_YEARS_QUARANTINE = new Qname("4years");
	private static final Qname COLL_ID_HAS_SECURE_LEVEL = new Qname("securelevel");

	private static class SecurerTestDao extends TestDAO {

		private static final Map<String, Qname> lookup;
		static {
			lookup = new HashMap<>();
			add(TUNTURIHAUKKA, TUNTURIHAUKKAID);
			add(HYPPYS_HYPPYS, HYPPYSID);
			add(PARUS_MAJOR, PARUSMAJORID);
			add(MERIKOTKA, MERIKOTKAID);
			add(NIITTYLAUKKA, NIITTYLAUKKA_ID);
			add(KANAGSKAARME, KANAGSKAARME_ID);
			add(VALKOSELKATIKKA_ID.toString(), VALKOSELKATIKKA_ID);
			add(KANAHAUKKA_SUBSPECIES_ID.toString(), KANAHAUKKA_SUBSPECIES_ID);
			add(MAAKOTKA_ID.toString(), MAAKOTKA_ID);
			add(KUUKKELI_ID.toString(), KUUKKELI_ID);
			add(SUOSIRRI_ID.toString(), SUOSIRRI_ID);
		}

		private static void add(String targetName, Qname taxonId) {
			for (String s : TaxonLinkingService.toTargetLookupStrings(targetName, Const.MASTER_CHECKLIST_QNAME, taxonId, true)) {
				lookup.put(s, taxonId);
			}
			for (String s : TaxonLinkingService.toTaxonIdLookupStrings(taxonId, Const.MASTER_CHECKLIST_QNAME)) {
				lookup.put(s, taxonId);
			}
		}

		private static final Map<String, Qname> searchmap;
		static {
			searchmap = new HashMap<>();
			searchmap.put(TUNTURIHAUKKA, TUNTURIHAUKKAID);
			searchmap.put(TUNTURIHAUKKAID.toString(), TUNTURIHAUKKAID);
			searchmap.put(TUNTURIHAUKKAID.toURI(), TUNTURIHAUKKAID);
			searchmap.put(HYPPYS_HYPPYS, HYPPYSID);
			searchmap.put(HYPPYSID.toString(), HYPPYSID);
			searchmap.put(HYPPYSID.toURI(), HYPPYSID);
			searchmap.put(PARUS_MAJOR, PARUSMAJORID);
			searchmap.put(PARUSMAJORID.toString(), PARUSMAJORID);
			searchmap.put(PARUSMAJORID.toURI(), PARUSMAJORID);

			searchmap.put(MERIKOTKAID.toString(), MERIKOTKAID);
			searchmap.put(MERIKOTKAID.toURI(), MERIKOTKAID);
			searchmap.put(NIITTYLAUKKA, NIITTYLAUKKA_ID);
			searchmap.put(VALKOSELKATIKKA_ID.toString(), VALKOSELKATIKKA_ID);
			searchmap.put(KANAHAUKKA_SUBSPECIES_ID.toString(), KANAHAUKKA_SUBSPECIES_ID);
			searchmap.put(MAAKOTKA_ID.toString(), MAAKOTKA_ID);
			searchmap.put(KUUKKELI_ID.toString(), KUUKKELI_ID);
			searchmap.put(SUOSIRRI_ID.toString(), SUOSIRRI_ID);
		}

		public SecurerTestDao() {
			super(SecurerTests.class);
		}

		@Override
		public TaxonLinkingService getTaxonLinkingService() {
			return new TaxonLinkingService(lookup);
		}

		@Override
		public Set<Qname> getAllTaxonMatches(String targetName) {
			if (targetName == null) return Collections.emptySet();
			Qname id = searchmap.get(targetName);
			if (id == null)  return Collections.emptySet();
			return Utils.set(id);
		}

		@Override
		public Taxon getTaxon(Qname taxonId) {
			if (taxonId == null) throw new NoSuchTaxonException(taxonId);

			if (taxonId.equals(TUNTURIHAUKKAID)) {
				Taxon taxon = new Taxon(TUNTURIHAUKKAID, null);
				taxon.setSecureLevel(new Qname("MX.secureLevelHighest"));
				return taxon;
			}

			if (taxonId.equals(HYPPYSID)) {
				Taxon taxon = new Taxon(HYPPYSID, null);
				taxon.setNestSiteSecureLevel(new Qname("MX.secureLevelKm1"));
				taxon.setNaturaAreaSecureLevel(new Qname("MX.secureLevelKm10"));
				return taxon;
			}

			if (taxonId.equals(NIITTYLAUKKA_ID)) {
				Taxon taxon = new Taxon(NIITTYLAUKKA_ID, null);
				taxon.setSecureLevel(new Qname("MX.secureLevelHighest"));
				return taxon;
			}

			if (taxonId.equals(KANAGSKAARME_ID)) {
				Taxon taxon = new Taxon(NIITTYLAUKKA_ID, null);
				taxon.setSecureLevel(new Qname("MX.secureLevelKm5"));
				return taxon;
			}

			if (taxonId.equals(PARUSMAJORID)) {
				return new Taxon(PARUSMAJORID, null);
			}

			if (taxonId.equals(MERIKOTKAID)) {
				Taxon taxon = new Taxon(MERIKOTKAID, null);
				taxon.setNestSiteSecureLevel(new Qname("MX.secureLevelKm50")); // custom rule: in south KM10, up north KM50
				taxon.setBreedingSecureLevel(new Qname("MX.secureLevelKm50")); // custom rule: in south KM10, up north KM50; custom breeding season 1.2.-31.8.
				return taxon;
			}

			if (taxonId.equals(VALKOSELKATIKKA_ID)) {
				Taxon taxon = new Taxon(VALKOSELKATIKKA_ID, null);
				taxon.setNestSiteSecureLevel(new Qname("MX.secureLevelKm5"));
				taxon.setBreedingSecureLevel(new Qname("MX.secureLevelKm5")); // custom breeding season 1.3.-31.8.
				taxon.setNaturaAreaSecureLevel(new Qname("MX.secureLevelHighest"));
				return taxon;
			}

			if (taxonId.equals(KANAHAUKKA_SUBSPECIES_ID)) {
				InMemoryTaxonContainerImple container = new InMemoryTaxonContainerImple(null, null, null);

				Taxon subsp = new Taxon(KANAHAUKKA_SUBSPECIES_ID, container);
				subsp.setParentQname(KANAHAUKKA_ID);
				subsp.setTaxonRank(new Qname("MX.subspecies"));

				Taxon species = new Taxon(KANAHAUKKA_ID, container);
				species.setBreedingSecureLevel(new Qname("MX.secureLevelKm10")); // custom breeding season 1.3.-31.8.
				species.setNestSiteSecureLevel(new Qname("MX.secureLevelKm10"));
				species.setTaxonRank(new Qname("MX.species"));

				container.addTaxon(subsp);
				container.addTaxon(species);

				return subsp;
			}

			if (taxonId.equals(KANAHAUKKA_ID)) {
				Taxon species = new Taxon(KANAHAUKKA_ID, null);
				species.setBreedingSecureLevel(new Qname("MX.secureLevelKm10")); // custom breeding season 1.3.-31.8.
				species.setNestSiteSecureLevel(new Qname("MX.secureLevelKm10"));
				species.setTaxonRank(new Qname("MX.species"));
				return species;
			}

			if (taxonId.equals(MAAKOTKA_ID)) {
				Taxon taxon = new Taxon(MAAKOTKA_ID, null);
				taxon.setSecureLevel(new Qname("MX.secureLevelKm50")); // always at least 50KM
				taxon.setNestSiteSecureLevel(new Qname("MX.secureLevelKm100"));
				taxon.setBreedingSecureLevel(new Qname("MX.secureLevelKm100")); // custom breeding season 1.2.-31.8.
				taxon.setNaturaAreaSecureLevel(new Qname("MX.secureLevelHighest"));
				return taxon;
			}

			if (taxonId.equals(KUUKKELI_ID)) {
				Taxon taxon = new Taxon(KUUKKELI_ID, null);
				taxon.setSecureLevel(new Qname("MX.secureLevelKm100")); // only secure in the south; otherwise no secure at all
				return taxon;
			}

			if (taxonId.equals(SUOSIRRI_ID)) {
				Taxon taxon = new Taxon(SUOSIRRI_ID, null);
				taxon.setNestSiteSecureLevel(new Qname("MX.secureLevelKm25")); // only secure in the south, otherwise no secure at all
				taxon.setBreedingSecureLevel(new Qname("MX.secureLevelKm25")); // only secure in the south, otherwise no secure at all
				return taxon;
			}

			throw new NoSuchTaxonException(taxonId);
		}

		@Override
		public boolean hasTaxon(Qname taxonId) {
			try {
				getTaxon(taxonId);
				return true;
			} catch (NoSuchTaxonException e) {
				return false;
			}
		}

		@Override
		public Map<String, CollectionMetadata> getCollections() {
			Map<String, CollectionMetadata> map = new LinkedHashMap<>();
			CollectionMetadata c1 = new CollectionMetadata(COL_ID_HAS_FOUR_YEARS_QUARANTINE, null, null);
			CollectionMetadata c2 = new CollectionMetadata(COL_ID_HAS_FEW_DAYS_QUARANTINE, null, null);
			CollectionMetadata c3 = new CollectionMetadata(new Qname("HR.1"), null, null);
			CollectionMetadata c4 = new CollectionMetadata(new Qname("HR.128"), null, null);
			CollectionMetadata c5 = new CollectionMetadata(COLL_ID_HAS_SECURE_LEVEL, null, null);
			c1.setDataQuarantinePeriod(4.0);  // 4 years
			c2.setDataQuarantinePeriod(0.01);
			c5.setSecureLevel(new Qname("MX.secureLevelKM1"));
			map.put(c1.getQname().toURI(), c1);
			map.put(c2.getQname().toURI(), c2);
			map.put(c3.getQname().toURI(), c3);
			map.put(c4.getQname().toURI(), c4);
			map.put(c5.getQname().toURI(), c5);
			return map;
		}

		@Override
		public ETLDAO getETLDAO() {
			return new ETLDAOStub() {
				@Override
				public Qname secureIndividualId(Qname individualId) {
					return Util.secureIndividualId(individualId, "saltsaltsalt");
				}

				@Override
				public SplittedDocumentIds getOrGenerateSplittedDocumentIds(Qname originalDocumentId, List<Qname> originalUnitIds) {
					SplittedDocumentIds splitted = new SplittedDocumentIds();
					int i = 1;
					for (Qname originalUnitId : originalUnitIds) {
						Qname splittedDocumentId = new Qname("A.ABC"+(i++));
						splitted.add(originalDocumentId, originalUnitId, splittedDocumentId);
					}
					return splitted;
				}

			};
		}

	}

	private static SecurerTestDao dao;
	private static Securer securer;
	private static Interpreter interpreter;

	@BeforeClass
	public static void before() {
		dao = new SecurerTestDao();
		securer = new Securer(dao);
		interpreter = new Interpreter(dao);
	}

	@AfterClass
	public static void after() {
		dao.close();
	}

	@Test
	public void test_quarantine_period__none_defined_for_collection() throws Exception {
		//		periods.put("http://tun.fi/4years", Double.valueOf("4"));
		//		periods.put("http://tun.fi/fewdays", Double.valueOf("0.01"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), new Date()));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.NONE, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals("[]", dwRoot.getPublicDocument().getSecureReasons().toString());
		assertEquals(false, dwRoot.getPublicDocument().isSecured());

		assertTrue(dwRoot.getPrivateDocument() == null);
	}

	@Test
	public void test_quarantine_period__4_years_defined_for_collection() throws Exception {
		//		periods.put("http://tun.fi/4years", Double.valueOf("4"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FOUR_YEARS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), new Date()));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM100, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DATA_QUARANTINE_PERIOD, dwRoot.getPublicDocument().getSecureReasons().get(0));
		assertEquals(true, dwRoot.getPublicDocument().isSecured());

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_quarantine_period__4_years_defined_for_collection__but_has_atlas_codes() throws Exception {
		//		periods.put("http://tun.fi/4years", Double.valueOf("4"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FOUR_YEARS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), new Date()));

		Unit u = new Unit(new Qname("12345#2"));
		u.setTaxonVerbatim("talitiainen");
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum64"));
		gathering.addUnit(u);

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DATA_QUARANTINE_PERIOD, dwRoot.getPublicDocument().getSecureReasons().get(0));
		assertEquals(true, dwRoot.getPublicDocument().isSecured());

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	private void assertPublicPrivateHaveIdenticalReasons(DwRoot dwRoot) {
		assertTrue(dwRoot.getPublicDocument().getSecureReasons().equals(dwRoot.getPrivateDocument().getSecureReasons()));
		assertTrue(dwRoot.getPublicDocument().isSecured() == dwRoot.getPrivateDocument().isSecured());
	}

	@Test
	public void test_quarantine_period__4_years_defined_for_collection_2() throws Exception {
		//		periods.put("http://tun.fi/4years", Double.valueOf("4"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FOUR_YEARS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), toDayMinusDays(365*3)));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM100, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DATA_QUARANTINE_PERIOD, dwRoot.getPublicDocument().getSecureReasons().get(0));

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_quarantine_period__4_years_defined_for_collection_3() throws Exception {
		//		periods.put("http://tun.fi/4years", Double.valueOf("4"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FOUR_YEARS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), toDayMinusDays(365*4 + 30)));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.NONE, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals("[]", dwRoot.getPublicDocument().getSecureReasons().toString());

		assertTrue(dwRoot.getPrivateDocument() == null);
	}

	@Test
	public void test_quarantine_period__freaction_of_years_defined_for_collection() throws Exception {
		// periods.put("http://tun.fi/fewdays", Double.valueOf("0.01"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FEW_DAYS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), toDayMinusDays(1)));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM100, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DATA_QUARANTINE_PERIOD, dwRoot.getPublicDocument().getSecureReasons().get(0));

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_quarantine_period__freaction_of_years_defined_for_collection_2() throws Exception {
		// periods.put("http://tun.fi/fewdays", Double.valueOf("0.01"));
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COL_ID_HAS_FEW_DAYS_QUARANTINE);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), toDayMinusDays(10)));

		securer.secure(dwRoot);
		assertEquals(SecureLevel.NONE, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(0, dwRoot.getPublicDocument().getSecureReasons().size());

		assertTrue(dwRoot.getPrivateDocument() == null);
	}

	private Date toDayMinusDays(int minusDays) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_YEAR, -1 * minusDays);
		return c.getTime();
	}

	@Test
	public void test_secure_level_defined_for_collection() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(COLL_ID_HAS_SECURE_LEVEL);
		dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("12345#1"));
		dwRoot.getPublicDocument().addGathering(gathering);

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM1, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals("[CUSTOM]", dwRoot.getPublicDocument().getSecureReasons().toString());

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_custom_secured_public() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().setSecureLevel(SecureLevel.KM10);
		securer.secure(dwRoot);

		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.CUSTOM, dwRoot.getPublicDocument().getSecureReasons().get(0));

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_private_will_not_be_secured_if_not_a_private_secure_taxa() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(666, 333, Type.YKJ));
		Unit u= new Unit(new Qname("uid"));
		u.setTaxonVerbatim(NIITTYLAUKKA); // highest
		dwRoot.getPublicDocument().getGatherings().get(0).addUnit(u);
		dwRoot.setPrivateDocument(dwRoot.getPublicDocument().copy(Concealment.PRIVATE));
		assertFalse(dwRoot.hasSplittedPublicDocuments());

		securer.secure(dwRoot);

		assertEquals(SecureLevel.HIGHEST, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, dwRoot.getPublicDocument().getSecureReasons().get(0));
		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates());

		assertPublicPrivateHaveIdenticalReasons(dwRoot);

		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates());
		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : null", dwRoot.getPrivateDocument().getGatherings().get(0).getCoordinates().toString());
	}

	@Test
	public void test_private_will_be_secured_if_a_private_secure_taxa() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(666, 333, Type.YKJ));
		Unit u= new Unit(new Qname("uid"));
		u.setTaxonVerbatim(TUNTURIHAUKKA); // highest - also private secured taxa
		dwRoot.getPublicDocument().getGatherings().get(0).addUnit(u);
		dwRoot.setPrivateDocument(dwRoot.getPublicDocument().copy(Concealment.PRIVATE));
		assertFalse(dwRoot.hasSplittedPublicDocuments());

		securer.secure(dwRoot);

		assertEquals(SecureLevel.HIGHEST, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, dwRoot.getPublicDocument().getSecureReasons().get(0));
		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates());
		assertEquals(null, dwRoot.getPrivateDocument().getGatherings().get(0).getCoordinates());
		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void test_secure_level_for_breedingsite() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		Unit unit = new Unit(new Qname("u123"));
		unit.setTaxonVerbatim(HYPPYS_HYPPYS); // secure level 1km for nest site
		unit.setBreedingSite(true);
		dwRoot.getPublicDocument().getGatherings().get(0).addUnit(unit);

		securer.secure(dwRoot);

		assertEquals(SecureLevel.NONE, dwRoot.getPrivateDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPrivateDocument().getSecureReasons().size());
		assertEquals(SecureReason.BREEDING_SITE_CONSERVATION, dwRoot.getPrivateDocument().getSecureReasons().get(0));

		assertEquals(SecureLevel.KM1, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, dwRoot.getPublicDocument().getSecureReasons().get(0)); // nest site conservation changed to taxon conservation for public
	}

	@Test
	public void test_secure_level_for_natura_area() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		Unit unit = new Unit(new Qname("u123"));
		unit.setTaxonVerbatim(HYPPYS_HYPPYS); // secure level 10km for natura area
		dwRoot.getPublicDocument().getGatherings().get(0).addUnit(unit);
		dwRoot.getPublicDocument().getGatherings().get(0).createInterpretations().addToNaturaAreas(new Qname("syke:FI1234"));

		securer.secure(dwRoot);

		assertEquals(SecureLevel.NONE, dwRoot.getPrivateDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPrivateDocument().getSecureReasons().size());
		assertEquals(SecureReason.NATURA_AREA_CONSERVATION, dwRoot.getPrivateDocument().getSecureReasons().get(0));

		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(1, dwRoot.getPublicDocument().getSecureReasons().size());
		assertEquals(SecureReason.DEFAULT_TAXON_CONSERVATION, dwRoot.getPublicDocument().getSecureReasons().get(0)); // natura area reason changed to taxon conservation reason for public
	}

	@Test
	public void test_highest_level() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("GV.1"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		Document document = dwRoot.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("GV.1#1"));
		Unit unit1 = new Unit(new Qname("GV.1#2"));
		Unit unit2 = new Unit(new Qname("GV.1#3"));
		document.addGathering(gathering);
		gathering.addUnit(unit1);
		gathering.addUnit(unit2);

		unit1.setTaxonVerbatim("Parus major"); // no secure level
		unit2.setTaxonVerbatim(TUNTURIHAUKKA); // secure highest level
		unit2.addAnnotation(new Annotation(new Qname("MAN.1"), document.getDocumentId(), unit2.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00")).secureAnnotation());

		gathering.setCoordinates(new Coordinates(60.123456, 30.123456, Type.WGS84));
		gathering.setLocality("Metsäkellonkatu 12");

		assertEquals(null, dwRoot.getPrivateDocument());

		securer.secure(dwRoot);

		assertEquals(1, dwRoot.getSplittedPublicDocuments().size());
		Document splittedDocument = dwRoot.getSplittedPublicDocuments().get(0);

		assertEquals(60.123456, dwRoot.getPrivateDocument().getGatherings().get(0).getCoordinates().getLatMax(), 0);
		assertEquals("Metsäkellonkatu 12", dwRoot.getPrivateDocument().getGatherings().get(0).getLocality());
		assertEquals(60.123456, dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates().getLatMax(), 0);
		assertEquals("Metsäkellonkatu 12", dwRoot.getPublicDocument().getGatherings().get(0).getLocality());

		assertEquals(null, splittedDocument.getGatherings().get(0).getCoordinates());
		assertEquals(null, splittedDocument.getGatherings().get(0).getLocality());

		assertTrue(dwRoot.getPublicDocument().isPartial());
		assertTrue(splittedDocument.isPartial());
		assertFalse(dwRoot.getPrivateDocument().isPartial());

		assertEquals("[]", dwRoot.getPublicDocument().getSecureReasons().toString());
		assertEquals("[DEFAULT_TAXON_CONSERVATION]", dwRoot.getPrivateDocument().getSecureReasons().toString());
		assertEquals("[DEFAULT_TAXON_CONSERVATION]", splittedDocument.getSecureReasons().toString());

		assertEquals(2, dwRoot.getPrivateDocument().getGatherings().size());
		assertEquals(1, dwRoot.getPrivateDocument().getGatherings().get(0).getUnits().size());
		assertEquals(1, dwRoot.getPrivateDocument().getGatherings().get(1).getUnits().size());
		assertEquals(1, dwRoot.getPublicDocument().getGatherings().get(0).getUnits().size());
		assertEquals(1, splittedDocument.getGatherings().get(0).getUnits().size());

		assertEquals(SecureLevel.NONE, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(SecureLevel.NONE, dwRoot.getPrivateDocument().getSecureLevel());
		assertEquals(SecureLevel.HIGHEST, splittedDocument.getSecureLevel());

		assertEquals(Concealment.PRIVATE, dwRoot.getPrivateDocument().getConcealment());
		assertEquals(Concealment.PUBLIC, dwRoot.getPublicDocument().getConcealment());
		assertEquals(Concealment.PUBLIC, splittedDocument.getConcealment());

		assertEquals("GV.1", dwRoot.getPrivateDocument().getDocumentId().toString());
		assertEquals("GV.1", dwRoot.getPublicDocument().getDocumentId().toString());
		assertEquals("A.ABC1", splittedDocument.getDocumentId().toString());

		assertEquals("GV.1#1", dwRoot.getPrivateDocument().getGatherings().get(0).getGatheringId().toString());
		assertEquals("GV.1_1_GV.1_3", dwRoot.getPrivateDocument().getGatherings().get(1).getGatheringId().toString());
		assertEquals("GV.1#1", dwRoot.getPublicDocument().getGatherings().get(0).getGatheringId().toString());
		assertEquals("A.ABC1#G", splittedDocument.getGatherings().get(0).getGatheringId().toString());

		assertEquals("GV.1#2", dwRoot.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getUnitId().toString());
		assertEquals("GV.1#3", dwRoot.getPrivateDocument().getGatherings().get(1).getUnits().get(0).getUnitId().toString());
		assertEquals("GV.1#2", dwRoot.getPublicDocument().getGatherings().get(0).getUnits().get(0).getUnitId().toString());
		assertEquals("A.ABC1#U", splittedDocument.getGatherings().get(0).getUnits().get(0).getUnitId().toString());

		assertEquals(1, splittedDocument.getGatherings().get(0).getUnits().get(0).getAnnotations().size());

		assertEquals("60.123456 : 60.123456 : 30.123456 : 30.123456 : WGS84 : null", dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		assertEquals(null, splittedDocument.getGatherings().get(0).getCoordinates());
		assertEquals("60.123456 : 60.123456 : 30.123456 : 30.123456 : WGS84 : null", dwRoot.getPrivateDocument().getGatherings().get(0).getCoordinates().toString());
		assertEquals(null, dwRoot.getPrivateDocument().getGatherings().get(1).getCoordinates());
	}

	@Test
	public void concealing_finnish_in_ykj_rest_in_wgs84() throws Exception {
		DwRoot root = new DwRoot(new Qname("F.1"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));
		Document doc = root.createPublicDocument();
		doc.setSecureLevel(SecureLevel.KM100);

		Gathering g1 = new Gathering(new Qname("g1"));
		Gathering g2 = new Gathering(new Qname("g2"));
		Gathering g3 = new Gathering(new Qname("g3"));
		Gathering g4 = new Gathering(new Qname("g4"));
		Gathering g5 = new Gathering(new Qname("g5"));
		Gathering g6 = new Gathering(new Qname("g6"));

		g1.setCoordinates(new Coordinates(60.2, 25.0, Type.WGS84)); // inside Finland, wgs84
		g2.setCoordinates(new Coordinates(-60.2, 25.0, Type.WGS84)); // outside Finland, wgs84
		g3.setCoordinates(new Coordinates(66666, 33333, Type.YKJ)); // inside Finland, ykj
		g4.setCoordinates(new Coordinates(68503, 37501, Type.YKJ)); // outside Finland, ykj
		g5.setCoordinates(new Coordinates(7019585, 599795, Type.EUREF)); // inside Finland, euref
		g6.setCoordinates(new Coordinates(7119425, 206579, Type.EUREF)); // outside Finland, euref

		doc.addGathering(g1).addGathering(g2).addGathering(g3).addGathering(g4).addGathering(g5).addGathering(g6);

		interpreter.interpret(doc);

		assertEquals(Type.WGS84, g1.getInterpretations().getCoordinates().getType());
		assertEquals(Const.FINLAND, g1.getInterpretations().getCountry());
		assertEquals(Type.WGS84, g2.getInterpretations().getCoordinates().getType());
		assertEquals(null, g2.getInterpretations().getCountry());
		assertEquals(Type.YKJ, g3.getInterpretations().getCoordinates().getType());
		assertEquals(Const.FINLAND, g3.getInterpretations().getCountry());
		assertEquals(Type.YKJ, g4.getInterpretations().getCoordinates().getType());
		assertEquals(null, g4.getInterpretations().getCountry());
		assertEquals(Type.EUREF, g5.getInterpretations().getCoordinates().getType());
		assertEquals(Const.FINLAND, g5.getInterpretations().getCountry());
		assertEquals(Type.EUREF, g6.getInterpretations().getCoordinates().getType());
		assertEquals(null, g6.getInterpretations().getCountry());

		securer.secure(root);

		doc = root.getPublicDocument();
		g1 = doc.getGatherings().get(0);
		g2 = doc.getGatherings().get(1);
		g3 = doc.getGatherings().get(2);
		g4 = doc.getGatherings().get(3);
		g5 = doc.getGatherings().get(4);
		g6 = doc.getGatherings().get(5);

		assertEquals("g1", g1.getGatheringId().toString());
		assertEquals("g2", g2.getGatheringId().toString());
		assertEquals("g3", g3.getGatheringId().toString());
		assertEquals("g4", g4.getGatheringId().toString());
		assertEquals("g5", g5.getGatheringId().toString());
		assertEquals("g6", g6.getGatheringId().toString());

		assertEquals(Type.YKJ, g1.getInterpretations().getCoordinates().getType()); // wgs84 -> ykj
		assertEquals(Const.FINLAND, g1.getInterpretations().getCountry());

		assertEquals(Type.WGS84, g2.getInterpretations().getCoordinates().getType()); // wgs84 -> wgs84
		assertEquals(null, g2.getInterpretations().getCountry());

		assertEquals(Type.YKJ, g3.getInterpretations().getCoordinates().getType());  // ykj -> ykj
		assertEquals(Const.FINLAND, g3.getInterpretations().getCountry());

		assertEquals(Type.WGS84, g4.getInterpretations().getCoordinates().getType()); // ykj -> gws84
		assertEquals(null, g4.getInterpretations().getCountry());

		assertEquals(Type.YKJ, g5.getInterpretations().getCoordinates().getType()); // euref -> ykj
		assertEquals(Const.FINLAND, g5.getInterpretations().getCountry());

		assertEquals(Type.WGS84, g6.getInterpretations().getCoordinates().getType()); // euref -> wgs84
		assertEquals(null, g6.getInterpretations().getCountry());
	}

	@Test
	public void test_setting_custom_level() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().setSecureLevel(SecureLevel.KM10);

		securer.secure(dwRoot);
		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(SecureReason.CUSTOM, dwRoot.getPublicDocument().getSecureReasons().get(0));

		assertPublicPrivateHaveIdenticalReasons(dwRoot);
	}

	@Test
	public void securingIndividualids() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		Document pub = dwRoot.createPublicDocument();
		Document priv = dwRoot.createPrivateDocument();
		Unit pubUnit = new Unit(new Qname("u1"));
		Unit privUnit = new Unit(new Qname("u1"));
		pubUnit.setIndividualIdUsingQname(RING_SFH_A12345);
		privUnit.setIndividualIdUsingQname(RING_SFH_A12345);

		pub.addGathering(new Gathering(new Qname("g1")).addUnit(pubUnit));
		priv.addGathering(new Gathering(new Qname("g1")).addUnit(privUnit));

		securer.secure(dwRoot);

		assertEquals("http://tun.fi/A.F66E71C48CD6C7524B67EC879BE5BF30", pubUnit.getIndividualId());
		assertEquals(RING_SFH_A12345.toURI(), privUnit.getIndividualId());
	}

	@Test
	public void securingHashMarkedUnits() throws Exception {
		Unit u1 = new Unit(new Qname("u1"));
		Unit u2 = new Unit(new Qname("u2"));
		u2.setTaxonVerbatim("blaa#");

		DwRoot root = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		root.setCollectionId(new Qname("HR.128"));
		Document doc = root.createPublicDocument();
		doc.addGathering(new Gathering(new Qname("g1")).addUnit(u1).addUnit(u2));

		securer.secure(root);

		assertEquals(1, root.getSplittedPublicDocuments().size());
		Document splittedDoc = root.getSplittedPublicDocuments().get(0);

		assertEquals(1, root.getPublicDocument().getGatherings().get(0).getUnits().size());
		assertEquals(1, splittedDoc.getGatherings().get(0).getUnits().size());
		assertEquals(2, root.getPrivateDocument().getGatherings().get(0).getUnits().size());

		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals(SecureLevel.KM100, splittedDoc.getSecureLevel());

		assertEquals("[]", root.getPublicDocument().getSecureReasons().toString());
		assertEquals("[USER_HIDDEN]", splittedDoc.getSecureReasons().toString());
	}

	@Test
	public void clearing_geo() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.128"));
		dwRoot.createPublicDocument().setSecureLevel(SecureLevel.KM10);
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("g1")));
		dwRoot.getPublicDocument().getGatherings().get(0).setGeo(new Geo(Type.WGS84).addFeature(Point.from(60.12345, 30.12345)));
		dwRoot.getPublicDocument().getGatherings().get(0).createInterpretations();
		dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().setCoordinates(new Coordinates(60.12345, 30.12345, Type.WGS84));

		securer.secure(dwRoot);

		assertEquals(SecureLevel.KM10, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(SecureReason.CUSTOM, dwRoot.getPublicDocument().getSecureReasons().get(0));
		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getGeo());
		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getCoordinates());
		assertEquals("60.1 : 60.2 : 30.1 : 30.2 : WGS84 : 10000", (dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinates().toString()));

		assertEquals("POINT (30.12345 60.12345)", dwRoot.getPrivateDocument().getGatherings().get(0).getGeo().getWKT());
		assertEquals(null, dwRoot.getPrivateDocument().getGatherings().get(0).getCoordinates());
		assertEquals("60.12345 : 60.12345 : 30.12345 : 30.12345 : WGS84 : null", dwRoot.getPrivateDocument().getGatherings().get(0).getInterpretations().getCoordinates().toString());

	}

	@Test
	public void no_secure_if_not_inside_secure_area() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		Gathering g1 = new Gathering(new Qname("g1"));
		Gathering g2 = new Gathering(new Qname("g2"));
		Gathering g3 = new Gathering(new Qname("g3"));
		Gathering g4 = new Gathering(new Qname("g4"));
		Gathering g5 = new Gathering(new Qname("g5"));

		doc.addGathering(g1).addGathering(g2).addGathering(g3).addGathering(g4).addGathering(g5);

		g1.setCoordinates(new Coordinates(60.2, 25.0, Type.WGS84)); // inside secure area
		g2.setCoordinates(new Coordinates(-60.2, 25.0, Type.WGS84)); // not inside secure area
		g3.setCountry("ruotsi"); // inside secure area
		g4.setCountry("kanada"); // not inside securea area
		g5.setCountry("blablaa"); // don't know if inside secure area

		Unit g1u1 = new Unit(new Qname("g1u1")); // highest secure level -> should secure even if not inside secure area
		g1u1.setTaxonVerbatim(TUNTURIHAUKKA);

		Unit g1u2 = new Unit(new Qname("g1u2")); // 1km for nest site
		g1u2.setTaxonVerbatim(HYPPYS_HYPPYS);
		g1u2.setBreedingSite(true);

		Unit g1u3 = new Unit(new Qname("g1u3")); // should secure in Finland - but not outside secure area
		g1u3.setTaxonVerbatim(KANAGSKAARME);

		Unit g2u1 = g1u1.copy(); g2u1.setUnitId(new Qname("g2u1"));
		Unit g2u2 = g1u2.copy(); g2u2.setUnitId(new Qname("g2u2"));
		Unit g2u3 = g1u3.copy(); g2u3.setUnitId(new Qname("g2u3"));
		Unit g3u1 = g1u1.copy(); g3u1.setUnitId(new Qname("g3u1"));
		Unit g3u2 = g1u2.copy(); g3u2.setUnitId(new Qname("g3u2"));
		Unit g3u3 = g1u3.copy(); g3u3.setUnitId(new Qname("g3u3"));
		Unit g4u1 = g1u1.copy(); g4u1.setUnitId(new Qname("g4u1"));
		Unit g4u2 = g1u2.copy(); g4u2.setUnitId(new Qname("g4u2"));
		Unit g4u3 = g1u3.copy(); g4u3.setUnitId(new Qname("g4u3"));
		Unit g5u1 = g1u1.copy(); g5u1.setUnitId(new Qname("g5u1"));
		Unit g5u2 = g1u2.copy(); g5u2.setUnitId(new Qname("g5u2"));
		Unit g5u3 = g1u3.copy(); g5u3.setUnitId(new Qname("g5u3"));

		g1.addUnit(g1u1).addUnit(g1u2).addUnit(g1u3);
		g2.addUnit(g2u1).addUnit(g2u2).addUnit(g2u3);
		g3.addUnit(g3u1).addUnit(g3u2).addUnit(g3u3);
		g4.addUnit(g4u1).addUnit(g4u2).addUnit(g4u3);
		g5.addUnit(g5u1).addUnit(g5u2).addUnit(g5u3);

		interpreter.interpret(dwRoot);
		securer.secure(dwRoot);

		assertEquals(13, dwRoot.getSplittedPublicDocuments().size());

		doc = dwRoot.getPublicDocument();
		System.out.println(doc.getGatherings().stream().map(g->g.getGatheringId().toString()).collect(Collectors.joining()));
		//assertEquals(2, doc.getGatherings().size());
		//g1 = doc.getGatherings().get(); all units from g1 have been splitted -> the gathering does not exists anymore in the document
		g2 = doc.getGatherings().get(0);
		//g3 = doc.getGatherings().get(); all units have been splitted
		g4 = doc.getGatherings().get(1);
		//g5 = doc.getGatherings().get(); all units have been splitted

		assertEquals("g2", g2.getGatheringId().toString());
		assertEquals("g4", g4.getGatheringId().toString());

		assertEquals(1, g2.getUnits().size()); // not inside secure area (based on coordinates):  highest and breeding site is splitted, meritkotka remains because not in secure area (and not a breeding site)
		assertEquals(1, g4.getUnits().size()); // not inside secure area (based on country name): highest and breeding site is splitted, meritkotka remains because not in secure area (and not a breeding site)

	}

	@Test
	public void test_secure_level_based_on_all_annotated_identifications_and_verbatim_and_reportedtaxonid() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		Gathering g1 = new Gathering(new Qname("g1"));
		doc.addGathering(g1);

		Unit u1 = new Unit(new Qname("u1"));
		u1.setTaxonVerbatim("foobar");
		Annotation u1A = new Annotation(new Qname("MAN.1"), doc.getDocumentId(), u1.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		u1A.setIdentification(new Identification(TUNTURIHAUKKA)); // expert annotation -> changes identification from foobar to tunturihaukka
		u1A.addTag(Tag.EXPERT_TAG_VERIFIED);
		u1.addAnnotation(u1A);

		Unit u2 = new Unit(new Qname("u2"));
		u2.setTaxonVerbatim(TUNTURIHAUKKA);
		Annotation u2A = new Annotation(new Qname("MAN.2"), doc.getDocumentId(), u2.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		u2A.setIdentification(new Identification("parus major")); // not an expert identification -> does not change taxon id
		u2.addAnnotation(u2A);

		Unit u3 = new Unit(new Qname("u3"));
		u3.setReportedTaxonId(TUNTURIHAUKKAID);

		g1.addUnit(u1).addUnit(u2).addUnit(u3);

		interpreter.interpret(doc);
		securer.secure(dwRoot);

		assertEquals(0, dwRoot.getPublicDocument().getGatherings().size());
		assertEquals(3, dwRoot.getSplittedPublicDocuments().size());
		for (Document d : dwRoot.getSplittedPublicDocuments()) {
			assertEquals(SecureLevel.HIGHEST, d.getSecureLevel());
		}
	}

	@Test
	public void test_spam_annotation() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		Gathering g1 = new Gathering(new Qname("g1"));
		doc.addGathering(g1);

		Unit u1 = new Unit(new Qname("u1"));
		u1.setTaxonVerbatim("foobar");
		Annotation u1A = new Annotation(new Qname("MAN.1"), doc.getDocumentId(), u1.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		u1A.addTag(Tag.ADMIN_MARKED_SPAM);
		u1.addAnnotation(u1A);

		Unit u2 = new Unit(new Qname("u2"));
		u2.setTaxonVerbatim(TUNTURIHAUKKA);

		g1.addUnit(u1).addUnit(u2);

		interpreter.interpret(doc);
		securer.secure(dwRoot);

		assertEquals(null, dwRoot.getSplittedPublicDocuments());
		assertEquals(true, dwRoot.getPublicDocument().isDeleted());
		assertEquals(true, dwRoot.getPrivateDocument().isDeleted());
		assertEquals(true, dwRoot.isDeleteRequest());
	}

	@Test
	public void test_securing_large_areas() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		doc.setSecureLevel(SecureLevel.KM10);
		doc.addSecureReason(SecureReason.USER_HIDDEN);
		Gathering g = new Gathering(new Qname("g1"));
		doc.addGathering(g);
		g.setMunicipality("Enontekiö");
		Unit u = new Unit(new Qname("u1"));
		u.setTaxonVerbatim("foobar");
		g.addUnit(u);

		interpreter.interpret(doc);

		assertEquals("68.115926 : 69.311877 : 20.548615 : 25.027173 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString()); // Original coordinates are a large WGS84 box
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(100000, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());

		securer.secure(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);

		Coordinates expected = CoordinateConverter.convert(g.getInterpretations().getCoordinates()).getYkj();
		expected.setAccuracyInMeters(expected.calculateBoundingBoxAccuracy());

		Coordinates actual = g.getInterpretations().getCoordinates().toFullYkjLength();

		assertEquals(expected.toString(), actual.toString()); // Secured in Finland area is converted to YKJ box
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(100000, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());
	}

	@Test
	public void test_securing_large_area_from_abroad() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		doc.setSecureLevel(SecureLevel.KM10);
		doc.addSecureReason(SecureReason.USER_HIDDEN);
		Gathering g = new Gathering(new Qname("g1"));
		doc.addGathering(g);
		g.setCoordinates(new Coordinates(-1, 3, 40, 80, Type.WGS84));
		Unit u = new Unit(new Qname("u1"));
		u.setTaxonVerbatim("foobar");
		g.addUnit(u);

		interpreter.interpret(doc);

		assertEquals("-1.0 : 3.0 : 40.0 : 80.0 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString()); // Large area
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(100000, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());

		securer.secure(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);

		Coordinates expected = g.getInterpretations().getCoordinates().copy();
		expected.setAccuracyInMeters(expected.calculateBoundingBoxAccuracy());

		assertEquals(expected.toString(), g.getInterpretations().getCoordinates().toString()); // Large area as it is, not secured to KM10
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(100000, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());
	}

	@Test
	public void test_securing__from_abroad() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		doc.setSecureLevel(SecureLevel.KM100);
		doc.addSecureReason(SecureReason.CUSTOM);
		Gathering g = new Gathering(new Qname("g1"));
		doc.addGathering(g);
		g.setCoordinates(new Coordinates(38.450719, -9.090514, Type.WGS84));

		Unit u = new Unit(new Qname("u1"));
		u.setTaxonVerbatim("foobar");
		g.addUnit(u);

		interpreter.interpret(doc);

		assertEquals("38.450719 : 38.450719 : -9.090514 : -9.090514 : WGS84 : 1", g.getInterpretations().getCoordinates().toString());
		assertEquals(1, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(1, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());

		securer.secure(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);

		assertEquals("38.0 : 39.0 : -10.0 : -9.0 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString()); // Area secured to KM10K
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(100000, g.getInterpretations().getCoordinates().getAccuracyInMeters().intValue());
	}

	@Test
	public void test_securing_custom_secured_issues() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document doc = dwRoot.createPublicDocument();
		doc.addSecureReason(SecureReason.CUSTOM);
		Gathering g = new Gathering(new Qname("g1"));
		doc.addGathering(g);
		g.createQuality().setLocationIssue(new Quality(Issue.INVALID_YKJ_COORDINATES, Source.AUTOMATED_FINBIF_VALIDATION, "Invalid YKJ: 12345:67890"));

		interpreter.interpret(doc);

		assertEquals(null, dwRoot.getPrivateDocument());
		securer.secure(dwRoot);

		assertEquals("Invalid YKJ: 12345:67890", dwRoot.getPrivateDocument().getGatherings().get(0).getQuality().getLocationIssue().getMessage());
		assertEquals(null, dwRoot.getPublicDocument().getGatherings().get(0).getQuality().getLocationIssue().getMessage());
		assertEquals(Issue.INVALID_YKJ_COORDINATES, dwRoot.getPublicDocument().getGatherings().get(0).getQuality().getLocationIssue().getIssue());
	}

	@Test
	public void securing_where_fact_value_contains_a_secured_species() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document d = dwRoot.createPublicDocument();
		Gathering g = new Gathering(new Qname("g1"));
		d.addGathering(g);
		Unit u = new Unit(new Qname("u1"));
		g.addUnit(u);

		u.setTaxonVerbatim("nothing to secure here");
		d.addFact("somefact", TUNTURIHAUKKAID.toURI());
		d.addFact("somefact", PARUS_MAJOR);
		d.addFact("somefact", "harmless");
		g.addFact("somefact", TUNTURIHAUKKA);
		g.addFact("somefact", "harmless");

		interpreter.interpret(d);
		securer.secure(dwRoot);

		assertNotNull(dwRoot.getPrivateDocument());
		assertNotNull(dwRoot.getPublicDocument());

		assertEquals(SecureLevel.HIGHEST, dwRoot.getPublicDocument().getSecureLevel());
		assertEquals(SecureLevel.NONE, dwRoot.getPrivateDocument().getSecureLevel());
		assertEquals("["+SecureReason.DEFAULT_TAXON_CONSERVATION+"]", dwRoot.getPublicDocument().getSecureReasons().toString());
		assertEquals("["+SecureReason.DEFAULT_TAXON_CONSERVATION+"]", dwRoot.getPrivateDocument().getSecureReasons().toString());

		assertEquals("[]", dwRoot.getPublicDocument().getFacts().toString());
		assertEquals("[somefact : "+TUNTURIHAUKKAID.toURI()+", somefact : parus major, somefact : harmless]", dwRoot.getPrivateDocument().getFacts().toString());
		assertEquals("[]", dwRoot.getPublicDocument().getGatherings().get(0).getFacts().toString());
		assertEquals("[somefact : "+TUNTURIHAUKKA+", somefact : harmless]", dwRoot.getPrivateDocument().getGatherings().get(0).getFacts().toString());

		assertEquals(1, dwRoot.getPrivateDocument().getGatherings().get(0).getUnits().size());
		assertEquals(1, dwRoot.getPublicDocument().getGatherings().get(0).getUnits().size());
	}

	@Test
	public void securing_long_timeframe() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document d = dwRoot.createPublicDocument();
		Gathering g = new Gathering(new Qname("g1"));
		d.addGathering(g);
		g.setGeo(Geo.fromWKT("POLYGON ((517178.7406015 7241953.7406015, 518178.7406015 7241953.7406015, 518178.7406015 7242953.7406015, 517178.7406015 7242953.7406015, 517178.7406015 7241953.7406015))", Type.EUREF));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2007"), DateUtils.convertToDate("31.12.2021")));

		d.setSecureLevel(SecureLevel.KM5);
		d.addSecureReason(SecureReason.CUSTOM);

		interpreter.interpret(d);
		securer.secure(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("DateRange [begin=2007-01-01, end=2021-12-31]", g.getEventDate().toString());
		assertEquals(null, g.getGeo());
		assertEquals("7245.0 : 7250.0 : 3515.0 : 3520.0 : YKJ : 5000", g.getInterpretations().getCoordinates().toString());
	}

	@Test
	public void ykj_boxes_as_wgs84_not_to_coarse_to_bigger_box_if_only_slightly_outside_box() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document d = dwRoot.createPublicDocument();
		Gathering g = new Gathering(new Qname("g1"));
		d.addGathering(g);
		g.setGeo(Geo.fromWKT("GEOMETRYCOLLECTION(POLYGON((25.679158 60.538284,25.679794 60.551318,25.720568 60.551344,25.72097 60.548698,25.720787 60.545802,25.720994 60.543057,25.721411 60.540069,25.703197 60.537293,25.679158 60.538284)), POINT(25.708514 60.545826), POLYGON((25.541444 60.492809,25.5374 60.582529,25.71978 60.584401,25.723321 60.494674,25.541444 60.492809)))", Type.WGS84));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.7.2021")));

		d.setSecureLevel(SecureLevel.KM10);
		d.addSecureReason(SecureReason.CUSTOM);

		interpreter.interpret(d);
		securer.secure(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("671.0 : 672.0 : 342.0 : 343.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
	}

	@Test
	public void custom_rules_merikotka() throws Exception {
		assertEquals(SecureLevel.NONE, createTestDoc(MERIKOTKAID, "5.1.2023", 66.4, false)); // outside breeding period and not nest site
		assertEquals(SecureLevel.KM50, createTestDoc(MERIKOTKAID, "5.1.2023", 66.4, true)); // is nest side up north
		assertEquals(SecureLevel.KM10, createTestDoc(MERIKOTKAID, "5.1.2023", 60.5, true)); // is nest side down south
		assertEquals(SecureLevel.KM50, createTestDoc(MERIKOTKAID, "5.2.2023", 66.4, false)); // is breeding season and up north
		assertEquals(SecureLevel.KM10, createTestDoc(MERIKOTKAID, "5.3.2023", 60.5, false)); // is breeding season and down south
		assertEquals(SecureLevel.KM50, createTestDoc(MERIKOTKAID, "5.4.2023", null, false)); // is breeding season and unknown location
	}

	@Test
	public void custom_rules_valkoselka() throws Exception {
		assertEquals(SecureLevel.NONE, createTestDoc(VALKOSELKATIKKA_ID, "5.1.2023", 66.4, false)); // outside breeding period and not nest site
		assertEquals(SecureLevel.KM5, createTestDoc(VALKOSELKATIKKA_ID, "5.1.2023", 66.4, true)); // is nest side
		assertEquals(SecureLevel.KM5, createTestDoc(VALKOSELKATIKKA_ID, "5.4.2023", 66.4, false)); // is breeding season
		assertEquals(SecureLevel.HIGHEST, createTestDoc(VALKOSELKATIKKA_ID, "5.4.2023", 66.4, false, "natura")); // is natura area
	}

	@Test
	public void kanahaukka() throws Exception {
		assertEquals(SecureLevel.NONE, createTestDoc(KANAHAUKKA_SUBSPECIES_ID, "5.1.2023", 66.4, false)); // outside breeding period and not nest site
		assertEquals(SecureLevel.KM10, createTestDoc(KANAHAUKKA_SUBSPECIES_ID, "5.1.2023", 66.4, true)); // is nest side
		assertEquals(SecureLevel.KM10, createTestDoc(KANAHAUKKA_SUBSPECIES_ID, "5.3.2023", 66.4, false)); // is breeding season
		assertEquals(SecureLevel.NONE, createTestDoc(KANAHAUKKA_SUBSPECIES_ID, "1.1.2023", 60.5, false, "natura")); // is natura area

		assertEquals(SecureLevel.NONE, createTestDoc(KANAHAUKKA_ID, "5.1.2023", 66.4, false)); // outside breeding period and not nest site
		assertEquals(SecureLevel.KM10, createTestDoc(KANAHAUKKA_ID, "5.1.2023", 66.4, true)); // is nest side
		assertEquals(SecureLevel.KM10, createTestDoc(KANAHAUKKA_ID, "5.3.2023", 66.4, false)); // is breeding season
		assertEquals(SecureLevel.NONE, createTestDoc(KANAHAUKKA_ID, "1.1.2023", 66.4, false, "natura")); // is natura area
	}

	@Test
	public void maakotka() throws Exception {
		assertEquals(SecureLevel.KM50, createTestDoc(MAAKOTKA_ID, "5.1.2023", 66.4, false)); // outside breeding period and not nest site
		assertEquals(SecureLevel.KM100, createTestDoc(MAAKOTKA_ID, "5.1.2023", 66.4, true)); // is nest side
		assertEquals(SecureLevel.KM100, createTestDoc(MAAKOTKA_ID, "5.2.2023", 66.4, false)); // is breeding season
		assertEquals(SecureLevel.HIGHEST, createTestDoc(MAAKOTKA_ID, "1.1.2023", 66.4, false, "natura")); // is natura area
		assertEquals(SecureLevel.HIGHEST, createTestDoc(MAAKOTKA_ID, "4.1.2023", 66.4, true, "natura")); // is natura area
	}

	@Test
	public void kuukkueli() throws Exception {
		assertEquals(SecureLevel.NONE, createTestDoc(KUUKKELI_ID, "1.1.2023", 66.4, false)); // up north
		assertEquals(SecureLevel.KM100, createTestDoc(KUUKKELI_ID, "1.1.2023", 61.0, false)); // down south
	}

	@Test
	public void suorsirri() throws Exception {
		assertEquals(SecureLevel.NONE, createTestDoc(SUOSIRRI_ID, "1.1.2023", 66.4, false)); // up north
		assertEquals(SecureLevel.NONE, createTestDoc(SUOSIRRI_ID, "1.6.2023", 66.4, true)); // up north
		assertEquals(SecureLevel.NONE, createTestDoc(SUOSIRRI_ID, "1.1.2023", 61.0, false)); // down south but nothing to secure
		assertEquals(SecureLevel.KM25, createTestDoc(SUOSIRRI_ID, "1.6.2023", 61.0, false)); // down south
		assertEquals(SecureLevel.KM25, createTestDoc(SUOSIRRI_ID, "1.1.2023", 61.0, true)); // down south
	}

	private SecureLevel createTestDoc(Qname taxonId, String date, Double lat, boolean breedingSite, String natura) throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document d = dwRoot.createPublicDocument();
		Gathering g = new Gathering(new Qname("g1"));
		d.addGathering(g);
		g.setEventDate(new DateRange(DateUtils.convertToDate(date)));
		if (lat != null) {
			g.setCoordinates(new Coordinates(lat, 25.7, Type.WGS84));
		}

		Unit u = new Unit(new Qname("u"));
		u.setReportedTaxonId(taxonId);
		u.setBreedingSite(breedingSite);
		g.addUnit(u);

		interpreter.interpret(dwRoot);
		if (natura != null) {
			dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().addToNaturaAreas(new Qname(natura));
		}
		securer.secure(dwRoot);
		return dwRoot.getPublicDocument().getSecureLevel();
	}

	private SecureLevel createTestDoc(Qname taxonId, String date, Double lat, boolean breedingSite) throws Exception {
		return createTestDoc(taxonId, date, lat, breedingSite, null);
	}

	@Test
	public void slightlyOutsideYKJ10GridAndSecure10KM() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(new Qname("HR.1"));
		Document d = dwRoot.createPublicDocument();
		Gathering g = new Gathering(new Qname("g1"));
		d.addGathering(g);
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.6.2000")));
		g.setGeo(Geo.fromWKT("POLYGON ((22.080795 60.499843, 22.26213 60.506419, 22.275174 60.416974, 22.094333 60.410422, 22.080795 60.499843))", Type.WGS84));
		//                             [22.080795 60.49984   22.26213 60.506418  22.275174 60.416974  22.094333 60.410422  22.080795 60.499842
		Unit u1 = new Unit(new Qname("u1"));
		u1.setReportedTaxonId(KANAHAUKKA_ID); // secure 10km
		u1.setBreedingSite(true);
		g.addUnit(u1);

		Unit u2 = new Unit(new Qname("u2"));
		u2.setTaxonVerbatim("talitiainen"); // no secure
		u2.setBreedingSite(true);
		g.addUnit(u2);

		interpreter.interpret(dwRoot);
		securer.secure(dwRoot);

		Gathering original = dwRoot.getPublicDocument().getGatherings().get(0);
		Gathering splitted = dwRoot.getSplittedPublicDocuments().get(0).getGatherings().get(0);

		assertEquals(10000, original.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(10000, splitted.getInterpretations().getCoordinateAccuracy().intValue());
	}

}
