package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.etl.models.IndividualCountInterpreter.AbundanceStringPart;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiETLJSONHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.LajistoreHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizerTests;
import fi.laji.datawarehouse.etl.models.harmonizers.SimpleDarwinRecordSetHarmonizer;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.FinlandAreaUtil;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.XMLReader;

public class InterpreterTests {

	private static final String ESPOO_ID = "ML.365";
	private static DAOImple dao;
	private static Interpreter interpreter;

	@BeforeClass
	public static void init() {
		dao = new TestDAO(InterpreterTests.class);
		interpreter = new Interpreter(dao);
	}

	@AfterClass
	public static void close() {
		dao.close();
	}

	@Test
	public void interpret_from_given_coordinates() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(6666, 3333, Type.YKJ).setAccuracyInMeters(1000));

		interpreter.interpret(dwRoot);
		GatheringInterpretations interpretations = dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations();

		assertEquals(Type.YKJ, interpretations.getCoordinates().getType());
		assertEquals(1000, interpretations.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(6666, interpretations.getCoordinates().getLatMin(), 0);
		assertEquals(6667, interpretations.getCoordinates().getLatMax(), 0);
		assertEquals(3333, interpretations.getCoordinates().getLonMin(), 0);
		assertEquals(3334, interpretations.getCoordinates().getLonMax(), 0);
		assertEquals(GeoSource.REPORTED_VALUE, interpretations.getSourceOfCoordinates());
		assertNull(dwRoot.getPublicDocument().getGatherings().get(0).getGeo());

		assertEquals("ML.363", interpretations.getFinnishMunicipality().toString()); // Inkoo
		assertEquals("[ML.363]", interpretations.getFinnishMunicipalities().toString());
		assertEquals(GeoSource.COORDINATES, interpretations.getSourceOfFinnishMunicipality());
		assertEquals(Const.FINLAND, interpretations.getCountry());
		assertEquals(GeoSource.COORDINATES, interpretations.getSourceOfCountry());
		assertEquals("ML.253", interpretations.getBiogeographicalProvince().toString());
		assertEquals(GeoSource.COORDINATES, interpretations.getSourceOfBiogeographicalProvince());

		assertEquals("Suomi", interpretations.getCountryDisplayname());
		assertEquals("Inkoo", interpretations.getMunicipalityDisplayname());
		assertEquals("Uusimaa (U)", interpretations.getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void interpret_from_given_geo() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		Geo geo = new Geo(Type.YKJ);
		geo.addFeature(Polygon.from(new double[][] {{6664878,3332806}, {6677427,3347404}, {6664878,3341513}}));
		geo.addFeature(Point.from(6666666,3333333));
		geo.addFeature(Line.from(new double[][] {{6666666, 3333333}, {6666555,3335555}}));
		dwRoot.getPublicDocument().getGatherings().get(0).setGeo(geo);

		interpreter.interpret(dwRoot);
		Gathering g = dwRoot.getPublicDocument().getGatherings().get(0);
		GatheringInterpretations interpretations = g.getInterpretations();

		String message = "";
		if (g.getQuality() != null) {
			if (g.getQuality().getIssue() != null) message += g.getQuality().getIssue().getMessage();
			if (g.getQuality().getLocationIssue() != null) message += g.getQuality().getLocationIssue().getMessage();
			if (g.getQuality().getTimeIssue() != null) message += g.getQuality().getTimeIssue().getMessage();
		}
		assertEquals("", message);

		assertEquals(Type.YKJ, interpretations.getCoordinates().getType());
		assertEquals(25000, interpretations.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(6664878, interpretations.getCoordinates().getLatMin(), 0);
		assertEquals(6677427, interpretations.getCoordinates().getLatMax(), 0);
		assertEquals(3332806, interpretations.getCoordinates().getLonMin(), 0);
		assertEquals(3347404, interpretations.getCoordinates().getLonMax(), 0);
		assertEquals(GeoSource.REPORTED_VALUE, interpretations.getSourceOfCoordinates());

		assertEquals("ML.363", interpretations.getFinnishMunicipality().toString()); // Inkoo by center point
		assertEquals("[ML.363, ML.410]", interpretations.getFinnishMunicipalities().toString());
		assertEquals(GeoSource.COORDINATE_CENTERPOINT, interpretations.getSourceOfFinnishMunicipality());
		assertEquals(Const.FINLAND, interpretations.getCountry());
		assertEquals(GeoSource.COORDINATES, interpretations.getSourceOfCountry());
		assertEquals("ML.253", interpretations.getBiogeographicalProvince().toString());
		assertEquals("[ML.253]", interpretations.getBiogeographicalProvinces().toString());
		assertEquals(GeoSource.COORDINATES, interpretations.getSourceOfBiogeographicalProvince());

		assertEquals("Suomi", interpretations.getCountryDisplayname());
		assertEquals("Inkoo, Siuntio", interpretations.getMunicipalityDisplayname());
		assertEquals("Uusimaa (U)", interpretations.getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void interpret_ykj_accuracy() throws DataValidationException {
		assertEquals(1, new Coordinates(7654321, 3333333, Type.YKJ).calculateBoundingBoxAccuracy());
		assertEquals(10, new Coordinates(765432, 333333, Type.YKJ).calculateBoundingBoxAccuracy());
		assertEquals(100, new Coordinates(76543, 33333, Type.YKJ).calculateBoundingBoxAccuracy());
		assertEquals(1000, new Coordinates(7654, 3333, Type.YKJ).calculateBoundingBoxAccuracy());
		assertEquals(10000, new Coordinates(765, 333, Type.YKJ).calculateBoundingBoxAccuracy());
		assertEquals(100000, new Coordinates(76, 33, Type.YKJ).calculateBoundingBoxAccuracy());
	}

	@Test
	public void interpret_coordinate_accuracy_from_given_coordinates() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		dwRoot.createPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(0).setCoordinates(new Coordinates(6666, 3333, Type.YKJ).setAccuracyInMeters(1));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(1).setCoordinates(new Coordinates(6666, 3333, Type.YKJ));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(2).setCoordinates(new Coordinates(66.0, 33.0, Type.WGS84).setAccuracyInMeters(100));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(3).setCoordinates(new Coordinates(66.0, 33.0, Type.WGS84));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(4).setCoordinates(new Coordinates(66.123456, 33.123456, Type.WGS84));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(5).setCoordinates(new Coordinates(6664201, 332898, Type.EUREF).setAccuracyInMeters(5000));
		dwRoot.getPublicDocument().addGathering(new Gathering(new Qname("gid")));
		dwRoot.getPublicDocument().getGatherings().get(6).setCoordinates(new Coordinates(6664201, 332898, Type.EUREF));

		interpreter.interpret(dwRoot);
		GatheringInterpretations interpretations0 = dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations();
		GatheringInterpretations interpretations1 = dwRoot.getPublicDocument().getGatherings().get(1).getInterpretations();
		GatheringInterpretations interpretations2 = dwRoot.getPublicDocument().getGatherings().get(2).getInterpretations();
		GatheringInterpretations interpretations3 = dwRoot.getPublicDocument().getGatherings().get(3).getInterpretations();
		GatheringInterpretations interpretations4 = dwRoot.getPublicDocument().getGatherings().get(4).getInterpretations();
		GatheringInterpretations interpretations5 = dwRoot.getPublicDocument().getGatherings().get(5).getInterpretations();
		GatheringInterpretations interpretations6 = dwRoot.getPublicDocument().getGatherings().get(6).getInterpretations();

		assertEquals(1000, interpretations0.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(1000, interpretations1.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(100, interpretations2.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(1, interpretations3.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(1, interpretations4.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(5000, interpretations5.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(1, interpretations6.getCoordinates().getAccuracyInMeters().intValue());
	}

	@Test
	public void interpret_individual_count() throws Exception {
		DwRoot root = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		root.getPublicDocument().addGathering(g);

		Unit u1 = new Unit(new Qname("u1"));
		Unit u2 = new Unit(new Qname("u2"));
		Unit u3 = new Unit(new Qname("u3"));
		Unit u4 = new Unit(new Qname("u4"));
		g.addUnit(u1);
		g.addUnit(u2);
		g.addUnit(u3);
		g.addUnit(u4);
		u1.addFact("pairCount", "4");

		u1.setAbundanceString("2");
		u2.setAbundanceString("");
		u3.setAbundanceString("3m^2");
		u4.setIndividualCountFemale(1);
		u4.setIndividualCountMale(2);
		u4.addFact(new Qname("MY.larvaIndividualCount").toURI(), "1");

		Gathering g2 = new Gathering(new Qname("gid2"));
		root.getPublicDocument().addGathering(g2);
		Unit u5 = new Unit(new Qname("u5"));
		Unit u6 = new Unit(new Qname("u6"));
		Unit u7 = new Unit(new Qname("u7"));
		g2.addUnit(u5);
		g2.addUnit(u6);
		g2.addUnit(u7);
		g2.addFact("http://tun.fi/MY.acknowledgeNoUnitsInCensus", "true");
		u6.setAbundanceString("1");
		u7.setAbundanceString("0");

		u1.setTaxonVerbatim("musthavesomething");
		u2.setTaxonVerbatim("musthavesomething");
		u3.setTaxonVerbatim("musthavesomething");
		u4.setTaxonVerbatim("musthavesomething");
		u5.setTaxonVerbatim("musthavesomething");
		u6.setTaxonVerbatim("musthavesomething");
		u7.setTaxonVerbatim("musthavesomething");
		u1.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u2.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u3.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u4.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u5.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u6.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		u7.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);

		interpreter.interpret(root);

		assertEquals(2, u1.getInterpretations().getIndividualCount());
		assertEquals(1, u2.getInterpretations().getIndividualCount());
		assertEquals(1, u3.getInterpretations().getIndividualCount());
		assertEquals(4, u4.getInterpretations().getIndividualCount());

		assertEquals(4, u1.getInterpretations().getPairCount().intValue());
		assertEquals(null, u2.getInterpretations().getPairCount());

		assertEquals(0, u5.getInterpretations().getIndividualCount());
		assertEquals(1, u6.getInterpretations().getIndividualCount());
		assertEquals(0, u7.getInterpretations().getIndividualCount());
	}

	// TODO testaa gathering interpretations kombinaatiot:
	//											Single municip.
	//	Case	Coordinates	Coords in Finland	matches coords		Municipality resolves
	//	1		Yes			Yes					Yes					-						i.Country = fi 		i.finnishM = <resolved by coords>
	//	2		Yes			Yes					No					Yes						i.Country = fi 		i.finnishM = <resolved by name>
	//	3		Yes			Yes					No					No						i.Country = fi
	//	4		Yes			No					-					-						i.Country = <resolved by name or none>

	//	Case	Coordinates	Country resolves	=FI		Municipality resolves
	//	5		No			Yes					Yes		Yes							i.Country = fi 		i.finnishM = <resolved by name>
	//	6		No			Yes					Yes		No							i.Country = fi
	//	7		No			Yes					No		-							i.Country = <resolved by name>
	//	8		No			No (given)			-		-
	//	9		No			No (NOT given)		-		Yes							i.Country = fi 		i.finnishM = <resolved by name>
	//	10		No			No (NOT given)		-		No

	@Test
	public void bioprovince() throws Exception {
		String data = "{ \"processTime\": 1449568582, \"documentId\": \"http://tun.fi/GAC.16990\", \"publicDocument\": { \"public\": true, \"editors\": [ \"http://tun.fi/MA.88\" ], \"createdDate\": \"2015-12-01\", \"concealment\": \"PUBLIC\", \"documentId\": \"http://tun.fi/GAC.16990\", \"collectionId\": \"http://tun.fi/HR.1\", \"sourceId\": \"http://tun.fi/KE.4\", \"gatherings\": [ { \"country\": \"Finland\", \"biogeographicalProvince\": \"Tb\", \"coordinatesVerbatim\": \"69248:33862 YKJ (10000m)\", \"municipality\": \"Multia\", \"coordinates\": { \"type\": \"YKJ\", \"accuracyInMeters\": 10000, \"latMin\": 69248, \"lonMin\": 33862, \"latMax\": 69249, \"lonMax\": 33863 }, \"gatheringId\": \"http://tun.fi/MY.1688788\", \"mediaCount\": 0, \"agents\": [ \"Pohjola, Mauno\" ], \"units\": [ { \"author\": \"Gyllenhal, 1810\", \"abundanceString\": \"1\", \"mediaCount\": 0, \"typeSpecimen\": false, \"breedingSite\": false, \"unitId\": \"http://tun.fi/MY.1688789\", \"lifeStage\": \"ADULT\", \"recordBasis\": \"PRESERVED_SPECIMEN\", \"taxonVerbatim\": \"Pteroloma forsstromii\", \"facts\": [ { \"value\": \"http://tun.fi/MY.recordBasisPreservedSpecimen\", \"fact\": \"http://tun.fi/MY.recordBasis\" }, { \"value\": \"http://tun.fi/MY.lifeStageAdult\", \"fact\": \"http://tun.fi/MY.lifeStage\" }, { \"value\": \"1\", \"fact\": \"http://tun.fi/MY.count\", \"integerValue\": 1, \"decimalValue\": 1 }, { \"value\": \"Pteroloma forsstromii\", \"fact\": \"http://tun.fi/MY.taxon\" }, { \"value\": \"yes\", \"fact\": \"http://tun.fi/MY.preferredIdentification\" }, { \"value\": \"Gyllenhal, 1810\", \"fact\": \"http://tun.fi/MY.author\" } ] } ], \"facts\": [ { \"value\": \"Multia\", \"fact\": \"http://tun.fi/MY.municipality\" }, { \"value\": \"Pohjola, Mauno\", \"fact\": \"http://tun.fi/MY.leg\" }, { \"value\": \"http://tun.fi/MY.coordinateSystemYkj\", \"fact\": \"http://tun.fi/MY.coordinateSystem\" }, { \"value\": \"Finland\", \"fact\": \"http://tun.fi/MY.country\" }, { \"value\": \"Tb\", \"fact\": \"http://tun.fi/MY.biologicalProvince\" }, { \"value\": \"24.795794\", \"fact\": \"http://tun.fi/MY.wgs84Longitude\", \"integerValue\": 24, \"decimalValue\": 24.795794 }, { \"value\": \"62.411342\", \"fact\": \"http://tun.fi/MY.wgs84Latitude\", \"integerValue\": 62, \"decimalValue\": 62.411342 }, { \"value\": \"Pohjola, M.\", \"fact\": \"http://tun.fi/MY.legVerbatim\" }, { \"value\": \"10000\", \"fact\": \"http://tun.fi/MY.coordinateRadius\", \"integerValue\": 10000, \"decimalValue\": 10000 }, { \"value\": \"69248\", \"fact\": \"http://tun.fi/MY.latitude\", \"integerValue\": 69248, \"decimalValue\": 69248 }, { \"value\": \"33862\", \"fact\": \"http://tun.fi/MY.longitude\", \"integerValue\": 33862, \"decimalValue\": 33862 } ] } ], \"secureLevel\": \"NONE\", \"notes\": \"[Data interpreted]\", \"facts\": [ { \"value\": \"false\", \"fact\": \"http://tun.fi/MZ.scheduledForDeletion\" }, { \"value\": \"2015-12-01T16:43:43+0200\", \"fact\": \"http://tun.fi/MZ.dateEdited\" }, { \"value\": \"zoospecimen\", \"fact\": \"http://tun.fi/MY.datatype\" }, { \"value\": \"http://tun.fi/MA.88\", \"fact\": \"http://tun.fi/MZ.editor\" }, { \"value\": \"http://tun.fi/MY.preservationGlued\", \"fact\": \"http://tun.fi/MY.preservation\" }, { \"value\": \"http://tun.fi/HR.1\", \"fact\": \"http://tun.fi/MY.collectionID\" }, { \"value\": \"http://tun.fi/MOS.1007\", \"fact\": \"http://tun.fi/MZ.owner\" }, { \"value\": \"2015-12-01T16:43:43+0200\", \"fact\": \"http://tun.fi/MZ.dateCreated\" }, { \"value\": \"Mattia, Jaakko\", \"fact\": \"http://tun.fi/MY.editor\" }, { \"value\": \"true\", \"fact\": \"http://tun.fi/MY.inMustikka\" }, { \"value\": \"1.12.2015\", \"fact\": \"http://tun.fi/MY.entered\" }, { \"value\": \"[Data interpreted]\", \"fact\": \"http://tun.fi/MY.notes\" }, { \"value\": \"http://tun.fi/MA.88\", \"fact\": \"http://tun.fi/MZ.creator\" } ] }, \"processSuccess\": true, \"collectionId\": \"http://tun.fi/HR.1\", \"sourceId\": \"http://tun.fi/KE.4\" }";
		DwRoot root = JsonToModel.rootFromJson(new JSONObject(data));
		interpreter.interpret(root);
		GatheringInterpretations interpretations = root.getPublicDocument().getGatherings().get(0).getInterpretations();
		assertEquals("ML.260", interpretations.getBiogeographicalProvince().toString());
		assertEquals("[ML.260]", interpretations.getBiogeographicalProvinces().toString());
	}

	@Test
	public void individualCount() {
		int max = IndividualCountInterpreter.MAX_VALID_COUNT;
		assertEquals(0, IndividualCountInterpreter.interpret("absent"));
		assertEquals(1, IndividualCountInterpreter.interpret(null));
		assertEquals(1, IndividualCountInterpreter.interpret(""));
		assertEquals(1, IndividualCountInterpreter.interpret(" "));
		assertEquals(1, IndividualCountInterpreter.interpret("a"));
		assertEquals(1, IndividualCountInterpreter.interpret("a1"));
		assertEquals(1, IndividualCountInterpreter.interpret("10m^2"));  // numbers before "m^2" are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("10m2"));  // numbers before "m2" are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("10dm^2"));  // numbers before "dm^2" are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("10dm2"));  // numbers before "dm2" are ignored
		assertEquals(3, IndividualCountInterpreter.interpret("1m2f")); // here m2 is ... what
		assertEquals(3, IndividualCountInterpreter.interpret("1m 2f")); // here m2 is ...what
		assertEquals(3, IndividualCountInterpreter.interpret("1f 2m"));
		assertEquals(1, IndividualCountInterpreter.interpret("-1"));
		assertEquals(1, IndividualCountInterpreter.interpret("1"));
		assertEquals(0, IndividualCountInterpreter.interpret("0"));
		assertEquals(3, IndividualCountInterpreter.interpret("1+2"));
		assertEquals(6, IndividualCountInterpreter.interpret("1/2/3"));
		assertEquals(4, IndividualCountInterpreter.interpret("1/0/3"));
		assertEquals(0, IndividualCountInterpreter.interpret("0/0/0"));
		assertEquals(7, IndividualCountInterpreter.interpret("7-10"));
		assertEquals(5, IndividualCountInterpreter.interpret("n. 5"));
		assertEquals(1, IndividualCountInterpreter.interpret("1 Ä"));
		assertEquals(1, IndividualCountInterpreter.interpret("/1"));
		assertEquals(2, IndividualCountInterpreter.interpret("2/"));
		assertEquals(8, IndividualCountInterpreter.interpret("n8 itiöemää"));
		assertEquals(100, IndividualCountInterpreter.interpret("100m"));
		assertEquals(131, IndividualCountInterpreter.interpret("3f128j"));
		assertEquals(12, IndividualCountInterpreter.interpret("n. 12-15"));
		assertEquals(1, IndividualCountInterpreter.interpret("useita isohkoja laikkuja"));
		assertEquals(1500, IndividualCountInterpreter.interpret("1500 kukkaa noin"));
		assertEquals(2, IndividualCountInterpreter.interpret("1/1p (Ä)"));
		assertEquals(60, IndividualCountInterpreter.interpret("60+ kukintoa"));
		assertEquals(400, IndividualCountInterpreter.interpret("n. 400p"));
		assertEquals(115, IndividualCountInterpreter.interpret("n. 80 + 35"));
		assertEquals(110, IndividualCountInterpreter.interpret(">110"));
		assertEquals(1, IndividualCountInterpreter.interpret("Kupruntie 12 B 2, 00110 Helsinki")); // over 30 chars
		assertEquals(1, IndividualCountInterpreter.interpret("Kupruntie 12 B 2")); // contains "tie"
		assertEquals(1, IndividualCountInterpreter.interpret("Kuprukatu 12 B 2")); // contains "katu"
		assertEquals(1, IndividualCountInterpreter.interpret("Kupruraitti 12 B 2")); // contains "raitti"
		assertEquals(1, IndividualCountInterpreter.interpret("Kuulinja 12 B 2")); // contains "linja"
		assertEquals(1, IndividualCountInterpreter.interpret("KUUPOLKU 12 B 2")); // contains "polku"
		assertEquals(1, IndividualCountInterpreter.interpret("00110 Helsinki")); // numbers starting with 0 but not equal to zero are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("00110")); // numbers starting with 0 but not equal to zero are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("97451 Lapjärvi")); // number > 10000 and contains text (not only numbers)
		assertEquals(0, IndividualCountInterpreter.interpret("0"));
		assertEquals(1, IndividualCountInterpreter.interpret("0/1"));
		assertEquals(1, IndividualCountInterpreter.interpret("Kohtalaisesti, n. 10 x 5 m alalla, 2 m2:n peittävyyden verran, arviolta 40 yksilöä")); // over 30 chars
		assertEquals(1, IndividualCountInterpreter.interpret("12345678901234567890")); // > MAX_VALID_COUNT
		assertEquals(1, IndividualCountInterpreter.interpret(""+(max+1))); // > > MAX_VALID_COUNT
		assertEquals(max, IndividualCountInterpreter.interpret(""+max)); // >= > MAX_VALID_COUNT
		assertEquals(max-1, IndividualCountInterpreter.interpret(""+(max-1))); // > > MAX_VALID_COUNT
		assertEquals(1, IndividualCountInterpreter.interpret("lis 440606")); // > > MAX_VALID_COUNT
		assertEquals(1, IndividualCountInterpreter.interpret(" 240606 2131422")); // > > MAX_VALID_COUNT
		assertEquals(1, IndividualCountInterpreter.interpret("lis 240606 3"));
		assertEquals(1, IndividualCountInterpreter.interpret("lis 440606 3"));
		assertEquals(40, IndividualCountInterpreter.interpret("10 x 5 m, 7 m2:n, 40 yks.")); // numbers around "x" ignored, numbers before "m2" ignored -> 40
		assertEquals(1, IndividualCountInterpreter.interpret("2 m2"));
		assertEquals(1, IndividualCountInterpreter.interpret("10 x 5 m alalla"));
		assertEquals(1, IndividualCountInterpreter.interpret("30x30 cm"));
		assertEquals(1, IndividualCountInterpreter.interpret("n. 30x30 cm"));
		assertEquals(1, IndividualCountInterpreter.interpret("n.30x30 cm"));
		assertEquals(1, IndividualCountInterpreter.interpret("runko 2.2. painunut luokalle"));
		assertEquals(34, IndividualCountInterpreter.interpret("32-37/2"));
		assertEquals(9, IndividualCountInterpreter.interpret("5/+4-5"));
		assertEquals(5, IndividualCountInterpreter.interpret("5m N +-"));
		assertEquals(2, IndividualCountInterpreter.interpret("1 emi- ja 1 hedepensas"));
		assertEquals(10, IndividualCountInterpreter.interpret("10 havaintoa (23:30 - 24:00)")); // numbers around ":" ignored
		assertEquals(10, IndividualCountInterpreter.interpret("10 havaintoa (23.30 - 24.00)"));
		assertEquals(1, IndividualCountInterpreter.interpret("-10")); // 1-10
		assertEquals(20, IndividualCountInterpreter.interpret("parvi (20)")); // numbers inside brackets are included if no other numbers
		assertEquals(1, IndividualCountInterpreter.interpret("1 (3)")); // numbers inside brackets are ignored if there are other numbers
		assertEquals(3, IndividualCountInterpreter.interpret("3 (2 koirasta 1 naaras)")); // numbers inside brackets are ignored if there are other numbers
		assertEquals(1, IndividualCountInterpreter.interpret("parvi (-20)"));
		assertEquals(1, IndividualCountInterpreter.interpret("0/1"));
		assertEquals(0, IndividualCountInterpreter.interpret("0?"));
		assertEquals(1, IndividualCountInterpreter.interpret("0,5 dm2"));
		assertEquals(1, IndividualCountInterpreter.interpret("0,5 m2"));
		assertEquals(1, IndividualCountInterpreter.interpret("0.5 m2"));
		assertEquals(1, IndividualCountInterpreter.interpret("0.5 M2"));
		assertEquals(1, IndividualCountInterpreter.interpret("m^2"));
		assertEquals(1, IndividualCountInterpreter.interpret("m2"));
		assertEquals(1, IndividualCountInterpreter.interpret("0.5"));
		assertEquals(1, IndividualCountInterpreter.interpret("0,5"));
		assertEquals(2, IndividualCountInterpreter.interpret("0/2 pho"));
		assertEquals(1, IndividualCountInterpreter.interpret("0 aarin kasvusto")); // numbers before "aari" are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("5 M2")); // numbers before "m2" are ignored
		assertEquals(1, IndividualCountInterpreter.interpret("5 M^2")); // numbers before "m^2" are ignored
		assertEquals(8, IndividualCountInterpreter.interpret("F3 M2 J3")); // numbers before "m^2" are ignored
		assertEquals(2, IndividualCountInterpreter.interpret("0 M 2 F 0 juv. "));
		assertEquals(2, IndividualCountInterpreter.interpret("0 M, 2 F, 0 juv. "));
		assertEquals(1, IndividualCountInterpreter.interpret("4 neliötä"));
		assertEquals(1, IndividualCountInterpreter.interpret("4 neliömetriä"));
		assertEquals(1, IndividualCountInterpreter.interpret("ATL:7"));
		assertEquals(1, IndividualCountInterpreter.interpret("atl:7"));
		assertEquals(3, IndividualCountInterpreter.interpret("ATL:7 3"));
		assertEquals(1, IndividualCountInterpreter.interpret("atl:7"));
		assertEquals(1, IndividualCountInterpreter.interpret("[1] XXX:11"));
		assertEquals(2, IndividualCountInterpreter.interpret("2 XXX:1"));
		assertEquals(1, IndividualCountInterpreter.interpret("1kv"));
		assertEquals(1, IndividualCountInterpreter.interpret("2kv"));
		assertEquals(1, IndividualCountInterpreter.interpret("2-kv"));
		assertEquals(1, IndividualCountInterpreter.interpret("3kv"));
		assertEquals(1, IndividualCountInterpreter.interpret("1 3kv"));
		assertEquals(4, IndividualCountInterpreter.interpret("4 3kv"));
		assertEquals(11, IndividualCountInterpreter.interpret("7 1kv 4 2kv"));
		assertEquals(8, IndividualCountInterpreter.interpret("yli 5ad+3 1kv"));
		assertEquals(152, IndividualCountInterpreter.interpret("4a152"));
		assertEquals(152, IndividualCountInterpreter.interpret("4a 152"));
		assertEquals(1, IndividualCountInterpreter.interpret("1 &#9794;"));
		assertEquals(1, IndividualCountInterpreter.interpret("40 cm:n matkalla lahopuurungolla"));
		assertEquals(10000, IndividualCountInterpreter.interpret(">10000"));
		assertEquals(5000, IndividualCountInterpreter.interpret("runsas, >5000"));
		assertEquals(10000, IndividualCountInterpreter.interpret("runsas, >10000"));
		assertEquals(1, IndividualCountInterpreter.interpret("runsas, >20000")); // See case "97451 Lapjärvi"
		assertEquals(1, IndividualCountInterpreter.interpret("x"));
	}

	@Test
	public void toAbudanceStringParts() {
		List<AbundanceStringPart> parts = IndividualCountInterpreter.toParts(null);
		assertEquals(0, parts.size());

		parts = IndividualCountInterpreter.toParts("");
		assertEquals(0, parts.size());

		parts = IndividualCountInterpreter.toParts("a");
		assertEquals(0, parts.size());

		parts = IndividualCountInterpreter.toParts("1");
		assertEquals(1, parts.size());
		assertEquals("", parts.get(0).prefix);
		assertEquals("1", parts.get(0).number);
		assertEquals("", parts.get(0).postfix);

		parts = IndividualCountInterpreter.toParts("a1");
		assertEquals(1, parts.size());
		assertEquals("a", parts.get(0).prefix);
		assertEquals("1", parts.get(0).number);
		assertEquals("", parts.get(0).postfix);

		parts = IndividualCountInterpreter.toParts("a1 b");
		assertEquals(1, parts.size());
		assertEquals("a", parts.get(0).prefix);
		assertEquals("1", parts.get(0).number);
		assertEquals("b", parts.get(0).postfix);

		parts = IndividualCountInterpreter.toParts("a11b");
		assertEquals(1, parts.size());
		assertEquals("a", parts.get(0).prefix);
		assertEquals("11", parts.get(0).number);
		assertEquals("b", parts.get(0).postfix);

		parts = IndividualCountInterpreter.toParts("a1 1b");
		assertEquals("[[prefix=a number=1 postfix=], [prefix= number=1 postfix=b]]", parts.toString());

		parts = IndividualCountInterpreter.toParts("1a2b");
		assertEquals("[[prefix= number=1 postfix=a], [prefix=a number=2 postfix=b]]", parts.toString());

		parts = IndividualCountInterpreter.toParts("10m^2");
		assertEquals("[[prefix= number=10 postfix=m^], [prefix=m^ number=2 postfix=]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("10m2");
		parts = IndividualCountInterpreter.toParts("1+2");
		assertEquals("[[prefix= number=1 postfix=+], [prefix=+ number=2 postfix=]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("n. 5");
		assertEquals("[[prefix=n. number=5 postfix=]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("Kiiperintie 12 B 2, 00110 Helsinki");
		assertEquals("[[prefix=Kiiperintie number=12 postfix=B], [prefix=B number=2 postfix=,], [prefix=, number=00110 postfix=Helsinki]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("10 x 5 m, 2 m2:n, 40 yks.");
		assertEquals("[[prefix= number=10 postfix=x], [prefix=x number=5 postfix=m,], [prefix=m, number=2 postfix=m], [prefix=m number=2 postfix=:n,], [prefix=:n, number=40 postfix=yks.]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("runko 2.2. painunut luokalle");
		assertEquals("[[prefix=runko number=2 postfix=.], [prefix=. number=2 postfix=. painunut luokalle]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("10 havaintoa (23:30 - 24:00)");
		assertEquals("[[prefix= number=10 postfix=havaintoa (], [prefix=havaintoa ( number=23 postfix=:], [prefix=: number=30 postfix=-], [prefix=- number=24 postfix=:], [prefix=: number=00 postfix=)]]", parts.toString());
		parts = IndividualCountInterpreter.toParts("parvi (-20)");
		assertEquals("[[prefix=parvi (- number=20 postfix=)]]", parts.toString());
	}

	@Test
	public void insideGeneralFinlandArea() throws DataValidationException {
		assertTrue(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(60, 60, 30, 30, Type.WGS84)));
		assertFalse(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(60, 60, -30, -30, Type.WGS84)));
		assertTrue(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(60.684034, 28.840381, Type.WGS84)));
		assertFalse(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(59.230210, 24.710104, Type.WGS84)));
		assertTrue(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(64.699649, 66.712736, 21.918022, 23.608320, Type.WGS84)));
		assertFalse(FinlandAreaUtil.isInsideGeneralFinlandArea(new Coordinates(-90, 90, -90, 90, Type.WGS84))); // This huge area contains Finland but our imple only checks corner points. This if fine by me!
	}

	@Test
	public void interpretingGeography() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);
		g.setGeo(new Geo(Type.YKJ)
				.addFeature(Point.from(6666.0, 3333.0))
				.addFeature(Line.from(new double[][] {{7000.0, 3870.0}, {7005.0, 3500.0}})));

		interpreter.interpret(root);

		Coordinates coordinates = g.getInterpretations().getCoordinates();
		assertEquals("6666.0 : 7005.0 : 3333.0 : 3870.0 : YKJ : 100000", coordinates.toString());
	}

	@Test
	public void interpretingGeography_2() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);
		g.setGeo(new Geo(Type.YKJ)
				.addFeature(Point.from(666601.0, 333011.0))
				.addFeature(Line.from(new double[][] {{666501.0, 337001.0}, {666501.0, 330001.0}})));

		interpreter.interpret(root);

		Coordinates coordinates = g.getInterpretations().getCoordinates();
		assertEquals("666501.0 : 666601.0 : 330001.0 : 337001.0 : YKJ : 100000", coordinates.toString());
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void interpretingGeography_2_5() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);
		g.setGeo(new Geo(Type.YKJ)
				.addFeature(Point.from(6666601.0, 3333011.0))
				.addFeature(Line.from(new double[][] {{6666501.0, 3337001.0}, {6666501.0, 3330001.0}})));

		interpreter.interpret(root);

		Coordinates coordinates = g.getInterpretations().getCoordinates();
		assertEquals(Type.YKJ, coordinates.getType());
		assertEquals(6666501.0, coordinates.getLatMin(), 0);
		assertEquals(6666601.0, coordinates.getLatMax(), 0);
		assertEquals(3330001.0, coordinates.getLonMin(), 0);
		assertEquals(3337001.0, coordinates.getLonMax(), 0);
		assertEquals(5000, coordinates.getAccuracyInMeters().intValue());
	}

	@Test
	public void interpretingGeography_3() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);
		g.setGeo(new Geo(Type.YKJ)
				.addFeature(Point.from(6666.0, 3333.0))
				.addFeature(Line.from(new double[][] {{7000123.0, 3870321.0}, {7005123.0, 3500321.0}})));

		interpreter.interpret(root);

		assertEquals(null, g.getInterpretations().getCoordinates()); // Invalid geometry -> will be handled in converter
	}

	@Test
	public void teamAgentNamesFromUserId() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);
		g.addTeamMember("Esko Piirainen");
		g.addTeamMember("MA.5");
		g.addTeamMember(new Qname("MA.1").toURI());
		g.addTeamMember("Huuppola, Hippu");
		g.addTeamMember("MA.FOOBAR");
		interpreter.interpret(root);

		assertEquals("Esko Piirainen", g.getTeam().get(0));
		assertEquals("Dare Talvitie", g.getTeam().get(1));
		assertEquals("Huuppola, Hippu", g.getTeam().get(2));
		assertEquals("Unknown person (MA.FOOBAR)", g.getTeam().get(3));
		assertEquals(4, g.getTeam().size());
	}

	@Test
	public void teamTextCleaning() throws Exception {
		DwRoot root = new DwRoot(new Qname("SX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Document doc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("G.1"));
		doc.addGathering(g);

		g.addTeamMember("\nEsko Piirainen\r\nSeppänen V.\n");
		g.addTeamMember("Kiiski P.J.;M.Repola; J-P. Vipula");
		g.addTeamMember("Kiiski P. J.");

		interpreter.interpret(root);

		assertEquals("Esko Piirainen", g.getTeam().get(0));
		assertEquals("Seppänen V.", g.getTeam().get(1));
		assertEquals("Kiiski P. J.", g.getTeam().get(2));
		assertEquals("M. Repola", g.getTeam().get(3));
		assertEquals("J-P. Vipula", g.getTeam().get(4));
		assertEquals(5, g.getTeam().size());
	}

	@Test
	public void insideMunicipality_1() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(6666, 3333, Type.YKJ));
		g.setMunicipality("ingå");
		interpreter.interpret(dwRoot);

		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Inkoo", g.getInterpretations().getMunicipalityDisplayname());
	}

	@Test
	public void outisdeMunicipality() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(6666, 3333, Type.YKJ));
		g.setMunicipality("kouvola");

		interpreter.interpret(dwRoot);

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g.getQuality().getLocationIssue().getSource());
		assertEquals(
				"Coordinates are not inside municipality Kouvola they are inside [Inkoo]: 6666:3333 YKJ",
				g.getQuality().getLocationIssue().getMessage());
	}

	@Test
	public void outsideMunicipality_butVeryClose() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(60.2777547, 25.124057, Type.WGS84)); // In Vantaa
		g.setMunicipality("Helsinki");

		interpreter.interpret(dwRoot);

		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("[ML.648]", g.getInterpretations().getFinnishMunicipalities().toString()); // Vantaa
		assertEquals("ML.648", g.getInterpretations().getFinnishMunicipality().toString());
		assertEquals("Vantaa", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfFinnishMunicipality());
	}

	@Test
	public void outsideMunicipality_a_bit_too_far_YKJ_SQUARE() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(6715, 3401, Type.YKJ)); // In Mäntsälä
		g.setMunicipality("Helsinki");

		interpreter.interpret(dwRoot);

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g.getQuality().getLocationIssue().getSource());
		assertEquals(
				"Coordinates are not inside municipality Helsinki they are inside [Mäntsälä]: 6715:3401 YKJ",
				g.getQuality().getLocationIssue().getMessage());
		assertEquals("Helsinki", g.getInterpretations().getMunicipalityDisplayname()); // has issue -> contains verbatim
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void outsideMunicipality_allowed_for_WGS84_point() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(60.64, 25.26, Type.WGS84)); // In Mäntsälä
		g.setMunicipality("Helsinki");

		interpreter.interpret(dwRoot);

		assertEquals(null, g.getQuality());
	}

	@Test
	public void outsideMunicipality_but_close_enough() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(61.335878, 24.490875, Type.WGS84)); // In Pälkäne but close to Tampere
		g.setMunicipality("Tampere");

		interpreter.interpret(dwRoot);

		assertEquals(null,g.getQuality());
	}

	@Test
	public void insideMunicipality__close_ykj() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(6693, 3347, Type.YKJ)); // In Vihti
		g.setMunicipality("Lohja");

		interpreter.interpret(dwRoot);

		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("6693.0 : 6694.0 : 3347.0 : 3348.0 : YKJ : 1000", g.getInterpretations().getCoordinates().toString());
	}

	@Test
	public void insideMunicipality__too_far_ykj() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(6693, 3347, Type.YKJ)); // In Vihti
		g.setMunicipality("Helsinki");

		interpreter.interpret(dwRoot);

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g.getQuality().getLocationIssue().getSource());
		assertEquals(
				"Coordinates are not inside municipality Helsinki they are inside [Vihti]: 6693:3347 YKJ",
				g.getQuality().getLocationIssue().getMessage());
	}

	@Test
	public void noRecordBasis() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		Unit u = new Unit(new Qname("uid"));
		g.addUnit(u);
		u.setTaxonVerbatim("susi");

		interpreter.interpret(dwRoot);

		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
		assertEquals(null, u.getQuality());
	}

	@Test
	public void bugtesting() throws Exception {
		String xml = "<SimpleDarwinRecordSet><SimpleDarwinRecord><dwc:collectionID>http://tun.fi/HR.1</dwc:collectionID><dwc:occurrenceID>7914-3937-7639-1766</dwc:occurrenceID><dc:created>2017-01-31 00:00:00</dc:created><dc:modified>2017-01-31 00:00:00</dc:modified><dwc:recordedBy>Janne Sinkkonen</dwc:recordedBy><dwc:basisOfRecord>HumanObservation</dwc:basisOfRecord><dwc:scientificName>Conistra vaccinii</dwc:scientificName><dwc:individualCount>3</dwc:individualCount><dwc:lifeStage>aikuinen</dwc:lifeStage><dwc:samplingProtocol>otsalamppu</dwc:samplingProtocol><dwc:verbatimEventDate>2017-01-30</dwc:verbatimEventDate><dwc:country>Finland</dwc:country><dwc:municipality>Espoo</dwc:municipality><dwc:verbatimCoordinates>668291:337439</dwc:verbatimCoordinates><dwc:geodeticDatum>FI KKJ27</dwc:geodeticDatum><dwc:dynamicProperties>{&quot;biogeographicalProvince&quot;:&quot;N&quot;,&quot;owner&quot;:&quot;virtala:34&quot;}</dwc:dynamicProperties></SimpleDarwinRecord></SimpleDarwinRecordSet>";
		List<DwRoot> dwRoots = new SimpleDarwinRecordSetHarmonizer().harmonize(new XMLReader().parse(xml), new Qname("KE.123"));
		DwRoot root = dwRoots.get(0);

		interpreter.interpret(root);

		Document d = root.getPublicDocument();
		assertEquals(false, d.createQuality().hasIssues());
		Gathering g = d.getGatherings().get(0);
		assertEquals(false, g.createQuality().hasIssues());
		Unit u = g.getUnits().get(0);
		assertEquals(false, u.createQuality().hasIssues());

		assertEquals("Espoo", g.getMunicipality());

		assertEquals("668291.0 : 668292.0 : 337439.0 : 337440.0 : YKJ : null", g.getCoordinates().toString());
		assertEquals("668291:337439 FI KKJ27", g.getCoordinatesVerbatim());
		assertEquals("668291.0 : 668292.0 : 337439.0 : 337440.0 : YKJ : 10", g.getInterpretations().getCoordinates().toString());
		assertEquals(10, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());

		assertEquals(ESPOO_ID, g.getInterpretations().getFinnishMunicipality().toString());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("Espoo", g.getInterpretations().getMunicipalityDisplayname());
	}

	@Test
	public void municipality_and_bioprovince_using_centerpoint() throws CriticalParseFailure, DataValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(66, 33, Type.YKJ));

		interpreter.interpret(dwRoot);

		assertEquals("Kirkkonummi", g.getInterpretations().getMunicipalityDisplayname()); // Over 10 municipalities, centerpoint name
		assertEquals("[ML.363, ML.364, ML.365, ML.366, ML.382, ML.383, ML.389, ML.390, ML.397, ML.410, ML.469, ML.643, ML.648, ML.660, ML.667]", g.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals("ML.364", g.getInterpretations().getFinnishMunicipality().toString());
		assertEquals(GeoSource.COORDINATE_CENTERPOINT, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("Uusimaa (U), Varsinais-Suomi (V)", g.getInterpretations().getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void municipality_and_bioprovince_using_centerpoint_2() throws CriticalParseFailure, DataValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(66, 33, Type.YKJ));
		g.setMunicipality("Inkooko?");

		interpreter.interpret(dwRoot);

		assertEquals("Kirkkonummi", g.getInterpretations().getMunicipalityDisplayname()); // Over 10 municipalities, picks centerpoint municipality
		assertEquals("Uusimaa (U), Varsinais-Suomi (V)", g.getInterpretations().getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void municipality_and_bioprovince_using_centerpoint_3() throws CriticalParseFailure, DataValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(666, 333, Type.YKJ));

		interpreter.interpret(dwRoot);

		assertEquals("Inkoo", g.getInterpretations().getMunicipalityDisplayname()); // Over 10 municipalities
		assertEquals("Uusimaa (U)", g.getInterpretations().getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void municipality_and_bioprovince_using_municipality_centerpoint() throws CriticalParseFailure {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setMunicipality("joensuu");

		interpreter.interpret(dwRoot);

		assertEquals("[ML.637]", g.getInterpretations().getFinnishMunicipalities().toString()); // Joensuu
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("Joensuu", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.262]", g.getInterpretations().getBiogeographicalProvinces().toString()); // Pohjois-Karjala
		assertEquals(GeoSource.COORDINATE_CENTERPOINT, g.getInterpretations().getSourceOfBiogeographicalProvince());
		assertEquals("Pohjois-Karjala (PK)", g.getInterpretations().getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void municipality_and_bioprovince_using_municipality_centerpoint_enontekiö() throws CriticalParseFailure {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("gid"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setMunicipality("enontekiö");

		interpreter.interpret(dwRoot);

		assertEquals("[ML.634]", g.getInterpretations().getFinnishMunicipalities().toString()); // Enontekiö
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("Enontekiö", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.270]", g.getInterpretations().getBiogeographicalProvinces().toString()); // Enontekiön Lappi
		assertEquals(GeoSource.COORDINATE_CENTERPOINT, g.getInterpretations().getSourceOfBiogeographicalProvince());
		assertEquals("Enontekiön Lappi (EnL)", g.getInterpretations().getBiogeographicalProvinceDisplayname());
	}

	@Test
	public void eventDateTimeValidation() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);

		Gathering g1 = new Gathering(new Qname("g1"));
		g1.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));

		Gathering g2 = new Gathering(new Qname("g2"));
		g2.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		g2.setHourBegin(11);
		g2.setHourEnd(10);

		Gathering g3 = new Gathering(new Qname("g3"));
		g3.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), DateUtils.convertToDate("2.1.2000")));
		g3.setHourBegin(11);
		g3.setHourEnd(10);

		Gathering g4 = new Gathering(new Qname("g4"));
		g4.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), DateUtils.convertToDate("2.1.2000")));
		g4.setHourBegin(11);
		g4.setMinutesEnd(50);

		dwRoot.createPublicDocument().addGathering(g1).addGathering(g2).addGathering(g3).addGathering(g4);

		interpreter.interpret(dwRoot);

		assertEquals(null, g1.getQuality());
		//		assertEquals(Quality.Issue.INVALID_HOUR, g2.getQuality().getTimeIssue().getIssue());
		//		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g2.getQuality().getTimeIssue().getSource());
		//		assertEquals("End hour is before start hour", g2.getQuality().getTimeIssue().getMessage());
		//		assertEquals(null, g3.getQuality());
		assertEquals(null, g3.getQuality());

		assertEquals(Quality.Issue.INVALID_MINUTE, g4.getQuality().getTimeIssue().getIssue());
		assertEquals("Minutes given without hour", g4.getQuality().getTimeIssue().getMessage());
	}

	@Test
	public void eventDateTimeValidation_1() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.getQuality());
	}

	@Test
	public void eventDateTimeValidation_2() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		g.setHourBegin(11);
		g.setHourEnd(10);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		//		assertEquals(Quality.Issue.INVALID_HOUR, g.getQuality().getTimeIssue().getIssue());
		//		assertEquals(Quality.Source.AUTOMATED_FINBIF_VALIDATION, g.getQuality().getTimeIssue().getSource());
		//		assertEquals("End hour is before start hour", g.getQuality().getTimeIssue().getMessage());
		assertEquals(null, g.getQuality());
	}

	@Test
	public void eventDateTimeValidation_3() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), DateUtils.convertToDate("2.1.2000")));
		g.setHourBegin(11);
		g.setHourEnd(10);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.getQuality());
	}

	@Test
	public void eventDateTimeValidation_4() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		g.setHourBegin(11);
		g.setMinutesEnd(50);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.INVALID_MINUTE, g.getQuality().getTimeIssue().getIssue());
		assertEquals("Minutes given without hour", g.getQuality().getTimeIssue().getMessage());
	}

	@Test
	public void eventDateTimeValidation_5() throws CriticalParseFailure, DateValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(null);
		g.setHourBegin(11);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.INVALID_DATE, g.getQuality().getTimeIssue().getIssue());
		assertEquals("Hours/minutes given without a date", g.getQuality().getTimeIssue().getMessage());
	}

	// Lets not be this strict
	//	@Test
	//	public void eventDateTimeValidation_6() throws CriticalParseFailure, DateValidationException, ParseException {
	//		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
	//		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
	//		Gathering g = new Gathering(new Qname("g"));
	//		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
	//		g.setHourEnd(11);
	//		dwRoot.createPublicDocument().addGathering(g);
	//
	//		interpreter.interpret(dwRoot);
	//		assertEquals(Quality.Issue.INVALID_HOUR, g.getQuality().getTimeIssue().getIssue());
	//		assertEquals("End hour given without start hour", g.getQuality().getTimeIssue().getMessage());
	//	}

	@Test
	public void eventDateTimeValidation_7() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		g.setMinutesBegin(40);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.INVALID_MINUTE, g.getQuality().getTimeIssue().getIssue());
		assertEquals("Minutes given without hour", g.getQuality().getTimeIssue().getMessage());
	}

	@Test
	public void eventDateTimeValidation_8() throws CriticalParseFailure, DateValidationException, ParseException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000")));
		g.setHourBegin(6);
		g.setMinutesBegin(40);
		g.setHourEnd(6);
		g.setMinutesEnd(35);
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		//		assertEquals(Quality.Issue.INVALID_MINUTE, g.getQuality().getTimeIssue().getIssue());
		//		assertEquals("Begin and end are on same day and same hour but end minutes are before begin minutes", g.getQuality().getTimeIssue().getMessage());
		assertEquals(null, g.getQuality());
	}

	@Test
	public void oldFinnishMunicipality_joinedToFinnishMunicipality() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setMunicipality("Punkaharju");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("Punkaharju", g.getMunicipality());
		assertEquals("Savonlinna", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.446]", g.getInterpretations().getFinnishMunicipalities().toString()); // Savonlinna
		assertEquals(GeoSource.OLD_FINNISH_MUNICIPALITY, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("61.533696 : 62.417751 : 28.370206 : 29.696811 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString()); // Savonlinna bounding box
		assertEquals(GeoSource.FINNISH_MUNICIPALITY, g.getInterpretations().getSourceOfCoordinates());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
	}

	@Test
	public void oldFinnishMunicipality_joinedToRussia() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setMunicipality("Petsamo");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Petsamo", g.getMunicipality());
		assertEquals("Petsamo", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCoordinates());
		assertEquals(null, g.getInterpretations().getSourceOfCoordinates());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
	}

	@Test
	public void oldFinnishMunicipality_coordinates_validation() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setMunicipality("Punkaharju");
		g.setCoordinates(new Coordinates(666, 333, Type.YKJ));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("Punkaharju", g.getMunicipality());
		assertEquals("Punkaharju", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());

		assertEquals("Coordinates are not inside old municipality [Punkaharju] that has been joined to [Savonlinna] they are inside [Inkoo]: 666:333 YKJ", g.getQuality().getLocationIssue().getMessage());
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());

		assertEquals(null, g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
	}

	@Test
	public void oldFinnishMunicipality_coordinates_validation_2() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("g"));
		g.setMunicipality("Punkaharju");
		g.setCoordinates(new Coordinates(6856, 3622, Type.YKJ));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		g = dwRoot.getPublicDocument().getGatherings().get(0);

		assertEquals(null, g.createQuality().getLocationIssue());

		assertEquals("Punkaharju", g.getMunicipality());
		assertEquals("Savonlinna", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.446]", g.getInterpretations().getFinnishMunicipalities().toString()); // Savonlinna
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals("6856.0 : 6857.0 : 3622.0 : 3623.0 : YKJ : 1000", g.getInterpretations().getCoordinates().toString());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
	}


	@Test
	public void country_1() throws Exception {
		List<DwRoot> roots = new LajiETLJSONHarmonizer().harmonize(new JSONObject(getTestData("etl-schema-country-bugtest.json")), new Qname("KE.3"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		// Reported country is "Russia" but coordinates and reported finnish municipality are in Finland
		// Time is after 1917
		// --> country should be Finland, no quality issues

		interpreter.interpret(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("DateRange [begin=1966-09-30, end=1966-09-30]", g.getEventDate().toString());
		assertEquals("667.0 : 668.0 : 313.0 : 314.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString()); // Tiiviisti Suomessa
		assertEquals("Föglö", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(dao.resolveMunicipalitiesByName("Föglö").get(0).getQname(), g.getInterpretations().getFinnishMunicipality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(dao.resolveCountiesByName("Suomi").get(0).getQname(), g.getInterpretations().getCountry());

		assertEquals(null, g.createQuality().getLocationIssue());
	}

	@Test
	public void country_2() throws Exception {
		List<DwRoot> roots = new LajiETLJSONHarmonizer().harmonize(new JSONObject(getTestData("etl-schema-country-bugtest2.json")), new Qname("KE.3"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		// Reported country is Russia, before year 1917
		// Coordinates are from Finland west coast
		// Reported municipality is Sakkola (now part of Russia)
		// -> should get quality issue coordinates / municipality mismatch

		interpreter.interpret(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("DateRange [begin=1906-07-15, end=1906-07-15]", g.getEventDate().toString());

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals("Coordinates are not inside old conceded municipality [Sakkola] they are inside [Pori]: 684:320 YKJ", g.getQuality().getLocationIssue().getMessage());

		assertEquals("684.0 : 685.0 : 320.0 : 321.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals("Sakkola", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("Russia", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCountry());
	}

	@Test
	public void country_3() throws Exception {
		List<DwRoot> roots = new LajiETLJSONHarmonizer().harmonize(new JSONObject(getTestData("etl-schema-country-bugtest3.json")), new Qname("KE.3"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		// Reported country is Russia, year after 1917
		// Coordinates are from Finland Helsinki area
		// Reported municipality is Kuolemajärvi (now part of Russia)
		// -> should get quality issue coordinates / municipality mismatch

		interpreter.interpret(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("DateRange [begin=1977-10-23, end=1977-10-23]", g.getEventDate().toString());

		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals("Coordinates are not inside old conceded municipality [Kuolemajärvi] they are inside [Espoo, Helsinki]: 667:338 YKJ", g.getQuality().getLocationIssue().getMessage());

		assertEquals("667.0 : 668.0 : 338.0 : 339.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals("Kuolemajärvi", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("Russia", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCountry());
	}

	@Test
	public void country_4() throws Exception {
		List<DwRoot> roots = new LajiETLJSONHarmonizer().harmonize(new JSONObject(getTestData("etl-schema-country-bugtest4.json")), new Qname("KE.3"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		// Reported country is Russia, year after 1917
		// Coordinates are from Kuolemajärvi area
		// Reported municipality is Kuolemajärvi (now part of Russia)
		// ->

		interpreter.interpret(root);

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("DateRange [begin=1977-10-23, end=1977-10-23]", g.getEventDate().toString());

		assertEquals(null, g.createQuality().getLocationIssue());

		assertEquals("669.0 : 670.0 : 360.0 : 361.0 : YKJ : 10000", g.getInterpretations().getCoordinates().toString());
		assertEquals("Kuolemajärvi", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfCountry());
		assertEquals(null, g.getInterpretations().getSourceOfFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
	}

	private String getTestData(String filename) throws Exception {
		return ConverterTests.getTestData(filename);
	}

	@Test
	public void springMonitoringCoordinateAccuracy() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(Interpreter.SPRING_MONITORING);
		Gathering g = new Gathering(new Qname("G"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(60, 30, Type.WGS84));

		interpreter.interpret(dwRoot);

		assertEquals(10000, g.getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void springMonitoringCoordinateAccuracy_2() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(Interpreter.SPRING_MONITORING);
		Gathering g = new Gathering(new Qname("G"));
		dwRoot.createPublicDocument().addGathering(g);
		g.setCoordinates(new Coordinates(60, 30, Type.WGS84).setAccuracyInMeters(20000));

		interpreter.interpret(dwRoot);

		assertEquals(20000, g.getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void removeUnitsWithoutTargets() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		dwRoot.createPublicDocument().addGathering(g);
		g.addUnit(new Unit(new Qname("U1")));
		g.addUnit(new Unit(new Qname("U2")));
		g.addUnit(new Unit(new Qname("U3")));
		g.getUnits().get(0).setTaxonVerbatim("susi");
		g.getUnits().get(1).setReportedTaxonId(new Qname("MX.1"));
		assertEquals(3, dwRoot.getPublicDocument().getGatherings().get(0).getUnits().size());
		interpreter.interpret(dwRoot);
		assertEquals(2, dwRoot.getPublicDocument().getGatherings().get(0).getUnits().size());
	}

	@Test
	public void unknownAreaNames() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCountry("Foobarland");
		g.setMunicipality("Foobarplace");
		g.setBiogeographicalProvince("FB");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		assertEquals("Foobarland", g.getInterpretations().getCountryDisplayname());
		assertEquals("Foobarplace", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("FB", g.getInterpretations().getBiogeographicalProvinceDisplayname());

		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getBiogeographicalProvince());
	}

	@Test
	public void coordinatesFarFarAway() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCountry("Foobarland");
		g.setMunicipality("Foobarplace");
		g.setBiogeographicalProvince("FB");
		g.setCoordinates(new Coordinates(0, -20, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
	}

	@Test
	public void coordinatesFarFarAway_2() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCountry("Finland");
		g.setMunicipality("Foobarplace");
		g.setCoordinates(new Coordinates(0, -20, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.COORDINATES_COUNTRY_MISMATCH, g.createQuality().getLocationIssue().getIssue());
		assertEquals("Country reported to be Finland but coordinates are not inside Finland: 0.0, -20.0 WGS84", g.createQuality().getLocationIssue().getMessage());
	}

	@Test
	public void coordinatesFarFarAway_3() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setMunicipality("Helsinki");
		g.setCoordinates(new Coordinates(0, -20, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.createQuality().getLocationIssue().getIssue());
		assertEquals("Coordinates are not inside municipality Helsinki they are outside Finland: 0.0, -20.0 WGS84", g.createQuality().getLocationIssue().getMessage());
	}

	@Test
	public void coordinatesFarFarAway_4() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setMunicipality("Petsamo");
		g.setCoordinates(new Coordinates(0, -20, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.createQuality().getLocationIssue().getIssue());
		assertEquals("Coordinates are not inside old conceded municipality [Petsamo] they are outside Finland: 0.0, -20.0 WGS84", g.createQuality().getLocationIssue().getMessage());
	}

	@Test
	public void coordinatesVeryCloseToBorder() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setMunicipality("Kolari");
		g.setCoordinates(new Coordinates(67.387324, 23.643033, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Kolari", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals(null, g.getInterpretations().getCountry());
	}

	@Test
	public void coordinatesInsideConceded() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.566669, 31.622541, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals(null, g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCountryDisplayname()); // Can't say country is Russia because might be norway as well (conceded municipality bounding boxes go to norway side)
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideConceded_2() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.566669, 31.622541, Type.WGS84));
		g.setMunicipality("Pitkäranta");
		g.setCountry("Suomi");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Pitkäranta", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideConceded_3() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.566669, 31.622541, Type.WGS84));
		g.setMunicipality("Petsamo");
		g.setCountry("Suomi");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals("Coordinates are not inside old conceded municipality [Petsamo] they are inside [Harlu, Impilahti, Salmi, Suojärvi]: 61.566669, 31.622541 WGS84", g.getQuality().getLocationIssue().getMessage());
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals("Petsamo", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideConceded_4() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.566669, 31.622541, Type.WGS84));
		g.setMunicipality("Helsinki");
		g.setCountry("Suomi");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals("Coordinates are not inside municipality Helsinki they are inside [Harlu, Impilahti, Salmi, Suojärvi]: 61.566669, 31.622541 WGS84", g.getQuality().getLocationIssue().getMessage());
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals("Helsinki", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideConceded_5() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(60.90, 29.89, Type.WGS84)); // Not inside Lappeenranta and far away
		g.setMunicipality("Lappeenranta");
		g.setCountry("Suomi");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(
				"Coordinates are not inside municipality Lappeenranta they are inside [Kaukola, Kirvu, Käkisalmi, Käkisalmi Mlk, Pyhäjärvi Vpl, Räisälä, Vuoksela, Vuoksenranta, Äyräpää]: 60.9, 29.89 WGS84",
				g.getQuality().getLocationIssue().getMessage());
		assertEquals(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, g.getQuality().getLocationIssue().getIssue());
		assertEquals("Lappeenranta", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideConceded_6() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(60.922570, 28.601544, Type.WGS84)); // Not inside Lappeenranta but close enough!
		g.setMunicipality("Lappeenranta");
		g.setCountry("Suomi");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.getQuality());
		assertEquals("Lappeenranta", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideFinland_and_in_accurate__concededNameGiven_but_coordinates_close_enough_to_conceded() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(60.931915, 028.468972, Type.WGS84));
		g.setMunicipality("Viipuri");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Viipuri", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void coordinatesInsideFinland_and_outside_concededNameGiven_and_match_conceded_coordinates() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(6758, 3580, Type.YKJ));
		g.setMunicipality("Viipuri");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Viipuri", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipality());
		assertEquals(null, g.getInterpretations().getCountryDisplayname()); // Can't say country is Russia because might be Norway as well (conceded municipality bounding boxes go to Norway side)
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals(null, g.getInterpretations().getSourceOfCountry());
	}

	@Test
	public void selectingTheMunicipalityAfterReportedWhenManyMatches() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(668, 336, Type.YKJ));
		g.setMunicipality("Vihti");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Vihti", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(dao.resolveMunicipalitiesByName("Vihti").get(0).getQname(), g.getInterpretations().getFinnishMunicipality());
		assertEquals("ML.382", g.getInterpretations().getFinnishMunicipality().toString());
		assertEquals("[ML.382]", g.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfFinnishMunicipality());
	}

	@Test
	public void finnishMunicipalitySatelliteArea() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(62.78851840346525, 23.74432618731355, Type.WGS84));
		dwRoot.createPublicDocument().addGathering(g);

		Gathering g2 = new Gathering(new Qname("G2"));
		g2.setCoordinates(new Coordinates(60.198337541568094, 19.33635089343007, Type.WGS84));
		dwRoot.getPublicDocument().addGathering(g2);

		interpreter.interpret(dwRoot);

		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("Alavus", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(dao.resolveMunicipalitiesByName("Alavus").get(0).getQname(), g.getInterpretations().getFinnishMunicipality());
		assertEquals("["+dao.resolveMunicipalitiesByName("Alavus").get(0).getQname()+"]", g.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfFinnishMunicipality());

		assertEquals(null, g2.createQuality().getLocationIssue());
		assertEquals("Hammarland", g2.getInterpretations().getMunicipalityDisplayname());
		assertEquals(dao.resolveMunicipalitiesByName("Hammarland").get(0).getQname(), g2.getInterpretations().getFinnishMunicipality());
		assertEquals("["+dao.resolveMunicipalitiesByName("Hammarland").get(0).getQname()+"]", g2.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals(GeoSource.COORDINATES, g2.getInterpretations().getSourceOfFinnishMunicipality());
	}

	@Test
	public void coordinateAccuracyWithGeo() throws CriticalParseFailure, DataValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setGeo(Geo.fromWKT("POINT(50 30)", Type.WGS84).setAccuracyInMeters(1000));
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);

		assertEquals(1000, dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinateAccuracy().intValue());
	}

	@Test
	public void accurateArea() throws CriticalParseFailure, DataValidationException {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);

		Gathering g1 = new Gathering(new Qname("G1"));
		g1.setGeo(Geo.fromWKT("POLYGON((29.867941 63.386143,29.868588 63.385294,29.869168 63.384959,29.871247 63.384869,29.872539 63.384655,29.873811 63.385107,29.872619 63.385773,29.870365 63.386166,29.868705 63.386576,29.86953 63.385464,29.867941 63.386143))", Type.WGS84));

		Gathering g2 = new Gathering(new Qname("G2"));
		g2.setGeo(Geo.fromWKT("POLYGON((29.867941 63.386143,29.868588 63.385294,29.869168 63.384959,29.871247 63.384869,29.872539 63.384655,29.873811 63.385107,29.872619 63.385773,29.870365 63.386166,29.868705 63.386576,29.86953 63.385464,29.867941 63.386143))", Type.WGS84));
		g2.setAccurateArea(true);

		dwRoot.createPublicDocument().addGathering(g1).addGathering(g2);

		interpreter.interpret(dwRoot);

		assertEquals(1000, dwRoot.getPublicDocument().getGatherings().get(0).getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(1, dwRoot.getPublicDocument().getGatherings().get(1).getInterpretations().getCoordinateAccuracy().intValue());
	}


	@Test
	public void pyhajarvi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(60.535090, 024.214494, Type.WGS84)); // Inside Karkkila (a current municipality) and old municipality Pyhäjärvi
		g.setMunicipality("Pyhäjärvi"); // Old municipality joined to Karkkila AND also a valid current municipality that is no-where close to Karkkila
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.6.1950"))); // Time actually doesn't affect anything
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		assertEquals(null, g.getQuality());
		assertEquals("Karkkila", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.370]", g.getInterpretations().getFinnishMunicipalities().toString()); // ML.370 = Karkkila
		assertEquals("Pyhäjärvi", g.getMunicipality()); // Verbatim
	}

	@Test
	public void salla_very_close_to_border_outside_finland_reported_fi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.94485435372174, 29.040813446044922, Type.WGS84)); // Just outside the border, near Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setLocality("Kelloselkä, raja-asema (Venäjän puolella)");
		g.setCountry("FI");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void salla_very_close_to_border_outside_finland_reported_ru() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.94485435372174, 29.040813446044922, Type.WGS84)); // Just outside the border, near Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void salla_inside_finland_reported_fi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.816148, 28.788891, Type.WGS84)); // Inside Finland, inside current Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setCountry("Finland");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.621]", g.getInterpretations().getFinnishMunicipalities().toString());
	}

	@Test
	public void salla_inside_finland_reported_ru() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.816148, 28.788891, Type.WGS84)); // Inside Finland, inside current Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.621]", g.getInterpretations().getFinnishMunicipalities().toString());
	}

	@Test
	public void salla_conceded_area_reported_fi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.95, 30.37, Type.WGS84)); // Inside conceded Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setCountry("Finland");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void salla_conceded_area_reported_ru() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(66.95, 30.37, Type.WGS84)); // Inside conceded Salla
		g.setMunicipality("Salla"); // Current municipality; parts of it have been conceded to Russia
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void salla_reported_ru_very_far() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(1, 2, Type.WGS84)); // Very far
		g.setMunicipality("Salla");
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void salla_reported_fi_very_far() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(1, 2, Type.WGS84)); // Very far
		g.setMunicipality("Salla");
		g.setCountry("Finland");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals("Coordinates are not inside municipality Salla they are outside Finland: 1.0, 2.0 WGS84", g.getQuality().getLocationIssue().getMessage());
		assertEquals("Finland", g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void uukuniemi_inside_finland_reported_fi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.806, 30.015, Type.WGS84)); // Inside Finland, inside old Uukuniemi
		g.setMunicipality("Uukuniemi"); // Not a current municipality; parts of it have been conceded to Russia parts joined to Parikkala
		g.setCountry("Finland");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
		assertEquals("Parikkala", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.443]", g.getInterpretations().getFinnishMunicipalities().toString());
	}

	@Test
	public void uukuniemi_inside_finland_reported_ru() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.806, 30.015, Type.WGS84)); // Inside Finland, inside old Uukuniemi
		g.setMunicipality("Uukuniemi"); // Not a current municipality; parts of it have been conceded to Russia parts joined to Parikkala
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
		assertEquals("Parikkala", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals("[ML.443]", g.getInterpretations().getFinnishMunicipalities().toString());
	}

	@Test
	public void uukuniemi_conceded_area_reported_fi() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.768058, 30.356924, Type.WGS84)); // Inside conceded Uukuniemi
		g.setMunicipality("Uukuniemi"); // Not a current municipality; parts of it have been conceded to Russia parts joined to Savonlinna
		g.setCountry("Finland");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Uukuniemi", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void uukuniemi_conceded_area_reported_ru() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(61.768058, 30.356924, Type.WGS84)); // Inside conceded Uukuniemi
		g.setMunicipality("Uukuniemi"); // Not a current municipality; parts of it have been conceded to Russia parts joined to Savonlinna
		g.setCountry("Russia");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Venäjä", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.RUSSIA, g.getInterpretations().getCountry());
		assertEquals("Uukuniemi", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void inside_conceded_municipality_box_reported_norway() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(69.81, 29.44, Type.WGS84)); // Norway side close to Russia border
		g.setMunicipality("Nesseby"); // Not any finnish municipality
		g.setCountry("Norway");
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("Norja", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.NORWAY, g.getInterpretations().getCountry());
		assertEquals("Nesseby", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void inside_conceded_municipality_box_country_not_reported() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);
		Gathering g = new Gathering(new Qname("G"));
		g.setCoordinates(new Coordinates(69.81, 29.44, Type.WGS84)); // Norway side close to Russia border
		g.setMunicipality("Nesseby"); // Not any finnish municipality
		dwRoot.createPublicDocument().addGathering(g);

		interpreter.interpret(dwRoot);
		g = dwRoot.getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals(null, g.getInterpretations().getCountryDisplayname());
		assertEquals(null, g.getInterpretations().getCountry());
		assertEquals("Nesseby", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void namedplaceInfo() throws Exception {
		DwRoot dwRoot = new DwRoot(new Qname("12345"),  new Qname("KE.3"));
		dwRoot.setCollectionId(TestDAO.LOW_QUALITY_COLLECTION);

		dwRoot.createPublicDocument().setNamedPlaceIdUsingQname(TestDAO.NAMED_PLACE_ID);

		Gathering g1 = new Gathering(new Qname("G1"));
		g1.setCoordinates(new Coordinates(6665432, 3332101, Type.YKJ));
		g1.setMunicipality("Porvoo");
		g1.setLocality("Jossain");

		Gathering g2 = new Gathering(new Qname("G2"));

		Gathering g3 = new Gathering(new Qname("G3"));
		g3.setCoordinates(new Coordinates(666789, 333456, Type.YKJ));

		Gathering g4 = new Gathering(new Qname("G4"));
		g4.setGeo(Geo.fromWKT("POINT(24.005016 60.043517)", Type.WGS84));

		dwRoot.getPublicDocument().addGathering(g1);
		dwRoot.getPublicDocument().addGathering(g2);
		dwRoot.getPublicDocument().addGathering(g3);
		dwRoot.getPublicDocument().addGathering(g4);

		interpreter.interpret(dwRoot);

		g1 = dwRoot.getPublicDocument().getGatherings().get(0);
		g2 = dwRoot.getPublicDocument().getGatherings().get(1);
		g3 = dwRoot.getPublicDocument().getGatherings().get(2);
		g4 = dwRoot.getPublicDocument().getGatherings().get(3);

		assertEquals("Porvoo", g1.getMunicipality());
		assertEquals(TestDAO.NAMEDPLACE_MUNICIPALITY, g2.getMunicipality());
		assertEquals(TestDAO.NAMEDPLACE_MUNICIPALITY, g3.getMunicipality());

		assertEquals("Jossain", g1.getLocality());
		assertEquals(TestDAO.NAMED_PLACE_NAME, g2.getLocality());
		assertEquals(TestDAO.NAMED_PLACE_NAME, g3.getLocality());

		assertEquals("6665432.0 : 6665433.0 : 3332101.0 : 3332102.0 : YKJ : null", g1.getCoordinates().toString());
		assertEquals(new Coordinates(TestDAO.NAMED_PLACE_YKJ10).toString(), g2.getCoordinates().toString());
		assertEquals("666789.0 : 666790.0 : 333456.0 : 333457.0 : YKJ : null", g3.getCoordinates().toString());
		assertEquals(null, g4.getCoordinates());
	}

	@Test
	public void outOfFinland_complexLine() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("lajistore-norway-complex_line_bug.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		assertEquals(null, g.getHigherGeography());
		assertEquals("Norway", g.getCountry());
		assertEquals("Troms", g.getProvince());
		assertEquals(null, g.getBiogeographicalProvince());
		assertEquals("Lyngen kommune", g.getMunicipality());
		assertEquals("Nordkjosbotn - Storslett", g.getLocality());
		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals("[LINESTRING (19.552917 69.219151, 19.632568 69.218176, 19.69574 69.187945, 19.78363 69.198677, 19.871521 69.253231, 19.970398 69.275597, 19.970398 69.295998, 20.077515 69.351276, 20.190125 69.389016, 20.278015 69.365799, 20.308228 69.38128, 20.267029 69.39095, 20.253296 69.418002, 20.32196 69.461408, 20.376892 69.543159, 20.588379 69.544119, 20.629578 69.518186, 20.690002 69.507612, 20.722961 69.511457, 20.830078 69.490297, 20.76416 69.523951, 20.681763 69.524912, 20.593872 69.562349, 20.585632 69.579605, 20.533447 69.604506, 20.475769 69.615989, 20.462036 69.641804, 20.484009 69.670449, 20.530701 69.705726, 20.522461 69.735237, 20.494995 69.753304, 20.569153 69.775154, 20.66803 69.79129, 20.791626 69.822576, 20.917969 69.805516, 20.975647 69.773255, 21.033325 69.768506)]", g.getGeo().getWKTList().toString());

		assertEquals(null, g.getInterpretations());

		interpreter.interpret(root);

		assertEquals("Norja", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.NORWAY, g.getInterpretations().getCountry());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCountry());

		assertEquals(null, g.getInterpretations().getBiogeographicalProvinces());
		assertEquals(null, g.getInterpretations().getSourceOfBiogeographicalProvince());

		assertEquals("69.187945 : 69.822576 : 19.552917 : 21.033325 : WGS84 : 100000", g.getInterpretations().getCoordinates().toString());
		assertEquals(100000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());

		assertEquals(null, g.getInterpretations().getFinnishMunicipalities());
		assertEquals("Lyngen kommune", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(null, g.getInterpretations().getSourceOfFinnishMunicipality());

		assertEquals(null, g.getInterpretations().getNaturaAreas());
	}

	@Test
	public void close_to_conceced_but_inside_Finland_but_wrong_municipality_verbatim() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("lajistore-venaja-bug.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		assertEquals(null, g.getHigherGeography());
		assertEquals("Suomi", g.getCountry());
		assertEquals(null, g.getProvince());
		assertEquals("Koillismaa", g.getBiogeographicalProvince());
		assertEquals("Kuusamo", g.getMunicipality()); // Wrong municipality name, actually in Salla
		assertEquals("Oulanka", g.getLocality());
		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals("[POINT (28.996375 66.457255)]", g.getGeo().getWKTList().toString());
		assertEquals(Type.WGS84, g.getGeo().getCRS());

		assertEquals(null, g.getInterpretations());

		interpreter.interpret(root);

		assertEquals("Suomi", g.getInterpretations().getCountryDisplayname());
		assertEquals(Const.FINLAND, g.getInterpretations().getCountry());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfCountry());

		assertEquals("[ML.267]", g.getInterpretations().getBiogeographicalProvinces().toString());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfBiogeographicalProvince());

		assertEquals("66.457255 : 66.457255 : 28.996375 : 28.996375 : WGS84 : 1", g.getInterpretations().getCoordinates().toString());
		assertEquals(1, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());

		assertEquals("[ML.621]", g.getInterpretations().getFinnishMunicipalities().toString());
		assertEquals("Salla", g.getInterpretations().getMunicipalityDisplayname());
		assertEquals(GeoSource.COORDINATES, g.getInterpretations().getSourceOfFinnishMunicipality());
	}

	@Test
	public void line_coordinate_accuracy_interpretation() throws Exception {
		List<DwRoot> roots = new LajistoreHarmonizer(null, null).harmonize(new JSONObject(getTestData("lajistore-coord-acc-bug.json")), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals("" +
				"[LINESTRING (22.196878 62.864457, 22.195955 62.864612, 22.195112 62.864784, 22.194277 62.864953, 22.193438 62.865115, 22.192599 62.865276, 22.191776 62.865436, 22.190958 62.865609, 22.190142 62.865774, 22.189336 62.865935, 22.188551 62.866095, 22.187778 62.866257, 22.186997 62.866422, 22.186204 62.866587, 22.185395 62.866752, 22.184585 62.866917, 22.183787 62.867085, 22.18301 62.867262, 22.182245 62.867447, 22.181484 62.867629, 22.180715 62.867819, 22.179937 62.868005, 22.179172 62.868192, 22.178397 62.868377, 22.177608 62.868558, 22.176815 62.868747, 22.175199 62.869131, 22.174373 62.869322, 22.173541 62.869516, 22.172703 62.86971, 22.171862 62.869903, 22.171032 62.8701, 22.170206 62.870296, 22.16938 62.870492, 22.168549 62.870686, 22.167713 62.870881, 22.166879 62.87108, 22.166047 62.871276, 22.165209 62.871471, 22.164349 62.871644, 22.160575 62.872057, 22.159624 62.872169, 22.158697 62.872295, 22.157824 62.872452, 22.157 62.872647, 22.156193 62.87286, 22.155389 62.873086, 22.154593 62.873313, 22.153814 62.873528, 22.153029 62.87374, 22.152233 62.873954, 22.151402 62.874169, 22.150571 62.87438, 22.149737 62.874594, 22.148926 62.874807, 22.148121 62.875024, 22.147318 62.875232, 22.146505 62.875436, 22.145677 62.875639, 22.144837 62.875848, 22.143992 62.876064, 22.143142 62.876285, 22.142276 62.876508, 22.141408 62.876729, 22.140545 62.876946, 22.139682 62.877166, 22.138813 62.877392, 22.137928 62.877617, 22.137037 62.877841, 22.136144 62.878066, 22.135267 62.87829, 22.134414 62.878514, 22.133564 62.878738, 22.132712 62.878957, 22.13186 62.879169, 22.131025 62.879383, 22.130287 62.879574, 22.129756 62.879715, 22.129257 62.879879, 22.128632 62.879959, 22.128016 62.880116, 22.127301 62.880303, 22.126745 62.880447, 22.126134 62.880599, 22.125506 62.880752, 22.124874 62.880909, 22.124214 62.881068, 22.123505 62.881239, 22.122761 62.881419, 22.121985 62.881608, 22.121189 62.881801, 22.120385 62.882003, 22.119594 62.882199, 22.118803 62.882392, 22.117997 62.882584, 22.117176 62.882781, 22.11635 62.882979, 22.115509 62.883175, 22.114656 62.883372, 22.113807 62.883568, 22.112952 62.883765, 22.112084 62.883965, 22.111201 62.884169, 22.110296 62.884377, 22.10939 62.884586, 22.108486 62.884795, 22.107581 62.885002, 22.106677 62.885217, 22.10577 62.885428, 22.104859 62.885637, 22.103948 62.885849, 22.103046 62.88606, 22.102153 62.886269, 22.101263 62.886479, 22.100389 62.886684, 22.099533 62.886877, 22.098695 62.887063, 22.097875 62.887254, 22.097055 62.887449, 22.096248 62.887651, 22.095513 62.887863, 22.094858 62.888098, 22.094262 62.888364, 22.093704 62.888658, 22.093202 62.888961, 22.092716 62.889273, 22.092201 62.889586, 22.091668 62.889891, 22.091113 62.890187, 22.09053 62.890466, 22.089904 62.890735, 22.089292 62.891001, 22.088687 62.891269, 22.088071 62.891534, 22.08745 62.891803, 22.086844 62.892066, 22.086237 62.892333), LINESTRING (22.196857 62.864449, 22.198871 62.86398), LINESTRING (22.233127 62.846259, 22.230948 62.847887, 22.22521 62.850463, 22.221677 62.853211, 22.209397 62.861301, 22.197412 62.864295)]",
				g.getGeo().getWKTList().toString());

		assertEquals(null, g.getInterpretations());

		interpreter.interpret(root);

		assertEquals(5000, g.getInterpretations().getCoordinateAccuracy().intValue());
		assertEquals(GeoSource.REPORTED_VALUE, g.getInterpretations().getSourceOfCoordinates());
		assertEquals("62.846259 : 62.892333 : 22.086237 : 22.233127 : WGS84 : 5000", g.getInterpretations().getCoordinates().toString());
	}

	@Test
	public void interpretUnitKeywords() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.addKeyword("key0");
		u.addKeyword("key0");
		u.addKeyword("key1");
		u.addFact("additionalIDs", "key1");
		u.addFact("additionalIDs", "key2");
		u.addFact("MY.additionalIDs", "key3");
		u.addFact("http://tun.fi/MY.additionalIDs", "key4");

		interpreter.interpret(root);

		assertEquals("[additionalIDs : key1, additionalIDs : key2, MY.additionalIDs : key3, http://tun.fi/MY.additionalIDs : key4]", u.getFacts().toString());
		assertEquals("[key0, key1, key2, key3, key4]", u.getKeywords().toString());
	}

	@Test
	public void invasiveControl() throws Exception {
		JSONObject json = new JSONObject(RdfXmlHarmonizerTests.getTestData("invasive-control.json"));
		List<DwRoot> roots = new LajistoreHarmonizer(dao.getLocalContextDefinition(), null).harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		interpreter.interpret(root);
		Document d = root.getPublicDocument();
		assertEquals(2, d.getGatherings().size());
		assertEquals(0, d.getGatherings().get(0).getUnits().size());
		assertEquals(1, d.getGatherings().get(1).getUnits().size());
		Unit u = d.getGatherings().get(1).getUnits().get(0);
		assertEquals(InvasiveControl.PARTIAL, u.getInterpretations().getInvasiveControlEffectiveness());
		assertEquals(false, u.getInterpretations().isInvasiveControlled());
	}

	@Test
	public void wild() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");

		// By default wild is unknown
		interpreter.interpret(root);
		assertEquals(null, u.isWild());
	}

	@Test
	public void wild_annotated_as_non_wild() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");

		u.setSourceTags(new ArrayList<>(Utils.list(Tag.ADMIN_MARKED_NON_WILD)));
		interpreter.interpret(root);
		assertEquals(false, u.isWild());
	}

	@Test
	public void wild_non_wild_by_plantstatuscode() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");

		u.setPlantStatusCodeUsingQname((new Qname("MY.plantStatusCodeR")));
		interpreter.interpret(root);
		assertEquals(false, u.isWild());
	}

	@Test
	public void wild_non_wildfrom_taxonomy() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);

		u.setTaxonVerbatim("koira");
		interpreter.interpret(root);
		assertEquals(false, u.isWild());
	}

	@Test
	public void wild_from_taxonomy_but_explicitly_set_as_wild() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);

		u.setWild(true);
		u.setTaxonVerbatim("koira");
		interpreter.interpret(root);
		assertEquals(true, u.isWild());
	}

	@Test
	public void non_individual_count_abundance_unit() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAbundanceString("3");
		u.setAbundanceUnit(AbundanceUnit.SQUARE_M);

		interpreter.interpret(root);

		assertEquals(1, u.getInterpretations().getIndividualCount());
	}

	@Test
	public void atlas_code_from_abundance() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAbundanceString("atl:3");

		interpreter.interpret(root);

		assertEquals(1, u.getInterpretations().getIndividualCount());
		assertEquals("http://tun.fi/MY.atlasCodeEnum3", u.getAtlasCode());
		assertEquals("http://tun.fi/MY.atlasClassEnumB", u.getAtlasClass());
	}

	@Test
	public void atlas_code_from_notes() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAbundanceString("3");
		u.setNotes("Atl:71");

		interpreter.interpret(root);

		assertEquals(3, u.getInterpretations().getIndividualCount());
		assertEquals("http://tun.fi/MY.atlasCodeEnum71", u.getAtlasCode());
		assertEquals("http://tun.fi/MY.atlasClassEnumD", u.getAtlasClass());
	}

	@Test
	public void atlas_code_from_notes_2() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAbundanceString("3");
		u.setNotes("Atl:71");
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum2"));
		interpreter.interpret(root);

		assertEquals(3, u.getInterpretations().getIndividualCount());
		assertEquals("http://tun.fi/MY.atlasCodeEnum2", u.getAtlasCode());
		assertEquals("http://tun.fi/MY.atlasClassEnumB", u.getAtlasClass());
	}

	@Test
	public void atlas_code_from_notes_3() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setNotes("Atl:0");
		interpreter.interpret(root);

		assertEquals(null, u.getAtlasCode());
		assertEquals(null, u.getAtlasClass());
	}

	@Test
	public void atlas_code_from_notes_4() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setNotes("ATL:50");
		interpreter.interpret(root);

		assertEquals("http://tun.fi/MY.atlasCodeEnum5", u.getAtlasCode());
		assertEquals("http://tun.fi/MY.atlasClassEnumC", u.getAtlasClass());
	}

	@Test
	public void local_from_atlascode_1() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum1"));
		interpreter.interpret(root);

		assertNull(u.isLocal());
	}

	@Test
	public void local_from_atlascode_2() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum61"));
		interpreter.interpret(root);

		assertTrue(u.isLocal());
	}


	@Test
	public void atlas_class() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");

		// no code or class
		interpreter.interpret(root);
		assertEquals(null, u.getAtlasClass());

		// class without code -> keep the given class
		u.setAtlasClassUsingQname(new Qname("MY.atlasClassEnumD"));
		interpreter.interpret(root);
		assertEquals(null, u.getAtlasCode());
		assertEquals("MY.atlasClassEnumD", Qname.fromURI(u.getAtlasClass()).toString());

		// resolve via code
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum1"));
		interpreter.interpret(root);
		assertEquals("MY.atlasClassEnumA", Qname.fromURI(u.getAtlasClass()).toString());
		assertEquals(null, u.isBreedingSite());

		// resolve via code
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum61"));
		interpreter.interpret(root);
		assertEquals("MY.atlasClassEnumC", Qname.fromURI(u.getAtlasClass()).toString());
		assertEquals(true, u.isBreedingSite());

		// resolve via code overrides set class
		u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum8"));
		u.setAtlasClassUsingQname(new Qname("MY.atlasClassEnumA"));
		interpreter.interpret(root);
		assertEquals("MY.atlasClassEnumD", Qname.fromURI(u.getAtlasClass()).toString());
		assertEquals(true, u.isBreedingSite());
	}

	@Test
	public void raatoseurantacode() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("JX.1"), new Qname("KE.1"));
		root.setCollectionId(TestDAO.HIGH_QUALITY_COLLECTION);
		root.createPublicDocument().addGathering(new Gathering(new Qname("JX.1#1")));
		Unit u = new Unit(new Qname("JX.1#2"));
		root.getPublicDocument().getGatherings().get(0).addUnit(u);
		u.setTaxonVerbatim("must have something");
		u.setAbundanceString("[1] XXX:12");

		interpreter.interpret(root);

		assertEquals(null, u.getLifeStage());
		assertEquals(false, u.isAlive());
		assertEquals(1, u.getInterpretations().getIndividualCount());
	}

}

