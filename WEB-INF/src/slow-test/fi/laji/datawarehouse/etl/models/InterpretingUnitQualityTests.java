package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.etl.models.Annotator.PersonIdResolver;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class InterpretingUnitQualityTests {

	private static DAOImple dao;
	private static Annotator annotator;
	private static Interpreter interpreter = new Interpreter(dao);

	@BeforeClass
	public static void init() {
		dao = new TestDAO(InterpretingUnitQualityTests.class);
		annotator = new Annotator(new PersonIdResolver() {
			@Override
			public Qname resolve(String userId) {
				return null;
			}
		});
		interpreter = new Interpreter(dao);
	}

	@AfterClass
	public static void close() {
		dao.close();
	}

	@Test
	public void does_not_invalidate_secured() throws Exception {
		DwRoot root = new DwRoot(new Qname("d"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));

		Annotation annotation = new Annotation(new Qname("a"), new Qname("d"), new Qname("u"), DateUtils.getCurrentEpoch());
		annotation.setIdentification(new Identification("kangaskäärme", new Qname("MX.37637")));
		annotation.setOccurrenceAtTimeOfAnnotation(new OccurrenceAtTimeOfAnnotation());
		annotation.getOccurrenceAtTimeOfAnnotation().setTaxonId(new Qname("MX.37637"));
		annotation.getOccurrenceAtTimeOfAnnotation().setDateBegin(DateUtils.convertToDate("1.1.2020")); // data has been secured to date range
		annotation.getOccurrenceAtTimeOfAnnotation().setDateEnd(DateUtils.convertToDate("31.12.2020"));
		annotation.getOccurrenceAtTimeOfAnnotation().setWgs84centerPointLat(61.334995); // coordinates have been secured - they are interpreted to centerpoint of municipality and then secured to grid and then this is the secured grid's centerpoint
		annotation.getOccurrenceAtTimeOfAnnotation().setWgs84centerPointLon(21.379006);
		annotation.getOccurrenceAtTimeOfAnnotation().setCountryVerbatim("FI");
		annotation.getOccurrenceAtTimeOfAnnotation().setMunicipalityVerbatim(""); // secured - municipality verbatim has been removed
		annotation.setAnnotationByPerson(new Qname("MA.1"));
		annotation.setAnnotationBySystem(new Qname("KE.3"));
		root.addAnnotation(annotation);

		Document document = root.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("g"));
		Unit unit = new Unit(new Qname("u"));

		gathering.setMunicipality("Eurajoki");
		gathering.setCountry("FI");
		gathering.setEventDate(new DateRange(DateUtils.convertToDate("5.7.2020")));

		unit.setTaxonVerbatim("kangaskäärme");

		document.addGathering(gathering);
		gathering.addUnit(unit);

		annotator.annotate(root);
		interpreter.interpret(root);

		assertEquals(Const.FINLAND, gathering.getInterpretations().getCountry());
		assertEquals("61.139812 : 61.530175 : 20.812056 : 21.945946 : WGS84 : 100000", gathering.getInterpretations().getCoordinates().toString());

		assertEquals(false, unit.getAnnotations().get(0).isDeleted());
		assertEquals(true, unit.getAnnotations().get(0).getValid());
		assertEquals(RecordQuality.COMMUNITY_VERIFIED, unit.getInterpretations().getRecordQuality());
		assertEquals(null, unit.getInterpretations().getAnnotatedTaxonId()); // identification has not changed from original

		assertEquals("Dare Talvitie", unit.getAnnotations().get(0).getAnnotationByPersonName());
		assertEquals("Kotka", unit.getAnnotations().get(0).getAnnotationBySystemName());
	}

	@Test
	public void source_tag_and_annotation() throws Exception {
		DwRoot root = new DwRoot(new Qname("d"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));

		Document document = root.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("g"));
		gathering.setCountry("FI");
		Unit unit = new Unit(new Qname("u"));
		unit.setTaxonVerbatim("meirantaputki");

		unit.addSourceTag(Tag.COMMUNITY_TAG_VERIFIED);

		document.addGathering(gathering);
		gathering.addUnit(unit);

		Annotation annotation = new Annotation(new Qname("a"), new Qname("d"), new Qname("u"), DateUtils.getCurrentEpoch());
		annotation.addTag(Tag.EXPERT_TAG_ERRONEOUS);
		annotation.setOccurrenceAtTimeOfAnnotation(new OccurrenceAtTimeOfAnnotation());
		annotation.getOccurrenceAtTimeOfAnnotation().setTaxonVerbatim("meirantaputki");
		annotation.getOccurrenceAtTimeOfAnnotation().setCountryVerbatim("FI");
		root.addAnnotation(annotation);

		annotator.annotate(root);
		interpreter.interpret(root);

		assertEquals(false, unit.getAnnotations().get(0).isDeleted());
		assertEquals(true, unit.getAnnotations().get(0).getValid());
		assertEquals(RecordQuality.ERRONEOUS, unit.getInterpretations().getRecordQuality()); // expert tag overrides community tag
		assertEquals(null, unit.getInterpretations().getAnnotatedTaxonId()); // identification has not changed from original
	}

	@Test
	public void automated_taxon_validations() throws Exception {
		Unit unit = createTestData(new DateRange(DateUtils.convertToDate("6.4.2019")), new Coordinates(62.18, 26.60, Type.WGS84));
		assertEquals("[AUTO_DISTRIBUTION_CHECK, AUTO_PERIOD_CHECK]", unit.getInterpretations().getEffectiveTags().toString());

		unit = createTestData(new DateRange(DateUtils.convertToDate("6.7.2019")), new Coordinates(62.18, 26.60, Type.WGS84));
		assertEquals("[AUTO_DISTRIBUTION_CHECK]", unit.getInterpretations().getEffectiveTags().toString());

		unit = createTestData(new DateRange(DateUtils.convertToDate("6.4.1975")), new Coordinates(62.18, 26.60, Type.WGS84));
		assertEquals("[AUTO_PERIOD_CHECK]", unit.getInterpretations().getEffectiveTags().toString());

		unit = createTestData(new DateRange(DateUtils.convertToDate("6.7.1975")), new Coordinates(62.18, 26.60, Type.WGS84));
		assertEquals("[AUTO_VALIDATIONS_PASS]", unit.getInterpretations().getEffectiveTags().toString());
	}

	private Unit createTestData(DateRange eventDate, Coordinates coordinates) throws Exception {
		DwRoot root = new DwRoot(new Qname("d"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));

		Document document = root.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("g"));
		gathering.setCountry("FI");
		gathering.setEventDate(eventDate);
		gathering.setCoordinates(coordinates);
		Unit unit = new Unit(new Qname("u"));
		unit.setTaxonVerbatim("Xanthorhoe decoloraria");

		document.addGathering(gathering);
		gathering.addUnit(unit);

		interpreter.interpret(root);
		return unit;
	}

	@Test
	public void unknown_taxon_expert_verified() throws Exception {
		DwRoot root = new DwRoot(new Qname("d"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));
		Document document = root.createPublicDocument();
		Gathering gathering = new Gathering(new Qname("g"));
		gathering.setCountry("FI");
		Unit unit = new Unit(new Qname("u"));
		unit.setTaxonVerbatim("talitiainen");
		document.addGathering(gathering);
		gathering.addUnit(unit);

		Annotation annotation = new Annotation(new Qname("a"), new Qname("d"), new Qname("u"), DateUtils.getCurrentEpoch());
		annotation.addTag(Tag.EXPERT_TAG_VERIFIED);
		annotation.setIdentification(new Identification("Foobirus baarius"));
		annotation.setOccurrenceAtTimeOfAnnotation(new OccurrenceAtTimeOfAnnotation());
		annotation.getOccurrenceAtTimeOfAnnotation().setTaxonVerbatim("talitiainen");
		annotation.getOccurrenceAtTimeOfAnnotation().setCountryVerbatim("FI");
		root.addAnnotation(annotation);

		annotator.annotate(root);
		interpreter.interpret(root);

		assertEquals(false, unit.getAnnotations().get(0).isDeleted());
		assertEquals(true, unit.getAnnotations().get(0).getValid());
		assertEquals(RecordQuality.NEUTRAL, unit.getInterpretations().getRecordQuality());
		assertEquals(null, unit.getInterpretations().getAnnotatedTaxonId());
		assertEquals("[]", unit.getInterpretations().getEffectiveTags().toString());
		assertEquals(false, unit.getInterpretations().getNeedsCheck());
		assertEquals(false, unit.getInterpretations().getNeedsIdentification());
		assertEquals("MX.34567", dao.getTaxonLinkingService().getTaxonId(unit, gathering, true).toString());
		assertEquals("MX.34567", dao.getTaxonLinkingService().getTaxonId(unit, gathering, false).toString());
	}

}

