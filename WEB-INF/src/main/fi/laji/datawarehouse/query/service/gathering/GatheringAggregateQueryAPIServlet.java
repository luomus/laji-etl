package fi.laji.datawarehouse.query.service.gathering;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitAggregateQueryAPI;

@WebServlet(urlPatterns = {"/query/gathering/aggregate/*"})
public class GatheringAggregateQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = 5449376292983313868L;

	@Override
	protected Base getBase() {
		return Base.GATHERING;
	}

}
