package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/sample/count/*"})
public class PrivateSampleCountQueryAPI extends SampleCountQueryAPI {

	private static final long serialVersionUID = 1063388208080848807L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
