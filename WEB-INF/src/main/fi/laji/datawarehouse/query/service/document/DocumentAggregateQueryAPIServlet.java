package fi.laji.datawarehouse.query.service.document;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitAggregateQueryAPI;

@WebServlet(urlPatterns = {"/query/document/aggregate/*"})
public class DocumentAggregateQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = 4222019171260575175L;

	@Override
	protected Base getBase() {
		return Base.DOCUMENT;
	}

}
