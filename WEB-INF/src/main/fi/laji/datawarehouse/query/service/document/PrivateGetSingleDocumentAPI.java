package fi.laji.datawarehouse.query.service.document;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/single/*"})
public class PrivateGetSingleDocumentAPI extends GetSingleDocumentAPI {

	private static final long serialVersionUID = -2086610885287498531L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
