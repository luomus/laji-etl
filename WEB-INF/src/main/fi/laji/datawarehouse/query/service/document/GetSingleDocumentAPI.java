package fi.laji.datawarehouse.query.service.document;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;

import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.UnsupportedFormatException;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.GetSingleQuery;
import fi.laji.datawarehouse.query.service.BaseQueryAPIServlet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/query/document/*", "/query/single/*"})
public class GetSingleDocumentAPI extends BaseQueryAPIServlet {

	private static final long serialVersionUID = -2517517519574351016L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		GetSingleQuery query = null;
		try {
			query = new GetSingleQuery(getBaseQuery(req, Base.GATHERING, false, false)); // Base gathering: when asking for own document the query is used to check if user has editor or observer id based access
			if (invalidDocumentIdFilter(query)) {
				throw new IllegalArgumentException("Must give one documentId");
			}
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		Access access = null;
		try {
			access = getAccess(query.getApiSourceId());
		} catch (TooManyRequestsException e) {
			return tooManyRequests429(res, req, e);
		}

		try {
			return executeQuery(query, res, req);
		} catch (UnsupportedFormatException | UnsupportedOperationException | IllegalArgumentException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		} finally {
			access.release();
		}
	}

	private boolean invalidDocumentIdFilter(GetSingleQuery query) {
		if (query.getFilters().getDocumentId() == null) return true;
		if (query.getFilters().getDocumentId().size() != 1) return true;
		return (!given(query.getFilters().getDocumentId().iterator().next()));
	}

	private ResponseData executeQuery(GetSingleQuery query, HttpServletResponse res, HttpServletRequest req) throws Exception {
		Qname documentId = query.getFilters().getDocumentId().iterator().next();
		VerticaDAO verticaDAO = getVerticaDao(query);
		getDao().logQuery(query);
		if (getWarehouseType() == Concealment.PUBLIC && query.getWarehouse() == Concealment.PRIVATE) {
			// Using public api to get document from private side (own observations or owned records search or some other kind of permission to private data):
			// Check using list query that all other filters with the document id causes a match -> the user has permissions to see the private document.
			// Note that this does not work for documents without gatherings... shouldn't be any
			CountQuery countQuery = new CountQuery(query);
			long count = verticaDAO.getQueryDAO().getCount(countQuery).getTotal();
			if (count < 1) {
				return unloggedNotFoundError404(
						"Nothing found with document id " + documentId.toURI() +
						" from " + query.getWarehouse() + " warehouse using the defined filters",
						res, req);
			}
		}
		Document document = verticaDAO.getQueryDAO().get(documentId, query.isApprovedDataRequest());
		if (document == null) return unloggedNotFoundError404(
				"Nothing found with document id " + documentId.toURI() +
				" from " + query.getWarehouse() + " warehouse",
				res, req);
		return writeResponse(document, getFormat(req));
	}


	private ResponseData writeResponse(Document document, ResponseFormat format) throws Exception {
		JSONObject jsonResponse = buildJsonResponse(document);
		if (format == ResponseFormat.JSON) {
			return jsonResponse(jsonResponse);
		} else if (format == ResponseFormat.XML){
			String xml = "<response>" + XML.toString(jsonResponse.reveal()) + "</response>";
			return response(xml, "application/xml");
		}
		throw new UnsupportedOperationException("Not yet implemented for format " + format);
	}

	private JSONObject buildJsonResponse(Document document) {
		JSONObject response = new JSONObject();
		response.setObject("document", ModelToJson.toJson(document));
		return response;
	}

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC;
	}

}
