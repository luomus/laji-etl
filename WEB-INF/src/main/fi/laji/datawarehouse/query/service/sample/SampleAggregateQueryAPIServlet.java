package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitAggregateQueryAPI;

@WebServlet(urlPatterns = {"/query/sample/aggregate/*"})
public class SampleAggregateQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = -7866340661057158211L;

	@Override
	protected Base getBase() {
		return Base.SAMPLE;
	}

}
