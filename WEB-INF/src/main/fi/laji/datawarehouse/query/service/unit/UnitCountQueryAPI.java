package fi.laji.datawarehouse.query.service.unit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;

import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.service.BaseQueryAPIServlet;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/query/unit/count/*", "/query/count/*"})
public class UnitCountQueryAPI extends BaseQueryAPIServlet {

	private static final long serialVersionUID = -6861535809234985431L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		CountQuery query = null;
		ResponseFormat format = null;
		try {
			format = getFormat(req);
			query = getQuery(req);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		Access access = null;
		try {
			access = getAccess(query.getApiSourceId());
		} catch (TooManyRequestsException e) {
			return tooManyRequests429(res, req, e);
		}
	
		try {
			return executeQuery(query, format);
		} catch (UnsupportedOperationException | IllegalArgumentException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		} finally {
			access.release();
		}
	}

	private ResponseData executeQuery(CountQuery query, ResponseFormat format) {
		VerticaDAO verticaDAO = getVerticaDao(query);
		getDao().logQuery(query);
		CountResponse response = verticaDAO.getQueryDAO().getCount(new CountQuery(query));
		return writeResponse(format, response);
	}

	private ResponseData writeResponse(ResponseFormat format, CountResponse response) {
		JSONObject json = new ResponseUtil().buildCountResponse(response);
		if (format == ResponseFormat.JSON) {
			return jsonResponse(json);
		} else if (format == ResponseFormat.XML){
			String xml = "<response>" + XML.toString(json.reveal()) + "</response>";
			return response(xml, "application/xml");
		} else if (format == ResponseFormat.TEXT_PLAIN) {
			return response(String.valueOf(response.getTotal()), "text/plain");
		}
		throw new UnsupportedOperationException("Not yet implemented for format " + format);
	}

	private CountQuery getQuery(HttpServletRequest req) throws Exception {
		BaseQuery baseQuery = getBaseQuery(req, getBase(), false, true);
		return new CountQuery(baseQuery);
	}

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC;
	}

	protected Base getBase() {
		return Base.UNIT;
	}

}
