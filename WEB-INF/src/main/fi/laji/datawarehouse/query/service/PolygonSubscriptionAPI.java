package fi.laji.datawarehouse.query.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.FinlandAreaUtil;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.service.DownloadRequestAPI.DownloadLimitExceededException;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/polygon/*"})
/**
 * This service is used to subscribe a polygon to get a polygon id that can be used to filter occurrences via polygonId -filter
 */
public class PolygonSubscriptionAPI extends ETLBaseServlet {

	public static final int MAX_POLYGON_COMPLEXITY = 100;
	public static final int MAX_AREA_SIZE = 100000;

	private static final long serialVersionUID = -163175229794292025L;

	public static class PolygonValidationException extends Exception {
		private static final long serialVersionUID = -7414875766227672678L;
		private String fi;
		private String en;
		private String sv;
		private PolygonValidationException(String fi, String en, String sv) {
			super(en);
			this.fi = fi;
			this.en = en;
			this.sv = sv;
		}
		private PolygonValidationException(String fi, String en, String sv, Exception cause) {
			super(en, cause);
			this.fi = fi;
			this.en = en;
			this.sv = sv;
		}
		public LocalizedText getLocalizedText() {
			return new LocalizedText().set("fi", fi).set("sv", sv).set("en", en);
		}

		public static PolygonValidationException noFeatures() {
			return new PolygonValidationException(
					"Annettu polygoni oli tyhjä",
					"Polygon must be given, it was empty",
					"Polygon var tom");
		}
		public static PolygonValidationException tooManyFeatures(int count) {
			return new PolygonValidationException(
					"Useita polygoneja annettu, tätyy antaa vain yksi, oli " + count,
					"Multiple polygons were given, must provide only one, there were " + count,
					"För många polygoner, det fanns " + count);
		}
		public static PolygonValidationException notPolygon() {
			return new PolygonValidationException(
					"Kuvio ei ollut polygoni",
					"Given geometry was not a polygon",
					"Given geometri var inte en polygon");
		}
		public static PolygonValidationException tooComplex(int size) {
			return new PolygonValidationException(
					"Annettu polygoni on liian monimutkainen: Pisteitä on " + size + ", maksimi on " + MAX_POLYGON_COMPLEXITY + " pistettä",
					"The polygon is too complex: " + size + " points, max allowed is " + MAX_POLYGON_COMPLEXITY,
					"Polygonen är för komplex: " + size + " poäng, max tillåtet är " + MAX_POLYGON_COMPLEXITY);
		}
		public static PolygonValidationException notFromFinland() {
			return new PolygonValidationException(
					"Polygonihaku toimii ainoastaan Suomessa (perustuu ETRS-TM35FIN koordinaattijärjestelmään)",
					"Polygon search is implemented only for Finland (based on ETRS-TM35FIN coordinate system)",
					"Polygonsökning har implementerats endast för Finland (ETRS-TM35FIN koordinatsystem)");
		}
		public static PolygonValidationException tooLarge() {
			return new PolygonValidationException(
					"Polygoni kattaa liian suuren alueen: Maksimi korkeus tai leveys on " + MAX_AREA_SIZE + " metriä",
					"Polygon area is too large: Max height or width is " + MAX_AREA_SIZE + " meters",
					"Polygonområdet är för stort: Max höjd eller bredd är " + MAX_AREA_SIZE + " meter");
		}
		public static PolygonValidationException conversionProblem() {
			return new PolygonValidationException(
					"Virheellinen polygoni (koordinaattimuunnos epäonnistui)",
					"Erroneous polygon (coordinate conversion failed)",
					"Felaktig polygon (koordinatkonverteringsproblem)");
		}
		public static PolygonValidationException parsingGeo(Exception e) {
			return new PolygonValidationException(
					"Epäkelpo polygoni: " + e.getMessage(),
					"Invalid polygon: " + e.getMessage(),
					"Felaktig polygon: " + e.getMessage(),
					e);
		}
		public static PolygonValidationException argumentError(Exception e) {
			return new PolygonValidationException(
					"Virheellinen API parametri: " + e.getMessage(),
					"Invalid API parameter: " + e.getMessage(),
					"Ogiltig API-parameter: " + e.getMessage());
		}
	}

	public static class PolygonSearchUtil {
		private final String crsParam;
		private final String geoJson;
		private final String wkt;

		public PolygonSearchUtil(HttpServletRequest req) {
			this.crsParam = req.getParameter(Const.CRS);
			this.geoJson = req.getParameter(Const.GEO_JSON_REQUEST);
			this.wkt = req.getParameter(Const.WKT);
		}

		public PolygonSearchUtil(String crsParam, String geoJson, String wkt) {
			this.crsParam = crsParam;
			this.geoJson = geoJson;
			this.wkt = wkt;

		}

		public String getAndValidateEurefWKT() throws PolygonValidationException {
			try {
				Polygon polygon = getAndValidatePolygon();
				return polygon.getWKT();
			} catch (PolygonValidationException e) {
				throw e;
			} catch (DataValidationException e) {
				throw PolygonValidationException.parsingGeo(e);
			} catch (Exception e) {
				throw PolygonValidationException.argumentError(e);
			}
		}

		private Polygon getAndValidatePolygon() throws PolygonValidationException, DataValidationException {
			Type crs = getCRS(crsParam);
			Polygon polygon = getPolygon(geoJson, wkt, crs);
			polygon = convert(polygon, crs);
			if (getBoundingBox(polygon).calculateBoundingBoxAccuracy() >= MAX_AREA_SIZE) {
				throw PolygonValidationException.tooLarge();
			}
			return polygon;
		}

		private Coordinates getBoundingBox(Polygon polygon) throws DataValidationException {
			Coordinates boundingBox = new Coordinates(polygon.getLatMin(), polygon.getLatMax(), polygon.getLonMin(), polygon.getLonMax(), Type.EUREF);
			return boundingBox;
		}

		private Polygon convert(Polygon polygon, Type crs) throws PolygonValidationException {
			if (crs == Type.EUREF) return polygon;
			Feature converted;
			try {
				converted = CoordinateConverter.convertFeature(polygon, Type.EUREF, crs);
			} catch (DataValidationException e) {
				throw PolygonValidationException.parsingGeo(e);
			}
			if (!(converted instanceof Polygon)) {
				throw PolygonValidationException.conversionProblem();
			}
			return (Polygon) converted;
		}

		private Polygon getPolygon(String geoJson, String wkt, Type crs) throws PolygonValidationException, DataValidationException {
			Geo geo = null;
			try {
				geo = parseGeo(geoJson, wkt, crs);
			} catch (Exception e) {
				throw PolygonValidationException.parsingGeo(e);
			}
			if (geo.getFeatures().isEmpty()) {
				throw PolygonValidationException.noFeatures();
			}
			if (geo.getFeatures().size() != 1) {
				throw PolygonValidationException.tooManyFeatures(geo.getFeatures().size());
			}
			Feature f = geo.getFeatures().get(0);
			if (!(f instanceof Polygon)) {
				throw PolygonValidationException.notPolygon();
			}
			Polygon polygon = (Polygon) f;
			if (polygon.size() > MAX_POLYGON_COMPLEXITY) {
				polygon = polygon.simplify(crs, 100);
				if (polygon.size() > MAX_POLYGON_COMPLEXITY) {
					polygon = polygon.convexHullAndSimplify(crs);
					if (polygon.size() > MAX_POLYGON_COMPLEXITY) {
						throw PolygonValidationException.tooComplex(polygon.size());
					}
				}
			}
			polygon.validate();
			if (crs == Type.WGS84) {
				if (!FinlandAreaUtil.isInsideGeneralFinlandArea(geo.getBoundingBox())) {
					throw PolygonValidationException.notFromFinland();
				}
			}
			return polygon;
		}

		private Geo parseGeo(String geoJson, String wkt, Type crs) throws DataValidationException {
			if (given(geoJson)) {
				return parseGeoJson(geoJson, crs);
			}
			if (given(wkt)) {
				return parseWKT(wkt, crs);
			}
			throw new IllegalArgumentException("Must give either " + Const.GEO_JSON_REQUEST + " or " + Const.WKT + " parameter");
		}

		private Geo parseWKT(String wkt, Type crs) throws DataValidationException {
			try {
				return Geo.fromWKT(wkt, crs);
			} catch (DataValidationException e) {
				throw e;
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid wkt", e);
			}
		}

		private Geo parseGeoJson(String geoJson, Type crs) throws DataValidationException {
			JSONObject json = new JSONObject(geoJson);
			if (!json.hasKey(Geo.CRS)) {
				json.setString(Geo.CRS, crs.toString());
			}
			return Geo.fromGeoJSON(json);
		}

		private Type getCRS(String crs) {
			if (!given(crs)) return Type.EUREF;
			try {
				return Type.valueOf(crs);
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid " + Const.CRS + ": " + crs);
			}
		}

	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Long id = null;
		Coordinates.Type crs = null;
		ResponseFormat format = null;
		try {
			id = Long.valueOf(getId(req));
		} catch (Exception e) {
			return unloggedUserError400(new IllegalArgumentException("Invalid id"), res, req);
		}
		try {
			crs = getCRS(req);
			format = getFormat(req);
		} catch (Exception e) {
			return unloggedUserError400(e, res, req);
		}

		if (crs == null) crs = Type.EUREF;

		try {
			String wkt = getDao().getPolygonSearch(id);
			if (wkt == null) {
				return status404(res);
			}
			if (format == ResponseFormat.TEXT_PLAIN && crs == Type.EUREF) {
				return response(wkt, "text/plain");
			}
			Geo geo = convertGeo(wkt, crs);
			return response(geo, format);
		} catch (Exception e) {
			return loggedSystemError500(e, PolygonSubscriptionAPI.class, res, req);
		}
	}

	@Override
	protected ResponseFormat getDefaultFormat(String method) {
		if ("GET".equals(method)) return ResponseFormat.TEXT_PLAIN;
		return super.getDefaultFormat(method);
	}

	@Override
	protected ResponseFormat getFormat(HttpServletRequest req) throws IllegalArgumentException {
		ResponseFormat format = super.getFormat(req);
		if (format == ResponseFormat.GEO_JSON) return format;
		if (format == ResponseFormat.JSON) return ResponseFormat.GEO_JSON;
		if (format == ResponseFormat.TEXT_PLAIN) return format;
		throw new IllegalArgumentException("Supported formats are application/geo+json (GEO_JSON) and text/plain (WKT) ");
	}

	private ResponseData response(Geo geo, ResponseFormat format) {
		if (format == ResponseFormat.TEXT_PLAIN) return response(geo.getWKT(), "text/plain");
		if (format == ResponseFormat.GEO_JSON) return response(geo.getGeoJSON().toString(), "application/geo+json");
		throw new UnsupportedOperationException("Not implemented for " + format);
	}

	private Geo convertGeo(String wkt, Type crs) {
		Geo geo;
		try {
			geo = Geo.fromWKT(wkt, Type.EUREF);
		} catch (DataValidationException e) {
			throw new IllegalStateException("Impossible polygon problem", e);
		}

		if (crs == Type.EUREF) return geo;
		try {
			return CoordinateConverter.convertGeo(geo, crs);
		} catch (DataValidationException e) {
			throw new IllegalStateException("Impossible polygon problem", e);
		}
	}

	private Type getCRS(HttpServletRequest req) {
		String val = req.getParameter(Const.CRS);
		if (val == null) return null;
		try {
			return Coordinates.Type.valueOf(val);
		} catch (Exception e) {
			throw new IllegalArgumentException(Const.CRS + " must be one of " + Util.toString(Coordinates.Type.values()));
		}
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String wkt = null;
		String personToken = req.getParameter(Const.PERSON_TOKEN);
		try {
			validate(personToken);
			wkt = new PolygonSearchUtil(req).getAndValidateEurefWKT();
		} catch (DownloadLimitExceededException e) {
			log(personToken, e);
			return status(429, res);
		} catch (PolygonValidationException e) {
			return validationException(e, res, req);
		} catch (IllegalArgumentException e) {
			log(personToken, e);
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			return loggedSystemError500(req, res, personToken, e);
		}

		try {
			long id = getDao().getPolygonSearchId(wkt);
			return response(id, getFormat(req));
		} catch (Exception e) {
			return loggedSystemError500(req, res, personToken, e);
		}
	}

	private ResponseData loggedSystemError500(HttpServletRequest req, HttpServletResponse res, String personToken, Exception e) {
		return loggedSystemError500(e, Const.LAJI_ETL_QNAME, PolygonSubscriptionAPI.class, personId(personToken), res, req);
	}

	private ResponseData validationException(PolygonValidationException e, HttpServletResponse res, HttpServletRequest req) {
		return error(400, res, req, e.getLocalizedText());
	}

	private void log(String personToken, Exception e) {
		getDao().logError(Const.LAJI_ETL_QNAME, PolygonSubscriptionAPI.class, personId(personToken), e);
	}

	private String personId(String personToken) {
		try {
			PersonInfo person = getDao().getPerson(personToken);
			if (person == null) return personToken;
			try {
				return person.getEmail();
			} catch (Exception e) {
				// missing email
			}
			if (person.getId() != null) return person.getId().toString();
			return personToken;
		} catch (Exception e) {
			return personToken;
		}
	}

	private void validate(String personToken) throws DownloadLimitExceededException {
		if (!given(personToken)) throw new IllegalArgumentException("Must give a valid " + Const.PERSON_TOKEN);
		DAO dao = getDao();
		Qname personId = dao.getPerson(personToken).getId();
		if (dao.exceedsPolygonSearchLimit(personId)) {
			throw new DownloadLimitExceededException();
		}
	}

	private ResponseData response(long id, ResponseFormat format) {
		if (format == ResponseFormat.TEXT_PLAIN) {
			return response(""+id, "text/plain");
		}
		JSONObject jsonResponse = new JSONObject().setInteger("id", (int)id);
		if (format == ResponseFormat.XML) {
			String xml = "<response>" + XML.toString(jsonResponse.reveal()) + "</response>";
			return response(xml, "application/xml");
		}
		return jsonResponse(jsonResponse);
	}

}
