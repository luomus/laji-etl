package fi.laji.datawarehouse.query.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.SwaggerAPIDescriptionGenerator;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/swagger/*"})
public class SwaggerAPI extends ETLBaseServlet {

	private static final long serialVersionUID = 8427666272737075646L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			JSONObject response = SwaggerAPIDescriptionGenerator.generateSwaggerDescription(getConfig());
			if (!getConfig().developmentMode()) {
				res.setHeader("Content-Disposition","attachment; filename=\"swagger.json\"");
			}
			return jsonResponse(response);
		} catch (Exception e) {
			return loggedSystemError500(e, SwaggerAPI.class, res, req);
		}
	}





}
