package fi.laji.datawarehouse.query.service.gathering;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/gathering/aggregate/*"})
public class PrivateGatheringAggregateQueryAPIServlet extends GatheringAggregateQueryAPIServlet {

	private static final long serialVersionUID = -2133672175469490292L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
