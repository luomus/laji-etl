package fi.laji.datawarehouse.query.service.unit;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/unit/list/*", "/private-query/list/*"})
public class PrivateUnitListQueryAPI extends UnitListQueryAPI {

	private static final long serialVersionUID = -2075403243728562653L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}
	
}
