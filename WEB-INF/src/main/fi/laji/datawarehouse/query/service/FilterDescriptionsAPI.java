package fi.laji.datawarehouse.query.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.laji.datawarehouse.query.model.FilterDefinition;
import fi.laji.datawarehouse.query.model.FilterDefinition.Type;
import fi.laji.datawarehouse.query.model.FilterInfo;
import fi.laji.datawarehouse.query.model.Filters;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/filters/*"})
public class FilterDescriptionsAPI extends ETLBaseServlet {

	private static final long serialVersionUID = 6044407188178367083L;

	public static class FilterDescriptions {

		private final DAO dao;
		private static final EnumToProperty enumToProperty = EnumToProperty.getInstance();

		public FilterDescriptions(DAO dao) {
			this.dao = dao;
		}

		private static JSONObject allDescriptions;

		public JSONObject getAllDescriptions() {
			if (allDescriptions != null) return allDescriptions;
			JSONObject descriptions = new JSONObject();
			for (FilterInfo filter : Filters.filterParameters(Base.UNIT)) {
				descriptions.setObject(filter.getName(), getDescription(filter));
			}
			allDescriptions = descriptions;
			return descriptions;
		}

		private static final Map<String, JSONObject> storage = new HashMap<>();

		public JSONObject getDescription(FilterInfo filter) {
			if (storage.containsKey(filter.getName())) return storage.get(filter.getName());

			JSONObject description = new JSONObject();
			FilterDefinition filterInfo = filter.getInfo();

			description.getObject("label").setString("fi", filterInfo.labelFi());
			description.getObject("label").setString("en", filterInfo.labelEn());
			description.getObject("label").setString("sv", filterInfo.labelSv());

			Type filterType = filterInfo.type();
			description.setString("type", filterType.name());

			if (filterType == Type.RESOURCE) {
				description.setString("resource", filterInfo.resourceName());
			} else if (filterType == Type.ENUMERATION) {
				addEnumerationValues(filter, description);
			}

			storage.put(filter.getName(), description);
			return description;
		}

		private void addEnumerationValues(FilterInfo filter, JSONObject description) {
			for (String enumValue : filter.getEnumerationValues()) {
				JSONObject enumDesc = new JSONObject();
				enumDesc.setString("name", enumValue);
				Qname enumProperty = enumToProperty.get(enumValue);
				try {
					LocalizedText label = dao.getLabels(enumProperty);
					enumDesc.getObject("label").setString("fi", label.forLocale("fi"));
					enumDesc.getObject("label").setString("en", label.forLocale("en"));
					enumDesc.getObject("label").setString("sv", label.forLocale("sv"));
				} catch (Exception e) {
					dao.logError(Const.LAJI_ETL_QNAME, FilterDescriptions.class, enumValue, e);
				}
				description.getArray("enumerations").appendObject(enumDesc);
			}
		}

	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		FilterDescriptions filterDescriptions = new FilterDescriptions(getDao());
		try {
			String filterName = getId(req);
			if (given(filterName)) {
				FilterInfo filter = Filters.filterInfo(filterName);
				return jsonResponse(filterDescriptions.getDescription(filter));
			}
			return jsonResponse(filterDescriptions.getAllDescriptions());
		} catch (Exception e) {
			return loggedSystemError500(e, FilterDescriptionsAPI.class, res, req);
		}
	}

}
