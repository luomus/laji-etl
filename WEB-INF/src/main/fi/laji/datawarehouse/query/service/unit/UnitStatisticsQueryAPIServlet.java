package fi.laji.datawarehouse.query.service.unit;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;

@WebServlet(urlPatterns = {"/query/unit/statistics/*", "/query/statistics/*"})
public class UnitStatisticsQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = -8419631674989840580L;

	@Override
	protected Base getBase() {
		return Base.UNIT;
	}

	@Override
	protected boolean isStatisticsQuery() {
		return true;
	}

}
