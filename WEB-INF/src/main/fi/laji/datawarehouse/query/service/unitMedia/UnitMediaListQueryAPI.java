package fi.laji.datawarehouse.query.service.unitMedia;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.OrderBy.Order;
import fi.laji.datawarehouse.query.model.queries.OrderBy.OrderCriteria;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.service.unit.UnitListQueryAPI;

@WebServlet(urlPatterns = {"/query/unitMedia/list/*"})
public class UnitMediaListQueryAPI extends UnitListQueryAPI {

	private static final long serialVersionUID = -5161834577492489562L;
	public static final OrderBy DEFAULT_ORDER_BY = initDefaultOrderBy();
	public static final Selected DEFAULT_SELECT = initDefaultSelect();

	private static OrderBy initDefaultOrderBy() {
		try {
			OrderBy unitOrderBy = UnitListQueryAPI.DEFAULT_ORDER_BY;
			OrderBy unitMediaOrderBy = new OrderBy(Base.UNIT_MEDIA);
			for (OrderCriteria unitOrderCriteria :  unitOrderBy.getOrderCriterias()) {
				unitMediaOrderBy.add(new OrderCriteria(unitOrderCriteria.getField(), unitOrderCriteria.getOrder()));
			}
			unitMediaOrderBy.add(new OrderCriteria("unit.media.mediaType", Order.ASC));
			unitMediaOrderBy.add(new OrderCriteria("unit.media.fullURL", Order.ASC));
			return unitMediaOrderBy;
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private static Selected initDefaultSelect() {
		try {
			return new Selected(Base.UNIT_MEDIA,
					"media", "document.documentId", "unit.unitId",
					"unit.taxonVerbatim", "unit.linkings.taxon.vernacularName", "unit.linkings.taxon.scientificName", "unit.reportedInformalTaxonGroup"
					);
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	@Override
	protected OrderBy getDefaultOrderBy() {
		return DEFAULT_ORDER_BY;
	}

	@Override
	protected Selected getDefaultSelect() {
		return DEFAULT_SELECT;
	}

	@Override
	protected Base getBase() {
		return Base.UNIT_MEDIA;
	}

}
