package fi.laji.datawarehouse.query.service.annotation;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.service.unit.UnitListQueryAPI;

@WebServlet(urlPatterns = {"/query/annotation/list/*"})
public class AnnotationListQueryAPI extends UnitListQueryAPI {

	private static final long serialVersionUID = 6260731655747407871L;
	public static final OrderBy DEFAULT_ORDER_BY = initDefaultOrderBy();
	public static final Selected DEFAULT_SELECT = initDefaultSelect();

	private static OrderBy initDefaultOrderBy() {
		try {
			return new OrderBy(Base.ANNOTATION)
					.addDescending("unit.annotations.created")
					.addAscending("unit.annotations.id");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private static Selected initDefaultSelect() {
		try {
			return new Selected(Base.ANNOTATION,
					"annotation",
					"document.documentId", "unit.unitId", "document.collectionId",
					"gathering.team", "gathering.displayDateTime",
					"unit.linkings.taxon.id", "unit.linkings.taxon.scientificName", "unit.linkings.taxon.vernacularName",
					"unit.linkings.originalTaxon.id", "unit.linkings.originalTaxon.scientificName", "unit.linkings.originalTaxon.vernacularName",
					"unit.taxonVerbatim"
					);
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	@Override
	protected OrderBy getDefaultOrderBy() {
		return DEFAULT_ORDER_BY;
	}

	@Override
	protected Selected getDefaultSelect() {
		return DEFAULT_SELECT;
	}

	@Override
	protected Base getBase() {
		return Base.ANNOTATION;
	}

}
