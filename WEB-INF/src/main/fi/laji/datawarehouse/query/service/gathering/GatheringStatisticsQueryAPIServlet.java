package fi.laji.datawarehouse.query.service.gathering;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitAggregateQueryAPI;

@WebServlet(urlPatterns = {"/query/gathering/statistics/*"})
public class GatheringStatisticsQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = 6756576742431828716L;

	@Override
	protected Base getBase() {
		return Base.GATHERING;
	}

	@Override
	protected boolean isStatisticsQuery() {
		return true;
	}

}
