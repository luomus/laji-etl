package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitCountQueryAPI;

@WebServlet(urlPatterns = {"/query/sample/count/*"})
public class SampleCountQueryAPI extends UnitCountQueryAPI {

	private static final long serialVersionUID = 3715431266421636797L;

	protected Base getBase() {
		return Base.SAMPLE;
	}

}
