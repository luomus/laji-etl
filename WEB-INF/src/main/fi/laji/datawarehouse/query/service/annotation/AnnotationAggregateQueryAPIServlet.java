package fi.laji.datawarehouse.query.service.annotation;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.service.unit.UnitAggregateQueryAPI;

@WebServlet(urlPatterns = {"/query/annotation/aggregate/*"})
public class AnnotationAggregateQueryAPIServlet extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = 2001655000110808067L;

	@Override
	protected Base getBase() {
		return Base.ANNOTATION;
	}

}
