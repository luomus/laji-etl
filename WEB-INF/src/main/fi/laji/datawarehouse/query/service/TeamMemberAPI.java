package fi.laji.datawarehouse.query.service;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.TeamMemberData;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/teamMember/*"})
public class TeamMemberAPI extends ETLBaseServlet {

	private static final long serialVersionUID = 4479895360871836109L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			String id = getId(req);
			String searchWord = req.getParameter("q");
			if (!given(id) && !given(searchWord)) {
				return unloggedUserError400(new IllegalArgumentException("TeamMember id or 'q' search query parameter must be given"), res, req);
			}
			if (given(searchWord)) {
				return searchResponse(searchWord);
			}
			return getByIdResponse(res, req, id);
		} catch (Exception e) {
			return loggedSystemError500(e, TeamMemberAPI.class, res, req);
		}
	}

	private ResponseData getByIdResponse(HttpServletResponse res, HttpServletRequest req, String id) {
		Long teamMemberId;
		try {
			teamMemberId = Long.valueOf(id);
		} catch (Exception e) {
			return unloggedUserError400(e, res, req);
		}
		String name = getDao().getVerticaDimensionsDAO().getIdForKey(teamMemberId, "gathering.team.memberName");
		JSONObject response = new JSONObject().setString("id", id).setString("name", name);
		return jsonResponse(response);
	}

	private ResponseData searchResponse(String searchWord) {
		List<TeamMemberData> results = getDao().getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getTeamMembers(searchWord);
		JSONObject responseJson = new JSONObject();
		JSONArray resultsJson = responseJson.getArray("results");
		for (TeamMemberData data : results) {
			resultsJson.appendObject(
					new JSONObject()
					.setString("id", String.valueOf(data.getId()))
					.setString("name", data.getName())
					.setInteger("count", data.getCount()));
		}
		return jsonResponse(responseJson);
	}

}
