package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/sample/list/*"})
public class PrivateSampleListQueryAPI extends SampleListQueryAPI {

	private static final long serialVersionUID = 6708053219943051688L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
