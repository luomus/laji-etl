package fi.laji.datawarehouse.query.service.unit;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/unit/aggregate/*", "/private-query/aggregate/*"})
public class PrivateUnitAggregateQueryAPI extends UnitAggregateQueryAPI {

	private static final long serialVersionUID = 4091009052965223472L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}
	
}
