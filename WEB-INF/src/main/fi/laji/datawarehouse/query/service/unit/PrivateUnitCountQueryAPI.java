package fi.laji.datawarehouse.query.service.unit;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/unit/count/*", "/private-query/count/*"})
public class PrivateUnitCountQueryAPI extends UnitCountQueryAPI {

	private static final long serialVersionUID = 9177101130505166888L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}
	
}
