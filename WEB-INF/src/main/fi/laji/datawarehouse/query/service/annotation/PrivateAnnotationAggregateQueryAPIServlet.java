package fi.laji.datawarehouse.query.service.annotation;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/annotation/aggregate/*"})
public class PrivateAnnotationAggregateQueryAPIServlet extends AnnotationAggregateQueryAPIServlet {

	private static final long serialVersionUID = 4925917289240544608L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
