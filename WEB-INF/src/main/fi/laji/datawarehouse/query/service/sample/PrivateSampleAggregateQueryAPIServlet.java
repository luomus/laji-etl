package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/sample/aggregate/*"})
public class PrivateSampleAggregateQueryAPIServlet extends SampleAggregateQueryAPIServlet {

	private static final long serialVersionUID = 1712238471642183261L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
