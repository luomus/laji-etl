package fi.laji.datawarehouse.query.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.query.model.EnumerationLabels;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/enumeration-labels/*"})
public class EnumerationLabelAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -3623511079493455L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		EnumerationLabels enumerationLabels = new EnumerationLabels(getDao());
		try {
			String enumerationName = getId(req);
			if (given(enumerationName)) {
				return jsonResponse(enumerationLabels.getLabels(enumerationName));
			}
			return jsonResponse(enumerationLabels.getAllLabels());
		} catch (Exception e) {
			return loggedSystemError500(e, EnumerationLabelAPI.class, res, req);
		}
	}

}
