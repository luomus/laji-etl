package fi.laji.datawarehouse.query.service.unit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;

import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.models.exceptions.UnsupportedFormatException;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.CetafRdfXmlUtil;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.laji.datawarehouse.query.service.BaseQueryAPIServlet;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/query/unit/list/*", "/query/list/*"})
public class UnitListQueryAPI extends BaseQueryAPIServlet {

	private static final long serialVersionUID = 5754900869658095986L;
	public static final OrderBy DEFAULT_ORDER_BY = initDefaultOrderBy();
	public static final Selected DEFAULT_SELECT = initDefaultSelect();

	private static OrderBy initDefaultOrderBy() {
		try {
			return new OrderBy(Base.UNIT).addDescending("gathering.eventDate.begin").addDescending("document.loadDate").addAscending("unit.taxonVerbatim");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private static Selected initDefaultSelect() {
		try {
			return new Selected(Base.UNIT,
					"document.documentId", "gathering.gatheringId", "unit.unitId",
					"document.sourceId", "document.collectionId", "document.licenseId",
					"document.secureLevel", "document.secureReasons",
					"gathering.team",
					"gathering.displayDateTime",
					"gathering.interpretations.municipalityDisplayname", "gathering.locality",
					"gathering.conversions.wgs84CenterPoint.lat", "gathering.conversions.wgs84CenterPoint.lon",
					"gathering.interpretations.coordinateAccuracy", "gathering.interpretations.sourceOfCoordinates",
					"unit.linkings.taxon.id", "unit.linkings.taxon.scientificName", "unit.linkings.taxon.vernacularName",
					"unit.taxonVerbatim", "unit.abundanceString", "unit.recordBasis", "unit.notes"
					);
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ListQuery query = null;
		ResponseFormat format = null;
		try {
			format = getFormat(req);
			query = getQuery(req, getBase());
			if (format == ResponseFormat.GEO_JSON) {
				query.validateGeoJSON();
			}
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		Access access = null;
		try {
			access = getAccess(query.getApiSourceId());
		} catch (TooManyRequestsException e) {
			return tooManyRequests429(res, req, e);
		}
		
		try {
			ListResponse response = executeQuery(query);
			return writeResponse(response, query, format);
		} catch (UnsupportedFormatException | UnsupportedOperationException | IllegalArgumentException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		} finally {
			access.release();
		}
	}

	protected Base getBase() {
		return Base.UNIT;
	}

	private ListResponse executeQuery(ListQuery query) {
		VerticaQueryDAO dao = getVerticaDao(query).getQueryDAO();
		getDao().logQuery(query);
		return dao.getList(query);
	}

	private ResponseData writeResponse(ListResponse response, ListQuery query, ResponseFormat format) throws IOException, Exception {
		if (format == ResponseFormat.JSON) {
			return writeJsonResponse(response, query);
		} else if (format == ResponseFormat.GEO_JSON) {
			return writeGeoJsonResponse(response, query);
		} else if (format == ResponseFormat.XML) {
			return writeXmlResponse(response, query);
		} else if (format == ResponseFormat.RDF_XML) {
			return writeRdfXmlResponse(response.getResults());
		}
		throw new UnsupportedOperationException("Not yet implemented for format " + format);
	}

	private ResponseData writeXmlResponse(ListResponse response, ListQuery query) throws Exception {
		JSONObject json = buildJsonResponse(response, query);
		String xml = "<response>" + XML.toString(json.reveal()) + "</response>";
		return response(xml, "application/xml");
	}

	private ResponseData writeRdfXmlResponse(List<JoinedRow> rows) throws Exception {
		String xml = CetafRdfXmlUtil.generate(rows, getDao().getCollections(), getDao().getAreas());
		return response(xml, "application/rdf+xml");
	}

	private ResponseData writeJsonResponse(ListResponse response, ListQuery query) {
		JSONObject json = buildJsonResponse(response, query);
		return jsonResponse(json);
	}

	private JSONObject buildJsonResponse(ListResponse response, ListQuery query) {
		return new ResponseUtil().buildListResponse(response, query);
	}

	private ResponseData writeGeoJsonResponse(ListResponse response, ListQuery query) {
		JSONObject geoJson = new ResponseUtil().buildGeoJsonResponse(response, query);
		return jsonResponse(geoJson);
	}

	protected ListQuery getQuery(HttpServletRequest req, Base base) throws Exception {
		BaseQuery baseQuery = getBaseQuery(req, base, false, true);
		Selected selected = getSelectedFields(req);
		OrderBy orderBy = getOrderBy(req, base);

		int pageSize = getPageSize(req);
		int currentPage = getCurrentPage(req);
		ListQuery query = new ListQuery(baseQuery, currentPage, pageSize);

		if (!orderBy.hasValues()) {
			orderBy = getDefaultOrderBy();
		}
		query.setSelected(selected);
		query.setOrderBy(orderBy);

		return query;
	}

	// TODO remove accepting legacy fields
	public static Set<String> LEGACY_FIELDS = initLegacy();

	private static Set<String> initLegacy() {
		Set<String> map = new HashSet<>();
		map.add("document.quality.reliabilityOfCollection");
		map.add("unit.quality.reliable");
		map.add("unit.quality.taxon.message");
		map.add("unit.quality.taxon.reliability");
		map.add("unit.quality.taxon.source");
		map.add("document.annotations.annotationClass");
		map.add("document.annotations.invasiveControlEffectiveness");
		map.add("document.annotations.opinion");
		map.add("document.annotations.rootID");
		map.add("document.annotations.targetID");
		map.add("document.annotations.type");
		map.add("unit.interpretations.unidentifiable");
		List<String> orderby = new ArrayList<>();
		for (String field : map) {
			orderby.add(field + " desc");
			orderby.add(field + " asc");
			orderby.add(field + " DESC");
			orderby.add(field + " ASC");
		}
		map.addAll(orderby);
		return map;
	}

	private Selected getSelectedFields(HttpServletRequest req) throws NoSuchFieldException {
		Selected selected = new Selected(getBase());
		String[] selectedFieldParams = req.getParameterValues(Const.SELECTED_FIELDS);
		if (!given(selectedFieldParams)) return getDefaultSelect();
		for (String param : selectedFieldParams) {
			if (!given(param)) continue;
			for (String paramPart : param.split(Pattern.quote(","))) {
				if (!given(paramPart)) continue;
				if (LEGACY_FIELDS.contains(paramPart)) continue;
				selected.add(paramPart);
			}
		}
		if (!selected.hasValues()) {
			return getDefaultSelect();
		}
		return selected;
	}

	protected OrderBy getDefaultOrderBy() {
		return DEFAULT_ORDER_BY;
	}

	protected Selected getDefaultSelect() {
		return DEFAULT_SELECT;
	}

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC;
	}

}

