package fi.laji.datawarehouse.query.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.PageableBaseQuery;
import fi.laji.datawarehouse.query.service.unit.UnitListQueryAPI;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public abstract class BaseQueryAPIServlet extends ETLBaseServlet {

	private static final long serialVersionUID = -4733011877269366748L;

	public static final int DEFAULT_PAGE_SIZE = 100;

	protected BaseQuery getBaseQuery(HttpServletRequest req, boolean isStatisticsQuery, boolean setDefaultFilters) throws Exception {
		return getBaseQuery(req, Base.UNIT, isStatisticsQuery, setDefaultFilters);
	}

	protected BaseQuery getBaseQuery(HttpServletRequest req, Base base, boolean isStatisticsQuery, boolean setDefaultFilters) throws Exception {
		String apiSourceId = getApiSourceIdValidateAccess(req);
		boolean canUseEditorOrObserverIdIsNot = sourceIdCanUseEditorOrObserverIdIsNot(req);

		Map<String, String[]> filterParameters = getFilterParameters(req);

		boolean useCache = shouldCache(req, filterParameters);
		Qname ownEditorId = getEditorIdByToken(req, isStatisticsQuery, apiSourceId);
		Qname ownObserverId = getPersonIdByToken(req, Const.OBSERVER_PERSON_TOKEN, base, isStatisticsQuery, apiSourceId);
		Qname ownEditorOrObserverId = getPersonIdByToken(req, Const.EDITOR_OR_OBSERVER_PERSON_TOKEN, base, isStatisticsQuery, apiSourceId);
		Qname editorOrObserverIdIsNotOwn = getPersonIdByToken(req, Const.EDITOR_OR_OBSERVER_IS_NOT_PERSON_TOKEN, base, isStatisticsQuery, apiSourceId);
		DownloadRequest permissionRequest = getPermissionRequest(req);
		Concealment concealment = getWarehouseType();

		boolean ownSearch = ownEditorId != null || ownObserverId != null || ownEditorOrObserverId != null; // editorOrObserverIdIsNot excluded by design; must NOT cause private query
		if (ownSearch) {
			concealment = Concealment.PRIVATE;
			setDefaultFilters = false;
		}
		if (permissionRequest != null) {
			concealment = Concealment.PRIVATE;
			setDefaultFilters = false;
			if (useCache && filterParameters.isEmpty()) {
				useCache = false;
			}
		}
		if (isStatisticsQuery) {
			concealment = Concealment.PRIVATE;
			useCache = true;
		}

		Coordinates.Type geoJsonCRS = getGeoJsonCRS(req);
		FeatureType geoJsonFeatureType = getGeoJsonFeatureType(req);
		if (geoJsonFeatureType != null && geoJsonCRS == null) geoJsonCRS = Coordinates.Type.WGS84;

		BaseQueryBuilder baseQueryBuilder = new BaseQueryBuilder(concealment)
				.setBase(base)
				.setOnlyStatisticsFilters(isStatisticsQuery)
				.setApiSourceId(apiSourceId)
				.setCaller(this.getClass())
				.setDefaultFilters(setDefaultFilters)
				.setUserAgent(req.getHeader("User-Agent"))
				.setUseCache(useCache)
				.setCrs(geoJsonCRS)
				.setFeatureType(geoJsonFeatureType)
				.setApiSourceIdCanUseEditorOrObserverIdIsNot(canUseEditorOrObserverIdIsNot);

		setUserInfo(baseQueryBuilder, req, apiSourceId, Utils.set(ownEditorId, ownObserverId, ownEditorOrObserverId, editorOrObserverIdIsNotOwn));

		if (getWarehouseType() == Concealment.PRIVATE) {
			if (!given(baseQueryBuilder.getPersonEmail())) {
				throw new IllegalAccessException("Private queries must be provided a " + Const.PERSON_TOKEN + " or " + Const.PERSON_ID + " or " + Const.PERSON_EMAIL);
			}
		}

		BaseQuery baseQuery = baseQueryBuilder.build();

		Filters filters = baseQuery.getFilters().addParameters(filterParameters);
		filters.validate();

		if (permissionRequest != null) {
			if (permissionRequest.getFilters() == null || permissionRequest.getFilters().getSetFiltersExcludingDefaults().isEmpty()) {
				throw new IllegalStateException("Somehow " + Const.PERMISSION_TOKEN + " does not define any filters");
			}
			baseQuery.setPermissionFilters(permissionRequest.getFilters());
			baseQuery.setPermissionId(permissionRequest.getId());
		}

		if (isStatisticsQuery) {
			if (collectionIdNotGiven(filters)) {
				throw new IllegalStateException(Const.COLLECTION_ID + " is required when using statistics API");
			}
			for (Qname collectionId : filters.getCollectionId()) {
				validateAllowedForStatistics(collectionId);
			}
		}

		if (ownEditorId != null) filters.setEditorId(ownEditorId);
		if (ownObserverId != null) filters.setObserverId(ownObserverId);
		if (ownEditorOrObserverId != null) filters.setEditorOrObserverId(ownEditorOrObserverId);
		if (editorOrObserverIdIsNotOwn != null) filters.setEditorOrObserverIdIsNot(editorOrObserverIdIsNotOwn);

		if (isPortal(apiSourceId) && filters.hasVirvaTaxonFilters()) {
			baseQuery.setUseCache(true);
		}
		return baseQuery;
	}

	private boolean isPortal(String apiSourceId) {
		return portalSources().contains(apiSourceId);
	}

	private Set<String> portalSources() {
		return getConfig().productionMode() ? Const.PROD_PORTALS : Const.STAGING_PORTALS;
	}

	private FeatureType getGeoJsonFeatureType(HttpServletRequest req) {
		String val = req.getParameter(Const.FEATURE_TYPE);
		if (val == null) return null;
		try {
			return FeatureType.valueOf(val);
		} catch (Exception e) {
			throw new IllegalArgumentException(Const.FEATURE_TYPE + " must be one of " + Util.toString(FeatureType.values()));
		}
	}

	private Coordinates.Type getGeoJsonCRS(HttpServletRequest req) {
		String val = req.getParameter(Const.CRS);
		if (val == null) return null;
		try {
			return Coordinates.Type.valueOf(val);
		} catch (Exception e) {
			throw new IllegalArgumentException(Const.CRS + " must be one of " + Util.toString(Coordinates.Type.values()));
		}
	}

	private void setUserInfo(BaseQueryBuilder baseQueryBuilder, HttpServletRequest req, String apiSourceId, Set<Qname> personIdsByPersonToken) throws IllegalAccessException {
		String personToken = req.getParameter(Const.PERSON_TOKEN);
		if (given(personToken)) {
			setUserInfo(baseQueryBuilder, apiSourceId, personToken);
			return;
		}
		String personId = req.getParameter(Const.PERSON_ID);
		if (given(personId)) {
			setUserInfo(baseQueryBuilder, new Qname(personId));
			return;
		}
		String personEmail = req.getParameter(Const.PERSON_EMAIL);
		if (given(personEmail)) {
			baseQueryBuilder.setPersonEmail(personEmail);
			return;
		}
		personIdsByPersonToken.remove(null);
		if (personIdsByPersonToken.size() == 0) return;
		if (personIdsByPersonToken.size() > 1) throw new IllegalAccessException("Multiple different token person values provided");
		setUserInfo(baseQueryBuilder, personIdsByPersonToken.iterator().next());
	}

	private void setUserInfo(BaseQueryBuilder baseQueryBuilder, String apiSourceId, String personToken) throws IllegalAccessException {
		try {
			Qname personId = getPersonId(personToken, apiSourceId);
			setUserInfo(baseQueryBuilder, personId);
		} catch (Exception e) {
			throw new IllegalAccessException("Must give a valid " + Const.PERSON_TOKEN + ": " + e.getMessage());
		}
	}

	private void setUserInfo(BaseQueryBuilder baseQueryBuilder, Qname personId) {
		PersonInfo personInfo = getDao().getPerson(personId);
		baseQueryBuilder.setPersonId(personInfo.getId()).setPersonEmail(personInfo.getEmail());
	}

	private void validateAllowedForStatistics(Qname collectionId) {
		if (!getDao().getStatisticsAllowedCollections().contains(collectionId)) throw new IllegalStateException(Const.COLLECTION_ID + " " + collectionId + " is not allowed with statistics API");
	}

	private boolean collectionIdNotGiven(Filters filters) {
		return filters.getCollectionId() == null || filters.getCollectionId().isEmpty();
	}

	private DownloadRequest getPermissionRequest(HttpServletRequest req) throws IllegalAccessException {
		String token = req.getParameter(Const.PERMISSION_TOKEN);
		if (!given(token)) return null;

		if (getWarehouseType() == Concealment.PRIVATE) throw new IllegalArgumentException("As a failsafe, " + Const.PERMISSION_TOKEN + " is usable only for /query/ endpont. Change to your query to point to /query/ instead of /private-query/");

		DownloadRequest request;
		try {
			request = getDao().getRequestForApiKey(token);
			if (request == null) throw new IllegalStateException();
		} catch (Exception e) {
			throw new IllegalAccessException("Not a valid " + Const.PERMISSION_TOKEN + "!");
		}
		if (request.getDownloadType() == DownloadType.AUTHORITIES_VIRVA_GEOAPI_KEY) {
			throw new IllegalAccessException("The provided" + Const.PERMISSION_TOKEN + " is subscribed for GeoAPI data products and can not be used to query occurrences");
		}
		if (request.getApiKeyExpires().after(new Date())) {
			return request;
		}
		throw new IllegalAccessException(Const.PERMISSION_TOKEN + " has expired!");
	}

	private Qname getPersonIdByToken(HttpServletRequest req, String param, Base base, boolean onlyStatisticsFilters, String apiSourceId) throws IllegalAccessException {
		String token = req.getParameter(param);
		if (!given(token)) return null;

		if (!base.includes(Base.GATHERING)) throw new Filters.UnsupportedFilterException(param, base, Base.GATHERING);
		if (onlyStatisticsFilters) throw new Filters.UnsupportedFilterException(param, onlyStatisticsFilters);

		return getPersonId(token, apiSourceId);
	}

	private Qname getEditorIdByToken(HttpServletRequest req, boolean onlyStatisticsFilters, String apiSourceId) throws IllegalAccessException {
		String token = req.getParameter(Const.EDITOR_PERSON_TOKEN);
		if (!given(token)) return null;

		if (onlyStatisticsFilters) throw new Filters.UnsupportedFilterException(Const.EDITOR_PERSON_TOKEN, onlyStatisticsFilters);

		return getPersonId(token, apiSourceId);
	}

	private Qname getPersonId(String personToken, String apiSourceId) throws IllegalAccessException {
		if (personToken == null) return null;
		try {
			Qname personTargetSystem = getDao().getPersonTargetSystem(personToken);
			if (!personTargetSystem.toString().equals(apiSourceId)) throw new IllegalAccessException("Person token is not provided for the same system that is calling this data warehouse api.");
			return getDao().getPerson(personToken).getId();
		} catch (IllegalAccessException e) {
			throw e;
		} catch (Exception e) {
			throw new IllegalAccessException("No person found with the token.");
		}
	}

	private boolean shouldCache(HttpServletRequest req, Map<String, String[]> filterParameters) {
		String cacheParam = req.getParameter(Const.CACHE);
		if ("true".equals(cacheParam)) return true;
		return filterParameters.isEmpty();
	}

	private Map<String, String[]> getFilterParameters(HttpServletRequest req) {
		Map<String, String[]> params = new HashMap<>(req.getParameterMap());
		for (String param : Const.ACCEPTED_PARAMETERS) {
			params.remove(param);
		}
		return params;
	}

	private String getApiSourceIdValidateAccess(HttpServletRequest req) throws Exception {
		ApiUser apiUser = getApiUser(req);

		if (getWarehouseType() == Concealment.PUBLIC) {
			return apiUser.getSystemIdOrEmail();
		}

		if (apiUser.getSystemId() == null) {
			throw new IllegalAccessException("" +
					"Please ask for your system to be registered in FinBIF and your " + Const.API_KEY +
					" to be assosiated with that system to be able to make queries to PRIVATE FinBIF warehouse!");
		}
		Qname sourceId = apiUser.getSystemId();
		Source source = getDao().getSources().get(apiUser.getSystemId().toURI());
		if (source == null || !source.isAllowedToQueryPrivateWarehouse()) {
			throw new IllegalAccessException("" +
					"The system ("+sourceId.toURI()+") that is assosiated to this " + Const.API_KEY +
					" has to be given permissions to make queries to PRIVATE FinBIF warehouse!");
		}
		return sourceId.toString();
	}

	private boolean sourceIdCanUseEditorOrObserverIdIsNot(HttpServletRequest req) throws Exception {
		ApiUser apiUser = getApiUser(req);

		if (apiUser.getSystemId() == null) {
			return false;
		}

		Source source = getDao().getSources().get(apiUser.getSystemId().toURI());

		if (source == null || !source.isAllowedToUseDatawarehouseEditorOrObserverIdIsNot()) {
			return false;
		}

		return true;
	}

	protected abstract Concealment getWarehouseType();

	protected VerticaDAO getVerticaDao(BaseQuery query) {
		if (query.isPublic()) {
			return getDao().getPublicVerticaDAO();
		}
		return getDao().getPrivateVerticaDAO();
	}

	protected int getCurrentPage(HttpServletRequest req) {
		String currentPage = req.getParameter(Const.CURRENT_PAGE);
		if (currentPage == null) return 1;
		try {
			return Integer.valueOf(currentPage);
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid current page: " + currentPage);
		}
	}

	protected int getPageSize(HttpServletRequest req) {
		String pageSizeParam = req.getParameter(Const.PAGE_SIZE);
		if (pageSizeParam == null) return DEFAULT_PAGE_SIZE;
		try {
			int pageSize = Integer.valueOf(pageSizeParam);
			if (pageSize > PageableBaseQuery.MAX_PAGE_SIZE) throw new IllegalArgumentException("Max page size is " + PageableBaseQuery.MAX_PAGE_SIZE);
			return pageSize;
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid page size: " + pageSizeParam);
		}
	}

	protected OrderBy getOrderBy(HttpServletRequest req, Base base) throws NoSuchFieldException {
		String[] orderByParams = req.getParameterValues(Const.ORDER_BY);
		OrderBy orderBy = new OrderBy(base);
		if (given(orderByParams)) {
			for (String param : orderByParams) {
				if (!given(param)) continue;
				for (String paramPart : param.split(Pattern.quote(","))) {
					if (!given(paramPart)) continue;
					if (UnitListQueryAPI.LEGACY_FIELDS.contains(paramPart)) continue; // TODO remove accepting legacy fields
					orderBy.add(OrderBy.toOrderCriteria(paramPart));
				}
			}
		}
		return orderBy;
	}

}
