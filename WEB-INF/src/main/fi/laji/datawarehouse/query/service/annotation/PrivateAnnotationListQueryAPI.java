package fi.laji.datawarehouse.query.service.annotation;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/annotation/list/*"})
public class PrivateAnnotationListQueryAPI extends AnnotationListQueryAPI {

	private static final long serialVersionUID = 6500369286379635848L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
