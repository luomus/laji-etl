package fi.laji.datawarehouse.query.service.unit;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.models.exceptions.UnsupportedFormatException;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.service.BaseQueryAPIServlet;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/query/unit/aggregate/*", "/query/aggregate/*"})
public class UnitAggregateQueryAPI extends BaseQueryAPIServlet {

	private static final long serialVersionUID = 7070853513558683713L;

	private static final Set<String> ONLY_COUNT = Collections.unmodifiableSet(Utils.set(Const.COUNT));

	private static final Set<String> APPROVED_DATA_REQUEST_APIKEY_DISALLOWED_FIELDS = Collections.unmodifiableSet(Utils.set(
			"document.editorUserIds",
			"document.linkings.editors",
			"document.media.author",
			"document.media.copyrightOwner",
			"gathering.linkings.observers",
			"gathering.media.author",
			"gathering.media.copyrightOwner",
			"gathering.observerUserIds",
			"gathering.team",
			"gathering.team.memberId",
			"gathering.team.memberName",
			"unit.media.author",
			"unit.media.copyrightOwner",
			"document.facts.value",
			"gathering.facts.value"));

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		AggregatedQuery query = null;
		ResponseFormat format;
		try {
			format = getFormat(req);
			query = getQuery(req, getBase(), isStatisticsQuery(), format);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		Access access = null;
		try {
			access = getAccess(query.getApiSourceId());
		} catch (TooManyRequestsException e) {
			return tooManyRequests429(res, req, e);
		}

		try {
			AggregateResponse response = executeQuery(query);
			return writeResponse(response, query, format, res);
		} catch (UnsupportedFormatException | UnsupportedOperationException | IllegalArgumentException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		} finally {
			access.release();
		}
	}

	protected Base getBase() {
		return Base.UNIT;
	}

	protected boolean isStatisticsQuery() {
		return false;
	}

	private AggregateResponse executeQuery(AggregatedQuery query) {
		DAO dao = getDao();
		VerticaQueryDAO queryDao = getVerticaDao(query).getQueryDAO();
		dao.logQuery(query);
		return queryDao.getAggregate(query);
	}

	private ResponseData writeResponse(AggregateResponse response, AggregatedQuery query, ResponseFormat format, HttpServletResponse res) throws Exception {
		VerticaDimensionsDAO dao = getDao().getVerticaDimensionsDAO();
		if (format == ResponseFormat.JSON || format == ResponseFormat.GEO_JSON) {
			JSONObject jsonResponse = buildJsonResponse(response, query, dao, format);
			return jsonResponse(jsonResponse);
		} else if (format == ResponseFormat.XML){
			JSONObject jsonResponse = buildJsonResponse(response, query, dao, format);
			String xml = "<response>" + XML.toString(jsonResponse.reveal()) + "</response>";
			return response(xml, "application/xml");
		}
		else if (format == ResponseFormat.TEXT_CSV) {
			String csv = csvResponse(response.getResults(), query, dao);
			res.setHeader("Content-disposition","attachment; filename=species.csv");
			return response(csv, "text/csv");
		}
		else if (format == ResponseFormat.TEXT_TSV) {
			String tsv = tsvResponse(response.getResults(), query, dao);
			res.setHeader("Content-disposition","attachment; filename=species.tsv");
			return response(tsv, "text/tsv");
		}
		throw new UnsupportedOperationException("Not yet implemented for format " + format);
	}

	private JSONObject buildJsonResponse(AggregateResponse response, AggregatedQuery query, VerticaDimensionsDAO dao, ResponseFormat format) {
		if (format == ResponseFormat.GEO_JSON) {
			return new ResponseUtil(dao).buildGeoJsonResponse(response, query);
		}
		return new ResponseUtil(dao).buildAggregateResponse(response, query);
	}

	private AggregatedQuery getQuery(HttpServletRequest req, Base base, boolean isStatisticsQuery, ResponseFormat format) throws Exception {
		BaseQuery baseQuery = getBaseQuery(req, base, isStatisticsQuery, true);

		int pageSize = getPageSize(req);
		int currentPage = getCurrentPage(req);
		Boolean onlyCountRequest = getOnlyCountRequestStatus(req);
		Boolean pairCountsRequest = getPairCountsRequestStatus(req);
		Boolean taxonAggregatesRequest = getTaxonAggregatesRequestStatus(req);
		Boolean atlasCountRequest = getAtlasCountRequestStatus(req);
		Boolean gatheringCountRequest = getGatheringCountRequestStatus(req);
		boolean exludeNulls = getExludeNulls(req);
		boolean pessimisticDateRangeHandling = getPessimisticDateRangeHandling(req);
		AggregateBy aggregateBy = getAggregateBy(req, base, isStatisticsQuery);
		if (format == ResponseFormat.GEO_JSON) {
			baseQuery.validateGeoJSON();
			aggregateBy.addGeoJsonAggregateBy(baseQuery.getCrs(), baseQuery.getFeatureType());
			aggregateBy.getCoordinateFields().validate();
		}

		OrderBy orderBy = getOrderBy(req, base);

		AggregatedQuery aggregatedQuery = new AggregatedQuery(baseQuery, aggregateBy, currentPage, pageSize).setOrderBy(orderBy);
		if (onlyCountRequest !=  null) aggregatedQuery.setOnlyCountRequest(onlyCountRequest);
		if (pairCountsRequest != null) aggregatedQuery.setPairCountRequest(pairCountsRequest);
		if (taxonAggregatesRequest != null) aggregatedQuery.setTaxonCountRequest(taxonAggregatesRequest);
		if (atlasCountRequest != null) aggregatedQuery.setAtlasCountRequest(atlasCountRequest);
		if (gatheringCountRequest != null) aggregatedQuery.setGatheringCountRequest(gatheringCountRequest);
		aggregatedQuery.setExludeNulls(exludeNulls);
		aggregatedQuery.setPessimisticDateRangeHandling(pessimisticDateRangeHandling);

		validateOrderByIsSelectedInAggregate(aggregatedQuery);
		if (baseQuery.isApprovedDataRequest()) {
			validateFieldUsage(aggregateBy);
		}
		return aggregatedQuery;
	}

	private void validateFieldUsage(AggregateBy aggregateBy) {
		for (String field : aggregateBy.getFields()) {
			if (APPROVED_DATA_REQUEST_APIKEY_DISALLOWED_FIELDS.contains(field)) {
				throw new IllegalArgumentException("Field " + field + " used in " + Const.AGGREGATE_BY + " is not allowed for " + DownloadType.APPROVED_API_KEY_REQUEST);
			}
		}
	}

	private void validateOrderByIsSelectedInAggregate(AggregatedQuery aggregatedQuery) {
		Set<String> allowedAggregateFunctionNames = getAllowedAggregateFunctions(aggregatedQuery);
		for (String field : aggregatedQuery.getOrderBy().getFieldNames()) {
			if (allowedAggregateFunctionNames.contains(field)) continue;
			if (aggregatedQuery.getAggregateBy().getFields().contains(field)) continue;
			throw new IllegalArgumentException("Field " + field + " used in " + Const.ORDER_BY + " is not among " + Const.AGGREGATE_BY + " fields or selected aggregate functions: " + allowedAggregateFunctionNames);
		}
	}

	private Set<String> getAllowedAggregateFunctions(AggregatedQuery aggregatedQuery) {
		if (defaults(aggregatedQuery)) {
			return ONLY_COUNT;
		}
		Set<String> allowed = aggregatedQuery.isOnlyCountRequest() ? Utils.set(Const.COUNT) : Utils.set(Const.aggregateFunctions(aggregatedQuery.getBase()));
		if (aggregatedQuery.getBase().includes(Base.UNIT)) {
			if (!aggregatedQuery.isPairCountRequest()) {
				allowed.remove(Const.PAIR_COUNT_SUM);
				allowed.remove(Const.PAIR_COUNT_MAX);
			}
			if (!aggregatedQuery.isTaxonAggregateRequest()) {
				allowed.remove(Const.TAXON_COUNT);
				allowed.remove(Const.SPECIES_COUNT);
				allowed.remove(Const.RED_LIST_STATUS_MAX);
			}
			if (!aggregatedQuery.isAtlasCountRequest()) {
				allowed.remove(Const.ATLAS_CODE_MAX);
				allowed.remove(Const.ATLAS_CLASS_MAX);
			}
			if (!aggregatedQuery.isGatheringCountRequest()) {
				allowed.remove(Const.GATHERING_COUNT);
			} else {
				allowed.add(Const.GATHERING_COUNT);
			}
		}
		return Collections.unmodifiableSet(allowed);
	}

	private boolean defaults(AggregatedQuery aggregatedQuery) {
		return aggregatedQuery.isOnlyCountRequest()
				&& !aggregatedQuery.isTaxonAggregateRequest()
				&& !aggregatedQuery.isPairCountRequest()
				&& !aggregatedQuery.isAtlasCountRequest()
				&& !aggregatedQuery.isGatheringCountRequest();
	}

	private Boolean getOnlyCountRequestStatus(HttpServletRequest req) {
		if (req.getParameter(Const.ONLY_COUNT) == null) return null;
		return "true".equals(req.getParameter(Const.ONLY_COUNT));
	}

	private boolean getExludeNulls(HttpServletRequest req) {
		if (req.getParameter(Const.EXCLUDE_NULLS) == null) return true;
		return "true".equals(req.getParameter(Const.EXCLUDE_NULLS));
	}

	private boolean getPessimisticDateRangeHandling(HttpServletRequest req) {
		if (req.getParameter(Const.PESSIMISTIC_DATE_RANGE_HANDLING) == null) return false;
		return "true".equals(req.getParameter(Const.PESSIMISTIC_DATE_RANGE_HANDLING));
	}

	private Boolean getTaxonAggregatesRequestStatus(HttpServletRequest req) {
		if (req.getParameter(Const.TAXON_COUNTS) == null) return null;
		return "true".equals(req.getParameter(Const.TAXON_COUNTS));
	}

	private Boolean getPairCountsRequestStatus(HttpServletRequest req) {
		if (req.getParameter(Const.PAIR_COUNTS) == null) return null;
		return "true".equals(req.getParameter(Const.PAIR_COUNTS));
	}

	private Boolean getAtlasCountRequestStatus(HttpServletRequest req) {
		if (req.getParameter(Const.ATLAS_COUNTS) == null) return null;
		return "true".equals(req.getParameter(Const.ATLAS_COUNTS));
	}

	private Boolean getGatheringCountRequestStatus(HttpServletRequest req) {
		if (req.getParameter(Const.GATHERING_COUNTS) == null) return null;
		return "true".equals(req.getParameter(Const.GATHERING_COUNTS));
	}

	private AggregateBy getAggregateBy(HttpServletRequest req, Base base, boolean isStatisticsQuery) throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy(base, isStatisticsQuery);
		String[] aggregateByParams = req.getParameterValues(Const.AGGREGATE_BY);
		if (given(aggregateByParams)) {
			for (String param : aggregateByParams) {
				if (!given(param)) continue;
				for (String paramPart : param.split(Pattern.quote(","))) {
					aggregateBy.addField(paramPart);
				}
			}
		}
		return aggregateBy;
	}

	private String csvResponse(List<AggregateRow> results, AggregatedQuery query, VerticaDimensionsDAO dao) {
		if (results.isEmpty()) return "";
		ResponseUtil util = new ResponseUtil(dao);
		StringBuilder b = new StringBuilder();
		List<String> headers = util.getRowHeaders(query, results);
		b.append(Utils.toCSV(headers));
		b.append("\n");
		for (AggregateRow row : results) {
			List<String> values = util.getRowValues(query, row);
			b.append(Utils.toCSV(values));
			b.append("\n");
		}
		return b.toString();
	}

	private String tsvResponse(List<AggregateRow> results, AggregatedQuery query, VerticaDimensionsDAO dao) {
		if (results.isEmpty()) return "";
		ResponseUtil util = new ResponseUtil(dao);
		StringBuilder b = new StringBuilder();
		List<String> headers = util.getRowHeaders(query, results);
		b.append(Utils.toTSV(headers));
		b.append("\n");
		for (AggregateRow row : results) {
			List<String> values = util.getRowValues(query, row);
			b.append(Utils.toTSV(values));
			b.append("\n");
		}
		return b.toString();
	}

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC;
	}

}
