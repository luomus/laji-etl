package fi.laji.datawarehouse.query.service.document;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/document/aggregate/*"})
public class PrivateDocumentAggregateQueryAPIServlet extends DocumentAggregateQueryAPIServlet {

	private static final long serialVersionUID = -4273452384922656L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
