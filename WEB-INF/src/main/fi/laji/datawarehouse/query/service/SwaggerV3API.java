package fi.laji.datawarehouse.query.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.SwaggerV3APIDescriptionGenerator;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/openapi-v3.json"})
public class SwaggerV3API extends ETLBaseServlet {

	private static final long serialVersionUID = -1273084699302934371L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			JSONObject response = SwaggerV3APIDescriptionGenerator.generateSwaggerDescription(getConfig());
			return jsonResponse(response);
		} catch (Exception e) {
			return loggedSystemError500(e, SwaggerV3API.class, res, req);
		}
	}





}
