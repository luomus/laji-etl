package fi.laji.datawarehouse.query.service.sample;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.service.unit.UnitListQueryAPI;

@WebServlet(urlPatterns = {"/query/sample/list/*"})
public class SampleListQueryAPI extends UnitListQueryAPI {

	private static final long serialVersionUID = -4755801363486943178L;
	public static final OrderBy DEFAULT_ORDER_BY = initDefaultOrderBy();
	public static final Selected DEFAULT_SELECT = initDefaultSelect();

	private static OrderBy initDefaultOrderBy() {
		try {
			return  new OrderBy(Base.SAMPLE)
					.addAscending("unit.taxonVerbatim")
					.addAscending("unit.unitId")
					.addAscending("unit.samples.sampleOrder");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private static Selected initDefaultSelect() {
		try {
			return new Selected(Base.SAMPLE, 
					"sample", "document.documentId", "unit.unitId", 
					"unit.taxonVerbatim", "unit.linkings.taxon.scientificName", "unit.linkings.taxon.id"
					);
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	@Override
	protected OrderBy getDefaultOrderBy() {
		return DEFAULT_ORDER_BY;
	}

	@Override
	protected Selected getDefaultSelect() {
		return DEFAULT_SELECT;
	}

	@Override
	protected Base getBase() {
		return Base.SAMPLE;
	}

}
