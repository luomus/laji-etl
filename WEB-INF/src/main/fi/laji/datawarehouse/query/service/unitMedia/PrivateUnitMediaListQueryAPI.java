package fi.laji.datawarehouse.query.service.unitMedia;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/unitMedia/list/*"})
public class PrivateUnitMediaListQueryAPI extends UnitMediaListQueryAPI {

	/**
	 *
	 */
	private static final long serialVersionUID = -2064106368804529527L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
