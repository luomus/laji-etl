package fi.laji.datawarehouse.query.download.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.models.exceptions.UnsupportedFormatException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.model.DownloadRequestRunnableTask;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.service.BaseQueryAPIServlet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/query/download/*"})
public class DownloadRequestAPI extends BaseQueryAPIServlet {

	public static class DownloadLimitExceededException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	private static final long serialVersionUID = -2844486239655641047L;
	public static final long MAX_ROW_COUNT = 2000000;

	// This is overriden by pyha download request
	protected boolean isValid(DownloadType type) {
		if (getWarehouseType() == Concealment.PUBLIC) {
			if (type == DownloadType.CITABLE) return true;
			if (type == DownloadType.LIGHTWEIGHT) return true;
		} else {
			if (type == DownloadType.AUTHORITIES_FULL) return true;
			if (type == DownloadType.AUTHORITIES_LIGHTWEIGHT) return true;
			if (type == DownloadType.AUTHORITIES_API_KEY) return true;
			if (type == DownloadType.AUTHORITIES_VIRVA_GEOAPI_KEY) return true;
		}
		return false;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		DownloadRequest request = null;
		try {
			request = getRequest(req);
		} catch (DownloadLimitExceededException e) {
			return status(429, res);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		try {
			return executeRequest(request);
		} catch (ResultCountException | UnsupportedFormatException | UnsupportedOperationException | NumberFormatException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		}
	}

	public class ResultCountException extends Exception {
		private static final long serialVersionUID = -3020910743933839666L;
		public ResultCountException(String message) {
			super(message);
		}
	}

	protected DownloadRequest getRequest(HttpServletRequest req) throws Exception {
		DownloadType downloadType = getDownloadType(req);
		boolean setDefaultFilters = !DownloadRequest.isApiKeyRequest(downloadType);
		BaseQuery baseQuery = getBaseQuery(req, false, setDefaultFilters);
		Date apiKeyExpires = getApiKeyExpires(req);
		validate(baseQuery, downloadType, apiKeyExpires);
		DownloadFormat downloadFormat = getDownloadFormat(req);
		Set<DownloadInclude> downloadIncludes = getDownloadIncludes(req);
		String locale = getLocale(req);
		String dataUsePurpose = req.getParameter(Const.DATA_USE_PURPOSE);
		Qname id = getDao().getSeqNextVal("HBF");
		DownloadRequest request = new DownloadRequest(id, new Date(), baseQuery, downloadType, downloadFormat, downloadIncludes)
				.setLocale(locale)
				.setDataUsePurpose(dataUsePurpose)
				.setApiKeyExpires(apiKeyExpires);
		return request;
	}

	private void validate(BaseQuery baseQuery, DownloadType downloadType, Date apiKeyExpires) throws DownloadLimitExceededException {
		if (!isValid(downloadType)) {
			throw new IllegalArgumentException("Invalid download type: " + downloadType + " for " + baseQuery.getWarehouse());
		}
		if (apiKeyExpires != null && !DownloadRequest.isApiKeyRequest(downloadType)) {
			throw new IllegalArgumentException(Const.API_KEY_EXPIRES + " can be given only for api key requests");
		}
		if (DownloadRequest.isApiKeyRequest(downloadType) && apiKeyExpires == null) {
			throw new IllegalArgumentException(Const.API_KEY_EXPIRES + " must be given for " + downloadType);
		}
		if (baseQuery.getPermissionId() != null) {
			throw new IllegalArgumentException("This endpoint does not accept the " + Const.PERMISSION_TOKEN + " parameter");
		}
		if (noFilters(baseQuery)) throw new IllegalArgumentException("Must give at least some filters");
		if (baseQuery.isPublic()) {
			if (getDao().exceedsDownloadLimit(baseQuery.getPersonId())) {
				throw new DownloadLimitExceededException();
			}
		}
		if (!baseQuery.isPublic()) {
			if (baseQuery.getPersonEmail() == null || baseQuery.getPersonId() == null) {
				throw new IllegalStateException("Unknown user");
			}
			if (DownloadRequest.isApiKeyRequest(downloadType)) {
				if (baseQuery.getPersonId() == null) {
					throw new IllegalStateException("Must identify user via " + Const.PERSON_TOKEN);
				}
			}
		}
	}

	private boolean noFilters(BaseQuery baseQuery) {
		return !baseQuery.getFilters().hasSetFiltersExludingDefaults();
	}

	@Override
	protected String getLocale(HttpServletRequest req) {
		String locale = req.getParameter(Const.LOCALE);
		if (!given(locale)) return "fi";
		if (!validLocale(locale)) return "fi";
		return locale;
	}

	private boolean validLocale(String locale) {
		return "fi;sv;en;".contains(locale+";");
	}

	protected Set<DownloadInclude> getDownloadIncludes(HttpServletRequest req) {
		Set<DownloadInclude> downloadIncludes = new HashSet<>();
		String[] downloadIncludesParams = req.getParameterValues(Const.DOWNLOAD_INCLUDES);
		if (!given(downloadIncludesParams)) return downloadIncludes;
		for (String param : downloadIncludesParams) {
			if (!given(param)) continue;
			for (String paramPart : param.split(Pattern.quote(","))) {
				if (!given(paramPart)) continue;
				try {

					downloadIncludes.add(DownloadInclude.valueOf(paramPart));
				} catch (Exception e) {
					throw new IllegalArgumentException("Invalid " + Const.DOWNLOAD_INCLUDES + ": " + paramPart);
				}
			}
		}
		return downloadIncludes;

	}

	protected Date getApiKeyExpires(HttpServletRequest req) {
		String s = req.getParameter(Const.API_KEY_EXPIRES);
		if (s == null) return null;
		try {
			long expiresInDays = Long.valueOf(s);
			if (expiresInDays < 1 || expiresInDays > 365) throw new IllegalArgumentException();
			long expiresTimestamp = System.currentTimeMillis() + (expiresInDays * 24 * 60 * 60 * 1000);
			return new Date(expiresTimestamp);
		} catch (Exception e) {
			throw new IllegalArgumentException(Const.API_KEY_EXPIRES + " must be a numeric integer between 1 and 365");
		}
	}

	protected DownloadType getDownloadType(HttpServletRequest req) {
		String type = req.getParameter(Const.DOWNLOAD_TYPE);
		if (!given(type)) return DownloadType.CITABLE;
		try {
			return DownloadType.valueOf(type);
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid " + Const.DOWNLOAD_TYPE + ": " + type);
		}
	}

	protected DownloadFormat getDownloadFormat(HttpServletRequest req) {
		String format = req.getParameter(Const.DOWNLOAD_FORMAT);
		if (!given(format)) return null;
		try {
			return DownloadFormat.valueOf(format);
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid " + Const.DOWNLOAD_FORMAT + ": " + format);
		}
	}

	protected ResponseData executeRequest(DownloadRequest request) throws Exception {
		VerticaQueryDAO queryDAO = request.isPublic() ? getDao().getPublicVerticaDAO().getQueryDAO() : getDao().getPrivateVerticaDAO().getQueryDAO();
		long totalResults = queryDAO.getCount(new CountQuery(request)).getTotal(); // Validates query params
		if (!request.isApiKeyRequest()) {
			if (totalResults == 0) throw new ResultCountException("No results with the given filters.");
			if (totalResults > MAX_ROW_COUNT) throw new ResultCountException("Too many results: " + totalResults + ". Maximum amount is " + MAX_ROW_COUNT);
		}
		request.setApproximateResultSize(totalResults);
		if (request.getDownloadType() == DownloadType.LIGHTWEIGHT || request.isApiKeyRequest()) {
			request.setCompleted(true);
			setCollectionIds(request);
			request.setCreated(new Date());
		}
		String apiKey = null;
		if (request.isApiKeyRequest()) {
			apiKey = getDao().generateAndStoreApiKey(request);
		}
		getDao().storeDownloadRequest(request);

		if (!request.isCompleted() && totalResults <= 10000) {
			startToHandleInBackgroundImmediately(request);
		}
		return writeResponse(request, totalResults, apiKey);
	}

	private void setCollectionIds(DownloadRequest request) throws NoSuchFieldException {
		VerticaDimensionsDAO dao = getDao().getVerticaDimensionsDAO();
		Set<Qname> collectionIds = new HashSet<>();
		for (AggregateRow row : getCollectionIds(request)) {
			collectionIds.add(getCollectionId(dao, row));
		}
		request.setCollectionIds(collectionIds);
	}

	protected Qname getCollectionId(VerticaDimensionsDAO dao, AggregateRow row) {
		String collectionId = ResponseUtil.getValue(row.getAggregateByValues().get(0), "document.collectionId", dao);
		return Qname.fromURI(collectionId);
	}

	private List<AggregateRow> getCollectionIds(DownloadRequest request) throws NoSuchFieldException {
		return getDao().getPublicVerticaDAO().getQueryDAO().getRawAggregate(new AggregatedQuery(request, new AggregateBy("document.collectionId"), 1, Integer.MAX_VALUE));
	}

	protected void startToHandleInBackgroundImmediately(DownloadRequest request) {
		try {
			new DownloadRequestRunnableTask(getConfig(), getDao(), getLocalizedTexts(), getThreadStatuses()).handle(request);
		} catch (Exception e) {
			// Will be handled when this request comes up again during normal scheduled request handling
		}
	}

	protected ResponseData writeResponse(DownloadRequest request, long totalResults, String apiKey) {
		JSONObject response = new JSONObject();
		response.setString("status", "200");
		response.setString("source", request.getApiSourceId());
		response.setInteger("approximateMatches", (int) totalResults);
		response.setString("id", request.getId().toURI());
		if (apiKey != null) {
			response.setString("apiKey", apiKey);
		}
		return jsonResponse(response);
	}

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC;
	}

}
