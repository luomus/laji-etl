package fi.laji.datawarehouse.query.download.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteStreams;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;

@WebServlet(urlPatterns = {"/download/*"})
public class DownloadFetch extends ETLBaseServlet {

	private static final long serialVersionUID = 1725950665779357696L;

	protected Concealment getWarehouse() {
		return Concealment.PUBLIC;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		OutputStream out = null;
		FileInputStream in = null;
		try {
			String id = getId(req);
			if (!given(id)) return status404(res);
			DownloadRequest downloadRequest = getDownloadRequest(id);
			validate(downloadRequest, req);

			String fileName = downloadRequest.getCreatedFile();
			File file = getPhysicalFile(fileName);
			if (!file.exists()) throw new IllegalStateException("File " + fileName + " does not exist!");

			res.setContentType("application/zip");
			res.setHeader("Content-disposition","attachment; filename=" + fileName);
			out = res.getOutputStream();
			in = new FileInputStream(file);
			ByteStreams.copy(in, out);
			return new ResponseData().setOutputAlreadyPrinted();
		} catch (Exception e) {
			if (out == null) {
				return handleError(req, res, e);
			}
			// res.getOutputStream has already been called so returning a different response is not longer possible
			if (userAborted(e)) {
				// don't want to get email about exception - there is no-one to receive response, just don't return anything
				return new ResponseData().setOutputAlreadyPrinted();
			}
			// will show default Servler Container (tomcat) error page
			throw new ServletException(e);
		} finally {
			FileUtils.close(in);
			if (out != null) {
				flush(out);
			}
		}
	}

	private void flush(OutputStream out) {
		try {
			out.flush();
		} catch (Exception e) {

		}
	}

	protected void validate(DownloadRequest downloadRequest, @SuppressWarnings("unused") HttpServletRequest req) throws Exception {
		if (!downloadRequest.isCompleted()) throw new IllegalStateException("Request is not yet completed!");

		if (downloadRequest.getWarehouse() != getWarehouse()) {
			throw new IllegalAccessException("Asking a file for " + downloadRequest.getWarehouse() + " request when making a " + getWarehouse() + " request.");
		}

		if (downloadRequest.getCreatedFile() == null) {
			throw new IllegalAccessException("Asking a file for " + downloadRequest.getDownloadType() + " request that does not have a generated file");
		}
	}

	private ResponseData handleError(HttpServletRequest req, HttpServletResponse res, Exception e) {
		String parameters = buildLogMessage(req);
		String id = null;
		try {
			id = getId(req);
		} catch (Exception e2) {}
		if (userAborted(e)) {
			// User aborted download - don't want to get error logs/emails
		} else {
			getDao().logError(Const.LAJI_ETL_QNAME, DownloadFetch.class, id, new Exception(parameters, e));
			getErrorReporter().report(DownloadFetch.class.getName() + " " + parameters, e);
		}
		return error(500, res, req, e.getMessage());
	}

	private boolean userAborted(Exception e) {
		String stack = LogUtils.buildStackTrace(e, 5);
		return stack.contains("ClientAbortException");
	}

	private DownloadRequest getDownloadRequest(String id) throws Exception {
		try {
			DownloadRequest downloadRequest = getDao().getDownloadRequest(new Qname(id));
			return downloadRequest;
		} catch (Exception e) {
			throw new IllegalArgumentException("Nothing found with id " + id, e);
		}
	}

	private File getPhysicalFile(String fileName) {
		Config config = getConfig();
		String baseFolder = config.baseFolder();
		File storageFolder = new java.io.File(baseFolder + config.get("StorageFolder"));
		File file = new java.io.File(storageFolder, fileName);
		return file;
	}

}
