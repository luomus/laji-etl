package fi.laji.datawarehouse.query.download.util;

import java.util.List;

public interface FileCreator {

	public File create(String name);

	public List<File> getWrittenFiles();
	
}
