package fi.laji.datawarehouse.query.download.rowhandlers;

import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;
import fi.luomus.commons.utils.Utils;

public abstract class AbstractCSVBaseHandler extends AbstractBaseHandler {

	public AbstractCSVBaseHandler(FileCreator fileCreator, DAO dao) {
		super(fileCreator, dao);
	}

	@Override
	protected void handleKeywords(File file, String parentId, List<String> strings) {
		for (String string : strings) {
			String line = Utils.toCSV(parentId, string);
			file.write(line);
		}
	}

	@Override
	protected void writeKeywordHeaders(File file) {
		file.write(Utils.toCSV("Parent", "Keyword"));
	}

	@Override
	protected abstract void handle(Document document, Gathering gathering, Unit unit);

	@Override
	protected void writeIncludeHeaders(File file, Class<?> includeClass) {
		List<String> headers = Utils.list("Parent");
		headers.addAll(getHeaders(includeClass, null));
		file.write(toCSV(headers));
	}

	@Override
	protected <T> void writeIncludeValues(File file, String parentId, List<T> items) {
		for (T item : items) {
			List<String> values = Utils.list(parentId);
			values.addAll(getValues(item));
			String line = toCSV(values);
			file.write(line);
		}
	}

	protected String toCSV(List<String> values) {
		return Utils.toCSV(values);
	}

	protected String toCSV(String ... values) {
		return Utils.toCSV(values);
	}
}
