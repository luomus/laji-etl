package fi.laji.datawarehouse.query.download.rowhandlers;

import java.util.ArrayList;
import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;

public class FlatTSVRowHandlerImple extends AbstractTSVBaseHandler {

	private final File rows;
	private final List<String> currentRow = new ArrayList<>();

	public FlatTSVRowHandlerImple(FileCreator fileCreator, DAO dao) {
		super(fileCreator, dao);
		this.rows = fileCreator.create("rows");
	}

	@Override
	protected void handle(Document document, Gathering gathering, Unit unit) {
		if (rows.isEmpty()) {
			writeHeaders();
		}
		setValues(unit);
		setValues(gathering);
		setValues(document);
		rows.write(toTSV(currentRow));
		currentRow.clear();
	}

	private void setValues(Object o) {
		currentRow.addAll(getValues(o));
	}

	private void writeHeaders() {
		StringBuilder b = new StringBuilder();
		b.append(toTSV(getHeaders(Unit.class, "Unit"))).append("\t");
		b.append(toTSV(getHeaders(Gathering.class, "Gathering"))).append("\t");
		b.append(toTSV(getHeaders(Document.class, "Document")));
		rows.write(b.toString());
	}

}
