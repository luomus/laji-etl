package fi.laji.datawarehouse.query.download.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/downloads/*"})
public class DownloadsAPI extends ApiKeysAPI {

	private static final long serialVersionUID = -5394438724353195921L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			Qname personId = getPersonTokenPersonId(req);
			Qname downloadId = new Qname(getId(req));
			if (personId != null) {
				if (downloadId.isSet()) {
					return writeResponse(byId(getDao().getPersonsAuthoritiesDownloadRequests(personId), downloadId), true);
				}
				return writeResponse(getDao().getPersonsAuthoritiesDownloadRequests(personId), true);
			}
			boolean privateAccess = hasPrivateAccess(req);
			if (privateAccess) {
				if (downloadId.isSet()) {
					return writeResponse(byId(downloadId), privateAccess);
				}
				return writeResponse(getDao().getAuthoritiesDownloadRequests(), privateAccess);
			}
			if (!downloadId.isSet()) throw new UnsupportedOperationException("When accessing public info on downloads, must provide id of the request (listing all is not supported)");
			return writeResponse(byId(downloadId), privateAccess);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			return loggedSystemError500(e, DownloadsAPI.class, res, req);
		}
	}

	private DownloadRequest byId(Qname downloadId) {
		try {
			return getDao().getDownloadRequest(downloadId);
		} catch (Exception e) {
			// not found
			return null;
		}
	}

}
