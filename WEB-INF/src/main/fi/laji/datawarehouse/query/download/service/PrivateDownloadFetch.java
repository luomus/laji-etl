package fi.laji.datawarehouse.query.download.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.luomus.commons.containers.rdf.Qname;

@WebServlet(urlPatterns = {"/download/secured/*"})
public class PrivateDownloadFetch extends DownloadFetch {

	private static final long serialVersionUID = -8880685493663717884L;

	@Override
	protected Concealment getWarehouse() {
		return Concealment.PRIVATE;
	}

	@Override
	protected void validate(DownloadRequest downloadRequest, HttpServletRequest req) throws Exception {
		super.validate(downloadRequest, req);

		String personToken = req.getParameter(Const.PERSON_TOKEN);
		if (!given(personToken)) throw new IllegalAccessException("Must give a valid " + Const.PERSON_TOKEN);

		Qname userId = null;
		try {
			userId =  getDao().getPerson(personToken).getId();
		} catch (Exception e) {
			throw new IllegalAccessException("This " + Const.PERSON_TOKEN + " does not define a valid logged in user");
		}

		if (!userId.equals(downloadRequest.getPersonId())) {
			throw new IllegalAccessException("Person defined by " + Const.PERSON_TOKEN + " (" + userId + ") does not match the person that made the request (" + downloadRequest.getId() + ").");
		}

	}

}
