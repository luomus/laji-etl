package fi.laji.datawarehouse.query.download.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class DownloadRequest extends BaseQuery implements Comparable<DownloadRequest> {

	public enum DownloadType {
		CITABLE, LIGHTWEIGHT,
		AUTHORITIES_FULL, AUTHORITIES_LIGHTWEIGHT,
		APPROVED_DATA_REQUEST, APPROVED_API_KEY_REQUEST, DATA_REQUEST,
		AUTHORITIES_API_KEY, AUTHORITIES_VIRVA_GEOAPI_KEY
	}

	public enum DownloadFormat { CSV_FLAT, TSV_FLAT, EXCEL_FLAT, ODS_FLAT }

	public enum DownloadInclude {
		DOCUMENT_FACTS, GATHERING_FACTS, UNIT_FACTS,
		DOCUMENT_MEDIA, GATHERING_MEDIA, UNIT_MEDIA,
		DOCUMENT_EDITORS, DOCUMENT_KEYWORDS
	}

	private final Qname id;
	private final Date requested;
	private final DownloadType downloadType;
	private final DownloadFormat downloadFormat;
	private final Set<DownloadInclude> downloadIncludes;
	private boolean completed;
	private boolean failed;
	private String failureMessage;
	private long approximateResultSize;
	private String createdFile;
	private Double createdFileSize;
	private Date created;
	private Set<Qname> collectionIds;
	private String locale;
	private String dataUsePurpose;
	private Date apiKeyExpires;

	public DownloadRequest(Qname id, Date requested, BaseQuery baseQuery, DownloadType downloadType, DownloadFormat downloadFormat, Set<DownloadInclude> downloadIncludes) {
		super(baseQuery);
		notNull(id, requested, baseQuery, downloadType);
		this.id = id;
		this.downloadType = downloadType;

		if (!supported(downloadFormat)) {
			if (!isApiKeyRequest()) {
				downloadFormat = DownloadFormat.TSV_FLAT;
			}
		}
		this.downloadFormat = downloadFormat;
		this.downloadIncludes = downloadIncludes == null ? Collections.emptySet() : downloadIncludes;
		this.requested = requested;
		this.completed = false;
		this.failed = false;
		if (isApprovedDataRequest(downloadType)) {
			setApprovedDataRequest(true);
		}
	}

	private static boolean isApprovedDataRequest(DownloadType downloadType) {
		return DownloadType.APPROVED_DATA_REQUEST.equals(downloadType) || DownloadType.APPROVED_API_KEY_REQUEST.equals(downloadType);
	}

	public boolean isApiKeyRequest() {
		return isApiKeyRequest(getDownloadType());
	}

	public static boolean isApiKeyRequest(DownloadType downloadType) {
		return DownloadType.AUTHORITIES_API_KEY.equals(downloadType)
				|| DownloadType.APPROVED_API_KEY_REQUEST.equals(downloadType)
				|| DownloadType.AUTHORITIES_VIRVA_GEOAPI_KEY.equals(downloadType);
	}

	public boolean isAuthoritiesDownload() {
		return DownloadType.AUTHORITIES_FULL.equals(getDownloadType())
				|| DownloadType.AUTHORITIES_LIGHTWEIGHT.equals(getDownloadType());
	}

	public boolean isPubliclyDownloadable() {
		return isPublic() && getDownloadType() == DownloadType.CITABLE;
	}

	private boolean supported(DownloadFormat downloadFormat) {
		if (downloadFormat == DownloadFormat.TSV_FLAT) return true;
		if (downloadFormat == DownloadFormat.CSV_FLAT) return true;
		return false;
	}

	private void notNull(Object ...objects) {
		for (Object o : objects) {
			if (o == null) throw new IllegalArgumentException("Required parameter is null");
		}
	}

	public DownloadType getDownloadType() {
		return downloadType;
	}

	public DownloadFormat getDownloadFormat() {
		return downloadFormat;
	}

	public Set<DownloadInclude> getDownloadIncludes() {
		return downloadIncludes;
	}

	public Qname getId() {
		return id;
	}

	public long getApproximateResultSize() {
		return approximateResultSize;
	}

	public DownloadRequest setApproximateResultSize(long approximateResultSize) {
		this.approximateResultSize = approximateResultSize;
		return this;
	}

	public Date getRequested() {
		return requested;
	}

	public boolean isCompleted() {
		return completed;
	}

	public DownloadRequest setCompleted(boolean completed) {
		this.completed = completed;
		return this;
	}

	public boolean isFailed() {
		return failed;
	}

	public DownloadRequest setFailed(boolean failed) {
		this.failed = failed;
		return this;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public DownloadRequest setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
		return this;
	}

	public String getCreatedFile() {
		return createdFile;
	}

	public DownloadRequest setCreatedFile(String createdFile) {
		this.createdFile = createdFile;
		return this;
	}

	public Set<Qname> getCollectionIds() {
		return collectionIds;
	}

	public DownloadRequest setCollectionIds(Set<Qname> collectionIds) {
		this.collectionIds = collectionIds;
		return this;
	}

	public Double getCreatedFileSize() {
		return createdFileSize;
	}

	public DownloadRequest setCreatedFileSize(Double createdFileSize) {
		this.createdFileSize = createdFileSize;
		return this;
	}

	public String getLocale() {
		if (locale == null) return "fi";
		return locale;
	}

	public DownloadRequest setLocale(String locale) {
		this.locale = locale;
		return this;
	}

	public Date getCreated() {
		return created;
	}

	public DownloadRequest setCreated(Date created) {
		this.created = created;
		return this;
	}

	@Override
	public String toString() {
		List<String> includes = new ArrayList<>();
		for (Enum<?> e : downloadIncludes) {
			includes.add(e.name());
		}
		Collections.sort(includes);
		return "DownloadRequest [id=" + id + ", requested=" + date(requested) + ", downloadType=" + downloadType + ", downloadFormat=" + downloadFormat +
				", downloadIncludes=" + includes + ", completed=" + completed + ", failed=" + failed + ", failureMessage=" + failureMessage +
				", approximateResultSize=" + approximateResultSize + ", createdFile=" + createdFile + ", createdFileSize=" + createdFileSize + ", created=" + date(created) +
				", collectionIds=" + collectionIds + ", locale=" + locale + ", baseQuery=" + super.toString() + "]";
	}

	private String date(Date date) {
		if (date == null) return null;
		return DateUtils.format(date, "yyyy-MM-dd");
	}

	public String getDataUsePurpose() {
		return dataUsePurpose;
	}

	public DownloadRequest setDataUsePurpose(String dataUsePurpose) {
		this.dataUsePurpose = dataUsePurpose;
		return this;
	}

	public Date getApiKeyExpires() {
		return apiKeyExpires;
	}

	public DownloadRequest setApiKeyExpires(Date apiKeyExpires) {
		this.apiKeyExpires = apiKeyExpires;
		return this;
	}

	@Override
	public int compareTo(DownloadRequest o) {
		Date thisD = this.getRequested();
		Date otherD = o.getRequested();
		if (thisD == null) thisD = new Date(0);
		if (otherD == null) otherD = new Date(0);
		int c = otherD.compareTo(thisD);
		if (c != 0) return c;
		return o.getId().compareTo(this.getId());
	}

}
