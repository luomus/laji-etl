package fi.laji.datawarehouse.query.download.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.CoordinatesWithOverlapRatio;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.laji.datawarehouse.query.model.Filter;
import fi.laji.datawarehouse.query.model.FilterDefinition.Type;
import fi.laji.datawarehouse.query.model.FilterInfo;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.PolygonIdSearch;
import fi.laji.datawarehouse.query.model.PolygonSearch;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonValidationException;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfResource;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class DownloadRequestReadmeGenerator {

	private static final EnumToProperty enumToProperty = EnumToProperty.getInstance();
	private static final String LINEBREAK = "\r\n";
	private final DAO dao;
	private final LocalizedTextsContainer locales;

	public DownloadRequestReadmeGenerator(DAO dao, LocalizedTextsContainer locales) {
		this.dao = dao;
		this.locales = locales;
	}

	public List<Pair<String, String>> generateFilterDescriptions(Filters filters, String locale) {
		List<Pair<String, String>> filtersDesc = new ArrayList<>();
		Iterator<Filter> i = filters.getSetFiltersExcludingDefaults().iterator();
		while (i.hasNext()) {
			Filter filter = i.next();
			FilterInfo filterInfo = Filters.filterInfo(filter.getFieldName());
			String label = getFilterLabel(filterInfo, locale);
			String value = getFilterValue(locale, filter, filterInfo);
			filtersDesc.add(new Pair<>(label, value));
		}
		return filtersDesc;
	}

	public String generateFiltersText(Filters filters, String locale, Concealment publicOrPrivate) {
		StringBuilder b = new StringBuilder();
		Iterator<Pair<String, String>> i = generateFilterDescriptions(filters, locale).iterator();
		while (i.hasNext()) {
			Pair<String, String> desc = i.next();
			b.append(desc.getKey()).append(": ");
			b.append(desc.getValue());
			if (i.hasNext()) linebreak(b);
		}
		linebreak(b);
		linebreak(b);
		b.append(linkText(locale)).append(":");
		linebreak(b);
		b.append(generateLink(filters, locale, publicOrPrivate));
		return b.toString();
	}

	public String generateLink(Filters filters, String locale, Concealment publicOrPrivate) {
		List<String> linkParams = new ArrayList<>();
		Iterator<Filter> i = filters.getSetFiltersExcludingDefaults().iterator();
		while (i.hasNext()) {
			Filter filter = i.next();
			linkParams.add(generateLinkParam(filter));
		}
		String domain = publicOrPrivate == Concealment.PUBLIC ? "laji.fi" : "viranomaiset.laji.fi";
		return "https://"+domain+"/"+locale+"/observation/map?" + linkParams.stream().collect(Collectors.joining("&"));
	}

	private String generateLinkParam(Filter filter) {
		return filter.getFieldName() + "=" + Utils.urlEncode(filter.valueToString());
	}

	private String linkText(String locale) {
		if ("fi".equals(locale)) return "Linkki hakuun";
		if ("sv".equals(locale)) return "Länk till sökning";
		return "Link to search";
	}

	public String generateReadme(DownloadRequest request) throws Exception {
		StringBuilder b = new StringBuilder();
		appendReadme(b, request, "fi");
		mainSeparator(b);

		appendReadme(b, request, "en");
		mainSeparator(b);

		appendReadme(b, request, "sv");
		linebreak(b);

		return trim80Char(b.toString());
	}

	public String generatePrivateReadme(DownloadRequest request) {
		StringBuilder b = new StringBuilder();

		appendPrivateReadme(b, request, "fi");
		mainSeparator(b);

		appendPrivateReadme(b, request, "en");
		mainSeparator(b);

		appendPrivateReadme(b, request, "sv");
		linebreak(b);

		return trim80Char(b.toString());
	}

	private void appendPrivateReadme(StringBuilder b, DownloadRequest request, String locale) {
		b.append(locales.getText("priv_readme_line1", locale)).append(" ").append(request.getId().toURI());
		linebreak(b);
		linebreak(b);
		b.append(locales.getText("readme_line2_pre", locale)).append(" ").append(getLocalizedDate(request, locale)).append(" ");
		b.append(locales.getText("readme_line2_post", locale));
		linebreak(b);
		Concealment filterLinkPublicity = request.isApprovedDataRequest() ? Concealment.PUBLIC : Concealment.PRIVATE;
		b.append(generateFiltersText(request.getFilters(), locale, filterLinkPublicity));
		if (request.isApprovedDataRequest()) {
			separator(b);
			header(b, locales.getText("terms_of_use", locale));
			b.append(locales.getText("terms_of_use_pyha", locale));
			separator(b);
		} else {
			header(b, locales.getText("terms_of_use", locale));
			b.append(locales.getText("terms_of_use_authorities", locale));
		}
		header(b, locales.getText("citation", locale));
		b.append(locales.getText("citationFinBIF", locale)).append(". ");
		b.append(generateCollectionIds(request));
		b.append(" (").append(locales.getText("accessed", locale)).append(" ").append(getLocalizedDate(request, locale)).append(").");
		linebreak(b);
		linebreak(b);
		b.append(locales.getText("partialCitation_private", locale));

		//		youMayCite = Voit viitata tähän lataukseen seuraavasti:
		//			citationFinBIF = Suomen Lajitietokeskus/FinBIF
		//			accessed = haettu
		//			partialCitation = Jos käytät vain osaa aineistoista, on suositeltavaa, että viittaat vain niihin aineistoihin. Latauksen osajoukkoon voi viitata seuraavasti (poista käyttämätön aineisto):

		collections(b, request, locale);
		instructions(b, locale);
	}

	private void appendReadme(StringBuilder b, DownloadRequest request, String locale) throws Exception {
		b.append(locales.getText("readme_line1", locale)).append(" ").append(request.getId().toURI());
		linebreak(b);
		linebreak(b);
		b.append(locales.getText("readme_line2_pre", locale)).append(" ").append(getLocalizedDate(request, locale)).append(" ");
		b.append(locales.getText("readme_line2_post", locale));
		linebreak(b);
		b.append(generateFiltersText(request.getFilters(), locale, Concealment.PUBLIC));
		collections(b, request, locale);
		instructions(b, locale);
	}

	private void collections(StringBuilder b, DownloadRequest request, String locale) {
		header(b, locales.getText("informationSources", locale));
		b.append(locales.getText("informationSourceIntro", locale));
		linebreak(b);
		linebreak(b);
		b.append(generateCollectionsText(request, locale));
	}

	private void instructions(StringBuilder b, String locale) {
		header(b, locales.getText("instructions", locale));
		b.append(locales.getText("gis_instructions", locale));
		linebreak(b);
		b.append(locales.getText("excel_instructions", locale));
	}

	public String getLocalizedDate(DownloadRequest request, String locale) {
		return DateUtils.format(request.getRequested(), locales.getText("dateLocale", locale));
	}

	public String generateCollectionsText(DownloadRequest request, String locale) {
		StringBuilder b = new StringBuilder();
		List<CollectionMetadata> metadatas = getCollectionsSortedByName(request, locale);
		Iterator<CollectionMetadata> i = metadatas.iterator();
		while (i.hasNext()) {
			CollectionMetadata collectionMetadata = i.next();
			Qname collectionId = collectionMetadata.getQname();
			String collectionName = collectionMetadata.getName().forLocale(locale);
			b.append(collectionName  + " - " + collectionId.toURI());
			linebreak(b);
			if (given(collectionMetadata.getIntellectualRights())) {
				String license = null;
				try {
					license = collectionMetadata.getIntellectualRightsDescription().forLocale(locale);
				} catch (Exception e) {}
				if (!given(license)) license = collectionMetadata.getIntellectualRights().toString();
				b.append(license);
				linebreak(b);
				if (given(collectionMetadata.getContactEmail())) {
					b.append(locales.getText("contactInfoLine", locale)).append(" ").append(collectionMetadata.getContactEmail());
					linebreak(b);
				}
			}
			if (i.hasNext()) linebreak(b);
		}
		return b.toString();
	}

	public String generateCollectionIds(DownloadRequest request) {
		return getCollectionsSortedById(request).stream().map(id->id.toURI()).collect(Collectors.joining(", "));
	}

	private List<CollectionMetadata> getCollectionsSortedByName(DownloadRequest request, String locale) {
		List<CollectionMetadata> metadatas = new ArrayList<>();
		for (Qname collectionId : request.getCollectionIds()) {
			try {
				CollectionMetadata collectionMetadata = dao.getCollections().get(collectionId.toURI());
				if (collectionMetadata != null) {
					metadatas.add(collectionMetadata);
					continue;
				}
				logError(collectionId.toString(), new IllegalStateException("Missing collection: " + collectionId));
			} catch (Exception e) {
				logError(collectionId.toString(), new IllegalStateException("Exception getting collection: " + collectionId, e));
			}
			metadatas.add(new CollectionMetadata(collectionId, new LocalizedText().set(locale, "ERROR: UNKNOWN COLLECTION! - INFORMATION MISSING!"), null));
		}
		Collections.sort(metadatas, new Comparator<CollectionMetadata>() {
			@Override
			public int compare(CollectionMetadata c1, CollectionMetadata c2) {
				String n1 = c1.getName().forLocale(locale);
				String n2 = c2.getName().forLocale(locale);
				if (n1 == null) n1 = "|";
				if (n2 == null) n2 = "|";
				return n1.compareTo(n2);
			}
		});
		return metadatas;
	}

	private List<Qname> getCollectionsSortedById(DownloadRequest request) {
		List<Qname> ids = new ArrayList<>(request.getCollectionIds());
		Collections.sort(ids);
		return ids;
	}


	private void logError(String info, Exception exception) {
		dao.logError(Const.LAJI_ETL_QNAME, DownloadRequestReadmeGenerator.class, info, exception);
		dao.getErrorReporter().report(exception);
	}

	private String getFilterValue(String locale, Filter filter, FilterInfo filterInfo) {
		if (filter.getFieldName().equals("taxonId") || filter.getFieldName().equals("taxonCensus")) {
			return getTaxonValue(filter.getQnames());
		}

		if (filter.getFieldName().equals("target")) {
			filter = replaceTaxonIdentifiers(filter);
		}

		Type type = filterInfo.getInfo().type();
		if (type == Type.STRING) {
			return getStringValue(locale, filter);
		}
		if (type == Type.ENUMERATION) {
			return getEnumerationFilterValue(locale, filter);
		}
		if (type == Type.RESOURCE) {
			return getResourceFilterValue(filter, locale, filterInfo);
		}

		throw new UnsupportedOperationException(type.name());
	}

	private Filter replaceTaxonIdentifiers(Filter filter) {
		Set<String> targets = new LinkedHashSet<>();
		for (String target : filter.getStrings()) {
			try {
				if (target.startsWith("MX.")) {
					targets.add(getTaxonValue(Utils.set(new Qname(target))));
				} else if (target.startsWith(RdfResource.DEFAULT_NAMESPACE_URI)) {
					targets.add(getTaxonValue(Utils.set(Qname.fromURI(target))));
				} else {
					targets.add(target);
				}
			} catch (Exception e) {
				targets.add(target);
			}
		}
		filter = new Filter(filter.getFieldName(), targets, filter.getParent());
		return filter;
	}

	private String getTaxonValue(Set<Qname> qnames) {
		StringBuilder b = new StringBuilder();
		Iterator<Qname> i  = qnames.iterator();
		while (i.hasNext()) {
			Qname taxonId = i.next();
			try {
				Taxon taxon = dao.getTaxon(taxonId);
				if (given(taxon.getScientificName())) {
					b.append(taxon.getScientificName()).append(" (").append(taxonId.toString()).append(")");
				} else if (given(taxon.getVernacularName().forLocale("en"))) {
					b.append(taxon.getVernacularName().forLocale("en")).append(" (").append(taxonId.toString()).append(")"); // virus
				} else {
					b.append(taxonId.toURI());
				}
			} catch (Exception e) {
				b.append(taxonId.toURI());
			}
			if (i.hasNext()) {
				b.append(", ");
			}
		}
		return b.toString();
	}

	private String getStringValue(String locale, Filter filter) {
		if (filter.getValue() instanceof Date) {
			return DateUtils.format((Date) filter.getValue(), "yyyy-MM-dd");
		}
		if (filter.getValue() instanceof PolygonIdSearch) {
			return getPolygonIdValue(filter.getPolygonId());
		}
		if (filter.getValue() instanceof PolygonSearch) {
			return filter.getPolygon().getWKT();
		}
		if (Collection.class.isAssignableFrom(filter.getValue().getClass())) {
			Collection<?> col = (Collection<?>) filter.getValue();
			Object first = col.iterator().next();
			if (first instanceof Coordinates) {
				return getCoordinateValue(filter.getCoordinates(), locale);
			}
			if (first instanceof CollectionAndRecordQuality) {
				return getCollectionAndRecordQualityValue(filter.getCollectionAndRecordQualities(), locale);
			}
		}
		if (filter.getFieldName().startsWith("teamMember")) {
			return filterByPersonString(locale);
		}
		String value = filter.getValue().toString();
		if (value.equals("true")) return trueFor(locale);
		if (value.equals("false")) return falseFor(locale);
		if (value.startsWith("[")) {
			value = value.replace("[", "").replace("]", "");
		}
		return value;
	}

	private String getCollectionAndRecordQualityValue(Set<CollectionAndRecordQuality> collectionAndRecordQualities, String locale) {
		StringBuilder b = new StringBuilder();
		Iterator<CollectionAndRecordQuality> i = collectionAndRecordQualities.iterator();
		while (i.hasNext()) {
			CollectionAndRecordQuality q = i.next();
			b.append(getEnumerationValue(locale, q.getCollectionQuality())).append(": ");
			Iterator<RecordQuality> ri = q.getRecordQuality().iterator();
			while (ri.hasNext()) {
				RecordQuality recordQuality = ri.next();
				b.append(getEnumerationValue(locale, recordQuality));
				if (ri.hasNext()) b.append(", ");
			}
			if (i.hasNext()) b.append("; ");
		}
		return b.toString();
	}

	private String getCoordinateValue(Set<CoordinatesWithOverlapRatio> coordinates, String locale) {
		StringBuilder b = new StringBuilder();
		Iterator<CoordinatesWithOverlapRatio> i = coordinates.iterator();
		while (i.hasNext()) {
			CoordinatesWithOverlapRatio c = i.next();
			b.append(c.getLatMin()).append(" - ").append(c.getLatMax()).append(" N ");
			b.append(c.getLonMin()).append(" - ").append(c.getLonMax()).append(" E ");
			b.append(c.getType().name());
			if (c.getOverlapRatio() != null) {
				b.append(" ").append(overlapStringFor(locale)).append(" ").append(c.getOverlapRatio());
			}
			if (i.hasNext()) b.append("; ");
		}
		return b.toString();
	}

	private String getPolygonIdValue(PolygonIdSearch polygonId) {
		String wkt = dao.getPolygonSearch(polygonId.getId());
		if (wkt == null) {
			throw new IllegalArgumentException("Nothing found with polygonId " + polygonId.getId());
		}
		try {
			PolygonSearch polygon = new PolygonSearch(wkt, Coordinates.Type.EUREF.name());
			return polygon.getWKT() + " (id " + polygonId.getId() + ")";
		} catch (PolygonValidationException e) {
			throw new IllegalStateException("Impossible polygon error for polygon id " + polygonId.getId(), e);
		}
	}

	private static final Map<String, String> OVERLAP_LOCALIZED;
	static {
		OVERLAP_LOCALIZED = new HashMap<>();
		OVERLAP_LOCALIZED.put("fi", "vaadittu päällekkäisyyssuhde");
		OVERLAP_LOCALIZED.put("sv", "Krävs överlappningsförhållande");
		OVERLAP_LOCALIZED.put("en", "required overlap ratio");
	}

	private Object overlapStringFor(String locale) {
		return OVERLAP_LOCALIZED.get(locale);
	}

	private static final Map<String, String> FALSE_LOCALIZED;
	static {
		FALSE_LOCALIZED = new HashMap<>();
		FALSE_LOCALIZED.put("fi", "Ei");
		FALSE_LOCALIZED.put("sv", "Nej");
		FALSE_LOCALIZED.put("en", "No");
	}

	private static final Map<String, String> TRUE_LOCALIZED;
	static {
		TRUE_LOCALIZED = new HashMap<>();
		TRUE_LOCALIZED.put("fi", "Kyllä");
		TRUE_LOCALIZED.put("sv", "Ja");
		TRUE_LOCALIZED.put("en", "Yes");
	}

	private String falseFor(String locale) {
		return FALSE_LOCALIZED.get(locale);
	}

	private String trueFor(String locale) {
		return TRUE_LOCALIZED.get(locale);
	}

	private String getEnumerationFilterValue(String locale, Filter filter) {
		StringBuilder b = new StringBuilder();
		if (Collection.class.isAssignableFrom(filter.getValue().getClass())) {
			Iterator<Enum<?>> i = filter.getEnums().iterator();
			while (i.hasNext()) {
				Enum<?> enumValue = i.next();
				b.append(getEnumerationValue(locale, enumValue));
				if (i.hasNext()) b.append(", ");
			}
			return b.toString();
		}
		return getEnumerationValue(locale, filter.getEnum());
	}

	private String getEnumerationValue(String locale, Enum<?> enumValue) {
		try {
			Qname enumProperty = enumToProperty.get(enumValue);
			LocalizedText label = dao.getLabels(enumProperty);
			if (given(label.forLocale(locale))) {
				return label.forLocale(locale);
			}
			throw new IllegalStateException("No label for " + enumProperty + " for locale " + locale);
		} catch (Exception e) {
			logError(enumValue.toString(), e);
			return enumValue.name();
		}
	}

	private String getResourceFilterValue(Filter filter, String locale, FilterInfo filterInfo) {
		StringBuilder b = new StringBuilder();
		Iterator<Qname> i = filter.getQnames().iterator();
		while (i.hasNext()) {
			Qname value = i.next();
			b.append(getResourceFilterValue(value, locale, filterInfo));
			if (i.hasNext()) b.append(", ");
		}
		return b.toString();
	}

	private Object getResourceFilterValue(Qname value, String locale, FilterInfo filterInfo) {
		String resourceName = filterInfo.getInfo().resourceName();
		try {
			if (resourceName.equals("person")) {
				return filterByPersonString(locale);
			}
			if (resourceName.equals("sources")) {
				return dao.getSources().get(value.toURI()).getName();
			}
			if (resourceName.equals("collections")) {
				String colName = dao.getCollections().get(value.toURI()).getName().forLocale(locale);
				if (given(colName)) return colName;
				throw new IllegalStateException("No name for " + value + " for locale " + locale);
			}
			if (resourceName.equals("areas")) {
				String areaName = getAreaName(dao.getAreas().get(value), locale);
				if (given(areaName)) return areaName;
				throw new IllegalStateException("No name for area " + value);
			}
			if (resourceName.equals("informal-taxon-groups")) {
				InformalTaxonGroup group = dao.getTaxonomyDAO().getInformalTaxonGroups().get(value.toString());
				if (group == null) return "removed group " + value.toString();
				String groupName = group.getName(locale);
				if (given(groupName)) return groupName;
				throw new IllegalStateException("No name for informal group " + value);
			}
			if (resourceName.startsWith("metadata")) {
				String label = dao.getLabels(value).forLocale(locale);
				if (given(label)) return label;
				throw new IllegalStateException("No name for " + value + " for locale " + locale);
			}
			throw new UnsupportedOperationException(resourceName);
		} catch (Exception e) {
			logError(value.toString(), new Exception("Loading filter description for " + resourceName + " " + value, e));
			return value.toURI();
		}
	}

	private String filterByPersonString(String locale) {
		if ("fi".equals(locale)) return "Rajaus henkilöllä";
		if ("sv".equals(locale)) return "Filtrerad efter person";
		return "Filtered by person";
	}

	private String getAreaName(Area area, String locale) {
		if (area == null) return null;
		String name = area.getName().forLocale(locale);
		if (!given(name)) return null;
		if (area.getType().equals(new Qname("ML.country"))) return name;
		if (given(area.getAbbreviation())) {
			name += " (" + area.getAbbreviation() + ")";
		}
		return name;
	}

	private boolean given(Object o) {
		return o != null && o.toString().length() > 0;
	}

	private String getFilterLabel(FilterInfo filterInfo, String locale) {
		if (locale.equals("fi")) return filterInfo.getInfo().labelFi();
		if (locale.equals("en")) return filterInfo.getInfo().labelEn();
		if (locale.equals("sv")) return filterInfo.getInfo().labelSv();
		throw new UnsupportedOperationException(locale);
	}

	private StringBuilder linebreak(StringBuilder b) {
		return b.append(LINEBREAK);
	}

	private void mainSeparator(StringBuilder b) {
		linebreak(b).append(LINEBREAK)
		.append("===========================================")
		.append(LINEBREAK).append(LINEBREAK);
	}

	private void separator(StringBuilder b) {
		linebreak(b).append(LINEBREAK)
		.append("-------------------------------------------");
	}

	private void header(StringBuilder b, String header) {
		linebreak(b).append(LINEBREAK)
		.append(header).append(LINEBREAK)
		.append(underline(header)).append(LINEBREAK).append(LINEBREAK);
	}

	private String underline(String word) {
		return "-----------------------------------".substring(0, word.length());
	}

	private String trim80Char(String string) {
		StringBuilder b = new StringBuilder();
		for (String line : string.split(Pattern.quote(LINEBREAK))) {
			b.append(trimLin80Char(line));
		}
		return b.toString();
	}

	private String trimLin80Char(String line) {
		if (line.length() == 0) return LINEBREAK;
		if (line.length() <= 80) return line + LINEBREAK;
		String[] words = line.split(" ");
		StringBuilder allLines = new StringBuilder();
		StringBuilder trimmedLine = new StringBuilder();
		for (String word : words) {
			if (trimmedLine.length() + 1 + word.length() <= 80) {
				trimmedLine.append(word).append(" ");
			} else {
				allLines.append(trimmedLine).append(LINEBREAK);
				trimmedLine = new StringBuilder();
				trimmedLine.append(word).append(" ");
			}
		}
		if (trimmedLine.length() > 0) {
			allLines.append(trimmedLine);
		}
		linebreak(allLines);
		return allLines.toString();
	}

}
