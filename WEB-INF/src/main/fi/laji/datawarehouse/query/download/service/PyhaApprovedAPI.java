package fi.laji.datawarehouse.query.download.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/private-query/downloadApproved/*"})
public class PyhaApprovedAPI extends PyhaApprovalRequestAPI {

	public static final String SENSITIVE_APPROVED = "sensitiveApproved"; // not used at least at the moment
	public static final String REJECTED_COLLECTIONS = "rejectedCollections";
	public static final String ID = "id";
	private static final long serialVersionUID = 7509860165260867787L;

	private static final Set<String> STAGING = Utils.set("KE.861", "KE.541");
	private static final String PRODUCTION = "KE.521";

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC; // This api does not need special permissions for the api key
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		DownloadRequest request = null;
		try {
			request = getRequest(req);
			validateIsPyha(request);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			return loggedSystemError500(e, this.getClass(), res, req);
		}

		try {
			return executeRequest(request);
		} catch (Exception e) {
			return loggedSystemError500(e, this.getClass(), res, req);
		}
	}

	private void validateIsPyha(DownloadRequest request) {
		boolean ok = false;
		if (getConfig().productionMode()) {
			if (PRODUCTION.equals(request.getApiSourceId())) ok = true;
		} else {
			if (STAGING.contains(request.getApiSourceId())) ok = true;
		}
		if (!ok) throw new IllegalAccessError("Endpoint usable only for Pyha");
	}

	@Override
	protected DownloadRequest getRequest(HttpServletRequest req) throws Exception {
		Qname id = new Qname(req.getParameter(ID));
		Qname personId = new Qname(req.getParameter(Const.PERSON_ID));
		if (!id.isSet()) throw new IllegalArgumentException("Must give id of the request");
		if (!personId.isSet()) throw new IllegalArgumentException("Must give " + Const.PERSON_ID);
		Date apiKeyExpires = getApiKeyExpires(req);

		String email = getDao().getPerson(personId).getEmail();

		BaseQuery baseQuery = getBaseQuery(req, false, false).setWarehouse(Concealment.PRIVATE).setPersonId(personId).setPersonEmail(email);

		List<Qname> rejectedCollections = getRejectedCollections(req);
		for (Qname rejectedCollection : rejectedCollections) {
			baseQuery.getFilters().setCollectionIdExplicitNot(rejectedCollection);
		}

		DownloadFormat downloadFormat = getDownloadFormat(req);
		Set<DownloadInclude> downloadIncludes = getDownloadIncludes(req);

		DownloadType type = apiKeyExpires == null ? DownloadType.APPROVED_DATA_REQUEST : DownloadType.APPROVED_API_KEY_REQUEST;
		return new DownloadRequest(id, new Date(), baseQuery, type, downloadFormat, downloadIncludes).setApiKeyExpires(apiKeyExpires);
	}

	private List<Qname> getRejectedCollections(HttpServletRequest req) {
		String[] values = req.getParameterValues(REJECTED_COLLECTIONS);
		if (values == null) return Collections.emptyList();
		Map<String, CollectionMetadata> allCollections = getDao().getCollections();
		List<Qname> collections = new ArrayList<>();
		for (String value : values) {
			if (!given(value)) continue;
			for (String valuePart : value.split(Pattern.quote(","))) {
				Qname id = new Qname(valuePart);
				if (!allCollections.containsKey(id.toURI())) throw new IllegalArgumentException("Unknown collection " + id);
				collections.add(id);
			}
		}
		return collections;
	}

	@Override
	protected ResponseData executeRequest(DownloadRequest request) throws Exception {
		long totalResults = getDao().getPrivateVerticaDAO().getQueryDAO().getCount(new CountQuery(request)).getTotal(); // Validates query params
		request.setApproximateResultSize(totalResults);
		getDao().storeDownloadRequest(request);
		if (request.isApiKeyRequest()) {
			getDao().generateAndStoreApiKey(request);
		}
		if (totalResults <= 10000) {
			startToHandleInBackgroundImmediately(request);
		}
		return writeResponse(request, totalResults, null);
	}

}
