package fi.laji.datawarehouse.query.download.model;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;

public class DownloadRequestEmailGenerator {

	private final DownloadRequestReadmeGenerator readmeGenerator;
	private final LocalizedTextsContainer locales;
	private final String downloadLink;

	public DownloadRequestEmailGenerator(DownloadRequestReadmeGenerator readmeGenerator, LocalizedTextsContainer locales, String downloadLink) {
		this.readmeGenerator = readmeGenerator;
		this.locales = locales;
		this.downloadLink = downloadLink;
	}

	public String buildEmailContent(DownloadRequest request) {
		StringBuilder b = new StringBuilder();
		String locale = request.getLocale();

		b.append(locales.getText("email_line1_pre", locale))
		.append(" ").append(readmeGenerator.getLocalizedDate(request, locale)).append(" ")
		.append(locales.getText("email_line1_post", locale)).append("\n").append(readmeGenerator.generateFiltersText(request.getFilters(), locale, Concealment.PUBLIC))
		.append("\n\n")
		.append(locales.getText("email_line2", locale)).append("\n").append(downloadLink).append("?locale=").append(locale);
		return b.toString();
	}

}
