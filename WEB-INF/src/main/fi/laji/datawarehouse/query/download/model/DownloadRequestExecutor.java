package fi.laji.datawarehouse.query.download.model;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.rowhandlers.RowToFileHandler;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class DownloadRequestExecutor {

	private static final int DELAY_BETWEEN_PAGES = 1000;

	private final DownloadRequest request;
	private final RowToFileHandler rowToFileHandler;
	private final VerticaQueryDAO queryDAO;
	private final long delay;
	private final Set<DownloadInclude> downloadIncludes;
	private final Set<Qname> collectionIds = new HashSet<>();
	private long count = 0;

	public DownloadRequestExecutor(DownloadRequest request, RowToFileHandler rowToFileHandler, VerticaQueryDAO queryDAO) {
		this.request = request;
		this.rowToFileHandler = rowToFileHandler;
		this.queryDAO = queryDAO;
		this.delay = request.getApproximateResultSize() < 10000 ? 0 : DELAY_BETWEEN_PAGES;
		this.downloadIncludes = request.getDownloadIncludes();
	}

	public void create() throws Exception {
		boolean hasActualLoadDateFilter = request.getFilters().getActualLoadBefore() != null;
		try {
			ListQuery listQuery = new ListQuery(request, 1, 1000).setOrderBy(new OrderBy("document.documentId", "unit.unitId"));
			if (!hasActualLoadDateFilter) {
				listQuery.getFilters().setActualLoadBefore(beginningOfThisDay());
			}
			while (true) {
				List<JoinedRow> page = queryDAO.getRawList(listQuery);
				if (page.isEmpty()) break;
				for (JoinedRow row : page) {
					rowToFileHandler.handle(row, downloadIncludes);
					collectionIds.add(row.getDocument().getCollectionId());
					count++;
				}
				listQuery.nextPage();
				sleep();
			}
		} finally {
			if (!hasActualLoadDateFilter) {
				request.getFilters().setActualLoadBefore(null);
			}
			rowToFileHandler.close();
		}
	}

	private Date beginningOfThisDay() throws ParseException {
		return DateUtils.convertToDate(DateUtils.getCurrentDate(), "yyyy-MM-dd");
	}

	private void sleep() throws InterruptedException {
		if (delay > 0) Thread.sleep(delay);
	}

	public List<File> getCreatedFiles() {
		return rowToFileHandler.getCreatedFiles();
	}

	public Set<Qname> getCollectionIds() {
		return collectionIds;
	}

	public long getResultCount() {
		return count;
	}

}
