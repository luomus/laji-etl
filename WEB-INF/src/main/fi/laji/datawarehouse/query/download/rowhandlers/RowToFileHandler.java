package fi.laji.datawarehouse.query.download.rowhandlers;

import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.util.File;

import java.io.Closeable;
import java.util.List;
import java.util.Set;

public interface RowToFileHandler extends Closeable {
	public void handle(JoinedRow row, Set<DownloadInclude> downloadIncludes);
	@Override
	public void close();
	public List<File> getCreatedFiles();
}