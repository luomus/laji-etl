package fi.laji.datawarehouse.query.download.util;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequestReadmeGenerator;
import fi.laji.datawarehouse.query.model.EnumerationLabels;
import fi.laji.datawarehouse.query.model.Filter;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class DownloadRequestToJSON {

	public static class CollectionAndReasons {
		private final Qname collectionId;
		private Map<SecureReason, Integer> counts = new TreeMap<>();
		public CollectionAndReasons(Qname collectionId) {
			this.collectionId = collectionId;
		}
		public Qname getCollectionId() {
			return collectionId;
		}
		public Map<SecureReason, Integer> getCounts() {
			return counts;
		}
		public void addCount(SecureReason reason, int count) {
			counts.put(reason, count);
		}
	}

	private static final String[] LOCALES = { "fi", "en", "sv" };

	public static JSONObject toJSON(DownloadRequest request, Collection<CollectionAndReasons> collectionReasons, DAO dao) {
		EnumerationLabels labels = new EnumerationLabels(dao);
		DownloadRequestReadmeGenerator generator = new DownloadRequestReadmeGenerator(dao, null);
		JSONObject response = new JSONObject();

		response.setString("id", request.getId().toString());
		response.setString("source", request.getApiSourceId());
		if (request.getPersonId() != null) {
			response.setString("personId", request.getPersonId().toString());
		}
		response.setString("locale", request.getLocale());
		response.setInteger("approximateMatches", (int) request.getApproximateResultSize());
		response.setString("requested", DateUtils.format(request.getRequested(), "yyyy-MM-dd"));

		response.setString("downloadType", request.getDownloadType().name());
		if (request.getDownloadFormat() != null) {
			response.setString("downloadFormat", request.getDownloadFormat().name());
		}
		response.getArray("downloadIncludes");
		for (DownloadInclude i : request.getDownloadIncludes()) {
			response.getArray("downloadIncludes").appendString(i.name());
		}
		if (request.getCreatedFile() != null) {
			response.setString("createdFile", request.getCreatedFile());
		}
		if (request.getCreatedFileSize() != null) {
			response.setDouble("createdFileSize", request.getCreatedFileSize());
		}

		for (Filter filter : request.getFilters().getSetFiltersExcludingDefaults()) {
			response.getArray("filters").appendObject(toJSON(filter));
		}
		for (String locale : LOCALES) {
			JSONArray filterDescs = response.getObject("filterDescriptions").getArray(locale);
			for (Pair<String, String> filterDesc : generator.generateFilterDescriptions(request.getFilters(), locale)) {
				filterDescs.appendObject(new JSONObject().setString("label", filterDesc.getKey()).setString("value", filterDesc.getValue()));
			}
			response.getObject("publicLink").setString(locale, generator.generateLink(request.getFilters(), locale, Concealment.PUBLIC));
			response.getObject("privateLink").setString(locale, generator.generateLink(request.getFilters(), locale, Concealment.PRIVATE));
		}

		for (CollectionAndReasons collectionReason : collectionReasons) {
			String collectionId = collectionReason.getCollectionId().toString();
			JSONObject colJson = new JSONObject().setString("id", collectionId);
			response.getArray("collections").appendObject(colJson);

			for (Map.Entry<SecureReason, Integer> e : collectionReason.getCounts().entrySet()) {
				JSONObject reasonJson = labels.getLabels(e.getKey().name()).copy();
				reasonJson.setInteger("count", e.getValue());
				colJson.getArray("counts").appendObject(reasonJson);
			}
		}
		if (request.getDataUsePurpose() != null) {
			response.setString("dataUsePurpose", request.getDataUsePurpose());
		}
		if (request.getApiKeyExpires() != null) {
			response.setString("apiKeyExpires", DateUtils.format(request.getApiKeyExpires(), "yyyy-MM-dd"));
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	private static JSONObject toJSON(Filter filter) {
		JSONObject json = new JSONObject();
		if (Collection.class.isAssignableFrom(filter.getValue().getClass())) {
			Collection<Object> list = (Collection<Object>)filter.getValue();
			for (Object o : list) {
				json.getArray(filter.getFieldName()).appendString(toJSON(o));
			}
		} else {
			json.setString(filter.getFieldName(), toJSON(filter.getValue()));
		}
		return json;
	}

	private static String toJSON(Object value) {
		if (value instanceof Date) return DateUtils.format((Date)value, "yyyy-MM-dd");
		return value.toString();
	}

}
