package fi.laji.datawarehouse.query.download.rowhandlers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.Person;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;
import fi.laji.datawarehouse.query.model.definedfields.UnitSelectableFields;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.RedListStatus;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public abstract class AbstractBaseHandler implements RowToFileHandler {

	private final FileCreator fileCreator;
	private final DAO dao;
	private final File documentEditorsFile;
	private final File documentFactsFile;
	private final File documentKeywordsFile;
	private final File documentMediaFile;
	private final File gatheringFactsFile;
	private final File gatheringMediaFile;
	private final File unitFactsFile;
	private final File unitMediaFile;
	private final File samples;

	public AbstractBaseHandler(FileCreator fileCreator, DAO dao) {
		this.fileCreator = fileCreator;
		this.dao = dao;
		this.documentEditorsFile = fileCreator.create("document_editors");
		this.documentFactsFile = fileCreator.create("document_facts");
		this.documentKeywordsFile = fileCreator.create("document_keywords");
		this.documentMediaFile = fileCreator.create("document_media");
		this.gatheringFactsFile = fileCreator.create("gathering_facts");
		this.gatheringMediaFile = fileCreator.create("gathering_media");
		this.unitFactsFile = fileCreator.create("unit_facts");
		this.unitMediaFile = fileCreator.create("unit_media");
		this.samples = fileCreator.create("samples");
	}

	protected FileCreator getFileCreator() {
		return fileCreator;
	}

	private String currentDocumentId = "";
	private String currentGatheringId = "";

	@Override
	public void handle(JoinedRow row, Set<DownloadInclude> downloadIncludes) {
		String documentId = row.getDocument().getDocumentId().toURI();
		String gatheringId = row.getGathering().getGatheringId().toURI();
		String unitId = row.getUnit().getUnitId().toURI();

		handle(row.getDocument(), row.getGathering(), row.getUnit());

		if (!row.getDocument().getDocumentId().toURI().equals(currentDocumentId)) {
			currentDocumentId = row.getDocument().getDocumentId().toURI();
			if (downloadIncludes.contains(DownloadInclude.DOCUMENT_EDITORS)) {
				if (given(row.getDocument().getLinkings()) && given(row.getDocument().getLinkings().getEditors())) {
					if (documentEditorsFile.isEmpty()) writeIncludeHeaders(documentEditorsFile, Person.class);
					writeIncludeValues(documentEditorsFile, documentId, row.getDocument().getLinkings().getEditors());
				}
			}
			if (downloadIncludes.contains(DownloadInclude.DOCUMENT_FACTS)) {
				if (given(row.getDocument().getFacts())) {
					if (documentFactsFile.isEmpty()) writeIncludeHeaders(documentFactsFile, Fact.class);
					writeIncludeValues(documentFactsFile, documentId, row.getDocument().getFacts());
				}
			}
			if (downloadIncludes.contains(DownloadInclude.DOCUMENT_KEYWORDS)) {
				if (given(row.getDocument().getKeywords())) {
					if (documentKeywordsFile.isEmpty()) writeKeywordHeaders(documentKeywordsFile);
					handleKeywords(documentKeywordsFile, documentId, row.getDocument().getKeywords());
				}
			}
			if (downloadIncludes.contains(DownloadInclude.DOCUMENT_MEDIA)) {
				if (given(row.getDocument().getMedia())) {
					if (documentMediaFile.isEmpty()) writeIncludeHeaders(documentMediaFile, MediaObject.class);
					writeIncludeValues(documentMediaFile, documentId, row.getDocument().getMedia());
				}
			}
		}
		if (!row.getGathering().getGatheringId().toURI().equals(currentGatheringId)) {
			currentGatheringId = row.getGathering().getGatheringId().toURI();
			if (downloadIncludes.contains(DownloadInclude.GATHERING_FACTS)) {
				if (given(row.getGathering().getFacts())) {
					if (gatheringFactsFile.isEmpty()) writeIncludeHeaders(gatheringFactsFile, Fact.class);
					writeIncludeValues(gatheringFactsFile, gatheringId, row.getGathering().getFacts());
				}
			}
			if (downloadIncludes.contains(DownloadInclude.GATHERING_MEDIA)) {
				if (given(row.getGathering().getMedia())) {
					if (gatheringMediaFile.isEmpty()) writeIncludeHeaders(gatheringMediaFile, MediaObject.class);
					writeIncludeValues(gatheringMediaFile, gatheringId, row.getGathering().getMedia());
				}
			}
		}
		if (downloadIncludes.contains(DownloadInclude.UNIT_FACTS)) {
			if (given(row.getUnit().getFacts())) {
				if (unitFactsFile.isEmpty()) writeIncludeHeaders(unitFactsFile, Fact.class);
				writeIncludeValues(unitFactsFile, unitId, row.getUnit().getFacts());
			}
		}
		if (downloadIncludes.contains(DownloadInclude.UNIT_MEDIA)) {
			if (given(row.getUnit().getMedia())) {
				if (unitMediaFile.isEmpty()) writeIncludeHeaders(unitMediaFile, MediaObject.class);
				writeIncludeValues(unitMediaFile, unitId, row.getUnit().getMedia());
			}
		}
		if (given(row.getUnit().getSamples())) {
			if (samples.isEmpty()) writeIncludeHeaders(samples, Sample.class);
			writeIncludeValues(samples, unitId, row.getUnit().getSamples());
		}
	}

	private boolean given(Object o) {
		return o != null;
	}

	private boolean given(List<?> list) {
		return list != null && !list.isEmpty();
	}

	protected abstract void handle(Document document, Gathering gathering, Unit unit);

	protected abstract void writeIncludeHeaders(File file, Class<?> includeClass);

	protected abstract <T> void writeIncludeValues(File file, String parentId, List<T> items);

	protected abstract void handleKeywords(File file, String parentId, List<String> strings);

	protected abstract void writeKeywordHeaders(File file);

	@Override
	public void close() {
		for (File f : fileCreator.getWrittenFiles()) {
			try {
				f.close();
			} catch (IOException e) {
				throw new ETLException(e);
			}
		}
	}

	protected List<String> getHeaders(Class<?> modelClass, String prefix) {
		if (modelClass == Taxon.class) return taxonHeaders();
		List<String> headers = new ArrayList<>();
		for (Method m : getHandlableFields(modelClass)) {
			String fieldName = m.getAnnotation(FileDownloadField.class).name();
			if (prefix != null) fieldName = prefix + "." + fieldName;
			if (isEmbeddable(m)) {
				headers.addAll(getHeaders(m.getReturnType(), fieldName));
			} else {
				headers.add(fieldName);
			}
		}
		return headers;
	}

	private final Map<Class<?>, List<Method>> handlableFields = new HashMap<>();

	protected List<Method> getHandlableFields(Class<?> modelClass) {
		if (!handlableFields.containsKey(modelClass)) {
			handlableFields.put(modelClass, initHandlableFields(modelClass));
		}
		return handlableFields.get(modelClass);
	}

	private static final Set<String> INCLUDED_COLLECTIONS = Utils.set("team", "keywords", "secureReasons", "effectiveTags");
	private List<Method> initHandlableFields(Class<?> modelClass) {
		List<Method> fields = new ArrayList<>();
		for (Method method : ReflectionUtil.getGetters(modelClass)) {
			if (method.getAnnotation(FileDownloadField.class) == null) continue;
			if (UnitSelectableFields.isCollection(method)) {
				if (INCLUDED_COLLECTIONS.contains(ReflectionUtil.cleanGetterFieldName(method))) {
					fields.add(method);
				}
			} else {
				fields.add(method);
			}
		}
		Collections.sort(fields, new Comparator<Method>() {
			@Override
			public int compare(Method o1, Method o2) {
				FileDownloadField f1 = o1.getAnnotation(FileDownloadField.class);
				FileDownloadField f2 = o2.getAnnotation(FileDownloadField.class);
				Double order1 = f1.order();
				Double order2 = f2.order();
				int c = order1.compareTo(order2);
				if (c != 0) return c;
				return f1.name().compareTo(f2.name());
			}
		});
		return fields;
	}

	protected List<String> getValues(Object o) {
		try {
			if (o.getClass() == Taxon.class) return taxonValues(o);
			List<String> values = new ArrayList<>();
			for (Method m : getHandlableFields(o.getClass())) {
				if (isEmbeddable(m)) {
					Object embedded = m.invoke(o);
					if (embedded == null) {
						values.addAll(getEmptyValues(m.getReturnType()));
					} else {
						values.addAll(getValues(embedded));
					}
				} else {
					values.add(getValue(o, m));
				}
			}
			return values;
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	private List<String> taxonHeaders() {
		return Utils.list(
				"Taxon.ID", "Taxon.TaxonomicOrder",
				"Taxon.ScientificName", "Taxon.FinnishName", "Taxon.EnglishName", "Taxon.SwedishName",
				"Taxon.LatestRedListStatus", "Taxon.Lajiturva", "Taxon.StatusesFI",
				"Taxon.InformalGroupFI", "Taxon.InformalGroupEN");
	}

	private List<String> taxonValues(Object o) {
		Taxon taxon = (Taxon) o;
		LocalizedText informalGroups = taxon.getInformalTaxonGroupNames();
		return Utils.list(
				taxon.getQname().toURI(),
				Long.toString(taxon.getTaxonomicOrder()),
				taxon.getScientificName(),
				taxon.getNameFinnish(),
				taxon.getNameEnglish(),
				taxon.getNameSwedish(),
				redListStatus(taxon.getLatestRedListStatusFinland()),
				threatenedStatus(taxon.getThreatenedStatus()),
				taxon.getAdministrativeStatusNames().forLocale("fi"),
				informalGroups.forLocale("fi"),
				informalGroups.forLocale("en"));
	}

	private String threatenedStatus(Qname threatenedStatus) {
		if (threatenedStatus == null) return "";
		LocalizedText t = dao.getLabels(threatenedStatus);
		if (t.hasTextForLocale("fi")) return t.forLocale("fi");
		return threatenedStatus.toString();
	}

	private String redListStatus(RedListStatus latestRedListStatusFinland) {
		if (latestRedListStatusFinland == null) return "";
		return latestRedListStatusFinland.getStatus().toString().replace("MX.iucn", "") + " " + latestRedListStatusFinland.getYear();
	}

	private boolean isEmbeddable(Method m) {
		return UnitSelectableFields.isEmbeddableType(m);
	}

	private Collection<String> getEmptyValues(Class<?> modelClass) {
		List<String> values = new ArrayList<>();
		for (@SuppressWarnings("unused") String s : getHeaders(modelClass, null)) {
			values.add("");
		}
		return values;
	}

	private String getValue(Object o, Method m) throws IllegalAccessException, InvocationTargetException {
		Object v = m.invoke(o);
		String value = "";
		if (v != null) value = toString(v);
		return value;
	}

	private String toString(Object v) {
		if (v instanceof Qname) {
			return ((Qname) v).toURI();
		}
		if (v instanceof Date) {
			return DateUtils.format((Date)v, "yyyy-MM-dd");
		}
		if (Collection.class.isAssignableFrom(v.getClass())) {
			return collectionToString(v);
		}
		return v.toString();
	}

	private String collectionToString(Object v) {
		StringBuilder b = new StringBuilder();
		Iterator<?> i = ((Collection<?>) v).iterator();
		while (i.hasNext()) {
			b.append(i.next().toString());
			if (i.hasNext()) b.append("; ");
		}
		return b.toString();
	}

	@Override
	public List<File> getCreatedFiles() {
		return fileCreator.getWrittenFiles();
	}

}
