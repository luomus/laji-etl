package fi.laji.datawarehouse.query.download.rowhandlers;

import java.util.ArrayList;
import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;

public class FlatCSVRowHandlerImple extends AbstractCSVBaseHandler {

	private final File rows;
	private final List<String> currentRow = new ArrayList<>();

	public FlatCSVRowHandlerImple(FileCreator fileCreator, DAO dao) {
		super(fileCreator, dao);
		this.rows = fileCreator.create("rows");
	}

	@Override
	protected void handle(Document document, Gathering gathering, Unit unit) {
		if (rows.isEmpty()) {
			writeHeaders();
		}
		setValues(unit);
		setValues(gathering);
		setValues(document);
		rows.write(toCSV(currentRow));
		currentRow.clear();
	}

	private void setValues(Object o) {
		currentRow.addAll(getValues(o));
	}

	private void writeHeaders() {
		StringBuilder b = new StringBuilder();
		b.append(toCSV(getHeaders(Unit.class, "Unit"))).append(",");
		b.append(toCSV(getHeaders(Gathering.class, "Gathering"))).append(",");
		b.append(toCSV(getHeaders(Document.class, "Document")));
		rows.write(b.toString());
	}

}
