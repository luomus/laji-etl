package fi.laji.datawarehouse.query.download.service;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

@WebServlet(urlPatterns = {"/private-query/download/*"})
public class PrivateDownloadRequestAPI extends DownloadRequestAPI {

	private static final long serialVersionUID = 7509860165260867787L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PRIVATE;
	}

}
