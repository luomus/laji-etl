package fi.laji.datawarehouse.query.download.util;

public class BufferedFileImple extends fi.luomus.commons.utils.BufferedFileWriter implements File {

	private final String name;
	
	public BufferedFileImple(java.io.File file) {
		super(file);
		this.name = file.getName();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public java.io.File getPhysicalFile() {
		return getFile();
	}

	@Override
	public String toString() {
		return getFile().getName();
	}
	
}
