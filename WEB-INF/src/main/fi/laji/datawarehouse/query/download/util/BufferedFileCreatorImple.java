package fi.laji.datawarehouse.query.download.util;

import java.util.ArrayList;
import java.util.List;

public class BufferedFileCreatorImple implements FileCreator {

	private final java.io.File folder;
	private final String fileNameSuffix;
	private final String fileExtension;
	private final List<File> createdFiles = new ArrayList<>();

	public BufferedFileCreatorImple(java.io.File folder, String fileNameSuffix, String fileExtension) {
		this.folder = folder;
		this.fileNameSuffix = fileNameSuffix;
		this.fileExtension = fileExtension;
	}

	@Override
	public File create(String name) {
		java.io.File ioF = new java.io.File(folder, name + "_" + fileNameSuffix + "." + fileExtension);
		File f = new fi.laji.datawarehouse.query.download.util.BufferedFileImple(ioF);
		if (ioF.exists()) {
			ioF.delete();
		}
		if (!createdFiles.contains(f)) {
			createdFiles.add(f);
		}
		return f;
	}

	@Override
	public List<File> getWrittenFiles() {
		List<File> list = new ArrayList<>();
		for (File f : createdFiles) {
			if (!f.isEmpty()) list.add(f);
		}
		return list;
	}

}
