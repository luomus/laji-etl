package fi.laji.datawarehouse.query.download.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON.CollectionAndReasons;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/api-keys/*"})
public class ApiKeysAPI extends ETLBaseServlet {

	private static final long serialVersionUID = 1729027260446178961L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			Qname personId = getPersonTokenPersonId(req);
			String pathParam = getId(req);
			Qname requestId = pathParam.startsWith("HBF.") ? new Qname(pathParam) : new Qname("");
			String apiKey = pathParam.startsWith("HBF.") ? "" : pathParam;

			// Single request by api-key
			if (given(apiKey)) {
				try {
					return writeResponse(getDao().getRequestForApiKey(apiKey), true);
				} catch (IllegalArgumentException e) {
					return unloggedAccessError403("Unknown api key", res, req);
				}
			}

			// Authenticated person's all requests or single request by id
			if (personId != null) {
				return writePersonResponse(getDao().getPersonsApiRequests(personId), requestId);
			}

			boolean privateAccess = hasPrivateAccess(req);

			// Single request by id - inclusion of apiKey to response is based on system's privileges
			if (requestId.isSet()) {
				return writeResponse(byId(getDao().getApiRequests(), requestId), privateAccess);
			}

			// All api key requests - inclusion of apiKey to response is based on system's privileges
			return writeResponse(getDao().getApiRequests(), privateAccess);

		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			return loggedSystemError500(e, ApiKeysAPI.class, res, req);
		}
	}

	protected DownloadRequest byId(List<DownloadRequest> requests, Qname id) {
		for (DownloadRequest r : requests) {
			if (r.getId().equals(id)) return r;
		}
		return null;
	}

	protected ResponseData writeResponse(DownloadRequest request, boolean privateAccess) {
		if (request == null) return jsonResponse(notFound());
		JSONObject json = toJson(getDao(), request, privateAccess);
		json.setBoolean("found", true);
		return jsonResponse(json);
	}

	private JSONObject notFound() {
		return new JSONObject().setBoolean("found", false);
	}

	protected ResponseData writeResponse(List<DownloadRequest> requests, boolean privateAccess) {
		JSONArray array = new JSONArray();
		DAO dao = getDao();
		for (DownloadRequest request : requests) {
			JSONObject json = toJson(dao, request, privateAccess);
			array.appendObject(json);
		}
		return jsonResponse(array);
	}

	private ResponseData writePersonResponse(List<Pair<String, DownloadRequest>> personsApiRequests, Qname id) {
		JSONArray array = new JSONArray();
		DAO dao = getDao();
		for (Pair<String, DownloadRequest> e : personsApiRequests) {
			String apiKey = e.getKey();
			DownloadRequest request = e.getValue();
			JSONObject json = toJson(dao, request, true);
			json.setString("apiKey", apiKey);
			if (id.equals(request.getId())) {
				json.setBoolean("found", true);
				return jsonResponse(json);
			}
			array.appendObject(json);
		}
		if (id.isSet()) return jsonResponse(notFound());
		return jsonResponse(array);
	}

	private static Set<String> WHITELIST_MINIMAL = Utils.set("id", "requested", "apiKeyExpires", "collections", "downloadType");
	private static Set<String> WHITELIST_PUBLIC_TYPES = Utils.set(WHITELIST_MINIMAL, "approximateMatches", "filters", "filterDescriptions", "publicLink");
	private static Set<String> WHITELIST_PUBLIC_DOWNLOADABLE = Utils.set(WHITELIST_PUBLIC_TYPES, "createdFile", "createdFileSize", "downloadFormat");

	private Set<String> whitelistFor(DownloadRequest request) {
		if (request.isPubliclyDownloadable()) return WHITELIST_PUBLIC_DOWNLOADABLE;
		if (request.isPublic()) return WHITELIST_PUBLIC_TYPES;
		return WHITELIST_MINIMAL;
	}

	protected JSONObject toJson(DAO dao, DownloadRequest request, boolean privateAccess) {
		JSONObject json = DownloadRequestToJSON.toJSON(request, collections(request), dao);
		if (!privateAccess) {
			Set<String> whitelist = whitelistFor(request);
			for (String key : json.getKeys()) {
				if (!whitelist.contains(key)) json.remove(key);
			}
		}
		Set<Qname> collectionSearch = collectionSearch(request);
		for (Qname collectionId : collectionSearch) {
			json.getArray("collectionSearch").appendString(collectionId.toString());
		}
		json.setBoolean("publicDownload", request.isPubliclyDownloadable());
		json.setBoolean("public", request.isPublic());
		return json;
	}

	private Set<Qname> collectionSearch(DownloadRequest request) {
		if (request.getCollectionIds() == null) return Collections.emptySet();
		Set<Qname> ids = new HashSet<>();
		Map<String, CollectionMetadata> meta = getDao().getCollections();
		for (Qname id : request.getCollectionIds()) {
			ids.add(id);
			CollectionMetadata colMeta = meta.get(id.toURI());
			if (colMeta == null) continue;
			Qname parentId;
			while ((parentId = colMeta.getParentQname()) != null) {
				ids.add(parentId);
				colMeta = meta.get(parentId.toURI());
				if (colMeta == null) break;
			}
		}
		return ids;
	}

	private Collection<CollectionAndReasons> collections(DownloadRequest request) {
		List<CollectionAndReasons> list = new ArrayList<>();
		if (request.getCollectionIds() == null) return Collections.emptyList();
		for (Qname id : request.getCollectionIds()) {
			list.add(new CollectionAndReasons(id));
		}
		return list;
	}

	protected Qname getPersonTokenPersonId(HttpServletRequest req) {
		String token = req.getParameter(Const.PERSON_TOKEN);
		if (!given(token)) return null;
		return getDao().getPerson(token).getId();
	}

	protected boolean hasPrivateAccess(HttpServletRequest req) throws Exception {
		ApiUser apiUser = getApiUser(req);
		if (apiUser.getSystemId() == null) {
			throw new IllegalAccessException("" +
					"Please ask for your system to be registered in FinBIF and your " + Const.API_KEY +
					" to be assosiated with that system to be able to make queries to PRIVATE FinBIF warehouse!");
		}
		Source source = getDao().getSources().get(apiUser.getSystemId().toURI());
		return source != null && source.isAllowedToQueryPrivateWarehouse();
	}

}
