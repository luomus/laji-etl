package fi.laji.datawarehouse.query.download.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FileDownloadField {

	public String name();
	public double order() default Double.MAX_VALUE;
}
