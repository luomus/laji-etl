package fi.laji.datawarehouse.query.download.model;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.rowhandlers.FlatCSVRowHandlerImple;
import fi.laji.datawarehouse.query.download.rowhandlers.FlatTSVRowHandlerImple;
import fi.laji.datawarehouse.query.download.rowhandlers.RowToFileHandler;
import fi.laji.datawarehouse.query.download.util.BufferedFileCreatorImple;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.FileCompresser;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class DownloadRequestRunnableTask {

	private static final Set<Qname> RUNNING_REQUESTS = new HashSet<>();

	private final DAO dao;
	private final ThreadStatusReporter statusReporter;
	private final java.io.File tempFolder;
	private final java.io.File storageFolder;
	private final Config config;
	private final DownloadRequestReadmeGenerator readmeGenerator;
	private final LocalizedTextsContainer locales;
	private final ThreadStatuses threadHandler;

	public DownloadRequestRunnableTask(Config config, DAO dao, LocalizedTextsContainer locales, ThreadStatuses threadHandler) {
		this.dao = dao;
		this.statusReporter = threadHandler.getThreadStatusReporterFor(DownloadRequestRunnableTask.class);
		String baseFolder = config.baseFolder();
		this.tempFolder = new java.io.File(baseFolder + config.get("TempFolder"));
		this.storageFolder = new java.io.File(baseFolder + config.get("StorageFolder"));
		this.config = config;
		this.readmeGenerator = new DownloadRequestReadmeGenerator(dao, locales);
		this.locales = locales;
		this.threadHandler = threadHandler;
	}

	public void handle() {
		if (!RUNNING_REQUESTS.isEmpty()) return;
		statusReporter.setStatus("init...");
		List<DownloadRequest> downloadRequests = dao.getUncompletedDownloadRequests();
		Thread t = new Thread(new Handler(downloadRequests));
		t.setName("Download Request Generator Thread");
		t.setDaemon(true);
		t.start();
	}

	public void handle(DownloadRequest downloadRequest) {
		Thread t = new Thread(new Handler(Utils.singleEntryList(downloadRequest)));
		t.setName("Download Request Generator Thread");
		t.setDaemon(true);
		t.start();
	}

	private class Handler implements Runnable {

		private final List<DownloadRequest> downloadRequests;

		public Handler(List<DownloadRequest> downloadRequests) {
			this.downloadRequests = downloadRequests;
		}

		@Override
		public void run() {
			try {
				handleNonFailed();
				retryFailed();
			} finally {
				threadHandler.reportThreadDead(DownloadRequestRunnableTask.class);
			}
		}

		private void retryFailed() {
			for (DownloadRequest request : downloadRequests) {
				if (request.isFailed()) {
					handle(request);
				}
			}
		}

		private void handleNonFailed() {
			for (DownloadRequest request : downloadRequests) {
				if (!request.isFailed()) {
					handle(request);
				}
			}
		}

		private void handle(DownloadRequest request) {
			synchronized (RUNNING_REQUESTS) {
				if (RUNNING_REQUESTS.contains(request.getId())) return;
				RUNNING_REQUESTS.add(request.getId());
			}

			try {
				log("Handling request " + request.getId() +" ...", request.getId().toString());
				boolean success = produceHandleErrors(request);
				if (success) {
					log("Request " + request.getId() + " handled!", request.getId().toString());
				} else {
					log("Request " + request.getId() + " failed!", request.getId().toString());
				}
			} finally {
				RUNNING_REQUESTS.remove(request.getId());
			}

		}

		private void log(String message, String identifier) {
			statusReporter.setStatus(message);
			dao.logMessage(Const.LAJI_ETL_QNAME, DownloadRequestRunnableTask.class, identifier, message);
		}
	}

	private boolean produceHandleErrors(DownloadRequest request) {
		try {
			validate(request);
			createDownloadAndSetInfoToRequest(request);
			markCompletedAndStore(request);
			if (request.getDownloadType() == DownloadType.CITABLE || request.getDownloadType() == DownloadType.AUTHORITIES_FULL) {
				sendEmail(request);
			}
			if (request.isApprovedDataRequest()) {
				notifyApprovalServiceOfCompletion(request);
			}
			return true;
		} catch (Exception e) {
			handleErrors(request, e);
			return false;
		}
	}

	private void createDownloadAndSetInfoToRequest(DownloadRequest request) throws Exception, FileNotFoundException {
		List<File> producedFiles = createDownloadFilesAndSetInfoToRequest(request);
		String fileName = createZip(request, producedFiles);
		setCreatedFileAndFileSizeToRequest(request, fileName);
		emptyTemp(producedFiles);
	}

	private List<File> createDownloadFilesAndSetInfoToRequest(DownloadRequest request) throws Exception {
		String fileNameSuffix = generateFileNameSuffix(request);
		DownloadRequestExecutor executor = getExecutor(request, fileNameSuffix);
		executor.create();
		setInfoToRequest(request, executor);
		List<File> producedFiles = executor.getCreatedFiles();
		addReadme(request, fileNameSuffix, producedFiles);
		return producedFiles;
	}

	private void setInfoToRequest(DownloadRequest request, DownloadRequestExecutor executor) {
		request.setCollectionIds(executor.getCollectionIds());
		request.setApproximateResultSize(executor.getResultCount());
	}

	private void addReadme(DownloadRequest request, String fileNameSuffix, List<File> producedFiles) throws Exception {
		if (request.isPublic()) {
			File readme = generateReadme(request, fileNameSuffix);
			producedFiles.add(readme);
		} else {
			File readme = generatePrivateReadme(request, fileNameSuffix);
			producedFiles.add(readme);
		}
	}

	private void setCreatedFileAndFileSizeToRequest(DownloadRequest request, String fileName) {
		double fileSize = getFileSize(fileName);
		request.setCreatedFileSize(fileSize);
		request.setCreatedFile(fileName);
	}

	private DownloadRequestExecutor getExecutor(DownloadRequest request, String fileNameSuffix) {
		VerticaQueryDAO queryDAO = getQueryDAO(request);
		RowToFileHandler rowToFileHandler = getRowHandler(request, fileNameSuffix);
		DownloadRequestExecutor creator = new DownloadRequestExecutor(request, rowToFileHandler, queryDAO);
		return creator;
	}

	private void validate(DownloadRequest request) {
		if (request.getDownloadType() == DownloadType.CITABLE) {
			if (request.isPrivate()) throw new IllegalStateException(request.getDownloadType().name() + " " + request.getWarehouse());
			return;
		}
		if (request.isApprovedDataRequest()) {
			if (request.isPublic()) throw new IllegalStateException(request.getDownloadType().name() + " " + request.getWarehouse());
			return;
		}
		if (request.getDownloadType() == DownloadType.AUTHORITIES_FULL) {
			if (request.isPublic()) throw new IllegalStateException(request.getDownloadType().name() + " " + request.getWarehouse());
			return;
		}
		if (request.getDownloadType() == DownloadType.AUTHORITIES_LIGHTWEIGHT) {
			if (request.isPublic()) throw new IllegalStateException(request.getDownloadType().name() + " " + request.getWarehouse());
			return;
		}
		throw new IllegalStateException(request.getDownloadType().name());
	}

	private void notifyApprovalServiceOfCompletion(DownloadRequest request) {
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), request.getId().toString(), "Notifying approval service of completion of " + request.getId());
		dao.notifyApprovalServiceOfCompletion(request.getId());
	}

	private double getFileSize(String fileName) {
		java.io.File physicalFile = getPhysicalFile(fileName);
		double sizeInMb = physicalFile.length() / (1024d * 1024d);
		return round(sizeInMb);
	}

	private double round(double fileSize) {
		return Math.round(fileSize * 100d) / 100d;
	}

	private File generatePrivateReadme(DownloadRequest request, String fileNameSuffix) throws Exception {
		FileCreator fileCreator = new BufferedFileCreatorImple(tempFolder, fileNameSuffix, "txt");
		File readme = fileCreator.create("readme");
		try {
			String readmeContents = readmeGenerator.generatePrivateReadme(request);
			readme.write(readmeContents);
		} finally {
			readme.close();
		}
		return readme;
	}

	private File generateReadme(DownloadRequest request, String fileNameSuffix) throws Exception {
		FileCreator fileCreator = new BufferedFileCreatorImple(tempFolder, fileNameSuffix, "txt");
		File readme = fileCreator.create("readme");
		try {
			String readmeContents = readmeGenerator.generateReadme(request);
			readme.write(readmeContents);
		} finally {
			readme.close();
		}
		return readme;
	}

	private void sendEmail(DownloadRequest request) throws Exception {
		if (request.getPersonEmail().equals("integrationtest@etl.laji.fi")) return;
		String downloadLink = downloadLink(request);
		String content = new DownloadRequestEmailGenerator(readmeGenerator, locales, downloadLink).buildEmailContent(request);
		if (config.developmentMode()) {
			dao.logMessage(Const.LAJI_ETL_QNAME, DownloadRequestRunnableTask.class, content);
		} else {
			EmailUtil emailUtil = new EmailUtil("localhost");
			emailUtil.send(request.getPersonEmail(), "noreply@laji.fi", subject(request), content, new java.io.File[]{});
		}
	}

	private String downloadLink(DownloadRequest request) {
		if (request.getDownloadType() == DownloadType.AUTHORITIES_FULL) {
			if (config.productionMode()) return "https://viranomaiset.laji.fi";
			return "https://viranomaiset-dev.laji.fi";
		}
		if (request.getDownloadType() == DownloadType.CITABLE) {
			if (config.productionMode()) return request.getId().toURI();
			return "https://dev.laji.fi/in/citation/" + request.getId().toString();
		}
		throw new UnsupportedOperationException(request.getDownloadType().name());
	}

	private String subject(DownloadRequest request) {
		String locale = request.getLocale();
		String prefix = config.productionMode() ? "" : "DEV ";
		if (request.getDownloadType() == DownloadType.AUTHORITIES_FULL) {
			if ("fi".equalsIgnoreCase(locale)) return prefix + "Viranomaisportaali: Tiedosto valmis";
			if ("sv".equalsIgnoreCase(locale)) return prefix + "Artdatacenter Myndigheters Portal: Nedladdningen slutförd";
			return prefix + "FinBIF Authorities' Portal: Download completed";
		}
		if (request.getDownloadType() == DownloadType.CITABLE) {
			if ("fi".equalsIgnoreCase(locale)) return prefix + "Laji.fi: Tiedosto valmis";
			if ("sv".equalsIgnoreCase(locale)) return prefix + "Artdatacenter: Nedladdningen slutförd";
			return prefix + "FinBIF: Download completed";
		}
		throw new UnsupportedOperationException(request.getDownloadType().name());
	}

	private void markCompletedAndStore(DownloadRequest request) {
		request.setCompleted(true);
		request.setFailed(false);
		request.setCreated(new Date());
		dao.storeDownloadRequest(request);
	}

	private void emptyTemp(List<File> producedFiles) {
		for (File file : producedFiles) {
			file.getPhysicalFile().delete();
		}
	}

	private String createZip(DownloadRequest request, List<File> producedFiles) throws FileNotFoundException {
		FileCompresser compresser = null;
		try {
			storageFolder.setWritable(true);
			storageFolder.mkdirs();
			String fileName = generateFileNameSuffix(request)+".zip";
			java.io.File physicalFile = getPhysicalFile(fileName);
			compresser = new FileCompresser(physicalFile);
			for (File file : producedFiles) {
				compresser.addToZip(file.getPhysicalFile());
			}
			return fileName;
		} finally {
			if (compresser != null) compresser.close();
		}
	}

	private java.io.File getPhysicalFile(String fileName) {
		java.io.File physicalFile = new java.io.File(storageFolder, fileName);
		return physicalFile;
	}

	private void handleErrors(DownloadRequest request, Exception e) {
		dao.logError(Const.LAJI_ETL_QNAME, DownloadRequestRunnableTask.class, request.getId().toString(), e);
		request.setFailed(true);
		request.setCompleted(false);
		request.setFailureMessage(LogUtils.buildStackTrace(e, 5));
		try {
			dao.storeDownloadRequest(request);
		} catch (Exception e1) {
			dao.logError(Const.LAJI_ETL_QNAME, DownloadRequestRunnableTask.class, request.getId().toString(), e);
		}
	}

	private RowToFileHandler getRowHandler(DownloadRequest request, String fileNameSuffix) {
		if (request.getDownloadFormat() == DownloadFormat.CSV_FLAT) {
			FileCreator fileCreator = new BufferedFileCreatorImple(tempFolder, fileNameSuffix, "csv");
			return new FlatCSVRowHandlerImple(fileCreator, dao);
		}
		FileCreator fileCreator = new BufferedFileCreatorImple(tempFolder, fileNameSuffix, "tsv");
		return new FlatTSVRowHandlerImple(fileCreator, dao);
	}

	private String generateFileNameSuffix(DownloadRequest request) {
		return request.getId().toString();
	}

	private VerticaQueryDAO getQueryDAO(DownloadRequest request) {
		return request.isPublic() ? dao.getPublicVerticaDAO().getQueryDAO() : dao.getPrivateVerticaDAO().getQueryDAO();
	}
}
