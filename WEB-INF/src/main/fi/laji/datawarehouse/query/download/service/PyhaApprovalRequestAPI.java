package fi.laji.datawarehouse.query.download.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.UnsupportedFormatException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON.CollectionAndReasons;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/private-query/downloadApprovalRequest/*"})
public class PyhaApprovalRequestAPI extends DownloadRequestAPI {

	private static final long serialVersionUID = 2869499104192205839L;

	@Override
	protected Concealment getWarehouseType() {
		return Concealment.PUBLIC; // This api does not need special permissions for the api key
	}

	@Override
	protected boolean isValid(DownloadType type) {
		return true;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		DownloadRequest request = null;
		try {
			request = getRequest(req);
		} catch (DownloadLimitExceededException e) {
			return status(429, res);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		} catch (Exception e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		}

		try {
			return executeRequest(request);
		} catch (ResultCountException | UnsupportedFormatException | UnsupportedOperationException | NumberFormatException e) {
			// Input data is erroneous; not our fault
			return unloggedUserError400(e, res, req);
		} catch (Exception e) {
			// Unknown error; our fault
			return loggedSystemError500(e, this.getClass(), res, req);
		}
	}

	private static final Set<DownloadInclude> DEFAULT_INCLUDES;
	static {
		DEFAULT_INCLUDES = new HashSet<>();
		DEFAULT_INCLUDES.add(DownloadInclude.DOCUMENT_FACTS);
		DEFAULT_INCLUDES.add(DownloadInclude.GATHERING_FACTS);
		DEFAULT_INCLUDES.add(DownloadInclude.UNIT_FACTS);
		DEFAULT_INCLUDES.add(DownloadInclude.UNIT_MEDIA);
	}

	@Override
	protected Set<DownloadInclude> getDownloadIncludes(HttpServletRequest req) {
		return DEFAULT_INCLUDES;
	}

	@Override
	protected DownloadFormat getDownloadFormat(HttpServletRequest req) {
		return DownloadFormat.TSV_FLAT;
	}

	@Override
	protected ResponseData executeRequest(DownloadRequest request) throws Exception {
		DAO dao = getDao();

		dao.logMessage(Const.LAJI_ETL_QNAME, PyhaApprovalRequestAPI.class, "Received request: " + request + ". Getting count ...");
		long totalResults =  dao.getPrivateVerticaDAO().getQueryDAO().getCount(new CountQuery(request)).getTotal(); // Validates query params
		if (totalResults > DownloadRequestAPI.MAX_ROW_COUNT) throw new ResultCountException("Too many results: " + totalResults + ". Maximum amount is " + MAX_ROW_COUNT);
		request.setApproximateResultSize(totalResults);

		Collection<CollectionAndReasons> collectionReasons = getCollectionsAndReasons(request, dao);

		sendRequestForApproval(request, collectionReasons, dao);

		return writeResponse(request, totalResults, null);
	}

	private void sendRequestForApproval(DownloadRequest request, Collection<CollectionAndReasons> collectionReasons, DAO dao) {
		JSONObject json = DownloadRequestToJSON.toJSON(request, collectionReasons, dao);
		dao.logMessage(Const.LAJI_ETL_QNAME, PyhaApprovalRequestAPI.class, json.beautify());
		if (!getConfig().developmentMode()) {
			dao.sendDownloadRequestForApproval(json);
		}
	}

	private Collection<CollectionAndReasons> getCollectionsAndReasons(DownloadRequest request, DAO dao) throws Exception {
		VerticaDimensionsDAO dimensionsDAO = dao.getVerticaDimensionsDAO();
		List<AggregateRow> rows = getCollectionReasonAggregateData(request, dao.getPrivateVerticaDAO().getQueryDAO());
		Map<Qname, CollectionAndReasons> collectionReasons = new LinkedHashMap<>();
		for (AggregateRow row : rows) {
			Object reason = row.getAggregateByValues().get(1);
			Qname collectionId = getCollectionId(dimensionsDAO, row);
			if (!collectionReasons.containsKey(collectionId)) {
				collectionReasons.put(collectionId, new CollectionAndReasons(collectionId));
			}
			CollectionAndReasons c = collectionReasons.get(collectionId);
			try {
				if (given(reason)) {
					c.addCount(getSecureReason(dimensionsDAO, reason), row.getCount().intValue());
				}
			} catch (Exception e) {
				dao.logError(Const.LAJI_ETL_QNAME, PyhaApprovalRequestAPI.class, "Failed secure reason " + reason, e);
			}
		}
		return collectionReasons.values();
	}

	private SecureReason getSecureReason(VerticaDimensionsDAO dimensionsDAO, Object reason) {
		return SecureReason.valueOf(ResponseUtil.getValue(reason, "document.secureReasons", dimensionsDAO));
	}

	private List<AggregateRow> getCollectionReasonAggregateData(DownloadRequest request, VerticaQueryDAO queryDAO) throws Exception {
		AggregatedQuery aggregatedQuery = new AggregatedQuery(request, new AggregateBy("document.collectionId", "document.secureReasons"), 1, 10000).setExludeNulls(true);
		return queryDAO.getRawAggregate(aggregatedQuery);
	}

}
