package fi.laji.datawarehouse.query.download.util;

import java.io.Closeable;

public interface File extends Closeable {

	public boolean isEmpty();

	public void write(String line);

	public String getName();

	public java.io.File getPhysicalFile();

}
