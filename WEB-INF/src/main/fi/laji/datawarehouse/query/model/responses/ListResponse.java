package fi.laji.datawarehouse.query.model.responses;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;

public class ListResponse extends PageableResponse<ListResponse> {

	private final List<JoinedRow> results;

	public ListResponse(long totalResults, int currentPage, int pageSize, List<JoinedRow> results) {
		super(totalResults, currentPage, pageSize);
		this.results = results;
	}

	@FieldDefinition(order=2)
	public List<JoinedRow> getResults() {
		return results;
	}

}
