package fi.laji.datawarehouse.query.model.queries;

public class ListQuery extends PageableBaseQuery {

	private Selected selected;
	private OrderBy orderBy;

	public ListQuery(BaseQuery baseQuery, int currentPage, int pageSize) {
		super(baseQuery, currentPage, pageSize);
	}

	public OrderBy getOrderBy() {
		return orderBy;
	}

	public ListQuery setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
		return this;
	}

	public Selected getSelected() {
		return selected;
	}

	public ListQuery setSelected(Selected selected) {
		if (!selected.hasValues()) throw new IllegalStateException("Must select some fields");
		this.selected = selected;
		return this;
	}

	@Override
	public String toString() {
		return super.toString() + " ListQuery [selected=" + selected + ", orderBy=" + orderBy + "]";
	}

	@Override
	public String toComparisonString() {
		return super.toComparisonString() + " ListQuery [selected=" + selected + ", orderBy=" + orderBy + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof ListQuery)) throw new UnsupportedOperationException("Can not compare to " + o.getClass());
		ListQuery other = (ListQuery) o;
		return this.toComparisonString().equals(other.toComparisonString());
	}

	@Override
	public int hashCode() {
		return this.toComparisonString().hashCode();
	}

}
