package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.taxonomy.Taxon;

public class DocumentSelectableFields extends UnitSelectableFields {

	public DocumentSelectableFields() {
		super(initFields(), Base.DOCUMENT);
	}

	private static Set<String> initFields() {
		Set<String> fields = initCommon(Base.DOCUMENT, Taxon.class, false);
		return Collections.unmodifiableSet(fields);
	}

}
