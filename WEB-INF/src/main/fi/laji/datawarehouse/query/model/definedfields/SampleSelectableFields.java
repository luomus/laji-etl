package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.taxonomy.Taxon;

public class SampleSelectableFields extends UnitSelectableFields {

	public SampleSelectableFields() {
		super(initFields(), Base.SAMPLE);
	}

	private static Set<String> initFields() {
		Set<String> fields = initCommon(Base.UNIT, Taxon.class, false);
		fields.addAll(getClassFields(Sample.class, false, null, Base.UNIT));
		return Collections.unmodifiableSet(fields);
	}

}
