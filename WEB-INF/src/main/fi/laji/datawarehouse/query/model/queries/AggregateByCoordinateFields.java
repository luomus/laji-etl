package fi.laji.datawarehouse.query.model.queries;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.utils.Utils;

public class AggregateByCoordinateFields {

	public static class CoordinateField {
		private final String fieldName;
		private final int index;
		public CoordinateField(String fieldName, int index) {
			this.fieldName = fieldName;
			this.index = index;
		}
		public String getFieldName() {
			return fieldName;
		}
		public int getIndex() {
			return index;
		}
	}

	private static final Set<String> COORD_SUFFIXES = Utils.set("lat", "lon", "latMin", "latMax", "lonMin", "lonMax");
	private final Map<String, Integer> fields = new LinkedHashMap<>();
	private final Map<String, String> suffixToField = new LinkedHashMap<>();

	public AggregateByCoordinateFields(AggregateBy aggregateBy) {
		int i = 0;
		for (String field : aggregateBy.getFields()) {
			addFieldCandidate(field, i++);
		}
	}

	public boolean contains(String fieldName) {
		return fields.containsKey(fieldName);
	}

	private void addFieldCandidate(String fieldName, int index) {
		String[] parts = fieldName.split(Pattern.quote("."));
		String suffix = parts[parts.length-1];
		if (COORD_SUFFIXES.contains(suffix)) {
			fields.put(fieldName, index);
			suffixToField.put(suffix, fieldName);
		}
	}

	public int count() {
		return fields.size();
	}

	public void validate() throws IllegalArgumentException  {
		Set<String> coordinateTypes = new LinkedHashSet<>();
		for (String field : fields.keySet()) {
			for (Coordinates.Type type : Coordinates.Type.values()) {
				if (field.contains(type.name().toLowerCase())) {
					coordinateTypes.add(type.name());
				}
			}
		}
		if (coordinateTypes.isEmpty()) throw new IllegalArgumentException("To form GeoJSON response you must define " + Const.CRS + " and " + Const.FEATURE_TYPE + " parameters OR define two lat/lon or four latMin/latMax/lonMin/lonMax fields in " + Const.AGGREGATE_BY);
		if (coordinateTypes.size() > 1) throw new IllegalArgumentException("Can not mix different coordinate types " + coordinateTypes);

		Set<String> suffixes = suffixToField.keySet();
		if (fields.size() == 2) {
			if (suffixes.contains("lat") && suffixes.contains("lon")) {
				return;
			}
		} else if (fields.size() == 4) {
			if (suffixes.contains("latMin") && suffixes.contains("latMax") && suffixes.contains("lonMin") && suffixes.contains("lonMax")) {
				return;
			}
		}
		throw new IllegalArgumentException("Must contain two or four coordinate fields (lat,lon) or (latMin,latMax,lonMin,lonMax), contains " + suffixes);
	}

	public CoordinateField getLat() {
		return get("lat");
	}

	public CoordinateField getLon() {
		return get("lon");
	}

	public CoordinateField getLatMin() {
		return get("latMin");
	}

	public CoordinateField getLatMax() {
		return get("latMax");
	}

	public CoordinateField getLonMin() {
		return get("lonMin");
	}

	public CoordinateField getLonMax() {
		return get("lonMax");
	}

	private CoordinateField get(String suffix) {
		String fieldName = suffixToField.get(suffix);
		int index = fields.get(fieldName);
		return new CoordinateField(fieldName, index);
	}

}