package fi.laji.datawarehouse.query.model;

import java.util.List;

public class FilterInfo {

	private final String name;
	private final FilterDefinition info;
	private final Class<?> type;
	private final List<String> enumerationValues;

	public FilterInfo(String name, FilterDefinition info, Class<?> type, List<String> enumerationValues) {
		this.name = name;
		this.info = info;
		this.type = type;
		this.enumerationValues = enumerationValues;
	}

	public String getName() {
		return name;
	}

	public FilterDefinition getInfo() {
		return info;
	}

	public Class<?> getType() {
		return type;
	}

	public List<String> getEnumerationValues() {
		return enumerationValues;
	}

}