package fi.laji.datawarehouse.query.model.responses;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;

public class PageableResponse<T extends PageableResponse<T>> extends BaseResponse<T> {

	private final int currentPage;
	private final int pageSize;
	private final int lastPage;

	public PageableResponse(long totalResults, int currentPage, int pageSize) {
		super(totalResults);
		if (totalResults == 0 && currentPage > 1) throw new IllegalArgumentException("Last page +1 was reached!");
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		this.lastPage = lastPage(totalResults, pageSize);
	}

	private int lastPage(long totalResults, int pageSize) {
		int lastPage = (int) Math.ceil( (double) totalResults / pageSize);
		if (lastPage == 0) lastPage = 1;
		return lastPage;
	}

	@FieldDefinition(order=0.1)
	public int getCurrentPage() {
		return currentPage;
	}

	@FieldDefinition(order=0.2)
	public Integer getPrevPage() {
		if (currentPage > 1) {
			return currentPage - 1;
		}
		return null;
	}

	@FieldDefinition(order=0.3)
	public Integer getNextPage() {
		if (currentPage != lastPage) {
			return currentPage + 1;
		}
		return null;
	}

	@FieldDefinition(order=0.4)
	public int getLastPage() {
		return lastPage;
	}

	@FieldDefinition(order=0.5)
	public int getPageSize() {
		return pageSize;
	}

}
