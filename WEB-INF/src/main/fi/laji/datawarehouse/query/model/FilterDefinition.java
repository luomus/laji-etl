package fi.laji.datawarehouse.query.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FilterDefinition {

	public enum Type { STRING, ENUMERATION, RESOURCE }

	String description();
	double order() default Integer.MAX_VALUE;
	String labelFi();
	String labelEn();
	String labelSv();
	Type type();
	String resourceName() default "null";
	String defaultValue() default "null";
	Base base();
	boolean usableInStatistics() default false;
	boolean useableInPublic() default true;
	String parameterSeparator() default ",";
	boolean taxonStatusFilter() default false;

}