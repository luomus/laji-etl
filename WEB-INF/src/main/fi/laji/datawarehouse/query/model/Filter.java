package fi.laji.datawarehouse.query.model;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class Filter {

	private final String fieldName;
	private final Object value;
	private final Filters parent;

	public Filter(String fieldName, Object value, Filters parent) {
		this.fieldName = fieldName;
		this.value = value;
		this.parent = parent;
	}

	public String getFieldName() {
		return fieldName;
	}

	@SuppressWarnings("unchecked")
	public Set<Qname> getQnames() {
		return (Set<Qname>) value;
	}

	@SuppressWarnings("unchecked")
	public Set<String> getStrings() {
		return (Set<String>) value;
	}

	@SuppressWarnings("unchecked")
	public Set<Integer> getIntegers() {
		return (Set<Integer>) value;
	}

	@SuppressWarnings("unchecked")
	public Set<Long> getLongs() {
		return (Set<Long>) value;
	}

	public Qname getQname() {
		return (Qname) value;
	}

	public int getInt() {
		return ((Integer) value).intValue();
	}

	public long getLong() {
		return ((Long) value).longValue();
	}

	@SuppressWarnings("unchecked")
	public Set<Enum<?>> getEnums() {
		return (Set<Enum<?>>) value;
	}

	public Date getDate() {
		return (Date) value;
	}

	@SuppressWarnings("unchecked")
	public Set<CoordinatesWithOverlapRatio> getCoordinates() {
		return (Set<CoordinatesWithOverlapRatio>) value;
	}

	@SuppressWarnings("unchecked")
	public Set<SingleCoordinates> getSingleCoordinates() {
		return (Set<SingleCoordinates>) value;
	}

	public PolygonSearch getPolygon() {
		return (PolygonSearch) value;
	}

	public PolygonIdSearch getPolygonId() {
		return (PolygonIdSearch) value;
	}

	public Boolean getBoolean() {
		return (Boolean) value;
	}

	@SuppressWarnings("unchecked")
	public Set<CollectionAndRecordQuality> getCollectionAndRecordQualities() {
		return (Set<CollectionAndRecordQuality>) value;
	}

	public Partition getPartition() {
		return (Partition) value;
	}

	public String valueToString() {
		if (value instanceof Date) {
			return DateUtils.format((Date) value, "yyyy-MM-dd");
		}
		if (Collection.class.isAssignableFrom(value.getClass())) {
			Collection<?> col = (Collection<?>) value;
			return col.stream().map(o->o.toString()).collect(Collectors.joining(getDefinition().parameterSeparator()));
		}
		return value.toString();
	}

	@Override
	public String toString() {
		if (value instanceof Date) {
			return fieldName + ": " + DateUtils.format((Date) value, "yyyy-MM-dd");
		}
		if (Collection.class.isAssignableFrom(value.getClass())) {
			Collection<?> col = (Collection<?>) value;
			return fieldName + ":[" + col.stream().map(o->o.toString()).collect(Collectors.joining(getDefinition().parameterSeparator()))+"]";
		}
		return fieldName + ":" + value;
	}

	public Object getValue() {
		return value;
	}

	public Enum<?> getEnum() {
		return (Enum<?>) value;
	}

	public Base getBase() {
		return parent.base();
	}

	public Filters getParent() {
		return parent;
	}

	public FilterDefinition getDefinition() {
		return Filters.filterDefinition(fieldName);
	}

}
