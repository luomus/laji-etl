package fi.laji.datawarehouse.query.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.atlas.json.JsonArray;

import com.google.common.math.DoubleMath;

import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.query.model.queries.AggregateByCoordinateFields;
import fi.laji.datawarehouse.query.model.queries.AggregateByCoordinateFields.CoordinateField;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.BaseResponse;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.laji.datawarehouse.query.model.responses.PageableResponse;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class ResponseUtil {

	private static final Map<String, Double> COORDINATE_SCALES = initCoordinateScales();

	private static Map<String, Double> initCoordinateScales() {
		Map<String, Double> scales = new HashMap<>();
		scales.put("gathering.conversions.ykj1km.lat", 1.0);
		scales.put("gathering.conversions.ykj1km.lon", 1.0);
		scales.put("gathering.conversions.ykj10km.lat", 1.0);
		scales.put("gathering.conversions.ykj10km.lon", 1.0);
		scales.put("gathering.conversions.ykj50km.lat", 5.0);
		scales.put("gathering.conversions.ykj50km.lon", 5.0);
		scales.put("gathering.conversions.ykj100km.lat", 1.0);
		scales.put("gathering.conversions.ykj100km.lon", 1.0);

		scales.put("gathering.conversions.ykj1kmCenter.lat", 1.0);
		scales.put("gathering.conversions.ykj1kmCenter.lon", 1.0);
		scales.put("gathering.conversions.ykj10kmCenter.lat", 1.0);
		scales.put("gathering.conversions.ykj10kmCenter.lon", 1.0);
		scales.put("gathering.conversions.ykj50kmCenter.lat", 5.0);
		scales.put("gathering.conversions.ykj50kmCenter.lon", 5.0);
		scales.put("gathering.conversions.ykj100kmCenter.lat", 1.0);
		scales.put("gathering.conversions.ykj100kmCenter.lon", 1.0);

		scales.put("gathering.conversions.wgs84Grid1.lat", 1.0);
		scales.put("gathering.conversions.wgs84Grid1.lon", 1.0);
		scales.put("gathering.conversions.wgs84Grid05.lat", 0.5);
		scales.put("gathering.conversions.wgs84Grid05.lon", 0.5);
		scales.put("gathering.conversions.wgs84Grid01.lat", 0.1);
		scales.put("gathering.conversions.wgs84Grid01.lon", 0.1);
		scales.put("gathering.conversions.wgs84Grid005.lat", 0.05);
		scales.put("gathering.conversions.wgs84Grid005.lon", 0.05);

		scales.put("gathering.conversions.wgs84CenterPoint.lat", 0.0);
		scales.put("gathering.conversions.wgs84CenterPoint.lon", 0.0);
		scales.put("gathering.conversions.eurefCenterPoint.lat", 0.0);
		scales.put("gathering.conversions.eurefCenterPoint.lon", 0.0);
		return scales;
	}

	private final VerticaDimensionsDAO dao;

	public ResponseUtil() {
		this.dao = null;
	}

	public ResponseUtil(VerticaDimensionsDAO dao) {
		this.dao = dao;
	}

	public JSONObject buildAggregateResponse(AggregateResponse response, AggregatedQuery query) {
		JSONObject json = new JSONObject();
		setPages(json, response);
		JSONArray results = new JSONArray();
		for (AggregateRow row : response.getResults()) {
			results.appendObject(buildAggregate(query, row));
		}
		json.setArray("results", results);
		return json;
	}

	private <T extends PageableResponse<T>> void setPages(JSONObject json, PageableResponse<T> response) {
		json.setInteger("currentPage", response.getCurrentPage());
		if (response.getPrevPage() != null) {
			json.setInteger("prevPage", response.getPrevPage());
		}
		if (response.getNextPage() !=  null) {
			json.setInteger("nextPage", response.getNextPage());
		}
		json.setInteger("lastPage", response.getLastPage());
		json.setInteger("pageSize", response.getPageSize());
		json.setInteger("total", Math.toIntExact(response.getTotal()));
		setCacheTime(response, json);
	}

	public JSONObject buildCountResponse(CountResponse response) {
		JSONObject json = new JSONObject();
		json.setInteger("total", Math.toIntExact(response.getTotal()));
		setCacheTime(response, json);
		return json;
	}

	private <T extends BaseResponse<T>> void setCacheTime(BaseResponse<T> response, JSONObject json) {
		if (response.getCacheTimestamp() != null) {
			json.setInteger("cacheTimestamp", response.getCacheTimestamp().intValue());
		}
	}

	private JSONObject buildAggregate(AggregatedQuery query, AggregateRow row) {
		JSONObject result = new JSONObject();
		int i = 0;
		for (String field : query.getAggregateBy().getFields()) {
			Object value = row.getAggregateByValues().get(i++);
			String detailedValue = getValue(value, field);
			result.getObject("aggregateBy").setString(field, detailedValue);
		}
		setAggregateFunctions(row, result);
		return result;
	}

	private void setAggregateFunctions(AggregateRow row, JSONObject json) {
		json.setInteger(Const.COUNT, row.getCount().intValue());
		if (row.getIndividualCountSum() != null)
			json.setInteger(Const.INDIVIDUAL_COUNT_SUM, row.getIndividualCountSum().intValue());
		if (row.getIndividualCountMax() != null)
			json.setInteger(Const.INDIVIDUAL_COUNT_MAX, row.getIndividualCountMax().intValue());
		if (row.getOldestRecord() != null)
			json.setString(Const.OLDEST_RECORD, DateUtils.format(row.getOldestRecord(), Const.YYYY_MM_DD));
		if (row.getNewestRecord() != null)
			json.setString(Const.NEWEST_RECORD, DateUtils.format(row.getNewestRecord(), Const.YYYY_MM_DD));
		if (row.getPairCountSum() != null)
			json.setInteger(Const.PAIR_COUNT_SUM, row.getPairCountSum().intValue());
		if (row.getPairCountMax() != null)
			json.setInteger(Const.PAIR_COUNT_MAX, row.getPairCountMax().intValue());
		if (row.getLineLengthSum() != null)
			json.setInteger(Const.LINELENGTH_SUM, row.getLineLengthSum().intValue());
		if (row.getFirstLoadDateMin() != null)
			json.setString(Const.FIRST_LOAD_DATE_MIN, DateUtils.format(row.getFirstLoadDateMin(), Const.YYYY_MM_DD));
		if (row.getFirstLoadDateMax() != null)
			json.setString(Const.FIRST_LOAD_DATE_MAX, DateUtils.format(row.getFirstLoadDateMax(), Const.YYYY_MM_DD));
		if (row.getTaxonCount() != null)
			json.setInteger(Const.TAXON_COUNT, row.getTaxonCount().intValue());
		if (row.getSpeciesCount() != null)
			json.setInteger(Const.SPECIES_COUNT, row.getSpeciesCount().intValue());
		if (row.getAtlasCodeMax() != null)
			json.setString(Const.ATLAS_CODE_MAX, row.getAtlasCodeMax());
		if (row.getAtlasClassMax() != null)
			json.setString(Const.ATLAS_CLASS_MAX, row.getAtlasClassMax());
		if (row.getRecordQualityMax() != null)
			json.setString(Const.RECORD_QUALITY_MAX, recordQuality(row));
		if (row.getRedListStatusMax() != null)
			json.setString(Const.RED_LIST_STATUS_MAX, row.getRedListStatusMax());
		if (row.getGatheringCount() != null)
			json.setInteger(Const.GATHERING_COUNT, row.getGatheringCount().intValue());
		if (row.getSecuredCount() != null) {
			json.setInteger(Const.SECURED_COUNT, row.getSecuredCount().intValue());
		}
	}

	private String recordQuality(AggregateRow row) {
		return getValue(row.getRecordQualityMax(), "unit.interpretations.recordQuality");
	}

	public JSONObject buildGeoJsonResponse(AggregateResponse response, AggregatedQuery query) {
		JSONObject json = new JSONObject().setString("type", "FeatureCollection");
		setPages(json, response);
		JSONArray features = json.getArray("features");
		AggregateByCoordinateFields coordinateFields = query.getAggregateBy().getCoordinateFields();
		for (AggregateRow row : response.getResults()) {
			JSONObject feature = toFeature(row, query.getAggregateBy().getFields(), coordinateFields);
			if (feature != null) {
				features.appendObject(feature);
			}
		}
		return json;
	}

	public static String getValue(Object value, String field, VerticaDimensionsDAO dao) {
		if (value == null) return "";
		if (value instanceof Long) {
			return dao.getIdForKey((Long) value, field);
		}
		if (value instanceof java.sql.Date) {
			return DateUtils.sqlDateToString((java.sql.Date) value, Const.YYYY_MM_DD);
		}
		return value.toString();
	}

	private String getValue(Object value, String field) {
		return getValue(value, field, dao);
	}

	private JSONObject toFeature(AggregateRow row, List<String> aggregateBy, AggregateByCoordinateFields coordinateFields) {
		JSONObject feature = new JSONObject().setString("type", "Feature");
		JSONObject geometry = toGeometry(row, coordinateFields);
		if (geometry == null) return null;
		feature.setObject("geometry", geometry);

		JSONObject properties = feature.getObject("properties");
		int i = 0;
		for (String field : aggregateBy) {
			if (!coordinateFields.contains(field)) {
				Object value = row.getAggregateByValues().get(i);
				String detailedValue = getValue(value, field);
				properties.setString(field, detailedValue);
			}
			i++;
		}
		setAggregateFunctions(row, properties);
		return feature;
	}

	private JSONObject toGeometry(AggregateRow row, AggregateByCoordinateFields coordinateFields) {
		if (coordinateFields.count() == 2) {
			CoordinateField latField = coordinateFields.getLat();
			CoordinateField lonField = coordinateFields.getLon();
			Double lat = (Double) row.getAggregateByValues().get(latField.getIndex());
			Double lon = (Double) row.getAggregateByValues().get(lonField.getIndex());
			return toPolygonGeometry(latField, lat, lonField, lon);
		}
		if (coordinateFields.count() == 4) {
			CoordinateField latMinField = coordinateFields.getLatMin();
			CoordinateField latMaxField = coordinateFields.getLatMax();
			CoordinateField lonMinField = coordinateFields.getLonMin();
			CoordinateField lonMaxField = coordinateFields.getLonMax();
			Double latMin = (Double) row.getAggregateByValues().get(latMinField.getIndex());
			Double latMax = (Double) row.getAggregateByValues().get(latMaxField.getIndex());
			Double lonMin = (Double) row.getAggregateByValues().get(lonMinField.getIndex());
			Double lonMax = (Double) row.getAggregateByValues().get(lonMaxField.getIndex());
			if (latMin == null || latMax == null || lonMin == null || lonMax == null) return null;
			if (same(latMin, latMax) && same(lonMin, lonMax)) {
				return toPointGeometry(latMin, lonMin);
			}
			if (!latMinField.getFieldName().contains("wgs84")) {
				if (latMin == latMax-1 && lonMin == lonMax-1) {
					return toPointGeometry(latMin, lonMin);
				}
			}
			return toPolygonGeometry(latMin, latMax, lonMin, lonMax);
		}
		throw new IllegalStateException("Impossible state " + coordinateFields.count());
	}

	private boolean same(double d1, double d2) {
		return DoubleMath.fuzzyEquals(d1, d2, 0.000001);
	}

	private JSONObject toPolygonGeometry(CoordinateField latField, Double lat, CoordinateField lonField, Double lon) {
		double latScale = getScale(latField);
		double lonScale = getScale(lonField);
		if (latScale == 0 && lonScale == 0) {
			return toPointGeometry(lat, lon);
		}
		double latMin = lat;
		double latMax = lat + latScale;
		double lonMin = lon;
		double lonMax = lon + lonScale;
		if (latField.getFieldName().contains("ykj")) {
			latMin = Coordinates.toFullYkjLength(latMin);
			latMax = Coordinates.toFullYkjLength(latMax);
			lonMin = Coordinates.toFullYkjLength(lonMin);
			lonMax = Coordinates.toFullYkjLength(lonMax);
		}
		return toPolygonGeometry(latMin, latMax, lonMin, lonMax);
	}

	private double getScale(CoordinateField field) {
		Double scale = COORDINATE_SCALES.get(field.getFieldName());
		if (scale == null) throw new IllegalStateException("No scale defined for " + field.getFieldName());
		return scale;
	}

	private JSONObject toPointGeometry(Double lat, Double lon) {
		if (lat == null || lon == null) return null;
		JSONObject geometry = new JSONObject();
		geometry.setString("type", "Point");
		geometry.setArray("coordinates", toCoordinatePair(lat, lon));
		return geometry;
	}

	private JSONObject toPolygonGeometry(Double latMin, Double latMax, Double lonMin, Double lonMax) {
		if (latMin == null || latMax == null || lonMin == null || lonMax == null) return null;
		JSONObject geometry = new JSONObject();
		geometry.setString("type", "Polygon");
		geometry.setArray("coordinates", new JSONArray().appendArray(toPolygonArray(latMin, latMax, lonMin, lonMax)));
		return geometry;
	}

	private JSONArray toPolygonArray(double latMin, double latMax, double lonMin, double lonMax) {
		JSONArray coordinates = new JSONArray();
		JSONArray first = toCoordinatePair(latMin, lonMin);
		coordinates.appendArray(first);
		coordinates.appendArray(toCoordinatePair(latMin, lonMax));
		coordinates.appendArray(toCoordinatePair(latMax, lonMax));
		coordinates.appendArray(toCoordinatePair(latMax, lonMin));
		coordinates.appendArray(first);
		return coordinates;
	}

	private JSONArray toCoordinatePair(double lat, double lon) {
		return new JSONArray().appendDouble(lon).appendDouble(lat);
	}

	public JSONObject buildListResponse(ListResponse response, ListQuery query) {
		JSONObject json = new JSONObject();
		setPages(json, response);
		JSONArray results = json.getArray("results");
		for (JoinedRow row : response.getResults()) {
			JSONObject selectedJSON = ModelToJson.toSelectedJSON(row, query.getSelected());
			results.appendObject(selectedJSON);
		}
		return json;
	}

	public JSONObject buildGeoJsonResponse(ListResponse response, ListQuery query) {
		JSONObject json = new JSONObject();
		setPages(json, response);
		json.setString("type", "FeatureCollection");
		JSONArray features = json.getArray("features");
		for (JoinedRow row : response.getResults()) {
			JSONObject feature = toFeature(row, query);
			if (feature != null) {
				features.appendObject(feature);
			}
		}
		return json;
	}

	private JSONObject toFeature(JoinedRow row, ListQuery query) {
		JSONObject feature = new JSONObject().setString("type", "Feature");
		JSONObject geometry = toGeometry(row, query);
		if (geometry == null) return null;
		feature.setObject("geometry", geometry);
		feature.setObject("properties", flatten(ModelToJson.toSelectedJSON(row, query.getSelected())));
		return feature;
	}

	private JSONObject flatten(JSONObject original) {
		JSONObject flattened = new JSONObject();
		flatten(flattened, "", original);
		return flattened;
	}

	private void flatten(JSONObject flattened, String path, JSONObject source) {
		for (String key : source.getKeys()) {
			String currentPath = path.isEmpty() ? key : path+"."+key;
			if (source.isArray(key)) {
				flatten(flattened, currentPath, source.getArray(key));
				continue;
			}
			if (source.isObject(key)) {
				flatten(flattened, currentPath, source.getObject(key));
				continue;
			}
			flattened.setRaw(currentPath, source.getRaw(key));
		}
	}

	private void flatten(JSONObject flattened, String path, JSONArray array) {
		int i = 0;
		for (Object o : array.iterateAsRaw()) {
			String currentPath = path + "["+i+"]";
			i++;
			if (o instanceof JSONObject) {
				flatten(flattened, currentPath, (JSONObject)o);
				continue;
			}
			if (o instanceof JsonArray) {
				flatten(flattened, currentPath, (JSONArray)o);
				continue;
			}
			flattened.setRaw(currentPath, o);
		}
	}

	private JSONObject toGeometry(JoinedRow row, ListQuery query) {
		if (row.getGathering() == null) return null;
		if (row.getGathering().getConversions() == null) return null;
		GatheringConversions conversions = row.getGathering().getConversions();
		if (query.getFeatureType() == FeatureType.CENTER_POINT) {
			if (query.getCrs() == Coordinates.Type.WGS84) {
				return toPointGeometry(conversions.getWgs84CenterPoint());
			}
			if (query.getCrs() == Coordinates.Type.EUREF) {
				return toPointGeometry(conversions.getEurefCenterPoint());
			}
			if (query.getCrs() == Coordinates.Type.YKJ) {
				return toPointGeometry(conversions.getYkj());
			}
		}
		if (query.getFeatureType() == FeatureType.ENVELOPE) {
			if (query.getCrs() == Coordinates.Type.WGS84) {
				return toPolygonGeometry(conversions.getWgs84());
			}
			if (query.getCrs() == Coordinates.Type.EUREF) {
				return toPolygonGeometry(conversions.getEuref());
			}
			if (query.getCrs() == Coordinates.Type.YKJ) {
				return toPolygonGeometry(conversions.getYkj());
			}
		}
		if (query.getFeatureType() == FeatureType.ORIGINAL_FEATURE) {
			if (query.getCrs() == Coordinates.Type.WGS84) {
				return geo(conversions.getWgs84Geo());
			}
			if (query.getCrs() == Coordinates.Type.EUREF) {
				return geo(conversions.getEurefGeo());
			}
			if (query.getCrs() == Coordinates.Type.YKJ) {
				return geo(conversions.getYkjGeo());
			}
		}
		throw new UnsupportedOperationException("Not yet implemented for " + Const.FEATURE_TYPE + " " + query.getFeatureType() + " or " + Const.CRS + " " + query.getCrs());
	}

	private JSONObject toPolygonGeometry(Coordinates c) {
		if (c == null) return null;
		return toPolygonGeometry(c.getLatMin(), c.getLatMax(), c.getLonMin(), c.getLonMax());
	}

	private JSONObject toPointGeometry(Coordinates c) {
		if (c == null) return null;
		return toPointGeometry(c.convertCenterPoint());
	}

	private JSONObject toPointGeometry(SingleCoordinates c) {
		if (c == null) return null;
		return toPointGeometry(c.getLat(), c.getLon());
	}

	private JSONObject geo(Geo geo) {
		if (geo == null) return null;
		if (geo.getFeatures().size() == 0) return null;
		try {
			if (geo.getFeatures().size() == 1) {
				return geo.getFeatures().get(0).getGeoJSON();
			}
			return toPolygonGeometry(geo.getBoundingBox());
		} catch (DataValidationException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getRowHeaders(AggregatedQuery query, List<AggregateRow> results) { 
		// If you add more fields to csv/tsv response, remember to add the field here and to List<String> getRowValues()
		List<String> headers = new ArrayList<>(query.getAggregateBy().getFields());
		headers.add(Const.COUNT);
		AggregateRow row = results.get(0);
		if (row.getIndividualCountSum() != null)
			headers.add(Const.INDIVIDUAL_COUNT_SUM);
		if (row.getIndividualCountMax() != null)
			headers.add(Const.INDIVIDUAL_COUNT_MAX);
		if (row.getOldestRecord() != null)
			headers.add(Const.OLDEST_RECORD);
		if (row.getNewestRecord() != null)
			headers.add(Const.NEWEST_RECORD);
		if (row.getPairCountSum() != null)
			headers.add(Const.PAIR_COUNT_SUM);
		if (row.getPairCountMax() != null)
			headers.add(Const.PAIR_COUNT_MAX);
		if (row.getLineLengthSum() != null)
			headers.add(Const.LINELENGTH_SUM);
		if (row.getFirstLoadDateMin() != null)
			headers.add(Const.FIRST_LOAD_DATE_MIN);
		if (row.getFirstLoadDateMax() != null)
			headers.add(Const.FIRST_LOAD_DATE_MAX);
		if (row.getTaxonCount() != null)
			headers.add(Const.TAXON_COUNT);
		if (row.getSpeciesCount() != null)
			headers.add(Const.SPECIES_COUNT);
		if (row.getAtlasCodeMax() != null)
			headers.add(Const.ATLAS_CODE_MAX);
		if (row.getAtlasClassMax() != null)
			headers.add(Const.ATLAS_CLASS_MAX);
		if (row.getRecordQualityMax() != null)
			headers.add(Const.RECORD_QUALITY_MAX);
		if (row.getRedListStatusMax() != null)
			headers.add(Const.RED_LIST_STATUS_MAX);
		if (row.getGatheringCount() != null) {
			headers.add(Const.GATHERING_COUNT);
		} if (row.getSecuredCount() != null) {
			headers.add(Const.SECURED_COUNT);
		}
		return headers;
	}

	public List<String> getRowValues(AggregatedQuery query, AggregateRow row) {
		// If you add more fields to csv/tsv response, remember to add the field here and to List<String> getRowHeaders()
		List<String> values = new ArrayList<>();
		int i = 0;
		for (String field : query.getAggregateBy().getFields()) {
			Object value = row.getAggregateByValues().get(i++);
			String detailedValue = getValue(value, field);
			values.add(detailedValue);
		}
		values.add(String.valueOf(row.getCount().intValue()));
		if (row.getIndividualCountSum() != null)
			values.add(String.valueOf(row.getIndividualCountSum().intValue()));
		if (row.getIndividualCountMax() != null)
			values.add(String.valueOf(row.getIndividualCountMax().intValue()));
		if (row.getOldestRecord() != null)
			values.add(String.valueOf(DateUtils.format(row.getOldestRecord(), Const.YYYY_MM_DD)));
		if (row.getNewestRecord() != null)
			values.add(String.valueOf(DateUtils.format(row.getNewestRecord(), Const.YYYY_MM_DD)));
		if (row.getPairCountSum() != null)
			values.add(String.valueOf(row.getPairCountSum().intValue()));
		if (row.getPairCountMax() != null)
			values.add(String.valueOf(row.getPairCountMax().intValue()));
		if (row.getLineLengthSum() != null)
			values.add(String.valueOf(row.getLineLengthSum().intValue()));
		if (row.getFirstLoadDateMin() != null)
			values.add(String.valueOf(DateUtils.format(row.getFirstLoadDateMin(), Const.YYYY_MM_DD)));
		if (row.getFirstLoadDateMax() != null)
			values.add(String.valueOf(DateUtils.format(row.getFirstLoadDateMax(), Const.YYYY_MM_DD)));
		if (row.getTaxonCount() != null)
			values.add(String.valueOf(row.getTaxonCount().intValue()));
		if (row.getSpeciesCount() != null)
			values.add(String.valueOf(row.getSpeciesCount().intValue()));
		if (row.getAtlasCodeMax() != null)
			values.add(row.getAtlasCodeMax());
		if (row.getAtlasClassMax() != null)
			values.add(row.getAtlasClassMax());
		if (row.getRecordQualityMax() != null)
			values.add(recordQuality(row));
		if (row.getRedListStatusMax() != null)
			values.add(row.getRedListStatusMax());
		if (row.getGatheringCount() != null)
			values.add(String.valueOf(row.getGatheringCount().intValue()));
		if (row.getSecuredCount() != null)
			values.add(String.valueOf(row.getSecuredCount().intValue()));
		return values;
	}

}
