package fi.laji.datawarehouse.query.model;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.utils.Utils;

public class CoordinatesWithOverlapRatio extends Coordinates {

	private Double overlapRatio;

	public CoordinatesWithOverlapRatio(Coordinates coordinates) throws DataValidationException {
		super(coordinates);
	}

	public CoordinatesWithOverlapRatio(double lat, double lon, Type type) throws DataValidationException {
		super(lat, lon, type);
	}

	public CoordinatesWithOverlapRatio(double latMin, double latMax, double lonMin, double lonMax, Type type) throws DataValidationException {
		super(latMin, latMax, lonMin, lonMax, type);
	}

	public Double getOverlapRatio() {
		return overlapRatio;
	}

	public CoordinatesWithOverlapRatio setOverlapRatio(Double overlapRatio) {
		if (overlapRatio != null) {
			if (overlapRatio > 1.0) throw new IllegalArgumentException("Largest allowed overlap ratio is 1.0");
			if (overlapRatio < 0.0) throw new IllegalArgumentException("Smallest allowed overlap ratio is 0.0");
			overlapRatio = Utils.round(overlapRatio, 2);
		}
		this.overlapRatio = overlapRatio;
		return this;
	}

	@Override
	public String toString() {
		return Utils.removeWhitespace(Utils.debugS(getLatMin(), getLatMax(), getLonMin(), getLonMax(), getType(), overlapRatio));
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return toString().equals(obj.toString());
	}

}
