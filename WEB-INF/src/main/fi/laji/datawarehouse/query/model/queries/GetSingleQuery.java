package fi.laji.datawarehouse.query.model.queries;

public class GetSingleQuery extends ListQuery {

	public GetSingleQuery(BaseQuery baseQuery) {
		super(baseQuery, 1, 1);
	}

	@Override
	public String toString() {
		return super.toString() + " GetSingleQuery []";
	}

}