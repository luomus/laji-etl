package fi.laji.datawarehouse.query.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.SwaggerAPIDescriptionGenerator;
import fi.laji.datawarehouse.query.model.FilterDefinition.Type;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.laji.datawarehouse.query.model.definedfields.UnitSelectableFields;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonValidationException;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class Filters {

	private static final String CRS_DESC = SwaggerAPIDescriptionGenerator.CRS_DESC;

	private static final String FACT_FILTER_DESCRIPTION = ""+
			"Format is \"factName=value;otherFact=value\". "+
			"If value is not given (for example just \"factName\"), this filter matches all records that have the given fact. "+
			"If value is a numeric range (for example \"factName=-5.0/-1.5\"), this filter matches all values where the value is between the range (inclusive). "+
			"When multiple fact names are given, this is an AND search. "+
			"For facts that are URIs, you can use full URI or Qname.";

	public static class UnsupportedFilterException extends UnsupportedOperationException {
		private static final long serialVersionUID = -8597729078352623343L;
		public UnsupportedFilterException(String filterName, Base attemptedBase, Base requiredBase) {
			super(filterName + " requires base " + requiredBase + " but was tried to use with base " + attemptedBase);
		}
		public UnsupportedFilterException(String filterName, boolean onlyStatistics) {
			super(filterName + " can not be used with statistics queries, onlyStatistics="+onlyStatistics);
		}
		public UnsupportedFilterException(String filterName, Concealment warehouse) {
			super(filterName + " can not be used with " +  warehouse  + " warehouse query");
		}
		public UnsupportedFilterException(String filterName) {
			super("Unknown parameter: " + filterName);
		}
		public UnsupportedFilterException(String parameter, Throwable e) {
			super("Invalid " + parameter + " value: " + e.getMessage(), e);
		}
	}

	public static enum QualityIssues { NO_ISSUES, BOTH, ONLY_ISSUES }

	public static enum Wild { WILD, WILD_UNKNOWN, NON_WILD }

	public static enum Operator { AND, OR }

	private final Base base;
	private final boolean onlyStatistics;
	private final Concealment warehouse;
	private final boolean canUseEditorOrObserverIdIsNot;

	private Filters(Concealment warehouse, Base base, boolean onlyStatistics, boolean setDefaults, boolean canUseEditorOrObserverIdIsNot) {
		this.warehouse = warehouse;
		this.base = base;
		this.onlyStatistics = onlyStatistics;
		this.canUseEditorOrObserverIdIsNot = canUseEditorOrObserverIdIsNot;

		if (setDefaults) {
			try {
				setDefaults(base);
			} catch (Exception e) {
				throw new IllegalStateException("Set defaults", e);
			}
		}
	}

	private void setDefaults(Base base) {
		if (base == Base.UNIT || base == Base.UNIT_MEDIA) {
			setUnitDefaults();
			return;
		}
		if (base == Base.ANNOTATION) {
			setAnnotationDefaults();
			return;
		}
		if (base == Base.SAMPLE) {
			setSampleDefaults();
			return;
		}
		if (base == Base.DOCUMENT || base == Base.GATHERING) {
			setDocumentDefaults();
			return;
		}
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	private void setUnitDefaults() {
		setIncludeNonValidTaxa(true);
		setUseIdentificationAnnotations(true);
		setIncludeSubTaxa(true);
		setIndividualCountMin(1);
		setQualityIssues(QualityIssues.NO_ISSUES);
		setWild(Wild.WILD);
		setWild(Wild.WILD_UNKNOWN);
	}

	private void setAnnotationDefaults() {
		setIncludeNonValidTaxa(true);
		setUseIdentificationAnnotations(true);
		setIncludeSubTaxa(true);
		setIncludeSystemAnnotations(false);
	}

	private void setSampleDefaults() {
		setIncludeNonValidTaxa(true);
		setUseIdentificationAnnotations(true);
		setIncludeSubTaxa(true);
	}

	private void setDocumentDefaults() {
		setQualityIssues(QualityIssues.NO_ISSUES);
	}

	private static final Set<String> LEGACY_IGNORE = Utils.set(
			"reliable",
			"taxonReliability",
			"reliabilityOfCollection"
			);

	public Filters addParameters(Map<String, String[]> parameters) throws Exception {
		if (parameters == null) return this;
		if (parameters.containsKey("wild")) {
			this.wild = null; // Clear default values
		}
		for (Map.Entry<String, String[]> e : parameters.entrySet()) {
			String parameter = e.getKey();
			String[] values = e.getValue();

			// TODO remove legacy support
			if (LEGACY_IGNORE.contains(parameter)) {
				if ("taxonReliability".equals(parameter)) {
					values = legacyTaxonReliabilityValues(values);
					parameter = "reliability";
				} else {
					continue;
				}
			}

			parseParameter(parameter, values);
		}
		return this;
	}

	public void addFilter(String contents) {
		try {
			if (contents == null) return;
			int index = contents.indexOf(":");
			String parameter = contents.substring(0, index);
			String values = contents.substring(index+1).trim();
			if (values.startsWith("[") && values.endsWith("]")) {
				values = values.substring(0, values.length()-1).substring(1);
			}
			parseParameter(parameter, values);
		} catch (UnsupportedFilterException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new ETLException("Add filter " + contents, e);
		}
	}

	private String[] legacyTaxonReliabilityValues(String[] values) {
		String[] replacedValues = new String[values.length];
		int i = 0;
		for (String value : values) {
			if ("RELIABLE".equals(value)) values[i] = Reliability.RELIABLE.toString();
			if ("LIKELY".equals(value)) values[i] = Reliability.RELIABLE.toString();
			if ("NEUTRAL".equals(value)) values[i] = Reliability.UNDEFINED.toString();
			if ("SUSPICIOUS".equals(value)) values[i] = Reliability.UNRELIABLE.toString();
			if ("UNRELIABLE".equals(value)) values[i] = Reliability.UNRELIABLE.toString();
			i++;
		}
		return replacedValues;
	}
	public static Filters createFilters(Concealment warehouse, Base base, boolean onlyStatistics, boolean setDefaults) {
		return new Filters(warehouse, base, onlyStatistics, setDefaults, false);
	}
	public static Filters createFilters(Concealment warehouse, Base base, boolean onlyStatistics, boolean setDefaults, boolean canUseEditorOrObserverIdIsNot) {
		return new Filters(warehouse, base, onlyStatistics, setDefaults, canUseEditorOrObserverIdIsNot);
	}

	public static Filters createDefaultFilters(Concealment warehouse, Base base, boolean onlyStatistics) {
		return createFilters(warehouse, base, onlyStatistics, true);
	}
	public static Filters createDefaultFilters(Concealment warehouse, Base base, boolean onlyStatistics, boolean canUseEditorOrObserverIdIsNot) {
		return createFilters(warehouse, base, onlyStatistics, true, canUseEditorOrObserverIdIsNot);
	}

	public static Filters createEmptyFilters(Concealment warehouse, Base base, boolean onlyStatistics) {
		return createFilters(warehouse, base, onlyStatistics, false);
	}
	public static Filters createEmptyFilters(Concealment warehouse, Base base, boolean onlyStatistics, boolean canUseEditorOrObserverIdIsNot) {
		return createFilters(warehouse, base, onlyStatistics, false, canUseEditorOrObserverIdIsNot);
	}

	@FilterDefinition(order=1.1, base=Base.UNIT, usableInStatistics=true,
			labelFi="Taksoni",
			labelEn="Taxon",
			labelSv="Taxon",
			type=Type.RESOURCE,
			resourceName="taxa",
			description="Filter based on URI or Qname identifier of a taxon. Use Taxonomy-API to find identifiers.")
	private Set<Qname> taxonId;

	@FilterDefinition(order=1.2, base=Base.UNIT, usableInStatistics=true,
			labelFi="Kohde (laji)",
			labelEn="Target (taxon)",
			labelSv="Föremål (taxon)",
			type=Type.STRING,
			description="Same as taxonId, but system resolves identifier of the taxon based on the given target name. If no such match can be resolved (name does not exist in taxonomy), will filter based on the given verbatim target name (case insensitive).")
	private Set<String> target;

	@FilterDefinition(order=2.1, base=Base.UNIT, usableInStatistics=true,
			labelFi="Käytä laadunvalvonnan määrityksiä",
			labelEn="Use quality control identifications",
			labelSv="Använd identifiering av kvalitetskontroll",
			type=Type.STRING,
			defaultValue="true",
			description="By default, all taxon linking related filters use taxon linking that may have been altered because of quality control identification annotations. If you want to use original user identifications, set this to false.")
	private Boolean useIdentificationAnnotations;

	@FilterDefinition(order=2.2, base=Base.UNIT, usableInStatistics=true,
			labelFi="Sisällytä alataksonit",
			labelEn="Include subtaxa",
			labelSv="Inkludera subtaxa",
			type=Type.STRING,
			defaultValue="true",
			description="By default, all taxon linking related filters return all entries that belong to the filtered taxa. To return only exact matches (no subtaxa), set this to false.")
	private Boolean includeSubTaxa;

	@FilterDefinition(order=2.31, base=Base.UNIT, usableInStatistics=true,
			labelFi="Myös linkittämättömät kohteet",
			labelEn="Also unlinked targets",
			labelSv="Också olänkade föremål",
			defaultValue="true",
			type=Type.STRING,
			description="Set to false if you want to include only those entries where reported target name can be linked with a taxon of the reference taxonomy. By default includes all entries.")
	private Boolean includeNonValidTaxa;

	@FilterDefinition(order=2.32, base=Base.UNIT, usableInStatistics=true,
			labelFi="Vain linkittämättömät kohteet",
			labelEn="Only unlinked targets",
			labelSv="Endast olänkade föremål",
			type=Type.STRING,
			description="Set to true if you want to include only those entries where reported target name can not be linked with a taxon of the reference taxonomy. By default includes all entries.")
	private Boolean onlyNonValidTaxa;

	@FilterDefinition(order=3.01, base=Base.UNIT, usableInStatistics=true,
			labelFi="Lajiryhmä",
			labelEn="Informal taxon group",
			labelSv="Organismgrupp",
			type=Type.RESOURCE,
			resourceName="informal-taxon-groups",
			description="Filter based on URI or Qname identifier of an informal taxon group. Use InformalTaxonGroups-API to find identifiers. Will return entries that have been linked with taxa that belong to one of the given groups.")
	private Set<Qname> informalTaxonGroupId;

	@FilterDefinition(order=3.02, base=Base.UNIT, usableInStatistics=true,
			labelFi="Poissuljettu lajiryhmä",
			labelEn="Excluded informal taxon group",
			labelSv="Utesluten organismgrupp",
			type=Type.RESOURCE,
			resourceName="informal-taxon-groups",
			description="Exclude based on URI or Qname identifier of an informal taxon group. Use InformalTaxonGroups-API to find identifiers. Will exclude entries that have been linked with taxa that belong to any of the given groups.")
	private Set<Qname> informalTaxonGroupIdNot;

	@FilterDefinition(order=3.03, base=Base.UNIT, usableInStatistics=true,
			labelFi="Lajiryhmä",
			labelEn="Informal taxon group",
			labelSv="Organismgrupp",
			type=Type.RESOURCE,
			resourceName="informal-taxon-groups",
			description="Filter based on URI or Qname identifier of an informal taxon group. Use InformalTaxonGroups-API to find identifiers. Will return entries that have been linked with taxa that belong to one of the given groups OR reported to belong to one of the given groups.")
	private Set<Qname> informalTaxonGroupIdIncludingReported;

	@FilterDefinition(order=3.031, base=Base.UNIT,
			labelFi="Lajin hallinnollinen rajaus",
			labelEn="Administrative status of species",
			labelSv="Artens administrativ status",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.adminStatusEnum",
			description="Filter based on URI or Qname identifier of an administrative status. Use Metadata-API to find identifiers. Will return entries of taxa that are marked with the admin status.",
			taxonStatusFilter=true)
	private Set<Qname> administrativeStatusId;

	@FilterDefinition(order=3.032, base=Base.UNIT,
			labelFi="Lajin uhanalaisuusluokitus",
			labelEn="Red list status of species",
			labelSv="Artens rödlistekategori",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.iucnStatuses",
			description="Filter based on URI or Qname identifier of red list status. Use Metadata-API to find identifiers. Will return entries of taxa that are marked with the red list status.",
			taxonStatusFilter=true)
	private Set<Qname> redListStatusId;

	@FilterDefinition(order=3.033, base=Base.UNIT,
			labelFi="Hallinnollinen rajaus ja uhnalaisuusluokitus: AND/OR",
			labelEn="Administrative status and red list status: AND/OR",
			labelSv="Administrativ status och rödlistekategori: AND/OR",
			type=Type.ENUMERATION,
			description="This parameter controls if search between administrativeStatusId and redListStatusId is an AND (default) or OR search.")
	private Operator taxonAdminFiltersOperator;

	@FilterDefinition(order=3.05, base=Base.UNIT,
			labelFi="Lajin esiintymisen tyyppi",
			labelEn="Species type of occurrence",
			labelSv="Artens typ av förekomst",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.typeOfOccurrenceEnum",
			description="Filter based on URI or Qname identifier of type of occurrence in Finland. Use Metadata-API to find identifiers. Will return entries of taxa that are marked with one or more of the specified statuses.")
	private Set<Qname> typeOfOccurrenceId;

	@FilterDefinition(order=3.06, base=Base.UNIT,
			labelFi="Poissuljettu lajin esiintymisen tyyppi",
			labelEn="Excluded species type of occurrence",
			labelSv="Utesluten artens typ av förekomst",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.typeOfOccurrenceEnum",
			description="Exclude based on URI or Qname identifier of type of occurrence in Finland. Use Metadata-API to find identifiers. Will return entries of taxa that are not marked with any of the specified statuses.")
	private Set<Qname> typeOfOccurrenceIdNot;

	@FilterDefinition(order=3.07, base=Base.UNIT,
			labelFi="Ensisijainen elinympäristö",
			labelEn="Primary habitat",
			labelSv="Primär habitat",
			type=Type.STRING,
			description="Filter based on primary habitat of taxa. Will return entries of taxa that have one of the specified habitats or a subhabitat of the given habitats. Syntax: MKV.habitatMk[MKV.habitatSpecificTypeJ,MKV.habitatSpecificTypePAK]",
			parameterSeparator=";")
	private Set<String> primaryHabitat;

	@FilterDefinition(order=3.08, base=Base.UNIT,
			labelFi="Elinympäristö",
			labelEn="Habitat",
			labelSv="Habitat",
			type=Type.STRING,
			description="Filter based on habitat of taxa (primary or secondary). Will return entries of taxa that have one of the specified habitats or a subhabitat of the given habitats. Syntax: MKV.habitatMk[MKV.habitatSpecificTypeJ,MKV.habitatSpecificTypePAK]",
			parameterSeparator=";")
	private Set<String> anyHabitat;

	@FilterDefinition(order=3.091, base=Base.UNIT,
			labelFi="Harvinaisuus: taksonista havaintoja maksimissaan",
			labelEn="Rarity: taxon occurrence count max",
			labelSv="Raritet: taxon förekomstantal max",
			type=Type.STRING,
			description="Filter based on occurrence count of taxa. Will return entries of taxa that have less occurrences than the given parameter.")
	private Integer occurrenceCountMax;

	@FilterDefinition(order=3.092, base=Base.UNIT,
			labelFi="Harvinaisuus: taksonista Suomessa havaintoja maksimissaan",
			labelEn="Rarity: taxon occurrence count in Finland max",
			labelSv="Raritet: taxon förekomstantal i Finland max",
			type=Type.STRING,
			description="Filter based on occurrence count in Finland of taxa. Will return entries of taxa that have less occurrences in Finland than the given parameter.")
	private Integer occurrenceCountFinlandMax;

	@FilterDefinition(order=3.10, base=Base.UNIT,
			labelFi="Laji on suomalainen",
			labelEn="Taxa is finnish",
			labelSv="Taxon är finska",
			type=Type.STRING,
			description="Filter only those taxa that are finnish or are not finnish.")
	private Boolean finnish;

	@FilterDefinition(order=3.11, base=Base.UNIT,
			labelFi="Laji on vieraslaji",
			labelEn="Species is an invasive species",
			labelSv="Arten är främmande arter",
			type=Type.STRING,
			description="Filter only those taxa that are invasive or are not invasive.")
	private Boolean invasive;

	@FilterDefinition(order=3.12, base=Base.UNIT,
			labelFi="Vain sensitiiviset lajit",
			labelEn="Only sensitive species",
			labelSv="Endast sensitiva arter",
			type=Type.STRING,
			description="Include only those occurrences that are of sensitive species or those that are of non-sensitive species")
	private Boolean sensitive;

	@FilterDefinition(order=3.13, base=Base.UNIT,
			labelFi="Taksonominen taso",
			labelEn="Taxon rank",
			labelSv="Taxonomisk nivå",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.taxonRankEnum",
			description="Filter based on URI or Qname identifier of taxon rank. Use Metadata-API to find identifiers. Will return entries of taxa that are of the specified ranks.")
	private Set<Qname> taxonRankId;

	@FilterDefinition(order=3.13, base=Base.UNIT,
			labelFi="Liitetty ylemmän tason taksoniin",
			labelEn="Is linked to a higher taxon",
			labelSv="Är länkad till ett högre taxon",
			type=Type.STRING,
			description="True: Filter those occurrence that are linked to a higher taxon (like genus, family). False: linked to taxon that is species, subspecies, aggregate or other lower rank.")
	private Boolean higherTaxon;

	@FilterDefinition(order=30, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Maa",
			labelEn="Country",
			labelSv="Land",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a country. Use Area-API to find identifiers. Will return entries where we have been able to interpret the country from coordinates or from reported area name.")
	private Set<Qname> countryId;

	@FilterDefinition(order=31.1, base=Base.GATHERING,
			labelFi="Kunta",
			labelEn="Municipality",
			labelSv="Kommun",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a finnish municipality. Use Area-API to find identifiers. Will return entries where we have been able to interpret the municipality from coordinates or from reported area name.")
	private Set<Qname> finnishMunicipalityId;

	@FilterDefinition(order=31.2, base=Base.GATHERING,
			labelFi="Eliömaakunta",
			labelEn="Biogeographical province",
			labelSv="Biogeografisk provins",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a biogeographical province. Use Area-API to find identifiers. Will return entries where we have been able to interpret the province from coordinates or from reported area name.")
	private Set<Qname> biogeographicalProvinceId;

	@FilterDefinition(order=31.3, base=Base.GATHERING,
			labelFi="Ympäristöasioiden ELY-keskus",
			labelEn="ELY Centre for Environmental Affairs",
			labelSv="NTM-central för miljöfrågor",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a ELY centre. Use Area-API to find identifiers. Implementation is based on municipality interpretations.")
	private Set<Qname> elyCentreId;

	@FilterDefinition(order=31.4, base=Base.GATHERING,
			labelFi="Maakunta",
			labelEn="Province",
			labelSv="Landskap",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a Finnish province. Use Area-API to find identifiers. Implementation is based on municipality interpretations.")
	private Set<Qname> provinceId;

	@FilterDefinition(order=33, base=Base.GATHERING,
			labelFi="Alueen nimi",
			labelEn="Area name",
			labelSv="Områdets namn",
			type=Type.STRING,
			description="Filter using name of country, municipality, province or locality. If the given name matches exactly one known area, the search will perform an identifier search. Otherwise the search looks from country verbatim, municipality verbatim, province verbatim and locality using exact match case insensitive search.")
	private Set<String> area;

	@FilterDefinition(order=40.1, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Nimetty paikka",
			labelEn="Named area",
			labelSv="Namngivna området",
			type=Type.RESOURCE,
			resourceName="named-places",
			description="Filter based on URI or Qname identifier of a NamedPlace. Use NamedPlace-API to find identifiers.")
	private Set<Qname> namedPlaceId;

	@FilterDefinition(order=40.2, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Nimettyn paikan tagi",
			labelEn="Named area tag",
			labelSv="Namngivna området tag",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MNP.tagEnum",
			description="Filter based on URI or Qname identifier of MNP.tagEnum (use metadata-api to resolve identifiers)")
	private Set<Qname> namedPlaceTag;

	@FilterDefinition(order=40.3, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Lintuyhdistyalue",
			labelEn="Bird association area",
			labelSv="Fågelförbundet",
			type=Type.RESOURCE,
			resourceName="areas",
			description="Filter based on URI or Qname identifier of a BirdAssociationArea. Use Area-API to find identifiers. Bird association area is interpreted based on YKJ 10KM grids (the grid the occurrence centerpoint is in).")
	private Set<Qname> birdAssociationAreaId;

	@FilterDefinition(order=45, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Lomake",
			labelEn="Form",
			labelSv="Form",
			type=Type.RESOURCE,
			resourceName="forms",
			description="Filter based on URI or Qname identifier of a Vihko Notebook form that was used to report the entry.")
	private Set<Qname> formId;

	@FilterDefinition(order=70.0, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Aika",
			labelEn="Time",
			labelSv="Tid",
			type=Type.STRING,
			description="Filter using event date. Date can be a full date or part of a date, for example 2000, 2000-06 or 2000-06-25. Time can be a range, for example 2000/2005 or 2000-01-01/2005-12-31. Relative days \"last N days\" can be used: 0 is today, -1 is yesterday and so on; for example -7/0 is a range between 7 days ago and today.")
	private Set<String> time;

	@FilterDefinition(order=70.1, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Ajan tarkkuus",
			labelEn="Time accuracy",
			labelSv="Tidens noggrannhet",
			type=Type.STRING,
			description="Filter using event date accuracy range in days. Will include entries where time span in days is less or equal to the given value.")
	private Integer timeAccuracy;

	@FilterDefinition(order=71, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Aika",
			labelEn="Time",
			labelSv="Tid",
			type=Type.STRING,
			description="Filter using event date. Value can be a year (2000), year range (2000/2001), year-month (2000-06) or a year-month range (2000-06/2000-08). (Note: this filter is mostly aimed to be used in /statistics queries because 'time' filter is not available for /statistics queries.)")
	private Set<String> yearMonth;

	@FilterDefinition(order=80, base=Base.GATHERING,
			labelFi="Päivän järjestysnumero",
			labelEn="Day of year",
			labelSv="Dagnummer",
			type=Type.STRING,
			description="Filter using day of year. For example \"100/160\" gives all records during spring and \"330/30\" during mid winter. If begin is ommited will use day 1 and if end is ommited will use day 366. Multiple ranges can be given by providing the parameter more times.")
	private Set<String> dayOfYear;

	@FilterDefinition(order=81, base=Base.GATHERING,
			labelFi="Vuodenaika",
			labelEn="Season",
			labelSv="Säsong",
			type=Type.STRING,
			description="Filter using season. For example \"501/630\" gives all records for May and July and \"1220/0220\" between 20.12. - 20.2. If begin is ommited will use 1.1. and if end is ommited will use 31.12. Multiple ranges can be given by providing the parameter more times.")
	private Set<String> season;

	@FilterDefinition(order=100, base=Base.DOCUMENT,
			labelFi="Lisätunniste",
			labelEn="Keyword",
			labelSv="Nyckelord",
			type=Type.STRING,
			description="Filter using keywords that have been tagged to entries. There are many types of keywods varying from legacy identifiers, project names and IDs, dataset ids, etc.  Will include records with quality issues (normally exluded by default).")
	private Set<String> keyword;

	@FilterDefinition(order=110.1, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Aineisto",
			labelEn="Information source",
			labelSv="Informationskällor",
			type=Type.RESOURCE,
			resourceName="collections",
			description="Filter based on URI or Qname identifier of collections. Use Collections-API to resolve identifiers.")
	private Set<Qname> collectionId;

	@FilterDefinition(order=110.2, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Aineisto",
			labelEn="Information source",
			labelSv="Informationskällor",
			type=Type.RESOURCE,
			resourceName="collections",
			description="Filter based on URI or Qname identifier of collections. Use Collections-API to resolve identifiers. Will not include child collections")
	private Set<Qname> collectionIdExplicit;

	@FilterDefinition(order=110.3, base=Base.DOCUMENT,
			labelFi="Poissuljettu aineisto",
			labelEn="Excluded information source",
			labelSv="Utesluten informationskälla",
			type=Type.RESOURCE,
			resourceName="collections",
			description="Exclude certain collections.")
	private Set<Qname> collectionIdNot;

	@FilterDefinition(order=110.4, base=Base.DOCUMENT,
			labelFi="Poissuljettu aineisto",
			labelEn="Excluded information source",
			labelSv="Utesluten informationskälla",
			type=Type.RESOURCE,
			resourceName="collections",
			description="Exclude certain collection (only the specified collection, not child collections)")
	private Set<Qname> collectionIdExplicitNot;

	@FilterDefinition(order=110.5, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Otetaanko alakokoelmat mukaan",
			labelEn="Include sub-collections",
			labelSv="Inkludera delkollektioner",
			type=Type.STRING,
			description="Defines if collectionId filter should include sub collections of the given collection ids. By default sub collections are included.")
	private Boolean includeSubCollections;

	@FilterDefinition(order=120, base=Base.DOCUMENT,
			labelFi="Tietojärjestelmä",
			labelEn="IT-system",
			labelSv="Informationssystem",
			type=Type.RESOURCE,
			resourceName="sources",
			description="Filter using identifiers of data sources (information systems). Use InformationSystem-API to resolve identifiers.")
	private Set<Qname> sourceId;

	@FilterDefinition(order=130, base=Base.UNIT,
			labelFi="Havaintotyyppi",
			labelEn="Record basis",
			labelSv="Observationtyp",
			type=Type.ENUMERATION,
			description="Filter using record basis. This can be used for example to get only preserved specimens.")
	private Set<RecordBasis> recordBasis;

	@FilterDefinition(order=131, base=Base.UNIT,
			labelFi="Havaintotyyppi",
			labelEn="Record basis",
			labelSv="Observationtyp",
			type=Type.ENUMERATION,
			description="Filter using super record basis. (Note: Even though the enumeration lists all record basis values, only few of those are super record basis: PRESERVED_SPECIMEN, HUMAN_OBSERVATION_UNSPECIFIED, ..; use aggregate by superRecordBasis to find used values.")
	private Set<RecordBasis> superRecordBasis;

	@FilterDefinition(order=140, base=Base.UNIT,
			labelFi="Elinvaihe",
			labelEn="Life stage",
			labelSv="Livsfas",
			type=Type.ENUMERATION,
			description="Filter using life stage of an unit.")
	private Set<LifeStage> lifeStage;

	@FilterDefinition(order=150, base=Base.UNIT,
			labelFi="Sukupuoli",
			labelEn="Sex",
			labelSv="Kön",
			type=Type.ENUMERATION,
			description="Filter using sex of an unit. When filtering MALE or FEMALE, will include those where individualCountMale/Female is >= 1")
	private Set<Sex> sex;

	@FilterDefinition(order=155, base=Base.UNIT,
			labelFi="Torjunnan teho",
			labelEn="Control effectiveness",
			labelSv="Styrnivån",
			type=Type.ENUMERATION,
			description="Filter using effectiveness of invasive control measures")
	private Set<InvasiveControl> invasiveControl;

	@FilterDefinition(order=156, base=Base.UNIT,
			labelFi="On torjuttu",
			labelEn="Is controlled",
			labelSv="Är styrs",
			type=Type.STRING,
			description="Filter only invasives that are reported to have been controlled successfully or not reported to have been controlled succesfully.")
	private Boolean invasiveControlled;

	@FilterDefinition(order=160.1, base=Base.DOCUMENT,
			labelFi="Dokumentin tunniste",
			labelEn="Document identifier",
			labelSv="Dokuments identifierare",
			type=Type.STRING,
			description="Filter using document URIs. Will include records with quality issues (normally exluded by default).")
	private Set<Qname> documentId;

	@FilterDefinition(order=160.2, base=Base.DOCUMENT,
			labelFi="Dokumentin tunnisteen prefix",
			labelEn="Document identifier prefix",
			labelSv="Dokuments identifierare prefix",
			type=Type.STRING,
			description="Filter using document URI prefix. For example prefix of http://id.luomus.fi/JA.1 is luomus:JA.  Will include records with quality issues (normally exluded by default).")
	private Set<String> documentIdPrefix;

	@FilterDefinition(order=160.3, base=Base.GATHERING,
			labelFi="Keruutapahtuman tunniste",
			labelEn="Gathering identifier",
			labelSv="Gathering identifierare",
			type=Type.STRING,
			description="Filter using gathering URIs. Will include records with quality issues (normally exluded by default).")
	private Set<Qname> gatheringId;

	@FilterDefinition(order=160.4, base=Base.UNIT,
			labelFi="Havainnon tunniste",
			labelEn="Unit identifier",
			labelSv="Observations identifierare",
			type=Type.STRING,
			description="Filter using unit ids.  Will include records with quality issues (normally exluded by default).")
	private Set<Qname> unitId;

	@FilterDefinition(order=160.5, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen tunniste",
			labelEn="Preparation/sample identifier",
			labelSv="Förberedelse/sampla identifierare",
			type=Type.STRING,
			description="Filter using preparation/sample ids.  Will include records with quality issues (normally exluded by default).")
	private Set<Qname> sampleId;

	@FilterDefinition(order=180, base=Base.UNIT,
			labelFi="Yksilön tunniste",
			labelEn="Individual identifier",
			labelSv="Individs identifierare",
			type=Type.STRING,
			description="Filter using identifier of an individual, for example bird ring.")
	private Set<Qname> individualId;

	@FilterDefinition(order=190, base=Base.UNIT, usableInStatistics=true,
			labelFi="Yksilömäärä, alaraja",
			labelEn="Individual count, min",
			labelSv="Individantal, min",
			defaultValue="1",
			type=Type.STRING,
			description="Filter using idividual count. Unreported individual count is assumed to mean \"1+\", so searching min=1 returns where count > 0 or count is not given. To search for \"zero observations\" use max=0. Defaults to 1 but when using annotation endpoint defaults to null.")
	private Integer individualCountMin;

	@FilterDefinition(order=200, base=Base.UNIT,
			labelFi="Yksilömäärä, yläraja",
			labelEn="Individual count, max",
			labelSv="Individantal, max",
			type=Type.STRING,
			description="Filter using idividual count. Unreported individual count is assumed to mean \"1+\", so searching min=1 returns where count > 0 or count is not given. To search for \"null observations\" use max=0.")
	private Integer individualCountMax;

	@FilterDefinition(order=210.1, base=Base.DOCUMENT,
			labelFi="Lataus tietovarastoon, päivänä tai jälkeen",
			labelEn="Loaded to data warehouse, on or after",
			labelSv="Laddning till datalager, på eller efter",
			type=Type.STRING,
			description="Filter using the date data was loaded to Data Warehouse. Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds. Returns entries loaded later or on the same date/timestamp.")
	private Date loadedSameOrAfter;

	@FilterDefinition(order=210.1, base=Base.DOCUMENT,
			labelFi="Lataus tietovarastoon, päivänä tai ennen",
			labelEn="Loaded to data warehouse, on or before",
			labelSv="Laddning till datalager, på eller före",
			type=Type.STRING,
			description="Filter using the date data was loaded to Data Warehouse. Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds. Returns entries loaded before or on the same date/timestamp.")
	private Date loadedSameOrBefore;

	@FilterDefinition(order=210.2, base=Base.DOCUMENT,
			labelFi="Ensimmäinen lataus tietovarastoon, päivänä tai jälkeen",
			labelEn="First loaded to data warehouse, on or after",
			labelSv="Första laddning till datalager, på eller efter",
			type=Type.STRING,
			description="Filter using the date data was loaded to Data Warehouse (first load of document). Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds. Returns entries loaded later or on the same date/timestamp.")
	private Date firstLoadedSameOrAfter;

	@FilterDefinition(order=210.3, base=Base.DOCUMENT,
			labelFi="Ensimmäinen lataus tietovarastoon, päivänä tai ennen",
			labelEn="First loaded to data warehouse, on or before",
			labelSv="Första laddning till datalager, på eller före",
			type=Type.STRING,
			description="Filter using the date data was loaded to Data Warehouse (first load of document). Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds. Returns entries loaded before or on the same date/timestamp.")
	private Date firstLoadedSameOrBefore;

	@FilterDefinition(order=210.4, base=Base.DOCUMENT, useableInPublic=false,
			labelFi="Ladattu/muokattu ennen täsmällistä ajanhetkeä",
			labelEn="Loaded/modified before this precise moment",
			labelSv="Lastad/ändrad före detta exakta tidpunkt",
			type=Type.STRING,
			description="Filter using the date data was loaded to Data Warehouse. Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds. Returns entries loaded before same date/timestamp. Only available in private-query-API.")
	private Date actualLoadBefore;

	@FilterDefinition(order=210.99, base=Base.DOCUMENT,
			labelFi="Luotu vuonna",
			labelEn="Created year",
			labelSv="Skapad år",
			type=Type.STRING,
			description="Filter using the year the record was created")
	private Integer createdDateYear;

	@FilterDefinition(order=220.1, base=Base.GATHERING,
			labelFi="Koordinaatit",
			labelEn="Coordinates",
			labelSv="Koordinater",
			type=Type.STRING,
			description="Filter using coordinates. Valid formats are latMin:latMax:lonMin:lonMax:CRS:ratio and lat:lon:CRS:ratio. The last parameter (ratio) is not required. Valid CRSs are WGS84, YKJ and EUREF "+CRS_DESC+". For metric coordinates (ykj, euref): the search 666:333:YKJ means lat between 6660000-6670000 and lon between 3330000-3340000. Ratio is a number between 0.0-1.0. Default ratio is 1.0 (observation area must be entirely inside the search area). Ratio 0.0: the search area must intersect with the observation area. For WGS84 the ratio is not calculated in meters but in degrees so it an approximation.")
	private Set<CoordinatesWithOverlapRatio> coordinates;

	@FilterDefinition(order=220.2, base=Base.GATHERING,
			labelFi="Polygonrajaus (wkt)",
			labelEn="Polygon (wkt)",
			labelSv="Polygon (wkt)",
			type=Type.STRING,
			description="Filter centerpoint of occurrences by polygon. Valid formats are WKT and WKT:CRS. Valid CRSs are WGS84, YKJ and EUREF (default) "+CRS_DESC+".  Polygon search is implemented only for Finland (based on ETRS-TM35FIN coordinate system). WKT must be somewhat shorter than 4000 chars. To overcome this limitation use polygonId filter and /polygon/ endpoint to get the polygonIds.")
	private PolygonSearch polygon;

	@FilterDefinition(order=220.3, base=Base.GATHERING,
			labelFi="Polygonrajaus",
			labelEn="Polygon",
			labelSv="Polygon",
			type=Type.STRING,
			description="Filter centerpoint occurrences using ID of a search polygon. Use /polygon/ endpoint to get id if the polygon.")
	private PolygonIdSearch polygonId;

	@FilterDefinition(order=221, base=Base.GATHERING,
			labelFi="Koordinaattien tarkkuus",
			labelEn="Coordinate accuracy",
			labelSv="Noggrannhet av koordinater",
			type=Type.STRING,
			description="Exclude coordinates that are less accurate or equal than the provided value (inclusive). Value is meters. Accuracy is a guiding logaritmic figure, for example 1m, 10m, 100m or 100km. (More specifically the longest length of the area bouding box rounded up on the logarithmic scale.)")
	private Integer coordinateAccuracyMax;

	@FilterDefinition(order=222, base=Base.GATHERING,
			labelFi="Koordinaatit (keskipiste)",
			labelEn="Coordinates (centerpoint)",
			labelSv="Koordinater (mittpunkt)",
			type=Type.STRING,
			description="Filter using WGS84 (EPSG:4326) centerpoint. Valid formats are lat:lon:WGS84 and latMin:latMax:lonMin:lonMax:WGS84. (You must include the crs WGS84 even though it is the only supported type.)")
	private Set<CoordinatesWithOverlapRatio> wgs84CenterPoint;

	@FilterDefinition(order=223, base=Base.GATHERING,
			labelFi="Neliökilometriruutu",
			labelEn="Square kilometer grid",
			labelSv="Kvadratkilometer rutnät",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 1km grid square(s). Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj1km;

	@FilterDefinition(order=224, base=Base.GATHERING,
			labelFi="Peninkulmaruutu",
			labelEn="Square ten kilometer grid",
			labelSv="Kvadrattiokilometer rutnät",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 10km grid square(s). Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj10km;

	@FilterDefinition(order=225, base=Base.GATHERING,
			labelFi="50km ruutu",
			labelEn="50 kilometer grid",
			labelSv="50 kilometer rutnät",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 50km grid square(s). Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj50km;

	@FilterDefinition(order=226, base=Base.GATHERING,
			labelFi="100km ruutu",
			labelEn="100 kilometer grid",
			labelSv="100 kilometer rutnät",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 100km grid square(s). Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj100km;

	@FilterDefinition(order=227, base=Base.GATHERING,
			labelFi="Neliökilometriruutu (keskipiste)",
			labelEn="Square kilometer grid (center point)",
			labelSv="Kvadratkilometer rutnät (centrumpunkt)",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 1km grid square(s) that are resolved using center point of the area. Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj1kmCenter;

	@FilterDefinition(order=228, base=Base.GATHERING,
			labelFi="Peninkulmaruutu (keskipiste)",
			labelEn="Square ten kilometer grid (center point)",
			labelSv="Kvadrattiokilometer rutnät (centrumpunkt)",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 10km grid square(s) that are resolved using center point of the area. Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj10kmCenter;

	@FilterDefinition(order=229, base=Base.GATHERING,
			labelFi="50km ruutu (keskipiste)",
			labelEn="50 kilometer grid (center point)",
			labelSv="50 kilometer rutnät (centrumpunkt)",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 50km grid square(s) that are resolved using center point of the area. Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj50kmCenter;

	@FilterDefinition(order=230, base=Base.GATHERING,
			labelFi="100km ruutu (keskipiste)",
			labelEn="100 kilometer grid (center point)",
			labelSv="100 kilometer rutnät (centrumpunkt)",
			type=Type.STRING,
			description="Filter using uniform (YKJ, EPSG:2393) 100km grid square(s) that are resolved using center point of the area. Valid format is lat:lon.")
	private Set<SingleCoordinates> ykj100kmCenter;

	@FilterDefinition(order=235, base=Base.GATHERING,
			labelFi="Koordinaattien lähde",
			labelEn="Source of coordinates",
			labelSv="Källa av koordinater",
			type=Type.ENUMERATION,
			description="Filter based on source of coordinates. Possible values are REPORTED_VALUE = the reported coordinates or FINNISH_MUNICIPALITY = the coordinates are the bounding box of the reported Finnish municipality (no coordinates were reported).")
	private Set<GeoSource> sourceOfCoordinates;

	@FilterDefinition(order=240, base=Base.UNIT,
			labelFi="On tyyppinäyte",
			labelEn="Is a type specimen",
			labelSv="Är typprover",
			type=Type.STRING,
			description="Filter only type specimens or those that are not type specimens.")
	private Boolean typeSpecimen;

	@FilterDefinition(order=241, base=Base.UNIT, usableInStatistics=true,
			labelFi="Luonnonvaraisuus",
			labelEn="Wild",
			labelSv="Vild",
			type=Type.ENUMERATION,
			defaultValue="WILD,UNKNOWN",
			description="Filter occurrences based on reported/annotated wild status. By default, non-wild occurrences are exluded.")
	private Set<Wild> wild;

	@FilterDefinition(order=242, base=Base.UNIT,
			labelFi="On pesintä/lisääntymispaikka",
			labelEn="Is a nest/breeding site",
			labelSv="Är en boetplats/avelsplats",
			type=Type.STRING,
			description="Filter only occurrences reported to be at their breeding site.")
	private Boolean breedingSite;

	@FilterDefinition(order=242.1, base=Base.UNIT,
			labelFi="On paikallinen laji",
			labelEn="Is a local species",
			labelSv="Är en lokal art",
			type=Type.STRING,
			description="Filter only for local species.")
	private Boolean local;

	@FilterDefinition(order=243.1, base=Base.UNIT,
			labelFi="Elossa",
			labelEn="Alive",
			labelSv="Levande",
			type=Type.STRING,
			description="Filter occurences reported to be dead (alive=false) or alive or unknown ( reported to be alive (true) or dead (false).")
	private Boolean alive;

	@FilterDefinition(order=243.2, base=Base.UNIT,
			labelFi="Määritysperuste",
			labelEn="Identification basis",
			labelSv="Bestämningsgrund",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.identificationBasisEnum",
			description="Filter based on URI or Qname identifier of identification basis. Use Metadata-API to find identifiers.")
	private Set<Qname> identificationBasis;

	@FilterDefinition(order=243.3, base=Base.UNIT,
			labelFi="Keruumenetelmä",
			labelEn="Sampling method",
			labelSv="Insamlingsmetod",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.samplingMethods",
			description="Filter based on URI or Qname identifier of sampling method. Use Metadata-API to find identifiers.")
	private Set<Qname> samplingMethod;

	@FilterDefinition(order=244, base=Base.UNIT,
			labelFi="Kasvin statuskoodi",
			labelEn="Plant status code",
			labelSv="Växtens statuskod",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.plantStatusCodeEnum",
			description="Filter only occurrences reported with a certain plant status code.")
	private Set<Qname> plantStatusCode;

	@FilterDefinition(order=250, base=Base.DOCUMENT,
			labelFi="Kuvia dokumentista",
			labelEn="Has document media",
			labelSv="Bilder av dokument",
			type=Type.STRING,
			description="Filter only units where parent document has media or doesn't have media.")
	private Boolean hasDocumentMedia;

	@FilterDefinition(order=251, base=Base.GATHERING,
			labelFi="Kuvia paikasta",
			labelEn="Has location media",
			labelSv="Bilder av plats",
			type=Type.STRING,
			description="Filter only units where parent gathering has media or doesn't have media.")
	private Boolean hasGatheringMedia;

	@FilterDefinition(order=252, base=Base.UNIT,
			labelFi="Mediatiedostoja",
			labelEn="Has media",
			labelSv="Har media",
			type=Type.STRING,
			description="Filter only units where unit has media or doesn't have media.")
	private Boolean hasUnitMedia;

	@FilterDefinition(order=253, base=Base.UNIT,
			labelFi="Kuvia",
			labelEn="Has image",
			labelSv="Har bilder",
			type=Type.STRING,
			description="Filter only units where unit has images or doesn't have images.")
	private Boolean hasUnitImages;

	@FilterDefinition(order=254, base=Base.UNIT,
			labelFi="Äänite",
			labelEn="Has audio",
			labelSv="Har audio",
			type=Type.STRING,
			description="Filter only units where unit has audio or doesn't have audio.")
	private Boolean hasUnitAudio;

	@FilterDefinition(order=255.1, base=Base.UNIT,
			labelFi="Video",
			labelEn="Has video",
			labelSv="Har video",
			type=Type.STRING,
			description="Filter only units where unit has video or doesn't have video.")
	private Boolean hasUnitVideo;

	@FilterDefinition(order=255.2, base=Base.UNIT,
			labelFi="3d-malleja",
			labelEn="Has 3d models",
			labelSv="Har 3d modeller",
			type=Type.STRING,
			description="Filter only units where unit has 3d models or doesn't have 3d-models.")
	private Boolean hasUnitModel;

	@FilterDefinition(order=256, base=Base.DOCUMENT,
			labelFi="Mediatiedostoja",
			labelEn="Has media",
			labelSv="Har media",
			type=Type.STRING,
			description="Filter only records where parent document, gathering or unit has media or none have media.")
	private Boolean hasMedia;

	@FilterDefinition(order=300, base=Base.DOCUMENT, useableInPublic=false,
			labelFi="Omistaja",
			labelEn="Owner",
			labelSv="Ägare",
			type=Type.RESOURCE,
			resourceName="person",
			description="Filter based on \"owners\" of records (those who have edit permissions or have edited, modified). Only available in private-query-API.")
	private Set<Qname> editorId;

	@FilterDefinition(order=301, base=Base.GATHERING, useableInPublic=false,
			labelFi="Havaitsija",
			labelEn="Observer",
			labelSv="Observer",
			type=Type.RESOURCE,
			resourceName="person",
			description="Filter based on observers of records. Only available in private-query-API.")
	private Set<Qname> observerId;

	@FilterDefinition(order=302, base=Base.GATHERING, useableInPublic=false,
			labelFi="Havaitsija tai omistaja",
			labelEn="Observer or owner",
			labelSv="Observer eller ägare",
			type=Type.RESOURCE,
			resourceName="person",
			description="Filter based on \"owners\" or observers of records. Only available in private-query-API.")
	private Set<Qname> editorOrObserverId;

	@FilterDefinition(order=302, base=Base.GATHERING, useableInPublic=false,
			labelFi="Poissuljettu havaitsija tai omistaja",
			labelEn="Excluded observer or owner",
			labelSv="Utensluten observer eller ägare",
			type=Type.RESOURCE,
			resourceName="person",
			description="Filter based on person not being \"owners\" or observers of records. Only available with special permissions.")
	private Set<Qname> editorOrObserverIdIsNot;

	@FilterDefinition(order=302, base=Base.GATHERING,
			labelFi="Havaitsijan nimi",
			labelEn="Observer name",
			labelSv="Observer namn",
			type=Type.STRING,
			description="Filter based on verbatim observer names. Search is case insensitive and wildcard * can be used.",
			parameterSeparator=";")
	private Set<String> teamMember;

	@FilterDefinition(order=303, base=Base.GATHERING,
			labelFi="Havaitsijan nimi",
			labelEn="Observer name",
			labelSv="Observer namn",
			type=Type.STRING,
			description="Filter based on ids of verbatim observer name strings. (The only way to access these ids is to aggregate by gathering.team.memberId)")
	private Set<Long> teamMemberId;

	@FilterDefinition(order=310, base=Base.DOCUMENT,
			labelFi="Salaussyy",
			labelEn="Secure reason",
			labelSv="Orsaken till hemlighållande",
			type=Type.ENUMERATION,
			description="Filter based on secure reasons.")
	private Set<SecureReason> secureReason;

	@FilterDefinition(order=311, base=Base.DOCUMENT,
			labelFi="Salaustaso",
			labelEn="Secure level",
			labelSv="Nivån av hemlighållande",
			type=Type.ENUMERATION,
			description="Filter based on secure level.")
	private Set<SecureLevel> secureLevel;

	@FilterDefinition(order=315, base=Base.DOCUMENT,
			labelFi="Vain salatut",
			labelEn="Only secured",
			labelSv="Endast hemlighållda",
			type=Type.STRING,
			description="Include only those that are secured or those that are not secured.")
	private Boolean secured;

	@FilterDefinition(order=350, base=Base.UNIT,
			labelFi="Laatumerkityt",
			labelEn="Annotated",
			labelSv="Annoterad",
			type=Type.STRING,
			description="Include only those units that have annotations or those that do not have annotations.")
	private Boolean annotated;

	@FilterDefinition(order=350, base=Base.ANNOTATION,
			labelFi="Laatumerkinnän tyyppi",
			labelEn="Annotation type",
			labelSv="Anteckningstyp",
			type=Type.ENUMERATION,
			description="Include only those units/annotations that are of the selected annotation type.")
	private Set<AnnotationType> annotationType;

	@FilterDefinition(order=351, base=Base.ANNOTATION,
			labelFi="Sisällytä automaattiset laatumerkinnät",
			labelEn="Include system annotations",
			labelSv="Inkludera systemannoteringar",
			type=Type.STRING,
			description="Include those annotations that are made by automated quality checks. Defaults to false.")
	private Boolean includeSystemAnnotations;

	@FilterDefinition(order=352, base=Base.ANNOTATION,
			labelFi="Annotointiaika, päivänä tai jälkeen",
			labelEn="Annotation created, on or after",
			labelSv="Anteckning skapad, på eller efter",
			type=Type.STRING,
			description="Include only those annotations that have been made after or on the same date/timestamp. Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds.")
	private Date annotatedSameOrAfter;

	@FilterDefinition(order=353, base=Base.ANNOTATION,
			labelFi="Annotointiaika, päivänä tai ennen",
			labelEn="Annotation created, on or before",
			labelSv="Anteckning skapad, på eller före",
			type=Type.STRING,
			description="Include only those annotations that have been made before or on the same date/timestamp. Format is yyyy-MM-dd or UNIX EPOCH timestamp in seconds.")
	private Date annotatedSameOrBefore;

	@FilterDefinition(order=400, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Virheelliset tietueet mukana",
			labelEn="Records with quality issues included",
			labelSv="Rekord med kvalitetsproblem ingår",
			defaultValue="NO_ISSUES",
			type=Type.ENUMERATION,
			description="Possible values: NO_ISSUES, BOTH, ONLY_ISSUES. Include records with quality issues (document, gathering or unit issues). Default is NO_ISSUES, but when searching by id (documentId, unitId, keyword) or using annotation endpoint the default is BOTH.")
	private QualityIssues qualityIssues;

	@FilterDefinition(order=420, base=Base.DOCUMENT,
			labelFi="Aineiston laatuluokitus",
			labelEn="Information source quality rating",
			labelSv="Informationskällor kvalitetsbedömning",
			type=Type.ENUMERATION,
			description="Filter based on quality rating of collections.")
	private Set<CollectionQuality> collectionQuality;

	@FilterDefinition(order=421, base=Base.UNIT,
			labelFi="Havainnon laatuluokitus",
			labelEn="Record quality",
			labelSv="Kvalitet",
			type=Type.ENUMERATION,
			description="Filter using quality rating of the occurrence")
	private Set<RecordQuality> recordQuality;

	@FilterDefinition(order=422, base=Base.UNIT,
			labelFi="Aineiston ja havainnon laatuluokitus",
			labelEn="Information source and record quality",
			labelSv="Informationskällor och rekord kvalitet",
			type=Type.STRING,
			description="Filter using quality rating of collection and occurrence. Format: \"PROFESSIONAL:NEUTRAL,UNCERTAIN\".",
			parameterSeparator=";")
	private Set<CollectionAndRecordQuality> collectionAndRecordQuality;

	@FilterDefinition(order=423, base=Base.UNIT,
			labelFi="Havainnon luotettavuus",
			labelEn="Record reliability",
			labelSv="Pålitlighet",
			type=Type.ENUMERATION,
			description="Filter using reliability of the occurrence")
	private Set<Reliability> reliability;

	@FilterDefinition(order=430, base=Base.UNIT,
			labelFi="Havainnon tagit",
			labelEn="Record tags",
			labelSv="Taggar",
			type=Type.ENUMERATION,
			description="Filter using effective tags of the record")
	private Set<Tag> effectiveTag;

	@FilterDefinition(order=500, base=Base.UNIT,
			labelFi="Määrittämättömät",
			labelEn="Unidentified",
			labelSv="Iidentifierad",
			type=Type.STRING,
			description="Show only records that need an identification (or do not need an identification)")
	private Boolean unidentified;

	@FilterDefinition(order=501, base=Base.UNIT,
			labelFi="Tarkistuslistalla",
			labelEn="Needs check",
			labelSv="Behöver kontroll",
			type=Type.STRING,
			description="Show only records that are marked to need checking by experts (or do not need checking)")
	private Boolean needsCheck;

	@FilterDefinition(order=600.1, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Täydellisen luettelon taksoni",
			labelEn="Complete list taxon",
			labelSv="Komplett lista taxon",
			type=Type.RESOURCE,
			resourceName="taxa",
			description="Show only records where document contains complete list for this higher taxon. For example include only records where all birds or mammals were documented, if they were seens -> something that is not documented was not seen. Use taxon IDs.")
	private Set<Qname> completeListTaxonId;

	@FilterDefinition(order=600.2, base=Base.DOCUMENT, usableInStatistics=true,
			labelFi="Täydellisen luettelon tyyppi",
			labelEn="Complete list type",
			labelSv="Komplett lista typ",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.completeListTypeEnum",
			description="Show only records where document contains complete list and the list is of this type: URI or Qname identifier of MY.completeListTypeEnum (use metadata-api to resolve identifiers)")
	private Set<Qname> completeListType;

	@FilterDefinition(order=600.3, base=Base.UNIT,
			labelFi="Täydellinen lajiluettelo",
			labelEn="Complete list",
			labelSv="Komplett lista",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MX.taxonSetEnum",
			description="Filter based on URI or Qname identifier of an taxon sets: Use Metadata-API to find identifiers. Returns occurrences of taxa that belong to the specified taxon set.")
	private Set<Qname> taxonSetId;

	@FilterDefinition(order=600.3, base=Base.GATHERING, usableInStatistics=true,
			labelFi="Kattavasti kirjattu taksoni",
			labelEn="Completely recorded taxon",
			labelSv="Helt registrerad taxon",
			type=Type.RESOURCE,
			resourceName="taxa",
			description="Show only records where observations are completely recorded for this higher taxon. For example include only records where all birds or mammals were documented, if they were seens -> something that is not documented was not seen. Use taxon IDs.")
	private Set<Qname> taxonCensus;

	@FilterDefinition(order=650.9, base=Base.UNIT,
			labelFi="On preparaatti/näyte",
			labelEn="Has preparation/sample",
			labelSv="Har förberedelse/sampla",
			type=Type.STRING,
			description="Include only those units that have samples or those that do not have samples.")
	private Boolean hasSample;

	@FilterDefinition(order=650.1, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen aineisto",
			labelEn="Preparation/sample information source",
			labelSv="Förberedelse/sampla informationskällor",
			type=Type.RESOURCE,
			resourceName="collections",
			description="Filter based on URI or Qname identifier of collections. Use Collections-API to resolve identifiers.")
	private Set<Qname> sampleCollectionId;

	@FilterDefinition(order=651, base=Base.SAMPLE,
			labelFi="Preparaatteja/näytteitä eri yksilöistä?",
			labelEn="Preparations/samples from multiple individuals?",
			labelSv="Förberedelse/sampla från flera individer?",
			type=Type.STRING,
			description="Was DNA extracted from single or multiple individuals? Include only those that were (true) or weren't (false).")
	private Boolean sampleMultiple;

	@FilterDefinition(order=652, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen tyyppi",
			labelEn="Preparation/sample type",
			labelSv="Förberedelse/sampla typ",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MF.preparationTypeEnum",
			description="Filter based on URI or Qname identifier of MF.preparationTypeEnum (use metadata-api to resolve identifiers)")
	private Set<Qname> sampleType;

	@FilterDefinition(order=653, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen laatu",
			labelEn="Preparation/sample quality",
			labelSv="Förberedelse/sampla kvalitet",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MF.qualityEnum",
			description="Filter based on URI or Qname identifier of MF.qualityEnum (use metadata-api to resolve identifiers)")
	private Set<Qname> sampleQuality;

	@FilterDefinition(order=654, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen tila",
			labelEn="Preparation/sample status",
			labelSv="Förberedelse/sampla status",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.statuses",
			description="Filter based on URI or Qname identifier of MY.statuses (use metadata-api to resolve identifiers)")
	private Set<Qname> sampleStatus;

	@FilterDefinition(order=655, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen materiaali",
			labelEn="Preparation/sample material",
			labelSv="Förberedelse/sampla material",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MF.materialEnum",
			description="Filter based on URI or Qname identifier of MY.statuses (use metadata-api to resolve identifiers)")
	private Set<Qname> sampleMaterial;

	@FilterDefinition(order=700, base=Base.UNIT,
			labelFi="Havainnon lisämuuttujat",
			labelEn="Observation additional variables",
			labelSv="Observation ytterligare variabler",
			type=Type.STRING,
			parameterSeparator=";",
			description=FACT_FILTER_DESCRIPTION)
	private Set<String> unitFact;

	@FilterDefinition(order=701, base=Base.GATHERING,
			labelFi="Paikan ja ajan lisämuuttujat",
			labelEn="Gathering additional variables",
			labelSv="Plats och tid ytterligare variabler",
			type=Type.STRING,
			parameterSeparator=";",
			description=FACT_FILTER_DESCRIPTION)
	private Set<String> gatheringFact;

	@FilterDefinition(order=702, base=Base.DOCUMENT,
			labelFi="Havaintoerän lisämuuttujat",
			labelEn="Document additional variables",
			labelSv="Document ytterligare variabler",
			type=Type.STRING,
			parameterSeparator=";",
			description=FACT_FILTER_DESCRIPTION)
	private Set<String> documentFact;

	@FilterDefinition(order=703, base=Base.SAMPLE,
			labelFi="Preparaatin/näytteen lisämuuttujat",
			labelEn="Preparation/sample additional variables",
			labelSv="Förberedelse/sampla ytterligare variabler",
			type=Type.STRING,
			parameterSeparator=";",
			description=FACT_FILTER_DESCRIPTION)
	private Set<String> sampleFact;

	@FilterDefinition(order=1000, base=Base.DOCUMENT,
			labelFi="Ositus/partition",
			labelEn="Partition",
			labelSv="Avdelning/partition",
			type=Type.STRING,
			description="You can split search results into partitions. Syntax: '1/5' splits the results to five partitions and returns the first. Useful when downloading large lists of results and you want to split the task into smaller sub-queries.")
	private Partition partition;

	@FilterDefinition(order=1001, base=Base.DOCUMENT,
			labelFi="Arvo kentälle annettu",
			labelEn="Field has value",
			labelSv="Fältet har ett värde ",
			type=Type.STRING,
			description="Name (or names) of fields that must be non-null for the occurrence to be included to results. The field must be from level document, gathering or unit (not for example annotation) and must not be an array field. Also, when quering gathering level, unit fields can not be used, etc. When multiple fields are listed, this is an AND search (all must be non-null).")
	private Set<String> hasValue;

	@FilterDefinition(order=1002.1, base=Base.UNIT,
			labelFi="Pesämävarmuusindeksi",
			labelEn="Breeding category",
			labelSv="Häckningsindex",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.atlasCodeEnum",
			description="Filter based on URI or Qname identifier of atlas code. Use Metadata-API to find identifiers.")
	private Set<Qname> atlasCode;

	@FilterDefinition(order=1002.2, base=Base.UNIT,
			labelFi="Pesimävarmuusluokka",
			labelEn="Breeding class",
			labelSv="Häcknings säkerhetsklass",
			type=Type.RESOURCE,
			resourceName="metadata/ranges/MY.atlasClassEnum",
			description="Filter based on URI or Qname identifier of atlas class. Use Metadata-API to find identifiers.")
	private Set<Qname> atlasClass;

	@FilterDefinition(order=1003, base=Base.UNIT,
			labelFi="Ei valtion mailta",
			labelEn="No state lands",
			labelSv="Inga statliga landområden ",
			type=Type.STRING,
			description="Filter to occurrences that are not on state lands (true) or to occurrences that are only from state lands (false)")
	private Boolean onlyNonStateLands;

	public Set<Qname> getTaxonId() {
		return taxonId;
	}
	public Filters setTaxonId(Qname taxon) {
		if (this.taxonId == null) this.taxonId = new LinkedHashSet<>();
		this.taxonId.add(taxon);
		return this;
	}
	public Set<String> getTarget() {
		return target;
	}
	public Filters setTarget(String target) {
		if (this.target == null) this.target = new LinkedHashSet<>();
		this.target.add(target);
		return this;
	}
	public Boolean getUseIdentificationAnnotations() {
		return useIdentificationAnnotations;
	}

	public Filters setUseIdentificationAnnotations(Boolean b) {
		this.useIdentificationAnnotations = b;
		return this;
	}
	public Boolean getIncludeSubTaxa() {
		return includeSubTaxa;
	}
	public Filters setIncludeSubTaxa(Boolean b) {
		this.includeSubTaxa = b;
		return this;
	}
	public Boolean getIncludeNonValidTaxa() {
		return includeNonValidTaxa;
	}
	public Filters setIncludeNonValidTaxa(Boolean b) {
		this.includeNonValidTaxa = b;
		return this;
	}
	public Boolean getOnlyNonValidTaxa() {
		return onlyNonValidTaxa;
	}
	public Filters setOnlyNonValidTaxa(Boolean b) {
		this.onlyNonValidTaxa = b;
		return this;
	}
	public Boolean getIncludeSubCollections() {
		return includeSubCollections;
	}
	public Filters setIncludeSubCollections(Boolean includeSubCollections) {
		this.includeSubCollections = includeSubCollections;
		return this;
	}
	public Set<Qname> getInformalTaxonGroupId() {
		return informalTaxonGroupId;
	}
	public Filters setInformalTaxonGroupId(Qname group) {
		if (this.informalTaxonGroupId == null) this.informalTaxonGroupId = new LinkedHashSet<>();
		this.informalTaxonGroupId.add(group);
		return this;
	}
	public Set<Qname> getInformalTaxonGroupIdNot() {
		return informalTaxonGroupIdNot;
	}
	public Filters setInformalTaxonGroupIdNot(Qname group) {
		if (this.informalTaxonGroupIdNot == null) this.informalTaxonGroupIdNot = new LinkedHashSet<>();
		this.informalTaxonGroupIdNot.add(group);
		return this;
	}
	public Set<Qname> getInformalTaxonGroupIdIncludingReported() {
		return informalTaxonGroupIdIncludingReported;
	}
	public Filters setInformalTaxonGroupIdIncludingReported(Qname group) {
		if (this.informalTaxonGroupIdIncludingReported == null) this.informalTaxonGroupIdIncludingReported = new LinkedHashSet<>();
		this.informalTaxonGroupIdIncludingReported.add(group);
		return this;
	}
	public Set<Qname> getAdministrativeStatusId() {
		return administrativeStatusId;
	}
	public Filters setAdministrativeStatusId(Qname status) {
		if (this.administrativeStatusId == null) this.administrativeStatusId = new LinkedHashSet<>();
		this.administrativeStatusId.add(status);
		return this;
	}
	public Set<Qname> getRedListStatusId() {
		return redListStatusId;
	}
	public Filters setRedListStatusId(Qname status) {
		if (this.redListStatusId == null) this.redListStatusId = new LinkedHashSet<>();
		this.redListStatusId.add(status);
		return this;
	}
	public Operator getTaxonAdminFiltersOperator() {
		return taxonAdminFiltersOperator;
	}
	public Filters setTaxonAdminFiltersOperator(Operator operator) {
		if (operator == null) operator = Operator.AND;
		this.taxonAdminFiltersOperator = operator;
		return this;
	}
	public Set<Qname> getTaxonSetId() {
		return taxonSetId;
	}
	public Filters setTaxonSetId(Qname taxonSet) {
		if (this.taxonSetId == null) this.taxonSetId = new LinkedHashSet<>();
		this.taxonSetId.add(taxonSet);
		return this;
	}
	public Set<Qname> getTypeOfOccurrenceId() {
		return typeOfOccurrenceId;
	}
	public Filters setTypeOfOccurrenceId(Qname type) {
		if (this.typeOfOccurrenceId == null) this.typeOfOccurrenceId = new LinkedHashSet<>();
		this.typeOfOccurrenceId.add(type);
		return this;
	}
	public Set<Qname> getTypeOfOccurrenceIdNot() {
		return typeOfOccurrenceIdNot;
	}
	public Filters setTypeOfOccurrenceIdNot(Qname type) {
		if (this.typeOfOccurrenceIdNot == null) this.typeOfOccurrenceIdNot = new LinkedHashSet<>();
		this.typeOfOccurrenceIdNot.add(type);
		return this;
	}
	public Set<String> getPrimaryHabitat() {
		return primaryHabitat;
	}
	public Filters setPrimaryHabitat(String habitat) {
		if (this.primaryHabitat == null) this.primaryHabitat = new LinkedHashSet<>();
		this.primaryHabitat.add(habitat);
		return this;
	}
	public Set<String> getAnyHabitat() {
		return anyHabitat;
	}
	public Filters setAnyHabitat(String habitat) {
		if (this.anyHabitat == null) this.anyHabitat = new LinkedHashSet<>();
		this.anyHabitat.add(habitat);
		return this;
	}
	public Integer getOccurrenceCountMax() {
		return occurrenceCountMax;
	}
	public Filters setOccurrenceCountMax(Integer count) {
		this.occurrenceCountMax = count;
		return this;
	}
	public Integer getOccurrenceCountFinlandMax() {
		return occurrenceCountFinlandMax;
	}
	public Filters setOccurrenceCountFinlandMax(Integer count) {
		this.occurrenceCountFinlandMax = count;
		return this;
	}
	public Set<Qname> getTaxonRankId() {
		return taxonRankId;
	}
	public Filters setTaxonRankId(Qname rank) {
		if (this.taxonRankId == null) this.taxonRankId = new LinkedHashSet<>();
		this.taxonRankId.add(rank);
		return this;
	}
	public Filters setHigherTaxon(Boolean value) {
		this.higherTaxon = value;
		return this;
	}
	public Boolean getHigherTaxon() {
		return higherTaxon;
	}
	public Set<Qname> getCountryId() {
		return countryId;
	}
	public Filters setCountryId(Qname country) {
		if (this.countryId == null) this.countryId = new LinkedHashSet<>();
		this.countryId.add(country);
		return this;
	}
	public Set<Qname> getFinnishMunicipalityId() {
		return finnishMunicipalityId;
	}
	public Filters setFinnishMunicipalityId(Qname finnishMunicipality) {
		if (this.finnishMunicipalityId == null) this.finnishMunicipalityId = new LinkedHashSet<>();
		this.finnishMunicipalityId.add(finnishMunicipality);
		return this;
	}
	public Set<Qname> getBiogeographicalProvinceId() {
		return biogeographicalProvinceId;
	}
	public Filters setBiogeographicalProvinceId(Qname biogeographicalProvince) {
		if (this.biogeographicalProvinceId == null) this.biogeographicalProvinceId = new LinkedHashSet<>();
		this.biogeographicalProvinceId.add(biogeographicalProvince);
		return this;
	}
	public Set<Qname> getElyCentreId() {
		return elyCentreId;
	}
	public Filters setElyCentreId(Qname elyCentreId) {
		if (this.elyCentreId == null) this.elyCentreId = new LinkedHashSet<>();
		this.elyCentreId.add(elyCentreId);
		return this;
	}
	public Set<Qname> getProvinceId() {
		return provinceId;
	}
	public Filters setProvinceId(Qname provinceId) {
		if (this.provinceId == null) this.provinceId = new LinkedHashSet<>();
		this.provinceId.add(provinceId);
		return this;
	}
	public Set<Qname> getNamedPlaceId() {
		return namedPlaceId;
	}
	public Filters setNamedPlaceId(Qname namedPlace) {
		if (this.namedPlaceId == null) this.namedPlaceId = new LinkedHashSet<>();
		this.namedPlaceId.add(namedPlace);
		return this;
	}
	public Set<Qname> getNamedPlaceTag() {
		return namedPlaceTag;
	}
	public Filters setNamedPlaceTag(Qname namedPlaceTag) {
		if (this.namedPlaceTag == null) this.namedPlaceTag = new LinkedHashSet<>();
		this.namedPlaceTag.add(namedPlaceTag);
		return this;
	}
	public Set<Qname> getBirdAssociationAreaId() {
		return birdAssociationAreaId;
	}
	public Filters setBirdAssociationAreaId(Qname birdAssociationAreaId) {
		if (this.birdAssociationAreaId == null) this.birdAssociationAreaId = new LinkedHashSet<>();
		this.birdAssociationAreaId.add(birdAssociationAreaId);
		return this;
	}
	public Set<Qname> getFormId() {
		return formId;
	}
	public Filters setFormId(Qname form) {
		if (this.formId == null) this.formId = new LinkedHashSet<>();
		this.formId.add(form);
		return this;
	}
	public Set<String> getArea() {
		return area;
	}
	public Filters setArea(String area) {
		if (this.area == null) this.area = new LinkedHashSet<>();
		this.area.add(area);
		return this;
	}
	public Set<String> getTime() {
		return time;
	}
	public Filters setTime(String time) {
		if (this.time == null) this.time = new LinkedHashSet<>();
		this.time.add(time);
		return this;
	}
	public Integer getTimeAccuracy() {
		return timeAccuracy;
	}
	public Filters setTimeAccuracy(Integer timeAccuracy) {
		this.timeAccuracy = timeAccuracy;
		return this;
	}
	public Set<String> getYearMonth() {
		return yearMonth;
	}
	public Filters setYearMonth(String yearMonth) {
		if (this.yearMonth == null) this.yearMonth = new LinkedHashSet<>();
		this.yearMonth.add(yearMonth);
		return this;
	}
	public Set<String> getDayOfYear() {
		return dayOfYear;
	}
	public Filters setDayOfYear(String time) {
		if (this.dayOfYear == null) this.dayOfYear = new LinkedHashSet<>();
		this.dayOfYear.add(time);
		return this;
	}
	public Set<String> getSeason() {
		return season;
	}
	public Filters setSeason(String time) {
		if (this.season == null) this.season = new LinkedHashSet<>();
		this.season.add(time);
		return this;
	}
	public Set<String> getKeyword() {
		return keyword;
	}
	public Filters setKeyword(String keyword) {
		if (this.keyword ==  null) this.keyword = new LinkedHashSet<>();
		this.keyword.add(keyword);
		clearDefaultsForIdSearch();
		return this;
	}

	private static Field qualityIssuesField;
	private static Field individualCountMinField;
	private static Field wildField;
	private static Field includeSystemAnnotationsField;
	static {
		try {
			qualityIssuesField = Filters.class.getDeclaredField("qualityIssues");
			individualCountMinField = Filters.class.getDeclaredField("individualCountMin");
			wildField = Filters.class.getDeclaredField("wild");
			includeSystemAnnotationsField = Filters.class.getDeclaredField("includeSystemAnnotations");
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private void clearDefaultsForIdSearch() {
		if (isDefaultValue(qualityIssuesField, qualityIssues)) {
			this.qualityIssues = null;
		}
		if (isDefaultValue(individualCountMinField, individualCountMin)) {
			this.individualCountMin = null;
		}
		if (isDefaultValue(wildField, wild)) {
			this.wild = null;
		}
		if (isDefaultValue(includeSystemAnnotationsField, includeSystemAnnotations)) {
			this.includeSystemAnnotations = null;
		}
	}

	public Set<String> getTeamMember() {
		return teamMember;
	}
	public Filters setTeamMember(String teamMember) {
		if (this.teamMember ==  null) this.teamMember = new LinkedHashSet<>();
		this.teamMember.add(teamMember);
		return this;
	}
	public Set<Long> getTeamMemberId() {
		return teamMemberId;
	}
	public Filters setTeamMemberId(Long id) {
		if (this.teamMemberId ==  null) this.teamMemberId = new LinkedHashSet<>();
		this.teamMemberId.add(id);
		return this;
	}
	public Set<Qname> getEditorId() {
		return editorId;
	}
	public Filters setEditorId(Qname editor) {
		if (this.editorId == null) this.editorId = new LinkedHashSet<>();
		this.editorId.add(editor);
		return this;
	}
	public Set<Qname> getObserverId() {
		return observerId;
	}
	public Filters setObserverId(Qname observer) {
		if (this.observerId == null) this.observerId = new LinkedHashSet<>();
		this.observerId.add(observer);
		return this;
	}
	public Set<Qname> getEditorOrObserverId() {
		return editorOrObserverId;
	}
	public Filters setEditorOrObserverId(Qname editorOrObserver) {
		if (this.editorOrObserverId == null) this.editorOrObserverId = new LinkedHashSet<>();
		this.editorOrObserverId.add(editorOrObserver);
		return this;
	}
	public Set<Qname> getEditorOrObserverIdIsNot() {
		return editorOrObserverIdIsNot;
	}
	public Filters setEditorOrObserverIdIsNot(Qname editorOrObserverIsNot) {
		if (this.editorOrObserverIdIsNot == null) this.editorOrObserverIdIsNot = new LinkedHashSet<>();
		this.editorOrObserverIdIsNot.add(editorOrObserverIsNot);
		return this;
	}
	public Set<Qname> getCollectionId() {
		return collectionId;
	}
	public Filters setCollectionId(Qname collection) {
		if (this.collectionId == null) this.collectionId = new LinkedHashSet<>();
		this.collectionId.add(collection);
		return this;
	}
	public Set<Qname> getCollectionIdExplicit() {
		return collectionIdExplicit;
	}
	public Filters setCollectionIdExplicit(Qname collection) {
		if (this.collectionIdExplicit == null) this.collectionIdExplicit = new LinkedHashSet<>();
		this.collectionIdExplicit.add(collection);
		return this;
	}
	public Set<Qname> getSampleCollectionId() {
		return sampleCollectionId;
	}
	public Filters setSampleCollectionId(Qname collection) {
		if (this.sampleCollectionId == null) this.sampleCollectionId = new LinkedHashSet<>();
		this.sampleCollectionId.add(collection);
		return this;
	}
	public Set<Qname> getCollectionIdNot() {
		return collectionIdNot;
	}
	public Filters setCollectionIdNot(Qname collection) {
		if (this.collectionIdNot == null) this.collectionIdNot = new LinkedHashSet<>();
		this.collectionIdNot.add(collection);
		return this;
	}
	public Set<Qname> getCollectionIdExplicitNot() {
		return collectionIdExplicitNot;
	}
	public Filters setCollectionIdExplicitNot(Qname collection) {
		if (this.collectionIdExplicitNot == null) this.collectionIdExplicitNot = new LinkedHashSet<>();
		this.collectionIdExplicitNot.add(collection);
		return this;
	}
	public Set<Qname> getSourceId() {
		return sourceId;
	}
	public Filters setSourceId(Qname source) {
		if (this.sourceId == null) this.sourceId = new LinkedHashSet<>();
		this.sourceId.add(source);
		return this;
	}
	public Set<CollectionQuality> getCollectionQuality() {
		return collectionQuality;
	}
	public Filters setCollectionQuality(CollectionQuality collectionQuality) {
		if (this.collectionQuality == null) this.collectionQuality = new LinkedHashSet<>();
		this.collectionQuality.add(collectionQuality);
		return this;
	}
	public Set<RecordBasis> getRecordBasis() {
		return recordBasis;
	}
	public Filters setRecordBasis(RecordBasis recordBasis) {
		if (this.recordBasis == null) this.recordBasis = new LinkedHashSet<>();
		this.recordBasis.add(recordBasis);
		return this;
	}
	public Set<RecordBasis> getSuperRecordBasis() {
		return superRecordBasis;
	}
	public Filters setSuperRecordBasis(RecordBasis recordBasis) {
		if (this.superRecordBasis == null) this.superRecordBasis = new LinkedHashSet<>();
		this.superRecordBasis.add(recordBasis);
		return this;
	}
	public Set<LifeStage> getLifeStage() {
		return lifeStage;
	}
	public Filters setLifeStage(LifeStage lifeStage) {
		if (this.lifeStage == null) this.lifeStage = new LinkedHashSet<>();
		this.lifeStage.add(lifeStage);
		return this;
	}
	public Set<Sex> getSex() {
		return sex;
	}
	public Filters setSex(Sex sex) {
		if (this.sex == null) this.sex = new LinkedHashSet<>();
		this.sex.add(sex);
		return this;
	}
	public Set<InvasiveControl> getInvasiveControl() {
		return invasiveControl;
	}
	public Filters setInvasiveControl(InvasiveControl invasiveControl) {
		if (this.invasiveControl == null) this.invasiveControl = new LinkedHashSet<>();
		this.invasiveControl.add(invasiveControl);
		return this;
	}
	public Set<Qname> getDocumentId() {
		return documentId;
	}
	public Filters setDocumentId(Qname documentId) {
		if (this.documentId == null) this.documentId = new LinkedHashSet<>();
		this.documentId.add(documentId);
		clearDefaultsForIdSearch();
		return this;
	}
	public Set<String> getDocumentIdPrefix() {
		return documentIdPrefix;
	}
	public Filters setDocumentIdPrefix(String documentIdPrefix) {
		if (this.documentIdPrefix == null) this.documentIdPrefix = new LinkedHashSet<>();
		this.documentIdPrefix.add(documentIdPrefix);
		clearDefaultsForIdSearch();
		return this;
	}
	public Set<Qname> getGatheringId() {
		return gatheringId;
	}
	public Filters setGatheringId(Qname gatheringId) {
		if (this.gatheringId == null) this.gatheringId = new LinkedHashSet<>();
		this.gatheringId.add(gatheringId);
		clearDefaultsForIdSearch();
		return this;
	}
	public Set<Qname> getUnitId() {
		return unitId;
	}
	public Filters setUnitId(Qname unitId) {
		if (this.unitId == null) this.unitId = new LinkedHashSet<>();
		this.unitId.add(unitId);
		clearDefaultsForIdSearch();
		return this;
	}
	public Set<Qname> getSampleId() {
		return sampleId;
	}
	public Filters setSampleId(Qname sampleId) {
		if (this.sampleId == null) this.sampleId = new LinkedHashSet<>();
		this.sampleId.add(sampleId);
		clearDefaultsForIdSearch();
		return this;
	}
	public Set<Qname> getIndividualId() {
		return individualId;
	}
	public Filters setIndividualId(Qname individualId) {
		if (this.individualId == null) this.individualId = new LinkedHashSet<>();
		this.individualId.add(individualId);
		return this;
	}
	public Set<Qname> getCompleteListTaxonId() {
		return completeListTaxonId;
	}
	public Filters setCompleteListTaxonId(Qname completeListTaxonId) {
		if (this.completeListTaxonId == null) this.completeListTaxonId = new LinkedHashSet<>();
		this.completeListTaxonId.add(completeListTaxonId);
		return this;
	}
	public Set<Qname> getCompleteListType() {
		return completeListType;
	}
	public Filters setCompleteListType(Qname completeListType) {
		if (this.completeListType == null) this.completeListType = new LinkedHashSet<>();
		this.completeListType.add(completeListType);
		return this;
	}
	public Set<Qname> getTaxonCensus() {
		return taxonCensus;
	}
	public Filters setTaxonCensus(Qname taxonId) {
		if (this.taxonCensus == null) this.taxonCensus = new LinkedHashSet<>();
		this.taxonCensus.add(taxonId);
		return this;
	}
	public Set<String> getUnitFact() {
		return unitFact;
	}
	public Filters setUnitFact(String factAndValue) {
		if (this.unitFact == null) this.unitFact = new LinkedHashSet<>();
		this.unitFact.add(factAndValue);
		return this;
	}
	public Set<String> getGatheringFact() {
		return gatheringFact;
	}
	public Filters setGatheringFact(String factAndValue) {
		if (this.gatheringFact == null) this.gatheringFact = new LinkedHashSet<>();
		this.gatheringFact.add(factAndValue);
		return this;
	}
	public Set<String> getDocumentFact() {
		return documentFact;
	}
	public Filters setDocumentFact(String factAndValue) {
		if (this.documentFact == null) this.documentFact = new LinkedHashSet<>();
		this.documentFact.add(factAndValue);
		return this;
	}
	public Set<String> getSampleFact() {
		return sampleFact;
	}
	public Filters setSampleFact(String factAndValue) {
		if (this.sampleFact == null) this.sampleFact = new LinkedHashSet<>();
		this.sampleFact.add(factAndValue);
		return this;
	}
	public Partition getPartition() {
		return partition;
	}
	public Filters setPartition(Partition partition) {
		this.partition = partition;
		return this;
	}
	public Date getLoadedSameOrAfter() {
		return loadedSameOrAfter;
	}
	public Filters setLoadedSameOrAfter(Date d) {
		this.loadedSameOrAfter = d;
		return this;
	}
	public Date getLoadedSameOrBefore() {
		return loadedSameOrBefore;
	}
	public Filters setLoadedSameOrBefore(Date d) {
		this.loadedSameOrBefore = d;
		return this;
	}
	public Date getFirstLoadedSameOrAfter() {
		return firstLoadedSameOrAfter;
	}
	public Filters setFirstLoadedSameOrAfter(Date d) {
		this.firstLoadedSameOrAfter = d;
		return this;
	}
	public Date getFirstLoadedSameOrBefore() {
		return firstLoadedSameOrBefore;
	}
	public Filters setFirstLoadedSameOrBefore(Date d) {
		this.firstLoadedSameOrBefore = d;
		return this;
	}
	public Date getActualLoadBefore() {
		return actualLoadBefore;
	}
	public Filters setActualLoadBefore(Date d) {
		this.actualLoadBefore = d;
		return this;
	}
	public Integer getCreatedDateYear() {
		return createdDateYear;
	}
	public Filters setCreatedDateYear(Integer year) {
		this.createdDateYear = year;
		return this;
	}
	public Boolean isTypeSpecimen() {
		return typeSpecimen;
	}
	public Filters setTypeSpecimen(Boolean isTypeSpecimen) {
		this.typeSpecimen = isTypeSpecimen;
		return this;
	}
	public Boolean isInvasiveControlled() {
		return invasiveControlled;
	}
	public Filters setInvasiveControlled(Boolean isInvasiveControlled) {
		this.invasiveControlled = isInvasiveControlled;
		return this;
	}
	public Set<Wild> getWild() {
		return wild;
	}
	public Filters setWild(Wild wild) {
		if (this.wild == null) this.wild = new LinkedHashSet<>();
		this.wild.add(wild);
		return this;
	}
	public Boolean isBreedingSite() {
		return breedingSite;
	}
	public Filters setBreedingSite(Boolean breedingSite) {
		this.breedingSite = breedingSite;
		return this;
	}

	public Boolean isLocal() {
		return local;
	}
	public Filters setLocal(Boolean local) {
		this.local = local;
		return this;
	}
	public Boolean isAlive() {
		return alive;
	}
	public Filters setAlive(Boolean alive) {
		this.alive = alive;
		return this;
	}
	public Set<Qname> getIdentificationBasis() {
		return identificationBasis;
	}
	public Filters setIdentificationBasis(Qname identificationBasis) {
		if (this.identificationBasis == null) this.identificationBasis = new LinkedHashSet<>();
		this.identificationBasis.add(identificationBasis);
		return this;
	}
	public Set<Qname> getSamplingMethod() {
		return samplingMethod;
	}
	public Filters setSamplingMethod(Qname samplingMethod) {
		if (this.samplingMethod == null) this.samplingMethod = new LinkedHashSet<>();
		this.samplingMethod.add(samplingMethod);
		return this;
	}
	public Set<Qname> getPlantStatusCode() {
		return plantStatusCode;
	}
	public Filters setPlantStatusCode(Qname plantStatusCode) {
		if (this.plantStatusCode == null) this.plantStatusCode = new LinkedHashSet<>();
		this.plantStatusCode.add(plantStatusCode);
		return this;
	}
	public Boolean getHasDocumentMedia() {
		return hasDocumentMedia;
	}
	public Filters setHasDocumentMedia(Boolean hasDocumentMedia) {
		this.hasDocumentMedia = hasDocumentMedia;
		return this;
	}
	public Boolean getHasGatheringMedia() {
		return hasGatheringMedia;
	}
	public Filters setHasGatheringMedia(Boolean hasGatheringMedia) {
		this.hasGatheringMedia = hasGatheringMedia;
		return this;
	}
	public Boolean getHasUnitMedia() {
		return hasUnitMedia;
	}
	public Filters setHasUnitMedia(Boolean hasUnitMedia) {
		this.hasUnitMedia = hasUnitMedia;
		return this;
	}
	public Boolean getHasUnitImages() {
		return hasUnitImages;
	}
	public Filters setHasUnitImages(Boolean hasUnitImages) {
		this.hasUnitImages = hasUnitImages;
		return this;
	}
	public Boolean getHasUnitAudio() {
		return hasUnitAudio;
	}
	public Filters setHasUnitAudio(Boolean hasUnitAudio) {
		this.hasUnitAudio = hasUnitAudio;
		return this;
	}
	public Boolean getHasUnitVideo() {
		return hasUnitVideo;
	}
	public Filters setHasUnitVideo(Boolean hasUnitVideo) {
		this.hasUnitVideo = hasUnitVideo;
		return this;
	}
	public Boolean getHasUnitModel() {
		return hasUnitModel;
	}
	public Filters setHasUnitModel(Boolean hasUnitModel) {
		this.hasUnitModel = hasUnitModel;
		return this;
	}
	public Boolean getHasMedia() {
		return hasMedia;
	}
	public Filters setHasMedia(Boolean hasMedia) {
		this.hasMedia = hasMedia;
		return this;
	}
	public Integer getCoordinateAccuracyMax() {
		return coordinateAccuracyMax;
	}
	public Filters setCoordinateAccuracyMax(Integer coordinateAccuracyMax) {
		this.coordinateAccuracyMax = coordinateAccuracyMax;
		return this;
	}
	public Set<CoordinatesWithOverlapRatio> getCoordinates() {
		return coordinates;
	}
	public Filters setCoordinates(CoordinatesWithOverlapRatio coordinates) {
		if (this.coordinates == null) this.coordinates = new LinkedHashSet<>();
		this.coordinates.add(coordinates);
		return this;
	}
	public PolygonSearch getPolygon() {
		return polygon;
	}
	public Filters setPolygon(PolygonSearch polygon) {
		if (this.polygonId != null) throw new IllegalArgumentException("Can only define polygon or polygonId - not two polygons at once");
		this.polygon = polygon;
		return this;
	}
	public PolygonIdSearch getPolygonId() {
		return polygonId;
	}
	public Filters setPolygonId(PolygonIdSearch polygonId) {
		if (this.polygon != null) throw new IllegalArgumentException("Can only define polygon or polygonId - not two polygons at once");
		this.polygonId = polygonId;
		return this;
	}
	public Set<CoordinatesWithOverlapRatio> getWgs84CenterPoint() {
		return wgs84CenterPoint;
	}
	public Filters setWgs84CenterPoint(CoordinatesWithOverlapRatio coordinates) {
		if (Coordinates.Type.WGS84 != coordinates.getType()) throw new IllegalArgumentException("Filter by wgs84 centerpoint only supports coordinates in WGS84 format");
		if (this.wgs84CenterPoint == null) this.wgs84CenterPoint = new LinkedHashSet<>();
		this.wgs84CenterPoint.add(coordinates);
		return this;
	}
	public Set<SingleCoordinates> getYkj1km() {
		return ykj1km;
	}
	public Filters setYkj1km(SingleCoordinates ykj1km) {
		if (this.ykj1km == null) this.ykj1km = new LinkedHashSet<>();
		this.ykj1km.add(ykj1km);
		return this;
	}
	public Set<SingleCoordinates> getYkj10km() {
		return ykj10km;
	}
	public Filters setYkj10km(SingleCoordinates ykj10km) {
		if (this.ykj10km == null) this.ykj10km = new LinkedHashSet<>();
		this.ykj10km.add(ykj10km);
		return this;
	}
	public Set<SingleCoordinates> getYkj50km() {
		return ykj50km;
	}
	public Filters setYkj50km(SingleCoordinates ykj50km) {
		if (this.ykj50km == null) this.ykj50km = new LinkedHashSet<>();
		this.ykj50km.add(ykj50km);
		return this;
	}
	public Set<SingleCoordinates> getYkj100km() {
		return ykj100km;
	}
	public Filters setYkj100km(SingleCoordinates ykj100km) {
		if (this.ykj100km == null) this.ykj100km = new LinkedHashSet<>();
		this.ykj100km.add(ykj100km);
		return this;
	}
	public Set<SingleCoordinates> getYkj1kmCenter() {
		return ykj1kmCenter;
	}
	public Filters setYkj1kmCenter(SingleCoordinates ykj1km) {
		if (this.ykj1kmCenter == null) this.ykj1kmCenter = new LinkedHashSet<>();
		this.ykj1kmCenter.add(ykj1km);
		return this;
	}
	public Set<SingleCoordinates> getYkj10kmCenter() {
		return ykj10kmCenter;
	}
	public Filters setYkj10kmCenter(SingleCoordinates ykj10km) {
		if (this.ykj10kmCenter == null) this.ykj10kmCenter = new LinkedHashSet<>();
		this.ykj10kmCenter.add(ykj10km);
		return this;
	}
	public Set<SingleCoordinates> getYkj50kmCenter() {
		return ykj50kmCenter;
	}
	public Filters setYkj50kmCenter(SingleCoordinates ykj50km) {
		if (this.ykj50kmCenter == null) this.ykj50kmCenter = new LinkedHashSet<>();
		this.ykj50kmCenter.add(ykj50km);
		return this;
	}
	public Set<SingleCoordinates> getYkj100kmCenter() {
		return ykj100kmCenter;
	}
	public Filters setYkj100kmCenter(SingleCoordinates ykj100km) {
		if (this.ykj100kmCenter == null) this.ykj100kmCenter = new LinkedHashSet<>();
		this.ykj100kmCenter.add(ykj100km);
		return this;
	}
	public Set<GeoSource> getSourceOfCoordinates() {
		return sourceOfCoordinates;
	}
	public Filters setSourceOfCoordinates(GeoSource source) {
		if (this.sourceOfCoordinates == null) this.sourceOfCoordinates = new LinkedHashSet<>();
		this.sourceOfCoordinates.add(source);
		return this;
	}
	public Integer getIndividualCountMin() {
		return individualCountMin;
	}
	public Filters setIndividualCountMin(Integer individualCountMin) {
		this.individualCountMin = individualCountMin;
		return this;
	}
	public Integer getIndividualCountMax() {
		return individualCountMax;
	}
	public Filters setIndividualCountMax(Integer individualCountMax) {
		this.individualCountMax = individualCountMax;
		return this;
	}
	public Filters setSecureReason(SecureReason secureReason) {
		if (this.secureReason == null) this.secureReason = new LinkedHashSet<>();
		this.secureReason.add(secureReason);
		return this;
	}
	public Set<SecureReason> getSecureReason() {
		return secureReason;
	}
	public Filters setSecureLevel(SecureLevel secureLevel) {
		if (this.secureLevel == null) this.secureLevel = new LinkedHashSet<>();
		this.secureLevel.add(secureLevel);
		return this;
	}
	public Set<SecureLevel> getSecureLevel() {
		return secureLevel;
	}
	public Boolean getSecured() {
		return secured;
	}
	public Filters setSecured(Boolean secured) {
		this.secured = secured;
		return this;
	}
	public Boolean getSensitive() {
		return sensitive;
	}
	public Filters setSensitive(Boolean sensitive) {
		this.sensitive = sensitive;
		return this;
	}
	public Boolean getFinnish() {
		return finnish;
	}
	public Filters setFinnish(Boolean finnish) {
		this.finnish = finnish;
		return this;
	}
	public Boolean isInvasive() {
		return invasive;
	}
	public Filters setInvasive(Boolean invasive) {
		this.invasive = invasive;
		return this;
	}
	public QualityIssues getQualityIssues() {
		return qualityIssues;
	}
	public Filters setQualityIssues(QualityIssues qualityIssues) {
		this.qualityIssues = qualityIssues;
		return this;
	}
	public Boolean getUnidentified() {
		return unidentified;
	}
	public Filters setUnidentified(Boolean unidentified) {
		this.unidentified = unidentified;
		return this;
	}
	public Boolean getNeedsCheck() {
		return needsCheck;
	}
	public Filters setNeedsCheck(Boolean needsCheck) {
		this.needsCheck = needsCheck;
		return this;
	}
	public Filters setRecordQuality(RecordQuality recordQuality) {
		if (this.recordQuality == null) this.recordQuality = new LinkedHashSet<>();
		this.recordQuality.add(recordQuality);
		if (recordQuality == RecordQuality.ERRONEOUS) {
			this.qualityIssues = QualityIssues.BOTH; // erroneous have an issue: if user wants to see erroneous, also show those with issues
		}
		return this;
	}
	public Set<RecordQuality> getRecordQuality() {
		return recordQuality;
	}
	public Filters setReliability(Reliability reliability) {
		if (this.reliability == null) this.reliability = new LinkedHashSet<>();
		this.reliability.add(reliability);
		if (reliability == Reliability.UNRELIABLE) {
			this.qualityIssues = QualityIssues.BOTH; // erroneous have an issue: if user wants to see erroneous, also show those with issues
		}
		return this;
	}
	public Set<Reliability> getReliability() {
		return reliability;
	}
	public Filters setCollectionAndRecordQuality(CollectionAndRecordQuality q) {
		if (this.collectionAndRecordQuality == null) this.collectionAndRecordQuality = new LinkedHashSet<>();
		this.collectionAndRecordQuality.add(q);
		if (q.getRecordQuality().contains(RecordQuality.ERRONEOUS)) {
			this.qualityIssues = QualityIssues.BOTH; // erroneous have an issue: if user wants to see erroneous, also show those with issues
		}
		return this;
	}
	public Set<CollectionAndRecordQuality> getCollectionAndRecordQuality() {
		return collectionAndRecordQuality;
	}
	public Filters setEffectiveTag(Tag effectiveTag) {
		if (this.effectiveTag == null) this.effectiveTag = new LinkedHashSet<>();
		this.effectiveTag.add(effectiveTag);
		return this;
	}
	public Set<Tag> getEffectiveTag() {
		return effectiveTag;
	}
	public Boolean getAnnotated() {
		return annotated;
	}
	public Filters setAnnotated(Boolean annotated) {
		this.annotated = annotated;
		return this;
	}
	public Filters setAnnotationType(AnnotationType annotationType) {
		if (this.annotationType == null) this.annotationType = new LinkedHashSet<>();
		this.annotationType.add(annotationType);
		return this;
	}
	public Set<AnnotationType> getAnnotationType() {
		return annotationType;
	}
	public Boolean getIncludeSystemAnnotations() {
		return includeSystemAnnotations;
	}
	public Filters setIncludeSystemAnnotations(Boolean includeSystemAnnotations) {
		this.includeSystemAnnotations = includeSystemAnnotations;
		return this;
	}
	public Date getAnnotatedSameOrAfter() {
		return annotatedSameOrAfter;
	}
	public Filters setAnnotatedSameOrAfter(Date d) {
		this.annotatedSameOrAfter = d;
		return this;
	}
	public Date getAnnotatedSameOrBefore() {
		return annotatedSameOrBefore;
	}
	public Filters setAnnotatedSameOrBefore(Date d) {
		this.annotatedSameOrBefore = d;
		return this;
	}
	public Boolean getHasSample() {
		return hasSample;
	}
	public Filters setHasSample(Boolean hasSample) {
		this.hasSample = hasSample;
		return this;
	}
	public Boolean getSampleMultiple() {
		return sampleMultiple;
	}
	public Filters setSampleMultiple(Boolean sampleMultiple) {
		this.sampleMultiple = sampleMultiple;
		return this;
	}
	public Set<Qname> getSampleType() {
		return sampleType;
	}
	public Filters setSampleType(Qname type) {
		if (this.sampleType == null) this.sampleType = new LinkedHashSet<>();
		this.sampleType.add(type);
		return this;
	}
	public Set<Qname> getSampleQuality() {
		return sampleQuality;
	}
	public Filters setSampleQuality(Qname quality) {
		if (this.sampleQuality == null) this.sampleQuality = new LinkedHashSet<>();
		this.sampleQuality.add(quality);
		return this;
	}
	public Set<Qname> getSampleStatus() {
		return sampleStatus;
	}
	public Filters setSampleStatus(Qname status) {
		if (this.sampleStatus == null) this.sampleStatus = new LinkedHashSet<>();
		this.sampleStatus.add(status);
		return this;
	}
	public Set<Qname> getSampleMaterial() {
		return sampleMaterial;
	}
	public Filters setSampleMaterial(Qname material) {
		if (this.sampleMaterial == null) this.sampleMaterial = new LinkedHashSet<>();
		this.sampleMaterial.add(material);
		return this;
	}
	public Set<String> getHasValue() {
		return hasValue;
	}
	public Filters setHasValue(String field) {
		if (this.hasValue == null) this.hasValue = new LinkedHashSet<>();
		this.hasValue.add(field);
		return this;
	}
	public Set<Qname> getAtlasCode() {
		return atlasCode;
	}
	public Filters setAtlasCode(Qname atlasCode) {
		if (this.atlasCode == null) this.atlasCode = new LinkedHashSet<>();
		this.atlasCode.add(atlasCode);
		return this;
	}
	public Set<Qname> getAtlasClass() {
		return atlasClass;
	}
	public Filters setAtlasClass(Qname atlasClass) {
		if (this.atlasClass == null) this.atlasClass = new LinkedHashSet<>();
		this.atlasClass.add(atlasClass);
		return this;
	}
	public Boolean getOnlyNonStateLands() {
		return onlyNonStateLands;
	}

	public Filters setOnlyNonStateLands(Boolean b) {
		this.onlyNonStateLands = b;
		return this;
	}

	private static final Map<String, Pair<Method, Class<?>>> SETTERS;
	static {
		SETTERS = new HashMap<>();
		for (Method method : ReflectionUtil.getSetters(Filters.class)) {
			String setterName = ReflectionUtil.cleanSetterFieldName(method);
			Class<?> parameterType = getParameterType(method);
			SETTERS.put(setterName, new Pair<Method, Class<?>>(method, parameterType));
		}
	}

	private static Class<?> getParameterType(Method setter) {
		Class<?>[] parameters = setter.getParameterTypes();
		if (parameters.length != 1) throw new IllegalStateException("Wrong number of parameters: " + setter.getName());
		return parameters[0];
	}

	private static final Map<String, Class<?>> GETTERS;
	static {
		GETTERS = new HashMap<>();
		for (Method method : ReflectionUtil.getGetters(Filters.class)) {
			String getterName = ReflectionUtil.cleanGetterFieldName(method);
			Class<?> parameterType = method.getReturnType();
			GETTERS.put(getterName, parameterType);
		}
	}


	private Filters parseParameter(String parameter, String... values) throws IllegalArgumentException {
		if (!SETTERS.containsKey(parameter)) throw new UnsupportedFilterException(parameter);
		FilterDefinition definition = filterDefinition(parameter);
		if (!this.base.includes(definition.base())) {
			throw new UnsupportedFilterException(parameter, this.base, definition.base());
		}
		if (!definition.useableInPublic() && warehouse == Concealment.PUBLIC && !(parameter.equals("editorOrObserverIdIsNot") && canUseEditorOrObserverIdIsNot)) {
			throw new UnsupportedFilterException(parameter, warehouse);
		}
		if (onlyStatistics && !definition.usableInStatistics()) {
			throw new UnsupportedFilterException(parameter, onlyStatistics);
		}
		Pair<Method, Class<?>> setterInfo = SETTERS.get(parameter);
		Method setter = setterInfo.getKey();
		Class<?> parameterClass = setterInfo.getValue();
		Class<?> getterClass = GETTERS.get(parameter);
		for (String value : values) {
			for (String valuePart : valueParts(value, getterClass, definition)) {
				valuePart = valuePart.trim();
				if (valuePart.isEmpty()) continue;
				Object parameterObject = toObject(valuePart, parameterClass);
				try {
					setter.invoke(this, parameterObject);
				} catch (Exception e) {
					if (e.getClass() != null) throw new UnsupportedFilterException(parameter, e.getCause());
					throw new UnsupportedFilterException(parameter, e);
				}
			}
		}
		return this;
	}

	private String[] valueParts(String value, Class<?> parameterClass, FilterDefinition definition) {
		if (Collection.class.isAssignableFrom(parameterClass)) {
			return value.split(Pattern.quote(definition.parameterSeparator()));
		}
		return new String[] {value};
	}

	private interface ValueConverter {
		Object convert(String value);
	}

	private static Map<Class<?>, ValueConverter> VALUE_CONVERTERS;

	static {
		VALUE_CONVERTERS = new HashMap<>();

		VALUE_CONVERTERS.put(String.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				return value;
			}
		});

		VALUE_CONVERTERS.put(Qname.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				if (value.startsWith("http")) {
					return Qname.fromURI(value);
				}
				return new Qname(value);
			}
		});

		VALUE_CONVERTERS.put(Integer.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				try {
					return Integer.valueOf(value);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid integer: " + value);
				}
			}
		});

		VALUE_CONVERTERS.put(Long.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				try {
					return Long.valueOf(value);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid long: " + value);
				}
			}
		});

		VALUE_CONVERTERS.put(Date.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				if (!value.contains("-")) {
					return parseEpochTimestamp(value);
				}
				return parseIsoDate(value);
			}

			private Date parseIsoDate(String value) {
				try {
					return DateUtils.convertToDate(value, "yyyy-MM-dd");
				} catch (ParseException e) {
					throw new IllegalArgumentException("Unparseable date: " + value, e);
				}
			}

			private Date parseEpochTimestamp(String value) {
				try {
					long epoch = Long.parseLong(value);
					return new Date(epoch * 1000);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Unparseable timestamp: " + value, e);
				}
			}
		});

		VALUE_CONVERTERS.put(Boolean.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				value = value.toLowerCase();
				if (value.equals("true")) return true;
				if (value.equals("false")) return false;
				throw new IllegalArgumentException("Invalid boolean: " + value);
			}
		});

		VALUE_CONVERTERS.put(CollectionAndRecordQuality.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				String[] parts = value.split(Pattern.quote(":"));
				if (parts.length != 2) throw new IllegalArgumentException("Invalid format for collectionAndRecordQuality: " + value);
				CollectionQuality collectionQuality = parseEnum(parts[0], CollectionQuality.class);
				CollectionAndRecordQuality quality = new CollectionAndRecordQuality(collectionQuality);
				for (String recordQuality : parts[1].split(Pattern.quote(","))) {
					quality.setRecordQuality(parseEnum(recordQuality, RecordQuality.class));
				}
				if (quality.getRecordQuality().isEmpty()) throw new IllegalArgumentException("Must give at least one record quality for collectionAndRecordQuality");
				return quality;
			}

			private <E extends Enum<E>> E parseEnum(String value, Class<E> enumClass) {
				if (value == null) throw new IllegalArgumentException("Invalid value for " + enumClass.getSimpleName());
				try {
					return Enum.valueOf(enumClass, value);
				} catch (Exception e) {
					throw new IllegalArgumentException("Invalid enumeration value " + value + " for " + enumClass.getSimpleName());
				}
			}

		});

		VALUE_CONVERTERS.put(Partition.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				return new Partition(value);
			}
		});

		VALUE_CONVERTERS.put(SingleCoordinates.class, new ValueConverter() {

			@Override
			public Object convert(String value) {
				try {
					String[] parts = value.split(Pattern.quote(":"));
					if (parts.length != 2) {
						throw new IllegalArgumentException("Invalid coordinate format: " + value);
					}
					double lat = Double.valueOf(parts[0].trim().replace(",", "."));
					double lon = Double.valueOf(parts[1].trim().replace(",", "."));
					return new SingleCoordinates(lat, lon, null);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Non-numeric coordinate values: " + value);
				}
			}
		});

		VALUE_CONVERTERS.put(CoordinatesWithOverlapRatio.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				try {
					String[] parts = value.split(Pattern.quote(":"));
					if (parts.length == 3 || parts.length == 4) {
						double lat = Double.valueOf(parts[0].trim());
						double lon = Double.valueOf(parts[1].trim());
						Coordinates.Type type = toCoordinateType(parts[2]);
						Double overlapRatio = parts.length == 4 ? toOverlapRatio(parts[3]) : null;
						return new CoordinatesWithOverlapRatio(lat, lon, type).setOverlapRatio(overlapRatio);
					}
					if (parts.length == 5 || parts.length == 6) {
						double latMin = Double.valueOf(parts[0].trim());
						double latMax = Double.valueOf(parts[1].trim());
						double lonMin = Double.valueOf(parts[2].trim());
						double lonMax = Double.valueOf(parts[3].trim());
						Coordinates.Type type = toCoordinateType(parts[4]);
						Double overlapRatio = parts.length == 6 ? toOverlapRatio(parts[5]) : null;
						return new CoordinatesWithOverlapRatio(latMin, latMax, lonMin, lonMax, type).setOverlapRatio(overlapRatio);
					}
					throw new IllegalArgumentException("Invalid coordinate format: " + value);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Non-numeric coordinate values: " + value + " (" + e.getMessage() + ")");
				} catch (DataValidationException e) {
					throw new IllegalArgumentException(e.getMessage(), e);
				}
			}
		});

		VALUE_CONVERTERS.put(PolygonSearch.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				try {
					String[] parts = value.split(Pattern.quote(":"));
					if (parts.length == 2) {
						String wkt = parts[0];
						String crs = parts[1];
						return new PolygonSearch(wkt, crs);

					}
					if (parts.length == 1) {
						String wkt = parts[0];
						return new PolygonSearch(wkt, Coordinates.Type.EUREF.name());
					}
					throw new IllegalArgumentException("Invalid polygon format: " + value);
				} catch (PolygonValidationException e) {
					throw new IllegalArgumentException(e.getMessage(), e);
				}
			}
		});

		VALUE_CONVERTERS.put(PolygonIdSearch.class, new ValueConverter() {
			@Override
			public Object convert(String value) {
				try {
					Long id = Long.valueOf(value);
					return new PolygonIdSearch(id);
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Invalid polygon id: " + value);
				}
			}
		});

	}

	private static Double toOverlapRatio(String s) {
		if (s == null) return null;
		s = s.trim();
		if (s.isEmpty()) return null;
		if (s.equals("null")) return null;
		try {
			return Double.valueOf(s);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid overlap ratio: " + s);
		}
	}

	private static Coordinates.Type toCoordinateType(String value) {
		try {
			return Coordinates.Type.valueOf(value.toUpperCase().trim());
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid CRS: " + value);
		}
	}

	private static Object toObject(String value, Class<?> parameterType) {
		if (parameterType.isEnum()) {
			return toEnum(value, parameterType);
		}
		ValueConverter valueConverter = VALUE_CONVERTERS.get(parameterType);
		if (valueConverter == null) throw new UnsupportedOperationException("No support yet for " + parameterType);
		return VALUE_CONVERTERS.get(parameterType).convert(value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Enum toEnum(String value, Class<?> parameterType) {
		try {
			return Enum.valueOf((Class<Enum>) parameterType, value.toUpperCase());
		} catch (Exception e) {
			// TODO remove legacy support
			if (parameterType == AnnotationType.class) {
				return AnnotationType.ALL;
			}
			// TODO remove legacy support
			if (parameterType == Wild.class) {
				return Wild.WILD_UNKNOWN;
			}
			throw new IllegalArgumentException("Invalid enumeration value: " + value);
		}
	}

	public boolean hasSetFiltersExludingDefaults() {
		return !getSetFiltersExcludingDefaults().isEmpty();
	}

	private static Map<Base, List<Field>> filterFields = new HashMap<>();

	private static List<Field> getFilterFields(Base base) {
		if (!filterFields.containsKey(base)) {
			initFilterFields(base);
		}
		return filterFields.get(base);
	}

	private synchronized static void initFilterFields(Base base) {
		if (filterFields.containsKey(base)) return;
		List<Field> fields = new ArrayList<>();
		for (Field field : Filters.class.getDeclaredFields()) {
			if (isFilterField(base, field)) {
				fields.add(field);
			}
		}
		filterFields.put(base, fields);
	}

	private static boolean isFilterField(Base base, Field field) {
		if (Modifier.isStatic(field.getModifiers())) return false;
		if (Modifier.isFinal(field.getModifiers())) return false;
		FilterDefinition filterDef = Filters.filterDefinition(field.getName());
		return base.includes(filterDef.base());
	}

	public Set<Filter> getSetFiltersIncludingDefaults() {
		Set<Filter> filters = new LinkedHashSet<>();
		for (Field field : getFilterFields(base)) {
			try {
				Object value = field.get(this);
				if (given(value)) {
					filters.add(new Filter(field.getName(), value, this));
				}
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		return filters;
	}

	public Set<Filter> getSetFiltersExcludingDefaults() {
		Set<Filter> filters = new LinkedHashSet<>();
		for (Field field : getFilterFields(base)) {
			try {
				Object value = field.get(this);
				if (given(value) && !isDefaultValue(field, value)) {
					filters.add(new Filter(field.getName(), value, this));
				}
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		return filters;
	}

	private static Map<Base, Filters> defaults = new HashMap<>();

	private static Filters getDefaults(Base base) {
		if (!defaults.containsKey(base)) {
			Filters defaultsSet = new Filters(Concealment.PRIVATE, base, false, true, false); // if defaults are in future defined to be different for private|public, static/nonstatic etc this must be refactored
			defaults.put(base, defaultsSet);
		}
		return defaults.get(base);
	}

	private boolean isDefaultValue(Field field, Object value) {
		try {
			Filters defaults = getDefaults(base);
			Object defaultValue = field.get(defaults);
			if (value == null) {
				return false;
			}
			return value.equals(defaultValue);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private boolean given(Object value) {
		if (value == null) return false;
		if (Collection.class.isAssignableFrom(value.getClass())) {
			return !((Collection<?>)value).isEmpty();
		}
		return true;
	}

	// Filters that should force a -> target -> taxon inner join must be listed here
	// Note that informalTaxonGroupIdIncludingReported is not a taxon filter (should not cause a inner join to taxon) because the filter is an OR query between unit.reportedInformalTaxonGroupId and taxon informalTaxonGroupId. We make a outer left join instead in vertica query imple.
	public boolean hasTaxonFilter() {
		return Boolean.FALSE.equals(includeNonValidTaxa) || taxonId != null ||
				finnish != null || invasive != null || taxonRankId != null || higherTaxon != null || sensitive != null ||
				administrativeStatusId != null || redListStatusId != null || taxonSetId != null ||
				typeOfOccurrenceId != null || typeOfOccurrenceIdNot != null ||
				primaryHabitat != null || anyHabitat != null ||
				informalTaxonGroupId != null || informalTaxonGroupIdNot != null ||
				occurrenceCountMax != null || occurrenceCountFinlandMax != null ||
				hasHasValueFilter("unit.linkings.originalTaxon.*") ||
				hasHasValueFilter("unit.linkings.taxon.*");
	}

	// TODO add support for hasValues - make the following check
	// hasTaxonFilter   -- limited to taxon id?   -- at least hasValues validation should not accept taxon array fields
	// hasTargetFilter  -- limited to taxon id?

	private boolean hasHasValueFilter(String field) {
		if (hasValue == null) return false;
		if (hasValue.contains(field)) return true;
		if (field.contains(".*")) {
			field = field.replace("*", "");
			for (String filterValue : hasValue) {
				if ((filterValue+".").startsWith(field)) return true;
			}
		}
		return false;
	}

	public boolean hasTargetFilter() {
		return target != null || Boolean.TRUE.equals(onlyNonValidTaxa);
	}

	public boolean hasNamedPlaceFilter() {
		return namedPlaceTag != null || hasHasValueFilter("document.namedPlace.*");
	}

	public boolean hasCollectionFilter() {
		if (collectionQuality != null) return true;
		if (hasHasValueFilter("document.linkings.collectionQuality")) return true;
		return false;
	}

	public boolean useIdentificationAnnotations() {
		return !Boolean.FALSE.equals(getUseIdentificationAnnotations());
	}

	public boolean hasVirvaTaxonFilters() {
		return Operator.OR.equals(taxonAdminFiltersOperator) && Taxon.isVirva(redListStatusId, administrativeStatusId);
	}

	public static Collection<FilterInfo> filterParameters(Base base) {
		List<FilterInfo> filters = new ArrayList<>();
		for (String filterName : SETTERS.keySet()) {
			FilterDefinition info = filterDefinition(filterName);
			if (base.includes(info.base())) {
				filters.add(new FilterInfo(filterName, info, filterType(filterName), enumerationValues(filterName)));
			}
		}
		Collections.sort(filters, new Comparator<FilterInfo>() {
			@Override
			public int compare(FilterInfo o1, FilterInfo o2) {
				return Double.compare(o1.getInfo().order(), o2.getInfo().order());
			}
		});
		return filters;
	}

	private static Class<?> filterType(String filter) {
		if (!GETTERS.containsKey(filter)) throw new IllegalStateException("Unmapped filter: " + filter);
		return GETTERS.get(filter);
	}

	private static List<String> enumerationValues(String filter) {
		Class<?> parameterClass = SETTERS.get(filter).getValue();

		if (!parameterClass.isEnum()) {
			return Collections.emptyList();
		}
		ArrayList<String> values = new ArrayList<>();
		for (Object enumValue : parameterClass.getEnumConstants()) {
			values.add(enumValue.toString());
		}
		return values;
	}

	public static FilterInfo filterInfo(String filterName) {
		FilterDefinition definition = filterDefinition(filterName);
		return new FilterInfo(filterName, definition, filterType(filterName), enumerationValues(filterName));
	}

	public static FilterDefinition filterDefinition(String filter) {
		try {
			Field field = Filters.class.getDeclaredField(filter);
			return field.getAnnotation(FilterDefinition.class);
		} catch (Exception e) {
			throw new IllegalStateException("Unmapped filter: " + filter);
		}
	}

	@Override
	public String toString() {
		Set<String> parameters = new LinkedHashSet<>();
		for (Filter f : getSetFiltersIncludingDefaults()) {
			parameters.add(f.toString());
		}
		return parameters.toString();
	}

	public Base base() {
		return base;
	}

	private final static Set<String> NON_ARRAY_UNIT_FIELDS = UnitSelectableFields.getNonArrayFields();

	public void validate() throws NoSuchFieldException {
		validateFilterToStringPresentationMaxLength();
		validateHasValueFilter();
	}

	private void validateHasValueFilter() throws NoSuchFieldException {
		if (getHasValue() == null) return;
		for (String fieldName : getHasValue()) {
			validateHasValueFilterField(fieldName, this);
		}
	}

	public static void validateHasValueFilterField(String fieldName, Filters filters) throws NoSuchFieldException {
		if (!Fields.aggregatable(filters.base()).isValidField(fieldName)) throw new NoSuchFieldException(fieldName, filters.base());
		if (!NON_ARRAY_UNIT_FIELDS.contains(fieldName)) throw new IllegalArgumentException(fieldName + " is not allowed for hasValue filter: it does not support array fields.");
		if (filters.useIdentificationAnnotations()) {
			if (fieldName.startsWith("unit.linkings.originalTaxon")) throw new IllegalArgumentException(fieldName + " is not allowed when using default taxon linking.");
		} else {
			if (fieldName.startsWith("unit.linkings.taxon")) throw new IllegalArgumentException(fieldName + " is not allowed when not using original taxon linking.");
		}
	}

	private void validateFilterToStringPresentationMaxLength() {
		for (Filter f : getSetFiltersIncludingDefaults()) {
			if (f.toString().length() >= 4000) throw new IllegalArgumentException("Too long value given for filter '" + f.getFieldName() + "'. Maximum is 4000 chars.");
		}
	}

}