package fi.laji.datawarehouse.query.model;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.query.model.CetafDocumentDescription.DescriptionField;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class CetafRdfXmlUtil {

	public static String generate(List<JoinedRow> rows, Map<String, CollectionMetadata> collectionMetadatas, Map<Qname, Area> areas) throws Exception {
		Document doc = createRootEntries();
		Node root = doc.getRootNode();
		for (List<JoinedRow> docRows :  getByDocument(rows)) {
			generateDocumentRows(collectionMetadatas, areas, root, docRows);
		}
		String xml = new XMLWriter(doc).generateXML(true);
		return xml;
	}

	private static void generateDocumentRows(Map<String, CollectionMetadata> collectionMetadatas, Map<Qname, Area> areas, Node root, List<JoinedRow> rows) throws Exception {
		CetafDocumentDescription documentDescription = createDocumentDescription(collectionMetadatas, rows.iterator().next());
		Node documentRdfDescription = createRdfDescription(documentDescription);
		for (JoinedRow row : rows) {
			documentRdfDescription.addChildNode("dc:hasPart").addAttribute("rdf:resource", row.getUnit().getUnitId().toURI());
		}
		root.addChildNode(documentRdfDescription);
		for (JoinedRow row : rows) {
			CetafUnitDescription unitDescription = createUnitDescription(collectionMetadatas, areas, row);
			Node unitRdfDescription = createRdfDescription(unitDescription);
			root.addChildNode(unitRdfDescription);
		}
	}

	private static Collection<List<JoinedRow>> getByDocument(List<JoinedRow> rows) {
		Map<Qname, List<JoinedRow>> map = new LinkedHashMap<>();
		for (JoinedRow row : rows) {
			Qname documentId = row.getDocument().getDocumentId();
			if (!map.containsKey(documentId)) {
				map.put(documentId, new ArrayList<>());
			}
			map.get(documentId).add(row);
		}
		return map.values();
	}

	private static CetafDocumentDescription createDocumentDescription(Map<String, CollectionMetadata> collectionMetadatas, JoinedRow row) {
		CollectionMetadata collectionMetadata = getCollectionMetadata(collectionMetadatas, row);
		return new CetafDocumentDescription(row, collectionMetadata);
	}

	private static CetafUnitDescription createUnitDescription(Map<String, CollectionMetadata> collectionMetadatas, Map<Qname, Area> areas, JoinedRow row) {
		CollectionMetadata collectionMetadata = getCollectionMetadata(collectionMetadatas, row);
		Qname countryId = row.getGathering().getInterpretations() == null ? null : row.getGathering().getInterpretations().getCountry();
		Area country = areas.get(countryId);
		return new CetafUnitDescription(row, collectionMetadata, country);
	}

	private static CollectionMetadata getCollectionMetadata(Map<String, CollectionMetadata> collectionMetadatas, JoinedRow row) {
		return collectionMetadatas.get(row.getDocument().getCollectionId().toURI());
	}

	private static Document createRootEntries() {
		Document doc = new Document("rdf:RDF");
		Node root = doc.getRootNode();
		root.addAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		root.addAttribute("xmlns:dc", "http://purl.org/dc/terms/");
		root.addAttribute("xmlns:owl", "http://www.w3.org/2002/07/owl");
		root.addAttribute("xmlns:geo", "http://www.w3.org/2003/01/geo/wgs84_pos#");
		root.addAttribute("xmlns:dwc", "http://rs.tdwg.org/dwc/terms/");
		root.addAttribute("xmlns:rdfschema", "http://www.w3.org/TR/2014/REC-rdf-schema-20140225/");
		root.addAttribute("xmlns:dwciri", "http://rs.tdwg.org/dwc/iri/");
		return doc;
	}

	private static <T extends CetafDocumentDescription> Node createRdfDescription(T description) throws Exception {
		Node node = new Node("rdf:Description");
		node.addAttribute("rdf:about", description.getIdentifier());
		for (Method m : getMethods(description)) {
			Object value = m.invoke(description);
			if (value == null) continue;
			DescriptionField desc = m.getAnnotation(DescriptionField.class);
			for (String predicate : desc.predicates()) {
				if (Collection.class.isAssignableFrom(value.getClass())) {
					for (Object o : (Collection<?>) value) {
						setValue(o, node, desc, predicate);
					}
				} else {
					setValue(value, node, desc, predicate);
				}
			}
		}
		return node;
	}

	private static void setValue(Object o, Node node, DescriptionField desc, String predicate) {
		if (desc.object() == CetafUnitDescription.Object.RESOURCE) {
			node.addChildNode(predicate).addAttribute("rdf:resource", o.toString());
		} else {
			if (o instanceof LocalizedText) {
				LocalizedText text = (LocalizedText) o;
				node.addChildNode(predicate).addAttribute("xml:lang", "en").setContents(text.forLocale("en"));
			} else {
				node.addChildNode(predicate).setContents(o.toString());
			}
		}
	}


	private static <T extends CetafDocumentDescription> List<Method> getMethods(T description) {
		if (description.getClass() == CetafDocumentDescription.class) return DOCUMENT_METHODS;
		return UNIT_METHODS;
	}

	private static final List<Method> DOCUMENT_METHODS = getMethods(CetafDocumentDescription.class);
	private static final List<Method> UNIT_METHODS = getMethods(CetafUnitDescription.class);

	private static List<Method> getMethods(Class<?> descriptionClass) {
		return ReflectionUtil.getGetters(descriptionClass).stream()
				.filter(m -> m.getAnnotation(DescriptionField.class) != null)
				.sorted(new Comparator<Method>() {
					@Override
					public int compare(Method m1, Method m2) {
						Double o1 = m1.getAnnotation(DescriptionField.class).order();
						Double o2 = m2.getAnnotation(DescriptionField.class).order();
						return o1.compareTo(o2);
					}
				})
				.collect(Collectors.toList());
	}

}
