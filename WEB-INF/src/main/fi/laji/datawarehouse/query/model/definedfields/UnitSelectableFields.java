package fi.laji.datawarehouse.query.model.definedfields;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationDwLinkings;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Person;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.StatisticsQueryAlllowedField;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.definedfields.Fields.DefinedFields;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.RedListStatus;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.ReflectionUtil;

public class UnitSelectableFields implements DefinedFields {

	private final Base base;
	protected final Set<String> fields;

	public UnitSelectableFields() {
		base = Base.UNIT;
		fields = Collections.unmodifiableSet(initCommon(Base.UNIT, Taxon.class, false));
	}

	public UnitSelectableFields(Set<String> fields, Base base) {
		this.base = base;
		this.fields = fields;
	}

	protected static Set<String> initCommon(Base base, Class<?> taxonClass, boolean onlyStatisticsFields) {
		Set<String> fields = new TreeSet<>();

		fields.addAll(getClassFields(Document.class, onlyStatisticsFields, taxonClass, base));

		if (base.includes(Base.GATHERING)) {
			fields.addAll(getClassFields(Gathering.class, onlyStatisticsFields, taxonClass, base));
		}

		if (base.includes(Base.UNIT)) {
			fields.addAll(getClassFields(Unit.class, onlyStatisticsFields, taxonClass, base));
		}

		Iterator<String> i = fields.iterator();
		while (i.hasNext()) {
			String field = i.next();
			if (field.startsWith("document.annotations.")) {
				if (field.contains(".identification.")) i.remove();
				if (field.contains(".occurrenceAtTimeOfAnnotation.")) i.remove();
			}
		}
		return fields;
	}

	private static final Set<Class<?>> PRIMARY_TYPES;
	static {
		PRIMARY_TYPES = new HashSet<>();
		PRIMARY_TYPES.add(String.class);
		PRIMARY_TYPES.add(Qname.class);
		PRIMARY_TYPES.add(Boolean.class);
		PRIMARY_TYPES.add(Integer.class);
		PRIMARY_TYPES.add(Double.class);
		PRIMARY_TYPES.add(Long.class);
		PRIMARY_TYPES.add(Date.class);
		PRIMARY_TYPES.add(int.class);
		PRIMARY_TYPES.add(long.class);
		PRIMARY_TYPES.add(double.class);
		PRIMARY_TYPES.add(boolean.class);
	}

	private static final Set<Class<?>> EMBEDDABLE_TYPES;
	static {
		EMBEDDABLE_TYPES = new HashSet<>();
		EMBEDDABLE_TYPES.add(DateRange.class);
		EMBEDDABLE_TYPES.add(GatheringConversions.class);
		EMBEDDABLE_TYPES.add(GatheringInterpretations.class);
		EMBEDDABLE_TYPES.add(UnitInterpretations.class);
		EMBEDDABLE_TYPES.add(DocumentQuality.class);
		EMBEDDABLE_TYPES.add(GatheringQuality.class);
		EMBEDDABLE_TYPES.add(UnitQuality.class);
		EMBEDDABLE_TYPES.add(Quality.class);
		EMBEDDABLE_TYPES.add(Coordinates.class);
		EMBEDDABLE_TYPES.add(SingleCoordinates.class);
		EMBEDDABLE_TYPES.add(DateRange.class);
		EMBEDDABLE_TYPES.add(Person.class);
		EMBEDDABLE_TYPES.add(TaxonCensus.class);
		EMBEDDABLE_TYPES.add(Taxon.class);
		EMBEDDABLE_TYPES.add(DocumentDWLinkings.class);
		EMBEDDABLE_TYPES.add(GatheringDWLinkings.class);
		EMBEDDABLE_TYPES.add(UnitDWLinkings.class);
		EMBEDDABLE_TYPES.add(Annotation.class);
		EMBEDDABLE_TYPES.add(Identification.class);
		EMBEDDABLE_TYPES.add(OccurrenceAtTimeOfAnnotation.class);
		EMBEDDABLE_TYPES.add(IdentificationDwLinkings.class);
		EMBEDDABLE_TYPES.add(Fact.class);
		EMBEDDABLE_TYPES.add(MediaObject.class);
		EMBEDDABLE_TYPES.add(TaxonBaseEntity.class);
		EMBEDDABLE_TYPES.add(Sample.class);
		EMBEDDABLE_TYPES.add(RedListStatus.class);
		EMBEDDABLE_TYPES.add(IdentificationEvent.class);
		EMBEDDABLE_TYPES.add(TypeSpecimen.class);
		EMBEDDABLE_TYPES.add(HabitatObject.class);
		EMBEDDABLE_TYPES.add(NamedPlaceEntity.class);
	}

	protected static List<String> getClassFields(Class<?> modelClass, boolean onlyStatisticsFields, Class<?> taxonClass, Base base) {
		String className = ReflectionUtil.caseName(modelClass.getSimpleName());
		return getClassFields(modelClass, className, onlyStatisticsFields, taxonClass, base);
	}

	public static Set<String> getNonArrayFields() {
		Set<String> fields = new TreeSet<>();
		fields.addAll(getNonArrayFields(Document.class));
		fields.addAll(getNonArrayFields(Gathering.class));
		fields.addAll(getNonArrayFields(Unit.class));
		fields.addAll(getNonArrayFields(TaxonBaseEntity.class, "unit.linkings.taxon"));
		fields.addAll(getNonArrayFields(TaxonBaseEntity.class, "unit.linkings.originalTaxon"));
		return Collections.unmodifiableSet(fields);
	}

	private static Collection<? extends String> getNonArrayFields(Class<?> modelClass) {
		return getNonArrayFields(modelClass, ReflectionUtil.caseName(modelClass.getSimpleName()));
	}

	private static Collection<? extends String> getNonArrayFields(Class<?> modelClass, String basePath) {
		List<String> fields = new ArrayList<>();
		for (Method method : ReflectionUtil.getGetters(modelClass)) {
			if (ignore(method)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(method);
			if (modelClass == Taxon.class) {
				continue;
			} else if (isPrimaryType(method)) {
				fields.add(basePath + "." + fieldName);
			} else if (isEmbeddableType(method)) {
				fields.addAll(getNonArrayFields(method.getReturnType(), basePath + "." + fieldName));
			}
		}
		return fields;
	}

	protected static List<String> getClassFields(Class<?> modelClass, String basePath, boolean onlyStatisticsFields, Class<?> taxonClass, Base base) {
		List<String> fields = new ArrayList<>();
		if (modelClass == Taxon.class) {
			modelClass = taxonClass;
		}
		for (Method method : ReflectionUtil.getGetters(modelClass)) {
			if (ignore(method)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(method);
			if (modelClass == Taxon.class) {
				if (!ModelToJson.INCLUDED_TAXON_FIELDS.contains(fieldName)) continue;
				if (isEmbeddableType(method)) {
					fields.addAll(getClassFields(method.getReturnType(), basePath + "." + fieldName, false, taxonClass, base));
				} else {
					fields.add(basePath + "." + fieldName);
				}
			} else if (isPrimaryType(method)) {
				if (onlyStatisticsFields && !isStatisticsQueryAllowed(method, base)) continue;
				fields.add(basePath + "." + fieldName);
			} else if (isEmbeddableType(method)) {
				if (onlyStatisticsFields && isStatisticsQueryAllowed(method, base)) {
					// Allow all under this embedabble type
					fields.addAll(getClassFields(method.getReturnType(), basePath + "." + fieldName, false, taxonClass, base));
				} else {
					fields.addAll(getClassFields(method.getReturnType(), basePath + "." + fieldName, onlyStatisticsFields, taxonClass, base));
				}
			} else if (isCollection(method)) {
				if (onlyStatisticsFields && !isStatisticsQueryAllowed(method, base)) continue;
				Class<?> collectionType = getCollectionParametrizedType(method);
				if (isEmbeddableType(collectionType)) {
					fields.addAll(getClassFields(collectionType, basePath + "." + fieldName, onlyStatisticsFields, taxonClass, base));
				} else {
					fields.add(basePath + "." + fieldName);
				}
			}
		}
		return fields;
	}

	private static Class<?> getCollectionParametrizedType(Method method) {
		ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
		java.lang.reflect.Type typeOfType = type.getActualTypeArguments()[0];
		if (!(typeOfType instanceof Class)) throw new IllegalStateException();
		return (Class<?>) typeOfType;
	}

	private static boolean ignore(Method method) {
		FieldDefinition f = method.getAnnotation(FieldDefinition.class);
		if (f == null) return false;
		return f.ignoreForQuery();
	}

	private static boolean isStatisticsQueryAllowed(Method method, Base base) {
		StatisticsQueryAlllowedField allowed = method.getAnnotation(StatisticsQueryAlllowedField.class);
		if (allowed == null) return false;
		return allowed.allowFor().includes(base);
	}

	private static boolean isEmbeddableType(Class<?> returnType) {
		return EMBEDDABLE_TYPES.contains(returnType);
	}

	public static boolean isEmbeddableType(Method method) {
		return isEmbeddableType(method.getReturnType());
	}

	public static boolean isCollection(Method method) {
		return Collection.class.isAssignableFrom(method.getReturnType());
	}

	public static boolean isPrimaryType(Method method) {
		Class<?> returnType = method.getReturnType();
		return isPrimaryType(returnType);
	}

	private static boolean isPrimaryType(Class<?> returnType) {
		if (returnType.equals(Concealment.class)) return false;
		if (returnType.isEnum()) return true;
		if (returnType.isPrimitive()) return true;
		return PRIMARY_TYPES.contains(returnType);
	}

	@Override
	public Set<String> getAllFields() {
		return fields;
	}

	@Override
	public void validateField(String field) throws NoSuchFieldException {
		if (!isValidField(field)) throw new NoSuchFieldException(field, base);
	}

	@Override
	public boolean isValidField(String field) {
		return fields.contains(field);
	}

}
