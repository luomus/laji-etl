package fi.laji.datawarehouse.query.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.Securer;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.Filters.Operator;
import fi.laji.datawarehouse.query.model.Filters.QualityIssues;
import fi.laji.datawarehouse.query.model.Filters.Wild;
import fi.luomus.commons.containers.rdf.Qname;

public class EnumToProperty {

	private static final Map<Enum<?>, List<Qname>> DEFINED = new LinkedHashMap<>();
	static {
		add(Coordinates.Type.WGS84, "MY.coordinateSystemWgs84");
		add(Coordinates.Type.EUREF, "MY.coordinateSystemEtrs-tm35fin");
		add(Coordinates.Type.YKJ, "MY.coordinateSystemYkj");

		add(MediaObject.MediaType.IMAGE, "MM.image");
		add(MediaObject.MediaType.AUDIO, "MM.audio");
		add(MediaObject.MediaType.VIDEO, "MM.video");
		add(MediaObject.MediaType.MODEL, "MM.model");

		add(Unit.Sex.MALE, "MY.sexM");
		add(Unit.Sex.FEMALE, "MY.sexF");
		add(Unit.Sex.WORKER, "MY.sexW");
		add(Unit.Sex.UNKNOWN, "MY.sexU");
		add(Unit.Sex.NOT_APPLICABLE, "MY.sexN");
		add(Unit.Sex.GYNANDROMORPH, "MY.sexX");
		add(Unit.Sex.MULTIPLE, "MY.sexE");
		add(Unit.Sex.CONFLICTING, "MY.sexC");

		add(Unit.LifeStage.ADULT, "MY.lifeStageAdult");
		add(Unit.LifeStage.JUVENILE, "MY.lifeStageJuvenile");
		add(Unit.LifeStage.IMMATURE, "MY.lifeStageImmature");
		add(Unit.LifeStage.EGG, "MY.lifeStageEgg");
		add(Unit.LifeStage.TADPOLE, "MY.lifeStageTadpole");
		add(Unit.LifeStage.PUPA, "MY.lifeStagePupa");
		add(Unit.LifeStage.NYMPH, "MY.lifeStageNymph");
		add(Unit.LifeStage.SUBIMAGO, "MY.lifeStageSubimago");
		add(Unit.LifeStage.LARVA, "MY.lifeStageLarva");
		add(Unit.LifeStage.SNAG, "MY.lifeStageSnag");
		add(Unit.LifeStage.EMBRYO, "MY.lifeStageEmbryo");
		add(Unit.LifeStage.SUBADULT, "MY.lifeStageSubadult");
		add(Unit.LifeStage.MATURE, "MY.lifeStageMature");
		add(Unit.LifeStage.GALL, "MY.lifeStageGall");
		add(Unit.LifeStage.MARKS, "MY.lifeStageMarks");
		add(Unit.LifeStage.TRIUNGULIN, "MY.lifeStageTriungulin");
		add(Unit.LifeStage.STERILE, "MY.plantLifeStageSterile", "MY.lifeStageSterile");
		add(Unit.LifeStage.FERTILE, "MY.plantLifeStageFertile", "MY.lifeStageFertile");
		add(Unit.LifeStage.SPROUT, "MY.plantLifeStageSprout");
		add(Unit.LifeStage.DEAD_SPROUT, "MY.plantLifeStageDeadSprout");
		add(Unit.LifeStage.BUD, "MY.plantLifeStageBud");
		add(Unit.LifeStage.FLOWER, "MY.plantLifeStageFlower");
		add(Unit.LifeStage.WITHERED_FLOWER, "MY.plantLifeStageWitheredFlower");
		add(Unit.LifeStage.SEED, "MY.plantLifeStageSeed", "MY.lifeStageSeed");
		add(Unit.LifeStage.RIPENING_FRUIT, "MY.plantLifeStageRipeningFruit");
		add(Unit.LifeStage.RIPE_FRUIT, "MY.plantLifeStageRipeFruit");
		add(Unit.LifeStage.SUBTERRANEAN, "MY.plantLifeStageSubterranean");

		add(Unit.AbundanceUnit.OCCURS_DOES_NOT_OCCUR, "MY.abundanceUnitOccursDoesNotOccur");
		add(Unit.AbundanceUnit.INDIVIDUAL_COUNT, "MY.abundanceUnitIndividualCount");
		add(Unit.AbundanceUnit.PAIRCOUNT, "MY.abundanceUnitPairCount");
		add(Unit.AbundanceUnit.NESTS, "MY.abundanceUnitNest");
		add(Unit.AbundanceUnit.BREEDING_SITES, "MY.abundanceUnitBreedingSite");
		add(Unit.AbundanceUnit.FEEDING_SITES, "MY.abundanceUnitFeedingSite");
		add(Unit.AbundanceUnit.COLONIES, "MY.abundanceUnitColony");
		add(Unit.AbundanceUnit.QUEENS, "MY.abundanceUnitQueen");
		add(Unit.AbundanceUnit.FRUITBODIES, "MY.abundanceUnitFruitbody");
		add(Unit.AbundanceUnit.SPROUTS, "MY.abundanceUnitSprout");
		add(Unit.AbundanceUnit.HUMMOCKS, "MY.abundanceUnitHummock");
		add(Unit.AbundanceUnit.THALLI, "MY.abundanceUnitThallus");
		add(Unit.AbundanceUnit.FLOWERS, "MY.abundanceUnitFlower");
		add(Unit.AbundanceUnit.SPOTS, "MY.abundanceUnitSpot");
		add(Unit.AbundanceUnit.TRUNKS, "MY.abundanceUnitTrunk");
		add(Unit.AbundanceUnit.SHELLS, "MY.abundanceUnitShell");
		add(Unit.AbundanceUnit.DROPPINGS, "MY.abundanceUnitDroppings");
		add(Unit.AbundanceUnit.FEEDING_MARKS, "MY.abundanceUnitMarks");
		add(Unit.AbundanceUnit.INDIRECT_MARKS, "MY.abundanceUnitIndirect");
		add(Unit.AbundanceUnit.SQUARE_DM, "MY.abundanceUnitSquareDM");
		add(Unit.AbundanceUnit.SQUARE_M, "MY.abundanceUnitSquareM");
		add(Unit.AbundanceUnit.RELATIVE_DENSITY, "MY.abundanceUnitRelativeDensity");

		add(Unit.RecordBasis.PRESERVED_SPECIMEN, "MY.recordBasisPreservedSpecimen");
		add(Unit.RecordBasis.LIVING_SPECIMEN, "MY.recordBasisLivingSpecimen");
		add(Unit.RecordBasis.FOSSIL_SPECIMEN, "MY.recordBasisFossilSpecimen");
		add(Unit.RecordBasis.SUBFOSSIL_SPECIMEN, "MY.recordBasisSubfossilSpecimen");
		add(Unit.RecordBasis.SUBFOSSIL_AMBER_INCLUSION_SPECIMEN, "MY.recordBasisSubfossilSpecimenAmberInclusion");
		add(Unit.RecordBasis.MICROBIAL_SPECIMEN, "MY.recordBasisMicrobialSpecimen");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, "MY.recordBasisHumanObservation");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_SEEN, "MY.recordBasisHumanObservationSeen");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_HEARD, "MY.recordBasisHumanObservationHeard");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_PHOTO, "MY.recordBasisHumanObservationPhoto");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_INDIRECT, "MY.recordBasisHumanObservationIndirect");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_HANDLED, "MY.recordBasisHumanObservationHandled");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_VIDEO, "MY.recordBasisHumanObservationVideo");
		add(Unit.RecordBasis.HUMAN_OBSERVATION_RECORDED_AUDIO, "MY.recordBasisHumanObservationAudio");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED, "MY.recordBasisMachineObservation");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_PHOTO, "MY.recordBasisMachineObservationPhoto");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_VIDEO, "MY.recordBasisMachineObservationVideo");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_AUDIO, "MY.recordBasisMachineObservationAudio");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_GEOLOGGER, "MY.recordBasisMachineObservationGeologger");
		add(Unit.RecordBasis.MACHINE_OBSERVATION_SATELLITE_TRANSMITTER, "MY.recordBasisMachineObservationSatelliteTransmitter");
		add(Unit.RecordBasis.LITERATURE, "MY.recordBasisLiterature");
		add(Unit.RecordBasis.MATERIAL_SAMPLE, "MY.recordBasisMaterialSample");
		add(Unit.RecordBasis.MATERIAL_SAMPLE_AIR, "MY.recordBasisMaterialSampleAir");
		add(Unit.RecordBasis.MATERIAL_SAMPLE_SOIL, "MY.recordBasisMaterialSampleSoil");
		add(Unit.RecordBasis.MATERIAL_SAMPLE_WATER, "MY.recordBasisMaterialSampleWater");

		add(DownloadType.CITABLE, "HBF.downloadTypeCitable");
		add(DownloadType.LIGHTWEIGHT, "HBF.downloadTypeLightweight");
		add(DownloadType.AUTHORITIES_FULL, "HBF.downloadTypeAuthoritiesFull");
		add(DownloadType.AUTHORITIES_LIGHTWEIGHT, "HBF.downloadTypeAuthoritiesLightweight");
		add(DownloadType.AUTHORITIES_API_KEY, "HBF.downloadTypeAuthoritiesApiKey");
		add(DownloadType.AUTHORITIES_VIRVA_GEOAPI_KEY, "HBF.downloadTypeAuthoritiesVirvaGeoApiKey");
		add(DownloadType.DATA_REQUEST, "HBF.downloadTypeDataRequest");
		add(DownloadType.APPROVED_DATA_REQUEST, "HBF.downloadTypeApprovedDataRequest");
		add(DownloadType.APPROVED_API_KEY_REQUEST, "HBF.downloadTypeApprovedApiKeyRequest");
		add(DownloadFormat.CSV_FLAT, "HBF.csvFlat");
		add(DownloadFormat.TSV_FLAT, "HBF.tsvFlat");
		add(DownloadFormat.EXCEL_FLAT, "HBF.excelFlat");
		add(DownloadFormat.ODS_FLAT, "HBF.odsFlat");
		add(DownloadInclude.DOCUMENT_FACTS, "HBF.documentFacts");
		add(DownloadInclude.DOCUMENT_MEDIA, "HBF.documentMedia");
		add(DownloadInclude.DOCUMENT_EDITORS, "HBF.documentEditors");
		add(DownloadInclude.DOCUMENT_KEYWORDS, "HBF.documentKeywords");
		add(DownloadInclude.GATHERING_FACTS, "HBF.gatheringFacts");
		add(DownloadInclude.GATHERING_MEDIA, "HBF.gatheringMedia");
		add(DownloadInclude.UNIT_FACTS, "HBF.unitFacts");
		add(DownloadInclude.UNIT_MEDIA, "HBF.unitMedia");

		add(SecureLevel.NONE, "MX.secureLevelNone");
		add(SecureLevel.KM1, "MX.secureLevelKM1");
		add(SecureLevel.KM5, "MX.secureLevelKM5");
		add(SecureLevel.KM10, "MX.secureLevelKM10");
		add(SecureLevel.KM25, "MX.secureLevelKM25");
		add(SecureLevel.KM50, "MX.secureLevelKM50");
		add(SecureLevel.KM100, "MX.secureLevelKM100");
		add(SecureLevel.HIGHEST, "MX.secureLevelHighest");
		add(SecureLevel.NOSHOW, "MX.secureLevelNoShow");

		add(SecureReason.DEFAULT_TAXON_CONSERVATION, "MZ.secureReasonTaxonConservation");
		add(SecureReason.BREEDING_SITE_CONSERVATION, "MZ.secureReasonBreedingSiteConservation");
		add(SecureReason.NATURA_AREA_CONSERVATION, "MZ.secureReasonNaturaAreaConservation");
		add(SecureReason.WINTER_SEASON_TAXON_CONSERVATION, "MZ.secureReasonWinterSeasonTaxonConservation");
		add(SecureReason.BREEDING_SEASON_TAXON_CONSERVATION, "MZ.secureReasonBreedingSeasonTaxonConservation");
		add(SecureReason.CUSTOM, "MZ.secureReasonCustom");
		add(SecureReason.USER_HIDDEN, "MZ.secureReasonUserHidden");
		add(SecureReason.USER_HIDDEN_LOCATION, "MZ.secureReasonUserHiddenLocation");
		add(SecureReason.USER_HIDDEN_TIME, "MZ.secureReasonUserHiddenTime");
		add(SecureReason.USER_PERSON_NAMES_HIDDEN, "MZ.secureReasonPersonNamesHidden");
		add(SecureReason.ADMIN_HIDDEN, "MZ.secureReasonAdminHidden");
		add(SecureReason.DATA_QUARANTINE_PERIOD, "MZ.secureReasonDataQuarantinePeriod");
		add(SecureReason.ONLY_PRIVATE, "MZ.secureReasonOnlyPrivate");

		add(GeoSource.COORDINATES, "MZ.geoSourceCoordinates");
		add(GeoSource.COORDINATE_CENTERPOINT, "MZ.geoSourceCoordinateCenterpoint");
		add(GeoSource.FINNISH_MUNICIPALITY, "MZ.geoSourceFinnishMunicipality");
		add(GeoSource.OLD_FINNISH_MUNICIPALITY, "MZ.geoSourceOldFinnishMunicipality");
		add(GeoSource.REPORTED_VALUE, "MZ.geoSourceReportedValue");

		add(InvasiveControl.FULL, "MY.invasiveControlEffectivenessFull");
		add(InvasiveControl.PARTIAL, "MY.invasiveControlEffectivenessPartial");
		add(InvasiveControl.NO_EFFECT, "MY.invasiveControlEffectivenessNone");
		add(InvasiveControl.NOT_FOUND, "MY.invasiveControlEffectivenessNotFound");

		add(TaxonConfidence.SURE, "MY.taxonConfidenceSure");
		add(TaxonConfidence.UNSURE, "MY.taxonConfidenceUnsure");
		add(TaxonConfidence.SUBSPECIES_UNSURE, "MY.taxonConfidenceSubspeciesUnsure");

		add(Quality.Issue.REPORTED_UNRELIABLE, "MZ.qualityIssueReportedUnreliable");
		add(Quality.Issue.MEDIA_ISSUE, "MZ.qualityIssueMedia");
		add(Quality.Issue.INVALID_CREATED_DATE, "MZ.qualityIssueCreatedDate");
		add(Quality.Issue.INVALID_MODIFIED_DATE, "MZ.qualityIssueModifiedDate");
		add(Quality.Issue.COORDINATES_COUNTRY_MISMATCH, "MZ.qualityIssueCoordinatesCountryMismatch");
		add(Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH, "MZ.qualityIssueMunicipalityMismatch");
		add(Quality.Issue.INVALID_GEO, "MZ.qualityIssueGeo");
		add(Quality.Issue.INVALID_COORDINATES, "MZ.qualityIssueCoordinates");
		add(Quality.Issue.INVALID_YKJ_COORDINATES, "MZ.qualityIssueYkjCoordinates");
		add(Quality.Issue.INVALID_EUREF_COORDINATES, "MZ.qualityIssueEurefCoordinates");
		add(Quality.Issue.INVALID_WGS84_COORDINATES, "MZ.qualityIssueWgs84Coordinates");
		add(Quality.Issue.TOO_LARGE_AREA, "MZ.qualityIssueTooLargeArea");
		add(Quality.Issue.DATE_END_BEFORE_BEGIN, "MZ.qualityIssueDateEndBeforeBegin");
		add(Quality.Issue.DATE_END_GIVEN_WITHOUT_BEGIN, "MZ.qualityIssueDateEndWithoutBegin");
		add(Quality.Issue.DATE_IN_FUTURE, "MZ.qualityIssueDateInFuture");
		add(Quality.Issue.DATE_TOO_FAR_IN_THE_PAST, "MZ.qualityIssueDateTooFarInThePast");
		add(Quality.Issue.INVALID_DATE, "MZ.qualityIssueInvalidDate");
		add(Quality.Issue.RECORD_BASIS_MISSING, "MZ.qualityIssueRecordBasisMissing");
		add(Quality.Issue.INVALID_HOUR, "MZ.qualityIssueInvalidHour");
		add(Quality.Issue.INVALID_MINUTE, "MZ.qualityIssueInvalidMinute");
		add(Quality.Issue.TIME_END_BEFORE_BEGIN, "MZ.qualityIssueTimeEndBeforeBegin");
		add(Quality.Issue.ETL_ISSUE, "MZ.qualityIssueDataTransfer");

		add(Quality.Source.AUTOMATED_FINBIF_VALIDATION, "MZ.qualitySourceValidation");
		add(Quality.Source.ORIGINAL_DOCUMENT, "MZ.qualitySourceOriginalDocument");
		add(Quality.Source.QUALITY_CONTROL, "MZ.qualitySourceQualityControl");

		add(QualityIssues.NO_ISSUES, "MZ.qualityIssueFilterNoIssues");
		add(QualityIssues.BOTH, "MZ.qualityIssueFilterBoth");
		add(QualityIssues.ONLY_ISSUES, "MZ.qualityIssueFilterOnlyIssues");

		add(AnnotationType.ALL, "MZ.annotationTypeFilterAll");
		add(AnnotationType.COMMENT, "MZ.annotationTypeFilterTypeComment");
		add(AnnotationType.ADMIN, "MZ.annotationTypeFilterTypeAdmin");
		add(AnnotationType.FORM_ADMIN, "MZ.annotationTypeFilterTypeFormAdmin");
		add(AnnotationType.DW_AUTO, "MZ.annotationTypeFilterTypeAuto");
		add(AnnotationType.USER_CHECK, "MZ.annotationTypeFilterTypeCheck");
		add(AnnotationType.USER_EFFECTIVE, "MZ.annotationTypeFilterTypeEffective");
		add(AnnotationType.INVASIVE_CONTROL, "MAN.typeInvasiveControlEffectiveness");
		add(AnnotationType.DELETED, "MZ.annotationTypeFilterTypeDeleted");

		add(Wild.WILD, "MY.wildWild");
		add(Wild.WILD_UNKNOWN, "MY.wildUnknown");
		add(Wild.NON_WILD, "MY.wildNonWild");

		add(CollectionQuality.AMATEUR, "MY.collectionQualityEnum1");
		add(CollectionQuality.HOBBYIST, "MY.collectionQualityEnum2");
		add(CollectionQuality.PROFESSIONAL, "MY.collectionQualityEnum3");

		add(RecordQuality.EXPERT_VERIFIED, "MZ.recordQualityExpertVerified");
		add(RecordQuality.COMMUNITY_VERIFIED, "MZ.recordQualityCommunityVerified");
		add(RecordQuality.NEUTRAL, "MZ.recordQualityNeutral");
		add(RecordQuality.UNCERTAIN, "MZ.recordQualityUncertain");
		add(RecordQuality.ERRONEOUS, "MZ.recordQualityErroneous");

		add(Reliability.RELIABLE, "MZ.reliabilityReliable");
		add(Reliability.UNDEFINED, "MZ.reliabilityNeutral");
		add(Reliability.UNRELIABLE, "MZ.reliabilityUnreliable");

		add(Tag.ADMIN_MARKED_SPAM, "MMAN.3");
		add(Tag.ADMIN_MARKED_COARSE, "MMAN.32");
		add(Tag.ADMIN_MARKED_NON_WILD, "MMAN.33");
		add(Tag.EXPERT_TAG_VERIFIED, "MMAN.5");
		add(Tag.EXPERT_TAG_UNCERTAIN, "MMAN.8");
		add(Tag.EXPERT_TAG_ERRONEOUS, "MMAN.9");
		add(Tag.COMMUNITY_TAG_VERIFIED, "MMAN.52");
		add(Tag.CHECKED_CANNOT_VERIFY, "MMAN.18");
		add(Tag.CHANGED_OWNER_MANUAL, "MMAN.10");
		add(Tag.CHANGED_DW_AUTO, "MMAN.30");
		add(Tag.CHECK, "MMAN.31");
		add(Tag.CHECK_COORDINATES, "MMAN.22");
		add(Tag.CHECK_DATETIME, "MMAN.23");
		add(Tag.CHECK_LOCATION, "MMAN.24");
		add(Tag.CHECK_OBSERVER, "MMAN.25");
		add(Tag.CHECK_TAXON, "MMAN.26");
		add(Tag.CHECK_WILDNESS, "MMAN.29");
		add(Tag.CHECK_DUPLICATE, "MMAN.28");
		add(Tag.CHECK_NEEDS_INFO, "MMAN.20");
		add(Tag.CHECK_SPAM, "MMAN.34");
		add(Tag.CHECK_BREEDING_INDEX, "MMAN.53");
		add(Tag.AUTO_DISTRIBUTION_CHECK, "MMAN.54");
		add(Tag.AUTO_PERIOD_CHECK, "MMAN.55");
		add(Tag.AUTO_VALIDATIONS_PASS, "MMAN.56");
		add(Tag.FORMADMIN_CENSUS_COUNT_ERROR, "MMAN.11");
		add(Tag.FORMADMIN_CENSUS_INNER_COUNT_ERROR, "MMAN.12");
		add(Tag.FORMADMIN_CENSUS_OTHER_ERROR, "MMAN.13");
		add(Tag.FORMADMIN_VERIFIED, "MMAN.50");
		add(Tag.FORMADMIN_UNCERTAIN, "MMAN.51");
		add(Tag.INVASIVE_FULL, "MMAN.14");
		add(Tag.INVASIVE_PARTIAL, "MMAN.15");
		add(Tag.INVASIVE_NO_EFFECT, "MMAN.16");
		add(Tag.INVASIVE_NOT_FOUND, "MMAN.17");

		add(Operator.AND, "MZ.operatorAnd");
		add(Operator.OR, "MZ.operatorOr");
	}

	private static void add(Enum<?> type, String ... qnames) {
		for (Enum<?> e : DEFINED.keySet()) {
			if (e.name().equals(type.name())) {
				throw new IllegalStateException("Enum value has two meanings: " + type.name()); // Note: dw /enumeration-labels/ API requires that each enum name is unique
			}
		}
		List<Qname> list = new ArrayList<>();
		for (String qname : qnames) {
			list.add(new Qname(qname));
		}
		DEFINED.put(type, list);
	}

	private static final EnumToProperty instance = new EnumToProperty();

	public static EnumToProperty getInstance() {
		return instance;
	}

	private EnumToProperty() {
		validateEnums(Securer.class);
		validateEnums(Coordinates.class);
		validateEnums(MediaObject.class);
		validateEnums(Unit.class);
		validateEnums(DownloadRequest.class);
		validateEnums(GatheringInterpretations.class);
		validateEnums(Quality.class);
		validateEnums(Filters.class);
	}

	public List<Qname> getAll(Enum<?> enumeration) {
		if (enumeration == null) throw new IllegalArgumentException("Null enum value given");
		return Collections.unmodifiableList(DEFINED.get(enumeration));
	}

	public Qname get(Enum<?> enumeration) {
		return DEFINED.get(enumeration).get(0);
	}

	public Qname get(String enumerationName) {
		Enum<?> e = getEnumerationByName(enumerationName);
		return get(e);
	}

	public Enum<?> getEnumerationByName(String enumerationName) {
		for (Enum<?> e : DEFINED.keySet()) {
			if (e.name().equals(enumerationName)) return e;
		}
		throw new IllegalArgumentException("Unknown enumeration " + enumerationName);
	}

	public Enum<?> get(Qname qname) {
		Enum<?> e = getEnumOrNull(qname);
		if (e != null) return e;
		throw new IllegalArgumentException("Unknown enumeration id " + qname);
	}

	public Enum<?> getEnumOrNull(Qname qname) {
		for (Map.Entry<Enum<?>, List<Qname>> e : DEFINED.entrySet()) {
			if (e.getValue().contains(qname)) return e.getKey();
		}
		return null;
	}

	public Collection<Enum<?>> getEnumerations() {
		return Collections.unmodifiableCollection(DEFINED.keySet());
	}

	private void validateEnums(Class<?> c) {
		for (Field f : c.getDeclaredFields()) {
			if (f.getType().equals(Base.class)) continue;
			if (f.getType().equals(Concealment.class)) continue;
			if (!f.getType().isEnum()) continue;
			Class<?> e = f.getType();
			for (Object o : e.getEnumConstants()) {
				List<Qname> qnames = DEFINED.get(o);
				if (qnames == null || qnames.isEmpty()) throw new IllegalStateException("Enum " + o + " is not mapped to rdf property");
			}
		}
	}

}
