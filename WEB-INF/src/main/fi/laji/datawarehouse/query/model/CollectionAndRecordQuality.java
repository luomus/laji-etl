package fi.laji.datawarehouse.query.model;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;

public class CollectionAndRecordQuality {

	private final CollectionQuality collectionQuality;
	private final Set<RecordQuality> recordQuality = new TreeSet<>();

	public CollectionAndRecordQuality(CollectionQuality collectionQuality) {
		this.collectionQuality = collectionQuality;
	}

	public CollectionQuality getCollectionQuality() {
		return collectionQuality;
	}

	public Set<RecordQuality> getRecordQuality() {
		return Collections.unmodifiableSet(recordQuality);
	}


	public CollectionAndRecordQuality setRecordQuality(RecordQuality recordQuality) {
		this.recordQuality.add(recordQuality);
		return this;
	}

	@Override
	public String toString() {
		return collectionQuality.name() + ":" +
				recordQuality.stream().map(e->e.name()).collect(Collectors.joining(","));
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectionAndRecordQuality other = (CollectionAndRecordQuality) obj;
		if (getCollectionQuality() != other.getCollectionQuality()) return false;
		return recordQuality.equals(other.getRecordQuality());
	}

}
