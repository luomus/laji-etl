package fi.laji.datawarehouse.query.model.queries;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.Filters;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class BaseQuery {

	private final String guid;
	private final Long timestamp;
	private Concealment warehouse;
	private final Base base;
	private final String apiSourceId;
	private final Class<?> caller;
	private final String userAgent;
	private Qname personId;
	private String personEmail;
	private boolean useCache;
	private final Filters filters;
	private Filters permissionFilters;
	private Qname permissionId;
	private final Coordinates.Type crs;
	private final FeatureType featureType;
	private boolean approvedDataRequest = false;

	public BaseQuery(BaseQueryBuilder builder) {
		this.timestamp = System.currentTimeMillis();
		this.guid = generateId();
		this.warehouse = builder.getWarehouse();
		this.base = builder.getBase();
		this.caller = builder.getCaller();
		this.userAgent = builder.getUserAgent();
		this.apiSourceId = builder.getApiSourceId();
		this.personId = builder.getPersonId();
		this.personEmail = builder.getPersonEmail();
		this.useCache = builder.getUseCache();
		this.crs = builder.getCrs();
		this.featureType = builder.getFeatureType();
		this.filters = Filters.createFilters(warehouse, base, builder.getOnlyStatisticsFilters(), builder.getSetDefaultFilters(), builder.getApiSourceIdCanUseEditorOrObserverIdIsNot());
		if (!isValidEmailAddress(personEmail)) {
			throw new IllegalArgumentException("User must have a valid email address: '" + personEmail + "' is not.");
		}
		notNull(warehouse, base, caller, apiSourceId);
	}

	private void notNull(Object ...objects) {
		int i = 1;
		for (Object o : objects) {
			if (o == null) throw new IllegalArgumentException("Argument " + i + " is null");
			i++;
		}
	}

	public BaseQuery(BaseQuery baseQuery) {
		this.timestamp = System.currentTimeMillis();
		this.guid = generateId();
		this.warehouse = baseQuery.warehouse;
		this.base = baseQuery.base;
		this.caller = baseQuery.caller;
		this.userAgent = baseQuery.userAgent;
		this.apiSourceId = baseQuery.apiSourceId;
		this.personId = baseQuery.personId;
		this.personEmail = baseQuery.personEmail;
		this.filters = baseQuery.filters;
		this.useCache = baseQuery.useCache;
		this.permissionFilters = baseQuery.permissionFilters;
		this.permissionId = baseQuery.permissionId;
		this.crs = baseQuery.crs;
		this.featureType = baseQuery.featureType;
		this.approvedDataRequest = baseQuery.approvedDataRequest;
	}

	private String generateId() {
		return Utils.generateGUID();
	}

	public Concealment getWarehouse() {
		return warehouse;
	}

	public BaseQuery setWarehouse(Concealment warehouse) {
		this.warehouse = warehouse;
		return this;
	}

	public String getApiSourceId() {
		return apiSourceId;
	}

	public Qname getPersonId() {
		return personId;
	}

	public BaseQuery setPersonId(Qname personId) {
		this.personId = personId;
		return this;
	}

	public String getPersonEmail() {
		return personEmail;
	}

	public BaseQuery setPersonEmail(String email) {
		this.personEmail = email;
		return this;
	}

	public Filters getFilters() {
		return filters;
	}

	public boolean useCache() {
		return useCache;
	}

	public BaseQuery setUseCache(boolean useCache) {
		this.useCache = useCache;
		return this;
	}

	public Base getBase() {
		return base;
	}

	public BaseQuery setPermissionFilters(Filters permissionFilters) {
		this.permissionFilters = permissionFilters;
		if (permissionFilters != null) {
			this.approvedDataRequest = true;
		}
		return this;
	}

	public Filters getPermissionFilters() {
		return permissionFilters;
	}

	public BaseQuery setPermissionId(Qname permissionId) {
		this.permissionId = permissionId;
		return this;
	}

	public Qname getPermissionId() {
		return permissionId;
	}

	public boolean isApprovedDataRequest() {
		return approvedDataRequest;
	}

	public void setApprovedDataRequest(boolean approvedDataRequest) {
		this.approvedDataRequest = approvedDataRequest;
	}

	@Override
	public String toString() {
		return "BaseQuery [filters=" + filters + ", permissionFilters=" + permissionFilters + ", approvedDataRequest=" + approvedDataRequest + ", warehouse=" + warehouse + ", base=" + base + ", apiSourceId=" + apiSourceId + ", caller=" + caller + ", userAgent=" + userAgent + ", userId=" + personId + ", userEmail=" + personEmail + ", permissionId=" + permissionId + ", crs=" + crs + ", featureType=" + featureType + ", useCache=" + useCache + "]";
	}

	public String toComparisonString() {
		return "BaseQuery [filters=" + filters + ", permissionFilters=" + permissionFilters + ", approvedDataRequest=" + approvedDataRequest + ", warehouse=" + warehouse + ", base=" + base + "]";
	}

	public boolean isPrivate() {
		return Concealment.PRIVATE == warehouse;
	}

	public boolean isPublic() {
		return !isPrivate();
	}

	private static boolean isValidEmailAddress(String email) {
		if (!given(email)) return true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
			return true;
		} catch (AddressException ex) {
			return false;
		}
	}

	private static boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getGUID() {
		return guid;
	}

	public Class<?> getCaller() {
		return caller;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public Coordinates.Type getCrs() {
		return crs;
	}

	public FeatureType getFeatureType() {
		return featureType;
	}

	public void validateGeoJSON() {
		if (crs == null && featureType == null) return;
		if (crs == null || featureType == null)
			throw new IllegalArgumentException("Must give both " + Const.CRS + " and " + Const.FEATURE_TYPE + " parameters");
	}

}
