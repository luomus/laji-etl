package fi.laji.datawarehouse.query.model.responses;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.luomus.commons.utils.DateUtils;

public class BaseResponse<T extends BaseResponse<T>> {

	private final long total;
	private Long cacheTimestamp;

	public BaseResponse(long total) {
		this.total = total;
	}

	@FieldDefinition(order=1)
	public long getTotal() {
		return total;
	}

	@FieldDefinition(order=999)
	public Long getCacheTimestamp() {
		return cacheTimestamp;
	}

	@SuppressWarnings("unchecked")
	public T setCacheTimestamp() {
		cacheTimestamp = DateUtils.getCurrentEpoch();
		return (T) this;
	}

}
