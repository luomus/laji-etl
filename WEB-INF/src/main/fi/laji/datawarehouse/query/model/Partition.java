package fi.laji.datawarehouse.query.model;

import java.util.regex.Pattern;

import fi.luomus.commons.utils.Utils;

public class Partition {

	private final int index;
	private final int size;

	public Partition(String partition) {
		if (partition == null) throw new IllegalArgumentException("Null partition value given");
		if (Utils.countNumberOf("/", partition) != 1) throw new IllegalArgumentException("Use syntax 1/5 where 1 is index (1 based) and 5 the number of partitions");
		String[] parts = partition.split(Pattern.quote("/"));
		index = iVal(parts[0]);
		size = iVal(parts[1]);
		if (index < 1 || index > size) throw new IllegalArgumentException("Index must be between 1 and number of partitions");
		if (size < 1) throw new IllegalArgumentException("Number of partitions must be greater than 0");
		if (size > 10000) throw new IllegalArgumentException("Too large number of partitions");
	}

	private int iVal(String string) {
		try {
			return Integer.valueOf(string);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid number " + string);
		}
	}

	public int getIndex() {
		return index;
	}

	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		return index+"/"+size;
	}

}
