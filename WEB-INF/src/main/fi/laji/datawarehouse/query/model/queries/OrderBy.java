package fi.laji.datawarehouse.query.model.queries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.definedfields.Fields;

public class OrderBy {

	public static enum Order { ASC, DESC }

	public static class OrderCriteria {
		private final String field;
		private final Order order;
		public OrderCriteria(String field, Order order) {
			this.field = field;
			this.order = order;
		}
		public String getField() {
			return field;
		}
		public Order getOrder() {
			return order;
		}
		@Override
		public String toString() {
			return field + " " + order;
		}
		@Override
		public int hashCode() {
			return field.hashCode();
		}
		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null)
				return false;
			if (getClass() != o.getClass())
				return false;
			OrderCriteria other = (OrderCriteria) o;
			return field.equals(other.field);
		}
	}

	private final Base base;
	private final List<OrderCriteria> orderCriterias = new ArrayList<>();

	/**
	 * Default base: unit
	 * @param ascendingFields
	 * @throws NoSuchFieldException
	 */
	public OrderBy(String ... ascendingFields) throws NoSuchFieldException {
		this(Base.UNIT, ascendingFields);
	}

	public OrderBy(Base base) {
		this.base = base;
	}

	public OrderBy(Base base, String ... ascendingFields) throws NoSuchFieldException {
		this(base);
		for (String field : ascendingFields) {
			addAscending(field);
		}
	}

	public OrderBy add(OrderCriteria orderCriteria) throws NoSuchFieldException {
		String field = orderCriteria.getField();
		if (validField(field)) {
			if (!orderCriterias.contains(orderCriteria)) {
				orderCriterias.add(orderCriteria);
			}
			return this;
		}
		throw new NoSuchFieldException(field, base);
	}

	private boolean validField(String field) {
		if (field.startsWith(Const.RANDOM)) return true;
		return Fields.sortable(base).isValidField(field) || Fields.aggregatable(base).isValidField(field);
	}

	public OrderBy addAscending(String field) throws NoSuchFieldException {
		add(new OrderCriteria(field, Order.ASC));
		return this;
	}

	public OrderBy addDescending(String field) throws NoSuchFieldException {
		add(new OrderCriteria(field, Order.DESC));
		return this;
	}

	public boolean hasValues() {
		return !orderCriterias.isEmpty();
	}

	@Override
	public String toString() {
		return orderCriterias.toString();
	}

	public List<OrderCriteria> getOrderCriterias() {
		return orderCriterias;
	}

	public static OrderCriteria toOrderCriteria(String param) {
		OrderBy.Order order = Order.ASC;
		if (param.contains(" ")) {
			String[] parts = param.split(Pattern.quote(" "));
			param = parts[0];
			String ascOrDesc = parts[1].toLowerCase();
			if (ascOrDesc.equals("desc") || ascOrDesc.equals("descending")) {
				order = Order.DESC;
			}
		}
		return new OrderCriteria(param, order);
	}

	public Collection<String> getFieldNames() {
		Set<String> fields = new HashSet<>();
		for (OrderCriteria c : orderCriterias) {
			fields.add(c.getField());
		}
		return fields;
	}

	public Base getBase() {
		return base;
	}

}
