package fi.laji.datawarehouse.query.model;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonSearchUtil;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonValidationException;
import fi.luomus.commons.utils.Utils;

public class PolygonSearch {

	private final String wkt;

	public PolygonSearch(String wkt, String crs) throws PolygonValidationException {
		this.wkt = new PolygonSearchUtil(crs, null, wkt).getAndValidateEurefWKT();
	}

	public PolygonSearch(String wkt) {
		this.wkt = wkt;
	}

	public String getWKT() {
		return wkt;
	}

	@Override
	public String toString() {
		return Utils.debugS(wkt, Type.EUREF);
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return toString().equals(obj.toString());
	}

}
