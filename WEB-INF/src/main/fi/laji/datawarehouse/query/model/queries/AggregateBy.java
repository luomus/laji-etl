package fi.laji.datawarehouse.query.model.queries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.definedfields.Fields;

public class AggregateBy {

	private final List<String> fields = new ArrayList<>();
	private final Base base;
	private final boolean isStatisticsQuery;

	/**
	 * Default base: unit
	 */
	public AggregateBy() {
		this(Base.UNIT, false);
	}

	/**
	 * Default base: unit
	 * @param fields
	 * @throws NoSuchFieldException
	 */
	public AggregateBy(String ... fields) throws NoSuchFieldException {
		this(Base.UNIT, false, fields);
	}

	public AggregateBy(Base base, boolean isStatisticsQuery) {
		this.base = base;
		this.isStatisticsQuery = isStatisticsQuery;
	}

	public AggregateBy(Base base, boolean isStatisticsQuery, String ... fields) throws NoSuchFieldException {
		this(base, isStatisticsQuery);
		for (String field : fields) {
			addField(field);
		}
	}

	public AggregateBy addField(String aggregateBy) throws NoSuchFieldException {
		if (isStatisticsQuery) {
			Fields.aggregatable(base).validateField(aggregateBy);
			if (!Fields.statisticsAggregatable(base).isValidField(aggregateBy)) {
				if (!Fields.aggregatable(base).isValidField(aggregateBy)) {
					throw new NoSuchFieldException(aggregateBy, base);
				}
				throw new NoSuchFieldException("Field not available for " + base+ " statistics endpoint", aggregateBy);
			}
		} else {
			Fields.aggregatable(base).validateField(aggregateBy);
		}
		if (!fields.contains(aggregateBy)) {
			fields.add(aggregateBy);
		}
		return this;
	}

	public List<String> getFields() {
		return Collections.unmodifiableList(fields);
	}

	@Override
	public String toString() {
		return getFields().toString();
	}

	public boolean hasValues() {
		return !fields.isEmpty();
	}

	public void addGeoJsonAggregateBy(Coordinates.Type geoJsonCRS, FeatureType geoJsonFeatureType) throws NoSuchFieldException {
		if (geoJsonCRS == null && geoJsonFeatureType == null) return;
		if (geoJsonFeatureType == FeatureType.CENTER_POINT) {
			if (geoJsonCRS == Type.EUREF) {
				addField("gathering.conversions.eurefCenterPoint.lat");
				addField("gathering.conversions.eurefCenterPoint.lon");
				return;
			}
			if (geoJsonCRS == Type.WGS84) {
				addField("gathering.conversions.wgs84CenterPoint.lat");
				addField("gathering.conversions.wgs84CenterPoint.lon");
				return;
			}
			throw new IllegalArgumentException(FeatureType.CENTER_POINT + " is not available for " + Const.CRS + " " + Type.YKJ);
		}
		if (geoJsonFeatureType == FeatureType.ENVELOPE) {
			addEnvelopeAggregateBy(geoJsonCRS);
			return;
		}
		throw new IllegalArgumentException(geoJsonFeatureType + " is not usable in /aggregate queries");
	}

	private void addEnvelopeAggregateBy(Coordinates.Type geoJsonCRS) throws NoSuchFieldException {
		addField("gathering.conversions."+geoJsonCRS.name().toLowerCase()+".latMax");
		addField("gathering.conversions."+geoJsonCRS.name().toLowerCase()+".latMin");
		addField("gathering.conversions."+geoJsonCRS.name().toLowerCase()+".lonMax");
		addField("gathering.conversions."+geoJsonCRS.name().toLowerCase()+".lonMin");
	}

	public AggregateByCoordinateFields getCoordinateFields() {
		return new AggregateByCoordinateFields(this);
	}

}
