package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.taxonomy.Taxon;

public class UnitMediaSelectableFields extends UnitSelectableFields {

	public UnitMediaSelectableFields() {
		super(initFields(), Base.UNIT_MEDIA);
	}

	private static Set<String> initFields() {
		Set<String> fields = initCommon(Base.UNIT_MEDIA, Taxon.class, false);
		fields.addAll(getClassFields(MediaObject.class, "media", false, null, Base.UNIT));
		return Collections.unmodifiableSet(fields);
	}

}
