package fi.laji.datawarehouse.query.model.responses;

import java.util.List;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.query.model.AggregateRow;

public class AggregateResponse extends PageableResponse<AggregateResponse> {

	private final List<AggregateRow> results;

	public AggregateResponse(long totalResults, int currentPage, int pageSize, List<AggregateRow> results) {
		super(totalResults, currentPage, pageSize);
		this.results = results;
	}

	@FieldDefinition(order=2)
	public List<AggregateRow> getResults() {
		return results;
	}

}
