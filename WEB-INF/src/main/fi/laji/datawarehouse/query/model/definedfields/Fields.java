package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collection;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;

public class Fields {

	public interface DefinedFields {
		Set<String> getAllFields();
		void validateField(String field) throws NoSuchFieldException;
		boolean isValidField(String field);
	}

	private static final DefinedFields unitSelectable = new UnitSelectableFields();
	private static final DefinedFields gatheringSelectable = new GatheringSelectableFields();
	private static final DefinedFields documentSelectable = new DocumentSelectableFields();
	private static final DefinedFields annotationSelectable = new AnnotationSelectableFields();
	private static final DefinedFields unitMediaSelectable = new UnitMediaSelectableFields();
	private static final DefinedFields sampleSelectable = new SampleSelectableFields();
	private static final DefinedFields unitAggregatable = new AggregatableFields(Base.UNIT, false);
	private static final DefinedFields gatheringAggregatable = new AggregatableFields(Base.GATHERING, false);
	private static final DefinedFields documentAggregatable = new AggregatableFields(Base.DOCUMENT, false);
	private static final DefinedFields unitSortable = new SortableFields(Base.UNIT);
	private static final DefinedFields gatheringSortable = new SortableFields(Base.GATHERING);
	private static final DefinedFields documentSortable = new SortableFields(Base.DOCUMENT);
	private static final DefinedFields annotationSortable = new SortableFields(Base.ANNOTATION);
	private static final DefinedFields unitMediaSortable = new SortableFields(Base.UNIT_MEDIA);
	private static final DefinedFields sampleSortable = new SortableFields(Base.SAMPLE);
	private static final DefinedFields unitStatisticsAggregatable = new AggregatableFields(Base.UNIT, true);
	private static final DefinedFields gatheringStatisticsAggregatable = new AggregatableFields(Base.GATHERING, true);
	private static final EnumToProperty enumToProperty = EnumToProperty.getInstance();

	public static DefinedFields selectable(Base base) {
		if (base == Base.UNIT) return unitSelectable;
		if (base == Base.ANNOTATION) return annotationSelectable;
		if (base == Base.DOCUMENT) return documentSelectable;
		if (base == Base.UNIT_MEDIA) return unitMediaSelectable;
		if (base == Base.SAMPLE) return sampleSelectable;
		if (base == Base.GATHERING) return gatheringSelectable;
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	public static DefinedFields aggregatable(Base base) {
		if (base.includes(Base.UNIT)) return unitAggregatable;
		if (base == Base.GATHERING) return gatheringAggregatable;
		if (base == Base.DOCUMENT) return documentAggregatable;
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	public static DefinedFields statisticsAggregatable(Base base) {
		if (base == Base.UNIT) return unitStatisticsAggregatable;
		if (base == Base.GATHERING) return gatheringStatisticsAggregatable;
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	public static DefinedFields sortable(Base base) {
		if (base == Base.UNIT) return unitSortable;
		if (base == Base.ANNOTATION) return annotationSortable;
		if (base == Base.UNIT_MEDIA) return unitMediaSortable;
		if (base == Base.GATHERING) return gatheringSortable;
		if (base == Base.DOCUMENT) return documentSortable;
		if (base == Base.SAMPLE) return sampleSortable;
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	public static Qname enumToProperty(Enum<?> enumeration) {
		return enumToProperty.get(enumeration);
	}

	public static Collection<Enum<?>> getEnumerations() {
		return enumToProperty.getEnumerations();
	}

	public static Enum<?> getEnumeration(String enumerationName) {
		return enumToProperty.getEnumerationByName(enumerationName);
	}

}
