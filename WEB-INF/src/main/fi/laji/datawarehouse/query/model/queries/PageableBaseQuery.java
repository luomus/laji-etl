package fi.laji.datawarehouse.query.model.queries;

public class PageableBaseQuery extends BaseQuery {

	public static final int MAX_PAGE_SIZE = 10000;
	private final int pageSize;
	private int currentPage;

	public PageableBaseQuery(BaseQuery baseQuery, int currentPage, int pageSize) {
		super(baseQuery);
		if (pageSize < 1) throw new IllegalArgumentException("Invalid page size: " + pageSize);
		if (pageSize > MAX_PAGE_SIZE) pageSize = MAX_PAGE_SIZE;
		if (currentPage < 1) throw new IllegalArgumentException("Invalid current page: " + currentPage);
		this.currentPage = currentPage;
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void nextPage() {
		this.currentPage++;
	}

	@Override
	public String toString() {
		return super.toString() + " PageableBaseQuery [pageSize=" + pageSize + ", currentPage=" + currentPage + "]";
	}

	@Override
	public String toComparisonString() {
		return super.toComparisonString() + " PageableBaseQuery [pageSize=" + pageSize + ", currentPage=" + currentPage + "]";
	}

}
