package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.taxonomy.Taxon;

public class AnnotationSelectableFields extends UnitSelectableFields {

	public AnnotationSelectableFields() {
		super(initFields(), Base.ANNOTATION);
	}

	private static Set<String> initFields() {
		Set<String> fields = initCommon(Base.UNIT, Taxon.class, false);
		fields.addAll(getClassFields(Annotation.class, false, Taxon.class, Base.UNIT));
		return Collections.unmodifiableSet(fields);
	}

}
