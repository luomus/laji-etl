package fi.laji.datawarehouse.query.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.luomus.commons.utils.DateUtils;

public class AggregateRow {

	private Long count;
	private Long individualCountSum;
	private Integer individualCountMax;
	private Date oldestRecord;
	private Date newestRecord;
	private Date firstLoadDateMin;
	private Date firstLoadDateMax;
	private Long pairCountSum;
	private Integer pairCountMax;
	private Long lineLengthSum;
	private Long taxonCount;
	private Long speciesCount;
	private String atlasCodeMax;
	private String atlasClassMax;
	private Long recordQualityMax;
	private String redListStatusMax;
	private Long gatheringCount;
	private Long securedCount;

	private final List<Object> aggregateByValues = new ArrayList<>();

	@FieldDefinition(order=0.1)
	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@FieldDefinition(order=0.2)
	public Long getGatheringCount() {
		return gatheringCount;
	}

	public void setGatheringCount(Long count) {
		this.gatheringCount = count;
	}

	@FieldDefinition(order=0.3)
	public Long getSecuredCount() {
		return securedCount;
	}

	public void setSecuredCount(Long securedCount) {
		this.securedCount = securedCount;
	}

	@FieldDefinition(order=3.1)
	public Long getIndividualCountSum() {
		return individualCountSum;
	}

	public void setIndividualCountSum(Long individualCountSum) {
		this.individualCountSum = individualCountSum;
	}

	@FieldDefinition(order=3.2)
	public Integer getIndividualCountMax() {
		return individualCountMax;
	}

	public void setIndividualCountMax(Integer individualCountMax) {
		this.individualCountMax = individualCountMax;
	}

	@FieldDefinition(order=3.51)
	public Long getRecordQualityMax() {
		return recordQualityMax;
	}

	public void setRecordQualityMax(Long recordQualityMax) {
		this.recordQualityMax = recordQualityMax;
	}

	@FieldDefinition(order=3.52)
	public String getRedListStatusMax() {
		return redListStatusMax;
	}

	public void setRedListStatusMax(String redListStatusMax) {
		this.redListStatusMax = redListStatusMax;
	}

	@FieldDefinition(order=5.1)
	public Long getPairCountSum() {
		return pairCountSum;
	}

	public void setPairCountSum(Long pairCountSum) {
		this.pairCountSum = pairCountSum;
	}

	@FieldDefinition(order=5.2)
	public Integer getPairCountMax() {
		return pairCountMax;
	}

	public void setPairCountMax(Integer pairCountMax) {
		this.pairCountMax = pairCountMax;
	}

	@FieldDefinition(order=1)
	public Date getOldestRecord() {
		return oldestRecord;
	}

	public void setOldestRecord(Long oldestRecord) {
		if (oldestRecord == null) {
			this.oldestRecord = null;
			return;
		}
		try {
			this.oldestRecord = DateUtils.convertToDate(oldestRecord.toString(), "yyyyMMdd");
		} catch (Exception e) {
			this.oldestRecord = DateRange.oldestAllowedDate(); // Was some very small number that in the future is not allowed into the dw
		}
	}

	@FieldDefinition(order=1.1)
	public Date getNewestRecord() {
		return newestRecord;
	}

	public void setNewestRecord(Long newestRecord) {
		if (newestRecord == null) {
			this.newestRecord = null;
			return;
		}
		try {
			this.newestRecord = DateUtils.convertToDate(newestRecord.toString(), "yyyyMMdd");
		} catch (Exception e) {
			this.newestRecord = DateRange.oldestAllowedDate(); // Was some very small number that in the future is not allowed into the dw
		}
	}

	@FieldDefinition(order=5)
	public Long getLineLengthSum() {
		return lineLengthSum;
	}

	public void setLineLengthSum(Long lineLengthSum) {
		this.lineLengthSum = lineLengthSum;
	}

	@FieldDefinition(order=2.1)
	public Date getFirstLoadDateMin() {
		return firstLoadDateMin;
	}

	public void setFirstLoadDateMin(Date firstLoadDateMin) {
		this.firstLoadDateMin = firstLoadDateMin;
	}

	@FieldDefinition(order=2.2)
	public Date getFirstLoadDateMax() {
		return firstLoadDateMax;
	}

	public void setFirstLoadDateMax(Date firstLoadDateMax) {
		this.firstLoadDateMax = firstLoadDateMax;
	}

	@FieldDefinition(order=4.2)
	public Long getTaxonCount() {
		return taxonCount;
	}

	public void setTaxonCount(Long taxonCount) {
		this.taxonCount = taxonCount;
	}

	@FieldDefinition(order=4.1)
	public Long getSpeciesCount() {
		return speciesCount;
	}

	public void setSpeciesCount(Long speciesCount) {
		this.speciesCount = speciesCount;
	}

	@FieldDefinition(order=6.1)
	public String getAtlasCodeMax() {
		return atlasCodeMax;
	}

	public void setAtlasCodeMax(String atlasCodeMax) {
		this.atlasCodeMax = atlasCodeMax;
	}

	@FieldDefinition(order=6.2)
	public String getAtlasClassMax() {
		return atlasClassMax;
	}

	public void setAtlasClassMax(String atlasClassMax) {
		this.atlasClassMax = atlasClassMax;
	}

	@FieldDefinition(ignoreForQuery=true)
	public List<Object> getAggregateByValues() {
		return aggregateByValues;
	}

	public void addAggregateValue(Object value) {
		aggregateByValues.add(value);
	}

	@Override
	public String toString() {
		return "AggregateRow [aggregateByValues=" + aggregateByValues + ", count=" + count + ", individualCountSum=" + individualCountSum + ", individualCountMax="
				+ individualCountMax + ", oldestRecord=" + format(oldestRecord) + ", newestRecord=" + format(newestRecord) + ", firstLoadDateMin=" + format(firstLoadDateMin) + ", firstLoadDateMax="
				+ format(firstLoadDateMax) + ", pairCountSum=" + pairCountSum + ", pairCountMax=" + pairCountMax + ", lineLengthSum=" + lineLengthSum + ", taxonCount=" + taxonCount
				+ ", speciesCount=" + speciesCount + ", atlasCodeMax=" + atlasCodeMax + ", atlasClassMax=" + atlasClassMax + ", recordQualityMax=" + recordQualityMax
				+ ", redListStatusGroupMax=" + redListStatusMax + ", gatheringCount=" + gatheringCount + ", securedCount=" + securedCount + "]";
	}

	private String format(Date date) {
		if (date == null) return null;
		return DateUtils.format(date, "yyyy-MM-dd");
	}

}
