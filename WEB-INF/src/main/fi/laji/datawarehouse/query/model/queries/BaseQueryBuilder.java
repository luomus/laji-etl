package fi.laji.datawarehouse.query.model.queries;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;

public class BaseQueryBuilder {

	private final Concealment warehouse;
	private Base base = Base.UNIT;
	private String apiSourceId;
	private boolean apiSourceIdCanUseEditorOrObserverIdIsNot = false;
	private Class<?> caller;
	private String userAgent;
	private Qname personId;
	private String personEmail;
	private boolean useCache = false;
	private boolean onlyStatisticsFilters = false;
	private boolean setDefaultFilters = true;
	private Coordinates.Type crs;
	private FeatureType featureType;

	public BaseQueryBuilder(Concealment warehouse) {
		if (warehouse == null) throw new IllegalArgumentException("Null warehouse type given");
		this.warehouse = warehouse;
	}

	public BaseQueryBuilder setBase(Base base) {
		this.base = base;
		return this;
	}

	public BaseQueryBuilder setCaller(Class<?> caller) {
		this.caller = caller;
		return this;
	}

	public BaseQueryBuilder setUserAgent(String userAgent) {
		this.userAgent = userAgent;
		return this;
	}

	public BaseQueryBuilder setApiSourceId(String apiSourceId) {
		this.apiSourceId = apiSourceId;
		return this;
	}

	public BaseQueryBuilder setApiSourceIdCanUseEditorOrObserverIdIsNot(boolean apiSourceIdCanUseEditorOrObserverIdIsNot) {
		this.apiSourceIdCanUseEditorOrObserverIdIsNot = apiSourceIdCanUseEditorOrObserverIdIsNot;
		return this;
	}

	public BaseQueryBuilder setPersonId(Qname personId) {
		this.personId = personId;
		return this;
	}

	public BaseQueryBuilder setPersonEmail(String email) {
		this.personEmail = email;
		return this;
	}

	public BaseQueryBuilder setUseCache(boolean useCache) {
		this.useCache = useCache;
		return this;
	}

	public BaseQueryBuilder setDefaultFilters(boolean setDefaultFilters) {
		this.setDefaultFilters = setDefaultFilters;
		return this;
	}

	public BaseQueryBuilder setOnlyStatisticsFilters(boolean onlyStatisticsFilters) {
		this.onlyStatisticsFilters = onlyStatisticsFilters;
		return this;
	}

	public BaseQueryBuilder setCrs(Coordinates.Type crs) {
		this.crs = crs;
		return this;
	}

	public BaseQueryBuilder setFeatureType(FeatureType featureType) {
		this.featureType = featureType;
		return this;
	}

	public BaseQuery build() {
		return new BaseQuery(this);
	}

	public Concealment getWarehouse() {
		return warehouse;
	}

	public Base getBase() {
		return base;
	}

	public Class<?> getCaller() {
		return caller;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getApiSourceId() {
		return apiSourceId;
	}

	public boolean getApiSourceIdCanUseEditorOrObserverIdIsNot() {
		return apiSourceIdCanUseEditorOrObserverIdIsNot;
	}

	public Qname getPersonId() {
		return personId;
	}

	public String getPersonEmail() {
		return personEmail;
	}

	public boolean getUseCache() {
		return useCache;
	}

	public boolean getSetDefaultFilters() {
		return setDefaultFilters;
	}

	public boolean getOnlyStatisticsFilters() {
		return onlyStatisticsFilters;
	}

	public Coordinates.Type getCrs() {
		return crs;
	}

	public FeatureType getFeatureType() {
		return featureType;
	}


}
