package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Base;

public class SortableFields extends UnitSelectableFields {

	public SortableFields(Base base) {
		super(initFields(base), base);
	}

	private static Set<String> initFields(Base base) {
		Set<String> fields = new TreeSet<>();
		fields.add(Const.RANDOM);
		fields.add(Const.RANDOM_WITH_SEED);
		fields.add("document.collectionId");
		fields.add("document.createdDate");
		fields.add("document.documentId");
		fields.add("document.firstLoadDate");
		fields.add("document.loadDate");
		fields.add("document.mediaCount");
		fields.add("document.modifiedDate");
		fields.add("document.namedPlace.name");
		fields.add("document.namedPlace.municipalityDisplayName");
		fields.add("document.namedPlace.birdAssociationAreaDisplayName");
		fields.add("document.quality.issue.issue");
		fields.add("document.quality.issue.source");
		fields.add("document.secureLevel");
		fields.add("document.secured");
		fields.add("document.sourceId");
		fields.add("document.linkings.collectionQuality");
		fields.add("document.siteType");
		fields.add("document.siteStatus");
		fields.add("document.dataSource");
		if (base.includes(Base.GATHERING)) {
			fields.add("gathering.gatheringOrder");
			fields.add("gathering.gatheringSection");
			fields.add("gathering.biogeographicalProvince");
			fields.add("gathering.conversions.boundingBoxAreaInSquareMeters");
			fields.add("gathering.conversions.century");
			fields.add("gathering.conversions.day");
			fields.add("gathering.conversions.dayOfYearBegin");
			fields.add("gathering.conversions.dayOfYearEnd");
			fields.add("gathering.conversions.decade");
			fields.add("gathering.conversions.euref.latMax");
			fields.add("gathering.conversions.euref.latMin");
			fields.add("gathering.conversions.euref.lonMax");
			fields.add("gathering.conversions.euref.lonMin");
			fields.add("gathering.conversions.month");
			fields.add("gathering.conversions.seasonBegin");
			fields.add("gathering.conversions.seasonEnd");
			fields.add("gathering.conversions.wgs84.latMax");
			fields.add("gathering.conversions.wgs84.latMin");
			fields.add("gathering.conversions.wgs84.lonMax");
			fields.add("gathering.conversions.wgs84.lonMin");
			fields.add("gathering.conversions.wgs84CenterPoint.lat");
			fields.add("gathering.conversions.wgs84CenterPoint.lon");
			fields.add("gathering.conversions.wgs84Grid005.lat");
			fields.add("gathering.conversions.wgs84Grid005.lon");
			fields.add("gathering.conversions.wgs84Grid01.lat");
			fields.add("gathering.conversions.wgs84Grid01.lon");
			fields.add("gathering.conversions.wgs84Grid05.lat");
			fields.add("gathering.conversions.wgs84Grid05.lon");
			fields.add("gathering.conversions.wgs84Grid1.lat");
			fields.add("gathering.conversions.wgs84Grid1.lon");
			fields.add("gathering.conversions.year");
			fields.add("gathering.conversions.ykj.latMax");
			fields.add("gathering.conversions.ykj.latMin");
			fields.add("gathering.conversions.ykj.lonMax");
			fields.add("gathering.conversions.ykj.lonMin");
			fields.add("gathering.conversions.ykj100km.lat");
			fields.add("gathering.conversions.ykj100km.lon");
			fields.add("gathering.conversions.ykj100kmCenter.lat");
			fields.add("gathering.conversions.ykj100kmCenter.lon");
			fields.add("gathering.conversions.ykj10km.lat");
			fields.add("gathering.conversions.ykj10km.lon");
			fields.add("gathering.conversions.ykj10kmCenter.lat");
			fields.add("gathering.conversions.ykj10kmCenter.lon");
			fields.add("gathering.conversions.ykj1km.lat");
			fields.add("gathering.conversions.ykj1km.lon");
			fields.add("gathering.conversions.ykj1kmCenter.lat");
			fields.add("gathering.conversions.ykj1kmCenter.lon");
			fields.add("gathering.conversions.ykj50km.lat");
			fields.add("gathering.conversions.ykj50km.lon");
			fields.add("gathering.conversions.ykj50kmCenter.lat");
			fields.add("gathering.conversions.ykj50kmCenter.lon");
			fields.add("gathering.coordinatesVerbatim");
			fields.add("gathering.country");
			fields.add("gathering.displayDateTime");
			fields.add("gathering.eventDate.begin");
			fields.add("gathering.eventDate.end");
			fields.add("gathering.gatheringId");
			fields.add("gathering.higherGeography");
			fields.add("gathering.hourBegin");
			fields.add("gathering.hourEnd");
			fields.add("gathering.interpretations.biogeographicalProvince");
			fields.add("gathering.interpretations.biogeographicalProvinceDisplayname");
			fields.add("gathering.interpretations.coordinateAccuracy");
			fields.add("gathering.interpretations.country");
			fields.add("gathering.interpretations.countryDisplayname");
			fields.add("gathering.interpretations.finnishMunicipality");
			fields.add("gathering.interpretations.municipalityDisplayname");
			fields.add("gathering.interpretations.sourceOfBiogeographicalProvince");
			fields.add("gathering.interpretations.sourceOfCoordinates");
			fields.add("gathering.interpretations.sourceOfCountry");
			fields.add("gathering.interpretations.sourceOfFinnishMunicipality");
			fields.add("gathering.locality");
			fields.add("gathering.mediaCount");
			fields.add("gathering.minutesBegin");
			fields.add("gathering.minutesEnd");
			fields.add("gathering.municipality");
			fields.add("gathering.province");
			fields.add("gathering.quality.issue.issue");
			fields.add("gathering.quality.issue.source");
			fields.add("gathering.quality.locationIssue.issue");
			fields.add("gathering.quality.locationIssue.source");
			fields.add("gathering.quality.timeIssue.issue");
			fields.add("gathering.quality.timeIssue.source");
			fields.add("gathering.team");
			fields.add("gathering.stateLand");
		}
		if (base.includes(Base.UNIT)) {
			fields.add("unit.unitOrder");
			fields.add("unit.abundanceString");
			fields.add("unit.author");
			fields.add("unit.breedingSite");
			fields.add("unit.det");
			fields.add("unit.individualId");
			fields.add("unit.interpretations.annotatedTaxonId");
			fields.add("unit.interpretations.individualCount");
			fields.add("unit.interpretations.invasiveControlEffectiveness");
			fields.add("unit.interpretations.invasiveControlled");
			fields.add("unit.interpretations.recordQuality");
			fields.add("unit.interpretations.recordQualityNumeric");
			fields.add("unit.interpretations.reliability");
			fields.add("unit.lifeStage");
			fields.add("unit.linkings.taxon.author");
			fields.add("unit.linkings.taxon.scientificNameDisplayName");
			fields.add("unit.linkings.taxon.finnish");
			fields.add("unit.linkings.taxon.invasive");
			fields.add("unit.linkings.taxon.nameEnglish");
			fields.add("unit.linkings.taxon.nameFinnish");
			fields.add("unit.linkings.taxon.nameSwedish");
			fields.add("unit.linkings.taxon.redListStatus");
			fields.add("unit.linkings.taxon.scientificName");
			fields.add("unit.linkings.taxon.species");
			fields.add("unit.linkings.taxon.speciesNameEnglish");
			fields.add("unit.linkings.taxon.speciesNameFinnish");
			fields.add("unit.linkings.taxon.speciesNameSwedish");
			fields.add("unit.linkings.taxon.speciesScientificName");
			fields.add("unit.linkings.taxon.taxonRank");
			fields.add("unit.linkings.taxon.taxonomicOrder");
			fields.add("unit.linkings.taxon.occurrenceCount");
			fields.add("unit.linkings.taxon.occurrenceCountFinland");
			fields.add("unit.linkings.originalTaxon.author");
			fields.add("unit.linkings.originalTaxon.scientificNameDisplayName");
			fields.add("unit.linkings.originalTaxon.finnish");
			fields.add("unit.linkings.originalTaxon.invasive");
			fields.add("unit.linkings.originalTaxon.nameEnglish");
			fields.add("unit.linkings.originalTaxon.nameFinnish");
			fields.add("unit.linkings.originalTaxon.nameSwedish");
			fields.add("unit.linkings.originalTaxon.redListStatus");
			fields.add("unit.linkings.originalTaxon.scientificName");
			fields.add("unit.linkings.originalTaxon.species");
			fields.add("unit.linkings.originalTaxon.speciesNameEnglish");
			fields.add("unit.linkings.originalTaxon.speciesNameFinnish");
			fields.add("unit.linkings.originalTaxon.speciesNameSwedish");
			fields.add("unit.linkings.originalTaxon.speciesScientificName");
			fields.add("unit.linkings.originalTaxon.taxonRank");
			fields.add("unit.linkings.originalTaxon.taxonomicOrder");
			fields.add("unit.linkings.originalTaxon.occurrenceCount");
			fields.add("unit.linkings.originalTaxon.occurrenceCountFinland");
			fields.add("unit.local");
			fields.add("unit.mediaCount");
			fields.add("unit.wild");
			fields.add("unit.alive");
			fields.add("unit.quality.documentGatheringUnitQualityIssues");
			fields.add("unit.quality.issue.issue");
			fields.add("unit.quality.issue.source");
			fields.add("unit.recordBasis");
			fields.add("unit.referencePublication");
			fields.add("unit.reportedTaxonConfidence");
			fields.add("unit.sex");
			fields.add("unit.superRecordBasis");
			fields.add("unit.taxonVerbatim");
			fields.add("unit.typeSpecimen");
			fields.add("unit.unitId");
			fields.add("unit.notes");
		}
		if (base.includes(Base.ANNOTATION)) {
			fields.add("unit.annotations.created");
			fields.add("unit.annotations.annotationByPersonName");
			fields.add("unit.annotations.annotationBySystemName");
			fields.add("unit.annotations.id");
		}
		if (base.includes(Base.SAMPLE)) {
			fields.add("unit.samples.collectionId");
			fields.add("unit.samples.multiple");
			fields.add("unit.samples.quality");
			fields.add("unit.samples.sampleId");
			fields.add("unit.samples.status");
			fields.add("unit.samples.type");
			fields.add("unit.samples.material");
			fields.add("unit.samples.sampleOrder");
		}
		if (base.includes(Base.UNIT_MEDIA)) {
			fields.add("unit.media.fullURL");
			fields.add("unit.media.mediaType");
		}

		if (base.equals(Base.DOCUMENT)) {
			fields.add("document.notes");
		}
		if (base.equals(Base.GATHERING)) {
			fields.add("gathering.notes");
		}

		return Collections.unmodifiableSet(fields);
	}

}
