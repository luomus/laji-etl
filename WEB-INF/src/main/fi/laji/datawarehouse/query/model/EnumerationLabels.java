package fi.laji.datawarehouse.query.model;

import java.util.HashMap;
import java.util.Map;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class EnumerationLabels {

	private final DAO dao;
	private static final Object LOCK = new Object();

	public EnumerationLabels(DAO dao) {
		this.dao = dao;
	}

	public JSONObject getLabels(String enumerationName) {
		Enum<?> enumeration = Fields.getEnumeration(enumerationName);
		return getLabels(enumeration);
	}

	private static JSONObject allLabels;

	public JSONObject getAllLabels() {
		if (allLabels != null) return allLabels;
		synchronized (LOCK) {
			JSONObject labels = new JSONObject();
			for (Enum<?> enumeration : Fields.getEnumerations()) {
				try {
					labels.getArray("results").appendObject(getLabels(enumeration));
				} catch (Exception e) {
					dao.logError(Const.LAJI_ETL_QNAME, EnumerationLabels.class, enumeration.toString(), e);
				}
			}
			allLabels = labels;
			return labels;
		}
	}

	private static final Map<Enum<?>, JSONObject> storage = new HashMap<>();

	private JSONObject getLabels(Enum<?> enumeration) {
		if (storage.containsKey(enumeration)) return storage.get(enumeration);
		Qname propertyQname = Fields.enumToProperty(enumeration);
		JSONObject labels = new JSONObject();
		labels.setString("enumeration", enumeration.name());
		labels.setString("property", propertyQname.toString());

		LocalizedText propetyLabel = dao.getLabels(propertyQname);
		labels.getObject("label").setString("fi", propetyLabel.forLocale("fi"));
		labels.getObject("label").setString("en", propetyLabel.forLocale("en"));
		labels.getObject("label").setString("sv", propetyLabel.forLocale("sv"));

		storage.put(enumeration, labels);
		return labels;
	}

}
