package fi.laji.datawarehouse.query.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;

public class CetafUnitDescription extends CetafDocumentDescription {

	public static final String TYPE_STATUS_FACT = new Qname("MY.typeStatus").toURI();
	public static final String LEG_ID_FACT = new Qname("MY.legID").toURI();
	public static final String ADDITIONAL_IDS_FACT = new Qname("MY.additionalIDs").toURI();

	private final Area country;

	public CetafUnitDescription(JoinedRow row, CollectionMetadata collectionMetadata, Area country) {
		super(row, collectionMetadata);
		this.country = country;
	}

	@Override
	@DescriptionField(predicates={"rdf:type"}, object=Object.RESOURCE, order=0.0)
	public String getRdfType() {
		return "http://rs.tdwg.org/dwc/terms/Occurrence";
	}

	@Override
	@DescriptionField(predicates={"dc:type"}, object=Object.RESOURCE, order=0.1)
	public String getDcType() {
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.PRESERVED_SPECIMEN) {
			return "http://purl.org/dc/dcmitype/PhysicalObject";
		}
		return "http://purl.org/dc/dcmitype/Event";
	}

	@DescriptionField(predicates={"dwc:basisOfRecord"}, object=Object.LITERAL, order=0.2)
	public String getBasisOfRecord() {
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.PRESERVED_SPECIMEN) return "PreservedSpecimen";
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED) return "MachineObservation";
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.LITERATURE) return "Literature";
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.MATERIAL_SAMPLE) return "MaterialSample";
		if (row.getUnit().getSuperRecordBasis() == RecordBasis.FOSSIL_SPECIMEN) return "FossilSpecimen";
		return "HumanObservation";
	}

	@Override
	@DescriptionField(predicates={"dc:identifier", "dwc:occurrenceID", "dwc:catalogNumber"}, object=Object.LITERAL, order=1.0)
	public String getIdentifier() {
		return row.getUnit().getUnitId().toURI();
	}

	@Override
	@DescriptionField(predicates={"owl:sameAs"}, object=Object.LITERAL, order=1.1)
	public List<String> getAdditionalIds() {
		List<String> additionalIds = new ArrayList<>();
		addAdditionalIds(additionalIds, row.getUnit().getFacts());
		return additionalIds;
	}


	@Override
	@DescriptionField(predicates={"dc:modified"}, object=Object.LITERAL, order=3.0)
	public String getModified() {
		return row.getDocument().getModifiedDate() == null ? null :
			DateUtils.format(row.getDocument().getModifiedDate(), "yyyy-MM-dd");
	}

	@Override
	@DescriptionField(predicates={"dc:created"}, object=Object.LITERAL, order=3.1)
	public String getCreated() {
		return getEventDate();
	}

	@Override
	@DescriptionField(predicates={"dc:title"}, object=Object.LITERAL, order=4.0)
	public LocalizedText getTitle() {
		return new LocalizedText().set("en", getBasisOfRecord() + " " + row.getUnit().getUnitId().toURI() + " of " + getScientificNameOrTargetName());
	}

	@Override
	@DescriptionField(predicates={"rdfschema:comment"}, object=Object.LITERAL, order=4.1)
	public LocalizedText getComment() {
		return new LocalizedText().set("en", "Describing the " + getBasisOfRecord() + " (summary data)");
	}

	@DescriptionField(predicates={"dwc:eventDate"}, object=Object.LITERAL, order=5.00)
	public String getEventDate() {
		if (!given(row.getGathering().getEventDate())) return null;
		Date begin = row.getGathering().getEventDate().getBegin();
		Date end = row.getGathering().getEventDate().getEnd();
		if (given(begin)) {
			String startEndDate = DateUtils.format(begin, "yyyy-MM-dd");
			if (given(end)) {
				startEndDate += "/" + DateUtils.format(end, "yyyy-MM-dd");
			}
			return startEndDate;
		}
		return null;
	}

	@DescriptionField(predicates={"geo:lat", "dwc:decimalLatitude"}, object=Object.LITERAL, order=5.01)
	public String getLat() {
		if (given(row.getGathering().getConversions()) && given(row.getGathering().getConversions().getWgs84CenterPoint())) {
			SingleCoordinates centerPoint = row.getGathering().getConversions().getWgs84CenterPoint();
			return round(centerPoint.getLat());
		}
		return null;
	}

	@DescriptionField(predicates={"geo:lon", "dwc:decimalLongitude"}, object=Object.LITERAL, order=5.02)
	public String getLon() {
		if (given(row.getGathering().getConversions()) && given(row.getGathering().getConversions().getWgs84CenterPoint())) {
			SingleCoordinates centerPoint = row.getGathering().getConversions().getWgs84CenterPoint();
			return round(centerPoint.getLon());
		}
		return null;
	}

	private static String round(Double value) {
		if (value == null) return null;
		return String.format(Locale.US, "%.6f%n", value);
	}

	@DescriptionField(predicates={"dwc:coordinateUncertaintyInMeters"}, object=Object.LITERAL, order=5.03)
	public String getCoordinateUncertaintyInMeters() {
		if (given(row.getGathering().getInterpretations()) && given(row.getGathering().getInterpretations().getCoordinateAccuracy())) {
			return row.getGathering().getInterpretations().getCoordinateAccuracy().toString();
		}
		return null;
	}

	@DescriptionField(predicates={"dwc:geodeticDatum"}, object=Object.LITERAL, order=5.04)
	public String getCoordSystem() {
		if (given(getLon())) return "WGS84";
		return null;
	}

	@DescriptionField(predicates={"dwc:georeferenceSources"}, object=Object.LITERAL, order=5.05)
	public String getdGeoreferenceSource() {
		if (given(row.getGathering().getInterpretations()) && given(row.getGathering().getInterpretations().getSourceOfCoordinates())) {
			return row.getGathering().getInterpretations().getSourceOfCoordinates().name().toLowerCase();
		}
		return null;
	}

	@DescriptionField(predicates={"dwc:higherGeography"}, object=Object.LITERAL, order=5.06)
	public String getHigherGeography() {
		return row.getGathering().getHigherGeography();
	}

	@DescriptionField(predicates={"dwc:country"}, object=Object.LITERAL, order=5.7)
	public LocalizedText getCountry() {
		return country == null ? null : country.getName();
	}

	@DescriptionField(predicates={"dwc:countryCode"}, object=Object.LITERAL, order=5.08)
	public String getCountryCode() {
		return country == null ? null : country.getAbbreviation();
	}

	@DescriptionField(predicates={"dwc:locality"}, object=Object.LITERAL, order=5.09)
	public String getLocality() {
		List<String> localities = new ArrayList<>();
		String province = row.getGathering().getProvince();
		String bioProvince = row.getGathering().getBiogeographicalProvince();
		String municipality = row.getGathering().getMunicipality();
		String locality = row.getGathering().getLocality();

		if (given(row.getGathering().getInterpretations())) {
			GatheringInterpretations interpretations = row.getGathering().getInterpretations();
			bioProvince = interpretations.getBiogeographicalProvinceDisplayname();
			municipality = interpretations.getMunicipalityDisplayname();
		}

		if (given(province)) {
			localities.add(province);
		}
		if (given(bioProvince)) {
			localities.add(bioProvince);
		}
		if (given(municipality)) {
			localities.add(municipality);
		}
		if (given(locality)) {
			localities.add(locality);
		}
		if (localities.isEmpty()) return null;
		return localities.stream().collect(Collectors.joining(", "));
	}

	@DescriptionField(predicates={"dwc:earliestDateCollected"}, object=Object.LITERAL, order=5.10)
	public String getEarliestEventDate() {
		if (given(row.getGathering().getEventDate())) {
			Date begin = row.getGathering().getEventDate().getBegin();
			if (given(begin)) {
				return DateUtils.format(begin, "yyyy-MM-dd");
			}
		}
		if (given(getCreated())) return getCreated();
		if (given(row.getDocument().getFirstLoadDate())) return DateUtils.format(row.getDocument().getFirstLoadDate(), "yyyy-MM-dd");
		return null;
	}

	@DescriptionField(predicates={"dwc:recordedBy"}, object=Object.LITERAL, order=6.0)
	public String getRecordedBy() {
		if (row.getGathering().getTeam() == null) return null;
		if (row.getGathering().getTeam().isEmpty()) return null;
		return row.getGathering().getTeam().stream().collect(Collectors.joining("|"));
	}

	@Override
	@DescriptionField(predicates={"dwc:occurrenceRemarks"}, object=Object.LITERAL, order=7.0)
	public String getNotes() {
		List<String> notes = new ArrayList<>();
		if (given(row.getDocument().getNotes())) notes.add(row.getDocument().getNotes());
		if (given(row.getGathering().getNotes())) notes.add(row.getGathering().getNotes());
		if (given(row.getUnit().getNotes())) notes.add(row.getUnit().getNotes());
		if (notes.isEmpty()) return null;
		return notes.stream().collect(Collectors.joining(", "));
	}

	@DescriptionField(predicates={"dwc:individualCount"}, object=Object.LITERAL, order=7.1)
	public String getIndividualCount() {
		if (!given(row.getUnit().getInterpretations())) return null;
		if (!given(row.getUnit().getAbundanceString())) return null;
		if (row.getUnit().getAbundanceString().equals(String.valueOf(row.getUnit().getInterpretations().getIndividualCount()))) return row.getUnit().getAbundanceString();
		return null;
	}

	@DescriptionField(predicates={"dwc:phylum"}, object=Object.LITERAL, order=10.0)
	public String getPhylumName() {
		return getScientificNameOfRank(new Qname("MX.phylum"));
	}

	@DescriptionField(predicates={"dwc:order"}, object=Object.LITERAL, order=10.1)
	public String getOrderName() {
		return getScientificNameOfRank(new Qname("MX.order"));
	}

	@DescriptionField(predicates={"dwc:class"}, object=Object.LITERAL, order=10.2)
	public String getClassName() {
		return getScientificNameOfRank(new Qname("MX.class"));
	}

	@DescriptionField(predicates={"dwc:family"}, object=Object.LITERAL, order=10.3)
	public String getFamilyName() {
		return getScientificNameOfRank(new Qname("MX.family"));
	}

	@DescriptionField(predicates={"dwc:genus"}, object=Object.LITERAL, order=10.4)
	public String getGenusName() {
		return getScientificNameOfRank(new Qname("MX.genus"));
	}

	@DescriptionField(predicates={"dwc:specificEpithet"}, object=Object.LITERAL, order=10.5)
	public String getSpecificEpithet() {
		String speciesName = getScientificNameOfRank(new Qname("MX.species"));
		if (speciesName == null) return null;
		if (!speciesName.contains(" ")) return null;
		return speciesName.split(Pattern.quote(" "))[1];
	}

	@DescriptionField(predicates={"dwc:infraspecificEpithet"}, object=Object.LITERAL, order=10.6)
	public String getInfraspecificEpithet() {
		Taxon taxon = getTaxon();
		if (taxon == null) return null;
		if (!given(taxon.getScientificName())) return null;
		List<String> epithets = new ArrayList<>();
		int i = 1;
		for (String part : taxon.getScientificName().split(Pattern.quote(" "))) {
			if (i++ > 2) epithets.add(part);
		}
		if (epithets.isEmpty()) return null;
		return epithets.stream().collect(Collectors.joining(" "));
	}

	@DescriptionField(predicates={"dwc:scientificName"}, object=Object.LITERAL, order=10.7)
	public String getScientificName() {
		Taxon taxon = getTaxon();
		if (given(taxon) && given(taxon.getScientificName())) {
			if (given(taxon.getScientificNameAuthorship())) return taxon.getScientificName() + " " + taxon.getScientificNameAuthorship();
			return taxon.getScientificName();
		}
		if (given(row.getUnit().getAuthor())) return row.getUnit().getTaxonVerbatim() + " " + row.getUnit().getAuthor();
		return row.getUnit().getTaxonVerbatim();
	}

	@DescriptionField(predicates={"dwc:identifiedBy"}, object=Object.LITERAL, order=10.8)
	public String getDet() {
		return row.getUnit().getDet();
	}

	@DescriptionField(predicates={"dwc:typeStatus"}, object=Object.LITERAL, order=10.8)
	public String getTypeStatus() {
		if (row.getUnit().isTypeSpecimen()) {
			for (Fact fact : row.getUnit().getFacts()) {
				if (fact.getFact().equals(TYPE_STATUS_FACT)) {
					try {
						return Qname.fromURI(fact.getValue()).toString().replace("MY.typeStatus", "").toLowerCase();
					} catch (Exception e) {}
				}
			}
		}
		return null;
	}

	@Override
	@DescriptionField(predicates={"dwc:associatedMedia"}, object=Object.RESOURCE, order=12)
	public List<String> getMedia() {
		List<MediaObject> media = new ArrayList<>();
		media.addAll(row.getDocument().getMedia());
		media.addAll(row.getGathering().getMedia());
		media.addAll(row.getUnit().getMedia());
		if (media.isEmpty()) return null;
		return media.stream().map(m->m.getFullURL()).collect(Collectors.toList());
	}

	private String getScientificNameOfRank(Qname taxonRank) {
		Taxon taxon = getTaxon();
		if (taxon == null) return null;
		return taxon.getScientificNameOfRank(taxonRank);
	}

	private Taxon getTaxon() {
		if (row.getUnit().getLinkings() == null) return null;
		return row.getUnit().getLinkings().getTaxon();
	}

	private String getScientificNameOrTargetName() {
		Taxon taxon = getTaxon();
		if (given(taxon) && given(taxon.getScientificName())) return taxon.getScientificName();
		return row.getUnit().getTaxonVerbatim();
	}

}