package fi.laji.datawarehouse.query.model.queries;

public class AggregatedQuery extends PageableBaseQuery {

	private final AggregateBy aggregateBy;
	private OrderBy orderBy;
	private boolean onlyCount = true;
	private boolean pairCounts = false;
	private boolean taxonAggregate = false;
	private boolean atlasCounts = false;
	private boolean gatheringCounts = false;
	private boolean exludeNulls = true;
	private boolean pessimisticDateRangeHandling = false;

	public AggregatedQuery(BaseQuery baseQuery, AggregateBy aggregateBy, int currentPage, int pageSize) {
		super(baseQuery, currentPage, pageSize);
		this.aggregateBy = aggregateBy;
	}

	public OrderBy getOrderBy() {
		return orderBy;
	}

	public AggregatedQuery setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
		return this;
	}

	public AggregateBy getAggregateBy() {
		return aggregateBy;
	}

	public boolean hasSetOrder() {
		return orderBy != null && orderBy.hasValues();
	}

	public boolean isOnlyCountRequest() {
		return onlyCount;
	}

	public AggregatedQuery setOnlyCountRequest(boolean onlyCountRequest) {
		this.onlyCount = onlyCountRequest;
		return this;
	}

	public boolean isPairCountRequest() {
		return pairCounts;
	}

	public AggregatedQuery setPairCountRequest(boolean pairCounts) {
		this.pairCounts = pairCounts;
		this.onlyCount = false;
		return this;
	}

	public boolean isTaxonAggregateRequest() {
		return taxonAggregate;
	}

	public AggregatedQuery setTaxonCountRequest(boolean taxonAggregates) {
		this.taxonAggregate = taxonAggregates;
		this.onlyCount = false;
		return this;
	}

	public boolean isAtlasCountRequest() {
		return atlasCounts;
	}

	public AggregatedQuery setAtlasCountRequest(boolean atlasCounts) {
		this.atlasCounts = atlasCounts;
		this.onlyCount = false;
		return this;
	}

	public boolean isGatheringCountRequest() {
		return gatheringCounts;
	}

	public AggregatedQuery setGatheringCountRequest(boolean gatheringCounts) {
		this.gatheringCounts = gatheringCounts;
		return this;
	}

	public boolean isExludeNulls() {
		return exludeNulls;
	}

	public AggregatedQuery setExludeNulls(boolean exludeNulls) {
		this.exludeNulls = exludeNulls;
		return this;
	}

	public boolean isPessimisticDateRangeHandling() {
		return pessimisticDateRangeHandling;
	}

	public AggregatedQuery setPessimisticDateRangeHandling(boolean pessimisticDateRangeHandling) {
		this.pessimisticDateRangeHandling = pessimisticDateRangeHandling;
		return this;
	}

	@Override
	public String toString() {
		return super.toString() + fieldsToString();
	}

	@Override
	public String toComparisonString() {
		return super.toComparisonString() + fieldsToString();
	}

	private String fieldsToString() {
		return " AggregatedQuery [aggregateBy=" + aggregateBy + ", orderBy=" + orderBy + ", onlyCount=" + onlyCount + ", pairCounts=" + pairCounts + ", taxonCounts=" + taxonAggregate + ", atlasCounts=" + atlasCounts + ", exludeNulls=" + exludeNulls + ", pessimisticDateRangeHandling=" + pessimisticDateRangeHandling + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof AggregatedQuery)) throw new UnsupportedOperationException("Can not compare to " + o.getClass());
		AggregatedQuery other = (AggregatedQuery) o;
		return this.toComparisonString().equals(other.toComparisonString());
	}

	@Override
	public int hashCode() {
		return this.toComparisonString().hashCode();
	}

}
