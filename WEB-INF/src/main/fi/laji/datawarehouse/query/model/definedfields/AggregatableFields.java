package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Base;

public class AggregatableFields extends UnitSelectableFields {

	private final Base base;

	public AggregatableFields(Base base, boolean onlyStatisticsFields) {
		super(initFields(base, onlyStatisticsFields), base);
		this.base = base;
	}

	private static Set<String> initFields(Base base, boolean onlyStatisticsFields) {
		Set<String> fields = initCommon(base, TaxonBaseEntity.class, onlyStatisticsFields);
		addDocumentNamedPlace(onlyStatisticsFields, fields, base);
		removeTextualFields(fields);
		removeUnimplemented(base, onlyStatisticsFields, fields);
		replaceUserIdLinkings(base, onlyStatisticsFields, fields);
		additionalTaxonJoins(base, fields);
		if (base == Base.DOCUMENT) {
			fields.add("document.randomKey");
		}
		if (!onlyStatisticsFields) {
			fields.add("document.createdDateMonth");
		}
		addNotesFields(base, onlyStatisticsFields, fields);
		return Collections.unmodifiableSet(fields);
	}

	private static void addNotesFields(Base base, boolean onlyStatisticsFields, Set<String> fields) {
		// Note that gathering table does not have column for document notes and unit table does not have gathering or document notes
		if (!onlyStatisticsFields) {
			if (base.includes(Base.UNIT)) {
				fields.add("unit.notes");
			}
			if (base.equals(Base.DOCUMENT)) {
				fields.add("document.notes");
			}
			if (base.equals(Base.GATHERING)) {
				fields.add("gathering.notes");
			}
		}
	}

	private static void addDocumentNamedPlace(boolean onlyStatisticsFields, Set<String> fields, Base base) {
		fields.addAll(getClassFields(NamedPlaceEntity.class, "document.namedPlace", onlyStatisticsFields, null, base));
	}

	private static void additionalTaxonJoins(Base base, Set<String> fields) {
		if (base.includes(Base.UNIT)) {
			fields.add("unit.linkings.taxon.informalTaxonGroups");
			fields.add("unit.linkings.taxon.taxonSets");
			fields.add("unit.linkings.taxon.habitats");
			fields.add("unit.linkings.taxon.administrativeStatuses");
			fields.add("unit.linkings.taxon.typesOfOccurrenceInFinland");
			fields.add("unit.linkings.originalTaxon.informalTaxonGroups");
			fields.add("unit.linkings.originalTaxon.taxonSets");
			fields.add("unit.linkings.originalTaxon.habitats");
			fields.add("unit.linkings.originalTaxon.administrativeStatuses");
			fields.add("unit.linkings.originalTaxon.typesOfOccurrenceInFinland");
		}
	}

	private static void replaceUserIdLinkings(Base base, boolean onlyStatisticsFields, Set<String> fields) {
		Iterator<String> i = fields.iterator();
		while (i.hasNext()) {
			String field = i.next();
			if (field.startsWith("document.linkings.editors.")) i.remove();
			if (field.startsWith("gathering.linkings.observers.")) i.remove();
		}
		if (!onlyStatisticsFields) {
			fields.add("document.linkings.editors");
			if (base.includes(Base.GATHERING)) {
				fields.add("gathering.linkings.observers");
				fields.add("gathering.team.memberId");
				fields.add("gathering.team.memberName");
			}
		}
	}

	private static void removeUnimplemented(Base base, boolean onlyStatisticsFields, Set<String> fields) {
		fields.remove("unit.reportedTaxonId"); // not in db
		fields.remove("document.partial"); // not replicated from document to unit table
		fields.remove("gathering.interpretations.finnishMunicipalities"); // join not implemented (can be if needed but it is tricky - link table does not include those that have only one municipality)
		fields.remove("gathering.interpretations.biogeographicalProvinces"); // join not implemented ( - " - )
		fields.remove("document.referenceURL"); // not in db
		fields.remove("unit.externalMediaCount"); // not in db
		fields.remove("unit.identificationBasis"); // array field

		// aggregating by document.annotations not implemented (could be implemented)
		Iterator<String> i = fields.iterator();
		while (i.hasNext()) {
			String field = i.next();
			if (field.startsWith("document.annotations.")) i.remove();
			if (field.startsWith("unit.annotations.")) i.remove();
		}
		// aggregate only by limited annotation fields
		if (base.includes(Base.UNIT) && !onlyStatisticsFields) {
			fields.add("unit.annotations.annotationByPerson");
			fields.add("unit.annotations.annotationBySystem");
			fields.add("unit.annotations.annotationByPersonName");
			fields.add("unit.annotations.annotationBySystemName");
			fields.add("unit.annotations.created");
		}

		// aggregate by sourceTags not implemented (could be implemented)
		fields.remove("unit.sourceTags");
		fields.remove("document.sourceTags");

		// aggregate by namedPlace.tags not implemented (just plain search string loaded)
		fields.remove("document.namedPlace.tags");

		// identifications and type specimen arrays are not loaded to dw to own tables, only in json
		i = fields.iterator();
		while (i.hasNext()) {
			String field = i.next();
			if (field.startsWith("unit.identifications.")) i.remove();
			if (field.startsWith("unit.types.")) i.remove();
			if (field.endsWith(".fullResolutionMediaAvailable")) i.remove();
			if (field.contains("media") && field.endsWith(".type")) i.remove();
		}
	}

	private static void removeTextualFields(Set<String> fields) {
		Iterator<String> i = fields.iterator();
		while (i.hasNext()) {
			String field = i.next();
			if (field.endsWith(".notes")) i.remove();
			if (field.endsWith(".caption")) i.remove();
			if (field.endsWith(".fullURL")) i.remove();
			if (field.endsWith(".squareThumbnailURL")) i.remove();
			if (field.endsWith(".thumbnailURL")) i.remove();
			if (field.endsWith(".wavURL")) i.remove();
			if (field.endsWith(".mp3URL")) i.remove();
			if (field.endsWith(".highDetailModelURL")) i.remove();
			if (field.endsWith(".lowDetailModelURL")) i.remove();
			if (field.endsWith(".videoURL")) i.remove();
			if (field.endsWith(".message")) i.remove();
			if (field.endsWith("WKT")) i.remove();
		}
	}

	@Override
	public void validateField(String field) throws NoSuchFieldException {
		if (!isValidField(field)) throw new NoSuchFieldException(field, base);
	}

	@Override
	public boolean isValidField(String field) {
		return getAllFields().contains(field) || Const.aggregateFunctions(base).contains(field);
	}

}
