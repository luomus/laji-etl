package fi.laji.datawarehouse.query.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class CetafDocumentDescription {

	public static final String TYPE_STATUS_FACT = new Qname("MY.typeStatus").toURI();
	public static final String LEG_ID_FACT = new Qname("MY.legID").toURI();
	public static final String ADDITIONAL_IDS_FACT = new Qname("MY.additionalIDs").toURI();

	public enum Object { RESOURCE, LITERAL }

	@Retention(RetentionPolicy.RUNTIME)
	public @interface DescriptionField {
		public String[] predicates();
		public Object object();
		public double order() default Double.MAX_VALUE;
	}

	protected final JoinedRow row;
	private final CollectionMetadata collectionMetadata;

	public CetafDocumentDescription(JoinedRow row, CollectionMetadata collectionMetadata) {
		this.row = row;
		this.collectionMetadata = collectionMetadata;
	}

	@DescriptionField(predicates={"rdf:type"}, object=Object.RESOURCE, order=0.0)
	public String getRdfType() {
		return "http://rs.tdwg.org/dwc/terms/Event";
	}

	@DescriptionField(predicates={"dc:type"}, object=Object.RESOURCE, order=0.1)
	public String getDcType() {
		return "http://purl.org/dc/dcmitype/Event";
	}

	@DescriptionField(predicates={"dc:identifier", "dwc:catalogNumber"}, object=Object.LITERAL, order=1.0)
	public String getIdentifier() {
		return row.getDocument().getDocumentId().toURI();
	}

	@DescriptionField(predicates={"owl:sameAs"}, object=Object.LITERAL, order=1.1)
	public List<String> getAdditionalIds() {
		return new ArrayList<>(row.getDocument().factValues(ADDITIONAL_IDS_FACT));
	}

	protected void addAdditionalIds(List<String> additionalIds, List<Fact> facts) {
		for (Fact f : facts) {
			if (f.getFact().equals(ADDITIONAL_IDS_FACT)) {
				additionalIds.add(f.getValue());
			}
		}
	}

	@DescriptionField(predicates={"dc:publisher"}, object=Object.LITERAL, order=2.0)
	public String getPublisher() {
		return "http://gbif.fi";
	}

	@DescriptionField(predicates={"dc:bibliographicCitation"}, object=Object.LITERAL, order=2.1)
	public String getCitation() {
		String license = "Creative Commons Attribution 4.0";
		try {
			String licenseFromMetadata = collectionMetadata.getIntellectualRightsDescription().forLocale("en");
			if (given(licenseFromMetadata)) license = licenseFromMetadata;
		} catch (Exception e) {}
		return "Finnish Biodiversity Information Facility (" +getPublisher() + "), " + row.getDocument().getDocumentId().toURI() + ", accessed via Finnish Biodiversity Information Facility (FinBIF), " + license;
	}

	@DescriptionField(predicates={"dwc:institutionCode"}, object=Object.LITERAL, order=2.2)
	public String getInstitutionCode() {
		return "TODO";
	}

	@DescriptionField(predicates={"dwc:collectionCode"}, object=Object.LITERAL, order=2.3)
	public String getCollectionName() {
		if (given(collectionMetadata.getAbbreviation())) {
			return collectionMetadata.getAbbreviation();
		}
		return collectionMetadata.getName().forLocale("en");
	}

	@DescriptionField(predicates={"dc:modified"}, object=Object.LITERAL, order=3.0)
	public String getModified() {
		return row.getDocument().getModifiedDate() == null ? null :
			DateUtils.format(row.getDocument().getModifiedDate(), "yyyy-MM-dd");
	}

	@DescriptionField(predicates={"dc:created"}, object=Object.LITERAL, order=3.1)
	public String getCreated() {
		return row.getDocument().getCreatedDate() == null ? null :
			DateUtils.format(row.getDocument().getCreatedDate(), "yyyy-MM-dd");
	}

	@DescriptionField(predicates={"dc:title"}, object=Object.LITERAL, order=4.0)
	public LocalizedText getTitle() {
		return new LocalizedText().set("en", "Event " + getIdentifier());
	}

	@DescriptionField(predicates={"rdfschema:comment"}, object=Object.LITERAL, order=4.1)
	public LocalizedText getComment() {
		return new LocalizedText().set("en", "Describing the event (summary data)");
	}

	@DescriptionField(predicates={"dwc:fieldNumber", "dwc:recordNumber"}, object=Object.LITERAL, order=6.1)
	public String getLegId() {
		return row.getDocument().factValue(LEG_ID_FACT);
	}

	@DescriptionField(predicates={"dwc:occurrenceRemarks"}, object=Object.LITERAL, order=7.0)
	public String getNotes() {
		return row.getDocument().getNotes();
	}

	@DescriptionField(predicates={"dwc:associatedMedia"}, object=Object.RESOURCE, order=12)
	public List<String> getMedia() {
		List<MediaObject> media = new ArrayList<>();
		media.addAll(row.getDocument().getMedia());
		if (media.isEmpty()) return null;
		return media.stream().map(m->m.getFullURL()).collect(Collectors.toList());
	}

	protected static boolean given(java.lang.Object ...objects) {
		for (java.lang.Object o : objects) {
			if (o == null) return false;
			if (o.toString().length() < 1) return false;
		}
		return true;
	}

}