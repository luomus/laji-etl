package fi.laji.datawarehouse.query.model.queries;

public class CountQuery extends BaseQuery {

	public CountQuery(BaseQuery baseQuery) {
		super(baseQuery);
	}

	@Override
	public String toString() {
		return super.toString() + " CountQuery []";
	}

	@Override
	public String toComparisonString() {
		return super.toComparisonString() + " CountQuery []" ;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof CountQuery)) throw new UnsupportedOperationException("Can not compare to " + o.getClass());
		CountQuery other = (CountQuery) o;
		return this.toComparisonString().equals(other.toComparisonString());
	}

	@Override
	public int hashCode() {
		return this.toComparisonString().hashCode();
	}

}
