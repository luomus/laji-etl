package fi.laji.datawarehouse.query.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.utils.Utils;

public enum Base { 
	UNIT(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.UNIT);
		}}), 
	DOCUMENT(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.DOCUMENT);
		}}), 
	GATHERING(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.GATHERING);
		}}), 
	ANNOTATION(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.ANNOTATION);
		}}), 
	UNIT_MEDIA(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.UNIT_MEDIA);
		}}),
	SAMPLE(new Includor() {
		@Override
		public boolean includes(Base other) {
			return baseIncludes(other, Base.SAMPLE);
		}});

	private static final Map<Base, Set<Base>> BASE_INCLUDES;
	static {
		BASE_INCLUDES = new HashMap<>();
		BASE_INCLUDES.put(Base.DOCUMENT, Utils.set(Base.DOCUMENT));
		BASE_INCLUDES.put(Base.GATHERING, Utils.set(Base.DOCUMENT, Base.GATHERING));
		BASE_INCLUDES.put(Base.UNIT, Utils.set(Base.DOCUMENT, Base.GATHERING, Base.UNIT));
		BASE_INCLUDES.put(Base.ANNOTATION, Utils.set(Base.DOCUMENT, Base.GATHERING, Base.UNIT, Base.ANNOTATION));
		BASE_INCLUDES.put(Base.UNIT_MEDIA, Utils.set(Base.DOCUMENT, Base.GATHERING, Base.UNIT, Base.UNIT_MEDIA));
		BASE_INCLUDES.put(Base.SAMPLE, Utils.set(Base.DOCUMENT, Base.GATHERING, Base.UNIT, Base.SAMPLE));
	}

	private static boolean baseIncludes(Base mustIncludeThis, Base theBase) {
		Set<Base> includes = BASE_INCLUDES.get(theBase);
		return includes.contains(mustIncludeThis);
	}

	private static interface Includor {
		public boolean includes(Base other);
	}

	private final Includor includor;

	private Base(Includor includor) {
		this.includor = includor;
	}

	public boolean includes(Base other) {
		return includor.includes(other);
	}


}