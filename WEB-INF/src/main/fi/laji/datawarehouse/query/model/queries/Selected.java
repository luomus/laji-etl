package fi.laji.datawarehouse.query.model.queries;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.definedfields.Fields;

public class Selected {

	private final Set<String> selectedFields = new TreeSet<>();
	private final Base base;

	/**
	 * Default base: unit
	 */
	public Selected() {
		this(Base.UNIT);
	}

	/**
	 * Default base: unit
	 * @param fields
	 * @throws NoSuchFieldException
	 */
	public Selected(String ... fields) throws NoSuchFieldException {
		this(Base.UNIT, fields);
	}

	public Selected(Base base) {
		this.base = base;
	}

	public Selected(Base base, String ... fields) throws NoSuchFieldException {
		this(base);
		for (String field : fields) {
			add(field);
		}
	}

	public Selected add(String fieldOrFieldPart) throws NoSuchFieldException {
		selectedFieldParts = null;
		if (Fields.selectable(base).getAllFields().contains(fieldOrFieldPart)) {
			Fields.selectable(base).validateField(fieldOrFieldPart);
			selectedFields.add(fieldOrFieldPart);
			return this;
		}

		List<String> matchingFields = getMatchingFields(fieldOrFieldPart);
		if (matchingFields.isEmpty()) {
			Fields.selectable(base).validateField(fieldOrFieldPart);
		}

		for (String field : matchingFields) {
			Fields.selectable(base).validateField(field);
			selectedFields.add(field);
		}
		return this;
	}

	private List<String> getMatchingFields(String fieldOrFieldPart) {
		List<String> fields = new ArrayList<>();
		for (String candidate : Fields.selectable(base).getAllFields()) {
			if (candidate.equals(fieldOrFieldPart)) {
				fields.add(candidate);
			} else if (candidate.startsWith(fieldOrFieldPart + ".")) {
				fields.add(candidate);
			}
		}
		return fields;
	}

	public Set<String> getSelectedFields() {
		return selectedFields;
	}

	@Override
	public String toString() {
		return selectedFields.toString();
	}

	public boolean hasValues() {
		return !selectedFields.isEmpty();
	}

	private Set<String> selectedFieldParts = null;

	public Set<String> getSelectedFieldParts() {
		if (selectedFieldParts == null) {
			selectedFieldParts = parseSelectedFieldParts();
		}
		return selectedFieldParts;
	}

	private Set<String> parseSelectedFieldParts() {
		Set<String> fieldParts = new TreeSet<>();
		for (String field : selectedFields) {
			String[] parts = field.split(Pattern.quote("."));
			String validPart = null;
			for (String part : parts) {
				if (validPart == null) {
					validPart = part;
				} else {
					validPart += "." + part;
				}
				fieldParts.add(validPart);
			}
		}
		return fieldParts;
	}

}
