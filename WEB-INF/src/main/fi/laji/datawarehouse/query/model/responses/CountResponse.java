package fi.laji.datawarehouse.query.model.responses;

public class CountResponse extends BaseResponse<CountResponse> {

	public CountResponse(long total) {
		super(total);
	}

}
