package fi.laji.datawarehouse.query.model.definedfields;

import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.taxonomy.Taxon;

public class GatheringSelectableFields extends UnitSelectableFields {

	public GatheringSelectableFields() {
		super(initFields(), Base.GATHERING);
	}

	private static Set<String> initFields() {
		Set<String> fields = initCommon(Base.GATHERING, Taxon.class, false);
		return Collections.unmodifiableSet(fields);
	}

}
