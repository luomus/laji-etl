package fi.laji.datawarehouse.etl.threads;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.containers.rdf.Qname;

public class InPipeCleanUpThread extends ETLThread {

	private static final long PAUSE = 10 * 1000;

	public InPipeCleanUpThread(Qname forSource, ThreadHandler threadHandler, ThreadStatusReporter statusReporter, DAO dao) {
		super(forSource, threadHandler, statusReporter, dao);
	}

	@Override
	protected long read() {
		try {
			boolean deletedSomething = startInPipeCleanup();
			return deletedSomething ? PAUSE : NO_WORK_PAUSE;
		} catch (Throwable t) {
			logError(t);
			return ERROR_PAUSE;
		}
	}

	private boolean startInPipeCleanup() {
		reportStatus("Pruning ...");
		long start = System.currentTimeMillis();
		int deleteCount = getDAO().getETLDAO().removeUnlinkedInPipeData(getSource());
		long duration = System.currentTimeMillis() - start;
		if (duration > 10000) {
			logMessage("In pipe pruning took " + (duration/1000.0) + "s and deleted " + deleteCount + " rows");
		}
		return deleteCount > 0;
	}

}
