package fi.laji.datawarehouse.etl.threads.custom;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.TiiraHarmonizer;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.DateUtils;

public class TiiraAtlasPullReader extends ETLTableSyncLogic_BasePullReader<String> {

	public static final Qname PROD_SOURCE = new Qname("KE.1481");
	public static final Qname STAGING_SOURCE = new Qname("KE.1281");

	private final Config config;

	public TiiraAtlasPullReader(Config config, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super("TiiraAtlas", source(config), new TiiraHarmonizer(), dao, errorReporter, threadHandler, statusReporter);
		this.config = config;
	}

	public static Qname source(Config config) {
		if (config.productionMode()) return PROD_SOURCE;
		return STAGING_SOURCE;
	}

	@Override
	protected ResultStream<String> getIncomingEntryStream() throws Exception {
		return new TiiraCSVStream();
	}

	private class TiiraCSVStream implements ResultStream<String> {

		private final List<String> lines;
		private final ConnectionDescription desc = config.connectionDescription("TiiraPull");

		public TiiraCSVStream() {
			String file = file();
			try (HttpClientService client = client()) {
				lines = getWith(client, file);
			} catch (Exception e) {
				throw new ETLException("Failed to read " + file, e);
			}
		}

		private List<String> getWith(HttpClientService client, String file) throws Exception {
			List<String> lines = client.contentAsList(new HttpGet(file), "Cp1252");
			if (lines.isEmpty()) throw new ETLException("No contents!");
			ListIterator<String> i = lines.listIterator();
			boolean first = true;
			while (i.hasNext()) {
				String line = i.next();
				if (first) {
					i.remove();
					first = false;
					continue;
				}
				if (line.isEmpty()) i.remove();
				i.set(toTsv(line));
			}
			return lines;
		}

		private String toTsv(String line) {
			return line.replace("\"", "").replace(";", "\t");
		}

		private HttpClientService client() throws URISyntaxException {
			if (config.developmentMode()) return new HttpClientService();
			return new HttpClientService(desc.url(), desc.username(), desc.password());
		}

		private String file() {
			String url = desc.url();
			if (config.developmentMode()) return desc.url();
			if (!url.endsWith("/")) url += "/";
			String date = DateUtils.getCurrentDateTime("yyyyMMdd");
			return url + date + ".csv";
		}

		@Override
		public Iterator<String> iterator() {
			return lines.iterator();
		}

		@Override
		public void close() {}

	}

}
