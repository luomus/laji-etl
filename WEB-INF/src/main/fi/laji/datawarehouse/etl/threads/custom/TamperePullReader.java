package fi.laji.datawarehouse.etl.threads.custom;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer.GISLayerData;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;

public class TamperePullReader extends ETLTableSyncLogic_BasePullReader<GISLayerData> {

	public static final Qname PROD_SOURCE = new Qname("KE.1381");
	public static final Qname STAGING_SOURCE = new Qname("KE.1462");

	public static final Qname TAMPERE_MOSS_COLL_ID = new Qname("HR.4391");
	public static final Qname TAMPERE_INVASIVE_COLL_ID = new Qname("HR.4114");
	public static final Qname TAMPERE_SPECIES_COLL_ID = new Qname("HR.4113");

	private final Config config;

	public TamperePullReader(Config config, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super("Tampere", source(config), new MunicipalityGISHarmonizer(), dao, errorReporter, threadHandler, statusReporter);
		this.config = config;
	}

	public static Qname source(Config config) {
		if (config.productionMode()) return PROD_SOURCE;
		return STAGING_SOURCE;
	}

	@Override
	protected ResultStream<GISLayerData> getIncomingEntryStream() throws Exception {
		return new TampereStream();
	}

	private static class StreamSource {
		HttpGet request;
		Qname collectionId;
		String idPrefix;
		StreamSource(HttpGet request, Qname collectionId, String idPrefix) {
			this.request = request;
			this.collectionId = collectionId;
			this.idPrefix = idPrefix;
		}
	}

	private class TampereStream implements ResultStream<GISLayerData> {

		private final List<StreamSource> layers;
		private final ConnectionDescription desc = config.connectionDescription("TamperePull");
		private final HttpClientService client;

		public TampereStream() {
			if (config.developmentMode()) {
				layers = Utils.list(
						testLayer("tampere_vieras.json", TAMPERE_INVASIVE_COLL_ID, null),
						testLayer("tampere_laji_piste.json", TAMPERE_SPECIES_COLL_ID, "P."),
						testLayer("tampere_laji_alue.json", TAMPERE_SPECIES_COLL_ID, "A."),
						testLayer("tampere_sammal.json", TAMPERE_MOSS_COLL_ID, null));
			} else {
				layers = Utils.list(
						layer("ymparisto_ja_terveys:yv_lajihavainto_pisteet_lajitietokeskus_gsview", TAMPERE_SPECIES_COLL_ID, "P."),
						layer("ymparisto_ja_terveys:yv_lajihavainto_alueet_lajitietokeskus_gsview", TAMPERE_SPECIES_COLL_ID, "A."),
						layer("ymparisto_ja_terveys:yv_vieraslajit_lajitietokeskus_gsview", TAMPERE_INVASIVE_COLL_ID, null),
						layer("ymparisto_ja_terveys:yv_lahokaviosammalhavainnot_lajitietokeskus_gsview", TAMPERE_MOSS_COLL_ID, null)
						);
			}
			client = new HttpClientService();
		}

		private StreamSource testLayer(String fileName, Qname collectionId, String idPrefix) {
			return new StreamSource(new HttpGet(desc.url() + "/" + fileName), collectionId, idPrefix);
		}

		private StreamSource layer(String layerName, Qname collectionId, String idPrefix) {
			URIBuilder b = new URIBuilder(desc.url()); // https://domain.name/geoserver/wfs
			b.addParameter("request", "GetFeature");
			b.addParameter("typeName", layerName);
			b.addParameter("outputFormat", "application/json");
			b.addParameter("srsname", "EPSG:3067");
			b.addParameter("authkey", desc.password());
			try {
				return new StreamSource(new HttpGet(b.getURI()), collectionId, idPrefix);
			} catch (URISyntaxException e) {
				throw new ETLException(e);
			}
		}

		@Override
		public Iterator<GISLayerData> iterator() {
			return layers.stream().flatMap(s->features(s.request).stream().map(json->new GISLayerData(s.collectionId, s.idPrefix, json))).iterator();
		}

		private List<JSONObject> features(HttpGet layer) {
			JSONObject data;
			try {
				data = client.contentAsJson(layer);
			} catch (Exception e) {
				throw new ETLException("Reading from " + layer.getURI(), e);
			}
			return data.getArray("features").iterateAsObject();
		}

		@Override
		public void close() {
			if (client != null) client.close();
		}

	}

}
