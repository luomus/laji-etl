package fi.laji.datawarehouse.etl.threads.custom;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.jena.riot.RDFFormat;

import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariDataSource;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.oracle.KastikkaDataSourceDefinition;
import fi.laji.datawarehouse.etl.threads.ETLThread;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Context;
import fi.luomus.commons.containers.rdf.InternalModelToJenaModelConverter;
import fi.luomus.commons.containers.rdf.JenaUtils;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

public class KastikkaPullReader extends ETLThread {

	public static final Qname SOURCE = new Qname("KE.167");

	private static final Predicate MZ_HAS_PART_PREDICATE = new Predicate("MZ.hasPart");
	private static final Predicate MZ_IS_PART_OF_PREDICATE = new Predicate("MZ.isPartOf");

	private static class TransactionEntry {
		private final int transactionId;
		private final Qname documentQname;
		public TransactionEntry(int transactionId, Qname documentQname) {
			this.transactionId = transactionId;
			this.documentQname = documentQname;
		}
	}

	private final DAO dao;
	private final ErrorReporter errorReporter;
	private final HikariDataSource dataSource;

	public KastikkaPullReader(DAO dao, Config config, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super(SOURCE, threadHandler, statusReporter, dao);
		this.dao = dao;
		this.errorReporter = errorReporter;
		this.dataSource = KastikkaDataSourceDefinition.initDataSource(config.connectionDescription("KastikkaPull"));
	}

	@Override
	public void close() {
		if (dataSource != null) {
			dataSource.close();
		}
	}

	@Override
	public long read() {
		TransactionConnection con = null;
		try {
			con = getCon();
			long delay = tryToRead(con);
			return delay;
		} catch (Throwable t) {
			reportStatus("Handling exceptions");
			logError(t);
			errorReporter.report("Kastikka pull", t);
			return ERROR_PAUSE;
		} finally {
			if (con != null) con.release();
		}
	}

	private SimpleTransactionConnection getCon() throws SQLException {
		return new SimpleTransactionConnection(dataSource.getConnection());
	}

	private long tryToRead(TransactionConnection con) throws Exception {
		con.startTransaction();
		reportStatus("Getting next entries");
		List<TransactionEntry> entries = getNextEntries(con);
		if (entries.isEmpty()) return NO_WORK_PAUSE;

		try {
			reportStatus("Getting document models for " + entries.size() + " transactions");
			Collection<Model> models = getDocumentModels(entries, con);

			if (!models.isEmpty()) {
				reportStatus("Generating rdf+xml");
				String rdf = generateRdf(models);
				reportStatus("Storing to in pipe");
				dao.getETLDAO().storeToInPipe(SOURCE, rdf, "application/rdf+xml");
			}

			List<String> notFoundIds = findNotFoundIds(entries, models);
			if (!notFoundIds.isEmpty()) {
				reportStatus("Generating delete entries");
				String deleted = generateDeleted(notFoundIds);
				reportStatus("Storing to in pipe");
				dao.getETLDAO().storeToInPipe(SOURCE, deleted, "text/plain");
			}

			reportStatus("Deleting transaction entries");
			deleteTransactionEntries(con, entries);

			getThreadHandler().runInPipe(SOURCE);
			reportStatus("Done, will sleep..");
			con.commitTransaction();
			return SMALL_PAUSE;
		} catch (Throwable e) {
			reportStatus("Handling exceptions");
			setTransactionEntriesErroneous(con, entries);
			con.commitTransaction();
			throw e;
		}
	}

	private List<String> findNotFoundIds(List<TransactionEntry> entries, Collection<Model> models) {
		List<String> notFoundIds = allIds(entries);
		for (Model m : models) {
			notFoundIds.remove(m.getSubject().getQname());
		}
		return notFoundIds;
	}

	private List<String> allIds(List<TransactionEntry> entries) {
		return entries.stream().map(e->e.documentQname.toString()).collect(Collectors.toList());
	}

	private String generateDeleted(List<String> notFoundIds) {
		return notFoundIds.stream().map(id->"DELETE " + id).collect(Collectors.joining("\n"));
	}

	private void deleteTransactionEntries(TransactionConnection con, List<TransactionEntry> entries) throws SQLException {
		executeUpdate(con, "DELETE FROM transactions", entries);
	}

	private void setTransactionEntriesErroneous(TransactionConnection con, List<TransactionEntry> entries) throws SQLException {
		executeUpdate(con, "UPDATE transactions SET inerror = inerror + 1", entries);
	}

	private void executeUpdate(TransactionConnection con, String updateClause, List<TransactionEntry> entries) throws SQLException {
		String sql = generateUpdateStatement(updateClause, entries);
		PreparedStatement p = null;
		try {
			p = con.prepareStatement(sql);
			int i = 1;
			for (TransactionEntry e : entries) {
				p.setInt(i++, e.transactionId);
			}
			p.executeUpdate();
		} finally {
			Utils.close(p);
		}
	}

	private String generateUpdateStatement(String updateClause, List<TransactionEntry> entries) {
		StringBuilder sql = new StringBuilder(updateClause + " WHERE transactionid IN (");
		Iterator<TransactionEntry> iterator = entries.iterator();
		while (iterator.hasNext()) {
			iterator.next();
			sql.append("?");
			if (iterator.hasNext()) sql.append(",");
		}
		sql.append(")");
		return sql.toString();
	}

	private static final int ROWS_PER_FETCH = 10;

	private static final String FETCH_ROWS_SQL = "" +
			" SELECT transactionid, documenturi " +
			" FROM transactions " +
			" WHERE inerror = 0 " +
			" ORDER BY transactionid " +
			" FETCH FIRST " + ROWS_PER_FETCH + " ROWS ONLY  ";

	private static final String FETCH_EROROREUS_ROW_SQL = "" +
			" SELECT transactionid, documenturi " +
			" FROM transactions " +
			" WHERE inerror = 1 " +
			" ORDER BY transactionid " +
			" FETCH FIRST 1 ROWS ONLY ";

	private List<TransactionEntry> getNextEntries(TransactionConnection con) throws SQLException {
		return getNextEntries(con, false);
	}

	private List<TransactionEntry> getNextEntries(TransactionConnection con, boolean erroneous) throws SQLException {
		List<TransactionEntry> entries = new ArrayList<>();
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			if (!erroneous) {
				p = con.prepareStatement(FETCH_ROWS_SQL);
			} else {
				p = con.prepareStatement(FETCH_EROROREUS_ROW_SQL);
			}
			rs = p.executeQuery();
			while (rs.next()) {
				entries.add(new TransactionEntry(rs.getInt(1), new Qname(rs.getString(2))));
			}
			if (entries.isEmpty() && !erroneous) {
				return getNextEntries(con, true);
			}
		} finally {
			Utils.close(p, rs);
		}
		return entries;
	}

	private Collection<Model> getDocumentModels(List<TransactionEntry> entries, TransactionConnection con) throws SQLException {
		if (entries.isEmpty()) return Collections.emptyList();
		Map<String, Model> models = new HashMap<>();
		List<String> ids = new ArrayList<>();
		for (TransactionEntry entry : entries) {
			String id = entry.documentQname.toString();
			if (!ids.contains(id)) {
				ids.add(id);
			}
		}
		addDocumentTreeModels(models, ids, con);
		return models.values();
	}

	private static final String GET_MODELS_SQL_PRE = "" +
			" SELECT  subjectname, predicatename, objectname, resourceliteral, langcodefk, contextname, statementid " +
			" FROM    rdf_statementview                                                    " +
			" WHERE   subjectname IN ( ";

	private static final String GET_MODELS_SQL_POST = " ) ORDER BY subjectname ";

	private void addDocumentTreeModels(Map<String, Model> models, List<String> ids, TransactionConnection con) throws SQLException {
		List<List<String>> partitioned = Lists.partition(ids, 500);
		List<String> addedIds = new ArrayList<>();
		for (List<String> partitionIds : partitioned) {
			if (partitionIds.isEmpty()) continue;
			reportStatus("Getting document model for " + partitionIds.size() + " ids " + partitionIds.get(0) + " ... ");
			StringBuilder sql = new StringBuilder(GET_MODELS_SQL_PRE);
			inClause(sql, partitionIds);
			sql.append(GET_MODELS_SQL_POST);

			ResultSet rs = null;
			PreparedStatement p = null;
			try {
				p = con.prepareStatement(sql.toString());
				rs = executeSelect(partitionIds, p);
				addedIds.addAll(addModels(models, rs));
			} finally {
				Utils.close(p, rs);
			}
		}

		if (addedIds.isEmpty()) return;

		List<String> children = getChildren(addedIds, con);
		if (children.isEmpty()) return;

		addDocumentTreeModels(models, children, con);
		addModelLinks(models, children);
	}

	private void addModelLinks(Map<String, Model> models, List<String> children) {
		for (String childId : children) {
			addModelLink(models, childId);
		}
	}

	private void addModelLink(Map<String, Model> models, String childId) {
		Model childModel = models.get(childId);
		String parentId = null;
		long statementId = -1;
		for (Statement s : childModel.getStatements(MZ_IS_PART_OF_PREDICATE.getQname())) {
			parentId = s.getObjectResource().getQname();
			statementId = s.getId();
			break;
		}
		if (parentId == null) return;
		childModel.removeStatement(statementId);
		Model parentModel = models.get(parentId);
		parentModel.addStatement(new Statement(MZ_HAS_PART_PREDICATE, new ObjectResource(childId)));
	}

	private List<String> addModels(Map<String, Model> models, ResultSet rs) throws SQLException {
		List<String> addedIds = new ArrayList<>();
		String currentId = "";
		Model model = null;
		while (rs.next()) {
			String subject = rs.getString(1);
			if (!subject.equals(currentId)) {
				model = new Model(new Qname(subject));
				addedIds.add(subject);
				currentId = subject;
				models.put(subject, model);
			}
			toModel(rs, model);
		}

		return addedIds;
	}

	private static final String GET_CHILDREN_SQL_PRE = "" +
			" SELECT DISTINCT subjectname              " +
			" FROM  rdf_statementview                  " +
			" WHERE objectname IN ( ";

	private static final String GET_CHILDREN_SQL_POST = " ) AND predicatename = '"+MZ_IS_PART_OF_PREDICATE+"' ";

	private List<String> getChildren(List<String> ids, TransactionConnection con) throws SQLException {
		List<String> children = new ArrayList<>();
		List<List<String>> partitioned = Lists.partition(ids, 500);
		for (List<String> partitionedIds : partitioned) {
			if (partitionedIds.isEmpty()) continue;
			reportStatus("Getting children of " + partitionedIds.size() + " ids " + partitionedIds.get(0) + " ... ");
			StringBuilder sql = new StringBuilder(GET_CHILDREN_SQL_PRE);
			inClause(sql, partitionedIds);
			sql.append(GET_CHILDREN_SQL_POST);

			ResultSet rs = null;
			PreparedStatement p = null;
			try {
				p = con.prepareStatement(sql.toString());
				rs = executeSelect(partitionedIds, p);
				while (rs.next()) {
					children.add(rs.getString(1));
				}
			} finally {
				Utils.close(p, rs);
			}
		}
		return children;
	}

	private void inClause(StringBuilder sql, List<String> ids) {
		Iterator<String> i = ids.iterator();
		while (i.hasNext()) {
			i.next();
			sql.append("?");
			if (i.hasNext()) {
				sql.append(",");
			}
		}
	}

	private ResultSet executeSelect(List<String> ids, PreparedStatement p) throws SQLException {
		int i = 1;
		for (String id : ids) {
			p.setString(i++, id);
		}
		return p.executeQuery();
	}

	public void toModel(ResultSet rs, Model model) throws SQLException {
		String predicatename = rs.getString(2);
		String objectname = rs.getString(3);
		String objectliteral = rs.getString(4);
		String langcode = rs.getString(5);
		String contextname = rs.getString(6);
		long statementId = rs.getLong(7);
		Context context = contextname == null ? null : new Context(contextname);

		if (objectliteral == null && objectname == null) return;

		if ("MZ.editor".equals(predicatename)) predicatename = "MY.editor";

		Statement statement = null;
		if (objectliteral != null) {
			statement = new Statement(new Predicate(predicatename), new ObjectLiteral(clean(objectliteral), langcode), context);
		} else {
			statement = new Statement(new Predicate(predicatename), new ObjectResource(objectname), context);
		}
		statement.setId(statementId);
		model.addStatement(statement);
	}

	private String clean(String objectliteral) {
		String s = "";
		for (char c : objectliteral.toCharArray()) {
			if (invalid(c)) {
				s += " ";
			} else {
				s += c;
			}
		}
		return s.trim();
	}

	private boolean invalid(char c) {
		return Character.getType(c) == Character.CONTROL;
	}

	private String generateRdf(Collection<Model> models) {
		org.apache.jena.rdf.model.Model jenaModel = new InternalModelToJenaModelConverter(models).getJenaModel();
		String rdfXml = JenaUtils.getSerialized(jenaModel, RDFFormat.RDFXML_ABBREV);
		return rdfXml;
	}

}
