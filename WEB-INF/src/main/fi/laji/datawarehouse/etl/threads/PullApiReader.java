package fi.laji.datawarehouse.etl.threads;
//package fi.laji.datawarehouse.etl.models.custom_etl;
//
//import java.net.URI;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.client.methods.HttpGet;
//
//import fi.laji.datawarehouse.dao.DAO;
//import fi.laji.datawarehouse.etl.models.ReaderThread;
//import fi.laji.datawarehouse.etl.models.ThreadHandler;
//import fi.laji.datawarehouse.etl.models.ThreadStatusReporter;
//import fi.laji.datawarehouse.etl.models.containers.PullAPIData;
//import fi.luomus.commons.http.HttpClientService;
//import fi.luomus.commons.utils.LogUtils;
//import fi.luomus.commons.utils.URIBuilder;
//import fi.luomus.commons.xml.Document;
//import fi.luomus.commons.xml.Document.Node;
//
//public class PullApiReader extends ReaderThread {
//
//	private static class Entry {
//		private final int sequence;
//		private final String documentId;
//		private final URI url;
//		private final boolean isDelete;
//		public Entry(int sequence, String documentId, URI url, boolean isDelete) {
//			this.sequence = sequence;
//			this.documentId = documentId;
//			this.url = url;
//			this.isDelete = isDelete;
//		}
//	}
//
//	private final String apiURL;
//	private final DAO dao;
//
//	public PullApiReader(PullAPIData pullAPIData, DAO dao, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
//		super(pullAPIData.getSource(), threadHandler, statusReporter);
//		this.dao = dao;
//		this.apiURL = pullAPIData.getApiURL().toString();
//	}
//
//	@Override
//	protected long read() throws Exception {
//		HttpClientService client = new HttpClientService();
//		try {
//			return tryToRead(client);
//		} catch (Exception e) {
//			reportStatus("Handling exceptions");
//			dao.logError(source, this.getClass(), e);
//			return ERROR_PAUSE;
//		} finally {
//			client.close();
//		}
//	}
//
//	private long tryToRead(HttpClientService client) throws Exception {
//		reportStatus("Getting last read entry");
//		Integer lastReadEntry = dao.getLastReadPullApiEntrySequenceFor(source);
//
//		reportStatus("Getting feed");
//		List<Entry> entries = getEntries(lastReadEntry, client);
//		if (entries.isEmpty()) return NO_WORK_PAUSE;
//
//		reportStatus("Getting entries");
//		for (Entry entry : entries) {
//			readEntry(entry, client);
//		}
//
//		getThreadHandler().reportOutPipeJob(source);
//
//		reportStatus("Setting last read sequence");
//		dao.setLastReadPullApiEntrySequence(source, getLast(entries).sequence);
//		return SMALL_PAUSE;
//	}
//
//	private List<Entry> getEntries(Integer lastReadEntry, HttpClientService client) throws Exception {
//		List<Entry> entries = null;
//		if (lastReadEntry != null) {
//			entries = getNextEntries(lastReadEntry+1, client);
//		} else {
//			entries = getNextEntries(1, client);
//		}
//		return entries;
//	}
//
//	private Entry getLast(List<Entry> entries) {
//		return entries.get(entries.size() - 1);
//	}
//
//	private void readEntry(Entry entry, HttpClientService client) {
//		if (entry.isDelete) {
//			dao.storeToInPipe(source, "DELETE " + entry.documentId, "text/plain");
//		} else {
//			try {
//				String response = client.contentAsString(new HttpGet(entry.url));
//				dao.storeToInPipe(source, response, "application/xml");
//			} catch (Exception e) {
//				String stacktrace = LogUtils.buildStackTrace(e);
//				dao.storeToInPipe(source, stacktrace, "text/plain");
//			}
//		}
//	}
//
//	private List<Entry> getNextEntries(int startSequence, HttpClientService client) throws Exception {
//		List<Entry> entries = new ArrayList<>();
//		URIBuilder uri = new URIBuilder(apiURL).addParameter("from", startSequence).addParameter("limit", 25);
//		reportStatus("Getting feed: " + uri.toString());
//		Document response = client.contentAsDocument(new HttpGet(uri.getURI()));
//		for (Node entry : response.getRootNode().getChildNodes("entry")) {
//			int sequence = Integer.valueOf(entry.getNode("id").getContents());
//			String documentId = entry.getNode("title").getContents();
//			URI url = new URI(entry.getNode("link").getAttribute("href"));
//			boolean isDelete = entry.hasChildNodes("summary") && entry.getNode("summary").getContents().equals("DELETE");
//			entries.add(new Entry(sequence, documentId, url, isDelete));
//		}
//		return entries;
//	}
//
//	@Override
//	public void close() {}
//
//}
