package fi.laji.datawarehouse.etl.threads.custom;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.github.jsonldjava.shaded.com.google.common.collect.Lists;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.Streams;
import fi.laji.datawarehouse.dao.Streams.ResultSetStream;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.Streams.ScrollableResultsStream;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer.EventOccurrences;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer.OccurrenceData;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

public class LajiGISPullReader extends ETLTableSyncLogic_BasePullReader<EventOccurrences> {

	public static final Qname SOURCE = new Qname("KE.921");

	private final LajiGISDataOriginDAO originalSource;
	private final DwTempTablesDAO tempTables;

	public LajiGISPullReader(Config config, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super("LajiGIS", Utils.list("occurrences", "geo", "event_geo", "projects"), SOURCE, new LajiGISHarmonizer(), dao, errorReporter, threadHandler, statusReporter);
		this.originalSource = new LajiGISDAOImple(config.connectionDescription("LajiGIS"));
		this.tempTables = new DwTempTablesDAOImple(dao.getETLDAO(), statusReporter);
	}

	@Override
	protected ResultStream<EventOccurrences> getIncomingEntryStream() throws Exception {
		for (OccurrenceSource source : originalSource.getOccurrenceSources()) {
			log("LajiGIS -> ETL: " + source.getName());
			try (ResultSetStream<Occurrence> stream = source.getStream()) {
				int i = tempTables.insertOccurrences(stream);
				log("LajiGIS -> ETL: " + source.getName() + " done: " + i + " rows");
			}
		}

		if (shouldStop()) return abort();

		log("LajiGIS -> ETL: geos");
		try (ResultSetStream<OccurrenceGeo> geoStream = originalSource.getGeos()) {
			int i = tempTables.insertGeos(geoStream);
			log("LajiGIS -> ETL: geos done: " + i + " rows");
		}

		if (shouldStop()) return abort();

		log("LajiGIS -> ETL: event geos");
		try (ResultSetStream<EventGeo> geoStream = originalSource.getEventGeos()) {
			int i = tempTables.insertEventGeos(geoStream);
			log("LajiGIS -> ETL: event geos done: " + i + " rows");
		}

		if (shouldStop()) return abort();

		log("LajiGIS -> ETL: projects");
		try (ResultSetStream<EventProject> projectStream = originalSource.getProjects()) {
			int i = tempTables.insertProjects(projectStream);
			log("LajiGIS -> ETL: projects done: " + i + " rows");
		}

		if (shouldStop()) return abort();

		return new IncomingStream();
	}

	private ResultStream<EventOccurrences> abort() {
		return Streams.abort();
	}

	private class IncomingStream implements ResultStream<EventOccurrences> {

		private class Batch {
			final Set<Integer> eventIds = new HashSet<>();
			final List<Integer> occurrenceIds = new ArrayList<>();
			final List<EventOccurrences> data = new ArrayList<>();
			int size = 0;
			void add(EventOccurrences e) {
				if (e.eventId != null) eventIds.add(e.eventId);
				e.occurrences.stream().map(o->o.occurrenceId).filter(id->id > 0).forEach(occurrenceIds::add);
				size += e.occurrences.size();
				data.add(e);
			}
			public boolean isFull() {
				return size > 500;
			}
			public boolean isEmpty() {
				return data.isEmpty() || size == 0;
			}
		}

		private ResultStream<Occurrence> incomingStream;
		private Iterator<Occurrence> incomingIterator;
		private Set<Integer> buggedEventIds;
		private Iterator<EventOccurrences> currentBatch = null;
		private Occurrence lastIteratedOccurrence = null;
		private List<OccurrenceData> currentEventOccurrences = new ArrayList<>();

		public IncomingStream() throws Exception {
			log("Fetching bugged eventIds");
			this.buggedEventIds = originalSource.getBuggedEventIds();
			log("Fetching bugged eventIds done: " + buggedEventIds.size() + " rows");

			reportStatus("Executing getOccurrences query");
			log("Generating ETL documents");
			this.incomingStream = tempTables.getOccurrences();
			this.incomingIterator = this.incomingStream.iterator();
			reportStatus("Scrolling results of getOccurrences");
		}

		@Override
		public Iterator<EventOccurrences> iterator() {
			return new Iterator<LajiGISHarmonizer.EventOccurrences>() {

				@Override
				public EventOccurrences next() {
					if (currentBatch == null || !currentBatch.hasNext()) {
						currentBatch = nextBatch();
					}
					return currentBatch.next();
				}

				@Override
				public boolean hasNext() {
					if (currentBatch == null) return true; // first row
					if (currentBatch.hasNext()) return true;
					currentBatch = nextBatch();
					return currentBatch.hasNext();
				}
			};

		}

		private Iterator<EventOccurrences> nextBatch() throws ETLException {
			Batch batch = new Batch();
			while (incomingIterator.hasNext()) {
				Occurrence o = incomingIterator.next();
				if (buggedEventIds.contains(o.getEventId())) continue;
				if (eventChanges(o)) {
					batch.add(new EventOccurrences(lastIteratedOccurrence.getEventId(), currentEventOccurrences));
					if (batch.isFull()) {
						lastIteratedOccurrence = o;
						currentEventOccurrences = new ArrayList<>();
						currentEventOccurrences.add(new OccurrenceData(o.eventId, o.occurrenceId, o.json));
						return finalizeBatch(batch);
					}
					currentEventOccurrences = new ArrayList<>();
				}
				lastIteratedOccurrence = o;
				currentEventOccurrences.add(new OccurrenceData(o.eventId, o.occurrenceId, o.json));
			}
			if (!currentEventOccurrences.isEmpty()) {
				batch.add(new EventOccurrences(lastIteratedOccurrence.getEventId(), currentEventOccurrences));
				currentEventOccurrences = new ArrayList<>();
			}
			return finalizeBatch(batch);
		}

		private Iterator<EventOccurrences> finalizeBatch(Batch batch) {
			fillData(batch);
			return batch.data.iterator();
		}

		private boolean eventChanges(Occurrence o) {
			if (lastIteratedOccurrence == null) return false;
			if (o.eventId == null) return true;
			return !o.eventId.equals(lastIteratedOccurrence.eventId);
		}

		private void fillData(Batch batch) {
			if (batch.isEmpty()) return;
			try {
				reportStatus("Getting geos for " + batch.occurrenceIds.size() + " occurrences");
				Map<Integer, List<String>> occurrenceWKTs = tempTables.getGeos(batch.occurrenceIds);
				Map<Integer, List<String>> eventWKTs = tempTables.getEventGeos(batch.eventIds);
				reportStatus("Getting project names for " + batch.eventIds.size() + " events");
				Map<Integer, List<String>> projectNames = tempTables.getProjects(batch.eventIds);
				for (EventOccurrences e : batch.data) {
					for (OccurrenceData o : e.occurrences) {
						o.occurrenceWKT = occurrenceWKTs.get(o.occurrenceId);
						o.eventWKT = eventWKTs.get(o.eventId);
						o.projectNames = projectNames.get(o.eventId);
					}
				}
			} catch (Exception e) {
				throw new ETLException(e);
			}
			reportStatus("Batch with " + batch.eventIds.size() + " events and " + batch.occurrenceIds.size() + " occurrences is ready for harmonization ("+dump(batch.occurrenceIds) + ")");
		}

		private String dump(List<Integer> occurrenceIds) {
			String s = occurrenceIds.stream().limit(3).map(id->id.toString()).collect(Collectors.joining(","));
			if (occurrenceIds.size() > 3) return s + ",...";
			return s;
		}

		@Override
		public void close() {
			if (incomingStream != null) incomingStream.close();
		}

	}

	private interface LajiGISDataOriginDAO {
		List<OccurrenceSource> getOccurrenceSources() throws Exception;
		ResultSetStream<OccurrenceGeo> getGeos() throws Exception;
		ResultSetStream<EventGeo> getEventGeos() throws Exception;
		ResultSetStream<EventProject> getProjects() throws Exception;
		Set<Integer> getBuggedEventIds() throws Exception;
	}

	private interface OccurrenceSource {
		String getName();
		ResultSetStream<Occurrence> getStream() throws Exception;
	}

	private static class LajiGISDAOImple implements LajiGISDataOriginDAO {

		private final ConnectionDescription desc;

		public LajiGISDAOImple(ConnectionDescription desc) {
			this.desc = desc;
		}

		private SimpleTransactionConnection openLajiGISConnection() throws SQLException {
			return new SimpleTransactionConnection(desc);
		}

		private class OccurrenceInstanter implements ResultSetStream.ToInstance<Occurrence> {

			private class Fact {
				String fact;
				final Map<String, String> values = new HashMap<>();
				public Fact(String fact) {
					this.fact = fact;
				}
			}
			private final Map<String, Fact> facts;

			public OccurrenceInstanter() throws SQLException {
				this.facts = loadFacts();
				customFactName("LG_KOHDE.KOHDE_TASO", LajiGISHarmonizer.SITE_TYPE_2);
				customFactName("LG_KOHDE.ELIORYHMA", "Eliöryhmä");
				customFactName("LG_KOHDE.RUUDUN_SYVYYS", "Arviointiruudun syvyys (m)");
				customFactName("LG_KOHDE.SEURATTAVA", "Seurattava");
				customFactName("LG_LAJIHAV.PEITTAVYYS_PROS", "Peittävyysprosentti");
				customFactName("LG_LAJIHAV.PALJAS_POT_KASVUALA", "Paljas potentiaalinen kasvuala");
				customFactName("LG_LAJIHAV.POT_KASVUALA", "Potentiaalinen kasvuala");
				customFactName("LG_LAJIHAV.HAV_TLAHDE_KUVAUS", LajiGISHarmonizer.DATA_SOURCE_2);
				customFactName("LG_LAJIHAV.EPIFYYTTI", "Laji kasvaa epifyyttinä");
				customFactName("LG_LAJIHAV.LAJIN_KESKIM_KORKEUS", "Lajin keskimääräinen korkeus (cm)");
				customFactName("LG_VIDEO.SYVYYS_VIDEON_ALUSSA", "Syvyys videon alussa (m)");
				customFactName("LG_VIDEO.SYVYYS_VIDEON_LOPUSSA", "Syvyys videon lopussa (m)");
				customFactName("LG_KERTA_VESI.MERI_ID", "Merialueen tunniste");
				customFactName("LG_KERTA_VESI.PL_ARVIO_EPAVARMA", "Pohjanlaatuarvio epävarma");
				customFactName("LG_KERTA_VESI.KIVI100", "Pohjanlaatu: Iso kivi 100-600 mm (%)");
				customFactName("LG_KERTA_VESI.KIVI60", "Pohjanlaatu: Pieni kivi 60-100 mm (%)");
				customFactName("LG_KERTA_VESI.LIIKKUVA", "Pohjanlaatu: Liikkuva pohjatyyppi (%)");
				customFactName("LG_KERTA_VESI.LOHKARE3000", "Pohjanlaatu: Lohkare >3000 mm (%)");
				customFactName("LG_KERTA_VESI.LOHKARE1200", "Pohjanlaatu: Lohkare 1200-3000 mm (%)");
				customFactName("LG_KERTA_VESI.LOHKARE600", "Pohjanlaatu: Lohkare 600-1200 mm (%)");
				customFactName("LG_KERTA_VESI.KONKRETIO", "Pohjanlaatu: Konkreetiot (%)");
				customFactName("LG_KERTA_VESI.KALLIO", "Pohjanlaatu: Kallio (%)");
				customFactName("LG_KERTA_VESI.TURVE", "Pohjanlaatu: Turve (%)");
				customFactName("LG_KERTA_VESI.HIEKKA", "Pohjanlaatu: Hiekka 0,06-2,0 mm (%)");
				customFactName("LG_KERTA_VESI.SILTTI", "Pohjanlaatu: Siltti 0,002-0,06 mm (%)");
				customFactName("LG_KERTA_VESI.SAVI", "Pohjanlaatu: Savi <0,002 mm (%)");
				customFactName("LG_KERTA_VESI.SORA", "Pohjanlaatu: Sora 2,0-60 mm (%)");
				customFactName("LG_KERTA_VESI.MUTA", "Pohjanlaatu: Muta <0,002 mm (%)");
				customFactName("LG_KERTA_VESI.KEINOTEKOINEN", "Pohjanlaatu: Keinotekoinen alusta (%)");
				customFactName("LG_KERTA_VESI.LIIKKUMATON", "Pohjanlaatu: Liikkumaton pohjatyyppi (%)");
				customFactName("LG_KERTA_VESI.PUU", "Pohjanlaatu: Puun rungot/oksat (%)");
				customFactName("LG_KERTA_VESI.GLASIAALISAVI", "Pohjanlaatu: Glasiaalisavi (%)");
				customFactName("LG_KERTA_VESI.HIEKKAKIVI", "Pohjanlaatu: Hiekkakivi (%)");
			}

			private void customFactName(String field, String factName) {
				if (this.facts.containsKey(field)) {
					this.facts.get(field).fact = factName;
				} else {
					this.facts.put(field, new Fact(factName));
				}
			}

			private Map<String, Fact> loadFacts() throws SQLException {
				Map<String, Fact> facts = new HashMap<>();
				TransactionConnection con = null;
				PreparedStatement p = null;
				ResultSet rs = null;
				try {
					String sql = "" +
							" SELECT	d.taulu as table_name, d.kentta as col_name, ala.koodi as col_value, paa.selite as fact, ala.selite as fact_value " +
							" FROM		DOMAINIT d " +
							" LEFT JOIN	MHS_KOODI_PAA paa ON d.domain = paa.nimi " +
							" LEFT JOIN	MHS_KOODI_ALA ala ON paa.koodi_paa_id = ala.koodi_paa_id " +
							" ORDER BY d.taulu, d.kentta, paa.nimi ";
					con = openLajiGISConnection();
					p = con.prepareStatement(sql);
					rs = p.executeQuery();
					while (rs.next()) {
						String field = (rs.getString(1) + "." + rs.getString(2)).toUpperCase();
						String value = rs.getString(3).toUpperCase();
						String fact = rs.getString(4);
						String factValue = rs.getString(5);
						if (fact == null) fact = field;
						if (factValue == null) factValue = value;
						if (!facts.containsKey(field)) {
							facts.put(field, new Fact(fact));
						}
						facts.get(field).values.put(value, factValue);
					}
				} finally {
					Utils.close(p, rs, con);
				}
				return facts;
			}

			@Override
			public Occurrence get(ResultSet rs) throws SQLException {
				Occurrence o = new Occurrence();
				o.eventId = rs.getInt(1) == 0 ? null : rs.getInt(1);
				o.occurrenceId = rs.getInt(2) == 0 ? -1 : rs.getInt(2);
				o.json = toJson(rs);
				return o;
			}

			private String toJson(ResultSet rs) throws SQLException {
				ResultSetMetaData meta = rs.getMetaData();
				JSONObject json = new JSONObject();
				for (int col = 1; col <= meta.getColumnCount(); col++) {
					String colValue = rs.getString(col);
					if (colValue == null) continue;
					colValue = clean(colValue);
					String colName = meta.getColumnLabel(col);
					if (isFact(colName)) {
						factToJson(json, colValue, colName);
					} else {
						json.setString(colName, colValue);
					}
				}
				return json.toString();
			}

			private void factToJson(JSONObject json, String colValue, String colName) {
				colName = colName.replace("FACT_", "").toUpperCase();
				Fact fact = facts.get(colName);
				if (fact != null) {
					String factValue = fact.values.get(colValue.toUpperCase());
					if (factValue == null) factValue = colValue;
					JSONObject factJson = new JSONObject();
					factJson.setString("fact", fact.fact);
					factJson.setString("value", factValue);
					factJson.setString("columnName", colName);
					json.getArray("facts").appendObject(factJson);
				} else {
					JSONObject factJson = new JSONObject();
					factJson.setString("fact", colName);
					factJson.setString("value", colValue);
					factJson.setString("columnName", colName);
					json.getArray("facts").appendObject(factJson);
				}
			}

			private boolean isFact(String colName) {
				return colName.startsWith("FACT_");
			}

		}

		@Override
		public List<OccurrenceSource> getOccurrenceSources() throws Exception {
			return Utils.list(
					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getGeneralOccurrences();
						}

						@Override
						public String getName() {
							return "general occurrences";
						}
					},

					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getSealGeneralOccurrences();
						}

						@Override
						public String getName() {
							return "general occurrences: seal";
						}
					},

					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getVelmuOccurrences();
						}

						@Override
						public String getName() {
							return "velmu occurrences";
						}
					},

					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getMonitoringSiteOccurrences();
						}

						@Override
						public String getName() {
							return "monitoring site occurrences";
						}
					},

					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getMonitoringSiteZeroOccurrences();
						}

						@Override
						public String getName() {
							return "monitoring site zero occurrences";
						}
					},

					new OccurrenceSource() {
						@Override
						public ResultSetStream<Occurrence> getStream() throws Exception {
							return getSpeciesSurveyOccurrences();
						}

						@Override
						public String getName() {
							return "species survey occurrences";
						}
					}
					);
		}

		private ResultSetStream<Occurrence> getGeneralOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT " +
					"	h_k.lg_kertaid as eventId, " + // "LGK." document.documentID; documentId + #G -> gathering.gatheringId
					"	h.lg_lajihavid as occurrenceId, " + // unit.keyword; gatheringId + h.lg_lajihavid -> unit.unitID (same lajihav can be in multi kerta); if kerta is NULL, h.lg_lajihavid = "LGH." documentId, #G, #U
					"	t.kartoituksen_tila as [FACT_lg_kart_tila.kartoituksen_tila], " +
					"	h.havainnon_laatu as recordBasis, " + // see record basis
					"	h.havainnon_laatu as [FACT_lg_lajihav.havainnon_laatu], " + // unit fact
					"	h.el_laji_id as herttaId, " +
					"	laji.laji_uri as taxonId, " + // unit.taxonId
					"	h.havaitsijan_antama_nimi as taxonVerbatim, " + // unit.taxonVerbatim
					"	lajinimi.nimi as scientificName, " + // unit.taxonVerbatim (if taxonVerbatim missing)
					"	lajinimi.auktori as scientificNameAuthor, " + // unit.author (if taxonVerbatim missing)
					"	h.maara as amount, " + // see Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as amountUnit, " + // Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as [FACT_lg_lajihav.maaran_yksikko], " + // Amount / individual counts / lifeStage / etc
					"	h.havaitsija as team, " + // gathering.team
					"   h.kayttooikeus as personHiding, " +
					"   h.num_lahde as [FACT_lg_lajihav.num_lahde], " + // document fact
					"	CONVERT(VARCHAR(25), h.pvm_alku, 127) as dateBegin, " + // gathering.eventDate.begin
					"	CONVERT(VARCHAR(25), h.pvm_loppu, 127) as dateEnd, " + // gathering.eventDate.end
					"	kunta.selite as municipality, " + // gathering.municipality (verbatim)
					"	em.selite as biogeographicalProvince, " + // gathering.biogeographicalProvince (verbatim)
					"	va.selite as basinName, " + // locality ( + fact Vesistöalue)
					"	h.kp_x as lonEurefCenter, " +
					"	h.kp_y as latEurefCenter, " +
					"	h.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	h.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	h.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (h.osuus_mhsa > 99 OR h.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	h.lajin_keskim_korkeus as [FACT_lg_lajihav.lajin_keskim_korkeus], " + // unit fact
					"	h.peittavyys_pros as [FACT_lg_lajihav.peittavyys_pros], " + // unit fact
					"	h.tarkistustarve as qualityControl, " + // see Quality control
					"	h.tarkistustarve as [FACT_lg_lajihav.tarkistustarve], " + // see Quality control
					"	n.nayte_keraaja as leg, " + // gathering.team (add together with havaitsija; unique values only stored)
					"	n.nayte_maarittaja as det, " + // unit.det
					"	n.nayte_sijainti as [FACT_lg_nayte.nayte_sijainti], " + // unit fact
					"	n.nayte_tyyppi as [FACT_lg_nayte.nayte_tyyppi], " + // unit fact
					"	n.nayte_uri as specimenUri, " + // unit.keyword
					"	n.nayte_museo_naytenro as specimenAdditionalId, " + // -- unit.keyword
					"	h.havainnon_kuvaus as unitNotes, " + // unit.notes
					"	h.huomautukset as unitNotes2, " + // unit.notes
					"	h.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus], " + // document fact
					"	ke.huomautukset as gatheringNotes " + // gathering.notes
					"	FROM      LG_LAJIHAV h " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON h.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON h.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON h.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	LEFT JOIN LG_LAJI laji            ON h.el_laji_id   = laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1" +
					"	LEFT JOIN LG_NAYTE n              ON n.objectid = (SELECT TOP 1 objectid FROM LG_NAYTE WHERE h.lg_lajihavid = lg_lajihavid ORDER BY objectid) " +
					"	LEFT JOIN LG_LAJIHAV_TO_KERTA h_k ON h.lg_lajihavid = h_k.lg_lajihavid " +
					"	LEFT JOIN LG_KERTA ke             ON ke.lg_kertaid  = h_k.lg_kertaid " +
					"	LEFT JOIN LG_KART_TILA t          ON t.lg_kertaid   = ke.lg_kertaid AND t.tilan_voimassa_olo = 1 " +
					"	LEFT JOIN LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					"	WHERE     h.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IS NULL " +
					"	AND       h.EL_LAJI_ID != 1349 " +
					"   ORDER BY  h_k.lg_kertaid, h.lg_lajihavid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private ResultSetStream<Occurrence> getSealGeneralOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT " +
					"	h_k.lg_kertaid as eventId, " + // "LGK." document.documentID; documentId + #G -> gathering.gatheringId
					"	h.lg_lajihavid as occurrenceId, " + // unit.keyword; gatheringId + h.lg_lajihavid -> unit.unitID (same lajihav can be in multi kerta); if kerta is NULL, h.lg_lajihavid = "LGH." documentId, #G, #U
					"   -2 as namedPlaceType, " +
					"	h.havainnon_laatu as recordBasis, " + // see record basis
					"	h.havainnon_laatu as [FACT_lg_lajihav.havainnon_laatu], " + // unit fact
					"	h.el_laji_id as herttaId, " +
					"	laji.laji_uri as taxonId, " + // unit.taxonId
					"	h.havaitsijan_antama_nimi as taxonVerbatim, " + // unit.taxonVerbatim
					"	lajinimi.nimi as scientificName, " + // unit.taxonVerbatim (if taxonVerbatim missing)
					"	lajinimi.auktori as scientificNameAuthor, " + // unit.author (if taxonVerbatim missing)
					"	h.maara as amount, " + // see Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as amountUnit, " + // Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as [FACT_lg_lajihav.maaran_yksikko], " + // Amount / individual counts / lifeStage / etc
					"	h.havaitsija as team, " + // gathering.team
					"   h.kayttooikeus as personHiding, " +
					"   h.num_lahde as [FACT_lg_lajihav.num_lahde], " + // document fact
					"	CONVERT(VARCHAR(25), h.pvm_alku, 127) as dateBegin, " + // gathering.eventDate.begin
					"	CONVERT(VARCHAR(25), h.pvm_loppu, 127) as dateEnd, " + // gathering.eventDate.end
					"	kunta.selite as municipality, " + // gathering.municipality (verbatim)
					"	em.selite as biogeographicalProvince, " + // gathering.biogeographicalProvince (verbatim)
					"	va.selite as basinName, " + // locality ( + fact Vesistöalue)
					"	h.kp_x as lonEurefCenter, " +
					"	h.kp_y as latEurefCenter, " +
					"	h.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	h.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	h.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (h.osuus_mhsa > 99 OR h.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	h.tarkistustarve as qualityControl, " + // see Quality control
					"	h.tarkistustarve as [FACT_lg_lajihav.tarkistustarve], " + // see Quality control
					"	h.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus] " + // document fact
					"	FROM      LG_LAJIHAV h " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON h.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON h.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON h.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	LEFT JOIN LG_LAJI laji            ON h.el_laji_id   = laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1" +
					"	LEFT JOIN LG_LAJIHAV_TO_KERTA h_k ON h.lg_lajihavid = h_k.lg_lajihavid " +
					"	LEFT JOIN LG_KERTA ke             ON ke.lg_kertaid  = h_k.lg_kertaid " +
					"	LEFT JOIN LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					"	WHERE     h.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IS NULL " +
					"	AND       h.EL_LAJI_ID = 1349 " +
					"   ORDER BY  h_k.lg_kertaid, h.lg_lajihavid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private ResultSetStream<Occurrence> getVelmuOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT " +
					"	h_k.lg_kertaid as eventId, " + // "LGK." document.documentID; documentId + #G -> gathering.gatheringId
					"	h.lg_lajihavid as occurrenceId, " + // unit.keyword; gatheringId + h.lg_lajihavid -> unit.unitID (same lajihav can be in multi kerta)
					"	kohde.lg_kohdeid as namedPlaceId,  " + // document.namedPlaceID
					"	kohde.nimi as onlyPrivateLocality,  " + // gathering.locality - but for Velmu only to private
					"	kohde.kohde_tyyppi as namedPlaceType, " + // document.collectionId
					"	kohde.kohde_tyyppi as [FACT_lg_kohde.kohde_tyyppi], " + // document fact
					"	kohde.seurattava as [FACT_lg_kohde.seurattava], " + // .document fact
					"	kohde.menetelma as [FACT_lg_kohde.menetelma], " + // document fact
					"	kohde_v.point_id as additionalId, " + // document.keyword
					"	kohde_v.menetelma_tark_vesi as [FACT_lg_kohde_vesi.menetelma_tark_vesi], " + // document fact
					"   kohde.elioryhma as [FACT_lg_kohde.elioryhma], " +
					//	-- gathering facts --------------
					"	kv.meri_id as [FACT_lg_kerta_vesi.meri_id], " +
					"	t.kartoituksen_tila as [FACT_lg_kart_tila.kartoituksen_tila], " +
					//  -- gathering facts ONLY PRIVATE ----
					"	kv.peittavyysarviointi as [FACT_lg_kerta_vesi.peittavyysarviointi], " +
					"	kv.pl_arvio_epavarma as [FACT_lg_kerta_vesi.pl_arvio_epavarma], " +
					"	kv.kallio as [FACT_lg_kerta_vesi.kallio], " +
					"	kv.lohkare3000 as [FACT_lg_kerta_vesi.lohkare3000], " +
					"	kv.lohkare1200 as [FACT_lg_kerta_vesi.lohkare1200], " +
					"	kv.lohkare600 as [FACT_lg_kerta_vesi.lohkare600], " +
					"	kv.glasiaalisavi as [FACT_lg_kerta_vesi.glasiaalisavi], " +
					"	kv.kivi100 as [FACT_lg_kerta_vesi.kivi100], " +
					"	kv.liikkumaton as [FACT_lg_kerta_vesi.liikkumaton], " +
					"	kv.kivi60 as [FACT_lg_kerta_vesi.kivi60], " +
					"	kv.sora as [FACT_lg_kerta_vesi.sora], " +
					"	kv.hiekka as [FACT_lg_kerta_vesi.hiekka], " +
					"	kv.siltti as [FACT_lg_kerta_vesi.siltti], " +
					"	kv.savi as [FACT_lg_kerta_vesi.savi], " +
					"	kv.muta as [FACT_lg_kerta_vesi.muta], " +
					"	kv.liikkuva as [FACT_lg_kerta_vesi.liikkuva], " +
					"	kv.konkretio as [FACT_lg_kerta_vesi.konkretio], " +
					"	kv.hiekkakivi as [FACT_lg_kerta_vesi.hiekkakivi], " +
					"	kv.keinotekoinen as [FACT_lg_kerta_vesi.keinotekoinen], " +
					"	kv.turve as [FACT_lg_kerta_vesi.turve], " +
					"	kv.puu as [FACT_lg_kerta_vesi.puu], " +
					"	v.syvyys_videon_alussa as [FACT_lg_video.syvyys_videon_alussa], " +
					"	v.syvyys_videon_lopussa as [FACT_lg_video.syvyys_videon_lopussa], " +
					"	kohde_v.ruudun_syvyys as [FACT_lg_kohde.ruudun_syvyys], " +
					//	---------------------------------
					"	h.pot_kasvuala as [FACT_lg_lajihav.pot_kasvuala], " + // unit fact
					"	h.paljas_pot_kasvuala as [FACT_lg_lajihav.paljas_pot_kasvuala], " + // unit fact
					"	h.havainnon_laatu as recordBasis, " + // see record basis
					"	h.havainnon_laatu as [FACT_lg_lajihav.havainnon_laatu], " + // unit fact
					"	h.el_laji_id as herttaId, " +
					"	laji.laji_uri as taxonId, " + // unit.taxonId
					"	h.havaitsijan_antama_nimi as taxonVerbatim, " + // unit.taxonVerbatim
					"	lajinimi.nimi as scientificName, " + // unit.taxonVerbatim (if taxonVerbatim missing)
					"	lajinimi.auktori as scientificNameAuthor, " + // unit.author (if taxonVerbatim missing)
					"	h.maara as amount, " + // see Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as amountUnit, " + // Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as [FACT_lg_lajihav.maaran_yksikko], " + // Amount / individual counts / lifeStage / etc
					"	h.havaitsija as team, " + // gathering.team
					"   h.kayttooikeus as personHiding, " +
					"   h.num_lahde as [FACT_lg_lajihav.num_lahde], " + // document fact
					"	CONVERT(VARCHAR(25), h.pvm_alku, 127) as dateBegin, " + // gathering.eventDate.begin
					"	CONVERT(VARCHAR(25), h.pvm_loppu, 127) as dateEnd, " + // gathering.eventDate.end
					"	kunta.selite as municipality, " + // gathering.municipality (verbatim)
					"	em.selite as biogeographicalProvince, " + // gathering.biogeographicalProvince (verbatim)
					"	va.selite as basinName, " + // locality ( + fact Vesistöalue)
					"	h.kp_x as lonEurefCenter, " +
					"	h.kp_y as latEurefCenter, " +
					"	h.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	h.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	h.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (h.osuus_mhsa > 99 OR h.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	h.lajin_keskim_korkeus as [FACT_lg_lajihav.lajin_keskim_korkeus], " + // unit fact
					"	h.peittavyys_pros as [FACT_lg_lajihav.peittavyys_pros], " + // unit fact
					"	h.epifyytti as [FACT_lg_lajihav.epifyytti], " + // unit fact
					"	h.tarkistustarve as qualityControl, " + // see Quality control
					"	h.tarkistustarve as [FACT_lg_lajihav.tarkistustarve], " + // see Quality control
					"	n.nayte_keraaja as leg, " + // gathering.team (add together with havaitsija; unique values only stored)
					"	n.nayte_maarittaja as det, " + // unit.det
					"	n.nayte_sijainti as [FACT_lg_nayte.nayte_sijainti], " + // unit fact
					"	n.nayte_tyyppi as [FACT_lg_nayte.nayte_tyyppi], " + // unit fact
					"	n.nayte_uri as specimenUri, " + // unit.keyword
					"	n.nayte_museo_naytenro as specimenAdditionalId," + // -- unit.keyword
					"	h.havainnon_kuvaus as unitNotes, " + // unit.notes
					"	h.huomautukset as unitNotes2, " + // unit.notes
					"	h.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus], " + // document fact
					"	ke.huomautukset as gatheringNotes " + // gathering.notes
					"	FROM      LG_LAJIHAV h " +
					"	JOIN      LG_LAJIHAV_TO_KERTA h_k ON h.lg_lajihavid = h_k.lg_lajihavid " +
					"	JOIN      LG_KERTA ke             ON ke.lg_kertaid  = h_k.lg_kertaid " +
					"	JOIN      LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					"	LEFT JOIN LG_KOHDE_VESI kohde_v   ON ke.lg_kohdeid  = kohde_v.lg_kohdeid " +
					"	LEFT JOIN LG_KERTA_VESI kv        ON kv.lg_kertaid  = ke.lg_kertaid " +
					"	LEFT JOIN LG_VIDEO v              ON v.lg_kertaid   = ke.lg_kertaid " +
					"	LEFT JOIN LG_KART_TILA t          ON t.lg_kertaid   = ke.lg_kertaid AND t.tilan_voimassa_olo = 1 " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON h.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON h.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON h.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	LEFT JOIN LG_LAJI laji            ON h.el_laji_id   = laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1" +
					"	LEFT JOIN LG_NAYTE n              ON n.objectid = (SELECT TOP 1 objectid FROM LG_NAYTE WHERE h.lg_lajihavid = lg_lajihavid ORDER BY objectid) " +
					"	WHERE     h.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IN (20, 21) " +
					"	AND       (kohde.menetelma IS NULL OR kohde.menetelma NOT IN (42,43) OR v.videotiedoston_id IS NOT NULL) " +
					"   ORDER BY  h_k.lg_kertaid, h.lg_lajihavid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private ResultSetStream<Occurrence> getMonitoringSiteOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT " +
					"	h_k.lg_kertaid as eventId, " + // "LGK." document.documentID; documentId + #G -> gathering.gatheringId
					"	h.lg_lajihavid as occurrenceId, " + // unit.keyword; gatheringId + h.lg_lajihavid -> unit.unitID (same lajihav can be in multi kerta)
					"	kohde.lg_kohdeid as namedPlaceId, " + // document.namedPlaceID
					"	kohde.nimi as locality, " + // gathering.locality
					"	CASE WHEN kohde.el_laji_id IN (142, 763, 677) THEN -1 ELSE kohde.kohde_tyyppi END as namedPlaceType, " + // document.collectionId
					"	kohde.kohde_tyyppi as [FACT_lg_kohde.kohde_tyyppi], " + // document fact
					"	kohde.menetelma as [FACT_lg_kohde.menetelma], " + // document fact
					"	kohde.huomautukset as documentNotes, " + // document.notes
					"	kohde.kohteen_kuvaus as documentNotes2, " + // document.notes
					"	kohde.kohde_taso as [FACT_lg_kohde.kohde_taso], " + // document fact
					"	kohde_s.el_paikan_tila as [FACT_lg_kohde_seur.el_paikan_tila], " + // document fact
					"	kohde_laji.laji_uri as namedPlaceTaxonId, " + // document fact
					"	kohde_lajinimi.nimi as namedPlaceTaxonScientificName, " + // document fact if taxonId missing
					"   kohde.elioryhma as [FACT_lg_kohde.elioryhma], " + // unit fact
					"	ke.ei_havaintoja as [FACT_lg_kerta.ei_havaintoja], " + // document fact
					"   km.pesintatulos as [FACT_lg_kerta_maa.pesintatulos], " + // gathering fact
					"   km.pesintatulos as nestingStatus, " +
					"	t.kartoituksen_tila as [FACT_lg_kart_tila.kartoituksen_tila], " + // document fact
					"	h.havainnon_laatu as recordBasis, " + // see record basis
					"	h.havainnon_laatu as [FACT_lg_lajihav.havainnon_laatu], " + // unit fact
					"	h.el_laji_id as herttaId, " +
					"	laji.laji_uri as taxonId, " + // unit.taxonId
					"	h.havaitsijan_antama_nimi as taxonVerbatim, " + // unit.taxonVerbatim
					"	lajinimi.nimi as scientificName, " + // unit.taxonVerbatim (if taxonVerbatim missing)
					"	lajinimi.auktori as scientificNameAuthor, " + // unit.author (if taxonVerbatim missing)
					"	h.maara as amount, " + // see Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as amountUnit, " + // Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as [FACT_lg_lajihav.maaran_yksikko], " + // Amount / individual counts / lifeStage / etc
					"	h.havaitsija as team, " + // gathering.team
					"   h.kayttooikeus as personHiding, " +
					"   h.num_lahde as [FACT_lg_lajihav.num_lahde], " + // document fact
					"	CONVERT(VARCHAR(25), h.pvm_alku, 127) as dateBegin, " + // gathering.eventDate.begin
					"	CONVERT(VARCHAR(25), h.pvm_loppu, 127) as dateEnd, " + // gathering.eventDate.end
					"	kunta.selite as municipality, " + // gathering.municipality (verbatim)
					"	em.selite as biogeographicalProvince, " + // gathering.biogeographicalProvince (verbatim)
					"	va.selite as basinName, " + // locality ( + fact Vesistöalue)
					"	h.kp_x as lonEurefCenter, " +
					"	h.kp_y as latEurefCenter, " +
					"	h.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	h.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	h.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (h.osuus_mhsa > 99 OR h.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	h.tarkistustarve as qualityControl, " + // see Quality control
					"	h.tarkistustarve as [FACT_lg_lajihav.tarkistustarve], " + // see Quality control
					"	n.nayte_keraaja as leg, " + // gathering.team (add together with havaitsija; unique values only stored)
					"	n.nayte_maarittaja as det, " + // unit.det
					"	n.nayte_sijainti as [FACT_lg_nayte.nayte_sijainti], " + // unit fact
					"	n.nayte_tyyppi as [FACT_lg_nayte.nayte_tyyppi], " + // unit fact
					"	n.nayte_uri as specimenUri, " + // unit.keyword
					"	n.nayte_museo_naytenro as specimenAdditionalId, " + // -- unit.keyword
					"	h.havainnon_kuvaus as unitNotes, " + // unit.notes
					"	h.huomautukset as unitNotes2, " + // unit.notes
					"	h.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus], " + // document fact
					"	ke.huomautukset as gatheringNotes " + // gathering.notes
					"	FROM      LG_LAJIHAV h " +
					"	JOIN      LG_LAJIHAV_TO_KERTA h_k ON h.lg_lajihavid = h_k.lg_lajihavid " +
					"	JOIN      LG_KERTA ke             ON ke.lg_kertaid  = h_k.lg_kertaid " +
					"	JOIN      LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					"	JOIN      LG_LAJI kohde_laji      ON kohde.el_laji_id = kohde_laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI kohde_lajinimi ON kohde.el_laji_id = kohde_lajinimi.el_laji_id AND kohde_lajinimi.kieli = 1 AND kohde_lajinimi.asema = 1" +
					"	LEFT JOIN LG_KART_TILA t          ON t.lg_kertaid   = ke.lg_kertaid AND t.tilan_voimassa_olo = 1 " +
					"	LEFT JOIN LG_KOHDE_SEUR kohde_s   ON kohde.lg_kohdeid = kohde_s.lg_kohdeid " +
					"	LEFT JOIN LG_KERTA_MAA km         ON km.lg_kertaid  = ke.lg_kertaid " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON h.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON h.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON h.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	LEFT JOIN LG_LAJI laji            ON h.el_laji_id   = laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1" +
					"	LEFT JOIN LG_NAYTE n              ON n.objectid = (SELECT TOP 1 objectid FROM LG_NAYTE WHERE h.lg_lajihavid = lg_lajihavid ORDER BY objectid) " +
					"	WHERE     h.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IN (31, 32) " +
					"	AND       kohde.seurattava = 1 " +
					"	ORDER BY  h_k.lg_kertaid, h.lg_lajihavid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private ResultSetStream<Occurrence> getMonitoringSiteZeroOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT  " +
					"	ke.lg_kertaid as eventId, " +
					"	null as occurrenceId, " +
					"	kohde.lg_kohdeid as namedPlaceId, " +
					"	kohde.nimi as locality, " +
					"	CASE WHEN kohde.el_laji_id IN (142, 763, 677) THEN -1 ELSE kohde.kohde_tyyppi END as namedPlaceType, " +
					"	kohde.kohde_tyyppi as [FACT_lg_kohde.kohde_tyyppi], " +
					"	kohde.menetelma as [FACT_lg_kohde.menetelma], " +
					"	kohde.huomautukset as documentNotes, " +
					"	kohde.kohteen_kuvaus as documentNotes2, " +
					"	kohde.kohde_taso as [FACT_lg_kohde.kohde_taso], " +
					"	kohde_s.el_paikan_tila as [FACT_lg_kohde_seur.el_paikan_tila], " +
					"	kohde_laji.laji_uri as namedPlaceTaxonId, " +
					"	kohde_lajinimi.nimi as namedPlaceTaxonScientificName, " +
					"   kohde.elioryhma as [FACT_lg_kohde.elioryhma], " +
					"	ke.ei_havaintoja as [FACT_lg_kerta.ei_havaintoja], " +
					"   km.pesintatulos as [FACT_lg_kerta_maa.pesintatulos], " +
					"   km.pesintatulos as nestingStatus, " +
					"	t.kartoituksen_tila as [FACT_lg_kart_tila.kartoituksen_tila], " +
					"	ke.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus], " +
					"	kohde.el_laji_id as herttaId, " +
					"	kohde_laji.laji_uri as taxonId,  " +
					"	lajinimi.nimi as scientificName, " +
					"	lajinimi.auktori as scientificNameAuthor, " +
					"	0 as amount, " +
					"	ke.kartoittaja as team, " +
					"	ke.kayttooikeus as personHiding, " +
					"   ke.num_lahde as [FACT_lg_lajihav.num_lahde], " +
					"	CONVERT(VARCHAR(25), ke.pvm_alku, 127) as dateBegin, " +
					"	CONVERT(VARCHAR(25), ke.pvm_loppu, 127) as dateEnd, " +
					"	kunta.selite as municipality, " +
					"	em.selite as biogeographicalProvince, " +
					"	va.selite as basinName, " +
					"	kohde.kp_x as lonEurefCenter, " +
					"	kohde.kp_y as latEurefCenter, " +
					"	kohde.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	kohde.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	kohde.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	kohde.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	kohde.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (kohde.osuus_mhsa > 99 OR kohde.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	ke.huomautukset as gatheringNotes " +
					"	FROM      LG_KERTA ke " +
					"	JOIN      LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid  " +
					"	JOIN      LG_LAJI kohde_laji      ON kohde.el_laji_id = kohde_laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI kohde_lajinimi ON kohde.el_laji_id = kohde_lajinimi.el_laji_id AND kohde_lajinimi.kieli = 1 AND kohde_lajinimi.asema = 1" +
					"	LEFT JOIN LG_KART_TILA t          ON t.lg_kertaid   = ke.lg_kertaid AND t.tilan_voimassa_olo = 1 " +
					"	LEFT JOIN LG_KOHDE_SEUR kohde_s   ON kohde.lg_kohdeid = kohde_s.lg_kohdeid " +
					"	LEFT JOIN LG_KERTA_MAA km         ON km.lg_kertaid  = ke.lg_kertaid " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON kohde_laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1 " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON kohde.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON kohde.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON kohde.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	WHERE     ke.ei_havaintoja = 1 " +
					"	AND       ke.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IN (31, 32) " +
					"	AND       kohde.seurattava = 1 " +
					"	ORDER BY  ke.lg_kertaid";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private ResultSetStream<Occurrence> getSpeciesSurveyOccurrences() throws Exception {
			ResultSetStream.ToInstance<Occurrence> instantor = new OccurrenceInstanter();
			String sql = "" +
					"	SELECT " +
					"	h_k.lg_kertaid as eventId, " + // "LGK." document.documentID; documentId + #G -> gathering.gatheringId
					"	h.lg_lajihavid as occurrenceId, " + // unit.keyword; gatheringId + h.lg_lajihavid -> unit.unitID (same lajihav can be in multi kerta)
					"	kohde.lg_kohdeid as namedPlaceId, " + // document.namedPlaceID
					"	kohde.nimi as locality, " + // gathering.locality
					"	kohde.kohde_tyyppi as namedPlaceType, " + // document.collectionId
					"	kohde.kohde_tyyppi as [FACT_lg_kohde.kohde_tyyppi], " + // document fact
					"	kohde.menetelma as [FACT_lg_kohde.menetelma], " + // document fact
					"	kohde.huomautukset as documentNotes, " + // document.notes
					"	kohde.kohteen_kuvaus as documentNotes2, " + // document.notes
					"	kohde.kohde_taso as [FACT_lg_kohde.kohde_taso], " + // document fact
					"   kohde.elioryhma as [FACT_lg_kohde.elioryhma], " + // unit fact
					"	ke.ei_havaintoja as [FACT_lg_kerta.ei_havaintoja], " + // document fact
					"	t.kartoituksen_tila as [FACT_lg_kart_tila.kartoituksen_tila], " + // document fact
					"	h.havainnon_laatu as recordBasis, " + // see record basis
					"	h.havainnon_laatu as [FACT_lg_lajihav.havainnon_laatu], " + // unit fact
					"	h.el_laji_id as herttaId, " +
					"	laji.laji_uri as taxonId, " + // unit.taxonId
					"	h.havaitsijan_antama_nimi as taxonVerbatim, " + // unit.taxonVerbatim
					"	lajinimi.nimi as scientificName, " + // unit.taxonVerbatim (if taxonVerbatim missing)
					"	lajinimi.auktori as scientificNameAuthor, " + // unit.author (if taxonVerbatim missing)
					"	h.maara as amount, " + // see Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as amountUnit, " + // Amount / individual counts / lifeStage / etc
					"	h.maaran_yksikko as [FACT_lg_lajihav.maaran_yksikko], " + // Amount / individual counts / lifeStage / etc
					"	h.havaitsija as team, " + // gathering.team
					"   h.kayttooikeus as personHiding, " +
					"   h.num_lahde as [FACT_lg_lajihav.num_lahde], " + // document fact
					"	CONVERT(VARCHAR(25), h.pvm_alku, 127) as dateBegin, " + // gathering.eventDate.begin
					"	CONVERT(VARCHAR(25), h.pvm_loppu, 127) as dateEnd, " + // gathering.eventDate.end
					"	kunta.selite as municipality, " + // gathering.municipality (verbatim)
					"	em.selite as biogeographicalProvince, " + // gathering.biogeographicalProvince (verbatim)
					"	va.selite as basinName, " + // locality ( + fact Vesistöalue)
					"	h.kp_x as lonEurefCenter, " +
					"	h.kp_y as latEurefCenter, " +
					"	h.orig_koord_id as coordSystem, " + // see Coordinates / Coordinate accuracy
					"	h.orig_x_koord as lon , " + // see Coordinates / Coordinate accuracy
					"	h.orig_y_koord as lat, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as coordinateAccuracy, " + // see Coordinates / Coordinate accuracy
					"	h.koord_tarkk as [FACT_lg_lajihav.koord_tarkk], " + // see Coordinates / Coordinate accuracy
					"	CASE WHEN (h.osuus_mhsa > 99 OR h.osuus_mh_muu > 99) THEN 1 ELSE 0 END as stateLand, " +
					"	h.tarkistustarve as qualityControl, " + // see Quality control
					"	h.tarkistustarve as [FACT_lg_lajihav.tarkistustarve], " + // see Quality control
					"	n.nayte_keraaja as leg, " + // gathering.team (add together with havaitsija; unique values only stored)
					"	n.nayte_maarittaja as det, " + // unit.det
					"	n.nayte_sijainti as [FACT_lg_nayte.nayte_sijainti], " + // unit fact
					"	n.nayte_tyyppi as [FACT_lg_nayte.nayte_tyyppi], " + // unit fact
					"	n.nayte_uri as specimenUri, " + // unit.keyword
					"	n.nayte_museo_naytenro as specimenAdditionalId, " + // -- unit.keyword
					"	h.havainnon_kuvaus as unitNotes, " + // unit.notes
					"	h.huomautukset as unitNotes2, " + // unit.notes
					"	h.hav_tlahde_kuvaus as [FACT_lg_lajihav.hav_tlahde_kuvaus], " + // document fact
					"	ke.huomautukset as gatheringNotes " + // gathering.notes
					"	FROM      LG_LAJIHAV h " +
					"	JOIN      LG_LAJIHAV_TO_KERTA h_k ON h.lg_lajihavid = h_k.lg_lajihavid " +
					"	JOIN      LG_KERTA ke             ON ke.lg_kertaid  = h_k.lg_kertaid " +
					"	JOIN      LG_KOHDE kohde          ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					"	LEFT JOIN LG_KART_TILA t          ON t.lg_kertaid   = ke.lg_kertaid AND t.tilan_voimassa_olo = 1 " +
					"	LEFT JOIN MHS_KOODI_ALA kunta     ON h.kunta_nro = kunta.koodi AND kunta.koodi_paa_id = 4125 " +
					"	LEFT JOIN MHS_KOODI_ALA em        ON h.eliomaakunta = em.koodi AND em.koodi_paa_id = 1013 " +
					"	LEFT JOIN MHS_KOODI_ALA va        ON h.vesistoalue = va.koodi AND va.koodi_paa_id = 3055 " +
					"	LEFT JOIN LG_LAJI laji            ON h.el_laji_id   = laji.el_laji_id " +
					"	LEFT JOIN LG_LAJINIMI lajinimi    ON laji.el_laji_id = lajinimi.el_laji_id AND lajinimi.kieli = 1 AND lajinimi.asema = 1" +
					"	LEFT JOIN LG_NAYTE n              ON n.objectid = (SELECT TOP 1 objectid FROM LG_NAYTE WHERE h.lg_lajihavid = lg_lajihavid ORDER BY objectid) " +
					"	WHERE     h.olotila = 1 " +
					"	AND       kohde.kohde_tyyppi IN (10, 11, 12, 13, 46, 50, 51, 52, 61, 62, 64) " +
					"	ORDER BY  h_k.lg_kertaid, h.lg_lajihavid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private class GeoInstanter implements ResultSetStream.ToInstance<OccurrenceGeo> {

			@Override
			public OccurrenceGeo get(ResultSet rs) throws SQLException {
				OccurrenceGeo geo = new OccurrenceGeo();
				String type = rs.getString(1);
				geo.occurrenceId = rs.getInt(2);
				geo.wkt = rs.getString(3);
				if ("polygon".equals(type)) {
					if (geo.wkt.length() > 2000) {
						try {
							Geo g = Geo.fromWKT(geo.wkt, Type.EUREF);
							g.convertComplex();
							geo.wkt = g.getWKT();
						} catch (Exception e) {
							// keep original wkt
						}
					}
				}
				return geo;
			}

		}

		private class EventGeoInstanter implements ResultSetStream.ToInstance<EventGeo> {

			@Override
			public EventGeo get(ResultSet rs) throws SQLException {
				EventGeo geo = new EventGeo();
				String type = rs.getString(1);
				geo.eventId = rs.getInt(2);
				geo.wkt = rs.getString(3);
				if ("polygon".equals(type)) {
					if (geo.wkt.length() > 2000) {
						try {
							Geo g = Geo.fromWKT(geo.wkt, Type.EUREF).convertComplex();
							geo.wkt = g.getWKT();
						} catch (Exception e) {
							// keep original wkt
						}
					}
				}
				return geo;
			}

		}

		@Override
		public ResultSetStream<OccurrenceGeo> getGeos() throws Exception {
			ResultSetStream.ToInstance<OccurrenceGeo> instantor = new GeoInstanter();
			String sql = "" +
					" SELECT 'line', lg_lajihavId, shape.STAsText() AS WKT FROM lg_lajihav_v " +
					" UNION " +
					" SELECT 'polygon', lg_lajihavId, shape.STAsText() AS WKT FROM lg_lajihav_a " +
					" UNION " +
					" SELECT 'point', lg_lajihavId, shape.STAsText() AS WKT FROM lg_lajihav_p " +
					"   WHERE lg_lajihavId IN (SELECT lg_lajihavid FROM lg_lajihav_p GROUP BY LG_LAJIHAVID having count(1) > 1) ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		@Override
		public ResultSetStream<EventGeo> getEventGeos() throws Exception {
			ResultSetStream.ToInstance<EventGeo> instantor = new EventGeoInstanter();
			String sql = "" +
					" SELECT 'line', lg_kertaId, shape.STAsText() AS WKT " +
					" FROM      LG_KERTA ke " +
					" JOIN      LG_KOHDE kohde ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					" JOIN      LG_KOHDE_V     ON kohde.LG_KOHDEID = LG_KOHDE_V.LG_KOHDEID " +
					" WHERE     ke.ei_havaintoja = 1 AND ke.olotila = 1 AND kohde.kohde_tyyppi IN (31, 32) AND kohde.seurattava = 1 " +
					" UNION " +
					" SELECT 'polygon', lg_kertaId, shape.STAsText() AS WKT " +
					" FROM      LG_KERTA ke " +
					" JOIN      LG_KOHDE kohde ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					" JOIN      LG_KOHDE_A     ON kohde.LG_KOHDEID = LG_KOHDE_A.LG_KOHDEID " +
					" WHERE     ke.ei_havaintoja = 1 AND ke.olotila = 1 AND kohde.kohde_tyyppi IN (31, 32) AND kohde.seurattava = 1 " +
					" UNION " +
					" SELECT 'point', lg_kertaId, shape.STAsText() AS WKT " +
					" FROM      LG_KERTA ke " +
					" JOIN      LG_KOHDE kohde ON ke.lg_kohdeid  = kohde.lg_kohdeid " +
					" JOIN      LG_KOHDE_P     ON kohde.LG_KOHDEID = LG_KOHDE_P.LG_KOHDEID " +
					" WHERE     ke.ei_havaintoja = 1 AND ke.olotila = 1 AND kohde.kohde_tyyppi IN (31, 32) AND kohde.seurattava = 1 " +
					" AND       kohde.lg_kohdeId IN (SELECT lg_kohdeId FROM lg_kohde_p GROUP BY LG_kohdeId having count(1) > 1) ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		private class ProjectInstanter implements ResultSetStream.ToInstance<EventProject> {

			@Override
			public EventProject get(ResultSet rs) throws SQLException {
				EventProject p = new EventProject();
				p.eventId = rs.getInt(1);
				p.projectName = rs.getString(2);
				String projectId = rs.getString(3);
				if (projectId != null) {
					if (p.projectName == null) {
						p.projectName = projectId;
					} else {
						p.projectName += " (" + projectId + ")";
					}
				}
				return p;
			}

		}

		@Override
		public ResultSetStream<EventProject> getProjects() throws Exception {
			ResultSetStream.ToInstance<EventProject> instantor = new ProjectInstanter();
			String sql = "" +
					"	SELECT k_h.lg_kertaid, hanke.hanke_nimi, hanke.hankkeen_tunnus " +
					"   FROM   LG_KERTA_TO_HANKE k_h " +
					"   JOIN   LG_HANKE hanke on k_h.lg_hankeid = hanke.lg_hankeid ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, instantor);
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		@Override
		public Set<Integer> getBuggedEventIds() throws Exception {
			String sql = "" +
					" SELECT    h_k.LG_KERTAID " +
					" FROM      LG_LAJIHAV_TO_KERTA h_k " +
					" LEFT JOIN LG_KERTA ke ON ke.lg_kertaid = h_k.lg_kertaid " +
					" WHERE     ke.LG_KERTAID IS NULL " +
					" AND       h_k.LG_KERTAID IS NOT NULL ";
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				Set<Integer> ids = new HashSet<>();
				con = openLajiGISConnection();
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				while (rs.next()) {
					ids.add(rs.getInt(1));
				}
				return ids;
			} finally {
				Utils.close(p, rs, con);
			}
		}

	}

	private interface DwTempTablesDAO {
		int insertOccurrences(Iterable<Occurrence> occurrences) throws Exception;
		int insertGeos(Iterable<OccurrenceGeo> geos) throws Exception;
		int insertEventGeos(Iterable<EventGeo> geos) throws Exception;
		int insertProjects(Iterable<EventProject> projects) throws Exception;
		ScrollableResultsStream<Occurrence> getOccurrences() throws Exception;
		Map<Integer, List<String>> getGeos(List<Integer> occurrenceIds) throws Exception;
		Map<Integer, List<String>> getEventGeos(Set<Integer> eventIds) throws Exception;
		Map<Integer, List<String>> getProjects(Set<Integer> eventId) throws Exception;
	}

	public static class DwTempTablesDAOImple implements DwTempTablesDAO {

		private final ETLDAO dao;
		private final ThreadStatusReporter statusReporter;

		public DwTempTablesDAOImple(ETLDAO dao, ThreadStatusReporter reporter) {
			this.dao = dao;
			this.statusReporter = reporter;
		}

		private void reportStatus(String status) {
			statusReporter.setStatus(status);
		}

		@Override
		public int insertOccurrences(Iterable<Occurrence> occurrences) throws SQLException {
			StatelessSession session = null;
			try {
				session = dao.getETLEntityConnection();
				session.beginTransaction();
				int i = 0;
				for (Occurrence o : occurrences) {
					session.insert(o);
					i++;
					if (i % 100 == 0) reportStatus("Occurrences: " + i);
				}
				session.getTransaction().commit();
				return i;
			} catch (Exception e) {
				if (session != null) session.getTransaction().rollback();
				throw e;
			} finally {
				close(session);
			}
		}

		@Override
		public int insertGeos(Iterable<OccurrenceGeo> geos) {
			StatelessSession session = null;
			try {
				session = dao.getETLEntityConnection();
				session.beginTransaction();
				int i = 0;
				for (OccurrenceGeo geo : geos) {
					session.insert(geo);
					i++;
					if (i % 100 == 0) reportStatus("Geos: " + i);
				}
				session.getTransaction().commit();
				return i;
			} catch (Exception e) {
				if (session != null) session.getTransaction().rollback();
				throw e;
			} finally {
				close(session);
			}
		}

		@Override
		public int insertEventGeos(Iterable<EventGeo> geos) {
			StatelessSession session = null;
			try {
				session = dao.getETLEntityConnection();
				session.beginTransaction();
				int i = 0;
				for (EventGeo geo : geos) {
					session.insert(geo);
					i++;
					if (i % 100 == 0) reportStatus("Event geos: " + i);
				}
				session.getTransaction().commit();
				return i;
			} catch (Exception e) {
				if (session != null) session.getTransaction().rollback();
				throw e;
			} finally {
				close(session);
			}
		}

		@Override
		public int insertProjects(Iterable<EventProject> projects) {
			StatelessSession session = null;
			try {
				session = dao.getETLEntityConnection();
				session.beginTransaction();
				int i = 0;
				for (EventProject p : projects) {
					session.insert(p);
					i++;
					if (i % 100 == 0) reportStatus("Projects: " + i);
				}
				session.getTransaction().commit();
				return i;
			} catch (Exception e) {
				if (session != null) session.getTransaction().rollback();
				throw e;
			} finally {
				close(session);
			}
		}

		@Override
		public ScrollableResultsStream<Occurrence> getOccurrences() {
			StatelessSession session = null;
			try {
				session = dao.getETLEntityConnection();
				ScrollableResults results = session.createCriteria(Occurrence.class)
						.addOrder(Order.asc("eventId"))
						.addOrder(Order.asc("occurrenceId"))
						.setFetchSize(4001).setReadOnly(true).scroll(ScrollMode.FORWARD_ONLY);
				return new ScrollableResultsStream<>(results, session, Occurrence.class);
			} catch (Exception e) {
				close(session);
				throw e;
			}
		}

		private void close(StatelessSession session) {
			if (session != null)
				try {
					session.close();
				} catch (Exception e) {}
		}

		@SuppressWarnings("unchecked")
		@Override
		public Map<Integer, List<String>> getProjects(Set<Integer> eventIds) {
			if (eventIds.isEmpty()) return Collections.emptyMap();
			try (StatelessSession session = dao.getETLEntityConnection()) {
				List<EventProject> results = session.createCriteria(EventProject.class).add(Restrictions.in("eventId", eventIds)).list();
				Map<Integer, List<String>> projectNames = new HashMap<>();
				for (EventProject e : results) {
					if (!projectNames.containsKey(e.eventId)) {
						projectNames.put(e.eventId, new ArrayList<>());
					}
					projectNames.get(e.eventId).add(e.projectName);
				}
				for (List<String> projectName : projectNames.values()) {
					Collections.sort(projectName);
				}
				return projectNames;
			}
		}

		@Override
		public Map<Integer, List<String>> getGeos(List<Integer> occurrenceIds) {
			if (occurrenceIds.isEmpty()) return Collections.emptyMap();
			try (StatelessSession session = dao.getETLEntityConnection()) {
				Map<Integer, List<String>> wkts = new HashMap<>();
				for (List<Integer> batch : Lists.partition(occurrenceIds, 995)) {
					@SuppressWarnings("unchecked")
					List<OccurrenceGeo> results = session.createCriteria(OccurrenceGeo.class).add(Restrictions.in("occurrenceId", batch)).list();
					for (OccurrenceGeo geo : results) {
						if (!wkts.containsKey(geo.occurrenceId)) {
							wkts.put(geo.occurrenceId, new ArrayList<>());
						}
						wkts.get(geo.occurrenceId).add(geo.wkt);
					}
				}
				for (List<String> wkt : wkts.values()) {
					Collections.sort(wkt);
				}
				return wkts;
			}
		}

		@Override
		public Map<Integer, List<String>> getEventGeos(Set<Integer> eventIds) {
			if (eventIds.isEmpty()) return Collections.emptyMap();
			List<Integer> ids = new ArrayList<>(eventIds);
			try (StatelessSession session = dao.getETLEntityConnection()) {
				Map<Integer, List<String>> wkts = new HashMap<>();
				for (List<Integer> batch : Lists.partition(ids, 995)) {
					@SuppressWarnings("unchecked")
					List<EventGeo> results = session.createCriteria(EventGeo.class).add(Restrictions.in("eventId", batch)).list();
					for (EventGeo geo : results) {
						if (!wkts.containsKey(geo.eventId)) {
							wkts.put(geo.eventId, new ArrayList<>());
						}
						wkts.get(geo.eventId).add(geo.wkt);
					}
				}
				for (List<String> wkt : wkts.values()) {
					Collections.sort(wkt);
				}
				return wkts;
			}
		}
	}

	@Entity
	@Table(name="lajigis_etl_occurrences")
	public static class Occurrence {
		private Integer eventId;
		private int occurrenceId;
		private String json;
		@Column
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		@Id
		@Column
		public int getOccurrenceId() {
			return occurrenceId;
		}
		public void setOccurrenceId(int occurrenceId) {
			this.occurrenceId = occurrenceId;
		}
		@Column
		public String getJson() {
			return json;
		}
		public void setJson(String json) {
			this.json = json;
		}
	}

	@Entity
	@Table(name="lajigis_etl_geo")
	public static class OccurrenceGeo {
		private int occurrenceId;
		private String wkt;
		@Id
		@Column
		public int getOccurrenceId() {
			return occurrenceId;
		}
		public void setOccurrenceId(int occurrenceId) {
			this.occurrenceId = occurrenceId;
		}
		@Column
		public String getWkt() {
			return wkt;
		}
		public void setWkt(String wkt) {
			this.wkt = wkt;
		}
	}

	@Entity
	@Table(name="lajigis_etl_event_geo")
	public static class EventGeo {
		private int eventId;
		private String wkt;
		@Id
		@Column
		public int getEventId() {
			return eventId;
		}
		public void setEventId(int eventId) {
			this.eventId = eventId;
		}
		@Column
		public String getWkt() {
			return wkt;
		}
		public void setWkt(String wkt) {
			this.wkt = wkt;
		}
	}

	@Entity
	@Table(name="lajigis_etl_projects")
	public static class EventProject  {
		private int eventId;
		private String projectName;
		@Id
		@Column
		public int getEventId() {
			return eventId;
		}
		public void setEventId(int eventId) {
			this.eventId = eventId;
		}
		@Column
		public String getProjectName() {
			return projectName;
		}
		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}

	}

	public static String clean(String value) {
		if (value == null) return null;
		if (shouldCleanDouble(value)) {
			return cleanDouble(value);
		}
		return value;
	}

	private static String cleanDouble(String value) {
		value = StringUtils.stripEnd(value, "0");
		if (value.endsWith(".")) return value + "0";
		return value;
	}

	private static boolean shouldCleanDouble(String value) {
		if (!value.contains(".")) return false;
		if (!value.endsWith("0")) return false;
		try {
			Double.valueOf(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
