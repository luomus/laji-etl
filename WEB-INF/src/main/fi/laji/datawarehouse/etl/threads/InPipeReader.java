package fi.laji.datawarehouse.etl.threads;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OriginalIds;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.models.harmonizers.Harmonizer;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;
import jersey.repackaged.com.google.common.collect.Lists;

public class InPipeReader extends ETLThread {

	private final Map<String, Harmonizer<String>> harmonizers;
	private final ETLDAO etl;
	private final Qname source;

	public InPipeReader(Qname forSource, ThreadHandler threadHandler, ThreadStatusReporter statusReporter, DAO dao, Map<String, Harmonizer<String>> harmonizers) {
		super(forSource, threadHandler, statusReporter, dao);
		this.harmonizers = harmonizers;
		this.etl = dao.getETLDAO();
		this.source = forSource;
	}

	private Harmonizer<String> getHarmonizer(String contentType) {
		return harmonizers.get(contentType);
	}

	private static class BatchResults {
		private final List<InPipeData> successful = new ArrayList<>();
		private final List<SaveFailure> failed = new ArrayList<>();
		private final List<SaveFailure> permantentlyFailed = new ArrayList<>();
		private final List<OutPipeData> outPipeData = new ArrayList<>();
		public void successful(InPipeData data) {
			successful.add(data);
		}
		public void failure(InPipeData data, String errorMessage) {
			failed.add(new SaveFailure(data, errorMessage));
		}
		public void permanentFailure(InPipeData data, String errorMessage) {
			permantentlyFailed.add(new SaveFailure(data, errorMessage));
		}
		public List<InPipeData> getSuccessful() {
			return successful;
		}
		public List<SaveFailure> getFailed() {
			return failed;
		}
		public List<SaveFailure> getPermanentlyFailed() {
			return permantentlyFailed;
		}
		public boolean hasFailed() {
			return !getFailed().isEmpty();
		}
		public List<OutPipeData> getOutPipeData() {
			return outPipeData;
		}
	}

	private static class SaveFailure {
		private final InPipeData data;
		private final String errorMessage;
		public SaveFailure(InPipeData data, String errorMessage) {
			this.data = data;
			this.errorMessage = errorMessage;
		}
		public InPipeData getData() {
			return data;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
	}

	@Override
	protected long read() {
		List<InPipeData> batch = null;
		boolean reprocessingErroneous = false;
		try {
			reportStatus("Getting unprocessed from in-pipe");
			batch = etl.getUnprocessedNotErroneousInOrderFromInPipe(source);

			if (batch.isEmpty()) {
				batch = etl.getFailedAttemptDataFromInPipe(source);
				if (batch.isEmpty()) return NO_WORK_PAUSE;
				reprocessingErroneous = true;
			}
		} catch (Throwable t) {
			reportStatus("Handling unknown pipe read exceptions");
			logError(t);
			return ERROR_PAUSE;
		}

		try {
			validateSourceIsDwSource();
			BatchResults batchResults = processBatch(batch);
			markSuccessfullyProcessed(batchResults);
			markFailed(batchResults);
			markPermanentlyFailed(batchResults);
			if (reprocessingErroneous && batchResults.hasFailed()) return ERROR_PAUSE;
			return SMALL_PAUSE;
		} catch (Throwable t) {
			reportStatus("Handling unknown exceptions");
			try {
				etl.reportAttempted(batch, LogUtils.buildStackTrace(t));
			} catch (Throwable t2) {
				logError(t2);
			}
			return ERROR_PAUSE;
		}
	}

	private void markSuccessfullyProcessed(BatchResults results) {
		reportStatus("Reporting in pipe data successes");
		etl.reportProcessed(results.getSuccessful());
	}

	private void markFailed(BatchResults results) {
		for (SaveFailure failure : results.getFailed()) {
			reportStatus("Reporting in pipe data failures");
			etl.reportAttempted(Utils.singleEntryList(failure.getData()), failure.getErrorMessage());
		}
	}

	private void markPermanentlyFailed(BatchResults results) {
		for (SaveFailure failure : results.getPermanentlyFailed()) {
			reportStatus("Reporting in pipe data permanent failures");
			etl.reportPermantentyFailed(Utils.singleEntryList(failure.getData()), failure.getErrorMessage());
		}
	}

	private BatchResults processBatch(List<InPipeData> batch) {
		BatchResults results = new BatchResults();
		for (InPipeData data : batch) {
			processBatch(results, data);
		}
		save(results.getOutPipeData());
		return results;
	}

	private void processBatch(BatchResults results, InPipeData data) {
		try {
			List<DwRoot> harmonized;
			try {
				harmonized = harmonize(data);
			} catch (CriticalParseFailure | UnknownHarmonizingFailure e) {
				reportStatus("Handling permanent parsing exceptions");
				String errorMessage = "Harmonization failed: \n" + LogUtils.buildStackTrace(e);
				results.permanentFailure(data, errorMessage);
				return;
			}
			Exception validationException = null;
			List<DwRoot> passesValidation = new ArrayList<>();
			for (DwRoot root : harmonized) {
				try {
					reportStatus("Validating " + data.getId() + " / " + documentId(root));
					validate(root);
					passesValidation.add(root);
				} catch (CriticalParseFailure | UnknownHarmonizingFailure | IllegalAccessException e) {
					if (validationException == null) validationException = e;
				}
			}
			setOutPipeDatas(results, data, passesValidation);
			storeAnnotationsNotifyReprocess(data, passesValidation);
			if (validationException != null) {
				reportStatus("Handling permanent parsing exceptions");
				String errorMessage = "Stored " + passesValidation.size() + "/" + harmonized.size() + " documents to outpipe: \n" + LogUtils.buildStackTrace(validationException);
				results.permanentFailure(data, errorMessage);
			} else {
				results.successful(data);
			}
		} catch (Throwable t) {
			reportStatus("Handling unknown parsing exceptions");
			String errorMessage = LogUtils.buildStackTrace(t);
			logError(t, "In pipe data " + data.getId());
			results.failure(data, errorMessage);
		}
	}

	private String documentId(DwRoot root) {
		if (root == null) return null;
		if (root.getDocumentId() == null) return null;
		try {
			return root.getDocumentId().toString();
		} catch (Exception e) {
			return null;
		}
	}

	private void save(List<OutPipeData> outPipeData) {
		if (outPipeData.isEmpty()) return;
		reportStatus("Saving out-pipe datas (" + outPipeData.size() + ")");
		if (outPipeData.size() > 100) {
			saveInBatches(outPipeData);
		} else {
			storeToOutPipeReportJob(outPipeData);
		}
	}

	private void saveInBatches(List<OutPipeData> outPipeData) {
		List<List<OutPipeData>> batches = Lists.partition(outPipeData, 100);
		int i = 1;
		for (List<OutPipeData> batch : batches) {
			reportStatus("Saving out-pipe data batch " + (i++) + "/" + batches.size() + " (" + outPipeData.size() + ")");
			storeToOutPipeReportJob(batch);
			sleep();
		}
	}

	private void sleep() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {}
	}

	private void storeToOutPipeReportJob(List<OutPipeData> data) {
		boolean causedChanges = etl.storeToOutPipe(data);
		if (causedChanges) {
			getThreadHandler().runOutPipe(source);
		}
	}

	private void storeAnnotationsNotifyReprocess(InPipeData data, List<DwRoot> harmonized) {
		reportStatus("Storing annotations");
		List<Annotation> annotations = storeAnnotations(harmonized, data.getId());
		if (!annotations.isEmpty()) {
			reportStatus("Marking annotated documents for reprocessing");
			markForReprocess(annotations);
		}
	}

	private void setOutPipeDatas(BatchResults results, InPipeData data, List<DwRoot> harmonized) {
		long inPipeId = data.getId();
		for (DwRoot root : harmonized) {
			if (root.hasOnlyAnnotations()) {
				continue;
			}
			String json = root.toJSON().toString();
			results.getOutPipeData().add(createOutPipeData(data, inPipeId, root, json));
		}
	}

	private OutPipeData createOutPipeData(InPipeData data, long inPipeId, DwRoot root, String json) {
		return new OutPipeData(-1L, inPipeId, source, root.getDocumentId(), root.getCollectionId(), root.getNamedPlaceId(), json, root.isDeleteRequest()).setAttemptCount(data.getAttemptCount());
	}

	private void markForReprocess(List<Annotation> annotations) {
		Set<Qname> annotationSources = new HashSet<>();
		for (Annotation annotation : annotations) {
			OutPipeData existing = etl.getFromOutPipe(annotation.getRootID()); // TODO could be done in batches
			if (existing != null) {
				// We also mark the outpipe db entry for reprocessing in case the system goes down before processing Threadhandler reprocess requests
				etl.markReprocessOutPipe(existing.getId()); // TODO could be done in batches
				getThreadHandler().markDocumentForReprocess(existing);
				annotationSources.add(existing.getSource());
			} else {
				logError(new IllegalStateException("Annotated document not found from out-pipe: " + annotation.getRootID().toURI()), annotation.getRootID().toURI());
			}
		}
		for (Qname source : annotationSources) {
			getThreadHandler().runOutPipe(source);
		}
	}

	private void validate(DwRoot root) throws CriticalParseFailure, IllegalAccessException, Exception {
		validateSource(root.getSourceId());
		if (root.isDeleteRequest()) {
			validateDocumentId(root.getDocumentId());
		} else if (root.hasAnnotations()) {
			if (!Const.ANNOTATION_SOURCE.equals(source)) {
				throw new IllegalAccessException("Allow annotations only from " + Const.ANNOTATION_SOURCE + " for now");
			}
			if (!root.hasOnlyAnnotations()) {
				throw new CriticalParseFailure("Root has annotations and documents");
			}
			for (Annotation annotation : root.getAnnotations()) {
				validate(annotation);
			}
		} else {
			validateDocumentId(root.getDocumentId());
			validate(root.getPublicDocument());
			validate(root.getPrivateDocument());
			if (root.hasSplittedPublicDocuments()) {
				throw new CriticalParseFailure("Root has splitted documents. This feature is ment only for the warehouse internally.");
			}
		}
	}

	private void validate(Annotation annotation) throws CriticalParseFailure {
		Util.validateIncomingId(annotation.getId(), "annotationId");
		Util.validateIncomingId(annotation.getRootID(), "rootId");
		if (annotation.getTargetID() != null) {
			Util.validateIncomingId(annotation.getTargetID(), "targetId");
		}
	}

	private void validateSourceIsDwSource() throws IllegalAccessException {
		Source source = getDAO().getSources().get(this.source.toURI());
		if (source == null) throw new IllegalAccessException("Unkown source " + this.source);
		if (!source.isWarehouseSource()) {
			throw new IllegalAccessException("Source " + this.source + " is not allowed to insert data into warehouse!");
		}
	}

	private void validateSource(Qname rootSource) throws IllegalAccessException {
		if (!source.equals(rootSource)) {
			throw new IllegalAccessException("Given sourceId " + rootSource + " does not match authentication source: " + source);
		}
	}

	private void validate(Document document) throws CriticalParseFailure, IllegalAccessException {
		if (document == null) return;
		validateSource(document.getSourceId());
		document.validateIncomingIds();
	}

	private void validateDocumentId(Qname documentId) throws CriticalParseFailure {
		Util.validateIncomingDocumentId(documentId);
	}

	private List<DwRoot> harmonize(InPipeData data) throws CriticalParseFailure, UnknownHarmonizingFailure {
		reportStatus("Harmonizing " + data.getId());
		Harmonizer<String> harmonizer = getHarmonizer(data.getContentType());
		if (harmonizer == null) throw new CriticalParseFailure("Not yet implemented for " + data.getContentType());
		List<DwRoot> harmonized = harmonizer.harmonize(data.getData(), data.getSource());
		if (harmonized.isEmpty()) throw new CriticalParseFailure("No data");
		return harmonized;
	}

	private List<Annotation> storeAnnotations(List<DwRoot> harmonized, long inPipeId) {
		List<Annotation> annotations = new ArrayList<>();
		for (DwRoot root : harmonized) {
			if (root.hasAnnotations()) {
				annotations.addAll(root.getAnnotations());
			}
		}

		Iterator<Annotation> i = annotations.iterator();
		while (i.hasNext()) {
			Annotation annotation = i.next();
			if (looksLikeSplittedDocumentId(annotation.getRootID())) {
				setOriginalIdsToSplitted(annotation);
			}

			Long existingAnnotationInPipeId = etl.getExistingAnnotationInPipeId(annotation.getId()); // TODO in batches -- inpipeid could be included in index?
			if (shouldOverrideExisting(inPipeId, existingAnnotationInPipeId)) {
				etl.storeAnnotation(annotation, inPipeId); // TODO in batches
				if (annotation.isDeleted()) {
					etl.removeUnsentNotifications(annotation.getId()); // TODO in batches
				}
			} else {
				i.remove(); // no need to reprocess the target document
			}
		}
		return annotations;
	}

	private boolean shouldOverrideExisting(long inPipeId, Long existingAnnotationInPipeId) {
		return existingAnnotationInPipeId == null || existingAnnotationInPipeId <= inPipeId;
	}

	private void setOriginalIdsToSplitted(Annotation annotation) {
		OriginalIds ids = etl.getOriginalIdsOfSplittedDocument(annotation.getRootID());
		if (ids == null) {
			// not splitted
			return;
		}
		try {
			annotation.setRootID(ids.getDocumentId());
			annotation.setTargetID(ids.getUnitId());
		} catch (CriticalParseFailure e) {
			throw new ETLException("Impossible state", e);
		}
	}

	private boolean looksLikeSplittedDocumentId(Qname documentId) {
		return documentId.toString().startsWith(Const.SPLITTED_DOCUMENT_ID_QNAME_NAMESPACE_PREFIX);
	}

}
