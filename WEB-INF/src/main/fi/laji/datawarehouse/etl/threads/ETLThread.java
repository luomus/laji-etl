package fi.laji.datawarehouse.etl.threads;

import java.util.List;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.PipeData;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.containers.rdf.Qname;

/**
 * Provides mechanism for thread run/stop/wake up control as well as some error logging
 */
public abstract class ETLThread extends Thread implements AutoCloseable {

	protected static final long SMALL_PAUSE = 10;
	protected static final long ERROR_PAUSE = 15 * 60 * 1000;
	protected static final long NO_WORK_PAUSE = -1;

	protected final DAO dao;
	protected final Qname source;
	private final ThreadHandler threadHandler;
	private final ThreadStatusReporter statusReporter;

	private boolean stopped = false;
	private boolean doingWork = false;

	public ETLThread(Qname forSource, ThreadHandler threadHandler, ThreadStatusReporter statusReporter, DAO dao) {
		this.source = forSource;
		this.threadHandler = threadHandler;
		this.statusReporter = statusReporter;
		this.dao = dao;
	}

	/**
	 * Read and return delay for next call.
	 * Should not throw any exceptions.
	 * @return delay in milliseconds - NO_WORK_PAUSE to stop working; few fixed delays are defined: SMALL_PAUSE and ERROR_PAUSE
	 */
	protected abstract long read();

	@Override
	public void close() throws Exception {}

	@Override
	public void run() {
		try {
			while (!stopped) {
				try {
					long delayTime = doWork();
					if (delayTime == NO_WORK_PAUSE) {
						return;
					}
					reportStatus("Will sleep for " + delayTime);
					sleep(delayTime);
				} catch (InterruptedException e) {
					// Check if stopped, continue working
				}
			}
		} finally {
			try {
				close();
			} catch (Throwable t) {
				logError(t);
			}
			getThreadHandler().reportThreadDead(source, this.getClass());
		}
	}

	private long doWork() {
		doingWork = true;
		try {
			return read();
		} catch (Throwable t) {
			// Should not let exceptions get this far
			logError(t);
			return ERROR_PAUSE;
		} finally {
			doingWork = false;
		}
	}

	protected boolean shouldStop() {
		return stopped;
	}

	public void stopWorking() {
		stopped = true;
		if (!doingWork) {
			this.interrupt();
		}
	}

	public void reportJob() {
		if (!doingWork) {
			this.interrupt();
		}
	}

	protected DAO getDAO() {
		return dao;
	}

	protected void reportStatus(String status) {
		statusReporter.setStatus(status);
	}

	protected ThreadStatusReporter getStatusReporter() {
		return statusReporter;
	}

	protected ThreadHandler getThreadHandler() {
		return threadHandler;
	}

	protected Qname getSource() {
		return source;
	}

	protected void logError(Throwable t) {
		dao.logError(getSource(), this.getClass(), null, t);
	}

	protected void logError(Throwable t, String identifier) {
		dao.logError(getSource(), this.getClass(), identifier, t);
	}

	protected void logError(Throwable t, List<? extends PipeData> data) {
		dao.logError(getSource(), this.getClass(), debugIds(data), t);
	}

	protected void logMessage(String message) {
		dao.logMessage(getSource(), this.getClass(), message);
	}

	protected void logGeneralMessage(String message) {
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), message);
	}

	private String debugIds(List<? extends PipeData> data) {
		if (data == null) return null;
		if (data.isEmpty()) return null;
		try {
			return data.stream().map(d->String.valueOf(d.getId())).collect(Collectors.joining(","));
		} catch ( Exception e) {
			return null;
		}
	}

}
