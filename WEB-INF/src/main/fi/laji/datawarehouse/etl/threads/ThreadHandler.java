package fi.laji.datawarehouse.etl.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.harmonizers.Harmonizer;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.containers.rdf.Qname;

public class ThreadHandler {

	private final Map<Qname, Threads> sourceThreads = new HashMap<>();
	private final Set<Qname> stoppedThreads = new HashSet<>();
	private final Map<String, Harmonizer<String>> harmonizers;
	private final ThreadStatuses threadStatuses;
	private final DAO dao;

	public ThreadHandler(DAO dao, Map<String, Harmonizer<String>> harmonizers, ThreadStatuses threadStatuses) {
		this.dao = dao;
		this.harmonizers = harmonizers;
		this.threadStatuses = threadStatuses;
	}

	private class Threads {

		private final Qname source;
		private ETLThread inReader;
		private ETLThread outReader;
		private ETLThread inPipeCleanUpThread;
		private ETLThread pullReader;

		private Threads(Qname source) {
			this.source = source;
		}

		void stop() {
			if (inReader != null) inReader.stopWorking();
			if (outReader != null) outReader.stopWorking();
			if (inPipeCleanUpThread != null) inPipeCleanUpThread.stopWorking();
			if (pullReader != null) pullReader.stopWorking();
		}

		boolean somethingRunning() {
			if (inReader != null && inReader.isAlive()) return true;
			if (outReader != null && outReader.isAlive()) return true;
			if (pullReader != null && pullReader.isAlive()) return true;
			if (inPipeCleanUpThread != null && inPipeCleanUpThread.isAlive()) return true;
			return false;
		}

		void runInPipe() {
			if (inReader != null && inReader.isAlive()) {
				inReader.reportJob();
			} else {
				inReader = new InPipeReader(source, ThreadHandler.this, reporterFor(InPipeReader.class), dao, harmonizers);
				inReader.start();
			}
		}

		void runOutPipe() {
			if (outReader != null && outReader.isAlive()) {
				outReader.reportJob();
			} else {
				outReader = new OutPipeReader(source, ThreadHandler.this, reporterFor(OutPipeReader.class), dao);
				outReader.start();
			}
		}

		void runPullReader() {
			if (pullReader != null && pullReader.isAlive()) {
				// already running
			} else {
				if (PullReaders.definesPullReader(source)) {
					pullReader = PullReaders.getPullReader(source);
					pullReader.start();
				}
			}
		}

		public void runInPipeCleanupThread() {
			if (inPipeCleanUpThread != null && inPipeCleanUpThread.isAlive()) {
				// already running
			} else {
				inPipeCleanUpThread = new InPipeCleanUpThread(source, ThreadHandler.this, reporterFor(InPipeCleanUpThread.class), dao);
				inPipeCleanUpThread.start();
			}
		}

		private ThreadStatusReporter reporterFor(Class<?> threadClass) {
			return threadStatuses.getThreadStatusReporterFor(source, threadClass);
		}

	}

	public void runInPipe(Qname source) {
		validate(source);
		if (stoppedThreads.contains(source)) return;
		getThreads(source).runInPipe();
	}

	public void runOutPipe(Qname source) {
		validate(source);
		if (stoppedThreads.contains(source)) return;
		getThreads(source).runOutPipe();
	}

	public void runInPipeCleanupThread(Qname source) {
		validate(source);
		if (Const.ANNOTATION_SOURCE.equals(source)) return; // no pipe cleaning for annotation source
		if (stoppedThreads.contains(source)) return;
		getThreads(source).runInPipeCleanupThread();
	}

	public void runPullReader(Qname source) {
		validate(source);
		if (stoppedThreads.contains(source)) return;
		getThreads(source).runPullReader();
	}

	public void startSource(Qname source, boolean removeFromStopped) {
		validate(source);
		if (removeFromStopped) {
			stoppedThreads.remove(source);
		}
		runInPipe(source);
		runOutPipe(source);
	}

	public void stopAllFor(Qname source) {
		validate(source);
		stoppedThreads.add(source);
		Threads threads = sourceThreads.get(source);
		if (threads == null) return;
		threads.stop();
		while (threads.somethingRunning()) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
		sourceThreads.remove(source);
	}

	private final Map<Qname, List<OutPipeData>> repocessEvents = new HashMap<>();

	public void markDocumentForReprocess(OutPipeData data) {
		synchronized (repocessEvents) {
			Qname source = data.getSource();
			if (!repocessEvents.containsKey(source)) {
				repocessEvents.put(source, new ArrayList<OutPipeData>());
			}
			List<OutPipeData> datas = repocessEvents.get(source);
			removeExistingWithSameDocumentId(data, datas);
			datas.add(data);
		}
	}

	private void removeExistingWithSameDocumentId(OutPipeData data, List<OutPipeData> datas) {
		Iterator<OutPipeData> i = datas.iterator();
		while (i.hasNext()) {
			OutPipeData existing = i.next();
			if (existing.getDocumentId().equals(data.getDocumentId())) {
				i.remove();
			}
		}
	}

	public List<OutPipeData> getReprocessRequested(Qname source) {
		validate(source);
		synchronized (repocessEvents) {
			List<OutPipeData> events = repocessEvents.get(source);
			if (events == null) return Collections.emptyList();
			repocessEvents.put(source, new ArrayList<OutPipeData>());
			return events;
		}
	}

	public void stopAll() {
		for (Threads threads : sourceThreads.values()) {
			threads.stop();
		}
	}

	public boolean somethingRunning() {
		for (Threads threads : sourceThreads.values()) {
			if (threads.somethingRunning()) return true;
		}
		return false;
	}

	private Threads getThreads(Qname source) {
		validate(source);
		if (!sourceThreads.containsKey(source)) {
			sourceThreads.put(source, new Threads(source));
		}
		return sourceThreads.get(source);
	}

	private void validate(Qname source) {
		if (source == null || !source.isSet()) throw new IllegalArgumentException("Empty source qname given");
	}

	public void reportThreadDead(Qname source, Class<?> readerClas) {
		threadStatuses.reportThreadDead(source, readerClas);
	}

}
