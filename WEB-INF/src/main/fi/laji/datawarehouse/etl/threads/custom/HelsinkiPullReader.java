package fi.laji.datawarehouse.etl.threads.custom;

import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer.GISLayerData;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.URIBuilder;

public class HelsinkiPullReader extends ETLTableSyncLogic_BasePullReader<GISLayerData> {

	public static final Qname PROD_SOURCE = new Qname("KE.1681");
	public static final Qname STAGING_SOURCE = new Qname("KE.1681");

	public static final Qname HELSINKI_INVASIVE_COLL_ID = new Qname("HR.1910");

	private final Config config;

	public HelsinkiPullReader(Config config, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super("Helsinki", source(config), new MunicipalityGISHarmonizer(), dao, errorReporter, threadHandler, statusReporter);
		this.config = config;
	}

	public static Qname source(Config config) {
		if (config.productionMode()) return PROD_SOURCE;
		return STAGING_SOURCE;
	}

	@Override
	protected ResultStream<GISLayerData> getIncomingEntryStream() throws Exception {
		return new HelsinkiStream();
	}

	private static class StreamSource {
		HttpGet request;
		Qname collectionId;
		String idPrefix;
		StreamSource(HttpGet request, Qname collectionId, String idPrefix) {
			this.request = request;
			this.collectionId = collectionId;
			this.idPrefix = idPrefix;
		}
	}

	private class HelsinkiStream implements ResultStream<GISLayerData> {

		private final StreamSource layer;
		private final ConnectionDescription desc = config.connectionDescription("HelsinkiPull");
		private final HttpClientService client;

		public HelsinkiStream() {
			if (config.developmentMode()) {
				layer = testLayer("helsinki_vieras.json", HELSINKI_INVASIVE_COLL_ID, null);
			} else {
				layer = layer("helsinki:vieraslajihavainnot", HELSINKI_INVASIVE_COLL_ID, null);
			}
			client = new HttpClientService();
		}

		private StreamSource testLayer(String fileName, Qname collectionId, String idPrefix) {
			return new StreamSource(new HttpGet(desc.url() + "/" + fileName), collectionId, idPrefix);
		}

		private StreamSource layer(String layerName, Qname collectionId, String idPrefix) {
			URIBuilder b = new URIBuilder(desc.url()); // https://domain.name/geoserver/wfs
			b.addParameter("request", "GetFeature");
			b.addParameter("typeName", layerName);
			b.addParameter("outputFormat", "application/json");
			b.addParameter("srsname", "EPSG:3067");
			try {
				HttpGet layer = new HttpGet(b.getURI());
				layer.setHeader("Authorization", this.getBasicAuthHeader(desc.username(), desc.password()));

				return new StreamSource(layer, collectionId, idPrefix);
			} catch (URISyntaxException e) {
				throw new ETLException(e);
			}
		}

		private String getBasicAuthHeader(String user, String password) {
			String authString = user + ":" + password;
			return "Basic " + Base64.getEncoder().encodeToString(authString.getBytes());
		}

		@Override
		public Iterator<GISLayerData> iterator() {
			return features(layer.request).stream().map(json->new GISLayerData(layer.collectionId, layer.idPrefix, json)).iterator();
		}

		private List<JSONObject> features(HttpGet layer) {
			JSONObject data;
			try {
				data = client.contentAsJson(layer);
			} catch (Exception e) {
				throw new ETLException("Reading from " + layer.getURI(), e);
			}
			return data.getArray("features").iterateAsObject();
		}

		@Override
		public void close() {
			if (client != null) client.close();
		}

	}

}
