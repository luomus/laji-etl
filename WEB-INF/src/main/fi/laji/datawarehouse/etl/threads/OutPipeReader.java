package fi.laji.datawarehouse.etl.threads;

import static fi.laji.datawarehouse.etl.utils.Util.batch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.Annotator;
import fi.laji.datawarehouse.etl.models.Annotator.PersonIdResolver;
import fi.laji.datawarehouse.etl.models.Converter;
import fi.laji.datawarehouse.etl.models.Interpreter;
import fi.laji.datawarehouse.etl.models.Securer;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class OutPipeReader extends ETLThread {

	private static final Object LOCK = new Object();
	private static boolean initialized = false;
	private static Annotator annotator;
	private static Interpreter interpreter;
	private static Securer securer;
	private static Converter converter;

	private final ETLDAO etl;
	private final Qname source;

	public OutPipeReader(Qname forSource, ThreadHandler threadHandler, ThreadStatusReporter statusReporter, DAO dao) {
		super(forSource, threadHandler, statusReporter, dao);
		this.source = forSource;
		this.etl = dao.getETLDAO();
		synchronized (LOCK) {
			if (!initialized) {
				initialize();
			}
		}
	}

	private void initialize() {
		if (initialized) return;

		logGeneralMessage("Initializing interpeter, securer, converter...");

		PersonIdResolver personIdResolver = new PersonIdResolver() {
			@Override
			public Qname resolve(String userId) {
				Qname personId = getDAO().getPersonLookupStructure().get(userId);
				return personId;
			}
		};

		annotator = new Annotator(personIdResolver);
		interpreter = new Interpreter(getDAO());
		securer = new Securer(getDAO());
		converter = new Converter(getDAO());

		initialized = true;
		logGeneralMessage("Interpeter, securer, converter initialized!");
	}

	private static final Set<SecureReason> ALLOW_LOAD_TIME_SECURE_REASONS = Utils.set(SecureReason.DATA_QUARANTINE_PERIOD, SecureReason.CUSTOM);

	private class LoadTimesService {
		private final List<Document> documents;
		private final Map<Qname, Long> createdTimes = new HashMap<>();
		private Map<Qname, Long> times;
		public LoadTimesService(List<Document> documents) {
			this.documents = documents;
		}
		public void setLoadTimes() {
			if (documents.isEmpty()) return;
			reportStatus("Setting load times");
			loadLoadTimes();
			for (Document document : documents) {
				if (showLoadTimes(document)) {
					Long time = getFirstLoadedDate(document);
					document.setFirstLoadTime(time);
					document.setLoadTimeNow();
				} else {
					document.clearLoadTimes();
				}
			}
		}
		private boolean showLoadTimes(Document document) {
			if (!document.isPublic()) return true;
			if (document.getSecureLevel() == SecureLevel.NONE) return true;
			if (document.getSecureReasons().isEmpty()) return false;
			for (SecureReason reason : document.getSecureReasons()) {
				if (!ALLOW_LOAD_TIME_SECURE_REASONS.contains(reason)) return false;
			}
			return true;
		}
		private void loadLoadTimes() {
			times = etl.getFirstLoadTimes(documents);
		}
		private Long getFirstLoadedDate(Document document) {
			Qname id = document.getDocumentId();
			Long timestamp = times.get(id);
			if (timestamp == null) {
				timestamp = DateUtils.getCurrentEpoch();
				times.put(id, timestamp);
				createdTimes.put(id, timestamp);
			}
			return timestamp;
		}
		public void commitChanges() {
			if (createdTimes.isEmpty()) return;
			reportStatus("Commiting first loaded times");
			etl.storeFirstLoadedTimes(createdTimes);
		}

	}

	private static class Results {
		private final List<OutPipeData> successful = new ArrayList<>();
		private final List<SaveFailure> failed = new ArrayList<>();
		public void successful(OutPipeData data) {
			successful.add(data);
		}
		public void failure(OutPipeData data, String errorMessage) {
			failed.add(new SaveFailure(data, errorMessage));
		}
		public List<OutPipeData> getSuccessful() {
			return successful;
		}
		public List<SaveFailure> getFailed() {
			return failed;
		}
		public boolean hasFailed() {
			return !getFailed().isEmpty();
		}
	}

	private static class SaveFailure {
		private final OutPipeData data;
		private final String errorMessage;
		public SaveFailure(OutPipeData data, String errorMessage) {
			this.data = data;
			this.errorMessage = errorMessage;
		}
		public OutPipeData getData() {
			return data;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
	}

	@Override
	protected long read() {
		List<OutPipeData> datas = null;
		boolean reprocessingErroneous = false;
		boolean reprocessingMultitimeErroneus = false;

		try {
			reportStatus("Getting entries marked for reprocess");
			datas = getMarkedForReprocess(); // Fast update annotated documents ahead of others
			if (datas.isEmpty()) {
				reportStatus("Getting unprocessed from out pipe");
				datas = etl.getUnprocessedNotErroneousInOrderFromOutPipe(source);
			}
			if (datas.isEmpty()) {
				reportStatus("Getting erroneous from out pipe");
				datas = etl.getFailedAttemptDataFromOutPipe(source);
				if (!datas.isEmpty()) {
					reprocessingErroneous = true;
				}
			}
			if (datas.isEmpty()) {
				reportStatus("Getting single erroneous from out pipe");
				OutPipeData singleErroneus = etl.getSingleMultiAttemptedDataFromOutPipe(source);
				if (singleErroneus != null) {
					datas = Collections.singletonList(singleErroneus);
					reprocessingErroneous = true;
					reprocessingMultitimeErroneus = true;
				}
			}
			if (datas.isEmpty()) {
				reportStatus("Getting expired splitted data from queue");
				List<QueueData> queueDatas = etl.getExpiredSplittedDocumentsFromQueue(source);
				if (!queueDatas.isEmpty()) {
					prevRunHadNoWork = false;
					return saveSplittedQueueData(queueDatas);
				}
				return noWork();
			}
			prevRunHadNoWork = false;
		} catch (Throwable t) {
			reportStatus("Handling unknown pipe read exceptions");
			logError(t);
			return ERROR_PAUSE;
		}

		try {
			validateSameDocumentNotTwiceInSameBatch(datas);
			Results results = process(datas);
			markSuccessfullyProcessed(results);
			markFailed(results);
			if (results.hasFailed() && reprocessingMultitimeErroneus) {
				reportError(results.getFailed().get(0).getData());
			}
			if (reprocessingErroneous && results.hasFailed()) return ERROR_PAUSE;
			return SMALL_PAUSE;
		} catch (Throwable t) {
			reportStatus("Handling unknown exceptions");
			try {
				etl.reportAttempted(datas, LogUtils.buildStackTrace(t));
			} catch (Throwable t2) {
				logError(t2);
			}
			if (reprocessingMultitimeErroneus) reportError(datas.get(0));
			return ERROR_PAUSE;
		}
	}

	private void reportError(OutPipeData data) {
		dao.getErrorReporter().report(data.getDocumentId().toURI() + " has failed multiple times in out pipe processing");
	}

	private boolean prevRunHadNoWork = false;

	private long noWork() {
		if (prevRunHadNoWork) {
			getThreadHandler().runInPipeCleanupThread(source);
			return NO_WORK_PAUSE; // Close down this thread
		}
		prevRunHadNoWork = true;
		return 1000*60; // Try again in 60s if there is work; then if there isn't do cleanup and close thread
	}

	private void validateSameDocumentNotTwiceInSameBatch(List<OutPipeData> datas) {
		Set<Qname> documentIds = new HashSet<>();
		for (OutPipeData data : datas) {
			if (documentIds.contains(data.getDocumentId())) {
				throw new ETLException("Document " + data.getDocumentId().toURI() + " is in same batch multiple times");
			}
			documentIds.add(data.getDocumentId());
		}
	}

	private List<OutPipeData> getMarkedForReprocess() {
		return getThreadHandler().getReprocessRequested(source);
	}

	private long saveSplittedQueueData(List<QueueData> queueDatas) {
		try {
			return tryToSaveSplittedQueueData(queueDatas);
		} catch (Exception e) {
			reportStatus("Reporting splitted data failures");
			logError(e, queueDatas);
			return ERROR_PAUSE;
		}
	}

	private long tryToSaveSplittedQueueData(List<QueueData> queueDatas) throws CriticalParseFailure {
		List<Document> splittedDocuments = generateSplittedDocuments(queueDatas);
		reportStatus("Queuing to save splitted data to Public DW");
		getDAO().getPublicVerticaDAO().save(splittedDocuments, getStatusReporter());
		removeFromQueue(queueDatas);
		return SMALL_PAUSE;
	}

	private List<Document> generateSplittedDocuments(List<QueueData> queueDatas) throws CriticalParseFailure {
		Map<String, CollectionMetadata> collections = getDAO().getCollections();
		List<Document> splittedDocuments = new ArrayList<>();
		for (QueueData queueData : queueDatas) {
			Document document = toDocument(queueData);
			validateCollectionId(document.getCollectionId(), collections);
			splittedDocuments.add(document);
			document.clearLoadTimes();
		}
		return splittedDocuments;
	}

	private void removeFromQueue(List<QueueData> queueDatas) {
		etl.removeSplittedQueue(ids(queueDatas));
	}

	private List<Long> ids(List<QueueData> queueDatas) {
		return queueDatas.stream().map(d->d.getId()).collect(Collectors.toList());
	}

	private Document toDocument(QueueData queueData) {
		try {
			return JsonToModel.documentFromJson(new JSONObject(queueData.getData()));
		} catch (CriticalParseFailure e) {
			throw new ETLException("Transforming queue data id " + queueData.getId(), e);
		}
	}

	private void markSuccessfullyProcessed(Results results) {
		reportStatus("Reporting out pipe data successes");
		etl.reportProcessed(results.getSuccessful());
	}

	private void markFailed(Results results) {
		for (SaveFailure failure : results.getFailed()) {
			reportStatus("Reporting out pipe data failures");
			etl.reportAttempted(Utils.singleEntryList(failure.getData()), failure.getErrorMessage());
		}
	}

	private Results process(List<OutPipeData> datas) {
		Set<Qname> documentIds = new HashSet<>();
		for (OutPipeData data : datas) {
			documentIds.add(data.getDocumentId());
		}

		reportStatus("Remove batch splitted");
		etl.removeSplittedDocumentsFromQueue(documentIds);
		reportStatus("Get batch annotations");
		Map<Qname, List<Annotation>> annotations = etl.getAnnotations(documentIds);
		reportStatus("Get batch splitted documents");
		SplittedDocumentIds splittedDocumentIds = etl.getSplittedDocumentIds(documentIds);

		Map<String, CollectionMetadata> collections = getDAO().getCollections();

		List<Document> publicDocuments = new ArrayList<>();
		List<Document> privateDocuments = new ArrayList<>();
		List<AnnotationNotification> notifications = new ArrayList<>();
		Results results = new Results();

		for (OutPipeData data : datas) {
			try {
				DwRoot root = getDwRoot(data);
				validate(root, collections);
				if (!root.isDeleteRequest()) {
					notifications.addAll(annotateInterpretSecureAndConvertReturnNotifications(root, annotations));
				}
				addToLists(publicDocuments, privateDocuments, root, splittedDocumentIds);
				reportStatus("Store splitted");
				addSplittedDocumentsToQueue(root);
				results.successful(data);
			} catch (Exception e) {
				String errorMessage = LogUtils.buildStackTrace(e);
				results.failure(data, errorMessage);
			}
		}

		LoadTimesService loadTimesService = setLoadTimes(publicDocuments, privateDocuments);
		saveToPublicAndPrivateDw(publicDocuments, privateDocuments);
		loadTimesService.commitChanges();

		reportStatus("Sending notifications");
		addNotificationsToNotificationQueue(notifications);

		return results;
	}

	private void validate(DwRoot root, Map<String, CollectionMetadata> collections) throws CriticalParseFailure {
		if (root.isDeleteRequest()) return;
		validateCollectionId(root.getCollectionId(), collections);
		if (root.getPublicDocument() != null) {
			validateCollectionId(root.getPublicDocument().getCollectionId(), collections);
		}
		if (root.getPrivateDocument() != null) {
			validateCollectionId(root.getPrivateDocument().getCollectionId(), collections);
		}
	}

	private void validateCollectionId(Qname collectionId, Map<String, CollectionMetadata> collections) throws CriticalParseFailure {
		if (!given(collectionId)) {
			throw new CriticalParseFailure("No collectionId");
		}
		if (!collections.containsKey(collectionId.toURI())) throw new CriticalParseFailure("Unknown collectionId: " + collectionId);
	}

	private boolean given(Qname q) {
		return q != null && q.isSet();
	}

	private LoadTimesService setLoadTimes(List<Document> publicDocuments, List<Document> privateDocuments) {
		List<Document> joined = new ArrayList<>(publicDocuments);
		joined.addAll(privateDocuments);
		joined.removeIf(deleted());
		LoadTimesService loadTimesService = new LoadTimesService(joined);
		loadTimesService.setLoadTimes();
		return loadTimesService;
	}

	private Predicate<Document> deleted() {
		return d->Boolean.TRUE.equals(d.isDeleted());
	}

	private void addNotificationsToNotificationQueue(List<AnnotationNotification> notifications) {
		if (notifications.isEmpty()) return;

		replaceIdsOfSplittedUnitsForAnnotatedDocumentAnnotatedNotifications(notifications);

		for (AnnotationNotification notification : notifications) {
			etl.storeNotification(notification);
		}
	}

	private void replaceIdsOfSplittedUnitsForAnnotatedDocumentAnnotatedNotifications(List<AnnotationNotification> notifications) {
		// If unit has been splitted from original document and that splitted document is being annotated:
		//  (1) original observer should get notification using the original document/unitid
		//  (2) other users (previous annotators of the document) should get the notification using the splitted document/unitid
		Set<Qname> annotatedDocumentAnnotatedDocumentIds = getDocumentIdsWhereAnnotatedDocumentAnnotated(notifications);
		if (!annotatedDocumentAnnotatedDocumentIds.isEmpty()) {
			replaceIdsOfSplittedUnitsToSplittedIds(notifications, annotatedDocumentAnnotatedDocumentIds);
		}
	}

	private void replaceIdsOfSplittedUnitsToSplittedIds(List<AnnotationNotification> notifications, Set<Qname> annotatedDocumentAnnotatedDocumentIds) {
		SplittedDocumentIds splittedDocumentIds = etl.getSplittedDocumentIds(annotatedDocumentAnnotatedDocumentIds);
		for (AnnotationNotification notification : notifications) {
			Qname splittedDocumentId = splittedDocumentIds.getSplittedDocumentIdFor(notification.getAnnotation().getRootID(), notification.getAnnotation().getTargetID());
			if (splittedDocumentId == null) continue;
			Qname splittedUnitId = new Qname(splittedDocumentId.toString()+"#U");
			try {
				notification.getAnnotation().setRootID(splittedDocumentId);
				notification.getAnnotation().setTargetID(splittedUnitId);
			} catch (CriticalParseFailure e) {
				throw new IllegalStateException("Impossible state");
			}
		}
	}

	private Set<Qname> getDocumentIdsWhereAnnotatedDocumentAnnotated(List<AnnotationNotification> notifications) {
		Set<Qname> annotatedDocumentAnnotatedDocumentIds = new HashSet<>();
		for (AnnotationNotification notification : notifications) {
			if (notification.getNotificationReason() == NotificationReason.ANNOTATED_DOCUMENT_ANNOTATED) {
				annotatedDocumentAnnotatedDocumentIds.add(notification.getAnnotation().getRootID());
			}
		}
		return annotatedDocumentAnnotatedDocumentIds;
	}

	private void addSplittedDocumentsToQueue(DwRoot root) {
		if (root.hasSplittedPublicDocuments()) {
			for (Document splittedDocument : root.getSplittedPublicDocuments()) {
				String json = splittedDocument.toJSON().toString();
				etl.storeSplittedDocumentToQueue(root.getSourceId(), root.getDocumentId(), json);
			}
		}
	}

	private DwRoot getDwRoot(OutPipeData data) throws CriticalParseFailure {
		return DwRoot.fromJson(new JSONObject(data.getData()));
	}

	private void saveToPublicAndPrivateDw(List<Document> publicDocuments, List<Document> privateDocuments) {
		save(publicDocuments, getDAO().getPublicVerticaDAO());
		save(privateDocuments, getDAO().getPrivateVerticaDAO());
	}

	private void save(List<Document> documents, VerticaDAO dao) {
		List<List<Document>> batches = batch(documents, 400);
		for (List<Document> batch : batches) {
			dao.save(batch, getStatusReporter());
			if (batches.size() > 1) {
				Thread.yield();
			}
		}
	}

	private void addToLists(List<Document> publicDocuments, List<Document> privateDocuments, DwRoot root, SplittedDocumentIds batchSplittedDocumentIds) {
		Collection<Qname> splittedDocumentIds = batchSplittedDocumentIds.getSplittedDocumentIdsFor(root.getDocumentId());

		if (root.isDeleteRequest()) {
			Document deleteDocument = Document.createDeletedDocument(root);
			publicDocuments.add(deleteDocument);
			privateDocuments.add(deleteDocument);
			for (Qname previoslySplittedDocumentId : splittedDocumentIds) {
				publicDocuments.add(createDeletedDocument(root.getSourceId(), previoslySplittedDocumentId));
			}
			return;
		}

		Document publicDocument = root.getPublicDocument();
		Document privateDocument = root.getPrivateDocument();
		if (publicDocument != null) {
			publicDocuments.add(publicDocument);
		}
		if (privateDocument != null) {
			privateDocuments.add(privateDocument);
		} else {
			privateDocuments.add(publicDocument);
		}

		if (root.hasSplittedPublicDocuments()) {
			reportStatus("Searching for already existing splitted document ids from public dw (" + root.getSplittedPublicDocuments().size() + ")");
			Set<Qname> existingInPublicDw = getExistingFromPublicDw(root.getSplittedPublicDocuments());
			Iterator<Document> splittedDocumentsIterator = root.getSplittedPublicDocuments().iterator();
			while (splittedDocumentsIterator.hasNext()) {
				Document splittedDocument = splittedDocumentsIterator.next();
				if (existingInPublicDw.contains(splittedDocument.getDocumentId())) {
					// no need to delay by adding to queue -> store immediately
					publicDocuments.add(splittedDocument);
					splittedDocumentsIterator.remove();
					splittedDocumentIds.remove(splittedDocument.getDocumentId());
				}
			}
		}

		// Remove those splitted that no longer exist in this new version of the document (have been deleted)
		for (Qname previoslySplittedDocumentId : splittedDocumentIds) {
			publicDocuments.add(createDeletedDocument(root.getSourceId(), previoslySplittedDocumentId));
		}
	}

	private Set<Qname> getExistingFromPublicDw(List<Document> splittedPublicDocuments) {
		try {
			AggregatedQuery query = new AggregatedQuery(
					new BaseQueryBuilder(Concealment.PUBLIC)
					.setBase(Base.DOCUMENT)
					.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
					.setCaller(this.getClass())
					.setDefaultFilters(false).build(),
					new AggregateBy(Base.DOCUMENT, false).addField("document.documentId"), 1, splittedPublicDocuments.size() * 2);
			Filters filters = query.getFilters();
			for (Document splittedDoc : splittedPublicDocuments) {
				filters.setDocumentId(splittedDoc.getDocumentId());
			}
			List<AggregateRow> results = getDAO().getPublicVerticaDAO().getQueryDAO().getRawAggregate(query);
			Set<Qname> existing = new HashSet<>();
			for (AggregateRow row : results) {
				existing.add(Qname.fromURI(row.getAggregateByValues().get(0).toString()));
			}
			return existing;
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private Document createDeletedDocument(Qname source, Qname previoslySplittedDocumentId) {
		@SuppressWarnings("deprecation")
		Document document = new Document();
		try {
			document.setDocumentId(previoslySplittedDocumentId);
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException();
		}
		document.setSourceId(source);
		document.setDeleted(true);
		return document;
	}

	private List<AnnotationNotification> annotateInterpretSecureAndConvertReturnNotifications(DwRoot root, Map<Qname, List<Annotation>> batchAnnotations) {
		reportStatus("Annotating " + root.getDocumentId());
		annotationsToRoot(root, batchAnnotations);
		List<AnnotationNotification> notifications = annotator.annotate(root);

		reportStatus("Interpreting " + root.getDocumentId());
		interpreter.interpret(root);

		reportStatus("Securing " + root.getDocumentId());
		securer.secure(root);

		reportStatus("Converting " + root.getDocumentId());
		converter.convert(root);

		return notifications;
	}

	private void annotationsToRoot(DwRoot root, Map<Qname, List<Annotation>> batchAnnotations) {
		List<Annotation> annotations = batchAnnotations.get(root.getDocumentId());
		if (annotations != null) {
			for (Annotation annotation : annotations) {
				root.addAnnotation(annotation);
			}
		}
	}

}
