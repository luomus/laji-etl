package fi.laji.datawarehouse.etl.threads.custom;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.StatelessSession;

import com.github.jsonldjava.shaded.com.google.common.collect.Lists;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.AbortedStream;
import fi.laji.datawarehouse.dao.Streams.ResultSetStream;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.Harmonizer;
import fi.laji.datawarehouse.etl.threads.ETLThread;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

public abstract class ETLTableSyncLogic_BasePullReader<E> extends ETLThread {

	private static final String TEXT_PLAIN = "text/plain";
	private static final String APPLICATION_JSON = "application/json";
	private static final int MAX_DELETE_COUNT = 10000;

	private final ErrorReporter errorReporter;
	private final DwETLDAO etl;
	private final Harmonizer<E> harmonizer;
	private final String processName;

	public ETLTableSyncLogic_BasePullReader(String processName, Qname source, Harmonizer<E> harmonizer, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		this(processName, Collections.emptyList(), source, harmonizer, dao, errorReporter, threadHandler, statusReporter);
	}

	public ETLTableSyncLogic_BasePullReader(String processName, List<String> additionalETLTables, Qname source, Harmonizer<E> harmonizer, DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super(source, threadHandler, statusReporter, dao);
		this.errorReporter = errorReporter;
		this.etl = new DwETLDAOImple(processName, additionalETLTables, dao);
		this.harmonizer = harmonizer;
		this.processName = processName;
	}

	protected abstract ResultStream<E> getIncomingEntryStream() throws Exception;

	@Override
	public long read() {
		boolean failed = false;
		try {
			tryToRead();
		} catch (Throwable t) {
			reportStatus("Handling exceptions");
			logError(t);
			errorReporter.report(processName + " pull", t);
			failed = true;
		} finally {
			if (failed) {
				log("Terminated with errors, will close down");
			} else {
				log("Done successfully, will close down");
			}
		}
		return NO_WORK_PAUSE;
	}

	private void tryToRead() throws Exception {
		log("Clear oracle tables");
		etl.clearTables();

		log("Generating ETL documents");
		int incoming = generateAndStoreIncomingDocuments();
		log("Generating ETL documents done: " + incoming + " rows");

		if (shouldStop()) return;

		log("Fetching new or changed documents and storing to IN-pipe");
		int upserts = resolveUpsertsStoreToInPipe();
		log("Stored " + upserts + " documents to IN-pipe");

		if (shouldStop()) return;

		log("Fetching deletions");
		List<String> deletedDocumentIds = etl.getDeletions();
		if (deletedDocumentIds.size() < MAX_DELETE_COUNT) {
			delete(deletedDocumentIds);
			log("Stored " + deletedDocumentIds.size() + " deletions to IN-pipe");
		} else {
			dontDelete(deletedDocumentIds);
		}

		if (shouldStop()) return;

		log("Swap incoming to previous");
		etl.swapIncomingToPrevious();

		dao.getETLDAO().releaseInPipeHold(source);

		getThreadHandler().runInPipe(source);
	}

	private void dontDelete(List<String> deletedDocumentIds) {
		// This is here for the case that reading incoming data is somehow only partial (from 3rd party's source or because of fault in our ETL)
		// -> we don't want to delete all existing documents
		String message = processName + ": Delete threshold reached, will not delete: " + deletedDocumentIds.size();
		errorReporter.report(message);
		log(message);
		// Maybe should also just abort with errors? In case incoming data really is only partial, when we swap incoming to previous that will mean almost everything in the next
		// update will be considered new inserts .. not a big deal, just unnecessary processing
	}

	private void delete(List<String> deletedDocumentIds) {
		List<String> entries = deletedDocumentIds.stream().map(s->"DELETE " + s).collect(Collectors.toList());
		for (List<String> batch : Lists.partition(entries, 100)) {
			storeToInPipe(batch, TEXT_PLAIN);
		}
	}

	private int resolveUpsertsStoreToInPipe() throws Exception {
		int stored = 0;
		List<String> batch = new ArrayList<>(100);
		try (ResultSetStream<String> upserts = etl.getIncomingUpserts()) {
			for (String json : upserts) {
				batch.add(json);
				stored++;
				if (batch.size() >= 100) {
					reportStatus("Storing to in pipe: " + stored);
					storeToInPipe(batch, APPLICATION_JSON);
					batch.clear();
					if (shouldStop()) return -1;
				}
			}
		}
		storeToInPipe(batch, APPLICATION_JSON);
		return stored;
	}

	private int generateAndStoreIncomingDocuments() throws Exception {
		int stored = 0;
		List<DwRoot> batch = new ArrayList<>(100);
		try (ResultStream<E> stream = getIncomingEntryStream()) {
			if (stream instanceof AbortedStream) return -1; // should stop
			for (E entry : stream) {
				List<DwRoot> roots = harmonizer.harmonize(entry, source);
				batch.addAll(roots);
				stored += roots.size();
				if (batch.size() >= 100) {
					reportStatus("Storing to incoming batch with size " + batch.size() + "; document count " + stored);
					etl.insertIncoming(batch);
					batch.clear();
					if (shouldStop()) return -1;
				}
			}
		}
		etl.insertIncoming(batch);
		return stored;
	}

	private void storeToInPipe(List<String> batch, String contentType) {
		if (batch.isEmpty()) return;
		try {
			tryToStoreToInPipe(batch, contentType);
		} catch (Exception e) {
			logError(e);
			tryToStoreToInPipeAgain(batch, contentType);
		}
	}

	private void tryToStoreToInPipeAgain(List<String> batch, String contentType) {
		int i = 0;
		while (i++ <= 5) {
			sleep();
			try {
				tryToStoreToInPipe(batch, contentType);
				return;
			} catch (Exception e) {
				logError(e);
			}
		}
		throw new ETLException("Tried to store to in pipe but gave up. Aborting!");
	}

	private void tryToStoreToInPipe(List<String> batch, String contentType) {
		dao.getETLDAO().storeToInPipeOnHold(source, batch, contentType);
	}

	private void sleep() {
		try { sleep(10000); } catch (InterruptedException e1) {}
	}

	private interface DwETLDAO {

		void clearTables() throws Exception;

		ResultSetStream<String> getIncomingUpserts() throws Exception;

		List<String> getDeletions() throws Exception;

		void swapIncomingToPrevious() throws Exception;

		void insertIncoming(List<DwRoot> roots) throws Exception;

	}

	private static class DwETLDAOImple implements DwETLDAO {

		private final String tablePrefix;
		private final List<String> etlTables;
		private final DAO dao;

		public DwETLDAOImple(String processName, List<String> additionalETLTables, DAO dao) {
			this.tablePrefix = processName.toLowerCase();
			this.dao = dao;
			this.etlTables = additionalETLTables == null ? Collections.emptyList() : additionalETLTables;
		}

		@Override
		public void clearTables() throws SQLException {
			TransactionConnection con = null;
			try {
				con = openDwETLConnection();
				truncateTable(tablePrefix+"_etl_incoming", con);
				for (String table : etlTables) {
					truncateTable(tablePrefix+"_etl_"+table, con);
				}
			} finally {
				Utils.close(con);
			}
		}

		private void truncateTable(String table, TransactionConnection con) throws SQLException {
			PreparedStatement p = con.prepareStatement("TRUNCATE TABLE " + table);
			p.execute();
			p.close();
		}

		private class StringInstanter implements ResultSetStream.ToInstance<String> {
			@Override
			public String get(ResultSet rs) throws SQLException {
				return rs.getString(1);
			}
		}

		@Override
		public ResultSetStream<String> getIncomingUpserts() throws Exception {
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = openDwETLConnection();
				String sql = "" +
						" SELECT incoming.json " +
						" FROM "+tablePrefix+"_etl_incoming incoming " +
						" LEFT JOIN "+tablePrefix+"_etl_previous previous ON incoming.documentId = previous.documentId " +
						" WHERE previous.hash IS NULL OR previous.hash != incoming.hash ";
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				return new ResultSetStream<>(rs, p, con, new StringInstanter());
			} catch (Exception e) {
				Utils.close(p, rs, con);
				throw e;
			}
		}

		@Override
		public List<String> getDeletions() {
			try (StatelessSession session = dao.getETLDAO().getETLEntityConnection()) {
				@SuppressWarnings("unchecked")
				List<String> results = session.createSQLQuery("" +
						" SELECT p.documentId " +
						" FROM "+tablePrefix+"_etl_previous p " +
						" LEFT JOIN "+tablePrefix+"_etl_incoming i ON i.documentId = p.documentId " +
						" WHERE i.documentId IS NULL ").setFetchSize(4001).list();
				return results;
			}
		}

		@Override
		public void swapIncomingToPrevious() throws SQLException {
			TransactionConnection con = null;
			PreparedStatement p = null;
			try {
				con = openDwETLConnection();
				truncateTable(tablePrefix+"_etl_previous", con);
				con.startTransaction();
				p = con.prepareStatement("INSERT INTO "+tablePrefix+"_etl_previous (documentId, json, hash) SELECT documentId, json, hash FROM "+tablePrefix+"_etl_incoming");
				p.execute();
				con.commitTransaction();
			} catch (Exception e) {
				if (con != null) con.rollbackTransaction();
				throw e;
			} finally {
				Utils.close(p, con);
			}
		}

		@Override
		public void insertIncoming(List<DwRoot> roots) throws Exception {
			if (roots.isEmpty()) return;
			TransactionConnection con = null;
			PreparedStatement p = null;
			Qname currentDocumentId = null;
			try {
				con = openDwETLConnection();
				p = con.prepareStatement("INSERT INTO " +tablePrefix+"_etl_incoming (documentId, json, hash) VALUES (?, ?, ?)");
				con.startTransaction();
				for (DwRoot root : roots) {
					currentDocumentId = root.getDocumentId();
					root.clearProcesstime(); // Documents must not have generated timestamp or hashes would never match (timestamp is later set when the stored json is processed through inpipe->outpipe)
					String json = root.toJSON().toString();
					p.setString(1, root.getDocumentId().toURI());
					p.setString(2, json);
					p.setString(3, Util.getHash(json));
					p.addBatch();
				}
				p.executeBatch();
				con.commitTransaction();
			} catch (Exception e) {
				if (con != null) con.rollbackTransaction();
				throw new ETLException("Storing to incoming failed for " + currentDocumentId + " (or batch containing at least this doc)", e);
			} finally {
				Utils.close(p, con);
			}
		}

		private TransactionConnection openDwETLConnection() {
			return new SimpleTransactionConnection(dao.getETLDAO().getETLConnection());
		}

	}

	protected void log(String message) {
		reportStatus(message);
		dao.logMessage(source, this.getClass(), message);
	}

}
