package fi.laji.datawarehouse.etl.threads.custom;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.threads.ETLThread;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.URIBuilder;

public class SykeZoobenthosPullReader extends ETLThread {

	public static final Qname SOURCE = new Qname("KE.941");
	private static final ZoneId UTC = ZoneId.of("UTC");

	private final DAO dao;
	private final ErrorReporter errorReporter;

	public SykeZoobenthosPullReader(DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatusReporter statusReporter) {
		super(SOURCE, threadHandler, statusReporter, dao);
		this.dao = dao;
		this.errorReporter = errorReporter;
	}

	@Override
	public long read() {
		SykeZoobenthosAPI api = new SykeZoobenthosAPIImple(getStatusReporter());
		SykeZoobenthosService service = new SykeZoobenthosService(api);
		try {
			tryToRead(service);
		} catch (Throwable t) {
			reportStatus("Handling exceptions");
			logError(t);
			errorReporter.report("Syke Zoobenthos pull", t);
			return ERROR_PAUSE;
		} finally {
			api.close();
		}
		return NO_WORK_PAUSE;
	}

	private void tryToRead(SykeZoobenthosService service) {
		int timestamp = latestTimestamp();
		reportAndLogStatus("Getting modifield root event ids since " + timestamp);
		Set<String> rootEventIds = service.getModifiedRootEventIds(timestamp);
		reportAndLogStatus("Modifield root event ids count: " + rootEventIds.size());

		if (rootEventIds.isEmpty()) return;
		if (shouldStop()) return;

		List<String> batch = new ArrayList<>(10);
		for (String rootEventId : rootEventIds) {
			reportStatus("Event tree for " + rootEventId);
			JSONObject json = service.getEventTree(rootEventId);
			batch.add(json.toString());
			if (batch.size() >= 10) {
				storeToInPipe(batch);
				batch.clear();
				if (shouldStop()) return;
			}
		}
		if (!batch.isEmpty()) {
			storeToInPipe(batch);
		}
		reportAndLogStatus("Setting last read timestamp to " + today());
		setLatestTimestamp(today());
		getThreadHandler().runInPipe(SOURCE);
		reportAndLogStatus("Done, will close down");
	}

	private void storeToInPipe(List<String> batch) {
		reportStatus("Storing to in pipe");
		dao.getETLDAO().storeToInPipe(SOURCE, batch, "application/json");
	}

	private void setLatestTimestamp(int timestamp) {
		dao.getETLDAO().setLastReadPullApiEntrySequence(SOURCE, timestamp);
	}

	private int today() {
		return (int) LocalDate.now().atStartOfDay(UTC).toEpochSecond();
	}

	private int latestTimestamp() {
		Integer i = dao.getETLDAO().getLastReadPullApiEntrySequenceFor(SOURCE);
		if (i == null) return 0;
		return i;
	}

	private void reportAndLogStatus(String status) {
		reportStatus(status);
		dao.logMessage(SOURCE, SykeZoobenthosPullReader.class, status);
	}

	public static class SykeZoobenthosService {

		private static final String EVENTS = "Events";
		private static final String PARENT_EVENT_ID = "parentEventID";
		private static final String EVENT_ID = "eventID";
		private final SykeZoobenthosAPI api;
		private final Map<String, String> eventIdParentIds = new HashMap<>();

		public SykeZoobenthosService(SykeZoobenthosAPI api) {
			this.api = api;
		}

		public Set<String> getModifiedRootEventIds(int timestamp) {
			Set<String> modifiedEventIds = getModifiedEventIds(timestamp);
			if (modifiedEventIds.isEmpty()) return modifiedEventIds;
			Set<String> rootEventIds = new HashSet<>();
			for (String eventId : modifiedEventIds) {
				if (eventId == null || eventId.isEmpty()) throw new IllegalStateException();
				String rootEventId = getRoot(eventId);
				if (rootEventId == null || rootEventId.isEmpty()) throw new IllegalStateException();
				rootEventIds.add(rootEventId);
			}
			return rootEventIds;
		}

		private String getRoot(String eventId) {
			String parentId;
			while ((parentId = getParentEventId(eventId)) != null) {
				eventId = parentId;
			}
			return eventId;
		}

		private String getParentEventId(String eventId) {
			if (eventIdParentIds.containsKey(eventId)) {
				return eventIdParentIds.get(eventId);
			}
			String parentId = api.getParentEventId(eventId);
			eventIdParentIds.put(eventId, parentId);
			return parentId;
		}

		private Set<String> getModifiedEventIds(int timestamp) {
			Set<String> modifiedEventIds = new HashSet<>();

			List<List<String>> event = api.get("Event", timestamp, EVENT_ID, PARENT_EVENT_ID);
			eventIdAndParent(modifiedEventIds, event);

			if (timestamp != 0) {
				List<List<String>> eventHistory = api.get("EventHistory", timestamp, EVENT_ID, PARENT_EVENT_ID);
				List<List<String>> occurrence = api.get("Occurrence", timestamp, EVENT_ID);
				List<List<String>> occurrenceHistory = api.get("OccurrenceHistory", timestamp, EVENT_ID);
				List<List<String>> measurement = api.get("Measurement", timestamp, EVENT_ID);
				List<List<String>> measurementHistory = api.get("MeasurementHistory", timestamp, EVENT_ID);
				eventIdAndParent(modifiedEventIds, eventHistory);
				eventId(modifiedEventIds, occurrence);
				eventId(modifiedEventIds, occurrenceHistory);
				eventId(modifiedEventIds, measurement);
				eventId(modifiedEventIds, measurementHistory);
			}

			return modifiedEventIds;
		}

		private void eventId(Set<String> modifiedEventIds, List<List<String>> occurrence) {
			for (List<String> values: occurrence) {
				modifiedEventIds.add(values.get(0));
			}
		}

		private void eventIdAndParent(Set<String> modifiedEventIds, List<List<String>> rows) {
			for (List<String> values : rows) {
				modifiedEventIds.add(values.get(0));
				eventIdParentIds.put(values.get(0), values.get(1));
			}
		}

		public JSONObject getEventTree(String rootEventId) {
			JSONObject root = api.getEvent(rootEventId);
			if (root == null) {
				return deletedEvent(rootEventId);
			}
			addChildEvents(root);
			root.setString(Const.SCHEMA, Const.SYKE_ZOOBENTHOS_SCHEMA);
			return root;
		}

		private void addChildEvents(JSONObject json) {
			String eventId = json.getString(EVENT_ID);
			for (JSONObject child : api.getChildEvents(eventId)) {
				json.getArray(EVENTS).appendObject(child);
				addChildEvents(child);
			}
		}

		private JSONObject deletedEvent(String rootEventId) {
			try {
				return DwRoot.createDeleteRequest(documentId(rootEventId), SOURCE).toJSON();
			} catch (CriticalParseFailure e) {
				throw new IllegalStateException("Invalid document id " + rootEventId);
			}
		}

		private Qname documentId(String rootEventId) {
			return new Qname(SOURCE.toString()+"/"+rootEventId);
		}

	}

	public interface SykeZoobenthosAPI {
		public static class SykeZoobenthosAPIException extends RuntimeException {
			private static final long serialVersionUID = -1062048504859595757L;
			public SykeZoobenthosAPIException(String message, Throwable cause) {
				super(message, cause);
			}
		}
		void close();
		JSONObject getEvent(String eventId) throws SykeZoobenthosAPIException;
		List<JSONObject> getChildEvents(String eventId) throws SykeZoobenthosAPIException;
		String getParentEventId(String eventId) throws SykeZoobenthosAPIException;
		List<List<String>> get(String endpoint, int timestamp, String... select) throws SykeZoobenthosAPIException;
	}

	public static class SykeZoobenthosAPIImple implements SykeZoobenthosAPI {

		private static final String EVENT_ID = "eventID";
		private static final String EXPAND = "$expand";
		private static final String PARENT_EVENT_ID = "parentEventID";
		private static final String SELECT = "$select";
		private static final String FILTER = "$filter";
		private static final String VALUE = "value";
		private static final String ODATA_NEXT_LINK = "odata.nextLink";
		private static final String BASE_URL = "https://rajapinnat.ymparisto.fi/api/Pohjaelainrajapinta/1.0/odata/";
		private final HttpClientService client;
		private final ThreadStatusReporter statusReporter;
		private boolean open;

		public SykeZoobenthosAPIImple(ThreadStatusReporter statusReporter) {
			this.client = new HttpClientService();
			this.statusReporter = statusReporter;
			statusReporter.setStatus(SykeZoobenthosAPIImple.class.getSimpleName() + " created");
			open = true;
		}

		@Override
		public void close() {
			open = false;
			client.close();
			statusReporter.setStatus(SykeZoobenthosAPIImple.class.getSimpleName() + " closed");
		}

		@Override
		public String getParentEventId(String eventId) {
			if (!open) closed();
			String query = buildParentEventIdQuery(eventId);
			JSONObject json = get(query);
			JSONArray rows = json.getArray(VALUE);
			if (rows.isEmpty()) return null;
			String parentEventId = rows.iterateAsObject().get(0).getString(PARENT_EVENT_ID);
			if (parentEventId.isEmpty()) return null;
			return parentEventId;
		}

		private String buildParentEventIdQuery(String eventId) {
			URIBuilder b = new URIBuilder(BASE_URL + "Event");
			b.addParameter(FILTER, "eventID eq '"+eventId+"'");
			b.addParameter(SELECT, PARENT_EVENT_ID);
			return b.toString();
		}

		@Override
		public List<List<String>> get(String endpoint, int timestamp, String... select) {
			if (!open) closed();
			String query = buildQuery(endpoint, timestamp, select);
			List<List<String>> results = new ArrayList<>();
			parsePaginatedResults(results, query, select);
			return results;
		}

		private String buildQuery(String endpoint, int timestamp, String... select) {
			URIBuilder b = new URIBuilder(BASE_URL + endpoint);
			ZonedDateTime t = ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestamp), UTC);
			String formatted = t.format(DateTimeFormatter.ISO_INSTANT);
			b.addParameter(FILTER, "ValidFrom ge datetime'"+formatted+"'");
			b.addParameter(SELECT, Arrays.stream(select).collect(Collectors.joining(",")));
			return b.toString();
		}

		private void parsePaginatedResults(List<List<String>> results, String query, String... select) {
			JSONObject json = get(query);
			for (JSONObject row : json.getArray(VALUE).iterateAsObject()) {
				results.add(values(row, select));
			}
			if (json.hasKey(ODATA_NEXT_LINK)) {
				query = json.getString(ODATA_NEXT_LINK);
				parsePaginatedResults(results, query, select);
			}
		}

		private JSONObject get(String query) {
			try {
				HttpGet request = new HttpGet(query);
				statusReporter.setStatus(query);
				JSONObject json = client.contentAsJson(request);
				return json;
			} catch (Throwable t) {
				throw new SykeZoobenthosAPIException(query, t);
			}
		}

		private List<String> values(JSONObject row, String[] select) {
			List<String> values = new ArrayList<>();
			for (String field : select) {
				if (row.hasKey(field)) {
					String value = row.getString(field);
					if (value.isEmpty()) {
						values.add(null);
					} else {
						values.add(value);
					}
				} else {
					values.add(null);
				}
			}
			return values;
		}

		@Override
		public JSONObject getEvent(String eventId) throws SykeZoobenthosAPIException {
			if (!open) closed();
			String query = buildGetEventQuery(eventId, EVENT_ID, "BenthosStation,Occurrences,Measurements");
			JSONArray response = get(query).getArray(VALUE);
			if (response.isEmpty()) return null;
			return response.iterateAsObject().get(0);
		}

		private void closed() {
			throw new IllegalStateException("Closed");
		}

		@Override
		public List<JSONObject> getChildEvents(String eventId) throws SykeZoobenthosAPIException {
			if (!open) closed();
			String query = buildGetEventQuery(eventId, PARENT_EVENT_ID, "Occurrences,Measurements");
			return get(query).getArray(VALUE).iterateAsObject();
		}

		private String buildGetEventQuery(String eventId, String filter, String expand) {
			URIBuilder b = new URIBuilder(BASE_URL + "Event");
			b.addParameter(FILTER, filter +" eq '"+eventId+"'");
			b.addParameter(EXPAND, expand);
			return b.toString();
		}

	}

}
