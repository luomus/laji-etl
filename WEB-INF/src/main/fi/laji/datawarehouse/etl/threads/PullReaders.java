package fi.laji.datawarehouse.etl.threads;

import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;

public class PullReaders {

	public interface PullReaderGenerator {
		ETLThread generate();
	}

	private static final Map<Qname, PullReaderGenerator> generators = new HashMap<>();

	public static boolean definesPullReader(Qname source) {
		return generators.containsKey(source);
	}

	public static ETLThread getPullReader(Qname source) {
		return generators.get(source).generate();
	}

	public static void register(Qname source, PullReaderGenerator generator) {
		generators.put(source, generator);
	}

}
