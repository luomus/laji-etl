package fi.laji.datawarehouse.etl.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class TeamTextSearchUtil {

	public static List<String> combinations(String searchTerm) {
		if (searchTerm == null) return Collections.emptyList();
		searchTerm = clean(searchTerm);
		if (searchTerm.isEmpty()) return Collections.emptyList();
		List<String> parts = uniqueParts(searchTerm);

		List<String> combinations = new ArrayList<>();
		if (parts.size() == 1 || parts.size() > 3) {
			combinations.add(searchTerm);
			return combinations;
		}

		for (String s : parts) {
			combinations.addAll(combinations(s, parts));
		}
		
		return combinationsWithCommas(combinations);
	}

	private static List<String> combinationsWithCommas(List<String> combinations) {
		List<String> withCommas = new ArrayList<>();
		if (combinations.size() == 2) {
			for (String s : combinations) {
				withCommas.add(s);
				withCommas.add(s.replace(" ", ", "));
			}
			return withCommas;
		}
		
		for (String s : combinations) {
			withCommas.add(s);
			withCommas.add(s.replaceFirst(Pattern.quote(" "), ", "));
			withCommas.add(replaceLast(s, " ", ", "));
		}
		return withCommas;
	}

	private static String replaceLast(String string, String toReplace, String replacement) {
	    int pos = string.lastIndexOf(toReplace);
	    if (pos > -1) {
	        return string.substring(0, pos)
	             + replacement
	             + string.substring(pos + toReplace.length(), string.length());
	    }
		return string;
	}
	
	private static String clean(String searchTerm) {
		searchTerm = searchTerm.trim().toLowerCase().replace(",", "");
		while (searchTerm.contains("  ")) {
			searchTerm = searchTerm.replace("  ", " ");
		}
		return searchTerm;
	}

	private static Collection<String> combinations(String s, List<String> parts) {
		List<String> combinations = new ArrayList<>();
		List<String> other = new ArrayList<>(parts);
		other.remove(s);
		if (other.size() == 1) {
			String combination = s+" "+other.get(0);
			combinations.add(combination);
			return combinations;
		}

		for (String o : other) {
			for (String c : combinations(o, other)) {
				combinations.add(s +" "+c);
			}
		}
		return combinations;
	}

	private static List<String> uniqueParts(String searchTerm) {
		List<String> parts = parts(searchTerm);
		return unique(parts);
	}

	private static List<String> parts(String searchTerm) {
		List<String> parts = new ArrayList<>();
		for (String part : searchTerm.split(Pattern.quote(" "))) {
			parts.add(part);
		}
		return parts;
	}

	private static List<String> unique(List<String> parts) {
		if (parts.isEmpty() || parts.size() == 1) return parts;

		Set<String> uniqueValues = new HashSet<>(); 
		Iterator<String> i = parts.iterator();
		while (i.hasNext()) {
			String s = i.next();
			if (uniqueValues.contains(s)) {
				i.remove();
			} else {
				uniqueValues.add(s);
			}
		}
		return parts;
	}

}
