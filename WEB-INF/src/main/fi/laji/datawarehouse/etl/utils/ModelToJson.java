package fi.laji.datawarehouse.etl.utils;

import java.lang.reflect.Method;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationDwLinkings;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.LocalizedTexts;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Occurrences;
import fi.luomus.commons.taxonomy.RedListStatus;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class ModelToJson {

	public static final Set<String> INCLUDED_TAXON_FIELDS = Utils.set(
			"id",
			"checklist",
			"scientificName",
			"scientificNameAuthorship",
			"scientificNameDisplayName",
			"cursiveName",
			"finnish",
			"taxonRank",
			"vernacularName",
			"nameFinnish",
			"nameSwedish",
			"nameEnglish",
			"informalTaxonGroups",
			"taxonomicOrder",
			"latestRedListStatusFinland",
			"sensitive",
			"kingdomScientificName",
			"occurrenceCountFinland",
			"primaryHabitat",
			"administrativeStatuses",
			"threatenedStatus",
			"taxonConceptIds"
			);

	public static JSONObject toJson(Object o) {
		return toJson(o, null, null, true);
	}

	public static JSONObject toJson(Collection<?> c) {
		JSONObject json = new JSONObject();
		COLLECTION_SERIALIZER.setValue(json, "results", c, null, null, true);
		return json;
	}

	public static JSONObject rootToJson(DwRoot root) {
		return toJson(root, null, null, true).setString(Const.SCHEMA, Const.LAJI_ETL_SCHEMA);
	}

	public static JSONObject rowToJson(JoinedRow row) {
		return toSelectedJSON(row, null);
	}

	public static JSONObject toSelectedJSON(JoinedRow row, Selected selected) {
		Set<String> selectedFieldParts = selected == null ? null : selected.getSelectedFieldParts();

		JSONObject json = new JSONObject();

		if (row.getUnit() != null) {
			JSONObject unitJson = toJson(row.getUnit(), "unit", selectedFieldParts, false);
			if (!unitJson.isEmpty()) json.setObject("unit", unitJson);
		}

		if (row.getGathering() != null) {
			JSONObject gatheringJson = toJson(row.getGathering(), "gathering", selectedFieldParts, false);
			if (!gatheringJson.isEmpty()) json.setObject("gathering", gatheringJson);
		}

		JSONObject documentJson = toJson(row.getDocument(), "document", selectedFieldParts, false);
		if (!documentJson.isEmpty()) json.setObject("document", documentJson);

		if (row.getAnnotation() !=  null) {
			JSONObject annotationJson = toJson(row.getAnnotation(), "annotation", selectedFieldParts, false);
			if (!annotationJson.isEmpty()) json.setObject("annotation", annotationJson);
		}

		if (row.getMedia() != null) {
			JSONObject mediaJson = toJson(row.getMedia(), "media", selectedFieldParts, false);
			if (!mediaJson.isEmpty()) json.setObject("media", mediaJson);
		}

		if (row.getSample() != null) {
			JSONObject sampleJson = toJson(row.getSample(), "sample", selectedFieldParts, false);
			if (!sampleJson.isEmpty()) json.setObject("sample", sampleJson);
		}

		return json;
	}

	private static JSONObject toJson(Object o, String currentPath, Set<String> selectedFieldParts, boolean tree) {
		try {
			JSONObject json = new JSONObject();
			Map<String, Method> methods = ReflectionUtil.getGettersMap(o.getClass());
			for (Map.Entry<String, Method> e : methods.entrySet()) {
				String fieldName = e.getKey();
				Method method = e.getValue();
				if (o instanceof Taxon) {
					if (!INCLUDED_TAXON_FIELDS.contains(fieldName)) continue;
				}
				String newPath = currentPath == null ? fieldName : currentPath + "." + fieldName;
				if (selectedFieldParts != null && !selectedFieldParts.contains(newPath)) continue;
				if (selectedFieldParts == null && !tree) {
					if (fieldName.equals("gatherings")) continue;
					if (fieldName.equals("units")) continue;
				}
				Object value = method.invoke(o);
				if (value != null) {
					setValue(json, fieldName, value, newPath, selectedFieldParts, tree);
				}
			}
			return json;
		} catch (Exception e) {
			if (e instanceof ETLException) throw (ETLException) e;
			throw new ETLException(Utils.debugS(currentPath), e);
		}
	}

	private static void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
		JSONSerializer serializer = CLASS_SERIALIZERS.get(value.getClass());
		if (serializer == null) {
			if (value instanceof Enum) {
				ENUM_SERIALIZER.setValue(json, fieldName, value, currentPath, selectedFieldParts, tree);
			} else {
				throw new UnsupportedOperationException("Not yet implemented for " + value.getClass().getName() + " : " + fieldName);
			}
		} else {
			serializer.setValue(json, fieldName, value, currentPath, selectedFieldParts, tree);
		}
	}

	private interface JSONSerializer {
		void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) ;
	}

	private static class ToObjectSerializer implements JSONSerializer {
		@Override
		public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
			JSONObject jsonObject = toJson(value, currentPath, selectedFieldParts, tree);
			if (!jsonObject.isEmpty()) {
				json.setObject(fieldName, jsonObject);
			}
		}
	}

	private static final JSONSerializer ENUM_SERIALIZER = new JSONSerializer() {
		@Override
		public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
			json.setString(fieldName, value.toString());
		}
	};

	private static final JSONSerializer TO_OBJECT_SERIALIZER = new ToObjectSerializer();

	private static final JSONSerializer NOP_SERIALIZER = new JSONSerializer() {
		@Override
		public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {}
	};

	private static final JSONSerializer COLLECTION_SERIALIZER = new JSONSerializer() {
		@Override
		public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
			Collection<?> c = (Collection<?>) value;
			for (Object o : c) {
				if (o instanceof String) {
					json.getArray(fieldName).appendString(o.toString().trim());
				} else if (o instanceof Qname) {
					json.getArray(fieldName).appendString(((Qname)o).toURI());
				} else if (o.getClass().isEnum()) {
					json.getArray(fieldName).appendString(o.toString());
				} else {
					JSONObject jsonObject = toJson(o, currentPath, selectedFieldParts, tree);
					if (!jsonObject.isEmpty()) {
						json.getArray(fieldName).appendObject(jsonObject);
					}
				}
			}

		}
	};

	private static final JSONSerializer MAP_SERIALIZER = new JSONSerializer() {
		@Override
		public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
			HashMap<?, ?> map = (HashMap<?, ?>) value;
			if (map.isEmpty()) return;
			JSONObject serialized = new JSONObject();
			for (Map.Entry<?, ?> e : map.entrySet()) {
				ModelToJson.setValue(serialized, e.getKey().toString(), e.getValue(), currentPath, selectedFieldParts, tree);
			}
			json.setObject(fieldName, serialized);
		}
	};

	private static final Map<Class<?>, JSONSerializer> CLASS_SERIALIZERS = initClassSerializers();

	private static Map<Class<?>, JSONSerializer> initClassSerializers() {
		Map<Class<?>, JSONSerializer> map = new HashMap<>();

		map.put(IdentificationDwLinkings.class, TO_OBJECT_SERIALIZER);
		map.put(UnitDWLinkings.class, TO_OBJECT_SERIALIZER);
		map.put(GatheringDWLinkings.class, TO_OBJECT_SERIALIZER);
		map.put(DocumentDWLinkings.class, TO_OBJECT_SERIALIZER);
		map.put(GatheringDWLinkings.class, TO_OBJECT_SERIALIZER);
		map.put(GatheringInterpretations.class, TO_OBJECT_SERIALIZER);
		map.put(GatheringConversions.class, TO_OBJECT_SERIALIZER);
		map.put(UnitInterpretations.class, TO_OBJECT_SERIALIZER);
		map.put(DocumentQuality.class, TO_OBJECT_SERIALIZER);
		map.put(GatheringQuality.class, TO_OBJECT_SERIALIZER);
		map.put(UnitQuality.class, TO_OBJECT_SERIALIZER);
		map.put(Quality.class, TO_OBJECT_SERIALIZER);
		map.put(Coordinates.class, TO_OBJECT_SERIALIZER);
		map.put(SingleCoordinates.class, TO_OBJECT_SERIALIZER);
		map.put(DateRange.class, TO_OBJECT_SERIALIZER);
		map.put(MediaObject.class, TO_OBJECT_SERIALIZER);
		map.put(TaxonCensus.class, TO_OBJECT_SERIALIZER);
		map.put(RedListStatus.class, TO_OBJECT_SERIALIZER);
		map.put(NamedPlaceEntity.class, TO_OBJECT_SERIALIZER);
		map.put(Taxon.class, TO_OBJECT_SERIALIZER);
		map.put(HabitatObject.class, TO_OBJECT_SERIALIZER);
		map.put(Identification.class, TO_OBJECT_SERIALIZER);
		map.put(ObsCount.class, TO_OBJECT_SERIALIZER);
		map.put(OccurrenceAtTimeOfAnnotation.class, TO_OBJECT_SERIALIZER);
		map.put(ArrayList.class, COLLECTION_SERIALIZER);
		map.put(HashSet.class, COLLECTION_SERIALIZER);
		map.put(LinkedHashSet.class, COLLECTION_SERIALIZER);
		map.put(Collections.unmodifiableCollection(new ArrayList<>()).getClass(), COLLECTION_SERIALIZER);
		map.put(Collections.unmodifiableList(new ArrayList<>()).getClass(), COLLECTION_SERIALIZER);
		map.put(Collections.unmodifiableSet(new HashSet<>()).getClass(), COLLECTION_SERIALIZER);
		map.put(Collections.unmodifiableSet(new LinkedHashSet<>()).getClass(), COLLECTION_SERIALIZER);
		map.put(HashMap.class, MAP_SERIALIZER);
		map.put(LinkedHashMap.class, MAP_SERIALIZER);
		map.put(Collections.emptySet().getClass(), NOP_SERIALIZER);
		map.put(Collections.emptyList().getClass(), NOP_SERIALIZER);
		map.put(TreeMap.class, NOP_SERIALIZER);
		map.put(Content.class, NOP_SERIALIZER);
		map.put(Occurrences.class, NOP_SERIALIZER);
		map.put(URL.class, NOP_SERIALIZER);

		map.put(Geo.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setObject(fieldName, ((Geo)value).getGeoJSON());
			}
		});

		map.put(Qname.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				Qname qname =  (Qname) value;
				if (qname.isSet()) {
					json.setString(fieldName, qname.toURI());
				}
			}
		});

		map.put(Date.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				Date date = (Date) value;
				json.setString(fieldName, toIsoDate(date));
			}
			private String toIsoDate(Date date) {
				return DateUtils.format(date, "yyyy-MM-dd");
			}
		});

		map.put(Timestamp.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				Date date = (Date) value;
				json.setString(fieldName, toIsoDate(date));
			}
			private String toIsoDate(Date date) {
				return DateUtils.format(date, "yyyy-MM-dd");
			}
		});

		map.put(Integer.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setInteger(fieldName, (Integer) value);
			}
		});

		map.put(Long.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setInteger(fieldName, ((Long) value).intValue());
			}
		});

		map.put(Double.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setDouble(fieldName, (Double) value);
			}
		});

		map.put(String.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setString(fieldName, ((String) value).trim());
			}
		});

		map.put(Document.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				Document document = (Document) value;
				JSONObject documentJSON = toJson(document, currentPath, selectedFieldParts, tree);
				if (document.isPublic()) {
					json.setObject("publicDocument", documentJSON);
				} else {
					json.setObject("privateDocument", documentJSON);
				}
			}
		});

		map.put(Boolean.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setBoolean(fieldName, (Boolean) value);
			}
		});

		map.put(boolean.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				json.setBoolean(fieldName, (Boolean) value);
			}
		});

		map.put(LocalizedTexts.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				LocalizedTexts localizedTextss = (LocalizedTexts) value;
				if (localizedTextss.getAllTexts().isEmpty()) return;
				JSONObject serialized = new JSONObject();
				if (localizedTextss.hasTextForLocale("fi")) {
					COLLECTION_SERIALIZER.setValue(serialized, "fi", localizedTextss.forLocale("fi"), null, null, tree);
				}
				if (localizedTextss.hasTextForLocale("sv")) {
					COLLECTION_SERIALIZER.setValue(serialized, "sv", localizedTextss.forLocale("sv"), null, null, tree);
				}
				if (localizedTextss.hasTextForLocale("en")) {
					COLLECTION_SERIALIZER.setValue(serialized, "en", localizedTextss.forLocale("en"), null, null, tree);
				}
				json.setObject(fieldName, serialized);
			}
		});

		map.put(LocalizedText.class, new JSONSerializer() {
			@Override
			public void setValue(JSONObject json, String fieldName, Object value, String currentPath, Set<String> selectedFieldParts, boolean tree) {
				LocalizedText localizedText = (LocalizedText) value;
				if (localizedText.getAllTexts().isEmpty()) return;
				JSONObject serialized = new JSONObject();
				if (localizedText.hasTextForLocale("fi")) {
					serialized.setString("fi", localizedText.forLocale("fi"));
				}
				if (localizedText.hasTextForLocale("sv")) {
					serialized.setString("sv", localizedText.forLocale("sv"));
				}
				if (localizedText.hasTextForLocale("en")) {
					serialized.setString("en", localizedText.forLocale("en"));
				}
				json.setObject(fieldName, serialized);
			}
		});

		return map;
	}

}
