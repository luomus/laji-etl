package fi.laji.datawarehouse.etl.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.containers.rdf.Qname;

public class OldFinnishMunicipalities {

	private static final Map<Qname, Coordinates> COORDINATES;
	static {
		try {
			Map<Qname, Coordinates> map = new HashMap<>();
			map.put(new Qname("ML.719"), new Coordinates(60.72, 61.16, 28.70, 29.58, Type.WGS84)); // ML.719	Antrea
			map.put(new Qname("ML.737"), new Coordinates(60.42, 60.97, 28.53, 29.63, Type.WGS84)); // ML.737	Heinjoki
			map.put(new Qname("ML.740"), new Coordinates(60.92, 61.47, 29.11, 30.21, Type.WGS84)); // ML.740	Hiitola
			map.put(new Qname("ML.751"), new Coordinates(61.39, 61.94, 30.86, 31.96, Type.WGS84)); // ML.751	Impilahti
			map.put(new Qname("ML.755"), new Coordinates(60.28, 60.72, 28.27, 29.15, Type.WGS84)); // ML.755	Johannes
			map.put(new Qname("ML.779"), new Coordinates(60.67, 61.33, 29.09, 30.41, Type.WGS84)); // ML.779	Kaukola
			map.put(new Qname("ML.793"), new Coordinates(60.84, 61.39, 28.83, 29.93, Type.WGS84)); // ML.793	Kirvu
			map.put(new Qname("ML.798"), new Coordinates(60.25, 60.58, 28.30, 28.96, Type.WGS84)); // ML.798	Koivisto
			map.put(new Qname("ML.799"), new Coordinates(60.19, 60.63, 28.19, 29.07, Type.WGS84)); // ML.799	Koivisto Lk
			map.put(new Qname("ML.803"), new Coordinates(61.78, 62.88, 30.40, 32.60, Type.WGS84)); // ML.803	Korpiselkä
			map.put(new Qname("ML.811"), new Coordinates(60.08, 60.74, 28.25, 29.57, Type.WGS84)); // ML.811	Kuolemajärvi
			map.put(new Qname("ML.814"), new Coordinates(61.05, 61.60, 29.28, 30.38, Type.WGS84)); // ML.814	Kurkijoki
			map.put(new Qname("ML.821"), new Coordinates(60.78, 61.22, 29.64, 30.52, Type.WGS84)); // ML.821	Käkisalmi
			map.put(new Qname("ML.828"), new Coordinates(59.77, 60.21, 27.39, 28.27, Type.WGS84)); // ML.828	Lavansaari
			map.put(new Qname("ML.853"), new Coordinates(60.44, 60.88, 29.06, 29.94, Type.WGS84)); // ML.853	Muolaa
			map.put(new Qname("ML.879"), new Coordinates(67.86, 70.20, 28.37, 33.66, Type.WGS84)); // ML.879	Petsamo
			map.put(new Qname("ML.898"), new Coordinates(60.56, 61.00, 29.81, 30.69, Type.WGS84)); // ML.898	Pyhäjärvi Vpl
			map.put(new Qname("ML.915"), new Coordinates(61.60, 62.26, 29.92, 31.24, Type.WGS84)); // ML.915	Ruskeala
			map.put(new Qname("ML.918"), new Coordinates(60.58, 61.24, 29.08, 30.40, Type.WGS84)); // ML.918	Räisälä
			map.put(new Qname("ML.921"), new Coordinates(60.29, 60.84, 29.65, 30.75, Type.WGS84)); // ML.921	Sakkola
			map.put(new Qname("ML.922"), new Coordinates(60.81, 61.91, 30.76, 32.96, Type.WGS84)); // ML.922	Salmi
			map.put(new Qname("ML.931"), new Coordinates(61.67, 62.33, 30.34, 31.66, Type.WGS84)); // ML.931	Soanlahti
			map.put(new Qname("ML.933"), new Coordinates(61.21, 61.97, 29.94, 31.30, Type.WGS84)); // ML.933	Sortavala
			map.put(new Qname("ML.934"), new Coordinates(61.21, 61.97, 29.94, 31.30, Type.WGS84)); // ML.934	Sortavala Lk
			map.put(new Qname("ML.935"), new Coordinates(61.57, 62.23, 30.43, 31.75, Type.WGS84)); // ML.935	Suistamo
			map.put(new Qname("ML.939"), new Coordinates(61.46, 62.89, 30.97, 33.83, Type.WGS84)); // ML.939	Suojärvi
			map.put(new Qname("ML.944"), new Coordinates(59.94, 60.16, 26.78, 27.22, Type.WGS84)); // ML.944	Suursaari
			map.put(new Qname("ML.945"), new Coordinates(60.36, 60.80, 27.64, 28.52, Type.WGS84)); // ML.945	Säkkijärvi
			map.put(new Qname("ML.958"), new Coordinates(59.97, 60.41, 29.28, 30.16, Type.WGS84)); // ML.958	Terijoki
			map.put(new Qname("ML.966"), new Coordinates(59.60, 60.20, 26.50, 27.80, Type.WGS84)); // ML.966	Tytärsaari
			map.put(new Qname("ML.973"), new Coordinates(60.02, 60.64, 28.63, 29.86, Type.WGS84)); // ML.973	Uusikirkko
			map.put(new Qname("ML.975"), new Coordinates(60.47, 61.02, 27.78, 28.88, Type.WGS84)); // ML.975	Vahviala
			map.put(new Qname("ML.986"), new Coordinates(60.45, 60.90, 28.30, 29.22, Type.WGS84)); // ML.986	Viborg
			map.put(new Qname("ML.987"), new Coordinates(60.35, 61.01, 28.10, 29.42, Type.WGS84)); // ML.987	Viborg Lk
			map.put(new Qname("ML.991"), new Coordinates(60.59, 61.03, 29.14, 30.02, Type.WGS84)); // ML.991	Vuoksenranta
			map.put(new Qname("ML.1006"), new Coordinates(60.48, 60.92, 29.14, 30.02, Type.WGS84)); // ML.1006	Äyräpää
			map.put(new Qname("ML.1167"), new Coordinates(61.26, 62.14, 30.02, 31.96, Type.WGS84)); // ML.1167	Harlu
			map.put(new Qname("ML.1168"), new Coordinates(61.06, 61.84, 29.43, 31.25, Type.WGS84)); // ML.1168	Jaakkima
			map.put(new Qname("ML.1169"), new Coordinates(59.67, 60.67, 28.78, 30.82, Type.WGS84)); // ML.1169	Kanneljärvi
			map.put(new Qname("ML.1170"), new Coordinates(59.66, 60.39, 29.41, 30.85, Type.WGS84)); // ML.1170	Kivennapa
			map.put(new Qname("ML.1171"), new Coordinates(60.32, 61.19, 29.37, 30.99, Type.WGS84)); // ML.1171	Käkisalmi Mlk
			map.put(new Qname("ML.1172"), new Coordinates(61.16, 61.63, 29.79, 30.78, Type.WGS84)); // ML.1172	Lahdenpohja
			map.put(new Qname("ML.1174"), new Coordinates(60.97, 61.61, 29.56, 30.82, Type.WGS84)); // ML.1174	Lumivaara
			map.put(new Qname("ML.1175"), new Coordinates(60.19, 60.97, 29.98, 31.14, Type.WGS84)); // ML.1175	Metsäpirtti
			map.put(new Qname("ML.1176"), new Coordinates(60.72, 61.07, 28.12, 28.86, Type.WGS84)); // ML.1176	Nuijamaa (luovutttu)
			map.put(new Qname("ML.1177"), new Coordinates(61.31, 61.71, 29.21, 30.00, Type.WGS84)); // ML.1177	Parikkala (luovutettu)
			map.put(new Qname("ML.1179"), new Coordinates(60.03, 60.79, 29.41, 31.20, Type.WGS84)); // ML.1179	Rautu
			map.put(new Qname("ML.1180"), new Coordinates(61.08, 61.27, 28.89, 29.26, Type.WGS84)); // ML.1180	Ruokolahti (luovutettu)
			map.put(new Qname("ML.1181"), new Coordinates(61.49, 61.76, 29.51, 30.14, Type.WGS84)); // ML.1181	Saari (luovutettu)
			map.put(new Qname("ML.1182"), new Coordinates(59.91, 60.13, 28.17, 28.56, Type.WGS84)); // ML.1182	Seiskari
			map.put(new Qname("ML.1183"), new Coordinates(61.26, 61.49, 29.22, 29.70, Type.WGS84)); // ML.1183	Simpele (luovutettu)
			map.put(new Qname("ML.1184"), new Coordinates(61.44, 61.92, 29.48, 30.53, Type.WGS84)); // ML.1184	Uukuniemi (luovutettu)
			map.put(new Qname("ML.1185"), new Coordinates(59.95, 60.89, 29.16, 31.05, Type.WGS84)); // ML.1185	Valkjärvi
			map.put(new Qname("ML.1186"), new Coordinates(60.36, 60.67, 27.65, 60.67, Type.WGS84)); // ML.1186	Virolahti (luovutettu)
			map.put(new Qname("ML.1187"), new Coordinates(60.09, 61.03, 28.57, 30.96, Type.WGS84)); // ML.1187	Vuoksela
			map.put(new Qname("ML.1188"), new Coordinates(60.58, 60.96, 27.92, 28.66, Type.WGS84)); // ML.1188	Ylämaa (luovutettu)
			map.put(new Qname("ML.1189"), new Coordinates(61.66, 63.56, 30.51, 33.26, Type.WGS84)); // ML.1189	Ilomantsi (luovutettu)
			map.put(new Qname("ML.1190"), new Coordinates(61.70, 62.38, 29.94, 31.14, Type.WGS84)); // ML.1190	Kitee (luovutettu)
			map.put(new Qname("ML.1191"), new Coordinates(61.71, 62.65, 30.01, 32.13, Type.WGS84)); // ML.1191	Pälkjärvi
			map.put(new Qname("ML.1192"), new Coordinates(61.60, 62.72, 29.89, 31.34, Type.WGS84)); // ML.1192	Tohmajärvi (luovutettu)
			map.put(new Qname("ML.1193"), new Coordinates(61.92, 62.39, 30.27, 31.48, Type.WGS84)); // ML.1193	Värtsilä (luovutettu)
			map.put(new Qname("ML.1194"), new Coordinates(65.13, 68.04, 28.35, 33.02, Type.WGS84)); // ML.1194	Kuusamo (luovutettu)
			map.put(new Qname("ML.1196"), new Coordinates(65.13, 68.04, 28.35, 33.02, Type.WGS84)); // ML.1196	Salla (luovutettu)
			map.put(new Qname("ML.1195"), new Coordinates(68.210, 70.34, 27.58, 33.69, Type.WGS84)); // ML.1195	Inari (luovutettu)
			map.put(new Qname("ML.1197"), new Coordinates(60.61, 61.35, 28.29, 29.67, Type.WGS84)); // ML.1197	Jääski (luovutettu)
			COORDINATES = map;
		} catch (DataValidationException e) {
			throw new ETLException(e);
		}
	}

	public static Coordinates getCoordinates(Qname id) {
		return COORDINATES.get(id);
	}

	public static Set<Qname> getMunicipalities(Coordinates wgs84Coordinates) {
		if (wgs84Coordinates == null) throw new IllegalArgumentException("Null coordinates");
		if (wgs84Coordinates.getType() != Type.WGS84) throw new IllegalArgumentException("Not wgs84 coordinates");
		Set<Qname> ids = new HashSet<>();
		for (Map.Entry<Qname, Coordinates> e : COORDINATES.entrySet()) {
			Qname id = e.getKey();
			Coordinates municipalityCoords = e.getValue();
			if (isClose(wgs84Coordinates, municipalityCoords)) {
				ids.add(id);
			}
		}
		return ids;
	}

	private static boolean isClose(Coordinates c1, Coordinates c2) {
		return
				c1.getLatMin() <= c2.getLatMax() &&
				c1.getLatMax() >= c2.getLatMin() &&
				c1.getLonMin() <= c2.getLonMax() &&
				c1.getLonMax() >= c2.getLonMin();
	}



}
