package fi.laji.datawarehouse.etl.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class Util {

	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	private static final TimeZone GMT_TIMEZONE = TimeZone.getTimeZone("GMT");

	private static final SecureRandom RANDOM = new SecureRandom();

	public static Long generateKey() {
		long next = RANDOM.nextLong();
		return next;
	}

	public static String getHash(String s) {
		byte[] bytes;
		try {
			bytes = s.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		byte[] raw = md.digest(bytes);
		return Base64.getEncoder().encodeToString(raw);
	}

	public static String trimToByteLength(String s, int size) {
		if (s == null) return null;
		s = s.trim();
		if (s.isEmpty()) return null;
		byte[] utf8 = toUTF8Bytes(s);
		while (utf8.length > size) {
			s = s.substring(0, s.length() - 10);
			utf8 = toUTF8Bytes(s);
		}
		return s;
	}

	private static byte[] toUTF8Bytes(String s) {
		try {
			return s.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
	}

	public static Qname secureIndividualId(Qname individualId, String secretSalt) {
		try {
			String id = individualId.toURI() + secretSalt;
			byte[] digest = MessageDigest.getInstance("MD5").digest(id.getBytes(StandardCharsets.UTF_8));
			String hex = (new HexBinaryAdapter()).marshal(digest);
			return new Qname("A." + hex);
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	public static enum DateValidateMode { STRICT, ALLOW_YEAR_END_FUTURE, ALLOW_ALL_FUTURE }

	public static void validate(Date date, String field, DateValidateMode mode) throws DateValidationException {
		if (date == null) return;
		if (mode == null) mode = DateValidateMode.STRICT;

		Calendar cal = Calendar.getInstance();
		cal.setLenient(false);
		cal.setTime(date);
		try {
			cal.getTime();
		}
		catch (Exception e) {
			throw new DateValidationException(Quality.Issue.INVALID_DATE, "The date " + DateUtils.format(date, "yyyy-MM-dd") + " is not a valid calendar date.");
		}

		if (mode == DateValidateMode.STRICT) {
			Date endOfThisDay = getEndOfTomorrow();
			if (date.after(endOfThisDay)) {
				String message = ReflectionUtil.reCaseName(field) + " is in the future: " + DateUtils.format(date, "yyyy-MM-dd");
				throw new DateValidationException(Quality.Issue.DATE_IN_FUTURE, message);
			}
		} else if (mode == DateValidateMode.ALLOW_YEAR_END_FUTURE) {
			Date endOfThisYear = getEndOfThisYear();
			if (date.after(endOfThisYear)) {
				String message = ReflectionUtil.reCaseName(field) + " is in the future: " + DateUtils.format(date, "yyyy-MM-dd");
				throw new DateValidationException(Quality.Issue.DATE_IN_FUTURE, message);
			}
		}

		if (date.before(DateRange.oldestAllowedDate())) {
			String message = ReflectionUtil.reCaseName(field) + " is too far in the past: " + DateUtils.format(date, "yyyy-MM-dd");
			throw new DateValidationException(Quality.Issue.DATE_TOO_FAR_IN_THE_PAST, message);
		}
	}

	public static void validateHour(Integer hour, String field) throws DateValidationException {
		if (hour == null) return;
		if (hour < 0) throw new DateValidationException(Quality.Issue.INVALID_HOUR, ReflectionUtil.reCaseName(field) + " is not a valid hour: " + hour);
		if (hour > 23) throw new DateValidationException(Quality.Issue.INVALID_HOUR, ReflectionUtil.reCaseName(field) + " is not a valid hour: " + hour);
	}

	public static void validateMinutes(Integer minutes, String field) throws DateValidationException {
		if (minutes == null) return;
		if (minutes < 0) throw new DateValidationException(Quality.Issue.INVALID_MINUTE, ReflectionUtil.reCaseName(field) + " is not a valid minute: " + minutes);
		if (minutes > 59) throw new DateValidationException(Quality.Issue.INVALID_MINUTE, ReflectionUtil.reCaseName(field) + " is not a valid minute: " + minutes);
	}

	private static Date getEndOfTomorrow() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		Date endOfTomorrow = c.getTime();
		return endOfTomorrow;
	}

	private static Date getEndOfThisYear() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, Calendar.DECEMBER);
		c.set(Calendar.DAY_OF_MONTH, 31);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		Date endOfThisDay = c.getTime();
		return endOfThisDay;
	}

	private static final String VALID_CHARS_STRING = "abcdefghijklmnopqrstuvwxyzåäö0123456789.-_/#:";

	private static final Set<Character> VALID_QNAME_CHARACTERS = (VALID_CHARS_STRING + VALID_CHARS_STRING.toUpperCase()).chars().mapToObj(e->(char)e).collect(Collectors.toSet());

	public static void validateId(Qname id, String field) throws CriticalParseFailure {
		if (id == null || !id.isSet()) throw new CriticalParseFailure("Id for " + field + " is not set");
		if (id.toURI().length() > 200) throw new CriticalParseFailure("Too long URI Identifier " + id.toURI());
	}

	public static void validateIncomingDocumentId(Qname documentId) throws CriticalParseFailure {
		validateIncomingId(documentId, "documentId");
		if (documentId.toString().contains("#")) throw new CriticalParseFailure("Document IDs must not contain # : " + documentId);
	}

	public static void validateIncomingId(Qname id, String field) throws CriticalParseFailure {
		validateId(id, field);
		for (char c : id.toString().toCharArray()) {
			if (!VALID_QNAME_CHARACTERS.contains(c)) {
				throw new CriticalParseFailure("Invalid characters " + id.toURI());
			}
		}
	}

	public static String catenate(List<String> strings, String delimeter) {
		if (strings == null || strings.isEmpty()) return null;
		StringBuilder b = new StringBuilder();
		Iterator<String> i = strings.iterator();
		while (i.hasNext()) {
			String s = i.next();
			b.append(s);
			if (i.hasNext()) b.append(delimeter).append(" ");
		}
		return b.toString();

	}

	public static String catenate(List<String> strings) {
		return catenate(strings, ";");
	}

	private static final Collection<Integer> ALL_MONTHS = Utils.list(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);

	public static Collection<Integer> getMonths(DateRange eventDate) {
		if (eventDate == null) return ALL_MONTHS;
		if (eventDate.getBegin() == null) return ALL_MONTHS;
		Set<Integer> monthNumbers = new TreeSet<>();

		LocalDate currentMonth = YearMonth.from(eventDate.getBegin().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).atDay(1);
		LocalDate endDate = eventDate.getEnd().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		while (!currentMonth.isAfter(endDate)) {
			monthNumbers.add(currentMonth.getMonthValue());
			currentMonth = currentMonth.plusMonths(1);
		}
		return monthNumbers;
	}

	public static long randomExpire(int fromMinutes, int toMinutes) {
		long currentTimestamp = DateUtils.getCurrentEpoch();
		long random = ThreadLocalRandom.current().nextLong(fromMinutes*60, toMinutes*60);
		return currentTimestamp + random;
	}

	public static String getDisplayDateTime(DateRange eventDate, Integer hourBegin, Integer minutesBegin, Integer hourEnd, Integer minutesEnd) {
		if (eventDate == null) return null;

		String beginDate = getDisplayDate(eventDate.getBegin());
		String beginTime = getDisplayTime(hourBegin, minutesBegin);
		String endDate = getDisplayDate(eventDate.getEnd());
		String endTime = getDisplayTime(hourEnd, minutesEnd);

		if (!given(beginDate) && !given(endDate)) return null;

		if (beginDate.equals(endDate)) {
			if (given(beginTime) && given(endTime)) {
				return beginDate + " [" + beginTime + "-" + endTime + "]";
			}
			return getDisplayDateTime(beginDate, beginTime);
		}

		String begin = getDisplayDateTime(beginDate, beginTime);
		String end = getDisplayDateTime(endDate, endTime);
		return begin + " - " + end;
	}

	private static String getDisplayDateTime(String date, String time) {
		if (given(time)) return date + " [" + time + "]";
		return date;
	}

	private static String getDisplayDate(Date date) {
		if (date == null) return "";
		return DateUtils.format(date, "yyyy-MM-dd");
	}

	private static String getDisplayTime(Integer hour, Integer minutes) {
		if (hour == null) return "";
		if (minutes == null) return hour.toString();
		return hour + ":" + getDisplayMinutes(minutes);
	}

	private static String getDisplayMinutes(Integer minutes) {
		String s = minutes.toString();
		if (s.length() == 1) return "0" + s;
		return s;
	}

	private static Map<String, String> areaDescriptions = null;

	public static Map<String, String> getAreaDescriptions(Collection<Area> areas, boolean refresh) {
		if (areaDescriptions == null || refresh) {
			areaDescriptions = new HashMap<>();
			for (Area area : areas) {
				areaDescriptions.put(area.getQname().toURI(), getAreaName(area));
			}
		}
		return areaDescriptions;
	}

	private static String getAreaName(Area area) {
		String name = area.getName().forLocale("fi");
		String abbr = area.getAbbreviation();
		if (given(abbr)) {
			name += " (" + abbr + ")";
		}
		return name;
	}

	public static double[] parseNumericRange(String value) {
		if (value == null) return null;
		if (!value.contains("/")) return null;
		String[] parts = value.split(Pattern.quote("/"), -1);
		if (parts.length != 2) return null;
		try {
			String minS = parts[0];
			String maxS = parts[1];
			double min = minS.isEmpty() ? -1* Double.MAX_VALUE : Double.valueOf(parts[0]);
			double max = maxS.isEmpty() ? Double.MAX_VALUE : Double.valueOf(parts[1]);
			return new double[] {min, max};
		} catch (Exception e) {
			return null;
		}
	}

	public static String formatToIsoDateWithTimezone(long epochSeconds) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date(epochSeconds*1000));
		c.setTimeZone(GMT_TIMEZONE);
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(GMT_TIMEZONE);
		return sdf.format(c.getTime())+"+00:00";
	}

	public static Date toDate(long epochSeconds) {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(GMT_TIMEZONE);
		c.setTime(new Date(epochSeconds*1000));
		return c.getTime();
	}

	public static Long toTimestamp(String isoDateTimeWithTimeZone) throws DataValidationException {
		if (!given(isoDateTimeWithTimeZone)) {
			return now();
		}
		try {
			// 2017-07-26T18:51:53+00:00
			// 2017-07-26T18:51:53+03:00
			int utzPlusHours = parseUtzPlusHours(isoDateTimeWithTimeZone);
			String utz = parseUtz(isoDateTimeWithTimeZone);
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			sdf.setTimeZone(GMT_TIMEZONE);
			Date date = sdf.parse(utz);
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.setTimeZone(GMT_TIMEZONE);
			c.add(Calendar.HOUR, -1*utzPlusHours);
			return c.getTimeInMillis() / 1000;
		} catch (Exception e) {
			throw new DataValidationException("Invalid date " + isoDateTimeWithTimeZone, e);
		}
	}

	private static String parseUtz(String created) {
		// 2017-07-26T18:51:53+00:00
		// 2017-07-26T18:51:53+03:00
		if (!created.contains("+")) return created;
		return created.split(Pattern.quote("+"))[0];
	}

	private static int parseUtzPlusHours(String created) {
		// 2017-07-26T18:51:53+00:00
		// 2017-07-26T18:51:53+03:00
		if (!created.contains("+")) return 0;
		String part = created.split(Pattern.quote("+"))[1];
		if (!part.contains(":")) return 0;
		part = part.split(Pattern.quote(":"))[0];
		return Integer.valueOf(part);
	}

	public static long now() {
		return DateUtils.getCurrentEpoch();
	}

	private static boolean given(String s) {
		return s != null && !s.trim().isEmpty();
	}

	private static class Batch {
		int unitCount = 0;
		List<Document> documents = new ArrayList<>();
		public void add(Document d, int unitCount) {
			documents.add(d);
			this.unitCount += unitCount;
		}
	}

	public static List<List<Document>> batch(List<Document> documents, int size) {
		if (documents.isEmpty()) return Collections.emptyList();

		if (countOfUnits(documents) <= size) {
			List<List<Document>> batches = new ArrayList<>();
			batches.add(documents);
			return batches;
		}

		List<Batch> batches = new ArrayList<>();

		docs: for (Document d : documents) {
			int c = countOfUnits(d);
			if (c > size) {
				newBatch(batches, d, c);
				continue;
			}
			for (Batch b : batches) {
				if (b.unitCount + c <= size) {
					b.add(d, c);
					continue docs;
				}
			}
			newBatch(batches, d, c);
		}

		return toList(batches);
	}

	private static List<List<Document>> toList(List<Batch> batches) {
		return batches.stream().map(b->b.documents).collect(Collectors.toList());
	}

	private static void newBatch(List<Batch> batches, Document d, int c) {
		Batch b = new Batch();
		b.add(d, c);
		batches.add(b);
	}


	public static int countOfUnits(List<Document> documents) {
		int c = 0;
		for (Document d : documents) {
			c += countOfUnits(d);
		}
		return c;
	}

	public static int countOfUnits(Document d) {
		int c = 0;
		for (Gathering g : d.getGatherings()) {
			c += g.getUnits().size();
		}
		return c;
	}

	public static String toString(Enum<?>[] e) {
		return Stream.of(e).map(t->t.name()).collect(Collectors.joining( ","));
	}

}
