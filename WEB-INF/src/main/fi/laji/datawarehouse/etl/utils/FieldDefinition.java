package fi.laji.datawarehouse.etl.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FieldDefinition {

	boolean ignoreForETL() default false;

	boolean ignoreForQuery() default false;

	double order() default Double.MAX_VALUE;

}
