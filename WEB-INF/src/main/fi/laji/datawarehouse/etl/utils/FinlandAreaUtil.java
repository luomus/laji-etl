package fi.laji.datawarehouse.etl.utils;

import java.awt.Polygon;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;

public class FinlandAreaUtil {

	/**
	 * This is the area inside which we accept coordinates to be in "Finland" -- anything outside this area is most likely a typo or other error in the data that should be caught in validations.
	 * Java doesn't have double polygon so we do this by multiplying coordinates by 100. So 59.51 ~ 5951.
	 */
	private static final Polygon GENERAL_FINLAND_AREA = initGeneralFinlandAreaPolygon();

	private static final int FIRST_LON = 2029;
	private static final int FIRST_LAT = 5941;

	private static Polygon initGeneralFinlandAreaPolygon() {
		Polygon generalFinlandArea = new Polygon();
		generalFinlandArea.addPoint(FIRST_LAT, FIRST_LON);
		generalFinlandArea.addPoint(6013, 1899);
		generalFinlandArea.addPoint(6301, 1881);
		generalFinlandArea.addPoint(6543, 2252);
		generalFinlandArea.addPoint(6787, 2276);
		generalFinlandArea.addPoint(6903, 1956);
		generalFinlandArea.addPoint(6959, 2148);
		generalFinlandArea.addPoint(7032, 2788);
		generalFinlandArea.addPoint(6994, 3211);
		generalFinlandArea.addPoint(6934, 3349);
		generalFinlandArea.addPoint(6512, 3517);
		generalFinlandArea.addPoint(6088, 3571);
		generalFinlandArea.addPoint(5982, 3021);
		generalFinlandArea.addPoint(5950, 2346);
		generalFinlandArea.addPoint(FIRST_LAT, FIRST_LON);
		return generalFinlandArea;
	}

	public static boolean isInsideGeneralFinlandArea(double lat, double lon) {
		return GENERAL_FINLAND_AREA.contains(lat*100, lon*100);
	}

	public static boolean isInsideGeneralFinlandArea(Coordinates wgs84Coordinates) {
		double latmin = wgs84Coordinates.getLatMin()*100; // Java doesn't have double polygon so Finland area is in wgs84 decimals turned into ints (multiplied by 100)
		double latmax = wgs84Coordinates.getLatMax()*100;
		double lonmin = wgs84Coordinates.getLonMin()*100;
		double lonmax = wgs84Coordinates.getLonMax()*100;
		if (GENERAL_FINLAND_AREA.contains(latmin, lonmin)) return true;
		if (GENERAL_FINLAND_AREA.contains(latmin, lonmax)) return true;
		if (GENERAL_FINLAND_AREA.contains(latmax, lonmin)) return true;
		if (GENERAL_FINLAND_AREA.contains(latmax, lonmax)) return true;
		return false;
	}

}
