package fi.laji.datawarehouse.etl.utils;

public class ThreadStatusReporter {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}