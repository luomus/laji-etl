package fi.laji.datawarehouse.etl.utils;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;

public class ConvertedCoordinates {

	private Coordinates wgs84;
	private Coordinates ykj;
	private Coordinates euref;
	public Coordinates getWgs84() {
		return wgs84;
	}
	public void setWgs84(Coordinates wgs84) {
		this.wgs84 = wgs84;
	}
	public Coordinates getYkj() {
		return ykj;
	}
	public void setYkj(Coordinates ykj) {
		this.ykj = ykj;
	}
	public Coordinates getEuref() {
		return euref;
	}
	public void setEuref(Coordinates euref) {
		this.euref = euref;
	}
	
}
