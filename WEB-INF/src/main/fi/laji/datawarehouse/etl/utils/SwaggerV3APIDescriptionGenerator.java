package fi.laji.datawarehouse.etl.utils;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationDwLinkings;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Person;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.laji.datawarehouse.query.download.service.PyhaApprovedAPI;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.FilterDefinition.Type;
import fi.laji.datawarehouse.query.model.FilterInfo;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.Partition;
import fi.laji.datawarehouse.query.model.PolygonIdSearch;
import fi.laji.datawarehouse.query.model.PolygonSearch;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.PageableBaseQuery;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.laji.datawarehouse.query.service.annotation.AnnotationListQueryAPI;
import fi.laji.datawarehouse.query.service.sample.SampleListQueryAPI;
import fi.laji.datawarehouse.query.service.unit.UnitListQueryAPI;
import fi.laji.datawarehouse.query.service.unitMedia.UnitMediaListQueryAPI;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.RedListStatus;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class SwaggerV3APIDescriptionGenerator {

	private static final String WKT = "wkt";
	private static final String APPLICATION_WKT = "application/wkt";

	private static final String TSV = "tsv";
	private static final String TEXT_TSV = "text/tab-separated-values";

	private static final String CSV = "csv";
	private static final String TEXT_CSV = "text/csv";

	private static final String PLAIN = "plain";
	private static final String TEXT_PLAIN = "text/plain";

	private static final String RDF_XML = "rdf_xml";
	private static final String APPLICATION_RDF_XML = "application/rdf+xml";

	private static final String XML = "xml";
	private static final String APPLICATION_XML = "application/xml";

	private static final String GEOJSON = "geojson";
	private static final String APPLICATION_GEO_JSON = "application/geo+json";

	private static final String JSON = "json";
	private static final String APPLICATION_JSON = "application/json";

	private static final String POST = "post";
	private static final String DELETE = "delete";
	private static final String GET = "get";
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String NUMBER = "number";
	private static final String ERROR_REF = "DwError";
	private static final String DW_SINGLE = "DwSingle_";
	private static final String DW_ETL = "DwETL_";
	private static final String DW_QUERY = "DwQuery_";
	private static final String REF = "$ref";
	private static final String RESPONSES = "responses";
	private static final String CONTENT = "content";
	private static final String FORMAT = "format";
	private static final String OBJECT = "object";
	private static final String PROPERTIES = "properties";
	private static final String PATH = "path";
	private static final String TAGS = "tags";
	private static final String WAREWHOUSE_TAG = "Warehouse";
	private static final String OR_FILTER = "When multiple values are given, this is an OR search.";
	public static final String CRS_DESC = "(WGS84 = EPSG:4326; EUREF = ETRS-TM35FIN EPSG:3067; YKJ = EPSG:2393)";
	private static final String STRING = "string";
	private static final String ITEMS = "items";
	private static final String MAXIMUM = "maximum";
	private static final String MINIMUM = "minimum";
	private static final String INTEGER = "integer";
	private static final String BOOLEAN = "boolean";
	private static final String DEFAULT = "default";
	private static final String ARRAY = "array";
	private static final String ENUM = "enum";
	private static final String QUERY = "query";
	private static final String TYPE = "type";
	private static final String SCHEMA = "schema";
	private static final String REQUIRED = "required";
	private static final String IN = "in";
	private static final String NAME = "name";
	private static final String PARAMETERS = "parameters";

	private static final String DESCRIPTION = "description";
	private static final String SUMMARY = "summary";

	private static enum Ref { ETL, SINGLE, QUERY }

	public static JSONObject generateSwaggerDescription(Config config) throws Exception {
		JSONObject response = new JSONObject();
		response.setString("openapi", "3.1.0");
		response.setObject("info", info());
		response.getArray("servers").appendObject(new JSONObject().setString("url", "https://" + config.get("Swagger_RealBaseHost") + config.get("Swagger_RealBasePath")));
		response.setObject("paths", paths());
		response.getObject("components").setObject("schemas", componentSchemas());
		response.setArray(TAGS, new JSONArray().appendObject(new JSONObject().setString(NAME, WAREWHOUSE_TAG)));
		return response;
	}

	private static JSONObject info() {
		JSONObject info = new JSONObject();
		info.setString("title", WAREWHOUSE_TAG);
		info.getObject("contact").setString("email", "helpdesk@laji.fi");
		info.getObject("contact").setString("url", "https://bitbucket.org/luomus/laji-etl/issues");
		info.getObject("license").setString(NAME, "The MIT License (MIT)");
		info.getObject("license").setString("url", "https://opensource.org/licenses/MIT");
		info.setString("version", "beta");
		return info;
	}

	private static JSONObject paths() {
		JSONObject paths = new JSONObject();
		paths.getObject("/push").setObject(POST, pushApi());
		paths.getObject("/push").setObject(DELETE, pushApiDelete());

		paths.getObject("/query/document").setObject(GET, singleApi());
		paths.getObject("/query/document/aggregate").setObject(GET, aggregateApi(Base.DOCUMENT, Concealment.PUBLIC));
		paths.getObject("/query/gathering/aggregate").setObject(GET, aggregateApi(Base.GATHERING, Concealment.PUBLIC));
		paths.getObject("/query/gathering/statistics").setObject(GET, statisticsApi(Base.GATHERING, Concealment.PUBLIC));
		paths.getObject("/query/unit/count").setObject(GET, countApi(Base.UNIT, Concealment.PUBLIC));
		paths.getObject("/query/unit/list").setObject(GET, listApi(Base.UNIT, UnitListQueryAPI.DEFAULT_SELECT, UnitListQueryAPI.DEFAULT_ORDER_BY, Concealment.PUBLIC));
		paths.getObject("/query/unit/aggregate").setObject(GET, aggregateApi(Base.UNIT, Concealment.PUBLIC));
		paths.getObject("/query/unit/statistics").setObject(GET, statisticsApi(Base.UNIT, Concealment.PUBLIC));
		paths.getObject("/query/annotation/list").setObject(GET, listApi(Base.ANNOTATION, AnnotationListQueryAPI.DEFAULT_SELECT, AnnotationListQueryAPI.DEFAULT_ORDER_BY, Concealment.PUBLIC));
		paths.getObject("/query/unitMedia/list").setObject(GET, listApi(Base.UNIT_MEDIA, UnitMediaListQueryAPI.DEFAULT_SELECT, UnitMediaListQueryAPI.DEFAULT_ORDER_BY, Concealment.PUBLIC));
		paths.getObject("/query/sample/list").setObject(GET, listApi(Base.SAMPLE, SampleListQueryAPI.DEFAULT_SELECT, SampleListQueryAPI.DEFAULT_ORDER_BY, Concealment.PUBLIC));
		paths.getObject("/query/annotation/aggregate").setObject(GET, aggregateApi(Base.ANNOTATION, Concealment.PUBLIC));

		// Removed to reduce the size of the document; private-queries are almost exact duplicates of the public ones and open api users rarely need to use the private versions
		//		paths.getObject("/private-query/unit/count").setObject("get", countApi(Base.UNIT, Concealment.PRIVATE));
		// ...

		paths.getObject("/enumeration-labels").setObject(GET, getEnumerationLabelsApi());
		paths.getObject("/enumeration-labels/{enumeration}").setObject(GET, getEnumerationLabelApi());

		paths.getObject("/filters").setObject(GET, getFilterDescriptionsApi());
		paths.getObject("/filters/{filter}").setObject(GET, getFilterDescriptionApi());

		paths.getObject("/polygon").setObject(POST, postPolygonApi());
		paths.getObject("/polygon/{id}").setObject(GET, getPolygonApi());

		// Removed to reduce the size of the document
		//      paths.getObject("/query/download").setObject("post", downloadApi());
		//		paths.getObject("/private-query/downloadApprovalRequest").setObject("post", privateDownloadApprovalRequestApi());
		//		paths.getObject("/private-query/downloadApproved").setObject("post", privateDownloadApprovedApi());
		return paths;
	}

	private static JSONObject componentSchemas() throws Exception {
		JSONObject schemas = new JSONObject();
		for (Class<?> definitionClass : QUERY_RESPONSE_CLASSES) {
			schemas.setObject(DW_QUERY+definitionClass.getSimpleName(), schema(definitionClass, Ref.QUERY));
		}
		for (Class<?> definitionClass : ETL_CLASSES) {
			schemas.setObject(DW_ETL+definitionClass.getSimpleName(), schema(definitionClass, Ref.ETL));
		}
		for (Class<?> definitionClass : SINGLE_CLASSES) {
			schemas.setObject(DW_SINGLE+definitionClass.getSimpleName(), schema(definitionClass, Ref.SINGLE));
		}
		schemas.setObject(DW_QUERY+Taxon.class.getSimpleName(), taxonDefinition(Ref.QUERY));
		schemas.setObject(DW_SINGLE+Taxon.class.getSimpleName(), taxonDefinition(Ref.SINGLE));
		schemas.getObject(DW_QUERY+AggregateRow.class.getSimpleName()).getObject(PROPERTIES).getObject(Const.AGGREGATE_BY).setString(TYPE, OBJECT);
		schemas.getObject(ERROR_REF).getObject(PROPERTIES).getObject("status").setString(TYPE, INTEGER);
		schemas.getObject(ERROR_REF).getObject(PROPERTIES).getObject("message").setString(TYPE, STRING);
		return schemas;
	}

	private static JSONObject taxonDefinition(Ref ref) throws NoSuchMethodException {
		JSONObject json = new JSONObject();
		for (String field : ModelToJson.INCLUDED_TAXON_FIELDS) {
			json.getObject(PROPERTIES).setObject(field, property(ReflectionUtil.getGetter(field, Taxon.class), ref));
		}
		return json;
	}


	private static JSONObject schema(Class<?> c, Ref ref) {
		return new JSONObject().setString(TYPE, OBJECT).setObject(PROPERTIES, properties(c, ref));
	}

	private static JSONObject properties(Class<?> c, Ref ref) {
		JSONObject properties = new JSONObject();
		for (Method getter : sort(ReflectionUtil.getGetters(c))) {
			if (skip(ref, getter)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(getter);
			properties.setObject(fieldName, property(getter, ref));
		}
		return properties;
	}

	private static boolean skip(Ref ref, Method getter) {
		if (ref == Ref.ETL && annotatedEtlIgnore(getter)) return true;
		if (ref == Ref.QUERY && annotatedQueryIgnore(getter)) return true;
		if (ref == Ref.SINGLE && annotatedSingleIgnore(getter)) return true;
		return false;
	}

	private static boolean annotatedSingleIgnore(Method getter) {
		if (getter.getName().equals("getGatherings")) return false;
		if (getter.getName().equals("getUnits")) return false;
		return annotatedQueryIgnore(getter);
	}

	private static boolean annotatedQueryIgnore(Method getter) {
		FieldDefinition a = getter.getAnnotation(FieldDefinition.class);
		if (a == null) return false;
		return a.ignoreForQuery();
	}

	private static boolean annotatedEtlIgnore(Method getter) {
		FieldDefinition a = getter.getAnnotation(FieldDefinition.class);
		if (a == null) return false;
		return a.ignoreForETL();
	}

	private static List<Method> sort(Collection<Method> getters) {
		List<Method> list = new ArrayList<>(getters);
		Collections.sort(list, new Comparator<Method>() {
			@Override
			public int compare(Method m1, Method m2) {
				Double order1 = getOrder(m1);
				Double order2 = getOrder(m2);
				int c = order1.compareTo(order2);
				if (c != 0) return c;
				return ReflectionUtil.cleanGetterFieldName(m1).compareTo(ReflectionUtil.cleanGetterFieldName(m2));
			}

			private Double getOrder(Method m) {
				FieldDefinition a1 = m.getAnnotation(FieldDefinition.class);
				FileDownloadField a2 = m.getAnnotation(FileDownloadField.class);
				double o1 = a1 != null ? a1.order() : Double.MAX_VALUE;
				double o2 = a2 != null ? a2.order() : Double.MAX_VALUE;
				return Math.min(o1, o2);
			}
		});
		return list;
	}

	private static JSONObject property(Method getter, Ref ref) {
		JSONObject property = new JSONObject();
		if (Collection.class.isAssignableFrom(getter.getReturnType())) {
			property.setString(TYPE, ARRAY);
			property.setObject(ITEMS, items(getter, ref));
			return property;
		}
		String type = getType(getter.getReturnType());
		property.setString(TYPE, type);
		if (type == OBJECT) {
			if (getter.getReturnType() == Geo.class) {
				property.getObject("externalDocs").setString("url", "http://geojson.org/geojson-spec.html#geometry-objects");
				property.setString("description",
						"GeoJSON object with custom \"crs\" required property that takes in values " + Util.toString(Coordinates.Type.values()) + " " + CRS_DESC);
			} else {
				setRef(property, getter.getReturnType(), ref);
			}
		}
		if (getter.getReturnType() == Date.class) {
			property.setString(FORMAT, DATE_FORMAT);
		}
		if (getter.getReturnType() == Qname.class) {
			property.setString(FORMAT, "URI");
		}
		if (getter.getReturnType().isEnum()) {
			property.setArray(ENUM, getEnum(getter.getReturnType()));
		}
		if (getter.getName().equals("getRecordQualityMax")) {
			property.setString(TYPE, "string");
			property.setString(FORMAT, "URI");
		}
		if (getter.getName().equals("getRedListStatusMax")) {
			property.setString(FORMAT, "URI");
		}
		if (getter.getName().equals("getAtlasClassMax")) {
			property.setString(FORMAT, "URI");
		}
		if (getter.getName().equals("getAtlasCodeMax")) {
			property.setString(FORMAT, "URI");
		}
		return property;
	}

	private static JSONObject items(Method getter, Ref ref) {
		JSONObject items = new JSONObject();
		Class<?> arrayContentsClass = getCollectionParametrizedType(getter);
		if (ALL_CLASSES.contains(arrayContentsClass)) {
			setRef(items, arrayContentsClass, ref);
			return items;
		}
		items.setString(TYPE, getType(arrayContentsClass));
		if (arrayContentsClass.isEnum()) {
			items.setArray(ENUM, getEnum(arrayContentsClass));
		}
		return items;
	}

	private static void setRef(JSONObject items, Class<?> refClass, Ref ref) {
		if (ALL_CLASSES.contains(refClass)) {
			items.setString(REF, ref(refClass, ref));
		}
	}

	private static JSONArray getEnum(Class<?> arrayContentsClass) {
		JSONArray values = new JSONArray();
		for (Object enumValue : arrayContentsClass.getEnumConstants()) {
			values.appendString(enumValue.toString());
		}
		return values;
	}

	private static Class<?> getCollectionParametrizedType(Method method) {
		ParameterizedType type = (ParameterizedType) method.getGenericReturnType();
		java.lang.reflect.Type typeOfType = type.getActualTypeArguments()[0];
		if (!(typeOfType instanceof Class)) throw new IllegalStateException();
		return (Class<?>) typeOfType;
	}

	@SuppressWarnings("unused")
	private static JSONObject privateDownloadApprovedApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Mark a private download request approved");
		desc.setString(DESCRIPTION, "Starts to process an approved request. The result files will be generated as a background task and approval service is notified when the files are ready for downloading.");

		queryParameter(desc, PyhaApprovedAPI.ID, "ID of the request", true, STRING);
		queryParameter(desc, Const.PERSON_ID, "ID of the person request", true, STRING);
		queryParameter(desc, PyhaApprovedAPI.REJECTED_COLLECTIONS, "List of collection ids (Qname) of rejected collections", true, ARRAY);
		queryParameter(desc, PyhaApprovedAPI.SENSITIVE_APPROVED, "Is release of sensitive information approved? Currently can handle only those where it is.", true, BOOLEAN);

		downloadCommon(desc, Concealment.PRIVATE);
		return desc;
	}

	private static void parameter(JSONObject desc, String in, String name, String description, boolean required, String type, Object defaultValue, Collection<?> enumValues, Integer min, Integer max, String format) {
		JSONObject param = new JSONObject()
				.setString(NAME, name)
				.setString(IN, in)
				.setBoolean(REQUIRED, required)
				.setString(DESCRIPTION, description);

		param.getObject(SCHEMA).setString(TYPE, type);
		if (format != null) param.getObject(SCHEMA).setString(FORMAT, format);

		if (ARRAY.equals(type)) {
			if (enumValues == null) throw new IllegalStateException("No enum given for array: " + name);
		}
		if (enumValues != null) {
			for (Object o : enumValues) {
				if (Collection.class.isAssignableFrom(o.getClass())) throw new IllegalStateException("Enum value must not be a collection: " + name);
				param.getObject(SCHEMA).getArray(ENUM).appendString(o.toString());
			}
		}

		if (defaultValue != null) {
			if (defaultValue instanceof Boolean) {
				param.getObject(SCHEMA).setBoolean(DEFAULT, (boolean)defaultValue);
			} else if (defaultValue.getClass() == int.class || defaultValue instanceof Integer) {
				param.getObject(SCHEMA).setInteger(DEFAULT, (int)defaultValue);
			} else {
				param.getObject(SCHEMA).setString(DEFAULT, defaultValue.toString());
			}
		}
		if (min != null) param.getObject(SCHEMA).setInteger(MINIMUM, min);
		if (max !=  null) param.getObject(SCHEMA).setInteger(MAXIMUM, max);

		desc.getArray(PARAMETERS).appendObject(param);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, String type) {
		parameter(desc, QUERY, name, description, required, type, null, null, null, null, null);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, int defaultValue, int min, Integer max) {
		parameter(desc, QUERY, name, description, required, INTEGER, defaultValue, null, min, max, null);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, String type, Object defaultValue, Collection<?> enumValues) {
		parameter(desc, QUERY, name, description, required, type, defaultValue, enumValues, null, null, null);
	}

	@SuppressWarnings("unused")
	private static JSONObject privateDownloadApprovalRequestApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Start the process to get a private download request approved");
		desc.setString(DESCRIPTION, "Accepts a private file download request. Unlike other private-requests, for this request the API-key does not need to have access to private warehouse.");
		personToken(desc);
		downloadCommon(desc, Concealment.PRIVATE);
		return desc;
	}

	private static void downloadCommon(JSONObject desc, Concealment warehouse) {
		queryParameter(desc, Const.DOWNLOAD_FORMAT, "Format of the produced files.", true, STRING, DownloadFormat.TSV_FLAT.toString(), Utils.list(DownloadFormat.values()));

		queryParameter(desc, Const.DOWNLOAD_INCLUDES,
				"The dump files always contain information from document, gathering and unit levels. Optionally it is possible to include a number of the following: " +
						"DOCUMENT/GATHERING/"+DownloadInclude.UNIT_FACTS + ": Facts (non-harmonized data) in separate files, " +
						"DOCUMENT/GATHERING/"+DownloadInclude.UNIT_MEDIA + ": Media information in separate files, " +
						DownloadInclude.DOCUMENT_EDITORS + ": Owners of records in separate file, " +
						DownloadInclude.DOCUMENT_KEYWORDS + ": Additional identifiers etc in a separate file",
						false, ARRAY, null, Utils.list(DownloadInclude.values()));

		queryParameter(desc, Const.DATA_USE_PURPOSE, "Free form description of data use purpose.", false, STRING);
		queryParameter(desc, Const.API_KEY_EXPIRES, "Api key duration in days as integer.", false, INTEGER);

		formatParameter(desc, Utils.list(JSON));

		addSharedQueryParameters(desc, false, Base.UNIT, false, warehouse);
		response(desc, 200, "Request was accepted and will be processed.", new Content(APPLICATION_JSON));
		commonResponses(desc);
	}

	private static void personToken(JSONObject desc) {
		queryParameter(desc, Const.PERSON_TOKEN, "Token of the user that wants to receive the file download.", true, STRING);
	}

	@SuppressWarnings("unused")
	private static JSONObject downloadApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Make a file download request");
		desc.setString(DESCRIPTION, "Accepts a file download request OR stores statistics about a lightweight data download. In case of non-lightweight download, the result files will be generated as a background task and an email will be send to the user (identified by the " +Const.PERSON_TOKEN+ ") when the files are ready for downloading.");

		personToken(desc);
		queryParameter(desc, Const.LOCALE, "Locale used when communicating with the maker of the request. One of fi, sv, en. Defaults to fi.", false, STRING, "fi", Utils.list("fi", "en", "sv"));
		queryParameter(desc, Const.DOWNLOAD_TYPE, "Format of the produced files.", false, STRING, null, Utils.list(DownloadType.values()));

		downloadCommon(desc, Concealment.PUBLIC);
		response(desc, 429, "User limit per day was exceeded.", new Content(APPLICATION_JSON, ref(ERROR_REF)));
		return desc;
	}

	private static JSONObject getEnumerationLabelApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Enumeration label");
		desc.setString(DESCRIPTION, "Get descriptions of enumeration.");
		parameter(desc, PATH, "enumeration", "Name of the enumeration", true, STRING, null, null, null, null, null);
		response(desc, 200, "Succesful response.", new Content(APPLICATION_JSON));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject getEnumerationLabelsApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Enumeration labels");
		desc.setString(DESCRIPTION, "Get descriptions of enumerations that are used in query parameters and responses.");
		response(desc, 200, "Succesful response.", new Content(APPLICATION_JSON));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject getFilterDescriptionApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Filter description");
		desc.setString(DESCRIPTION, "Get description of a filter.");
		parameter(desc, PATH, "filter", "Name of the filter", true, STRING, null, null, null, null, null);
		response(desc, 200, "Succesful response.", new Content(APPLICATION_JSON));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject getFilterDescriptionsApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Filter descriptions");
		desc.setString(DESCRIPTION, "Get descriptions of filters used in queries.");
		response(desc, 200, "Succesful response.", new Content(APPLICATION_JSON));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject postPolygonApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Polygon ids for polygon search");
		desc.setString(DESCRIPTION, "" +
				"Submit polygons in various coordinate reference systems and in GeoJSON or WKT format, and get id of the polygon which can be given to polygonId search filter.");

		queryParameter(desc, Const.PERSON_TOKEN, "Person token is required to limit number of created polygon ids per day to 100", true, STRING);

		queryParameter(desc, Const.CRS, "Give coordinate reference system of the GeoJSON or WKT. Defaults to " + Coordinates.Type.EUREF + ". " + CRS_DESC,
				false, STRING, Coordinates.Type.EUREF.toString(), Utils.list(Coordinates.Type.values()));

		queryParameter(desc, Const.GEO_JSON_REQUEST, "Either this or " + Const.WKT + " is required. The polygon as GeoJSON.", false, STRING);
		queryParameter(desc, Const.WKT, "Either this or " + Const.GEO_JSON_REQUEST + " is required. The polygon as WKT.", false, STRING);

		formatParameter(desc, Utils.list(JSON, XML, PLAIN));

		response(desc, 200, "Succesful response.", Utils.list(
				new Content(APPLICATION_JSON),
				new Content(APPLICATION_XML),
				new Content(TEXT_PLAIN)));
		commonResponses(desc);
		response(desc, 429, "User limit per day was exceeded.", new Content(APPLICATION_JSON, ref(ERROR_REF)));
		return desc;
	}

	private static JSONObject getPolygonApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Polygon by polygonId");
		desc.setString(DESCRIPTION, "" +
				"Get polygon as GeoJSON or WKT in various coordinate reference systems. Polygon has been previously subscribed and given and id by making a POST request.");

		parameter(desc, PATH, "id", "Id of the polygon", true, INTEGER, null, null, null, null, null);

		formatParameter(desc, Utils.list(WKT, GEOJSON));

		queryParameter(desc, Const.CRS, "Give coordinate reference system in which to return the polygon. Defaults to " + Coordinates.Type.EUREF + ". " + CRS_DESC,
				false, STRING, Coordinates.Type.EUREF.toString(), Utils.list(Coordinates.Type.values()));

		response(desc, 200, "Succesful response.", Utils.list(
				new Content(APPLICATION_WKT),
				new Content(APPLICATION_GEO_JSON)));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject singleApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Get single full document.");
		desc.setString(DESCRIPTION, "Get single full document by document URI. Contains the document, gatherings and units, including facts, media etc");

		formatParameter(desc, Utils.list(JSON, XML));

		queryParameter(desc, "documentId", "Full document ID (URI identifier)", true, STRING);

		tokenFilters(Base.UNIT, desc);

		response(desc, 200, "Succesful response.", Utils.list(
				new Content(APPLICATION_JSON, ref(Document.class, Ref.SINGLE)),
				new Content(APPLICATION_XML)));
		commonResponses(desc);
		return desc;
	}

	private static JSONObject aggregateApi(Base base, Concealment warehouse) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Perform aggregate queries (group by) on " + base.toString().toLowerCase() + "s.");

		String description = "Aggregates the results of the query based on given \""+Const.AGGREGATE_BY+"\" parameters.";
		if (Const.aggregateFunctions(base).size() > 1) {
			description += " Always includes count of rows (count(*)) to the result. Other aggregate functions vary based on the given parameters. Possible aggregate functions are " + Const.aggregateFunctions(base);
		} else {
			description += " Returns count of rows (count(*)).";
		}

		desc.setString(DESCRIPTION, description);

		queryParameter(desc, Const.AGGREGATE_BY, "Define fields to aggregate by. " + arraySeparator(","), false, ARRAY, null, Fields.aggregatable(base).getAllFields());

		List<String> orderbyItems = new ArrayList<>(Fields.aggregatable(base).getAllFields());
		for (String s : Const.aggregateFunctions(base)) {
			orderbyItems.add(s);
		}
		String orderByDescription = "Define what fields to use when sorting results. Defaults to " +Const.COUNT+ " (desc) and each aggregate by field (asc). " +
				"Each fieldname given as parameter defaults to ASC - if you want to sort using descending order, add \" DESC\" to the end of the field name. " +
				"In addition to " +Const.AGGREGATE_BY+ " fields you can use the following aggregate function names: " + Const.aggregateFunctions(base) + ". " +
				arraySeparator(",");
		queryParameter(desc, Const.ORDER_BY, orderByDescription, false, ARRAY, null, orderbyItems);

		if (base.includes(Base.GATHERING)) {
			geoJsonParameters(desc);
		}

		aggregateParameters(base, desc);

		pagingParams(desc);

		addSharedQueryParameters(desc, true, base, false, warehouse);
		addSharedQueryResponses(desc, AggregateResponse.class, Utils.list(APPLICATION_JSON, APPLICATION_GEO_JSON, APPLICATION_XML, TEXT_CSV, TEXT_TSV), Utils.list(JSON, GEOJSON, XML, CSV, TSV));
		return desc;
	}

	private static void geoJsonParameters(JSONObject desc) {
		queryParameter(desc, Const.CRS, "For GeoJSON requests there are two additional parameters: " + Const.CRS + " and " + Const.FEATURE_TYPE + ". This controls the coordinate reference system used in the returned GeoJSON features. " + CRS_DESC,
				false, STRING, null, Utils.list(Coordinates.Type.values()));
		queryParameter(desc, Const.FEATURE_TYPE, "For GeoJSON requests there are two additional parameters: " + Const.CRS + " and " + Const.FEATURE_TYPE + ". This controls the type of returned GeoJSON features.",
				false, STRING, null, Utils.list(Const.FeatureType.values()));
	}

	private static void aggregateParameters(Base base, JSONObject desc) {
		if (Const.aggregateFunctions(base).size() > 1) {
			queryParameter(desc, Const.ONLY_COUNT, "Return only count of rows (default) or also additional aggregate function values.", false, BOOLEAN, Boolean.TRUE, null);
		}

		if (base == Base.UNIT) {
			queryParameter(desc, Const.TAXON_COUNTS, "Include taxon count, species count and max red list status", false, BOOLEAN, Boolean.FALSE, null);
			queryParameter(desc, Const.GATHERING_COUNTS, "Include gatheringCount", false, BOOLEAN, Boolean.FALSE, null);
			queryParameter(desc, Const.PAIR_COUNTS, "Include pair count sum and max.", false, BOOLEAN, Boolean.FALSE, null);
			queryParameter(desc, Const.ATLAS_COUNTS, "Include atlas code and class max.", false, BOOLEAN, Boolean.FALSE, null);
		}

		queryParameter(desc, Const.EXCLUDE_NULLS, "Include or exclude nulls to result. Will only check nullness of the first aggregateBy field.", false, BOOLEAN, Boolean.TRUE, null);
		queryParameter(desc, Const.PESSIMISTIC_DATE_RANGE_HANDLING, "Value of this parameter affects how " + Const.OLDEST_RECORD + " and " + Const.NEWEST_RECORD + " are calculated regarding observations reported as date span. False (default): oldest=min(date.begin), newest=max(date.end). True: oldest=min(date.end), newest=max(date.begin).",
				false, BOOLEAN, Boolean.FALSE, null);
	}

	private static JSONObject statisticsApi(Base base, Concealment warehouse) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Perform aggregate queries on " + base.toString().toLowerCase() + "s to PRIVATE side of the data warehouse.");

		String description = " Functionality is same as normal /aggregate API except functionaly is limited to only certain collections, filters and " + Const.AGGREGATE_BY + " fields. CollectionId filter is required and only certain collections are allowed.";

		desc.setString(DESCRIPTION, description);

		queryParameter(desc, Const.AGGREGATE_BY, "Define fields to aggregate by. " + arraySeparator(","), false, ARRAY, null, Fields.statisticsAggregatable(base).getAllFields());

		List<String> orderbyItems = new ArrayList<>(Fields.statisticsAggregatable(base).getAllFields());
		for (String s : Const.aggregateFunctions(base)) {
			orderbyItems.add(s);
		}
		String orderByDescription = "Define what fields to use when sorting results. Defaults to " +Const.COUNT+ " (desc) and each aggregate by field (asc). " +
				"Each fieldname given as parameter defaults to ASC - if you want to sort using descending order, add \" DESC\" to the end of the field name. " +
				"In addition to " +Const.AGGREGATE_BY+ " fields you can use the following aggregate function names: " + Const.aggregateFunctions(base) + ". " +
				arraySeparator(",");
		queryParameter(desc, Const.ORDER_BY, orderByDescription, false, ARRAY, null, orderbyItems);

		aggregateParameters(base, desc);

		pagingParams(desc);

		addSharedQueryParameters(desc, true, base, true, warehouse);
		addSharedQueryResponses(desc, AggregateResponse.class, Utils.list(APPLICATION_JSON, APPLICATION_GEO_JSON, APPLICATION_XML, TEXT_CSV, TEXT_TSV), Utils.list(JSON, GEOJSON, XML, CSV, TSV));
		return desc;
	}

	private static JSONObject listApi(Base base, Selected defaultSelect, OrderBy defaultOrderBy, Concealment warehouse) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Get list of "+base.toString().toLowerCase()+"s using given filters");
		desc.setString(DESCRIPTION, "Get list of results as a 'flat row'. Application/json and application/xml responses respect the \""+Const.SELECTED_FIELDS+"\" parameter, but application/rdf+xml returns always the same \"CETAF standard\" fields.");

		queryParameter(desc, Const.SELECTED_FIELDS, "Define what fields to include to the result. Defaults to " + defaultSelect + " " + arraySeparator(","),
				false, ARRAY, null, Fields.selectable(base).getAllFields());

		queryParameter(desc, Const.ORDER_BY, "Define what fields to use when sorting results. Defaults to " + defaultOrderBy + ". Unit key is always added as a last parameter to ensure correct paging. " +
				"You can include ASC or DESC after the name of the field (defaults to ASC)." +
				arraySeparator(","),
				false, ARRAY, null, Fields.sortable(base).getAllFields());

		geoJsonParameters(desc);

		pagingParams(desc);

		addSharedQueryParameters(desc, true, base, false, warehouse);
		addSharedQueryResponses(desc, ListResponse.class, Utils.list(APPLICATION_JSON, APPLICATION_GEO_JSON, APPLICATION_XML, APPLICATION_RDF_XML), Utils.list(JSON, GEOJSON, XML, RDF_XML));
		return desc;
	}

	private static void pagingParams(JSONObject desc) {
		queryParameter(desc, Const.PAGE_SIZE, "Set number of results in one page.", false, UnitListQueryAPI.DEFAULT_PAGE_SIZE, 1, PageableBaseQuery.MAX_PAGE_SIZE);
		queryParameter(desc, Const.CURRENT_PAGE, "Set current page.", false, 1, 1, null);
	}

	private static JSONObject countApi(Base base, Concealment warehouse) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Get count of units using given filters");
		desc.setString(DESCRIPTION, "Use this API to test how many results your query would return and then proceed with list query.");
		addSharedQueryResponses(desc, CountResponse.class, Utils.list(APPLICATION_JSON, APPLICATION_XML, TEXT_PLAIN), Utils.list(JSON, XML, PLAIN));
		addSharedQueryParameters(desc, true, base, false, warehouse);
		return desc;
	}

	private static void addSharedQueryResponses(JSONObject desc, Class<?> schemaRef, List<String> acceptContentTypes, List<String> formatParameters) {
		formatParameter(desc, formatParameters);

		List<Content> contentTypes = new ArrayList<>();
		for (String contentType : acceptContentTypes) {
			if (contentType.equals(APPLICATION_JSON)) {
				contentTypes.add(new Content(APPLICATION_JSON, ref(schemaRef, Ref.QUERY)));
			} else {
				contentTypes.add(new Content(contentType));
			}
		}

		response(desc, 200, "Succesful query. Schema varies based on content-type of the response.", contentTypes);
		commonResponses(desc);
	}

	private static void formatParameter(JSONObject desc, List<String> formatParameters) {
		queryParameter(desc, Const.FORMAT, "Alternative way to Accept header to define content type of the response.", false, STRING, null, formatParameters);
	}

	private static JSONObject pushApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Load data to the Data Warehouse");
		desc.setString(DESCRIPTION, "" +
				"Requires that API key has load permissions. Data is given in request body. " +
				"Supports multiple data formats. See [documentation](https://laji.fi/about/1402). "+
				"Accepts all payloads that pass format validation (for example is valid XML), but that does not mean the data will be processed succesfully. "+
				"");

		requestBody(desc,
				new Content(APPLICATION_JSON, ref(DwRoot.class, Ref.ETL)),
				new Content(APPLICATION_XML),
				new Content(APPLICATION_RDF_XML),
				new Content(TEXT_PLAIN),
				new Content(TEXT_CSV));

		queryParameter(desc, "sourceId",
				"Normally sourceId is received via the API key. By giving this parameter you can override the sourceId. API key must have permissions to use that sourceId.",
				false, STRING);

		response(desc, 200,
				"Accepted and stored for processing. Does not neccesarilly mean the data will be successfully processed. Returns \"ok\"",
				new Content(TEXT_PLAIN));
		commonResponses(desc);
		response(desc, 400,
				"Data was not accepted. Message tells why.",
				new Content(APPLICATION_JSON, ref(ERROR_REF)));
		return desc;
	}

	private static JSONObject pushApiDelete() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Report document deleted");
		desc.setString(DESCRIPTION, "" +
				"Requires that API key has load permissions. Note that you can only delete documents from the source that is defined by " + Const.API_KEY + ""+
				"");
		queryParameter(desc, "documentId", "Document URI to be deleted.", true, STRING);
		queryParameter(desc, "sourceId", "Normally sourceId is received via the API key. By giving this parameter you can override the sourceId. API key must have permissions to use that sourceId.", false, STRING);

		response(desc, 200, "Accepted delete request. Does not neccesarilly mean there was anything to delete or that delete has gone through yet. Returns \"ok\"", new Content(TEXT_PLAIN));
		commonResponses(desc);
		response(desc, 400, "Data was not accepted. Message tells why.", new Content(APPLICATION_JSON, ref(ERROR_REF)));
		return desc;
	}

	private static void addSharedQueryParameters(JSONObject desc, boolean cacheable, Base base, boolean onlyStatistics, Concealment warehouse) {
		if (cacheable) {
			queryParameter(desc, Const.CACHE, "Use cache for this query. Defaults to false.", false, BOOLEAN, Boolean.FALSE, null);
		}
		addFilters(desc, base, onlyStatistics, warehouse);
		if (!onlyStatistics) {
			tokenFilters(base, desc);
		}
	}

	private static void addFilters(JSONObject desc, Base base, boolean onlyStatistics, Concealment warehouse) {
		for (FilterInfo filter : Filters.filterParameters(base)) {
			if (warehouse == Concealment.PUBLIC && !filter.getInfo().useableInPublic()) {
				continue;
			}
			if (!onlyStatistics || filter.getInfo().usableInStatistics()) {
				filterDescription(desc, filter);
			}
		}
	}

	private static void filterDescription(JSONObject desc, FilterInfo filter) {
		String parameterType = getParameterTypeString(filter);
		Object defaultValue = getDefaultValue(filter.getInfo().defaultValue());
		String type = null;
		String description = filter.getInfo().description();
		String format = null;
		Collection<?> enumValues = null;

		if (parameterType.equals(ARRAY)) {
			type = STRING;
			if (filter.getName().endsWith("Fact")) {
				// keep description as it is
			} else if (filter.getName().equals("hasValue")) {
				description += " " + arraySeparator(filter.getInfo().parameterSeparator());
			} else {
				description += " " + arraySeparator(filter.getInfo().parameterSeparator()) + " " + OR_FILTER;
			}
		} else {
			if (parameterType.equals("date")) {
				type = STRING;
				format = DATE_FORMAT;
			} else {
				type = parameterType;
			}
		}

		if (filter.getInfo().type().equals(Type.ENUMERATION)) {
			type = STRING;
			enumValues = filter.getEnumerationValues();
		}

		if (filter.getInfo().type() == Type.RESOURCE && !"null".equals(filter.getInfo().resourceName())) {
			description += " API resource: /" + filter.getInfo().resourceName();
		}
		parameter(desc, QUERY, filter.getName(), description, false, type, defaultValue, enumValues, null, null, format);
	}

	private static String arraySeparator(String separatorChar) {
		return  "Multiple values are seperated by '"+separatorChar+"'.";
	}

	private static Object getDefaultValue(String defaultValue) {
		if (defaultValue.equals("null")) return null;
		if (defaultValue.equals("true") || defaultValue.equals("false")) return Boolean.valueOf(defaultValue);
		if (isInteger(defaultValue)) return Integer.valueOf(defaultValue);
		return defaultValue;
	}

	private static boolean isInteger(String defaultValue) {
		try {
			Integer.valueOf(defaultValue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static void tokenFilters(Base base, JSONObject desc) {
		queryParameter(desc, Const.EDITOR_PERSON_TOKEN, "Search for records the user has save or modified. When using this filter, results come from the private warehouse!", false, STRING);
		if (base.includes(Base.GATHERING)) {
			queryParameter(desc, Const.OBSERVER_PERSON_TOKEN, "Search for records where the user has been marked as the observer. When using this filter, results come from the private warehouse!", false, STRING);
			queryParameter(desc, Const.EDITOR_OR_OBSERVER_PERSON_TOKEN, "Search for records the user has saved OR where marked as the observer. When using this filter, results come from the private warehouse!", false, STRING);
			queryParameter(desc, Const.EDITOR_OR_OBSERVER_IS_NOT_PERSON_TOKEN, "Search for records where the user has not saved or observed the record (= everyone else's records). These come from the public warehouse! -> Results may contain records that have actually been saved by the user, but the info is not available in public (has been secured).", false, STRING);
		}
		queryParameter(desc, Const.PERMISSION_TOKEN, "Use granted permissions to search the private warehouse", false, STRING);
	}

	private static String getParameterTypeString(FilterInfo filter) {
		Class<?> filterClass = filter.getType();
		if (List.class.isAssignableFrom(filterClass)) return ARRAY;
		if (Set.class.isAssignableFrom(filterClass)) return ARRAY;
		if (filterClass == Qname.class) return STRING;
		if (filterClass == Partition.class) return STRING;
		if (filterClass == PolygonSearch.class) return STRING;
		if (filterClass == PolygonIdSearch.class) return STRING;
		String filterType = filter.getType().getSimpleName().toLowerCase();
		return filterType;
	}

	private static class Content {
		private final String contentType;
		private final String ref;
		public Content(String contentType, String ref) {
			this.contentType = contentType;
			this.ref = ref;
		}
		public Content(String contentType) {
			this.contentType = contentType;
			this.ref = null;
		}
	}

	private static void requestBody(JSONObject desc, Content ... contents) {
		desc.getObject("requestBody").setString(DESCRIPTION, "See [documentation](https://laji.fi/about/1402) for complete reference. Can contain multiple documents.");
		for (Content bodyContent : contents) {
			if (bodyContent.ref != null) {
				desc.getObject("requestBody").getObject(CONTENT).getObject(bodyContent.contentType).getObject(SCHEMA).setString(REF, bodyContent.ref);
			} else {
				desc.getObject("requestBody").getObject(CONTENT).getObject(bodyContent.contentType).getObject(SCHEMA).setString(TYPE, STRING);
			}
		}
		desc.getObject("requestBody").setBoolean(REQUIRED, true);
	}

	private static void commonResponses(JSONObject desc) {
		response(desc, 400, "Parameters were not accepted. Message has details.", new Content(APPLICATION_JSON, ref(ERROR_REF)));
		response(desc, 403, "Invalid credentials. Message has details.", new Content(APPLICATION_JSON, ref(ERROR_REF)));
		response(desc, 429, "Too many pending requests for the " + Const.API_KEY + "; max is " + ETLBaseServlet.MAX_PENDING_REQUESTS, new Content(APPLICATION_JSON, ref(ERROR_REF)));
		response(desc, 500, "Service is in unknown erroneous state.", new Content(TEXT_PLAIN));
		desc.setArray(TAGS, WAREHOUSE_TAG_ARRAY);
	}

	private static void response(JSONObject desc, int status, String description, Content content) {
		response(desc, status, description, Utils.list(content));
	}

	private static void response(JSONObject desc, int status, String description, List<Content> contents) {
		if (status != 200 && !desc.getObject(RESPONSES).hasKey(""+200)) throw new IllegalStateException("Should always define 200 first, errors after");

		JSONObject response = new JSONObject();
		response.setString(DESCRIPTION, description);

		for (Content content : contents) {
			if (content.ref != null) {
				response.getObject(CONTENT).getObject(content.contentType).getObject(SCHEMA).setString(REF, content.ref);
			} else {
				response.getObject(CONTENT).getObject(content.contentType).getObject(SCHEMA).setString(TYPE, STRING);
			}
		}
		desc.getObject(RESPONSES).setObject(""+status, response);
	}

	private static final Map<Ref, String> REF_PREFIX;
	static {
		REF_PREFIX = new HashMap<>();
		REF_PREFIX.put(Ref.ETL, DW_ETL);
		REF_PREFIX.put(Ref.QUERY, DW_QUERY);
		REF_PREFIX.put(Ref.SINGLE, DW_SINGLE);
	}

	private static String ref(Class<?> arrayContentsClass, Ref ref) {
		String prefix = REF_PREFIX.get(ref);
		return "#/components/schemas/" + prefix + arrayContentsClass.getSimpleName();
	}

	private static String ref(String ref) {
		return "#/components/schemas/" + ref;
	}

	private static final JSONArray WAREHOUSE_TAG_ARRAY = new JSONArray().appendString(WAREWHOUSE_TAG);

	private static final Set<Class<?>> COMMON_CLASSES = Utils.set(
			Document.class, Gathering.class, Unit.class, Sample.class,
			MediaObject.class, Coordinates.class, DateRange.class, Fact.class, TaxonCensus.class,
			DocumentQuality.class, GatheringQuality.class, UnitQuality.class, Quality.class,
			IdentificationEvent.class, TypeSpecimen.class);

	private static final Set<Class<?>> DW_CLASSES;
	static {
		DW_CLASSES = Utils.set(DocumentDWLinkings.class, GatheringInterpretations.class, GatheringConversions.class, GatheringDWLinkings.class,
				UnitDWLinkings.class, UnitInterpretations.class, SingleCoordinates.class, Person.class,
				Annotation.class, Identification.class, OccurrenceAtTimeOfAnnotation.class, IdentificationDwLinkings.class, RedListStatus.class,
				HabitatObject.class, NamedPlaceEntity.class);
	}

	private static final Set<Class<?>> ETL_CLASSES;
	static {
		ETL_CLASSES = Utils.set(DwRoot.class);
		ETL_CLASSES.addAll(COMMON_CLASSES);
	}

	private static final Set<Class<?>> SINGLE_CLASSES;
	static {
		SINGLE_CLASSES = Utils.set();
		SINGLE_CLASSES.addAll(COMMON_CLASSES);
		SINGLE_CLASSES.addAll(DW_CLASSES);
	}

	private static final Set<Class<?>> QUERY_RESPONSE_CLASSES;
	static {
		QUERY_RESPONSE_CLASSES = Utils.set(CountResponse.class, ListResponse.class, AggregateResponse.class, JoinedRow.class, AggregateRow.class);
		QUERY_RESPONSE_CLASSES.addAll(COMMON_CLASSES);
		QUERY_RESPONSE_CLASSES.addAll(DW_CLASSES);
	}

	private static final Set<Class<?>> ALL_CLASSES;
	static {
		ALL_CLASSES = new HashSet<>();
		ALL_CLASSES.addAll(QUERY_RESPONSE_CLASSES);
		ALL_CLASSES.addAll(ETL_CLASSES);
		ALL_CLASSES.add(Taxon.class);
	}
	private static final Set<Class<?>> STRING_TYPE_CLASSES = Utils.set(String.class, Qname.class, Date.class);
	private static final Set<Class<?>> INTEGER_TYPE_CLASSES = Utils.set(Integer.class, Long.class, long.class);
	private static final Set<Class<?>> NUMBER_TYPE_CLASSES = Utils.set(Double.class);

	private static String getType(Class<?> returnType) {
		if (returnType.isEnum()) return STRING;
		if (STRING_TYPE_CLASSES.contains(returnType)) return STRING;
		if (returnType.equals(Boolean.TYPE)) return BOOLEAN;
		if (returnType == Boolean.class) return BOOLEAN;
		if (returnType.equals(Integer.TYPE)) return INTEGER;
		if (INTEGER_TYPE_CLASSES.contains(returnType)) return INTEGER;
		if (NUMBER_TYPE_CLASSES.contains(returnType)) return NUMBER;
		if (ALL_CLASSES.contains(returnType)) return OBJECT;
		if (returnType == Geo.class) return OBJECT;
		if (returnType == AggregateRow.class) return OBJECT;
		if (returnType == Taxon.class) return OBJECT;
		if (returnType == LocalizedText.class) return OBJECT;
		throw new IllegalStateException(returnType.getSimpleName());
	}

}
