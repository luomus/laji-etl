package fi.laji.datawarehouse.etl.utils;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import fi.luomus.commons.utils.Utils;

public class AccessLimiter {

	public class TooManyRequestsException extends Exception {
		private static final long serialVersionUID = -3769561443667075468L;
		public TooManyRequestsException(int maxPerUser, String remoteUser) {
			super("There are too many pending requests from user '" + remoteUser + "' " + maxPerUser+"/"+maxPerUser);
		}
	}

	public interface Access {
		public void release();
		public static Access noLimit() {
			return new Access() {
				@Override
				public void release() {}
			};
		}
	}
	
	private static class AccessImple implements Access {
		private final AccessLimiter granter;
		private final String id;
		private final String userId;
		private AccessImple(AccessLimiter granter, String userId) {
			this.id = Utils.generateGUID();
			this.granter = granter;
			this.userId = userId;
		}
		public void release() {
			granter.release(this);
		}
		@Override
		public int hashCode() {
			return id.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			AccessImple other = (AccessImple) obj;
			return id.equals(other.id);
		}
	}

	private final Map<String, Set<AccessImple>> open = new ConcurrentHashMap<>();
	private final int maxPerUser;

	public AccessLimiter(int maxPerUser) {
		this.maxPerUser = maxPerUser;
	}

	public Access delayAccessIfNecessary(String userId) throws TooManyRequestsException {
		if (userId == null) throw new IllegalArgumentException("no api user");
		AccessImple access = new AccessImple(this, userId);
		AtomicBoolean tooManyPending = new AtomicBoolean(false);
		open.compute(userId, (key, grantedAccesses) -> {
			if (grantedAccesses == null) {
				grantedAccesses = Collections.newSetFromMap(new ConcurrentHashMap<>());
				grantedAccesses.add(access);
				return grantedAccesses;
			}

			int i = 0;
			while (grantedAccesses.size() >= maxPerUser) {
				if (i++ > 3) {
					tooManyPending.set(true);
					return grantedAccesses;
				}
				sleep();
			}

			grantedAccesses.add(access);
			return grantedAccesses;
		});

		if (tooManyPending.get()) {
			throw new TooManyRequestsException(maxPerUser, userId);
		}

		return access;
	}

	private void sleep() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
		}
	}

	private void release(AccessImple access) {
		getAccesses(access.userId).remove(access);
	}

	private Set<AccessImple> getAccesses(String userId) {
		open.putIfAbsent(userId, Collections.newSetFromMap(new ConcurrentHashMap<AccessImple, Boolean>()));
		return open.get(userId);
	}

}
