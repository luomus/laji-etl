package fi.laji.datawarehouse.etl.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class AccessMonitor {

	private final ConcurrentHashMap<String, LongAdder> userRequestCounts = new ConcurrentHashMap<>();

	private final Map<String, String> systemNames;

	public AccessMonitor(Map<String, Source> systems) {
		this.systemNames = systems.entrySet().stream().collect(Collectors.toMap(
				e -> Qname.fromURI(e.getKey()).toString(),
				e -> e.getValue().getName()
				));
		Thread monitorThread = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(60_000);
					dumpAndClearCounts();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					break;
				}
			}
		});
		monitorThread.setName("AccessMonitor Thread");
		monitorThread.setDaemon(true);
		monitorThread.start();
	}

	public void logAccess(String userId) {
		if (userId == null || userId.isEmpty()) throw new IllegalArgumentException("no api user");
		userRequestCounts.computeIfAbsent(userId, key -> new LongAdder()).increment();
	}

	private void dumpAndClearCounts() {
		// Create a snapshot of the user counts
		List<Map.Entry<String, LongAdder>> entries = new ArrayList<>(userRequestCounts.entrySet());

		// Clear the map for the next interval
		userRequestCounts.clear();

		// Print the counts
		entries.sort(Map.Entry.comparingByKey());
		System.out.println(" === " + DateUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss") + " DW API User Request Counts (Last Minute) === ");
		for (Map.Entry<String, LongAdder> entry : entries) {
			System.out.println(systemName(entry.getKey()) + ": " + entry.getValue().sum());
		}
	}

	private String systemName(String apiuser) {
		String systemName = systemNames.get(apiuser);
		if (systemName != null) {
			return systemName + " " + apiuser;
		}
		return apiuser;
	}

}
