package fi.laji.datawarehouse.etl.utils;

import fi.laji.datawarehouse.dao.DAO;

public class ThreadStatusReporterWithLogging extends ThreadStatusReporter {

	private final DAO dao;
	private final Class<?> reportingClass;
	private final ThreadStatusReporter statusReporter;

	public ThreadStatusReporterWithLogging(ThreadStatuses statuses, DAO dao, Class<?> reportingClass) {
		this.dao = dao;
		this.reportingClass = reportingClass;
		this.statusReporter = statuses.getThreadStatusReporterFor(reportingClass);
	}

	@Override
	public void setStatus(String status) {
		statusReporter.setStatus(status);
		dao.logMessage(Const.LAJI_ETL_QNAME, reportingClass, status);
	}

}