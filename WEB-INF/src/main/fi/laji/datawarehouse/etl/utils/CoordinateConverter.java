package fi.laji.datawarehouse.etl.utils;

import java.util.ArrayList;
import java.util.List;

import fi.laji.Conversion;
import fi.laji.CoordinateConverterProj4jImple;
import fi.laji.DegreePoint;
import fi.laji.MetricPoint;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;

public class CoordinateConverter {

	private static final CoordinateConverterProj4jImple CONVERTER = new CoordinateConverterProj4jImple();

	public static Geo convertGeo(Geo geo, Type convertToType) throws DataValidationException {
		Geo converted = new Geo(convertToType);
		for (Feature f : geo.getFeatures()) {
			converted.addFeature(convertFeature(f, convertToType, geo.getCRS()));
		}
		return converted.validate().setAccuracyInMeters(geo.getAccuracyInMeters());
	}

	public static Feature convertFeature(Feature f, Type convertToType, Type convertFromType) throws DataValidationException {
		if (f instanceof Point) {
			return convertPoint((Point)f, convertToType, convertFromType);
		} else if (f instanceof Polygon) {
			return convertPolygon(f, convertToType, convertFromType);
		} else if (f instanceof Line) {
			return convertLine(f, convertToType, convertFromType);
		} else {
			throw new UnsupportedOperationException("Unknown geo type " + f.getClass());
		}
	}

	private static Feature convertPolygon(Feature orig, Type convertToType, Type convertFromType) throws DataValidationException {
		List<Point> convertedPoints = convertPoints(orig, convertToType, convertFromType);
		return Polygon.from(convertedPoints).validate();
	}

	private static Feature convertLine(Feature orig, Type convertToType, Type convertFromType) throws DataValidationException {
		List<Point> convertedPoints = convertPoints(orig, convertToType, convertFromType);
		return Line.from(convertedPoints).validate();
	}

	private static List<Point> convertPoints(Feature feature, Type convertToType, Type convertFromType) throws DataValidationException {
		List<Point> convertedPoints = new ArrayList<>();
		Point prev = null;
		for (Point point : feature) {
			Point convertedPoint = convertPoint(point, convertToType, convertFromType);
			if (notTheSame(prev, convertedPoint)) { // If because of conversion, previous and this point have become the same, ignore this point
				convertedPoints.add(convertedPoint);
			}
			prev = convertedPoint;
		}
		return convertedPoints;
	}

	private static Point convertPoint(Point p, Type convertToType, Type convertFromType) throws DataValidationException {
		Coordinates converted = convertPointToCoordinates(p, convertToType, convertFromType);
		if (converted == null) {
			if (convertToType == Type.YKJ) {
				throw new DataValidationException("Conversion to YKJ failed. (Too large area?)");
			}
			if (convertToType == Type.EUREF) {
				throw new DataValidationException("Conversion to EUREF failed. (Too large area?)");
			}
			throw new ETLException("Conversion failed for " + p + " from " + convertFromType + " to " + convertToType);
		}
		return Point.from(converted.getLatMin(), converted.getLonMin());
	}

	private static Coordinates convertPointToCoordinates(Point p, Type convertToType, Type convertFromType) throws DataValidationException {
		ConvertedCoordinates converted = CoordinateConverter.convert(new Coordinates(p.getLat(), p.getLon(), convertFromType));
		if (convertToType == Type.EUREF) {
			return converted.getEuref();
		}
		if (convertToType == Type.WGS84) {
			return converted.getWgs84();
		}
		if (convertToType == Type.YKJ) {
			return converted.getYkj();
		}
		throw new UnsupportedOperationException("Unknown coordinate type " + convertToType);
	}

	public static ConvertedCoordinates convert(Coordinates coordinates) throws DataValidationException {
		Conversion p1, p2, p3, p4;
		try {
			p1 = convert(coordinates.getLatMin(), coordinates.getLonMin(), coordinates.getType());
			p2 = convert(coordinates.getLatMin(), coordinates.getLonMax(), coordinates.getType());
			p3 = convert(coordinates.getLatMax(), coordinates.getLonMin(), coordinates.getType());
			p4 = convert(coordinates.getLatMax(), coordinates.getLonMax(), coordinates.getType());
		} catch (Exception e) {
			Throwable t = e;
			while (t.getCause() != null) {
				t = t.getCause();
				if (t instanceof DataValidationException) {
					throw (DataValidationException) t;
				}
			}
			throw e;
		}
		ConvertedCoordinates conversions = new ConvertedCoordinates();
		conversions.setWgs84(toWGS84(p1, p2, p3, p4));
		conversions.setYkj(toMetric(Type.YKJ, p1.getYkj(), p2.getYkj(), p3.getYkj(), p4.getYkj()));
		conversions.setEuref(toMetric(Type.EUREF, p1.getEuref(), p2.getEuref(), p3.getEuref(), p4.getEuref()));
		return conversions;
	}

	private static Coordinates toMetric(Type type, MetricPoint...points) throws DataValidationException {
		int latMin = Integer.MAX_VALUE;
		int latMax = Integer.MIN_VALUE;
		int lonMin = Integer.MAX_VALUE;
		int lonMax = Integer.MIN_VALUE;
		for (MetricPoint point : points) {
			if (point == null) return null;
			if (point.getNorthing() ==  null || point.getEasting() == null) return null;
			int lat = point.getNorthing();
			int lon = point.getEasting();
			latMin = Math.min(latMin, lat);
			latMax = Math.max(latMax, lat);
			lonMin = Math.min(lonMin, lon);
			lonMax = Math.max(lonMax, lon);
		}
		if (latMin == Integer.MAX_VALUE || latMax == Integer.MIN_VALUE || lonMin == Integer.MAX_VALUE || lonMax == Integer.MIN_VALUE) return null;
		return new Coordinates(latMin, latMax, lonMin, lonMax, type);
	}

	private static Coordinates toWGS84(Conversion ... points) throws DataValidationException {
		double latMin = Double.POSITIVE_INFINITY;
		double latMax = Double.NEGATIVE_INFINITY;
		double lonMin = Double.POSITIVE_INFINITY;
		double lonMax = Double.NEGATIVE_INFINITY;
		for (Conversion point : points) {
			double lat = point.getWgs84().getLat();
			double lon = point.getWgs84().getLon();
			latMin = Math.min(latMin, lat);
			latMax = Math.max(latMax, lat);
			lonMin = Math.min(lonMin, lon);
			lonMax = Math.max(lonMax, lon);
		}
		return new Coordinates(latMin, latMax, lonMin, lonMax, Type.WGS84);
	}

	public static Conversion convert(SingleCoordinates coordinates) throws DataValidationException {
		return convert(coordinates.getLat(), coordinates.getLon(), coordinates.getType());
	}

	private static Conversion convert(Double lat, Double lon, Type type) throws DataValidationException {
		if (type == Type.YKJ) {
			return CONVERTER.convertFromYKJ(new MetricPoint(lat, lon));
		}
		if (type == Type.WGS84) {
			return CONVERTER.convertFromWGS84(new DegreePoint(lat, lon));
		}
		if (type == Type.EUREF) {
			try {
				return CONVERTER.convertFromEuref(new MetricPoint(lat, lon));
			} catch (IllegalArgumentException e) {
				throw new DataValidationException("Invalid EUREF coordinates");
			}
		}
		throw new UnsupportedOperationException("Unknown type " + type);
	}

	private static boolean notTheSame(Point prev, Point convertedPoint) {
		return prev == null || !prev.equals(convertedPoint);
	}

}
