package fi.laji.datawarehouse.etl.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import fi.laji.datawarehouse.query.download.service.PyhaApprovedAPI;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;


public class Const {

	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String SCHEMA = "schema";
	public static final String LAJI_ETL_SCHEMA = "laji-etl";
	public static final String LAJISTORE_SCHEMA = "lajistore";
	public static final String LAJISTORE_SECONDAY_DATA_SCHEMA = "lajistore-secondary-data";
	public static final String SYKE_ZOOBENTHOS_SCHEMA = "syke-zoobenthos";

	public static final String SPLITTED_DOCUMENT_ID_QNAME_NAMESPACE_PREFIX = "A.";

	public static final Qname FINLAND = new Qname("ML.206");
	public static final Qname RUSSIA = new Qname("ML.239");
	public static final Qname SOVIET_UNION = new Qname("ML.247");
	public static final Qname SWEDEN = new Qname("ML.181");
	public static final Qname ESTONIA = new Qname("ML.241");
	public static final Qname NORWAY = new Qname("ML.157");

	public static final Set<Qname> COUNTRIES_SURROUNDING_FINLAND = Collections.unmodifiableSet(Utils.set(ESTONIA, SOVIET_UNION, RUSSIA, SWEDEN, NORWAY));

	public static final Qname LAJI_ETL_QNAME = new Qname("KE.398");
	public static final Qname ANNOTATION_SOURCE = new Qname("KE.388");

	public static final Qname MASTER_CHECKLIST_BIOTA_QNAME = new Qname("MX.37600");
	public static final Qname MASTER_CHECKLIST_QNAME = new Qname("MR.1");

	public static final String CONTENT_TYPE = "content-type";
	public static final String ACCEPT = "accept";

	public static final String FORMAT = "format";
	public static final String PAGE_SIZE = "pageSize";
	public static final String CURRENT_PAGE = "page";
	public static final String API_KEY = "access_token";
	public static final String PERSON_TOKEN = "personToken";
	public static final String PERMISSION_TOKEN = "permissionToken";
	public static final String ORDER_BY = "orderBy";
	public static final String SELECTED_FIELDS = "selected";
	public static final String AGGREGATE_BY = "aggregateBy";
	public static final String GEO_JSON_REQUEST = "geoJSON";
	public static final String CRS = "crs";
	public static final String WKT = "wkt";
	public static final String FEATURE_TYPE = "featureType";
	public static final String EXCLUDE_NULLS = "excludeNulls";
	public static final String ONLY_COUNT = "onlyCount";
	public static final String PAIR_COUNTS = "pairCounts";
	public static final String TAXON_COUNTS = "taxonCounts";
	public static final String ATLAS_COUNTS = "atlasCounts";
	public static final String GATHERING_COUNTS = "gatheringCounts";
	public static final String PESSIMISTIC_DATE_RANGE_HANDLING = "pessimisticDateRangeHandling";
	public static final String DOWNLOAD_TYPE = "downloadType";
	public static final String DOWNLOAD_FORMAT = "downloadFormat";
	public static final String DOWNLOAD_INCLUDES = "downloadIncludes";
	public static final String DATA_USE_PURPOSE = "dataUsePurpose";
	public static final String API_KEY_EXPIRES = "apiKeyExpires";
	public static final String LOCALE = "locale";
	public static final String CACHE = "cache";
	public static final String EDITOR_PERSON_TOKEN = "editorPersonToken";
	public static final String OBSERVER_PERSON_TOKEN = "observerPersonToken";
	public static final String EDITOR_OR_OBSERVER_PERSON_TOKEN = "editorOrObserverPersonToken";
	public static final String EDITOR_OR_OBSERVER_IS_NOT_PERSON_TOKEN = "editorOrObserverIsNotPersonToken";
	public static final String PERSON_ID = "personId";
	public static final String PERSON_EMAIL = "personEmail";
	public static final Collection<String> ACCEPTED_PARAMETERS = Collections.unmodifiableCollection(Utils.list(
			FORMAT, PAGE_SIZE, CURRENT_PAGE, API_KEY, PERSON_TOKEN, EXCLUDE_NULLS,
			ORDER_BY, SELECTED_FIELDS, AGGREGATE_BY, GEO_JSON_REQUEST, CRS, FEATURE_TYPE, WKT,
			ONLY_COUNT, PAIR_COUNTS, TAXON_COUNTS, ATLAS_COUNTS, GATHERING_COUNTS,
			DOWNLOAD_TYPE, DOWNLOAD_FORMAT, DOWNLOAD_INCLUDES, DATA_USE_PURPOSE, API_KEY_EXPIRES, LOCALE, CACHE, PESSIMISTIC_DATE_RANGE_HANDLING,
			EDITOR_PERSON_TOKEN, OBSERVER_PERSON_TOKEN, EDITOR_OR_OBSERVER_PERSON_TOKEN, EDITOR_OR_OBSERVER_IS_NOT_PERSON_TOKEN,
			PERSON_ID, PERSON_EMAIL, PERMISSION_TOKEN,
			PyhaApprovedAPI.ID, PyhaApprovedAPI.SENSITIVE_APPROVED, PyhaApprovedAPI.REJECTED_COLLECTIONS));

	public static enum ResponseFormat { JSON, GEO_JSON, XML, RDF_XML, TEXT_PLAIN, TEXT_CSV, TEXT_TSV }

	public static enum FeatureType { CENTER_POINT, ENVELOPE, ORIGINAL_FEATURE }

	public static final String NEWEST_RECORD = "newestRecord";
	public static final String OLDEST_RECORD = "oldestRecord";
	public static final String INDIVIDUAL_COUNT_MAX = "individualCountMax";
	public static final String INDIVIDUAL_COUNT_SUM = "individualCountSum";
	public static final String FIRST_LOAD_DATE_MIN = "firstLoadDateMin";
	public static final String FIRST_LOAD_DATE_MAX = "firstLoadDateMax";
	public static final String PAIR_COUNT_MAX = "pairCountMax";
	public static final String PAIR_COUNT_SUM = "pairCountSum";
	public static final String COUNT = "count";
	public static final String LINELENGTH_SUM = "lineLengthSum";
	public static final String TAXON_COUNT = "taxonCount";
	public static final String SPECIES_COUNT = "speciesCount";
	public static final String ATLAS_CODE_MAX = "atlasCodeMax";
	public static final String ATLAS_CLASS_MAX = "atlasClassMax";
	public static final String RECORD_QUALITY_MAX = "recordQualityMax";
	public static final String RED_LIST_STATUS_MAX = "redListStatusMax";
	public static final String GATHERING_COUNT = "gatheringCount";
	public static final String SECURED_COUNT = "securedCount";

	public static Set<String> aggregateFunctions(Base base) {
		if (base.includes(Base.UNIT)) return UNIT_AGGREGATE_FUNCTIONS;
		if (base == Base.GATHERING) return GATHERING_AGGREGATE_FUNCTIONS;
		if (base == Base.DOCUMENT) return DOCUMENT_AGGREGATE_FUNCTIONS;
		throw new UnsupportedOperationException("Not implemented for " + base);
	}

	private static final Set<String> UNIT_AGGREGATE_FUNCTIONS =
			Collections.unmodifiableSet(Utils.set(
					COUNT, INDIVIDUAL_COUNT_SUM, INDIVIDUAL_COUNT_MAX, OLDEST_RECORD, NEWEST_RECORD, RECORD_QUALITY_MAX,
					FIRST_LOAD_DATE_MIN, FIRST_LOAD_DATE_MAX, GATHERING_COUNT, SECURED_COUNT,
					PAIR_COUNT_MAX, PAIR_COUNT_SUM, TAXON_COUNT, SPECIES_COUNT, RED_LIST_STATUS_MAX,
					ATLAS_CODE_MAX, ATLAS_CLASS_MAX));

	private static final Set<String> GATHERING_AGGREGATE_FUNCTIONS =
			Collections.unmodifiableSet(Utils.set(COUNT, OLDEST_RECORD, NEWEST_RECORD, LINELENGTH_SUM, FIRST_LOAD_DATE_MIN, FIRST_LOAD_DATE_MAX, SECURED_COUNT));

	private static final Set<String> DOCUMENT_AGGREGATE_FUNCTIONS =
			Collections.unmodifiableSet(Utils.set(COUNT, FIRST_LOAD_DATE_MIN, FIRST_LOAD_DATE_MAX, SECURED_COUNT));

	public static final String EDITOR_ID_FILTER = "editorId";
	public static final String OBSERVER_ID_FILTER = "observerId";
	public static final String EDITOR_OR_OBSERVER_ID_FILTER = "editorOrObserverId";
	public static final String EDITOR_OR_OBSERVER_ID_IS_NOT_FILTER = "editorOrObserverIdIsNot";
	public static final String COLLECTION_ID = "collectionId";
	public static final String RANDOM = "RANDOM";
	public static final String RANDOM_WITH_SEED = "RANDOM:seed";

	public static final Qname INVASIVE_ALIEN_SPECIES_CONTROL_COLLECTION_ID = new Qname("HR.2049");

	public static final Set<String> PROD_PORTALS = Utils.set(
			"KE.389", // laji.fi
			"KE.601", // viranomaiset.laji.fi
			"KE.1161", // vieraslajit.fi
			"KE.1041" // embedded.laji.fi
			);

	public static final Set<String> PROD_NO_PUBLIC_QUERY_LOG = Utils.set(
			PROD_PORTALS,
			"KE.3", // Kotka
			"KE.6", // id redirect service
			"KE.861", // viekas
			"KE.1541", // finbif2gbif
			"KE.1221" // Mobiilivihko
			);

	public static final Set<String> PROD_UNLIMITED_API_ACCESS_COUNT = Utils.set(
			PROD_PORTALS,
			"KE.3", // Kotka
			"KE.6", // id redirect service
			"KE.1221" // Mobiilivihko
			);

	public static final Set<String> STAGING_PORTALS = Utils.set(
			"KE.389", // dev.laji.fi
			"KE.841", // beta.laji.fi
			"KE.542", // laji.fi localhost
			"KE.601", // viranomaiset.laji.fi
			"KE.963", // viranomaiset localhost
			"KE.781" // vieraslajit-dev.laji.fi
			);

	public static final Set<String> STAGING_NO_PUBLIC_QUERY_LOG = Utils.set(
			STAGING_PORTALS,
			"KE.3", // kotka
			"KE.6", // id redirect service
			"KE.1301" // finbif2gbif test
			);

	public static final Set<String> STAGING_UNLIMITED_API_ACCESS_COUNT = Utils.set(
			STAGING_PORTALS,
			"KE.3", // kotka
			"KE.6" // id redirect service
			);

	public static final Qname KOTKA = new Qname("KE.3");
	public static final Qname KASTIKKA = new Qname("KE.167");

	public static final Set<Qname> NO_MEDIA_SECURE = Utils.set(KOTKA, KASTIKKA);

	public static final Set<Qname> BIRD_LINE_TRANSECT_COLLECTION = Utils.set(new Qname("HR.61"), new Qname("HR.2691"), new Qname("HR.2692"), new Qname("HR.4731"));
	public static final Set<Qname> BIRD_POINT_COUNT_COLLECTION = Utils.set(new Qname("HR.157"));

	public static final Qname WATERBIRD_LUKE_COLLECTION = new Qname("HR.3992");
	public static final Qname WATERBIRD_LUOMUS_COLLECTION = new Qname("HR.3991");
	public static final Qname WATERBIRD_TOP_COLLECTION = new Qname("HR.62");
	public static final Set<Qname> WATERBIRD_COLLECTION = Utils.set(WATERBIRD_LUKE_COLLECTION, WATERBIRD_LUOMUS_COLLECTION, WATERBIRD_TOP_COLLECTION);
	public static final Qname WATERBIRD_PAIRCOUNT_FORM_ID = new Qname("MHL.65");

	public static final Set<Qname> COMPLETE_LIST_COLLECTION = Utils.set(
			new Qname("HR.5615"),
			new Qname("HR.5236"),
			new Qname("HR.5575"),
			new Qname("HR.5095"),
			new Qname("HR.5235"),
			new Qname("HR.5256"),
			new Qname("HR.5259"),
			new Qname("HR.5257"),
			new Qname("HR.5255"),
			new Qname("HR.5260"),
			new Qname("HR.5258")
			);

	public static final Set<String> PERSON_INFO_FACTS = Utils.set(
			"email",
			"city",
			"emailAddress",
			"inheritedName",
			"phoneNumber",
			"postalCode",
			"preferredName",
			"streetAddress",
			"MZ.city",
			"MZ.country",
			"MA.emailAddress",
			"MA.inheritedName",
			"MZ.phoneNumber",
			"MZ.postalCode",
			"MA.preferredName",
			"MZ.streetAddress",
			new Qname("MZ.city").toURI(),
			new Qname("MZ.country").toURI(),
			new Qname("MA.emailAddress").toURI(),
			new Qname("MA.inheritedName").toURI(),
			new Qname("MZ.phoneNumber").toURI(),
			new Qname("MZ.postalCode").toURI(),
			new Qname("MA.preferredName").toURI(),
			new Qname("MZ.streetAddress").toURI()
			);

}
