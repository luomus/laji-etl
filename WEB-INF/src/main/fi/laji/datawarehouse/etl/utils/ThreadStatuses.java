package fi.laji.datawarehouse.etl.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.etl.models.containers.ThreadStatusData;
import fi.luomus.commons.containers.rdf.Qname;

public class ThreadStatuses {

	private final Map<Qname, Map<Class<?>, ThreadStatusReporter>> sourceThreads = new HashMap<>();
	private final Map<Class<?>, ThreadStatusReporter> otherThreads = new HashMap<>();

	public ThreadStatusReporter getThreadStatusReporterFor(Qname source, Class<?> threadClass) {
		if (!sourceThreads.containsKey(source)) {
			sourceThreads.put(source, new HashMap<Class<?>, ThreadStatusReporter>());
		}
		if (!sourceThreads.get(source).containsKey(threadClass)) {
			sourceThreads.get(source).put(threadClass, new ThreadStatusReporter());
		}
		return sourceThreads.get(source).get(threadClass);
	}

	public ThreadStatusReporter getThreadStatusReporterFor(Class<?> threadClass) {
		if (!otherThreads.containsKey(threadClass)) {
			otherThreads.put(threadClass, new ThreadStatusReporter());
		}
		return otherThreads.get(threadClass);
	}

	public void reportThreadDead(Qname source, Class<?> threadClass) {
		if (sourceThreads.containsKey(source)) {
			sourceThreads.get(source).remove(threadClass);
		}
	}

	public void reportThreadDead(Class<?> threadClass) {
		otherThreads.remove(threadClass);
	}

	public List<ThreadStatusData> getThreadStatuses() {
		List<ThreadStatusData> allStatuses = new ArrayList<>();
		for (Map.Entry<Qname, Map<Class<?>, ThreadStatusReporter>> e : sourceThreads.entrySet()) {
			for (Map.Entry<Class<?>, ThreadStatusReporter> e2 : e.getValue().entrySet()) {
				allStatuses.add(new ThreadStatusData(e.getKey(), e2.getKey().getSimpleName(), e2.getValue().getStatus()));
			}
		}
		for (Map.Entry<Class<?>, ThreadStatusReporter> e : otherThreads.entrySet()) {
			Class<?> threadClass = e.getKey();
			ThreadStatusReporter reporter = e.getValue();
			allStatuses.add(new ThreadStatusData(Const.LAJI_ETL_QNAME, threadClass.getSimpleName(), reporter.getStatus()));
		}
		return allStatuses;
	}

}
