package fi.laji.datawarehouse.etl.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.ReflectionUtil;

public class JsonToModel {

	public static DwRoot rootFromJson(JSONObject jsonObject) throws CriticalParseFailure {
		String documentId = jsonObject.getString("documentId");
		if (!given(documentId)) throw new IllegalStateException("No document id");

		String sourceId = jsonObject.getString("sourceId");
		if (!given(sourceId)) throw new IllegalStateException("No source id");

		DwRoot root = new DwRoot(Qname.fromURI(documentId), Qname.fromURI(sourceId));
		setValues(jsonObject, root);

		return root;
	}

	public static Document documentFromJson(JSONObject jsonObject) throws CriticalParseFailure {
		boolean isPublic = jsonObject.getBoolean("public");
		Qname sourceId = Qname.fromURI(jsonObject.getString("sourceId"));
		Qname documentId = Qname.fromURI(jsonObject.getString("documentId"));
		Qname collectionId = Qname.fromURI(jsonObject.getString("collectionId"));
		Document document = new Document(isPublic ? Concealment.PUBLIC : Concealment.PRIVATE, sourceId, documentId, collectionId);
		setValues(jsonObject, document);
		return document;
	}

	public static Gathering gatheringFromJson(JSONObject gatheringJSON) throws CriticalParseFailure {
		Gathering gathering = new Gathering(Qname.fromURI(gatheringJSON.getString("gatheringId")));
		setValues(gatheringJSON, gathering);
		return gathering;
	}

	public static Unit unitFromJson(JSONObject unitJSON) throws CriticalParseFailure {
		Unit unit = new Unit(Qname.fromURI(unitJSON.getString("unitId")));
		setValues(unitJSON, unit);
		return unit;
	}

	public static Sample sampleFromJson(JSONObject sampleJSON) throws CriticalParseFailure {
		Sample sample = new Sample(Qname.fromURI(sampleJSON.getString("sampleId")));
		setValues(sampleJSON, sample);
		return sample;
	}

	public static IdentificationEvent identificationEventFromJson(JSONObject identificationEventJSON) throws CriticalParseFailure {
		IdentificationEvent identification = new IdentificationEvent();
		setValues(identificationEventJSON, identification);
		return identification;
	}

	public static JoinedRow joinedRowFromJson(JSONObject documentJson, String unitId) throws CriticalParseFailure {
		return joinedRowFromJson(documentJson, unitId, null, null);
	}

	public static JoinedRow joinedRowFromJson(JSONObject documentJson, String unitId, Base base, String id) throws CriticalParseFailure {
		Document document = documentFromJson(documentJson);

		Gathering gathering = null;
		Unit unit = null;
		boolean found = false;
		if (unitId != null) {
			for (Gathering g : document.getGatherings()) {
				for (Unit u : g.getUnits()) {
					if (unitId.equals(u.getUnitId().toURI())) {
						gathering = g;
						unit = u;
						found = true;
						break;
					}
					if (found) break;
				}
			}
			if (gathering == null || unit == null) throw new CriticalParseFailure("Document is missing required unit: " + unitId);
			gathering.getUnits().clear();
		}
		document.getGatherings().clear();

		JoinedRow row = new JoinedRow(unit, gathering, document);

		if (base == Base.ANNOTATION) {
			row.setAnnotation(findAnnotation(unit, id));
		}

		if (base == Base.UNIT_MEDIA) {
			row.setMedia(findMedia(unit, id));
		}

		if (base == Base.SAMPLE) {
			row.setSample(findSample(unit, id));
		}
		return row;
	}

	private static Annotation findAnnotation(Unit unit, String id) {
		for (Annotation a : unit.getAnnotations()) {
			if (id.equals(a.getId().toURI())) return a;
		}
		return null;
	}

	private static MediaObject findMedia(Unit unit, String id) {
		for (MediaObject m : unit.getMedia()) {
			if (id.equals(m.getFullURL())) return m;
		}
		return null;
	}

	private static Sample findSample(Unit unit, String id) {
		for (Sample s : unit.getSamples()) {
			if (id.equals(s.getSampleId().toURI())) return s;
		}
		return null;
	}

	public static Annotation annotationFromJson(JSONObject json) throws CriticalParseFailure {
		Qname annotationId = Qname.fromURI(json.getString("id"));
		Qname rootId = Qname.fromURI(json.getString("rootID"));
		if (!json.hasKey("targetID")) {
			return Annotation.deletedAnnotation(annotationId, rootId);
		}
		Qname targetId = Qname.fromURI(json.getString("targetID"));
		Long created = Long.valueOf(json.getInteger("createdTimestamp"));
		Annotation annotation = new Annotation(annotationId, rootId, targetId, created);
		setValues(json, annotation);
		return annotation;
	}

	public static <T> T objectFromJson(JSONObject json, T object) throws CriticalParseFailure {
		setValues(json, object);
		return object;
	}

	private static void setValues(JSONObject jsonObject, Object object) throws CriticalParseFailure {
		Map<String, Method> methods = ReflectionUtil.getSettersMap(object.getClass());
		for (String fieldName : methods.keySet()) {
			if (!jsonObject.hasKey(fieldName)) continue;
			try {
				Method method = methods.get(fieldName);
				Object value = getValue(jsonObject, fieldName, method);
				if (value == null) continue;
				method.invoke(object, value);
			} catch (CriticalParseFailure e) {
				throw e.addField(fieldName);
			} catch (Throwable e) {
				throw new CriticalParseFailure(ExceptionUtils.getRootCause(e)).addField(fieldName);
			}
		}
	}

	private static Object getValue(JSONObject jsonObject, String fieldName, Method method) throws Exception {
		Class<?> classOfValue = method.getParameterTypes()[0];
		JSONDeSerializer deSerializer = CLASS_DESERIALIZERS.get(classOfValue);
		if (deSerializer != null) {
			return deSerializer.getValue(jsonObject, fieldName, method);
		}
		if (classOfValue.isEnum()) {
			return ENUM_SERIALIZER.getValue(jsonObject, fieldName, method);
		}
		throw new UnsupportedOperationException("Not yet implemented for " + classOfValue + " : " + fieldName);
	}

	private static boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private interface JSONDeSerializer {
		Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure, Exception;
	}

	private static final Map<Class<?>, JSONDeSerializer> CLASS_DESERIALIZERS = initClassSerializers();

	private static Map<Class<?>, JSONDeSerializer> initClassSerializers() {
		Map<Class<?>, JSONDeSerializer> map = new HashMap<>();

		map.put(Geo.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				try {
					return Geo.fromGeoJSON(jsonObject.getObject(fieldName));
				} catch (DataValidationException e) {
					throw new ETLException(fieldName, e);
				}
			}
		});

		map.put(Qname.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				String value = jsonObject.getString(fieldName);
				if (!given(value)) return null;
				Qname qname = null;
				if (value.startsWith("http")) {
					qname = Qname.fromURI(value);
				} else {
					qname = new Qname(value);
				}
				if (!qname.isSet()) return null;
				return qname;
			}
		});

		map.put(Identification.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				Identification identification = new Identification();
				setValues(jsonObject.getObject(fieldName), identification);
				return identification;
			}
		});

		map.put(OccurrenceAtTimeOfAnnotation.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				OccurrenceAtTimeOfAnnotation o = new OccurrenceAtTimeOfAnnotation();
				setValues(jsonObject.getObject(fieldName), o);
				return o;
			}
		});

		map.put(DocumentQuality.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				DocumentQuality documentQuality = new DocumentQuality();
				setValues(jsonObject.getObject(fieldName), documentQuality);
				return documentQuality;
			}
		});

		map.put(GatheringQuality.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				GatheringQuality gatheringQuality = new GatheringQuality();
				setValues(jsonObject.getObject(fieldName), gatheringQuality);
				return gatheringQuality;
			}
		});

		map.put(UnitQuality.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				UnitQuality unitQuality = new UnitQuality();
				setValues(jsonObject.getObject(fieldName), unitQuality);
				return unitQuality;
			}
		});

		map.put(Quality.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				jsonObject = jsonObject.getObject(fieldName);
				@SuppressWarnings("deprecation")
				Quality quality = new Quality();
				Quality.Issue issue = jsonObject.hasKey("issue") ? Quality.Issue.valueOf(jsonObject.getString("issue")) : null;
				Quality.Source source = jsonObject.hasKey("source") ? Quality.Source.valueOf(jsonObject.getString("source")) : null;
				String message = jsonObject.getString("message");
				quality.setIssue(issue);
				quality.setSource(source);
				if (given(message)) {
					quality.setMessage(message);
				}
				return quality;
			}
		});

		map.put(UnitInterpretations.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				UnitInterpretations interpretations = new UnitInterpretations();
				setValues(jsonObject.getObject(fieldName), interpretations);
				return interpretations;
			}
		});

		map.put(GatheringInterpretations.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				GatheringInterpretations interpretations = new GatheringInterpretations();
				setValues(jsonObject.getObject(fieldName), interpretations);
				return interpretations;
			}
		});

		map.put(GatheringConversions.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				GatheringConversions conversions = new GatheringConversions();
				setValues(jsonObject.getObject(fieldName), conversions);
				return conversions;
			}
		});

		map.put(Coordinates.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				jsonObject = jsonObject.getObject(fieldName);
				double latMin = jsonObject.getDouble("latMin");
				double latMax = jsonObject.getDouble("latMax");
				double lonMin = jsonObject.getDouble("lonMin");
				double lonMax = jsonObject.getDouble("lonMax");
				Integer accuracy = jsonObject.hasKey("accuracyInMeters") ? jsonObject.getInteger("accuracyInMeters") : null;
				Coordinates.Type type = Coordinates.Type.valueOf(jsonObject.getString("type"));
				Coordinates coordinates = new Coordinates(); // don't use constructor to avoid validation
				coordinates.setLatMin(latMin);
				coordinates.setLatMax(latMax);
				coordinates.setLonMin(lonMin);
				coordinates.setLonMax(lonMax);
				coordinates.setType(type);
				coordinates.setAccuracyInMeters(accuracy);
				return coordinates;
			}
		});

		map.put(SingleCoordinates.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				jsonObject = jsonObject.getObject(fieldName);
				double lat = jsonObject.getDouble("lat");
				double lon = jsonObject.getDouble("lon");
				Coordinates.Type type = Coordinates.Type.valueOf(jsonObject.getString("type"));
				SingleCoordinates coordinates = new SingleCoordinates();
				coordinates.setLat(lat);
				coordinates.setLon(lon);
				coordinates.setType(type);
				return coordinates;
			}
		});

		map.put(DateRange.class, new JSONDeSerializer() {
			@SuppressWarnings("deprecation")
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				jsonObject = jsonObject.getObject(fieldName);
				Date begin = dateFromIso(jsonObject.getString("begin"));
				Date end = dateFromIso(jsonObject.getString("end"));
				DateRange dateRange = new DateRange(); // don't user constructor to avoid validation
				dateRange.setBegin(begin);
				dateRange.setEnd(end);
				return dateRange;
			}
		});

		map.put(Date.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				return dateFromIso(jsonObject.getString(fieldName));
			}
		});

		map.put(Integer.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				try {
					return jsonObject.getInteger(fieldName);
				} catch (Exception e) {
					return null;
				}
			}
		});

		map.put(Long.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				try {
					Integer i = jsonObject.getInteger(fieldName);
					return Long.valueOf(i);
				} catch (Exception e) {
					return null;
				}
			}
		});

		map.put(Double.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				try {
					return jsonObject.getDouble(fieldName);
				} catch (Exception e) {
					return null;
				}
			}
		});

		map.put(int.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				return jsonObject.getInteger(fieldName);
			}
		});

		map.put(double.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				return jsonObject.getDouble(fieldName);
			}
		});

		map.put(String.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				return jsonObject.getString(fieldName).trim();
			}
		});

		map.put(Document.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws CriticalParseFailure {
				JSONObject documentJson = jsonObject.getObject(fieldName);
				Concealment concealment = Concealment.valueOf(documentJson.getString("concealment"));
				Qname sourceId = Qname.fromURI(jsonObject.getString("sourceId"));
				Qname documentId = Qname.fromURI(jsonObject.getString("documentId"));
				Qname collectionId = Qname.fromURI(jsonObject.getString("collectionId"));
				Document document = new Document(concealment, sourceId, documentId, collectionId);
				setValues(documentJson, document);
				return document;
			}
		});

		map.put(boolean.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				return jsonObject.getBoolean(fieldName);
			}
		});

		map.put(Boolean.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
				return jsonObject.getBoolean(fieldName);
			}
		});

		map.put(ArrayList.class, new JSONDeSerializer() {
			@Override
			public Object getValue(JSONObject jsonObject, String fieldName, Method method) throws Exception {
				try {
					ParameterizedType type = (ParameterizedType) method.getGenericParameterTypes()[0];
					Type typeOfType = type.getActualTypeArguments()[0];
					if (!(typeOfType instanceof Class)) throw new IllegalStateException();

					Class<?> listValueClass = (Class<?>) typeOfType;
					if (listValueClass.isEnum()) {
						return getEnumList(jsonObject, fieldName, listValueClass);
					} if (listValueClass.equals(String.class)) {
						return getStringList(jsonObject, fieldName);
					} if (listValueClass.equals(Qname.class)) {
						return getQnameList(jsonObject, fieldName);
					}
					return getObjectList(jsonObject, fieldName, listValueClass);
				} catch (ETLException | CriticalParseFailure e) {
					throw e;
				} catch (Exception e) {
					throw new ETLException(fieldName, e);
				}
			}

			private Object getObjectList(JSONObject jsonObject, String fieldName, Class<?> listValueClass) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ETLException, CriticalParseFailure {
				Constructor<?> constructor = getConstructor(listValueClass);
				ArrayList<Object> list = new ArrayList<>();
				for (JSONObject listValueJsonObject : jsonObject.getArray(fieldName).iterateAsObject()) {
					Object listValueObject = initObject(constructor, listValueJsonObject);
					setValues(listValueJsonObject, listValueObject);
					list.add(listValueObject);
				}
				return list;
			}

			private Object initObject(Constructor<?> constructor, JSONObject jsonObject) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
				if (constructor.getDeclaringClass() == Unit.class) {
					return constructor.newInstance(Qname.fromURI(jsonObject.getString("unitId")));
				}
				if (constructor.getDeclaringClass() == Gathering.class) {
					return constructor.newInstance(Qname.fromURI(jsonObject.getString("gatheringId")));
				}
				if (constructor.getDeclaringClass() == Sample.class) {
					return constructor.newInstance(Qname.fromURI(jsonObject.getString("sampleId")));
				}
				return constructor.newInstance();
			}

			private Constructor<?> getConstructor(Class<?> listValueClass) throws NoSuchMethodException {
				if (listValueClass == Unit.class || listValueClass == Gathering.class || listValueClass == Sample.class) {
					return listValueClass.getConstructor(Qname.class);
				}
				return listValueClass.getConstructor();
			}

			private Object getStringList(JSONObject jsonObject, String fieldName) {
				ArrayList<String> list = new ArrayList<>();
				for (String s : jsonObject.getArray(fieldName)) {
					list.add(s.trim());
				}
				return list;
			}

			private Object getQnameList(JSONObject jsonObject, String fieldName) {
				ArrayList<Qname> list = new ArrayList<>();
				for (String s : jsonObject.getArray(fieldName)) {
					list.add(Qname.fromURI(s));
				}
				return list;
			}

			@SuppressWarnings("unchecked")
			private <T extends Enum<?>> Object getEnumList(JSONObject jsonObject, String fieldName, Class<?> enumClass) {
				ArrayList<T> list = new ArrayList<>();
				for (String s : jsonObject.getArray(fieldName)) {
					Object enumValue = getEnumValue(enumClass, s);
					list.add((T) enumValue);
				}
				return list;
			}
		});

		return map;
	}

	private static final JSONDeSerializer ENUM_SERIALIZER = new JSONDeSerializer() {
		@Override
		public Object getValue(JSONObject jsonObject, String fieldName, Method method) {
			Type enumParameterType = method.getParameterTypes()[0];
			if (!(enumParameterType instanceof Class)) throw new IllegalStateException();

			Class<?> enumClass = (Class<?>) enumParameterType;
			if (!enumClass.isEnum()) throw new IllegalStateException();

			String enumString = jsonObject.getString(fieldName);
			Object enumValue = getEnumValue(enumClass, enumString);
			return enumValue;
		}
	};

	private static Object getEnumValue(Class<?> enumClass, String enumValue) {
		for (Object enumConstant : enumClass.getEnumConstants()) {
			if (enumConstant.toString().equals(enumValue)) {
				return enumConstant;
			}
		}
		return null;
	}

	private static final char ZERO = '0';
	private static final int MONTH_DAY_ZERO = ZERO * 11;
	private static final int YEAR_ZERO = ZERO * 1111;

	public static Date dateFromIso(String date) {
		if (!given(date)) return null;
		// Implementation is more performant than using SimpleDateFormat etc
		while (date.length() < 10) {
			date = ZERO + date;
		}
		int year = date.charAt(0) * 1000 + date.charAt(1) * 100 + date.charAt(2) * 10 + date.charAt(3) - YEAR_ZERO;
		int month = date.charAt(5) * 10 + date.charAt(6) - MONTH_DAY_ZERO;
		int day = date.charAt(8) * 10 + date.charAt(9) - MONTH_DAY_ZERO;
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		return c.getTime();
	}

}
