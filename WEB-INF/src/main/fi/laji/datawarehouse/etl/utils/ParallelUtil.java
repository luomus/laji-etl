package fi.laji.datawarehouse.etl.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;

public class ParallelUtil<T> {

	private static class ParallelData<T> {
		private final T data;
		private boolean dataProcessed = false;
		private Exception causedException = null;
		public ParallelData(T data) {
			this.data = data;
		}
	}

	public interface Task<T> {
		void execute(T data) throws Exception;
	}

	private ExecutorService executor;

	public ParallelUtil(ExecutorService executorService) {
		this.executor = executorService;
	}

	public void execute(final Task<T> task, Collection<T> targets) {
		List<Callable<Void>> tasks = new ArrayList<>();
		List<ParallelData<T>> parallelTargets = new ArrayList<>();
		for (T target : targets) {
			parallelTargets.add(new ParallelData<>(target));
		}
		for (final ParallelData<T> target: parallelTargets) {
			Callable<Void> callableTask = new Callable<Void>() {
				@Override
				public Void call() {
					try {
						task.execute(target.data);
						target.dataProcessed = true;
					} catch (Exception e) {
						target.causedException = e;
					}
					return null;
				}
			};
			tasks.add(callableTask);
		}

		try {
			executor.invokeAll(tasks);
		} catch (Exception e) {
			throw new ETLException("Parallel mechanism fail while processing all tasks", e);
		}

		for (ParallelData<T> target: parallelTargets) {
			if (target.causedException != null) {
				throw new ETLException("Parallel task caused exception", target.causedException);
			}
			if (!target.dataProcessed) {
				throw new ETLException("Parallel mechanism fail; some data was not processed but exception was not thrown");
			}
		}

	}

}
