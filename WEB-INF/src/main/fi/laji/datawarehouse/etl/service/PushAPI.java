package fi.laji.datawarehouse.etl.service;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/push/*"})
public class PushAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -1717500765172945882L;
	private static final String HTTP = "http://";

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return processPut(req, res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Qname source = null;
		try {
			source = getSourceFromApiKeyAndValidateAccess(req);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		}

		Qname overridingSource = getOverridingSource(req);
		if (overridingSource != null) {
			try {
				validateOverridingSource(source, overridingSource);
			} catch (IllegalAccessException e) {
				return unloggedAccessError403(e.getMessage(), res, req);
			}
			source = overridingSource;
		}

		Access access = null;
		try {
			access = getAccess(source.toString());
		} catch (TooManyRequestsException e) {
			return tooManyRequests429(res, req, e);
		}

		try {
			String contentType = getContentType(req);
			if (!validContentType(contentType)) return unloggedUserError400(new IllegalArgumentException("Unsupported content-type: " + contentType), res, req);

			String data = readData(req);
			if (!given(data) || data.length() < 5) return unloggedUserError400(new IllegalArgumentException("No data"),  res, req);

			if (!wellFormed(data, contentType)) return unloggedUserError400(new IllegalArgumentException("Data was not well formed " + contentType), res, req);

			try {
				DAO dao = getDao();
				dao.getETLDAO().storeToInPipe(source, data, contentType);
			} catch (Throwable e) {
				return loggedSystemError500(e, source, this.getClass(), res, req);
			}

			runInPipe(source);

			return ok(res);
		} finally {
			access.release();
		}
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Qname source = null;
		try {
			source = getSourceFromApiKeyAndValidateAccess(req);
		} catch (IllegalAccessException e) {
			return unloggedAccessError403(e.getMessage(), res, req);
		}

		Qname overridingSource = getOverridingSource(req);
		if (overridingSource != null) {
			try {
				validateOverridingSource(source, overridingSource);
			} catch (IllegalAccessException e) {
				return unloggedAccessError403(e.getMessage(), res, req);
			}
			source = overridingSource;
		}

		String documentId = req.getParameter("documentId");
		if (!given(documentId)) return unloggedUserError400(new IllegalArgumentException("Must give documentId parameter."), res, req);

		if (!documentId.startsWith(HTTP)) {
			documentId = source.toURI() + "/" + documentId;
		}

		try {
			Util.validateIncomingDocumentId(Qname.fromURI(documentId));
		} catch (Exception e) {
			return unloggedUserError400(e, res, req);
		}

		try {
			DAO dao = getDao();
			String data = "DELETE " + documentId;
			dao.getETLDAO().storeToInPipe(source, data, "text/plain");
		} catch (Throwable e) {
			return loggedSystemError500(e, source, this.getClass(), res, req);
		}

		runInPipe(source);

		return ok(res);
	}

	private void runInPipe(Qname source) {
		try {
			getThreadHandler().runInPipe(source);
		} catch (Exception e) {
			getDao().logError(Const.LAJI_ETL_QNAME, this.getClass(), source.toString(), e);
		}
	}

	private void validateOverridingSource(Qname sourceId, Qname overridingSourceId) throws IllegalAccessException {
		Source source = getDao().getSources().get(sourceId.toURI());
		if (!source.getValidOverridingSources().contains(overridingSourceId)) {
			throw new IllegalAccessException("Your " + Const.API_KEY + " has not been given permissions " +
					"to use the source given in sourceId parameter.");
		}
		Source overridingSource = getDao().getSources().get(overridingSourceId.toURI());
		if (overridingSource == null) {
			unknownSystem();
		}
	}

	private Qname getOverridingSource(HttpServletRequest req) {
		String s = req.getParameter("sourceId");
		if (s == null) return null;
		return new Qname(s);
	}

	protected Qname getSourceFromApiKeyAndValidateAccess(HttpServletRequest req) throws IllegalAccessException, Exception {
		ApiUser apiUser = getApiUser(req);
		if (apiUser.getSystemId() == null) {
			unknownSystem();
		}
		Qname sourceId = apiUser.getSystemId();
		Source source = getDao().getSources().get(apiUser.getSystemId().toURI());
		if (source == null) {
			unknownSystem();
		}
		return sourceId;
	}

	private void unknownSystem() throws IllegalAccessException {
		throw new IllegalAccessException("" +
				"Please ask for your system to be registered in FinBIF and your " + Const.API_KEY +
				" to be assosiated with that system!");
	}

	protected String readData(HttpServletRequest req) throws IOException {
		BufferedReader reader = req.getReader();
		StringBuilder b = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			b.append(line).append("\n");
		}
		String data = b.toString();
		return data;
	}

}
