package fi.laji.datawarehouse.etl.service;

import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.threads.custom.KastikkaPullReader;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.query.download.model.DownloadRequestRunnableTask;

class TenMinuteTasks implements Runnable {

	private final InitializationServlet initializationServlet;

	public TenMinuteTasks(InitializationServlet initializationServlet) {
		this.initializationServlet = initializationServlet;
	}

	@Override
	public void run() {
		ThreadStatusReporter statusReporter = initializationServlet.getThreadStatuses().getThreadStatusReporterFor(this.getClass());
		try {
			statusReporter.setStatus("Ten minute tasks starting..");
			startTasks();
		} catch (Throwable e) {
			log("start ten minute", e);
		} finally {
			initializationServlet.getThreadStatuses().reportThreadDead(this.getClass());
		}
	}

	private void startTasks() {
		try {
			startToHandleDownloadRequests();
		} catch (Exception e) {
			log("download requests", e);
		}
		try {
			sendNotifications();
		} catch (Exception e) {
			log("notifications", e);
		}
		try {
			startKastikkaPull();
		} catch (Exception e) {
			log("kastikka pull", e);
		}
		try {
			callGeoUpdate();
		} catch (Throwable e) {
			log("update geo tables", e);
		}
		try {
			startReadersForSourcesWithSomethingUnprosessed();
		} catch (Exception e) {
			log("start threads with unprocessed", e);
		}
	}

	private void callGeoUpdate() {
		initializationServlet.getDao().getPublicVerticaDAO().callGeoUpdate();
		initializationServlet.getDao().getPrivateVerticaDAO().callGeoUpdate();
	}

	private void startKastikkaPull() {
		if (!initializationServlet.getConfig().productionMode()) return;
		initializationServlet.getThreadHandler().runPullReader(KastikkaPullReader.SOURCE);
	}

	private void startReadersForSourcesWithSomethingUnprosessed() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					initializationServlet.startReadersForSourcesWithSomethingUnprosessed(initializationServlet.getDao());
				} catch (Exception e) {
					log("start threads with unprocessed", e);
				}
			}
		});
		t.setName("Ten Minute Tasks Thread");
		t.setDaemon(true);
		t.start();
	}

	private void sendNotifications() {
		DAO dao = initializationServlet.getDao();
		List<AnnotationNotification> notifications = dao.getETLDAO().getUnsentNotifications();
		for (AnnotationNotification notification : notifications) {
			try {
				dao.sendNotification(notification);
				dao.getETLDAO().markNotificationSent(notification.getId());
			} catch (Exception e) {
				log(""+notification.getId() + " " + notification.getAnnotation().getId().toURI(), e);
			}
		}
	}

	private void startToHandleDownloadRequests() {
		new DownloadRequestRunnableTask(
				this.initializationServlet.getConfig(),
				this.initializationServlet.getDao(),
				this.initializationServlet.getLocalizedTexts(),
				this.initializationServlet.getThreadStatuses())
		.handle();
	}

	private void log(String info, Throwable e) {
		this.initializationServlet.getDao().logError(Const.LAJI_ETL_QNAME, this.getClass(), info, e);
	}

}