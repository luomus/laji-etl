package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/logout/*"})
public class Logout extends UIBaseServlet {

	private static final long serialVersionUID = -2727247353104385980L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		getSession(req).removeAuthentication(getConfig().systemId());
		return redirectTo(getConfig().baseURL()+"/console/login");
	}

}
