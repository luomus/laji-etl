package fi.laji.datawarehouse.etl.service.console;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.FileDecompresser;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/gbif-reload"})
public class StartGBIFReload extends UIBaseServlet {

	private static final Qname EARTHCAPE_SYSTEM_ID = new Qname("KE.881");
	private static final String TEXT_CSV = "text/csv";
	private static final long serialVersionUID = 7756319770089253518L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadDatasets();
					getThreadHandler().runInPipe(EARTHCAPE_SYSTEM_ID);
				} catch (Throwable e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartGBIFReload.class, null, e);
				}
			}

		}).start();

		return ok(res);
	}

	private void loadDatasets() {
		getDao().logMessage(Const.LAJI_ETL_QNAME, StartGBIFReload.class, "Loading GBIF dataset endpoints");
		Collection<Pair<Qname, Collection<URI>>> endpoints = getDao().getGBIFDatasetEndpoints();
		getDao().logMessage(Const.LAJI_ETL_QNAME, StartGBIFReload.class, "Loaded " + endpoints.size() + " GBIF dataset endpoints");

		for (Pair<Qname, Collection<URI>> endpointData : endpoints) {
			tryToLoad(endpointData);
		}
		getDao().logMessage(Const.LAJI_ETL_QNAME, StartGBIFReload.class, "Done!");
	}

	private void tryToLoad(Pair<Qname, Collection<URI>> endpointData) {
		int attemptCount = 0;
		while (attemptCount++ < 3) {
			try {
				loadDatasetFromEndpoints(endpointData);
				sleep(1000); // Rate limit
				return;
			} catch (Exception e) {
				getDao().logError(Const.LAJI_ETL_QNAME, StartGBIFReload.class, endpointData.getKey().toURI(), new ETLException("Failed to load collection " + endpointData.getKey() + " ... will reattempt in 10 seconds ...", e));
				sleep(10000);
			}
		}
		getDao().logError(Const.LAJI_ETL_QNAME, StartGBIFReload.class, endpointData.getKey().toURI(), new ETLException("Giving up on loading collection " + endpointData.getKey()));
	}

	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e1) {}
	}

	private void loadDatasetFromEndpoints(Pair<Qname, Collection<URI>> endpointData) {
		Qname collectionId = endpointData.getKey();
		CollectionMetadata collectionMetadata = getDao().getCollections().get(collectionId.toURI());
		if (collectionMetadata == null) throw new ETLException("Collection metadata missing for " + collectionId);
		for (URI endpoint : endpointData.getValue()) {
			tryToLoadDatasetFromEndpoint(collectionId, collectionMetadata, endpoint);
		}
	}

	private static class Counts {
		private int upserts = 0;
		private int deletions = 0;
	}

	private void tryToLoadDatasetFromEndpoint(Qname collectionId, CollectionMetadata collectionMetadata, URI endpoint) {
		getDao().logMessage(Const.LAJI_ETL_QNAME, StartGBIFReload.class, "Starting to load occurrences for " + collectionId + " " + collectionMetadata.getName().forLocale("en") + " (" + endpoint + ")...");
		try {
			Counts counts = loadDatasetFromEndpoint(endpoint, collectionId);
			getDao().logMessage(Const.LAJI_ETL_QNAME, StartGBIFReload.class, "Loading occurrences for " + collectionId + " (" + endpoint + ") completed! Upserts " + counts.upserts + " deletions " + counts.deletions);
		} catch (Exception e) {
			getDao().logError(Const.LAJI_ETL_QNAME, StartGBIFReload.class, endpoint.toString(), new ETLException("Failed to load from endpoint " + endpoint + " for collection " + collectionId, e));
		}
	}

	private Counts loadDatasetFromEndpoint(URI endpoint, Qname collectionId) throws Exception {
		HttpClientService client = null;
		String guid = Utils.generateGUID();
		File tmpDecompressedFolder = new File(getTempFolder(), guid);
		File tmpZipLocation = new File(getTempFolder(), guid+".zip");
		try (FileOutputStream out = new FileOutputStream(tmpZipLocation)) {
			client = new HttpClientService();
			client.contentToStream(new HttpGet(endpoint), out);
			return loadDatasetFromZipFile(tmpZipLocation, tmpDecompressedFolder, collectionId);
		} finally {
			if (client != null) client.close();
			tmpZipLocation.delete();
			org.apache.commons.io.FileUtils.deleteDirectory(tmpDecompressedFolder);
		}
	}

	private String getTempFolder() {
		return getConfig().get("TempFolder");
	}

	private Counts loadDatasetFromZipFile(File tmpZipLocation, File tmpDecompressedFolder, Qname collectionId) throws Exception {
		FileDecompresser decompresser = new FileDecompresser(tmpZipLocation, tmpDecompressedFolder);
		boolean ok = decompresser.decompress();
		if (!ok) throw new ETLException("Failed to decompress " + tmpZipLocation.getAbsolutePath());
		List<File> csvFiles = getCSVFiles(decompresser);
		return loadDatasetFromCSVFiles(csvFiles, collectionId);
	}

	private List<File> getCSVFiles(FileDecompresser decompresser) {
		return decompresser.getDecompressedFiles().stream().filter(f->f.getName().endsWith(".csv")).collect(Collectors.toList());
	}

	private Counts loadDatasetFromCSVFiles(List<File> csvFiles, Qname collectionId) throws Exception {
		Counts counts = new Counts();
		for (File csv : csvFiles) {
			loadDatasetFromCSVFile(csv, collectionId, counts);
		}
		return counts;
	}

	private void loadDatasetFromCSVFile(File csv,  Qname collectionId, Counts counts) throws Exception {
		List<String> lines = FileUtils.readLines(csv);
		int upserts = lines.size() - 1;
		StringBuilder b = new StringBuilder();
		boolean header = true;
		for (String line : lines) {
			if (header) {
				b.append("collectionId,");
				header = false;
			} else {
				b.append(collectionId.toString()).append(",");
			}
			b.append(line).append("\n");
		}
		getDao().getETLDAO().storeToInPipe(EARTHCAPE_SYSTEM_ID, b.toString(), TEXT_CSV);
		counts.upserts += upserts;
	}

}
