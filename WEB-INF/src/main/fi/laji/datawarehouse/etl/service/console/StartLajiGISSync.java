package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/lajigis-sync"})
public class StartLajiGISSync extends UIBaseServlet {

	private static final long serialVersionUID = 8909214038153201380L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		getThreadHandler().runPullReader(LajiGISPullReader.SOURCE);
		return ok(res);
	}

}
