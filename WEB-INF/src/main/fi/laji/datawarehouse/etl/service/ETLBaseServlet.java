package fi.laji.datawarehouse.etl.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.json.XML;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.etl.models.DataIsWellFormedValidators;
import fi.laji.datawarehouse.etl.models.DataIsWellFormedValidators.WellFormedValidator;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.harmonizers.DarwinCoreArchiveHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.DeleteRequestHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.Harmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.JSONHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.XMLHarmonizer;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.AccessLimiter;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.Access;
import fi.laji.datawarehouse.etl.utils.AccessLimiter.TooManyRequestsException;
import fi.laji.datawarehouse.etl.utils.AccessMonitor;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Const.ResponseFormat;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.BaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.LogUtils;

public abstract class ETLBaseServlet extends BaseServlet {

	private static final long serialVersionUID = 2722689380289699296L;

	private static final Object LOCK = new Object();

	public static ResponseFormat DEFAULT_FORMAT = ResponseFormat.JSON;

	private static Map<String, ResponseFormat> CONTENT_TYPE_TO_FORMAT;
	static {
		CONTENT_TYPE_TO_FORMAT = new HashMap<>();
		CONTENT_TYPE_TO_FORMAT.put("json", ResponseFormat.JSON);
		CONTENT_TYPE_TO_FORMAT.put("xml", ResponseFormat.XML);
		CONTENT_TYPE_TO_FORMAT.put("rdf+xml", ResponseFormat.RDF_XML);
		CONTENT_TYPE_TO_FORMAT.put("dwc_xml", ResponseFormat.RDF_XML);
		CONTENT_TYPE_TO_FORMAT.put("rdf_xml", ResponseFormat.RDF_XML);
		CONTENT_TYPE_TO_FORMAT.put("dwc+rdf+xml", ResponseFormat.RDF_XML);
		CONTENT_TYPE_TO_FORMAT.put("text/plain", ResponseFormat.TEXT_PLAIN);
		CONTENT_TYPE_TO_FORMAT.put("plain", ResponseFormat.TEXT_PLAIN);
		CONTENT_TYPE_TO_FORMAT.put("text/csv", ResponseFormat.TEXT_CSV);
		CONTENT_TYPE_TO_FORMAT.put("csv", ResponseFormat.TEXT_CSV);
		CONTENT_TYPE_TO_FORMAT.put("text/tsv", ResponseFormat.TEXT_TSV);
		CONTENT_TYPE_TO_FORMAT.put("tsv", ResponseFormat.TEXT_TSV);
		CONTENT_TYPE_TO_FORMAT.put("geo+json", ResponseFormat.GEO_JSON);
		CONTENT_TYPE_TO_FORMAT.put("geojson", ResponseFormat.GEO_JSON);
		CONTENT_TYPE_TO_FORMAT.put("geo_json", ResponseFormat.GEO_JSON);
		CONTENT_TYPE_TO_FORMAT.put("wkt", ResponseFormat.TEXT_PLAIN);
	}

	public static int MAX_PENDING_REQUESTS = 12;

	private static final AccessLimiter ACCESS_LIMITER = new AccessLimiter(MAX_PENDING_REQUESTS);

	@Override
	protected String configFileName() {
		return "laji-etl.properties";
	}

	@Override
	protected void applicationInit() {}

	@Override
	protected void applicationInitOnlyOnce() {}

	@Override
	protected void applicationDestroy() {
		if (dao != null) {
			dao.close();
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	protected ResponseData unloggedAccessError403(String error, HttpServletResponse res, HttpServletRequest req) {
		return error(403, res, req, error);
	}

	private ResponseData unloggedUserError400(String error, HttpServletResponse res, HttpServletRequest req) {
		return error(400, res, req, error);
	}

	protected ResponseData unloggedNotFoundError404(String error, HttpServletResponse res, HttpServletRequest req) {
		return error(404, res, req, error);
	}

	protected ResponseData unloggedUserError400(Exception e, HttpServletResponse res, HttpServletRequest req) {
		if (e.getMessage() != null) {
			return unloggedUserError400(e.getMessage(), res, req);
		}
		return unloggedUserError400(e.getClass().getSimpleName(), res, req);
	}

	protected ResponseData loggedSystemError500(Throwable e, Class<?> phase, HttpServletResponse res, HttpServletRequest req) {
		return loggedSystemError500(e, Const.LAJI_ETL_QNAME, phase, res, req);
	}

	protected ResponseData loggedSystemError500(Throwable e, Qname source, Class<?> phase, HttpServletResponse res, HttpServletRequest req) {
		String user = req.getRemoteUser();
		if (!given(user)) {
			SessionHandler session = getSession(req, false);
			if (session.hasSession()) {
				user = session.userId();
			}
		}
		return loggedSystemError500(e, source, phase, user, res, req);
	}

	protected ResponseData loggedSystemError500(Throwable e, Qname source, Class<?> phase, String user, HttpServletResponse res, HttpServletRequest req) {
		String parameters = buildLogMessage(req);
		getDao().logError(source, phase, user, new Exception(parameters, e));
		getErrorReporter().report(phase.getName() + " " + parameters, e);
		String message = LogUtils.buildStackTrace(e);
		return error(500, res, req, message);
	}

	protected ResponseData tooManyRequests429(HttpServletResponse res, HttpServletRequest req, TooManyRequestsException e) {
		System.out.println(e.getMessage());
		logButNotTooOften(req, e);
		return error(429, res, req, e.getMessage());
	}

	private static Map<String, Long> exceptionLogged = new HashMap<>();
	private static final long LOG_COOLDOWN = 12 * 60 * 60 * 1000; // 12 hours in milliseconds

	private void logButNotTooOften(HttpServletRequest req, TooManyRequestsException e) {
		try {
			ApiUser user = getApiUser(req);
			long currentTime = System.currentTimeMillis();
			long lastLogTime = exceptionLogged.getOrDefault(user.getSystemIdOrEmail(), 0L);
			if (currentTime - lastLogTime > LOG_COOLDOWN) {
				Qname systemId = user.getSystemId() != null ? user.getSystemId() : Const.LAJI_ETL_QNAME;
				getDao().logError(systemId, getClass(), user.getSystemIdOrEmail(), e);
				getErrorReporter().report(TooManyRequestsException.class.getSimpleName() + ": " + user.getSystemIdOrEmail() + " " + e.getMessage(), e);
				exceptionLogged.put(user.getSystemIdOrEmail(), currentTime);
			}
		} catch (Exception e2) {
			// can't resolve api user... should not happen at this stage
		}
	}

	protected ResponseData error(int status, HttpServletResponse res, HttpServletRequest req, String message) {
		res.setStatus(status);
		ResponseFormat format = getFormatWithoutExceptions(req);
		if (format == ResponseFormat.TEXT_PLAIN) {
			return response(message, "text/plain");
		}
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.setInteger("status", status);
		jsonResponse.setString("message", message);
		if (format == ResponseFormat.JSON || format == ResponseFormat.GEO_JSON) {
			return jsonResponse(jsonResponse);
		}
		String xml = "<error>" + XML.toString(jsonResponse.reveal()) + "</error>";
		return response(xml, "application/xml");
	}

	protected ResponseData error(int status, HttpServletResponse res, HttpServletRequest req, LocalizedText message) {
		res.setStatus(status);
		ResponseFormat format = getFormatWithoutExceptions(req);
		if (format == ResponseFormat.TEXT_PLAIN) {
			return response(message.forLocale("en"), "text/plain");
		}
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.setInteger("status", status);
		jsonResponse.setString("message", message.forLocale("en"));
		jsonResponse.setObject("localizedMessage", new JSONObject()
				.setString("fi", message.forLocale("fi"))
				.setString("en", message.forLocale("en"))
				.setString("sv", message.forLocale("sv")));
		if (format == ResponseFormat.JSON || format == ResponseFormat.GEO_JSON) {
			return jsonResponse(jsonResponse);
		}
		String xml = "<error>" + XML.toString(jsonResponse.reveal()) + "</error>";
		return response(xml, "application/xml");
	}

	protected ResponseData ok(HttpServletResponse res) throws IOException {
		res.setContentType("text/plain; charset=utf-8");
		res.setStatus(200);
		PrintWriter out = res.getWriter();
		out.write("ok");
		out.flush();
		return new ResponseData().setOutputAlreadyPrinted();
	}

	protected boolean given(Object o) {
		return o != null && o.toString().trim().length() > 0;
	}

	protected boolean given(Object[] array) {
		return array != null && array.length > 0;
	}

	private static ThreadStatuses threadStatuses = new ThreadStatuses();

	protected ThreadStatuses getThreadStatuses() {
		return threadStatuses;
	}

	private static ThreadHandler handler = null;

	protected ThreadHandler getThreadHandler() {
		if (handler == null) {
			synchronized (LOCK) {
				if (handler == null) {
					handler = new ThreadHandler(getDao(), initHarmonizers(getDao()), threadStatuses);
				}
			}
		}
		return handler;
	}

	private static final Map<String, WellFormedValidator> SUPPORTED_CONTENT_TYPES = initSupportedContentTypes();

	private static Map<String, WellFormedValidator> initSupportedContentTypes() {
		Map<String, WellFormedValidator> contentTypes = new HashMap<>();
		contentTypes.put("application/rdf+xml", DataIsWellFormedValidators.XML_VALIDATOR);
		contentTypes.put("application/xml", DataIsWellFormedValidators.XML_VALIDATOR);
		contentTypes.put("application/json", DataIsWellFormedValidators.JSON_VALIDATOR);
		contentTypes.put("text/plain", DataIsWellFormedValidators.TEXT_PLAIN_VALIDATOR);
		contentTypes.put("text/csv", DataIsWellFormedValidators.TEXT_CSV_TSV_VALIDATOR);
		contentTypes.put("text/tsv", DataIsWellFormedValidators.TEXT_CSV_TSV_VALIDATOR);
		return contentTypes;
	}

	private Map<String, Harmonizer<String>> initHarmonizers(DAO dao) {
		Map<String, Harmonizer<String>> harmonizers = new HashMap<>();
		harmonizers.put("application/rdf+xml", new RdfXmlHarmonizer(dao));
		harmonizers.put("application/xml", new XMLHarmonizer());
		harmonizers.put("application/json", new JSONHarmonizer(dao.getContextDefinitions(), dao));
		harmonizers.put("text/plain", new DeleteRequestHarmonizer());
		harmonizers.put("text/csv", new DarwinCoreArchiveHarmonizer(CSVFormat.RFC4180));
		harmonizers.put("text/tsv", new DarwinCoreArchiveHarmonizer(CSVFormat.TDF));
		return harmonizers;
	}

	protected boolean validContentType(String contentType) {
		return SUPPORTED_CONTENT_TYPES.containsKey(contentType);
	}

	protected boolean wellFormed(String data, String contentType) {
		return SUPPORTED_CONTENT_TYPES.get(contentType).isWellFormed(data);
	}

	private static DAO dao = null;

	protected DAO getDao() {
		if (dao == null) {
			synchronized (LOCK) {
				if (dao == null) {
					Config config = getConfig();
					dao = new DAOImple(config, config.get("Vertica_Schema"), getErrorReporter(), threadStatuses, this.getClass());
				}
			}
		}
		return dao;
	}

	protected String getContentType(HttpServletRequest req) {
		String contentType = req.getHeader(Const.CONTENT_TYPE);
		if (contentType == null) return null;
		contentType = contentType.toLowerCase();
		if (contentType.contains(";")) {
			contentType = contentType.split(Pattern.quote(";"))[0];
		}
		return contentType;
	}

	protected ApiUser getApiUser(HttpServletRequest req) throws IllegalAccessException, Exception {
		String apiKey = req.getParameter(Const.API_KEY);
		if (!given(apiKey)) {
			apiKey = req.getHeader("Authorization");
		}
		if (!given(apiKey)) {
			apiKey = req.getHeader("authorization");
		}
		if (!given(apiKey)) throw new IllegalAccessException("Must give a valid " + Const.API_KEY);
		ApiUser apiUser = getDao().getApiUser(apiKey);
		if (!apiUser.isValid()) throw new IllegalAccessException("Must give a valid " + Const.API_KEY);
		return apiUser;
	}

	protected ResponseFormat getFormatWithoutExceptions(HttpServletRequest req) {
		try {
			return getFormat(req);
		} catch (Exception e) {
			return ResponseFormat.TEXT_PLAIN;
		}
	}

	protected ResponseFormat getFormat(HttpServletRequest req) throws IllegalArgumentException {
		String acceptHeader = req.getHeader(Const.ACCEPT);
		String formatParameter = req.getParameter(Const.FORMAT);
		String geoJsonParameter = req.getParameter(Const.GEO_JSON_REQUEST);
		if ("true".equals(geoJsonParameter)) return ResponseFormat.GEO_JSON;
		if (formatParameter != null) {
			ResponseFormat format = getFormat(formatParameter.toLowerCase());
			if (format == null)	throw new IllegalArgumentException("Unknown format " + format);
			return format;
		}
		if (acceptHeader != null) {
			String accept = acceptHeader.toLowerCase();
			if (accept.contains(";")) {
				accept = accept.split(Pattern.quote(";"))[0];
			}
			ResponseFormat format = getFormat(accept);
			if (format != null) return format;
		}
		return getDefaultFormat(req.getMethod());
	}

	protected ResponseFormat getDefaultFormat(@SuppressWarnings("unused") String method) {
		return DEFAULT_FORMAT;
	}

	private ResponseFormat getFormat(String contentTypeOrFormatParameter) {
		return CONTENT_TYPE_TO_FORMAT.get(contentTypeOrFormatParameter.replace("application/", ""));
	}

	@Override
	public void log(String message) {
		try {
			getDao().logError(Const.LAJI_ETL_QNAME, ETLBaseServlet.class, null, new UnsupportedOperationException("This is generic java servlet log method - you prob wanted something else. Original message: " + message));
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(message);
		}
	}

	private static AccessMonitor accessMonitor = null;

	private AccessMonitor getAccessMonitor() {
		if (accessMonitor == null) {
			synchronized (LOCK) {
				if (accessMonitor == null) {
					accessMonitor = new AccessMonitor(getDao().getSources());
				}
			}
		}
		return accessMonitor;
	}

	protected Access getAccess(String apiSourceId) throws TooManyRequestsException {
		getAccessMonitor().logAccess(apiSourceId);
		if (noLimit(apiSourceId)) return Access.noLimit();
		return ACCESS_LIMITER.delayAccessIfNecessary(apiSourceId);
	}

	private boolean noLimit(String apiSourceId) {
		return getConfig().productionMode() ?
				Const.PROD_UNLIMITED_API_ACCESS_COUNT.contains(apiSourceId) : Const.STAGING_UNLIMITED_API_ACCESS_COUNT.contains(apiSourceId);
	}

}
