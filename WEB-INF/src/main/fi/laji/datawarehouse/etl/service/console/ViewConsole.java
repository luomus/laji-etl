package fi.laji.datawarehouse.etl.service.console;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/*"})
public class ViewConsole extends UIBaseServlet {

	public static final String MINIMAL_PARAMETER = "minimal";
	private static final long serialVersionUID = -8419379102466872956L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("console");
		boolean minimal = handleMinimalParameter(req, responseData);
		taxonSecureLevels(responseData);
		if (!minimal) {
			DAO dao = getDao();
			try {
				unlinkedTargetNames(responseData, dao);
				unlinkedUserIds(responseData, dao);
				downloadRequests(responseData);
			} catch (Exception e) {
				getErrorReporter().report(e);
				dao.logError(Const.LAJI_ETL_QNAME, ViewConsole.class, null, e);
			}
		}
		return responseData;
	}

	private boolean handleMinimalParameter(HttpServletRequest req, ResponseData responseData) {
		SessionHandler session = getSession(req);
		String minimal = req.getParameter(MINIMAL_PARAMETER);
		if (minimal == null) {
			minimal = session.get(MINIMAL_PARAMETER);
			if (minimal == null) {
				minimal = "true";
			}
		}
		session.put(MINIMAL_PARAMETER, minimal);
		responseData.setData(MINIMAL_PARAMETER, minimal);
		return "true".equals(minimal);
	}

	private final SingleObjectCache<List<DownloadRequest>> failedCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<List<DownloadRequest>>() {
		@Override
		public List<DownloadRequest> load() {
			try {
				return getDao().getFailedDownloadRequests();
			} catch (Exception e) {
				throw getDao().exceptionAndReport(ViewConsole.class.getName(), "Failed download requests cache loader", e);
			}
		}
	}, 5, TimeUnit.MINUTES);

	private final SingleObjectCache<List<DownloadRequest>> todayCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<List<DownloadRequest>>() {
		@Override
		public List<DownloadRequest> load() {
			try {
				List<DownloadRequest> downloadRequests = getDao().getDownloadRequestsFromToday();
				removeFailed(downloadRequests);
				return downloadRequests;
			} catch (Exception e) {
				throw getDao().exceptionAndReport(ViewConsole.class.getName(), "Download requests cache loader", e);
			}
		}

		private void removeFailed(List<DownloadRequest> downloadRequests) {
			Iterator<DownloadRequest> i = downloadRequests.iterator();
			while (i.hasNext()) {
				DownloadRequest downloadRequest = i.next();
				if (downloadRequest.isFailed()) i.remove();
			}
		}
	}, 5, TimeUnit.MINUTES);

	private void downloadRequests(ResponseData responseData) {
		responseData.setData("downloadRequestsToday", todayCache.get());
		responseData.setData("failedDownloadRequests", failedCache.get());
	}

	private void unlinkedTargetNames(ResponseData responseData, final DAO dao) throws Exception {
		try {
			List<UnlinkedTargetNameData> data = Utils.executeWithTimeOut(
					new Callable<List<UnlinkedTargetNameData>>() {
						@Override
						public List<UnlinkedTargetNameData> call() throws Exception {
							return dao.getUnlinkedTargetNames();
						}
					},
					1, TimeUnit.SECONDS);
			responseData.setData("unlinkedTargetNames", data);
		} catch (TimeoutException timeout) {
			responseData.setData("unlinkedTargetNames", Collections.emptyList());
		}
	}

	private void unlinkedUserIds(ResponseData responseData, final DAO dao) throws Exception {
		try {
			List<UnlinkedUserIdsData> data = Utils.executeWithTimeOut(
					new Callable<List<UnlinkedUserIdsData>>() {
						@Override
						public List<UnlinkedUserIdsData> call() throws Exception {
							return dao.getUnlinkedUserIds();
						}
					}, 1, TimeUnit.SECONDS);
			responseData.setData("unlinkedUserIds", data);
		} catch (TimeoutException timeout) {
			responseData.setData("unlinkedUserIds", Collections.emptyList());
		}
	}

	private void taxonSecureLevels(ResponseData responseData) throws Exception {
		try {
			List<Taxon> taxa = Utils.executeWithTimeOut(
					new Callable<List<Taxon>>() {
						@Override
						public List<Taxon> call() throws Exception {
							return getDao().getTaxaWithSecureLevels();
						}
					}, 1, TimeUnit.SECONDS);
			responseData.setData("taxaWithSecureLevels", taxa);
		} catch (TimeoutException timeout) {
			responseData.setData("taxaWithSecureLevels", Collections.emptyList());
		}
	}

}
