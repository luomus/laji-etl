package fi.laji.datawarehouse.etl.service.console;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/log-entries"})
public class GetLogEntries extends UIBaseServlet {

	private static final long serialVersionUID = -3618087487107051098L;

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		return status403(res);
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("log-entries");

		List<LogEntry> entires = getEntries(req);
		responseData.setData("entries", entires);

		return responseData;
	}

	private List<LogEntry> getEntries(HttpServletRequest req) {
		List<LogEntry> entries = getDao().getLogEntries();
		if ("true".equals(getSession(req).get(ViewConsole.MINIMAL_PARAMETER))) {
			if (entries.size() > 20) entries = entries.subList(0, 10);
		}
		for (LogEntry e : entries) {
			e.setMessage(Utils.trimToLength(e.getMessage(), 4000));
		}
		return entries;
	}

}
