package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.CollectionUpdater;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-collection-update"})
public class StartCollectionUpdate extends UIBaseServlet {

	private static final long serialVersionUID = -6995638038387798999L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					new CollectionUpdater(getDao()).update();
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartCollectionUpdate.class, null, e);
				}
			}
		}).start();
		return ok(res);
	}

}
