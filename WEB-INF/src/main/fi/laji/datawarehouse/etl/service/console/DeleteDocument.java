package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/delete-document/*"})
public class DeleteDocument extends UIBaseServlet {

	private static final long serialVersionUID = -9108262721140224419L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		Qname sourceId = Qname.fromURI(req.getParameter("source"));
		Qname documentId = Qname.fromURI(req.getParameter("documentId").trim());

		Source source = getDao().getSources().get(sourceId.toURI());
		if (source == null) throw new IllegalAccessError("Invalid source");

		getDao().getETLDAO().storeToInPipe(sourceId, "DELETE " + documentId.toURI(), "text/plain");
		getThreadHandler().runInPipe(sourceId);

		return status(200, res);
	}

}
