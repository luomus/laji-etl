package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.utils.LoginUtil;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/login/*"})
public class Login extends UIBaseServlet {

	private static final long serialVersionUID = -8419379102466872956L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	private LoginUtil util = null;

	private LoginUtil getLoginUtil() {
		if (util == null) {
			util = new LoginUtil(getConfig(), getErrorReporter());
		}
		return util;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return getLoginUtil().processGet(req, getSession(req), super.initResponseData(req));
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return getLoginUtil().processPost(req, getSession(req), super.initResponseData(req));
	}

}
