package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/thread-statuses"})
public class GetThreadStatuses extends UIBaseServlet {

	private static final long serialVersionUID = 4252641816878953544L;

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		return status403(res);
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("thread-statuses");
		responseData.setData("statuses", getThreadStatuses().getThreadStatuses());
		return responseData;
	}

}
