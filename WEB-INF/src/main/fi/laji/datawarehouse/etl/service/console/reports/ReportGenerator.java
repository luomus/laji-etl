package fi.laji.datawarehouse.etl.service.console.reports;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.query.download.util.BufferedFileCreatorImple;
import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileCompresser;
import fi.luomus.commons.utils.Utils;

public abstract class ReportGenerator {

	@Retention(RetentionPolicy.RUNTIME)
	public @interface Order {
		double value();
	}

	public static final String COLLECTIONS = "<COLLECTIONS>";

	private static final String GATHERING_FACT_SQL = "" +
			" SELECT    g.document_id, gf.property, gf.value " +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <SCHEMA>.gf ON gf.gathering_key = g.key " +
			" ORDER BY  g.document_id, gf.property ";

	private static final String UNIT_FACT_SQL = "" +
			" SELECT    u.unit_id, uf.property, uf.value " +
			" FROM      <SCHEMA>.unit u " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (u.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <SCHEMA>.uf ON uf.unit_key = u.key " +
			" ORDER BY  u.unit_id, uf.property ";

	private final FileCreator fileCreator;
	private final java.io.File tempFolder;
	private final java.io.File reportFolder;
	private final String filename;
	private final ErrorReporter errorReporter;
	private final List<String> errors = new ArrayList<>();
	private final String collections;
	private final DAO dao;

	public ReportGenerator(Config config, String filename, Set<Qname> collectionIds, ErrorReporter errorReporter, DAO dao) {
		this.filename = filename;
		this.tempFolder = new java.io.File(config.baseFolder() + config.get("TempFolder"));
		this.reportFolder = new java.io.File(config.baseFolder() + config.get("ReportFolder"));
		this.fileCreator = new BufferedFileCreatorImple(tempFolder, filename, "txt");
		this.errorReporter = errorReporter;
		this.dao = dao;
		this.collections = collectionIds.stream().map(c->"'"+c.toURI()+"'").collect(Collectors.joining(","));
	}

	public abstract void generateActual();

	public void startToGenerateInBackground() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					generate();
				} catch (Exception e) {
					errorReporter.report(filename, e);
				}
				finally {
					closeGeneratedFiles();
				}

			}
		});
		t.setName("Report " + filename + " Thread");
		t.setDaemon(true);
		t.start();
	}

	private void generate() throws Exception {
		generateActual();
		closeGeneratedFiles();
		createZip();
		emptyTemp();
	}

	protected File getFile(String name) {
		return fileCreator.create(name + "_" + DateUtils.getFilenameDatetime());
	}

	private void closeGeneratedFiles() {
		for (File f : fileCreator.getWrittenFiles()) {
			try {
				f.close();
			} catch (IOException e) {
				errorReporter.report(f.getPhysicalFile().getAbsolutePath(), e);
			}
		}
	}

	private void emptyTemp() {
		for (File file : fileCreator.getWrittenFiles()) {
			file.getPhysicalFile().delete();
		}
	}

	private void createZip() throws Exception {
		FileCompresser compresser = null;
		try {
			reportFolder.setWritable(true);
			reportFolder.mkdirs();
			String fileName = filename + "_" + DateUtils.getFilenameDatetime()+".zip";
			java.io.File physicalFile = new java.io.File(reportFolder, fileName);
			compresser = new FileCompresser(physicalFile);
			for (File file : fileCreator.getWrittenFiles()) {
				compresser.addToZip(file.getPhysicalFile());
			}
		} finally {
			if (compresser != null) compresser.close();
		}
	}

	protected String header(Class<?> headerClass) {
		Collection<String> headers = getFields(headerClass);
		return Utils.catenate(headers, "\t");
	}

	private static final Map<Class<?>, Collection<String>> FIELD_CACHE = new HashMap<>();


	private Collection<String> getFields(Class<?> headerClass) {
		if (!FIELD_CACHE.containsKey(headerClass)) {
			FIELD_CACHE.put(headerClass, generateFields(headerClass));
		}
		return FIELD_CACHE.get(headerClass);
	}

	private Collection<String> generateFields(Class<?> headerClass) {
		TreeMap<Double, String> headers = new TreeMap<>();
		for (Field field : headerClass.getDeclaredFields()) {
			Order order = field.getAnnotation(Order.class);
			if (order != null) {
				headers.put(order.value(), field.getName());
			}
		}
		return headers.values();
	}

	protected String line(Object line) {
		List<String> values = new ArrayList<>();
		for (String fieldName : getFields(line.getClass())) {
			try {
				Field field = line.getClass().getDeclaredField(fieldName);
				Object value = field.get(line);
				if (value == null) {
					values.add("");
				} else {
					values.add(toString(value));
				}
			} catch (IllegalAccessException | NoSuchFieldException | SecurityException e) {
				throw new IllegalStateException(fieldName, e);
			}
		}
		return Utils.catenate(values, "\t");
	}

	private String toString(Object value) {
		if (value instanceof Double) {
			return Double.toString(Utils.round((double)value, 6));
		}
		String s = value.toString().replace("\n", " ").replace("\r", " ").replace("\t", " ").replace("\"", " ").replace("'", " ");
		while (s.contains("  ")) {
			s = s.replace("  ", " ");
		}
		return s.trim();
	}

	protected String string(Object object) {
		if (object == null) return null;
		return object.toString();
	}

	protected String cleanUserName(Object object) {
		if (object == null) return null;
		String s = object.toString();
		if (s.startsWith("http://tun.fi/")) return s.replace("http://tun.fi/", "");
		return s;
	}

	protected Qname qname(Object object) {
		if (object == null) return null;
		return Qname.fromURI((String)object);
	}

	protected Integer integer(Object object) {
		if (object == null) return null;
		if (object instanceof String) return Integer.valueOf(object.toString());
		return ((BigInteger) object).intValue();
	}

	protected Double doubleVal(Object object) {
		if (object == null) return null;
		if (object instanceof String) return Double.valueOf(object.toString());
		return ((BigDecimal) object).doubleValue();
	}

	protected Integer iVal(String s, String errorMessageIfFails) {
		if (!given(s)) return null;
		try {
			return Integer.valueOf(s);
		} catch (Exception e) {
			errors.add(e.getMessage() + " " + errorMessageIfFails);
			return null;
		}
	}

	protected Boolean bool(Object object) {
		if (object == null) return null;
		if (object instanceof Boolean) return (Boolean) object;
		if (object.toString().equals("true")) return true;
		if (object.toString().equals("false")) return false;
		return null;
	}

	protected Time time(Object object, String errorMessageIfFails) {
		String time = string(object);
		if (time == null) return null;
		String[] parts = time.split(Pattern.quote(":"));
		if (parts.length == 1) return new Time(parts[0], errorMessageIfFails);
		return new Time(parts[0], parts[1], errorMessageIfFails);
	}

	protected class Time {
		final Integer hour;
		final Integer minute;
		public Time(String hours, String errorMessageIfFails) {
			this(hours, null, errorMessageIfFails);
		}
		public Time(String hours, String minute, String errorMessageIfFails) {
			this.hour = iVal(hours, errorMessageIfFails);
			this.minute = iVal(minute, errorMessageIfFails);
		}
	}

	protected String issues(JoinedRow row) {
		List<Quality> issues = new ArrayList<>();
		DocumentQuality dq = row.getDocument().getQuality();
		GatheringQuality gq = row.getGathering().getQuality();
		UnitQuality uq = row.getUnit().getQuality();
		if (dq != null && dq.hasIssues()) {
			issues.add(dq.getIssue());
		}
		if (gq != null && gq.hasIssues()) {
			issues.add(gq.getIssue());
			issues.add(gq.getTimeIssue());
			issues.add(gq.getLocationIssue());
		}
		if (uq != null && uq.hasIssues()) {
			issues.add(uq.getIssue());
		}
		if (issues.isEmpty()) return null;
		return issues.stream().filter(Objects::nonNull).map(q->q.humanReadable()).collect(Collectors.joining("; "));
	}

	protected String issues(Object[] row, int startIndex) {
		return issues(null, row, startIndex);
	}

	protected String issues(String inheritedIssues, Object[] row, int startIndex) {
		List<String> issues = new ArrayList<>();
		issues.add(inheritedIssues);
		for (int i = startIndex; i<row.length; i++) {
			issues.add(string(row[i]));
		}
		if (issues.isEmpty()) return null;
		return issues.stream().filter(Objects::nonNull).collect(Collectors.joining("; "));
	}
	protected boolean given(String s) {
		return s != null && !s.trim().isEmpty();
	}

	protected void writeErrors() {
		if (errors.isEmpty()) return;
		File errorFile = getFile("errors");
		for (String error : errors) {
			errorFile.write(error);
		}
	}

	protected void error(String error) {
		errors.add(error);
	}

	protected ResultStream<Object[]> getCustomQuery(String sql) {
		return dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getCustomQuery(sql.replace(COLLECTIONS, collections));
	}

	protected void addLinkings(JoinedRow row) {
		dao.getPrivateVerticaDAO().getQueryDAO().setJoinedRowLinkings(row);

	}

	protected void writeUnitFacts(String filename) {
		writeFacts(filename, UNIT_FACT_SQL);
	}

	protected void writeGatheringFacts(String filename) {
		writeFacts(filename, GATHERING_FACT_SQL);
	}

	private void writeFacts(String filename, String sql) {
		File factFile = getFile(filename);
		factFile.write(header(Fact.class));
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. " + filename + " line " + count);
				Object[] row = i.next();
				Qname parentId = qname(row[0]);
				String property = string(row[1]);
				String value = string(row[2]);
				Fact fact = buildFact(parentId, property, value);
				factFile.write(line(fact));
			}
		}
	}

	private Fact buildFact(Qname parentId, String property, String value) {
		Fact fact = new Fact();
		fact.parentId = parentId;
		fact.fact = cleanFactOrValue(property);
		fact.value = cleanFactOrValue(value);
		fact.factLabel = label(property, "en");
		fact.valueLabel = label(value, "en");
		return fact;
	}

	public static String cleanFactOrValue(String factOrValue) {
		if (factOrValue == null) return null;
		if (doesNotLookLikeURI(factOrValue)) return factOrValue;
		try {
			String qname = Qname.fromURI(factOrValue).toString();
			if (qname.contains(":")) {
				qname = qname.substring(qname.indexOf(':')+1);
			}
			if (qname.contains(".")) {
				qname = qname.substring(qname.indexOf('.')+1);
			}
			if (qname.isEmpty()) return Qname.fromURI(factOrValue).toString();
			return qname;
		} catch (Exception e) {
			return factOrValue;
		}
	}

	protected String label(String value, String preferredLocale) {
		if (value == null) return null;
		if (doesNotLookLikeURI(value)) return value;
		try {
			LocalizedText label = dao.getLabels(Qname.fromURI(value));
			if (label.hasTextForLocale(preferredLocale)) return label.forLocale(preferredLocale);
			if (label.hasTextForLocale("en")) return label.forLocale("en");
			if (label.hasTextForLocale("fi")) return label.forLocale("fi");
			return value;
		} catch (Exception e) {
			return value;
		}
	}

	private static boolean doesNotLookLikeURI(String value) {
		return !value.startsWith("http");
	}

	private class Fact {
		@Order(0) public Qname parentId;
		@Order(1) public String fact;
		@Order(2) public String value;
		@Order(3) public String factLabel;
		@Order(4) public String valueLabel;
	}

}
