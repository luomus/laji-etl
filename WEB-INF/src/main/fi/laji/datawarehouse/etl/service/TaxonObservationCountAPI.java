package fi.laji.datawarehouse.etl.service;

import java.util.Collection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/taxon-obs-count/*"})
public class TaxonObservationCountAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -3406648181086089612L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			DAO dao = getDao();
			String data = dao.getPersisted("obscounts");
			if (data == null) {
				Collection<ObsCount> counts = dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getTaxonObservationCounts();
				data = ModelToJson.toJson(counts).toString();
				dao.persist("obscounts", data);
			}
			return jsonResponse(data);
		} catch (Throwable e) {
			return loggedSystemError500(e, TaxonObservationCountAPI.class, res, req);
		}
	}

}
