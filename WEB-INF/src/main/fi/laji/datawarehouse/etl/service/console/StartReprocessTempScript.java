package fi.laji.datawarehouse.etl.service.console;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO.VerticaCustomQueriesDAO;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-error-reprocess"})
public class StartReprocessTempScript extends UIBaseServlet {

	private static final long serialVersionUID = -4606395793526404854L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					doit();
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartReprocessTempScript.class, null, e);
				}
			}

		}).start();

		return ok(res);
	}

	// Note: outpipe reprocessing needed this time
	private void doit() {
		try {
			Set<String> documentIds = new HashSet<>(40000);
			DAO dao = getDao();
			dao.getETLDAO().clearReprocessDocumentIds();
			//VerticaCustomQueriesDAO privateDao = dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries();
			VerticaCustomQueriesDAO publicDao = dao.getPublicVerticaDAO().getQueryDAO().getCustomQueries();

			documentIds.addAll(publicDao.getdDocumentIdsWithSecureLevel());

			log("Replacing splitted document ids with actuals", dao);
			documentIds = replaceSplitted(documentIds, dao);

			log("Storing " + documentIds.size() + " reprocess ids (clears old entries)", dao);
			dao.getETLDAO().storeReprocessDocumentIds(documentIds);

			log("Exiting", dao);
		} finally {
			getThreadStatuses().reportThreadDead(StartReprocessTempScript.class);
		}
	}

	//@SuppressWarnings("unused")
	private Set<String> replaceSplitted(Set<String> documentIds, DAO dao) {
		Set<String> actual = new HashSet<>(documentIds.size());
		Map<String, SplittedDocumentIdEntity> splitted = dao.getETLDAO().getAllSplittedIdToOriginalId();
		log("Loaded " + splitted.size() +" spitted document ids", dao);
		int i = 0;
		for (String documentId : documentIds) {
			if (i++ % 100000 == 0) log("" + i + " / " + documentIds.size(), dao);
			if (documentId.startsWith("http://tun.fi/A.")) {
				SplittedDocumentIdEntity s = splitted.get(documentId);
				if (s != null) {
					actual.add(s.getOriginalDocumentId());
				} else {
					actual.add(documentId);
				}
			} else {
				actual.add(documentId);
			}
		}
		return actual;
	}

	private void log(String message, DAO dao) {
		dao.logMessage(Const.LAJI_ETL_QNAME, StartReprocessTempScript.class, message);
		status(message);
	}

	private void status(String message) {
		getThreadStatuses().getThreadStatusReporterFor(StartReprocessTempScript.class).setStatus(message);
	}

}
