package fi.laji.datawarehouse.etl.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.CollectionUpdater;
import fi.laji.datawarehouse.etl.models.InvasiveSpeciesEarlyWarningNotifier;
import fi.laji.datawarehouse.etl.models.NamedPlaceUpdater;
import fi.laji.datawarehouse.etl.models.PestSpeciesEarlyWarningNotifier;
import fi.laji.datawarehouse.etl.models.QuarantineReleaser;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporterWithLogging;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

class NightlyScheduledTasks implements Runnable {

	private final DAO dao;
	private final ErrorReporter errorReporter;
	private final Config config;
	private final ThreadHandler threadHandler;
	private final ThreadStatuses threadStatuses;

	public NightlyScheduledTasks(InitializationServlet initializationServlet) {
		this.dao = initializationServlet.getDao();
		this.errorReporter = initializationServlet.getErrorReporter();
		this.config = initializationServlet.getConfig();
		this.threadHandler = initializationServlet.getThreadHandler();
		this.threadStatuses = initializationServlet.getThreadStatuses();
	}

	@Override
	public void run() {
		ThreadStatusReporterWithLogging reporterWithLogging = new ThreadStatusReporterWithLogging(threadStatuses, dao, this.getClass());
		try {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Staring scheduled tasks ===== ");
			startTasks(reporterWithLogging);
		} catch (Throwable e) {
			log("nightly", e);
		} finally {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Scheduled tasks end ===== ");
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private void startTasks(ThreadStatusReporter statusReporter) {
		try {
			statusReporter.setStatus("Log occurrence counts");
			dao.logOccurrenceCounts();
		} catch (Throwable e) {
			log("Log occurrence counts", e);
		}

		try {
			int count = invasiveSpeciesEarlyWarnings();
			statusReporter.setStatus("Sent " + count + " invasive species early warnings");
		} catch (Throwable e) {
			log("Invasive early warnings", e);
		}

		try {
			int count = pestSpeciesEarlyWarnings();
			statusReporter.setStatus("Sent " + count + " Evira pest early warnings");
		} catch (Throwable e) {
			log("Pest early warnings", e);
		}

		try {
			statusReporter.setStatus("Clear person download limits");
			dao.clearPersonDailyLimits();
		} catch (Throwable e) {
			log("Clear person download limits", e);
		}

		try {
			statusReporter.setStatus("Clear query caches");
			dao.getPublicVerticaDAO().getQueryDAO().clearCaches();
			dao.getPrivateVerticaDAO().getQueryDAO().clearCaches();
		} catch (Throwable e) {
			log("Clear query caches", e);
		}

		try {
			statusReporter.setStatus("Starting reload of taxa and persons");
			dao.getVerticaDimensionsDAO().startTaxonAndPersonReprosessing();
		} catch (Throwable e) {
			log("Starting reload of taxa and persons", e);
		}

		try {
			statusReporter.setStatus("Releasing quarantined data");
			int count = new QuarantineReleaser(dao, errorReporter, threadHandler).release(new Date());
			dao.logMessage(Const.LAJI_ETL_QNAME, NightlyScheduledTasks.class, "Released " + count + " quarantined documents.");
		} catch (Throwable e) {
			log("Releasing quarantined data", e);
		}

		try {
			statusReporter.setStatus("Updating named places");
			List<Qname> ids = new NamedPlaceUpdater(dao).update();
			int updated = dao.getETLDAO().markReprocessOutPipeByNamedPlaceIds(ids);
			dao.logMessage(Const.LAJI_ETL_QNAME, NightlyScheduledTasks.class, "Set " + updated + " documents to be reprocessed");
		} catch (Throwable e) {
			log("Updating named places", e);
		}
		try {
			statusReporter.setStatus("Updating collections");
			new CollectionUpdater(dao).update();
		} catch (Throwable e) {
			log("Updating collections", e);
		}
		try {
			statusReporter.setStatus("Updating resource names");
			dao.getVerticaDimensionsDAO().updateNameableEntityNames();
		} catch (Throwable e) {
			log("Updating resource names", e);
		}

		try {
			cleanups(statusReporter);
		} catch (Throwable e) {
			log("Cleanups", e);
		}

		try {
			statusReporter.setStatus("Persist query log");
			long count = dao.persistQueryLogToLogTable();
			statusReporter.setStatus("Persisted " + count + " log entries");
		} catch (Throwable e) {
			log("Persist query log", e);
		}

		try {
			statusReporter.setStatus("Store taxon occurrence counts");
			Collection<ObsCount> counts = dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getTaxonObservationCounts();
			String data = ModelToJson.toJson(counts).toString();
			dao.persist("obscounts", data);
		} catch (Throwable e) {
			log("Store taxon occurrence counts", e);
		}

		try {
			statusReporter.setStatus("Outpipe InError 1 -> 0");
			dao.getETLDAO().markOutPipeErroneousForReattempt();
		} catch (Throwable e) {
			log("Clear outpipe error flags", e);
		}
	}

	private void cleanups(ThreadStatusReporter statusReporter) throws InterruptedException {
		statusReporter.setStatus("Performing Vertica cleanup for dimensions schema");
		dao.getVerticaDimensionsDAO().performCleanUp();
		statusReporter.setStatus("Performing Vertica cleanup for dimensions schema: done");

		Thread pubT = new Thread(new Runnable() {
			@Override
			public void run() {
				statusReporter.setStatus("Performing Vertica cleanup for public schema");
				dao.getPublicVerticaDAO().performCleanUp(statusReporter);
				statusReporter.setStatus("Performing Vertica cleanup for public schema: done");
			}
		});
		Thread priT = new Thread(new Runnable() {
			@Override
			public void run() {
				statusReporter.setStatus("Performing Vertica cleanup for private schema");
				dao.getPrivateVerticaDAO().performCleanUp(statusReporter);
				statusReporter.setStatus("Performing Vertica cleanup for private schema: done");
			}
		});
		pubT.setName("Nightly public cleanup Thread");
		priT.setName("Nightly private cleanup Thread");
		pubT.start();
		priT.start();
		pubT.join();
		priT.join();
	}

	private int invasiveSpeciesEarlyWarnings() throws Exception {
		return new InvasiveSpeciesEarlyWarningNotifier(dao, errorReporter, config).sendWarnings();
	}

	private int pestSpeciesEarlyWarnings() throws Exception {
		return new PestSpeciesEarlyWarningNotifier(dao, errorReporter, config).sendWarnings();
	}

	private void log(String info, Throwable e) {
		dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), info, e);
	}

}