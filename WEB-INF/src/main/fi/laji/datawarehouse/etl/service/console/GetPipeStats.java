package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/pipe-stats"})
public class GetPipeStats extends UIBaseServlet {

	private static final long serialVersionUID = -2367987879084810172L;

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		return status403(res);
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("pipeStats");
		boolean forceReload = "forceReload".equals(req.getParameter("forceReload"));
		responseData.setData("pipeStats", getDao().getETLDAO().getPipeStats(forceReload));
		return responseData;
	}

}
