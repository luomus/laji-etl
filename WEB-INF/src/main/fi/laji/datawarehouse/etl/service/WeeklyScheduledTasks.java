package fi.laji.datawarehouse.etl.service;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporterWithLogging;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;

class WeeklyScheduledTasks implements Runnable {

	private final DAO dao;
	private final ThreadHandler threadHandler;
	private final ThreadStatuses threadStatuses;
	private final Config config;

	public WeeklyScheduledTasks(InitializationServlet initializationServlet) {
		this.dao = initializationServlet.getDao();
		this.threadHandler = initializationServlet.getThreadHandler();
		this.threadStatuses = initializationServlet.getThreadStatuses();
		this.config = initializationServlet.getConfig();
	}

	@Override
	public void run() {
		ThreadStatusReporterWithLogging reporterWithLogging = new ThreadStatusReporterWithLogging(threadStatuses, dao, this.getClass());
		try {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Staring weekly scheduled tasks ===== ");
			startTasks(reporterWithLogging);
		} catch (Throwable e) {
			log("weekly", e);
		} finally {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Weekly scheduled tasks end ===== ");
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private void startTasks(ThreadStatusReporter statusReporter) {
		if (!config.productionMode()) return;
		try {
			statusReporter.setStatus("Starting LajiGIS pull");
			threadHandler.runPullReader(LajiGISPullReader.SOURCE);
		} catch (Throwable e) {
			log("lajigis pull", e);
		}
	}

	private void log(String info, Throwable e) {
		dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), info, e);
	}

}