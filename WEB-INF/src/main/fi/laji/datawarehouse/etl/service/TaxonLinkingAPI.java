package fi.laji.datawarehouse.etl.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.taxonomy.Taxon;

@WebServlet(urlPatterns = {"/taxon-linking/*"})
public class TaxonLinkingAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -1860150741398230477L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			String name = req.getParameter("name");
			String taxonId = req.getParameter("taxonId");
			if (!given(name) && !(given(taxonId))) return unloggedUserError400(new IllegalArgumentException("Must give 'name' or 'taxonId' parameter"), res, req);
			if (given(name)) {
				Qname resolvedTaxonId = getDao().getTaxonLinkingService().getTaxonId(name);
				Taxon taxon = given(resolvedTaxonId) ? getDao().getTaxon(resolvedTaxonId) : null;
				Collection<Qname> allMatches = getDao().getAllTaxonMatches(name);
				Collection<Taxon> allTaxonMatches = new ArrayList<>();
				for (Qname id : allMatches) {
					Taxon t = getDao().getTaxon(id);
					if (t != null) allTaxonMatches.add(t);
				}
				return jsonResponse(response(taxon, allTaxonMatches));
			}
			Collection<String> names = getDao().getTaxonLinkingService().getLookUpNames(new Qname(taxonId));
			return jsonResponse(response(names));
		} catch (Throwable e) {
			return loggedSystemError500(e, TaxonLinkingAPI.class, res, req);
		}
	}

	private JSONArray response(Collection<String> names) {
		JSONArray response = new JSONArray();
		for (String name : names) {
			response.appendString(name);
		}
		return response;
	}

	private JSONObject response(Taxon taxon, Collection<Taxon> allTaxonMatches) {
		JSONObject response = new JSONObject();
		response.setBoolean("found", taxon != null);
		if (taxon != null) {
			set(taxon, response);
		}
		for (Taxon t : allTaxonMatches) {
			response.getArray("all").appendObject(set(t, new JSONObject()));
		}
		return response;
	}

	private JSONObject set(Taxon taxon, JSONObject json) {
		json.setString("taxonId", taxon.getId().toString());
		json.setString("taxonURI", taxon.getId().toURI());
		json.setObject("taxon", ModelToJson.toJson(taxon));
		return json;
	}

}
