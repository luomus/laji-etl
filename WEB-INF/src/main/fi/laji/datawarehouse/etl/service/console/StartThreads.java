package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-threads/*"})
public class StartThreads extends UIBaseServlet {

	private static final long serialVersionUID = 7773229010976753371L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		Qname sourceId = new Qname(getId(req));
		getThreadHandler().startSource(sourceId, true);
		return ok(res);
	}

}
