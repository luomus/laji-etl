package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

public class WinterBirdReportGenerator extends CensusReportGenerator {

	private static final String FILENAME_PREFIX = "talvilintu";

	private static final String GATHERING_SQL = "" +
			" SELECT    g.document_id, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje, " +
			"           g.year, g.month, g.day, g.hourbegin, g.hourend, g.linelength, team.team_text as team, g.notes, dataOrigin.value as dataOrigin, " +
			"           document_issue_message, gathering_issue_message, timeissue_message, locationissue_message " +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (g.namedplace_id = np.id) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.team ON (g.team_key = team.key) " +
			" LEFT JOIN <SCHEMA>.df dataOrigin ON (dataOrigin.document_key = g.document_key AND dataOrigin.property = 'http://tun.fi/MY.dataOrigin') " +
			" ORDER BY  routeName, routeId, datebegin_key ";

	private static final String UNIT_SQL = "" +
			" SELECT    unit.document_id, unit.unit_id, " +
			"           unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name, " +
			"           unit.individualcount, unit.individualcount_male, unit.individualcount_female, unit.notes, " +
			"           a.value_integer as a, b.value_integer as b, c.value_integer as c, d.value_integer as d, e.value_integer as e, f.value_integer as f, g.value_integer as g, h.value_integer as h, " +
			"           unit_issue_message" +
			" FROM      <SCHEMA>.unit " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (unit.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (unit.namedplace_id = np.id) " +
			" JOIN      <DIMENSION_SCHEMA>.target ON unit.target_key = target.key " +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxon ON target.taxon_key = taxon.key " +
			" LEFT JOIN <SCHEMA>.uf a ON unit.key = a.unit_key AND a.property = 'http://tun.fi/WBC.individualCountBiotopeA' " +
			" LEFT JOIN <SCHEMA>.uf b ON unit.key = b.unit_key AND b.property = 'http://tun.fi/WBC.individualCountBiotopeB' " +
			" LEFT JOIN <SCHEMA>.uf c ON unit.key = c.unit_key AND c.property = 'http://tun.fi/WBC.individualCountBiotopeC' " +
			" LEFT JOIN <SCHEMA>.uf d ON unit.key = d.unit_key AND d.property = 'http://tun.fi/WBC.individualCountBiotopeD' " +
			" LEFT JOIN <SCHEMA>.uf e ON unit.key = e.unit_key AND e.property = 'http://tun.fi/WBC.individualCountBiotopeE' " +
			" LEFT JOIN <SCHEMA>.uf f ON unit.key = f.unit_key AND f.property = 'http://tun.fi/WBC.individualCountBiotopeF' " +
			" LEFT JOIN <SCHEMA>.uf g ON unit.key = g.unit_key AND g.property = 'http://tun.fi/WBC.individualCountBiotopeG' " +
			" LEFT JOIN <SCHEMA>.uf h ON unit.key = h.unit_key AND h.property = 'http://tun.fi/WBC.individualCountBiotopeH' " +
			" ORDER BY  np.name, np.id, unit.datebegin_key, unit.document_id, unit.unit_order ";

	public WinterBirdReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, FILENAME_PREFIX, Utils.set(new Qname("HR.39")), errorReporter, dao);
	}

	@Override
	public void generateActual() {
		System.out.println("Winter bird report starts");

		Map<Qname, Event> events = getEvents();
		System.out.println("Events: " + events.size());

		writeEvents(events);
		System.out.println("Event file written");

		writeObservations(events);
		System.out.println("Observations file written");

		writeGatheringFacts("laskenta_facts");
		System.out.println("Gathering fact file written");

		writeUnitFacts("havainnot_facts");
		System.out.println("Unit fact file written");

		writeErrors();

		System.out.println("Winter bird report completed");
	}

	private void writeEvents(Map<Qname, Event> events) {
		File eventFile = getFile("laskennat");
		eventFile.write(header(Event.class));
		for (Event event : events.values()) {
			eventFile.write(line(event));
		}
	}

	private void writeObservations(Map<Qname, Event> events) {
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));
		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				// SELECT    unit.document_id, unit.unit_id, ...
				// ORDER BY  np.name, np.id, unit.datebegin_key, unit.document_id, unit.unit_order
				Qname eventId = qname(row[0]);
				Event event = events.get(eventId);
				Observation observation = buildObservation(row, event);
				observationsFile.write(line(observation));
			}
		}
	}

	private Observation buildObservation(Object[] row, Event event) {
		// SELECT    unit.document_id, unit.unit_id,
		//			 unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,
		//			 unit.individualcount, unit.individualcount_male, unit.individualcount_female, unit.notes, a, b, c, d, e, f, g, h
		Observation obs = new Observation();
		obs.eventId = event.eventId;
		obs.routeId = event.routeId;
		obs.year = event.year;
		int i = 1;
		obs.unitId = qname(row[i++]);
		obs.reportedName = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		String birdLifeCode = string(row[i++]);
		String euringCode = string(row[i++]);
		obs.birdCode = birdLifeCode != null ? birdLifeCode : euringCode;
		obs.scientificName = string(row[i++]);
		obs.individualCount = integer(row[i++]);
		obs.individualCountMale = integer(row[i++]);
		obs.individualCountFemale = integer(row[i++]);
		obs.notes = string(row[i++]);
		obs.a = integer(row[i++]);
		obs.b = integer(row[i++]);
		obs.c = integer(row[i++]);
		obs.d = integer(row[i++]);
		obs.e = integer(row[i++]);
		obs.f = integer(row[i++]);
		obs.g = integer(row[i++]);
		obs.h = integer(row[i++]);
		obs.issues = issues(event.issues, row, i);
		return obs;
	}

	private Map<Qname, Event> getEvents() {
		Set<Qname> mammaliaTaxonCensusEvents = getMammaliaTaxonCensusEvents();
		ObserverIds observerIds = getObserverIds();
		Map<Qname, AnnotationData> documentAnnotations = getAnnotations();

		Map<Qname, Event> events = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(GATHERING_SQL)) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				Object[] row = i.next();
				count++;
				if (count % 10000 == 0) System.out.println(" .. event line " + count);
				Event event = buildEvent(row);
				event.mammaliaCensus = mammaliaTaxonCensusEvents.contains(event.eventId);
				event.userIds = observerIds.userIds.get(event.eventId);
				event.personIds = observerIds.personIds.get(event.eventId);
				event.lintuvaaraIds = observerIds.lintuvaaraIds.get(event.eventId);
				if (documentAnnotations.containsKey(event.eventId)) {
					event.errors = documentAnnotations.get(event.eventId).getTags();
					event.errorNotes = documentAnnotations.get(event.eventId).getNotes();
				}
				events.put(event.eventId, event);
			}
		}
		return events;
	}

	private Event buildEvent(Object[] row) {
		// SELECT    g.document_id, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje,
		//			  g.year, g.month, g.day, g.hourbegin, g.hourend, g.linelength, team.team_text as team, g.notes
		Event event = new Event();
		int i = 0;
		event.eventId = qname(row[i++]);
		event.routeId = qname(row[i++]);
		event.routeName = string(row[i++]);
		event.municipality = string(row[i++]);
		event.ykjLat = integer(row[i++]);
		event.ykjLon = integer(row[i++]);
		event.year = integer(row[i++]);
		event.month = integer(row[i++]);
		event.day = integer(row[i++]);
		event.startHour = integer(row[i++]);
		event.endHour = integer(row[i++]);
		event.linelength = integer(row[i++]);
		event.observers = string(row[i++]);
		event.notes = string(row[i++]);
		event.dataOrigin = qname(row[i++]);
		event.issues = issues(row, i);
		return event;
	}

	public class Event {
		@Order(1) public Qname eventId;
		@Order(4) public Qname routeId;
		@Order(6) public String municipality;
		@Order(7) public String routeName;
		@Order(8) public Integer ykjLat;
		@Order(9) public Integer ykjLon;
		@Order(11) public Integer year;
		@Order(12) public Integer month;
		@Order(13) public Integer day;
		@Order(14) public Integer startHour;
		@Order(16) public Integer endHour;
		@Order(22) public Boolean mammaliaCensus;
		@Order(23) public Integer linelength;
		@Order(25.0) public String errors;
		@Order(25.1) public String errorNotes;
		@Order(26.0) public Set<String> userIds;
		@Order(26.1) public Set<String> personIds;
		@Order(26.2) public Set<Integer> lintuvaaraIds;
		@Order(27) public String observers;
		@Order(28) public String notes;
		@Order(29) public Qname dataOrigin;
		@Order(30) public String issues;
	}

	public class Observation {
		@Order(0) public Qname unitId;
		@Order(1) public Qname eventId;
		@Order(2) public Qname routeId;
		@Order(3) public Integer year;
		@Order(18) public String scientificName;
		@Order(19) public Qname taxonId;
		@Order(20) public String birdCode;
		@Order(21) public String reportedName;
		@Order(24.0) public Integer individualCount;
		@Order(24.1) public Integer individualCountMale;
		@Order(24.2) public Integer individualCountFemale;
		@Order(29) public String notes;
		@Order(40) public Integer a;
		@Order(41) public Integer b;
		@Order(42) public Integer c;
		@Order(43) public Integer d;
		@Order(44) public Integer e;
		@Order(45) public Integer f;
		@Order(46) public Integer g;
		@Order(47) public Integer h;
		@Order(50) public String issues;
	}

}
