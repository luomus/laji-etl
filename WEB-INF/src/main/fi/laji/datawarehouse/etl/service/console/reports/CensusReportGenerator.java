package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.LogUtils;

public abstract class CensusReportGenerator extends ReportGenerator {

	private static final String TAXON_SET_SQL = "" +
			" SELECT g.document_id, g.gathering_id, gf.value " +
			" FROM   <SCHEMA>.gathering g " +
			" JOIN   <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN   <SCHEMA>.gf ON (gf.gathering_key = g.key AND gf.property = 'http://tun.fi/MY.taxonCensus')";

	private static final String MAMMALIA_TAXON_CENSUS_SQL = "" +
			" SELECT DISTINCT g.document_id " +
			" FROM   <SCHEMA>.taxoncensus " +
			" JOIN   <SCHEMA>.gathering g ON taxoncensus.gathering_key = g.key " +
			" JOIN   <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" WHERE  taxon_key = 37612 " +
			" AND    type = 'http://tun.fi/MY.taxonCensusTypeCounted' ";

	private static final String ANNOTATION_SQL = "" +
			" SELECT	document_id, original_document " +
			" FROM		<SCHEMA>.document " +
			" WHERE		document_id in ( " +
			" 				SELECT distinct annotation.document_id " +
			"				FROM <SCHEMA>.document d " +
			"				JOIN <DIMENSION_SCHEMA>.collection ON d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+") " +
			"				JOIN <SCHEMA>.annotation ON annotation.document_key = d.key " +
			" ) ";

	private static final String KEYWORD_SQL = "" +
			" SELECT d.document_id, dk.keyword " +
			" FROM   <SCHEMA>.document d " +
			" JOIN   <DIMENSION_SCHEMA>.collection ON (d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN   <SCHEMA>.document_keyword dk ON dk.document_key = d.key ";

	private static final String GATHERING_OBSERVER_ID_SQL = "" +
			" SELECT    DISTINCT g.document_id as documentId, userid.id as userId, person.id as personId, person.lintuvaara_id as lintuvaaraId, person.omariista_id as omaRiistaId " +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <SCHEMA>.gathering_userid gu ON (gu.gathering_key = g.key) " +
			" JOIN      <DIMENSION_SCHEMA>.userid ON (gu.userid_key = userid.key) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.person ON (userid.person_key = person.key) ";

	private static final String DOCUMENT_EDITOR_ID_SQL = "" +
			" SELECT    DISTINCT d.document_id as documentId, userid.id as userId, person.id as personId, person.lintuvaara_id as lintuvaaraId, person.omariista_id as omaRiistaId " +
			" FROM      <SCHEMA>.document d " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <SCHEMA>.document_userid du ON (du.document_key = d.key) " +
			" JOIN      <DIMENSION_SCHEMA>.userid ON (du.userid_key = userid.key) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.person ON (userid.person_key = person.key) ";

	private static final String DOCUMENT_NOTES_SQL = "" +
			" SELECT    document_id, notes " +
			" FROM      <SCHEMA>.document d " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" WHERE     notes IS NOT NULL ";

	public CensusReportGenerator(Config config, String filename, Set<Qname> collectionIds, ErrorReporter errorReporter, DAO dao) {
		super(config, filename, collectionIds, errorReporter, dao);
	}

	protected Map<Qname, String> getDocumentNotes() {
		Map<Qname, String> notes = new HashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(DOCUMENT_NOTES_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname id = qname(row[0]);
				String note = string(row[1]);
				notes.put(id, note);
			}
		}
		System.out.println("Document notes: " + notes.size());
		return notes;
	}

	public class AnnotationData {
		private String tags;
		private String notes;
		public AnnotationData(Annotation a) {
			addTags(a.getAddedTags());
			addNotes(a.getNotes());
		}
		public AnnotationData addTags(List<Tag> tags) {
			if (tags == null || tags.isEmpty()) return this;
			if (this.tags == null) this.tags = "";
			this.tags += tags.toString() + " ";
			return this;
		}
		public AnnotationData addNotes(String notes) {
			if (notes == null || notes.isEmpty()) return this;
			if (this.notes == null) this.notes = "";
			this.notes += notes + " ";
			return this;
		}
		public String getTags() {
			if (tags == null) return null;
			return tags.trim();
		}
		public String getNotes() {
			if (notes == null) return null;
			return notes.trim();
		}
	}

	protected Map<Qname, AnnotationData> getAnnotations() {
		Map<Qname, AnnotationData> annotations = new HashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(ANNOTATION_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname eventId = qname(row[0]);
				String json = string(row[1]);
				try {
					Document d = JsonToModel.documentFromJson(new JSONObject(json));
					Annotation documentAnnotation = getLast(d.getAnnotations());
					if (documentAnnotation != null) {
						annotations.put(d.getDocumentId(), new AnnotationData(documentAnnotation));
					}
					for (Gathering g : d.getGatherings()) {
						for (Unit u : g.getUnits()) {
							Annotation unitAnnotation = getLast(u.getAnnotations());
							if (unitAnnotation != null) {
								annotations.put(u.getUnitId(), new AnnotationData(unitAnnotation));
							}
						}
					}
				} catch (Exception e) {
					error("Invalid annotation " + eventId + " " + LogUtils.buildStackTrace(e, 5));
				}
			}
		}
		System.out.println("Annotations: " + annotations.size());
		return annotations;
	}

	private Annotation getLast(List<Annotation> annotations) {
		if (annotations == null || annotations.isEmpty()) return null;
		return annotations.get(annotations.size()-1);
	}

	protected Map<Qname, String> getOldEventIds() {
		Map<Qname, String> keywords = new HashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(KEYWORD_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname id = qname(row[0]);
				String keyword = string(row[1]);
				keywords.put(id, keyword);
			}
		}
		return keywords;
	}

	public static class ObserverIds {
		Map<Qname, Set<String>> userIds = new HashMap<>();
		Map<Qname, Set<String>> personIds = new HashMap<>();
		Map<Qname, Set<Integer>> lintuvaaraIds = new HashMap<>();
		Map<Qname, Set<Integer>> omaRiistaIds = new HashMap<>();
	}

	protected ObserverIds getObserverIds() {
		ObserverIds ids = new ObserverIds();
		observerIds(ids, GATHERING_OBSERVER_ID_SQL);
		observerIds(ids, DOCUMENT_EDITOR_ID_SQL);
		System.out.println("User ids: " + ids.userIds.size());
		System.out.println("Person ids: " + ids.personIds.size());
		System.out.println("Lintuvaara ids: " + ids.lintuvaaraIds.size());
		System.out.println("Oma Riista ids: " + ids.omaRiistaIds.size());
		return ids;
	}

	private void observerIds(ObserverIds ids, String sql) {
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				// SELECT    DISTINCT g.document_id, userid.id, person.id, person.lintuvaara_id, person.omariista_id
				Qname eventId = qname(row[0]);
				String userId = cleanUserName(row[1]);
				Qname personId = qname(row[2]);
				String personQname = personId == null ? null : personId.toString();
				Integer lintuvaaraId = integer(row[3]);
				Integer omaRiistaId = integer(row[4]);
				add(ids.userIds, eventId, userId);
				add(ids.personIds, eventId, personQname);
				add(ids.lintuvaaraIds, eventId, lintuvaaraId);
				add(ids.omaRiistaIds, eventId, omaRiistaId);
			}
		}
	}

	private <T> void add(Map<Qname, Set<T>> map, Qname eventId, T id) {
		if (id == null) return;
		if (!map.containsKey(eventId)) {
			map.put(eventId, new HashSet<>());
		}
		map.get(eventId).add(id);
	}

	protected Set<Qname> getMammaliaTaxonCensusEvents() {
		Set<Qname> ids = new HashSet<>();
		try (ResultStream<Object[]> results = getCustomQuery(MAMMALIA_TAXON_CENSUS_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname eventId = qname(row[0]);
				ids.add(eventId);
			}
		}
		System.out.println("Mammalia taxon census: " + ids.size());
		return ids;
	}

	protected Map<Qname, List<String>> getTaxonCensusTaxonSets() {
		Map<Qname, List<String>> taxonSets = new HashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(TAXON_SET_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname documentId = qname(row[0]);
				Qname gatheringId = qname(row[1]);
				String taxonSetName = cleanTaxonSetName(qname(row[2]));
				addTaxonSet(taxonSets, gatheringId, taxonSetName);
				addTaxonSet(taxonSets, documentId, taxonSetName);
			}
		}
		System.out.println("Taxon census taxon sets: " + taxonSets.size());
		return taxonSets;
	}

	private void addTaxonSet(Map<Qname, List<String>> taxonSets, Qname id, String taxonSetName) {
		if (!taxonSets.containsKey(id)) {
			taxonSets.put(id, new ArrayList<>());
		}
		List<String> taxonSetsOfId = taxonSets.get(id);
		if (taxonSetsOfId.contains(taxonSetName)) return;
		taxonSetsOfId.add(taxonSetName);
		Collections.sort(taxonSetsOfId);
	}

	private String cleanTaxonSetName(Qname taxonSetId) {
		return taxonSetId.toString().replace("MX.taxonSetSykeButterflyCensus", "").replace("MX.taxonSetWaterbird", "");
	}

}
