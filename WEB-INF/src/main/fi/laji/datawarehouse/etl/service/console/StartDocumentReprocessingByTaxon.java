package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.TaxonObservationsForReprosessingMarker;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-taxon-reprocess/*"})
public class StartDocumentReprocessingByTaxon extends UIBaseServlet {

	private static final long serialVersionUID = 7435351966264216628L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		final String id = getId(req);
		if (!given(id)) throw new IllegalArgumentException();
		Qname taxonQname = id.startsWith("http") ? Qname.fromURI(id) : new Qname(id);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					getDao().logMessage(Const.LAJI_ETL_QNAME, StartDocumentReprocessingByTaxon.class, "Starting to reprocess observations for taxon " + taxonQname);
					int count = new TaxonObservationsForReprosessingMarker(getDao(), getErrorReporter(), getThreadHandler(), getThreadStatuses()).reprocess(taxonQname);
					getDao().logMessage(Const.LAJI_ETL_QNAME, StartDocumentReprocessingByTaxon.class, "Reprocessing observations for taxon " + taxonQname + " completed! Affected " + count + " documents.");
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartDocumentReprocessingByTaxon.class, taxonQname.toString(), e);
				}
			}
		}).start();
		return ok(res);
	}

}
