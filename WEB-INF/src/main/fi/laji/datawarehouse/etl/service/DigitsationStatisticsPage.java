package fi.laji.datawarehouse.etl.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.ResponseUtil;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/digitisation-statistics/*"})
public class DigitsationStatisticsPage extends ETLBaseServlet {

	private static final long serialVersionUID = 4563723484655213137L;

	private static final Qname LUOMUS_TOP_COLLECTION = new Qname("HR.128");

	public static class SumCalculatorCreator {
		public SumCalculator getNew() {
			return new SumCalculator();
		}
	}
	public static class SumCalculator {
		private int digitizedCountTotal = 0;
		private int imagedCountTotal = 0;
		private int sizeTotal = 0;
		private int typeSizeTotal = 0;
		private Map<Integer, Integer> monthCountTotal = new HashMap<>();

		public void add(DigitizationStatistic s) {
			digitizedCountTotal += s.getDigitizedCount() == null ? 0 : s.getDigitizedCount();
			imagedCountTotal += s.getImagedCount() == null ? 0 : s.getImagedCount();
			sizeTotal += s.getCollection().getSize() == null ? 0 : s.getCollection().getSize();
			typeSizeTotal += s.getCollection().getTypeSpecimenSize() == null ? 0 : s.getCollection().getTypeSpecimenSize();
			for (Integer month : s.getMonths()) {
				if (!monthCountTotal.containsKey(month)) {
					monthCountTotal.put(month, 0);
				}
				monthCountTotal.put(month, monthCountTotal.get(month)+s.getMonth(month));
			}
		}

		public int getDigitizedCountTotal() {
			return digitizedCountTotal;
		}

		public int getImagedCountTotal() {
			return imagedCountTotal;
		}

		public int getSizeTotal() {
			return sizeTotal;
		}

		public int getTypeSizeTotal() {
			return typeSizeTotal;
		}

		public Integer getMonthCountTotal(Integer month) {
			Integer c = monthCountTotal.get(month);
			if (c == null) return 0;
			return c;
		}
	}

	public static class DigitizationStatistic {
		private final CollectionMetadata collection;
		private final String subParentId;
		private Integer digitizedCount;
		private Integer imagedCount;
		private final Map<Integer, Integer> digitizedByMonth;

		public DigitizationStatistic(CollectionMetadata collection, String subParentId) {
			this.collection = collection;
			this.subParentId = subParentId;
			this.digitizedByMonth = new TreeMap<>();
		}
		public Integer getDigitizedCount() {
			return digitizedCount;
		}
		public void setDigitizedCount(Integer digitizedCount) {
			this.digitizedCount = digitizedCount;
		}
		public Integer getImagedCount() {
			return imagedCount;
		}
		public void setImagedCount(Integer imagedCount) {
			this.imagedCount = imagedCount;
		}
		public CollectionMetadata getCollection() {
			return collection;
		}
		public String getSubParentId() {
			return subParentId;
		}
		public Collection<Integer> getMonths() {
			return digitizedByMonth.keySet();
		}
		public Integer getMonth(Integer month) {
			Integer v = digitizedByMonth.get(month);
			if (v == null) return 0;
			return v;
		}
		public void setMonth(int month, int count) {
			digitizedByMonth.put(month, count);
		}
	}

	public static class DigitizationByPrefixStatistic {
		private final String prefix;
		private Integer digitizedCount;
		private Integer imagedCount;
		private final Map<Integer, Integer> digitizedByMonth;
		public DigitizationByPrefixStatistic(String prefix) {
			this.prefix = prefix;
			this.digitizedByMonth = new TreeMap<>();
		}
		public Integer getDigitizedCount() {
			return digitizedCount;
		}
		public void setDigitizedCount(Integer digitizedCount) {
			this.digitizedCount = digitizedCount;
		}
		public Integer getImagedCount() {
			return imagedCount;
		}
		public void setImagedCount(Integer imagedCount) {
			this.imagedCount = imagedCount;
		}
		public String getPrefix() {
			return prefix;
		}
		public Collection<Integer> getMonths() {
			return digitizedByMonth.keySet();
		}
		public Integer getMonth(Integer month) {
			Integer v = digitizedByMonth.get(month);
			if (v == null) return 0;
			return v;
		}
		public void setMonth(int month, int count) {
			digitizedByMonth.put(month, count);
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			int year = getYear(req);
			VerticaQueryDAO dao = getDao().getPrivateVerticaDAO().getQueryDAO();

			// List<DigitizationStatistic> stats = FAKEcollections(false);
			// List<DigitizationStatistic> typeStats = FAKEcollections(true);
			// List<DigitizationByPrefixStatistic> prefixStats = FAKEprefixes(false);
			// List<DigitizationByPrefixStatistic> prefixTypeStats = FAKEprefixes(true);

			List<DigitizationStatistic> stats = collections(dao, false, year);
			List<DigitizationStatistic> typeStats = collections(dao, true, year);
			List<DigitizationByPrefixStatistic> prefixStats = prefixes(dao, false, year);
			List<DigitizationByPrefixStatistic> prefixTypeStats = prefixes(dao, true, year);

			return new ResponseData()
					.setViewName("digitisation-statistics")
					.setData("collections", stats)
					.setData("collectionsTypes", typeStats)
					.setData("prefixes", prefixStats)
					.setData("prefixesTypes", prefixTypeStats)
					.setData("year", year)
					.setData("months", months(stats, prefixStats))
					.setData("calculator", new SumCalculatorCreator());
		} catch (Exception e) {
			return handleError(req, res, e);
		}
	}

	private Collection<Integer> months(List<DigitizationStatistic> stats, List<DigitizationByPrefixStatistic> prefixStats) {
		Set<Integer> months = new TreeSet<>();
		for (DigitizationStatistic s : stats) {
			months.addAll(s.getMonths());
		}
		for (DigitizationByPrefixStatistic s : prefixStats) {
			months.addAll(s.getMonths());
		}
		return months;
	}

	private int getYear(HttpServletRequest req) {
		int currentYear = DateUtils.getCurrentYear();
		String v = req.getParameter("year");
		if (v == null) return currentYear;
		try {
			int y = Integer.valueOf(v);
			if (y > currentYear || y < 2000) return currentYear;
			return y;
		} catch (Exception e) {
			return currentYear;
		}
	}

	@SuppressWarnings("unused")
	private List<DigitizationStatistic> FAKEcollections(boolean onlyTypes) {
		List<DigitizationStatistic> stats = new ArrayList<>();
		if (!onlyTypes) {
			stats.add(testStat("HR.129", 225, 225, Utils.list(1, 2, 3, 4)));
			stats.add(testStat("HR.124", 525, 2, Utils.list()));
			stats.add(testStat("HR.130", 252, 3, Utils.list((Integer)null, 10, (Integer)null, 1)));
			stats.add(testStat("HR.1167", 291, null, Utils.list(1, 2, 3, 4)));
			stats.add(testStat("HR.103", 383, 1, Utils.list(1, 2)));
			stats.add(testStat("HR.101", 1811, 125, Utils.list((Integer)null, (Integer)null, 3, 4)));
			stats.add(testStat("HR.1328", 2787, 13, Utils.list(100, 200, 300, 400)));
		} else {
			stats.add(testStat("HR.129", 10, 10, Utils.list(1, 2, 3, 4)));
			stats.add(testStat("HR.1328", 5, 0, Utils.list(1, 0, 1, 0)));

		}
		return stats;
	}

	private DigitizationStatistic testStat(String id, Integer digitized, Integer imaged, List<Integer> months) {
		Map<String, CollectionMetadata> collections = getDao().getCollections();
		CollectionMetadata c = collections.get(new Qname(id).toURI());
		DigitizationStatistic stat = new DigitizationStatistic(c, getSubParentId(c, collections));
		stat.setDigitizedCount(digitized);
		stat.setImagedCount(imaged);
		int i = 1;
		for (Integer m : months) {
			if (m != null) {
				stat.setMonth(i, m);
			}
			i++;
		}
		return stat;
	}

	@SuppressWarnings("unused")
	private List<DigitizationByPrefixStatistic> FAKEprefixes(boolean onlyTypes) {
		List<DigitizationByPrefixStatistic> stats = new ArrayList<>();
		if (!onlyTypes) {
			stats.add(testPrefixStat("F", 225, 225, Utils.list(1, 2, 3, 4)));
			stats.add(testPrefixStat("C", 10, 5, Utils.list(0, 0, 5, 5)));
		} else {
			stats.add(testPrefixStat("F", 225, 225, Utils.list(1, 2, 3, 4)));
			stats.add(testPrefixStat("C", 1, 2, Utils.list(0, 0, 1, 1)));
		}
		return stats;
	}

	private DigitizationByPrefixStatistic testPrefixStat(String prefix, Integer digitized, Integer imaged, List<Integer> months) {
		DigitizationByPrefixStatistic stat = new DigitizationByPrefixStatistic(prefix);
		stat.setDigitizedCount(digitized);
		stat.setImagedCount(imaged);
		int i = 1;
		for (Integer m : months) {
			if (m != null) {
				stat.setMonth(i, m);
			}
			i++;
		}
		return stat;
	}

	private List<DigitizationStatistic> collections(VerticaQueryDAO dao, boolean onlyTypes, int year) throws NoSuchFieldException {
		Map<String, CollectionMetadata> collections = getDao().getCollections();
		Map<String, DigitizationStatistic> statistics = new HashMap<>();

		AggregatedQuery query = buildCollectionQuery(onlyTypes);
		for (AggregateRow result : dao.getAggregate(query).getResults()) {
			CollectionMetadata collection = collections.get(collectionId(result));
			if (collection == null) continue;
			DigitizationStatistic stats = new DigitizationStatistic(collection, getSubParentId(collection, collections));
			stats.setDigitizedCount(result.getCount().intValue());
			statistics.put(collection.getQname().toURI(), stats);
		}

		AggregatedQuery mediaQuery = buildCollectionQuery(onlyTypes);
		mediaQuery.getFilters().setHasMedia(true);
		for (AggregateRow result : dao.getAggregate(mediaQuery).getResults()) {
			DigitizationStatistic stats = statistics.get(collectionId(result));
			if (stats == null) continue;
			stats.setImagedCount(result.getCount().intValue());
		}

		AggregatedQuery monthQuery = buildCollectioMonthQuery(onlyTypes, year);
		for (AggregateRow result : dao.getAggregate(monthQuery).getResults()) {
			DigitizationStatistic stats = statistics.get(collectionId(result));
			if (stats == null) continue;
			stats.setMonth((int)result.getAggregateByValues().get(1), result.getCount().intValue());
		}

		if (onlyTypes) {
			collections.values().stream().filter(c->isLuomus(c, collections)).forEach(c->{
				if (c.getTypeSpecimenSize() != null && c.getTypeSpecimenSize() > 0) {
					if (!statistics.containsKey(c.getQname().toURI())) {
						DigitizationStatistic stat = new DigitizationStatistic(c, getSubParentId(c, collections));
						stat.setDigitizedCount(0);
						stat.setImagedCount(0);
						statistics.put(c.getQname().toURI(), stat);
					}
				}
			});
		}
		return sort(statistics.values());
	}

	private boolean isLuomus(CollectionMetadata c, Map<String, CollectionMetadata> collections) {
		while (c.hasParent()) {
			c = collections.get(c.getParentQname().toURI());
			if (c == null) return false;
		}
		return c.getQname().equals(LUOMUS_TOP_COLLECTION);
	}

	private List<DigitizationStatistic> sort(Collection<DigitizationStatistic> statistics) {
		Map<String, Integer> collectionOrders = collectionOrders();
		List<DigitizationStatistic> sorted = new ArrayList<>(statistics);
		Collections.sort(sorted, new Comparator<DigitizationStatistic>() {

			@Override
			public int compare(DigitizationStatistic o1, DigitizationStatistic o2) {
				Integer i1 = collectionOrders.get(o1.getCollection().getQname().toURI());
				Integer i2 = collectionOrders.get(o2.getCollection().getQname().toURI());
				if (i1 == null) i1 = Integer.MAX_VALUE;
				if (i2 == null) i2 = Integer.MAX_VALUE;
				int c = i1.compareTo(i2);
				if (c != 0) return c;
				return o1.getCollection().getName().forLocale("en").compareTo(o2.getCollection().getName().forLocale("en"));
			}

		});

		return sorted;
	}

	private Map<String, Integer> collectionOrders() {
		Map<String, Integer> collectionOrders = new HashMap<>();
		List<CollectionMetadata> roots = getDao().getCollections().values().stream().filter(c->!c.hasParent()).collect(Collectors.toList());
		sort(roots);
		int i = 0;
		for (CollectionMetadata root : roots) {
			i = setOrders(root, collectionOrders, i);
		}
		return collectionOrders;
	}

	private int setOrders(CollectionMetadata coll, Map<String, Integer> collectionOrders, int i) {
		collectionOrders.put(coll.getQname().toURI(), i);
		i++;
		List<CollectionMetadata> children = new ArrayList<>(coll.getChildren());
		sort(children);
		for (CollectionMetadata child : children) {
			i = setOrders(child, collectionOrders, i);
		}
		return i;
	}

	private void sort(List<CollectionMetadata> collections) {
		Collections.sort(collections, new Comparator<CollectionMetadata>() {

			@Override
			public int compare(CollectionMetadata o1, CollectionMetadata o2) {
				return o1.getName().forLocale("en").compareTo(o2.getName().forLocale("en"));
			}

		});
	}

	private List<DigitizationByPrefixStatistic> prefixes(VerticaQueryDAO dao, boolean onlyTypes, int year) throws NoSuchFieldException {
		AggregatedQuery query = buildPrefixQuery(onlyTypes);
		Map<String, DigitizationByPrefixStatistic> statistics = new HashMap<>();

		for (AggregateRow result : dao.getAggregate(query).getResults()) {
			String prefix = prefix(result);
			DigitizationByPrefixStatistic stats = new DigitizationByPrefixStatistic(prefix);
			stats.setDigitizedCount(result.getCount().intValue());
			statistics.put(prefix, stats);
		}

		AggregatedQuery mediaQuery = buildPrefixQuery(onlyTypes);
		mediaQuery.getFilters().setHasMedia(true);
		for (AggregateRow result : dao.getAggregate(mediaQuery).getResults()) {
			DigitizationByPrefixStatistic stats = statistics.get(prefix(result));
			if (stats == null) continue;
			stats.setImagedCount(result.getCount().intValue());
		}

		AggregatedQuery monthQuery = buildPrefixMonthQuery(onlyTypes, year);
		for (AggregateRow result : dao.getAggregate(monthQuery).getResults()) {
			DigitizationByPrefixStatistic stats = statistics.get(prefix(result));
			if (stats == null) continue;
			stats.setMonth((int)result.getAggregateByValues().get(1), result.getCount().intValue());
		}

		return sortByPrefix(statistics.values());
	}

	private AggregatedQuery buildCollectionQuery(boolean onlyTypes) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(base(), new AggregateBy("document.collectionId"), 1, Integer.MAX_VALUE);
		setFilters(onlyTypes, query);
		return query;
	}

	private AggregatedQuery buildCollectioMonthQuery(boolean onlyTypes, int year) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(base(), new AggregateBy("document.collectionId", "document.createdDateMonth"), 1, Integer.MAX_VALUE);
		setFilters(onlyTypes, query);
		query.getFilters().setCreatedDateYear(year);
		return query;
	}

	private AggregatedQuery buildPrefixQuery(boolean onlyTypes) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(base(), new AggregateBy("document.prefix"), 1, Integer.MAX_VALUE);
		setFilters(onlyTypes, query);
		query.getFilters().setDocumentIdPrefix("luomus:F");
		query.getFilters().setDocumentIdPrefix("luomus:C");
		query.getFilters().setDocumentIdPrefix("luomus:Q");
		return query;
	}

	private AggregatedQuery buildPrefixMonthQuery(boolean onlyTypes, int year) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(base(), new AggregateBy("document.prefix", "document.createdDateMonth"), 1, Integer.MAX_VALUE);
		setFilters(onlyTypes, query);
		query.getFilters().setCreatedDateYear(year);
		query.getFilters().setDocumentIdPrefix("luomus:F");
		query.getFilters().setDocumentIdPrefix("luomus:C");
		query.getFilters().setDocumentIdPrefix("luomus:Q");
		return query;
	}


	private void setFilters(boolean onlyTypes, AggregatedQuery query) {
		query.getFilters().setSuperRecordBasis(RecordBasis.PRESERVED_SPECIMEN);
		query.getFilters().setSourceId(Const.KOTKA).setSourceId(Const.KASTIKKA);
		query.getFilters().setCollectionId(new Qname("HR.128"));
		if (onlyTypes) {
			query.getFilters().setTypeSpecimen(true);
		}
	}

	private BaseQuery base() {
		return new BaseQueryBuilder(Concealment.PRIVATE)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.setDefaultFilters(false)
				.setUseCache(true).build();
	}

	private List<DigitizationByPrefixStatistic> sortByPrefix(Collection<DigitizationByPrefixStatistic> stats) {
		List<DigitizationByPrefixStatistic> sorted = new ArrayList<>(stats);
		Collections.sort(sorted, new Comparator<DigitizationByPrefixStatistic>() {
			@Override
			public int compare(DigitizationByPrefixStatistic o1, DigitizationByPrefixStatistic o2) {
				return o1.prefix.compareTo(o2.prefix);
			}
		});
		return sorted;
	}

	private String prefix(AggregateRow result) {
		return result.getAggregateByValues().get(0).toString();
	}

	private Object collectionId(AggregateRow result) {
		return ResponseUtil.getValue(result.getAggregateByValues().get(0), "document.collectionId", getDao().getVerticaDimensionsDAO());
	}

	private ResponseData handleError(HttpServletRequest req, HttpServletResponse res, Exception e) {
		getDao().logError(Const.LAJI_ETL_QNAME, DigitsationStatisticsPage.class, null, e);
		getErrorReporter().report(DigitsationStatisticsPage.class.getName(), e);
		return error(500, res, req, e.getMessage());
	}

	private String getSubParentId(CollectionMetadata collection, Map<String, CollectionMetadata> collections) {
		while (collection.hasParent()) {
			if (isTopCollection(collection.getParentQname(), collections)) {
				return collection.getQname().toURI();
			}
			collection = collections.get(collection.getParentQname().toURI());
			if (collection == null) return null;
		}
		return null;
	}

	private boolean isTopCollection(Qname collectionId, Map<String, CollectionMetadata> collections) {
		CollectionMetadata c = collections.get(collectionId.toURI());
		if (c == null) return false;
		return !c.hasParent();
	}

}
