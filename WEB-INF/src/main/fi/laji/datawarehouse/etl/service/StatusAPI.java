package fi.laji.datawarehouse.etl.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/status/*"})
public class StatusAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -7878206415391877349L;
	private static final long MAX_ALLOWED_DIFFERENCE = 1000 * 60 * 60; // 1 hour

	private static class HealthCheckException extends Exception {
		private static final long serialVersionUID = 7360009600277055048L;
		public HealthCheckException(String message) {
			super(message);
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			checkPipeProcessingNotBlocked();
			checkPublicQueryDAO();
			checkTaxonLoaderStatus();
			checkPersonLoaderStatus();
		} catch (HealthCheckException e) {
			return error(e.getMessage(), res);
		} catch (Exception e) {
			Throwable t = e;
			while (t != null) {
				if (t instanceof HealthCheckException) {
					return error(t.getMessage(), res);
				}
				t = t.getCause();
			}
			return error(e.getMessage(), res);
		}
		return ok(res);
	}

	private void checkPersonLoaderStatus() throws HealthCheckException {
		try {
			Qname id = getDao().getPersonLookupStructure().get(new Qname("MA.1").toURI());
			if (id == null) throw new NullPointerException();
		} catch (Exception e) {
			throw new HealthCheckException("Person lookup failure");
		}
	}

	private void checkTaxonLoaderStatus() throws HealthCheckException {
		try {
			Qname taxonId = getDao().getTaxonLinkingService().getTaxonId("aves");
			if (taxonId == null) throw new NullPointerException();
		} catch (Exception e) {
			throw new HealthCheckException("Taxon lookup failure");
		}
	}

	private void checkPublicQueryDAO() throws HealthCheckException, NoSuchFieldException {
		final ListQuery query = new ListQuery(new BaseQueryBuilder(Concealment.PUBLIC)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.build(), 1, 1);
		query.getFilters().setTarget("susi").setTaxonId(new Qname("MX.46549")).setTime("2010/2011");
		try {
			checkPublicQueryDAOWithTimeout(query);
		} catch (TimeoutException e) {
			throw new HealthCheckException("Public query timeout: " + query.getFilters().getSetFiltersExcludingDefaults());
		}

		final AggregatedQuery annotationQuery = new AggregatedQuery(
				new BaseQueryBuilder(Concealment.PUBLIC)
				.setBase(Base.ANNOTATION)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.setUseCache(true).build(),
				new AggregateBy(Base.ANNOTATION, false).addField("unit.annotations.annotationByPerson").addField("unit.annotations.annotationByPersonName"),
				1, 50);
		annotationQuery.setOnlyCountRequest(true);
		annotationQuery.getFilters().setInformalTaxonGroupId(new Qname("MVL.2"));
		annotationQuery.getFilters().setAnnotationType(AnnotationType.USER_EFFECTIVE);

		try {
			checkPublicQueryDAOWithTimeout(annotationQuery);
		} catch (TimeoutException e) {
			throw new HealthCheckException("Public query timeout: " + annotationQuery.getFilters().getSetFiltersExcludingDefaults());
		}
	}

	private void checkPublicQueryDAOWithTimeout(final BaseQuery query) throws TimeoutException {
		Utils.executeWithTimeOut(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				checkpublicQueryDAO(query);
				return null;
			}
		}, 10, TimeUnit.SECONDS);
	}

	private void checkpublicQueryDAO(final BaseQuery query) throws HealthCheckException {
		try {
			if (query instanceof ListQuery) {
				if (getDao().getPublicVerticaDAO().getQueryDAO().getList((ListQuery)query).getResults().isEmpty()) {
					throw new HealthCheckException("Public list query api is not returning results for test query " + query.getFilters().getSetFiltersExcludingDefaults());
				}
			}
			if (query instanceof AggregatedQuery) {
				if (getDao().getPublicVerticaDAO().getQueryDAO().getAggregate((AggregatedQuery)query).getResults().isEmpty()) {
					throw new HealthCheckException("Public aggregate query api is not returning results for test query " + query.getFilters().getSetFiltersExcludingDefaults());
				}
			}
		} catch (HealthCheckException e) {
			throw e;
		} catch (Exception e) {
			throw new HealthCheckException("Public query api is in erroreous state: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
	}

	private void checkPipeProcessingNotBlocked() throws HealthCheckException {
		Date outPipeLatest = getOutPipeTimestampWithTimeOut();
		if (outPipeLatest == null) return;

		Date verticaLatest = getVerticaTimestampWithTimeOut();
		if (verticaLatest == null) return;

		long difference = outPipeLatest.getTime() - verticaLatest.getTime();
		if (difference > MAX_ALLOWED_DIFFERENCE) {
			if (getConfig().productionMode()) {
				throw new HealthCheckException("Nothing loaded to Vertica for " + (difference/1000/60) + " minutes");
			}
		}

		getTopOutPipeWithTimeOut();
	}

	private void getTopOutPipeWithTimeOut() throws HealthCheckException {
		try {
			Utils.executeWithTimeOut(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					getDao().getETLDAO().getUnprocessedNotErroneousInOrderFromInPipe(new Qname("KE.3"));
					getDao().getETLDAO().getUnprocessedNotErroneousInOrderFromOutPipe(new Qname("KE.3"));
					return null;
				}
			}, 15, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			throw new HealthCheckException("Outpipe is blocked (slow)");
		}
	}

	private Date getVerticaTimestampWithTimeOut() throws HealthCheckException {
		try {
			return Utils.executeWithTimeOut(new Callable<Date>() {
				@Override
				public Date call() throws Exception {
					return getDao().getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getLatestLoadTimestamp();
				}
			}, 10, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			throw new HealthCheckException("Query to Vertica timed out (get max load timestamp)");
		}
	}

	private Date getOutPipeTimestampWithTimeOut() throws HealthCheckException {
		try {
			return Utils.executeWithTimeOut(new Callable<Date>() {
				@Override
				public Date call() throws Exception {
					return getDao().getETLDAO().getLatestTimestampFromOutPipe();
				}
			}, 5, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			throw new HealthCheckException("Get latest timestamp failed");
		}
	}

	private ResponseData error(String message, HttpServletResponse res) throws IOException {
		res.setContentType("text/plain; charset=utf-8");
		res.setStatus(500);
		PrintWriter out = res.getWriter();
		out.write(message);
		out.flush();
		System.err.println("DW STATUS ERROR: " + message);
		return new ResponseData().setOutputAlreadyPrinted();
	}

}
