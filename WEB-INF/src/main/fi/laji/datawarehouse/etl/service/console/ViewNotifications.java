package fi.laji.datawarehouse.etl.service.console;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/notifications/*"})
public class ViewNotifications extends UIBaseServlet {

	private static final long serialVersionUID = 8849707383930570263L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req)
				.setViewName("notifications");
		if (req.getRequestURI().contains("/unsent")) {
			responseData.setData("notifications", getUnsent());
		} else {
			responseData.setData("notifications", getSent());
		}
		return responseData;
	}

	private List<AnnotationNotification> getUnsent() {
		return getDao().getETLDAO().getUnsentNotifications();
	}

	private List<AnnotationNotification> getSent() {
		return getDao().getETLDAO().getTopSentNotifications(100);
	}

}
