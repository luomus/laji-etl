package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.Iterator;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class InvasivePreventionReportGenerator extends ReportGenerator {

	private static final String UNIT_SQL = "" +
			" SELECT	d.original_document as json, u.unit_id as unitId" +
			" FROM		<SCHEMA>.unit u " +
			" JOIN		<SCHEMA>.document d ON (u.document_key = d.key) " +
			" JOIN 		<DIMENSION_SCHEMA>.collection ON (u.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" LEFT JOIN	<DIMENSION_SCHEMA>.namedplace np ON (u.namedplace_id = np.id) " +
			" ORDER BY	np.id, u.datebegin_key ";

	public InvasivePreventionReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, "inasive", Utils.set(new Qname("HR.2049")), errorReporter, dao);
	}

	@Override
	public void generateActual() {
		System.out.println("Invasive prevention report generator starts");

		write();
		writeErrors();

		System.out.println("Invasive prevention report completed");
	}

	private void write() {
		File eventFile = getFile("torjunnat");
		eventFile.write(header(Event.class));

		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				Object[] row = i.next();
				count++;
				if (count % 10000 == 0) System.out.println(" .. line " + count);
				JoinedRow joinedRow = null;
				try {
					joinedRow = toModel(row);
				} catch (Exception e) {
					error(LogUtils.buildStackTrace(e, 5));
				}
				if (joinedRow != null) {
					Event e = buildEvent(joinedRow);
					eventFile.write(line(e));
				}
			}
		}
	}

	private JoinedRow toModel(Object[] row) throws CriticalParseFailure {
		String json = (String) row[0];
		String unitId = (String) row[1];
		JoinedRow joinedRow = JsonToModel.joinedRowFromJson(new JSONObject(json), unitId);
		addLinkings(joinedRow);
		return joinedRow;
	}

	private Event buildEvent(JoinedRow row) {
		Event e = new Event();
		e.eventId = row.getDocument().getDocumentId();
		e.keywords = row.getDocument().getKeywords().stream().collect(Collectors.joining("; "));
		e.routeId = row.getDocument().getNamedPlaceId();
		e.routeName = row.getDocument().getNamedPlace() != null ? row.getDocument().getNamedPlace().getName() : null;

		e.municipality = row.getGathering().getInterpretations().getMunicipalityDisplayname();
		e.locality = row.getGathering().getLocality();
		e.date = row.getGathering().getDisplayDateTime();
		e.team = row.getGathering().getTeam().stream().collect(Collectors.joining("; "));

		if (row.getUnit().getLinkings() != null) {
			Taxon taxon = row.getUnit().getLinkings().getTaxon();
			if (taxon != null) {
				e.scientificName = taxon.getScientificName();
				e.finnishName = taxon.getVernacularName().forLocale("fi");
			}
		}

		e.abundance = row.getUnit().getAbundanceString();
		e.areaInSquareMeters = getFact(row.getUnit(), "http://tun.fi/MY.areaInSquareMeters");
		e.notes = row.getUnit().getNotes();

		e.areaKnown = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlAreaKnown");
		e.danger = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlDangerous");
		e.dangerDesc = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlDangerousDescriptio");
		e.methods = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlMethods");
		e.methodsDesc = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlMethodsDescription");
		e.effectiveness = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlEffectiveness");
		e.effectivenessDesc = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlEffectivenessNotes");
		e.needsActions = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlOpen");
		e.actionsDesc = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlOpenDescription");
		e.workHours = getFact(row.getGathering(), "http://tun.fi/MY.invasiveControlWorkHours");
		e.expensesEUR = getFact(row.getGathering(), "GF http://tun.fi/MY.invasiveControlOtherExpensesInEuros");

		e.timeCreated = DateUtils.format(row.getDocument().getFirstLoadDate(), "yyyy-MM-dd");
		e.dataOrigin = getFact(row.getDocument(), "http://tun.fi/MY.dataOrigin");

		if (row.getGathering().getConversions() != null) {
			if (row.getGathering().getConversions().getEurefGeo() != null) {
				e.ETRSTM35FIN = row.getGathering().getConversions().getEurefGeo().getWKT();
			}
		}

		e.issues = issues(row);
		return e;
	}

	private String getFact(BaseModel m, String fact) {
		return label(m.factValue(fact), "fi");
	}

	public class Event {
		@Order(0) public Qname eventId;
		@Order(0.1) public String keywords;
		@Order(1.1) public String routeId;
		@Order(1.3) public String routeName;

		@Order(2.1) public String municipality;
		@Order(2.2) public String locality;
		@Order(3) public String date;
		@Order(4) public String team;

		@Order(5.1) public String scientificName;
		@Order(5.2) public String finnishName;

		@Order(6.1) public String abundance;
		@Order(6.2) public String areaInSquareMeters; // UF http://tun.fi/MY.areaInSquareMeters	9914
		@Order(6.3) public String notes;

		@Order(7.1) public String areaKnown; // GF http://tun.fi/MY.invasiveControlAreaKnown
		@Order(7.2) public String danger; // GF http://tun.fi/MY.invasiveControlDangerous
		@Order(7.3) public String dangerDesc; // GF http://tun.fi/MY.invasiveControlDangerousDescription
		@Order(7.4) public String methods; // GF http://tun.fi/MY.invasiveControlMethods
		@Order(7.5) public String methodsDesc; // GF http://tun.fi/MY.invasiveControlMethodsDescription
		@Order(7.6) public String effectiveness; // GF http://tun.fi/MY.invasiveControlEffectiveness
		@Order(7.7) public String effectivenessDesc; // GF http://tun.fi/MY.invasiveControlEffectivenessNotes
		@Order(7.8) public String needsActions; // GF http://tun.fi/MY.invasiveControlOpen
		@Order(7.9) public String actionsDesc; // GF http://tun.fi/MY.invasiveControlOpenDescription
		@Order(7.10) public String workHours; // GF http://tun.fi/MY.invasiveControlWorkHours
		@Order(7.11) public String expensesEUR; // GF http://tun.fi/MY.invasiveControlOtherExpensesInEuros

		@Order(9.1) public String timeCreated;
		@Order(9.2) public String dataOrigin;

		@Order(10) public String ETRSTM35FIN;

		@Order(11) public String issues;
	}

}
