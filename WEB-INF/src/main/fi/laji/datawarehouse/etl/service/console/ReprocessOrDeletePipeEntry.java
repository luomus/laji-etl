package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/reprocess/*", "/console/delete/*"})
public class ReprocessOrDeletePipeEntry extends UIBaseServlet {

	private static final long serialVersionUID = -8836464283724792069L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		int id = Integer.valueOf(getId(req));
		String pipe = isInPipe(req) ? "in" : "out";

		Qname sourceId = isDelete(req) ? delete(pipe, id) : reprocess(pipe, id);

		getSession(req).setFlashSuccess("Done.");
		return new ResponseData().setRedirectLocation(getConfig().baseURL() + "/console/"+pipe+"/error/"+sourceId);
	}

	private boolean isDelete(HttpServletRequest req) {
		return req.getRequestURI().contains("/delete/");
	}

	private Qname delete(String pipe, int id) {
		return in(pipe) ? deleteInPipe(id) : deleteOutPipe(id); 
	}

	private Qname deleteOutPipe(int id) {
		ETLDAO dao = getDao().getETLDAO();
		OutPipeData data = dao.getFromOutPipe(id);
		String documentId = data.getDocumentId().toURI();
		dao.storeToInPipe(data.getSource(), "DELETE " + documentId, "text/plain");
		getThreadHandler().runInPipe(data.getSource());
		return data.getSource();
	}

	private Qname deleteInPipe(int id) {
		ETLDAO dao = getDao().getETLDAO();
		Qname source = dao.getFromInPipe(id).getSource();
		dao.removeInPipe(id);
		return source;
	}

	private Qname reprocess(String pipe, int id) {
		return in(pipe) ? reprocessInPipe(id) : reprocessOutPipe(id);
	}

	private Qname reprocessOutPipe(int id) {
		ETLDAO dao = getDao().getETLDAO();
		dao.markReprocessOutPipe(id);
		Qname source = dao.getFromOutPipe(id).getSource();
		getThreadHandler().runOutPipe(source);
		return source;
	}

	private Qname reprocessInPipe(int id) {
		ETLDAO dao = getDao().getETLDAO();
		dao.markReprocessInPipe(id);
		Qname source = dao.getFromInPipe(id).getSource();
		getThreadHandler().runInPipe(source);
		return source;
	}

	private boolean in(String pipe) {
		return "in".equals(pipe);
	}

	private boolean isInPipe(HttpServletRequest req) {
		return req.getRequestURI().contains("/in/");
	}

}
