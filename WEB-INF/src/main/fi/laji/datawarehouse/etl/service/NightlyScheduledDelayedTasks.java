package fi.laji.datawarehouse.etl.service;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader;
import fi.laji.datawarehouse.etl.threads.custom.TamperePullReader;
import fi.laji.datawarehouse.etl.threads.custom.TiiraAtlasPullReader;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporterWithLogging;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;

class NightlyScheduledDelayedTasks implements Runnable {

	private final DAO dao;
	private final Config config;
	private final ThreadHandler threadHandler;
	private final ThreadStatuses threadStatuses;

	public NightlyScheduledDelayedTasks(InitializationServlet initializationServlet) {
		this.dao = initializationServlet.getDao();
		this.config = initializationServlet.getConfig();
		this.threadHandler = initializationServlet.getThreadHandler();
		this.threadStatuses = initializationServlet.getThreadStatuses();
	}

	@Override
	public void run() {
		ThreadStatusReporterWithLogging reporterWithLogging = new ThreadStatusReporterWithLogging(threadStatuses, dao, this.getClass());
		try {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Staring delayed tasks ===== ");
			startTasks(reporterWithLogging);
		} catch (Throwable e) {
			log("nightly", e);
		} finally {
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ===== Delayed tasks started ===== ");
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private void startTasks(ThreadStatusReporter statusReporter) {
		try {
			callKastikkaSync(statusReporter);
		} catch (Throwable e) {
			log("Kastikka sync", e);
		}

		try {
			runZoobenthosPull(statusReporter);
		} catch (Throwable e) {
			log("Zoobenthos pull", e);
		}

		try {
			runTiiraAtlasPull(statusReporter);
		} catch (Throwable e) {
			log("Tiira atlas pull", e);
		}

		try {
			runTamperePull(statusReporter);
		} catch (Throwable e) {
			log("Tampere pull", e);
		}

		//		try {
		//			runHelsinkiPull(statusReporter);
		//		} catch (Throwable e) {
		//			log("Helsinki pull", e);
		//		}
	}

	//	private void runHelsinkiPull(ThreadStatusReporter statusReporter) {
	//		if (!config.productionMode()) return;
	//		statusReporter.setStatus("Run Helsinki pull");
	//		threadHandler.runPullReader(HelsinkiPullReader.source(config));
	//	}

	private void runTamperePull(ThreadStatusReporter statusReporter) {
		if (!config.productionMode()) return;
		statusReporter.setStatus("Run Tampere pull");
		threadHandler.runPullReader(TamperePullReader.source(config));
	}

	private void runTiiraAtlasPull(ThreadStatusReporter statusReporter) {
		if (!config.productionMode()) return;
		statusReporter.setStatus("Run Tiira atlas pull");
		threadHandler.runPullReader(TiiraAtlasPullReader.source(config));
	}

	private void runZoobenthosPull(ThreadStatusReporter statusReporter) {
		if (!config.productionMode()) return;
		statusReporter.setStatus("Run Zoobenthos pull");
		threadHandler.runPullReader(SykeZoobenthosPullReader.SOURCE);
	}

	private void callKastikkaSync(ThreadStatusReporter statusReporter) {
		if (!config.productionMode()) return;
		statusReporter.setStatus("Calling Kastikka sync");
		dao.getETLDAO().callKastikkaSync();
	}

	private void log(String info, Throwable e) {
		dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), info, e);
	}

}