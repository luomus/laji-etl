package fi.laji.datawarehouse.etl.service.console;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.StatelessSession;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.oracle.AnnotationEntity;
import fi.laji.datawarehouse.dao.oracle.FirstLoadTimesEntity;
import fi.laji.datawarehouse.dao.oracle.NotificationEntity;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentQueueEntity;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;

@WebServlet(urlPatterns = {"/console/other-stats"})
public class GetOtherStats extends UIBaseServlet {

	private static final long serialVersionUID = -2367987879084810172L;

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		return status403(res);
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("otherStats");
		DAO dao = getDao();
		unitAccumulationStats(responseData, dao);
		sourceDocumentLoadStats(responseData, dao);
		annotationStats(responseData, dao);
		spllitedQueueStats(responseData, dao);
		splittedDocIdStats(responseData, dao);
		firstLoadDateStats(responseData, dao);
		notificationStats(responseData, dao);
		return responseData;
	}

	private void notificationStats(ResponseData responseData, DAO dao) {
		try (StatelessSession session = dao.getETLDAO().getETLEntityConnection()) {
			@SuppressWarnings("unchecked")
			List<Object[]> res = session.createCriteria(NotificationEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("sent"))
					.add(Projections.rowCount()))
			.addOrder(Order.asc("sent"))
			.list();
			for (Object[] row : res) {
				if (row[0].toString().equals("1")) {
					responseData.setData("notificationSentCount", row[1]);
				} else {
					responseData.setData("notificationUnsentCount", row[1]);
				}
			}
		}
	}

	private void firstLoadDateStats(ResponseData responseData, DAO dao) {
		responseData.setData("firstLoadDateCount", getCount(dao, FirstLoadTimesEntity.class));
	}

	private void splittedDocIdStats(ResponseData responseData, DAO dao) {
		responseData.setData("splittedDocumentIdCount", getCount(dao, SplittedDocumentIdEntity.class));
	}

	private void spllitedQueueStats(ResponseData responseData, DAO dao) {
		responseData.setData("splittedDocumentQueueCount", getCount(dao, SplittedDocumentQueueEntity.class));
	}

	private void annotationStats(ResponseData responseData, DAO dao) {
		responseData.setData("annotationCount", getCount(dao, AnnotationEntity.class));
	}

	private long getCount(DAO dao, Class<?> cls) {
		try (StatelessSession session = dao.getETLDAO().getETLEntityConnection()) {
			return (long) session.createCriteria(cls).setProjection(Projections.rowCount()).uniqueResult();
		}
	}

	public static class DayCount {
		private final String dayString;
		private final long count;
		public DayCount(Date day, long count) {
			DateValue dateValue = DateUtils.convertToDateValue(day);
			this.dayString = dateValue.getYear() + ", " + (dateValue.getMonthAsInt() - 1) + ", " + dateValue.getDay();
			this.count = count;
		}
		public String getDay() {
			return dayString;
		}
		public long getCount() {
			return count;
		}
	}

	private void unitAccumulationStats(ResponseData responseData, DAO dao) throws Exception {

		List<AggregateRow> unitsByDay = twoYearData(dao);

		List<DayCount> unitAccumulation = new ArrayList<>();
		long accumulation = startCountBeforeTwoYears(dao);
		for (AggregateRow row : unitsByDay) {
			Date date = (Date) row.getAggregateByValues().get(0);
			Long count = row.getCount();
			accumulation += count;
			unitAccumulation.add(new DayCount(date, accumulation));
		}
		responseData.setData("unitAccumulation", unitAccumulation);
	}

	private long startCountBeforeTwoYears(DAO dao) {
		CountQuery query = new CountQuery(createBase());
		query.getFilters().setFirstLoadedSameOrBefore(twoYears());
		return dao.getPrivateVerticaDAO().getQueryDAO().getCount(query).getTotal();
	}

	private List<AggregateRow> twoYearData(DAO dao) throws NoSuchFieldException {
		AggregatedQuery query = new AggregatedQuery(
				createBase(),
				new AggregateBy("document.firstLoadDate"),
				1, 1000).setOrderBy(new OrderBy("document.firstLoadDate"));
		query.getFilters().setFirstLoadedSameOrAfter(twoYears());

		return dao.getPrivateVerticaDAO().getQueryDAO().getRawAggregate(query);
	}

	private BaseQuery createBase() {
		return new BaseQueryBuilder(Concealment.PRIVATE)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.setDefaultFilters(false).build();
	}

	private Date twoYears() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -2);
		return c.getTime();
	}

	private void sourceDocumentLoadStats(ResponseData responseData, DAO dao) {
		responseData.setData("loadStats", dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getLoadStats());
	}

}
