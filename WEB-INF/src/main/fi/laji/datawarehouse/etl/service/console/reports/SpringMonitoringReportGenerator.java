package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.Iterator;
import java.util.Map;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Utils;

public class SpringMonitoringReportGenerator extends CensusReportGenerator {

	private static final String YEARFILTER = "[YEARFILTER]";
	private static final Qname SPRINGM = new Qname("HR.206");
	private static final Qname INAT = new Qname("HR.3211");
	private static final String INAT_PROJECT = "project-95747";

	private static final String OBSERVER_SQL = "" +
			" SELECT" +
			"			g.year AS year, email.value AS email, kevatseurantaInfo.value AS kevatseurantaInfo, luontoliittoInfo.value AS luontoliittoInfo, count(1) AS count " +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN		<DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id = '"+SPRINGM.toURI()+"')  " +
			" JOIN		<SCHEMA>.df email ON (email.document_key = g.document_key AND email.property = 'email') " +
			" LEFT JOIN	<SCHEMA>.df kevatseurantaInfo on (kevatseurantaInfo.document_key = g.document_key AND kevatseurantaInfo.property = 'requestKevatseurantaInfo') " +
			" LEFT JOIN	<SCHEMA>.df luontoliittoInfo on (luontoliittoInfo.document_key = g.document_key AND luontoliittoInfo.property = 'requestLLInfo') " +
			" WHERE 1=1 " +
			YEARFILTER +
			" GROUP BY	year, email, kevatseurantaInfo, luontoliittoInfo " +
			" ORDER BY	1, 2 ";

	private static final String UNIT_SQL = "" +
			" SELECT " +
			" u.datebegin_key as datestart, u.dateend_key as dateend, u.displaydatetime, u.dayofyearbegin, u.dayofyearend, u.year, " +
			" u.municipality_displayname as municipality, u.locality, " +
			" u.latitudecenter, u.longitudecenter, u.euref_wkt, " +
			" u.taxon_verbatim, taxon.key, taxon.scientific_name, " +
			" u.abundancestring, " +
			" team.team_text,  email.value as email, " +
			" collection.id, u.document_id, u.unit_id " +
			" FROM 		<SCHEMA>.unit u " +
			" JOIN 		<DIMENSION_SCHEMA>.collection ON (u.collection_key = collection.key and collection.id IN ("+COLLECTIONS+")) " +
			" JOIN 		<DIMENSION_SCHEMA>.target ON u.target_key = target.key " +
			" LEFT JOIN	<DIMENSION_SCHEMA>.taxon ON target.taxon_key = taxon.key " +
			" JOIN 		<DIMENSION_SCHEMA>.team ON u.team_key = team.key " +
			" LEFT JOIN <SCHEMA>.df email on (email.document_key = u.document_key and email.property = 'email') " +
			" WHERE issues = false " +
			" AND   ( collection.id = '" + SPRINGM.toURI() + "' " +
			"         OR u.document_key IN ( SELECT document_key FROM <SCHEMA>.document_keyword WHERE keyword = '"+INAT_PROJECT+"') )" +
			YEARFILTER +
			" ORDER BY u.datebegin_key desc, u.municipality_displayname, taxon.taxonomic_order, u.taxon_verbatim ";

	private final Integer year;

	public SpringMonitoringReportGenerator(Integer year, Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, "kevatseuranta", Utils.set(SPRINGM, INAT), errorReporter, dao);
		this.year = year;
	}

	@Override
	public void generateActual() {
		System.out.println("Spring monitoring report starts");

		Map<Qname, AnnotationData> annotations = getAnnotations();

		writeObservations(annotations);

		writeObservers();

		writeErrors();

		System.out.println("Spring monitoring report completed");
	}

	private void writeObservers() {
		File observersFile = getFile("henkilöt");
		observersFile.write(header(Observer.class));

		try (ResultStream<Object[]> results = getCustomQuery(setYearFilter(OBSERVER_SQL))) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observer line " + count);
				Object[] row = i.next();
				Observer observer = buildObserver(row);
				observersFile.write(line(observer));
			}
		}
	}

	private Observer buildObserver(Object[] row) {
		Observer o = new Observer();
		int i = 0;
		o.year = integer(row[i++]);
		o.email = string(row[i++]);
		o.kevatseurantaInfo = bool(row[i++]);
		o.luontoliittoInfo = bool(row[i++]);
		o.count = integer(row[i++]);
		return o;
	}

	private void writeObservations(Map<Qname, AnnotationData> annotations) {
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));

		try (ResultStream<Object[]> results = getCustomQuery(setYearFilter(UNIT_SQL))) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				Observation observation = buildObservation(row, annotations);
				observationsFile.write(line(observation));
			}
		}
	}

	private String setYearFilter(String sql) {
		if (year == null) return sql.replace(YEARFILTER, " ");
		return sql.replace(YEARFILTER, " AND year = " + year + " ");
	}

	private Observation buildObservation(Object[] row, Map<Qname, AnnotationData> annotations) {
		int i = 0;
		Observation obs = new Observation();
		obs.startDate = integer(row[i++]);
		obs.endDate = integer(row[i++]);
		obs.displayDate = string(row[i++]);
		obs.dayOfYearStart = integer(row[i++]);
		obs.dayOfYearEnd = integer(row[i++]);
		obs.year = integer(row[i++]);

		obs.municipality = string(row[i++]);
		obs.locality = string(row[i++]);
		obs.lat = doubleVal(row[i++]);
		obs.lon = doubleVal(row[i++]);
		obs.eurefWKT = string(row[i++]);

		obs.taxonVerbatim = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		obs.scientificName = string(row[i++]);

		obs.abundance = string(row[i++]);
		obs.team = string(row[i++]);
		obs.email = string(row[i++]);

		obs.source = toColName(qname(row[i++]));
		obs.documentId = qname(row[i++]);
		obs.unitId = qname(row[i++]);

		if (annotations.containsKey(obs.unitId)) {
			obs.quality = annotations.get(obs.unitId).getTags();
			obs.qualityNotes = annotations.get(obs.unitId).getNotes();
		}
		return obs;
	}

	private String toColName(Qname qname) {
		if (SPRINGM.equals(qname)) return "Kevatseuranta";
		if (INAT.equals(qname)) return "iNaturalist";
		return "unknown";
	}

	public class Observer {
		@Order(0) public Integer year;
		@Order(1) public String email;
		@Order(2) public Boolean kevatseurantaInfo;
		@Order(3) public Boolean luontoliittoInfo;
		@Order(4) public Integer count;
	}

	public class Observation {
		@Order(1) public Integer year;
		@Order(2) public Integer startDate;
		@Order(3) public Integer endDate;
		@Order(4) public String displayDate;
		@Order(5) public Integer dayOfYearStart;
		@Order(6) public Integer dayOfYearEnd;

		@Order(7) public String municipality;
		@Order(8) public String locality;

		@Order(9) public Double lat;
		@Order(10) public Double lon;
		@Order(11) public String eurefWKT;

		@Order(12) public String taxonVerbatim;
		@Order(13) public Qname taxonId;
		@Order(14) public String scientificName;

		@Order(15) public String abundance;

		@Order(16) public String team;
		@Order(17) public String email;

		@Order(18) public String source;
		@Order(19) public Qname documentId;
		@Order(20) public Qname unitId;

		@Order(21) public String quality;
		@Order(22) public String qualityNotes;
	}

}

