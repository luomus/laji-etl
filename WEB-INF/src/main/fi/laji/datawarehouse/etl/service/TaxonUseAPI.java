package fi.laji.datawarehouse.etl.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/taxon-use/*"})
public class TaxonUseAPI extends ETLBaseServlet {

	private static final long serialVersionUID = -1411795686257381544L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			Qname taxonId = getTaxonId(req);
			if (invalid(taxonId)) return unloggedUserError400(new IllegalArgumentException("Invalid taxon qname given " + taxonId), res, req);
			boolean used = isUsed(taxonId);
			JSONObject response = new JSONObject().setBoolean("used", used);
			return jsonResponse(response);
		} catch (Throwable e) {
			return loggedSystemError500(e, TaxonUseAPI.class, res, req);
		}
	}

	private boolean invalid(Qname taxonId) {
		try {
			return taxonId == null || !taxonId.isSet() || !taxonId.toString().startsWith("MX.") || TaxonBaseEntity.parseTaxonKey(taxonId).longValue() < 1;
		} catch (Exception e) {
			return true;
		}
	}

	private boolean isUsed(Qname taxonId) {
		return getDao().getPrivateVerticaDAO().getQueryDAO().getCustomQueries().isTaxonIdUsed(taxonId);
	}

	private Qname getTaxonId(HttpServletRequest req) {
		return new Qname(getId(req));
	}

}
