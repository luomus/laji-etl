package fi.laji.datawarehouse.etl.service.console.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.io.ByteStreams;

import fi.laji.datawarehouse.etl.service.console.UIBaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.FileUtils;

@WebServlet(urlPatterns = {"/console/reports/download/*"})
public class DownloadReportServlet extends UIBaseServlet {

	private static final long serialVersionUID = 6975442842427020832L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String filename = getId(req);
		File file = new File(getConfig().reportFolder(), filename);
		if (!file.exists()) throw new IllegalStateException("File " + filename + " does not exist!");

		OutputStream out = null;
		FileInputStream in = null;
		try {
			res.setContentType("application/zip");
			res.setHeader("Content-disposition","attachment; filename=" + filename);
			out = res.getOutputStream();
			in = new FileInputStream(file);
			ByteStreams.copy(in, out);
			return new ResponseData().setOutputAlreadyPrinted();
		} finally {
			FileUtils.close(in);
			if (out != null) out.flush();
		}
	}

}
