package fi.laji.datawarehouse.etl.service.console;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/add-reprocess-temp"})
public class AddToReprocessTemp extends UIBaseServlet {

	private static final long serialVersionUID = 8793279578993682657L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData(req).setViewName("add-reprocess-temp");
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		String data = req.getParameter("document_ids");
		Set<String> documentIds = parseDocumentIds(data);
		startRunningInBackground(documentIds);
		getSession(req).setFlashSuccess("Started running in background - check thread status and log for progress");
		return new ResponseData().setRedirectLocation(getConfig().baseURL() + "/console/add-reprocess-temp");
	}

	private Set<String> parseDocumentIds(String data) {
		Set<String> documentIds = new HashSet<>(40000);
		data = data.replace("\r", "");
		for (String line : data.split(Pattern.quote("\n"))) {
			String documentId = parseDocumentId(line);
			if (given(documentId)) {
				documentIds.add(documentId);
			}
		}
		return documentIds;
	}

	private String parseDocumentId(String line) {
		line = Utils.removeWhitespaceAround(line);
		if (!line.startsWith("http")) return null;
		return line;
	}

	private void startRunningInBackground(Set<String> documentIds) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					store(documentIds);
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, AddToReprocessTemp.class, null, e);
				}
			}

		}).start();
	}

	private void store(Set<String> documentIds) {
		try {
			DAO dao = getDao();

			log("Replacing splitted document ids with actuals", dao);
			documentIds = replaceSplitted(documentIds, dao);

			log("Storing " + documentIds.size() + " reprocess ids (clears old entries)", dao);
			dao.getETLDAO().clearReprocessDocumentIds();
			dao.getETLDAO().storeReprocessDocumentIds(documentIds);

			log("Exiting", dao);
		} finally {
			getThreadStatuses().reportThreadDead(AddToReprocessTemp.class);
		}
	}

	private Set<String> replaceSplitted(Set<String> documentIds, DAO dao) {
		Set<String> actual = new HashSet<>(documentIds.size());
		Map<String, SplittedDocumentIdEntity> splitted = dao.getETLDAO().getAllSplittedIdToOriginalId();
		log("Loaded " + splitted.size() +" spitted document ids", dao);
		int i = 0;
		for (String documentId : documentIds) {
			if (i++ % 100000 == 0) log("" + i + " / " + documentIds.size(), dao);
			if (documentId.startsWith("http://tun.fi/A.")) {
				SplittedDocumentIdEntity s = splitted.get(documentId);
				if (s != null) {
					actual.add(s.getOriginalDocumentId());
				} else {
					actual.add(documentId);
				}
			} else {
				actual.add(documentId);
			}
		}
		return actual;
	}

	private void log(String message, DAO dao) {
		dao.logMessage(Const.LAJI_ETL_QNAME, AddToReprocessTemp.class, message);
		status(message);
	}

	private void status(String message) {
		getThreadStatuses().getThreadStatusReporterFor(AddToReprocessTemp.class).setStatus(message);
	}

}
