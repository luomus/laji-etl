package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class LineTransectReportGenerator extends CensusReportGenerator {

	private static final String FILENAME_PREFIX = "linjalaskenta";

	private static final String DOCUMENT_SQL = "" +
			" SELECT    d.document_id, collection.id as collectionId, collection.name as collectionName, " +
			"           np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje, " +
			"           min(g.datebegin_key) as date, sum(g.linelength) as linelength," +
			"           timeStart.value as timeStart, timeEnd.value as timeEnd, aborted.value as aborted, hindered.value as hindered, gps.value as gps, breaks.value as breaks " +
			" FROM      <SCHEMA>.document d " +
			" JOIN      <SCHEMA>.gathering g ON (g.document_key = d.key) " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (d.namedplace_id = np.id) " +
			" LEFT JOIN <SCHEMA>.df timeStart ON (timeStart.document_key = d.key AND timeStart.property = 'http://tun.fi/MY.timeStart') " +
			" LEFT JOIN <SCHEMA>.df timeEnd ON (timeEnd.document_key = d.key AND timeEnd.property = 'http://tun.fi/MY.timeEnd') " +
			" LEFT JOIN <SCHEMA>.df aborted ON (aborted.document_key = d.key AND aborted.property = 'http://tun.fi/MY.censusPrematurelyAborted') " +
			" LEFT JOIN <SCHEMA>.df hindered ON (hindered.document_key = d.key AND hindered.property = 'http://tun.fi/MY.censusHinderedByEnviromentalFactors') " +
			" LEFT JOIN <SCHEMA>.df gps ON (gps.document_key = d.key AND gps.property = 'http://tun.fi/MY.gpsUsed') " +
			" LEFT JOIN <SCHEMA>.df breaks ON (breaks.document_key = d.key AND breaks.property = 'http://tun.fi/MY.breaksDuringCensusInMinutes') " +
			" LEFT JOIN <SCHEMA>.gf counted ON (counted.gathering_key = g.key AND counted.property = 'http://tun.fi/MY.lineTransectSegmentCounted') " +
			" WHERE     (counted.value IS NULL OR counted.value = 'true') " +
			" GROUP BY  d.document_id, collectionId, collectionName, routeId, routeName, municipality, ykjn, ykje, timeStart,timeEnd, aborted, hindered, gps, breaks " +
			" ORDER BY  collectionId, routeName, routeId, date ";

	private static final String GATHERING_SQL = "" +
			" SELECT    g.document_id, g.gathering_id, team.team_text as team, " +
			"           biotopeTime.value as biotopeTime, " +
			"           linelength, mStart.value_integer as startM, mEnd.value_integer as endM, " +
			"           biotope.value as biotope, counted.value as counted, " +
			"           ykjnmin, ykjnmax, ykjemin, ykjemax, " +
			"           document_issue_message, gathering_issue_message, timeissue_message, locationissue_message " +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (g.namedplace_id = np.id) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.team ON (g.team_key = team.key) " +
			" LEFT JOIN <SCHEMA>.gf biotopeTime ON (biotopeTime.gathering_key = g.key AND biotopeTime.property = 'http://tun.fi/MY.timeStart') " +
			" LEFT JOIN <SCHEMA>.gf mStart ON (mStart.gathering_key = g.key AND mStart.property = 'http://tun.fi/MY.lineTransectSegmentMetersStart') " +
			" LEFT JOIN <SCHEMA>.gf mEnd ON (mEnd.gathering_key = g.key AND mEnd.property = 'http://tun.fi/MY.lineTransectSegmentMetersEnd') " +
			" LEFT JOIN <SCHEMA>.gf biotope ON (biotope.gathering_key = g.key AND biotope.property = 'http://tun.fi/MY.habitatClassification') " +
			" LEFT JOIN <SCHEMA>.gf counted ON (counted.gathering_key = g.key AND counted.property = 'http://tun.fi/MY.lineTransectSegmentCounted') " +
			" WHERE     (counted.value IS NULL OR counted.value = 'true') " +
			" ORDER BY  g.document_id, startM ";

	private static final String UNIT_SQL = "" +
			" SELECT    unit.document_id, unit.gathering_id, unit.unit_id, " +
			"           unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name, " +
			"           field.value as field, type.value as type, unit.abundanceString, unit.pairCount, " +
			"           broodSize.value_integer as broodSize, migrant.value as migrant, notes, unit_issue_message " +
			" FROM      <SCHEMA>.unit " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (unit.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (unit.namedplace_id = np.id) " +
			" JOIN      <DIMENSION_SCHEMA>.target ON unit.target_key = target.key " +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxon ON target.taxon_key = taxon.key " +
			" LEFT JOIN <SCHEMA>.uf field ON (field.unit_key = unit.key and field.property = 'http://tun.fi/MY.lineTransectRouteFieldType') " +
			" LEFT JOIN <SCHEMA>.uf type ON (type.unit_key = unit.key and type.property = 'http://tun.fi/MY.lineTransectObsType') " +
			" LEFT JOIN <SCHEMA>.uf migrant ON (migrant.unit_key = unit.key and migrant.property = 'http://tun.fi/MY.likelyMigrant') " +
			" LEFT JOIN <SCHEMA>.uf broodSize ON (broodSize.unit_key = unit.key and broodSize.property = 'http://tun.fi/MY.broodSize') " +
			" ORDER BY  np.name, np.id, unit.datebegin_key, unit.document_id, unit.gathering_id, unit.unit_order ";

	public LineTransectReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, FILENAME_PREFIX, Const.BIRD_LINE_TRANSECT_COLLECTION, errorReporter, dao);
	}

	@Override
	public void generateActual() {
		System.out.println("Line-transect report starts");

		Map<Qname, AnnotationData> annotations = getAnnotations();

		Map<Qname, Event> events = getEvents(annotations);
		System.out.println("Events: " + events.size());

		Map<Qname, BiotopeStrip> biotopes = getBiotopesFillObserverNames(events);
		System.out.println("Biotope strips: " + biotopes.size());

		writeEvents(events);
		System.out.println("Event file written");

		writeBiotopes(biotopes);
		System.out.println("Biotope file written");

		writeEventSumsAndObservations(events, biotopes, annotations);
		System.out.println("Observations and sums files written");

		writeErrors();

		System.out.println("Line-transect report completed");
	}

	private void writeBiotopes(Map<Qname, BiotopeStrip> biotopes) {
		File biotopeFile = getFile("biotoopit");
		biotopeFile.write(header(BiotopeStrip.class));
		for (BiotopeStrip biotope : biotopes.values()) {
			biotopeFile.write(line(biotope));
		}
	}

	private void writeEvents(Map<Qname, Event> events) {
		File eventFile = getFile("laskennat");
		eventFile.write(header(Event.class));
		for (Event event : events.values()) {
			eventFile.write(line(event));
		}
	}

	private void writeEventSumsAndObservations(Map<Qname, Event> events, Map<Qname, BiotopeStrip> biotopes, Map<Qname, AnnotationData> annotations) {
		File eventSumFile = getFile("summat");
		eventSumFile.write(header(EventSums.class));
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));

		Qname currentEventId = new Qname("");
		Map<String, EventSums> taxaSums = new LinkedHashMap<>();
		Map<Integer, List<Observation>> eventObservations = new TreeMap<>();

		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				//  SELECT    unit.document_id, unit.gathering_id, ...
				//  ORDER BY  np.name, np.id, unit.datebegin_key, unit.document_id, unit.gathering_id, unit.unit_order
				Qname eventId = qname(row[0]);
				Qname biotopeId = qname(row[1]);
				Event event = events.get(eventId);
				BiotopeStrip biotope = biotopes.get(biotopeId);
				if (biotope == null || biotope.startMeters == null) {
					error("Missing biotope or start meters: " + biotopeId + " / " + eventId);
				} else {
					Observation observation = buildObservation(row, event, biotope, annotations);
					if (!currentEventId.equals(observation.eventId)) {
						writeSums(eventSumFile, taxaSums);
						writeObservations(observationsFile, eventObservations);
						taxaSums.clear();
						eventObservations.clear();
						currentEventId = observation.eventId;
					}
					addSums(observation, taxaSums, event);
					addObservation(eventObservations, biotope, observation);
				}
			}
			if (!taxaSums.isEmpty()) {
				writeSums(eventSumFile, taxaSums);
			}
			if (!eventObservations.isEmpty()) {
				writeObservations(observationsFile, eventObservations);
			}
		}
	}

	private void addObservation(Map<Integer, List<Observation>> eventObservations, BiotopeStrip biotope, Observation observation) {
		if (!eventObservations.containsKey(biotope.startMeters)) {
			eventObservations.put(biotope.startMeters, new ArrayList<Observation>());
		}
		eventObservations.get(biotope.startMeters).add(observation);
	}

	private void addSums(Observation observation, Map<String, EventSums> taxaSums, Event event) {
		String key = scientificName(observation);
		if (!taxaSums.containsKey(key)) {
			taxaSums.put(key, buildEventSums(observation, event));
		}
		if (observation.pairCount != null) {
			if ("P".equals(observation.field)) {
				taxaSums.get(key).innerCount += observation.pairCount;
			} else if ("A".equals(observation.field)) {
				taxaSums.get(key).outerCount += observation.pairCount;
			}
		}
	}

	private EventSums buildEventSums(Observation observation, Event event) {
		EventSums sums = new EventSums();
		sums.eventId = event.eventId;
		sums.routeId = event.routeId;
		sums.year = event.year;
		sums.linelength = event.linelength;
		sums.scientificName = scientificName(observation);
		sums.birdCode = observation.birdCode;
		return sums;
	}

	private String scientificName(Observation observation) {
		if (observation.scientificName !=  null) return observation.scientificName;
		return observation.reportedName;
	}

	private void writeSums(File eventSumFile, Map<String, EventSums> taxaSums) {
		for (EventSums sums : taxaSums.values()) {
			eventSumFile.write(line(sums));
		}
	}

	private void writeObservations(File observationsFile, Map<Integer, List<Observation>> eventObservations) {
		for (Map.Entry<Integer, List<Observation>> e : eventObservations.entrySet()) {
			for (Observation o : e.getValue()) {
				observationsFile.write(line(o));
			}
		}
	}

	private Observation buildObservation(Object[] row, Event event, BiotopeStrip biotope, Map<Qname, AnnotationData> annotations) {
		//		 SELECT    unit.document_id, unit.gathering_id, unit.unit_id,
		//				   unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,
		//				   field.value as field, type.value as type, unit.abundanceString, unit.pairCount,
		//				   broodSize.value as broodSize, migrant.value as migrant, notes, unit_issue_message
		Observation obs = new Observation();
		obs.eventId = event.eventId;
		obs.routeId = event.routeId;
		obs.year = event.year;
		obs.biotopeStripId = biotope.biotopeStripId;
		obs.startMeters = biotope.startMeters;
		obs.endMeters = biotope.endMeters;
		obs.meters = biotope.meters;
		obs.biotope = biotope.biotope;
		obs.biotopeSpecifier = biotope.biotopeSpecifier;
		obs.area = biotope.area;
		obs.height = biotope.height;
		obs.startHour = biotope.startHour;
		obs.startMinute = biotope.startMinute;
		obs.endHour = biotope.endHour;
		obs.endMinute = biotope.endMinute;
		obs.stripStartHour = biotope.stripStartHour;
		obs.stripStartMinute = biotope.stripStartMinute;
		int i = 2;
		obs.unitId = qname(row[i++]);
		obs.reportedName = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		String birdLifeCode = string(row[i++]);
		String euringCode = string(row[i++]);
		obs.birdCode = birdLifeCode != null ? birdLifeCode : euringCode;
		obs.scientificName = string(row[i++]);

		String fieldType = string(row[i++]);
		String obsType = string(row[i++]);

		obs.field = toFieldType(fieldType);
		obs.type = toObsType(obsType);
		obs.individualCount = iVal(string(row[i++]), "Invalid individual count for " + event.eventId + " " + obs.unitId);
		obs.pairCount = integer(row[i++]);
		obs.broodSize =integer(row[i++]);
		obs.migrant = bool(row[i++]);
		obs.notes = string(row[i++]);
		if (annotations.containsKey(obs.unitId)) {
			obs.quality = annotations.get(obs.unitId).getTags();
			obs.qualityNotes = annotations.get(obs.unitId).getNotes();
		}
		obs.issues = issues(biotope.issues, row, i);
		return obs;
	}

	private static final Map<Qname, String> OBS_TYPE_MAP;
	static {
		OBS_TYPE_MAP = new HashMap<>();
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSong"), "X");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeOtherSound"), "V");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeen"), "O");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeenMale"), "K");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeenFemale"), "N");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeFlyingOverhead"), "Y");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeFlock"), "AO");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeFlockFlyingOverhead"), "AY");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeenPair"), "PAIR");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeenBrood"), "BROOD");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeSeenNest"), "NEST");
		OBS_TYPE_MAP.put(new Qname("MY.lineTransectObsTypeUnknown"), "UNKNOWN");
	}

	private String toObsType(String obsType) {
		if (obsType == null) return null;
		return OBS_TYPE_MAP.get(Qname.fromURI(obsType));
	}

	private String toFieldType(String fieldType) {
		if (fieldType == null) return null;
		Qname qname = Qname.fromURI(fieldType);
		if ("MY.lineTransectRouteFieldTypeInner".equals(qname.toString())) return "P";
		if ("MY.lineTransectRouteFieldTypeOuter".equals(qname.toString())) return "A";
		return null;
	}

	private Map<Qname, BiotopeStrip> getBiotopesFillObserverNames(Map<Qname, Event> events) {
		Map<Qname, BiotopeStrip> biotopes = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(GATHERING_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				BiotopeStrip biotope = buildBiotopeFillObserverName(row, events);
				if (biotope != null) {
					biotopes.put(biotope.biotopeStripId, biotope);
				}
			}
		}
		return biotopes;
	}

	private BiotopeStrip buildBiotopeFillObserverName(Object[] row, Map<Qname, Event> events) {
		//		SELECT    g.document_id, g.gathering_id, team.team_text as team,
		//		          biotopeTime.value as biotopeTime,
		//		          linelength, mStart.value_integer as startM, mEnd.value_integer as endM,
		//		          biotope.value as biotope, counted.value as counted,
		//		          ykjnmin, ykjnmax, ykjemin, ykjemax
		BiotopeStrip biotope = new BiotopeStrip();
		int i = 0;
		biotope.eventId = qname(row[i++]);
		biotope.biotopeStripId = qname(row[i++]);

		Event event = events.get(biotope.eventId);
		if (event == null) {
			error("No event for biotope strip " + biotope.biotopeStripId);
			return null;
		}
		biotope.routeId = event.routeId;

		String team = string(row[i++]);
		if (given(team)) {
			event.observers = team;
		}

		biotope.year = event.year;

		Time stripTime = time(row[i++], "Invalid time for " + event.eventId + " " + biotope.biotopeStripId);
		biotope.startHour = event.startHour;
		biotope.startMinute = event.startMinute;
		biotope.endHour = event.endHour;
		biotope.endMinute = event.endMinute;
		if (stripTime != null) {
			biotope.stripStartHour = stripTime.hour;
			biotope.stripStartMinute = stripTime.minute;
		}

		biotope.meters = integer(row[i++]);
		biotope.startMeters = integer(row[i++]);
		biotope.endMeters = integer(row[i++]);

		String biotopeCode = string(row[i++]);
		if (given(biotopeCode)) {
			try {
				biotope.biotope = charAt(biotopeCode, 0);
				biotope.biotopeSpecifier = charAt(biotopeCode, 1);
				if (biotopeCode.length() > 3) {
					String measurementType = charAt(biotopeCode, 2);
					Integer measurement = iVal(biotopeCode.substring(3), "Biotope error for event " + event.eventId);
					if ("A".equals(measurementType)) {
						biotope.area = measurement;
					} else if ("K".equals(measurementType)) {
						biotope.height = measurement;
					}
				}
			} catch (Exception e) {
				error("Biotope error: " + e.getMessage() + " for event " + event.eventId);
			}
		}

		biotope.counted = bool(row[i++]);

		biotope.ykjLatMin = integer(row[i++]);
		biotope.ykjLatMax = integer(row[i++]);
		biotope.ykjLonMin = integer(row[i++]);
		biotope.ykjLonMax = integer(row[i++]);

		biotope.issues = issues(row, i);
		return biotope;
	}

	private String charAt(String s, int index) {
		if (s.length() > index) {
			return Character.valueOf(s.charAt(index)).toString();
		}
		return null;
	}

	private Map<Qname, Event> getEvents(Map<Qname, AnnotationData> annotations) {
		Set<Qname> mammaliaTaxonCensusEvents = getMammaliaTaxonCensusEvents();
		ObserverIds observerIds = getObserverIds();
		Map<Qname, String> oldEventIds = getOldEventIds();
		Map<Qname, String> notes = getDocumentNotes();

		Map<Qname, Event> events = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(DOCUMENT_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Event event = buildEvent(row);
				event.mammaliaCensus = mammaliaTaxonCensusEvents.contains(event.eventId);
				event.userIds = observerIds.userIds.get(event.eventId);
				event.personIds = observerIds.personIds.get(event.eventId);
				event.lintuvaaraIds = observerIds.lintuvaaraIds.get(event.eventId);
				event.oldEventId = oldEventIds.get(event.eventId);
				event.notes = notes.get(event.eventId);
				if (annotations.containsKey(event.eventId)) {
					event.errors = annotations.get(event.eventId).getTags();
					event.errorNotes = annotations.get(event.eventId).getNotes();
				}
				events.put(event.eventId, event);
			}
		}
		return events;
	}

	private Event buildEvent(Object[] row) {
		//  SELECT    d.document_id, collection.id as collectionId, collection.name as collectionName,
		//            np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje
		//            min(g.datebegin_key) as date, sum(g.linelength) as linelength,
		//            timeStart.value as timeStart, timeEnd.value as timeEnd, aborted.value as aborted, hindered.value as hindered, gps.value as gps, breaks.value as breaks
		Event event = new Event();
		int i = 0;
		event.eventId = qname(row[i++]);
		event.collectionId = qname(row[i++]);
		event.collectionName = string(row[i++]);
		event.routeId = qname(row[i++]);
		event.routeName = string(row[i++]);
		event.municipality = string(row[i++]);
		event.ykjLat = integer(row[i++]);
		event.ykjLon = integer(row[i++]);

		Integer dateKey = integer(row[i++]);
		if (dateKey != null) {
			try {
				event.year = Integer.valueOf(dateKey.toString().substring(0, 4));
				event.month = Integer.valueOf(dateKey.toString().substring(4, 6));
				event.day = Integer.valueOf(dateKey.toString().substring(6));
			} catch (Exception e) {
				error("Invalid date " + dateKey + " for " + event.eventId);
			}
		}

		event.linelength = integer(row[i++]);

		Time startTime = time(row[i++], "Invalid star time " + event.eventId);
		Time endTime = time(row[i++], "Invalid end time " + event.eventId);
		if (startTime != null) {
			event.startHour = startTime.hour;
			event.startMinute = startTime.minute;
		}
		if (endTime != null) {
			event.endHour = endTime.hour;
			event.endMinute = endTime.minute;
		}

		event.aborted = bool(row[i++]);
		event.hindered =bool(row[i++]);
		event.gps = bool(row[i++]);
		event.breaks = iVal(string(row[i++]), "Number of breaks " + event.eventId);
		return event;
	}

	public class Event {
		@Order(1) public Qname eventId;
		@Order(2) public Qname collectionId;
		@Order(3) public String collectionName;
		@Order(4) public Qname routeId;
		@Order(5) public String oldEventId;
		@Order(6) public String municipality;
		@Order(7) public String routeName;
		@Order(8) public Integer ykjLat;
		@Order(9) public Integer ykjLon;
		@Order(10) public Integer linelength;
		@Order(11) public Integer year;
		@Order(12) public Integer month;
		@Order(13) public Integer day;
		@Order(14) public Integer startHour;
		@Order(15) public Integer startMinute;
		@Order(16) public Integer endHour;
		@Order(17) public Integer endMinute;
		@Order(18) public Boolean aborted;
		@Order(19) public Boolean hindered;
		@Order(20) public Boolean gps;
		@Order(21) public Integer breaks;
		@Order(22) public Boolean mammaliaCensus;
		@Order(23) public String errors;
		@Order(24) public String errorNotes;
		@Order(26.0) public Set<String> userIds;
		@Order(26.1) public Set<String> personIds;
		@Order(26.2) public Set<Integer> lintuvaaraIds;
		@Order(27) public String observers;
		@Order(28) public String notes;
	}

	public class BiotopeStrip {
		@Order(1) public Qname eventId;
		@Order(2) public Qname routeId;
		@Order(3) public Integer year;
		@Order(4) public Qname biotopeStripId;
		@Order(5) public Boolean counted;
		@Order(6) public Integer startMeters;
		@Order(7) public Integer endMeters;
		@Order(8) public Integer meters;
		@Order(9) public String biotope;
		@Order(10) public String biotopeSpecifier;
		@Order(11) public Integer area;
		@Order(12) public Integer height;
		@Order(13) public Integer startHour;
		@Order(14) public Integer startMinute;
		@Order(15) public Integer endHour;
		@Order(16) public Integer endMinute;
		@Order(17) public Integer stripStartHour;
		@Order(18) public Integer stripStartMinute;
		@Order(19) public Integer ykjLatMin;
		@Order(20) public Integer ykjLatMax;
		@Order(21) public Integer ykjLonMin;
		@Order(22) public Integer ykjLonMax;
		@Order(23) public String issues;
	}

	public class EventSums {
		@Order(1) public Qname eventId;
		@Order(2) public Qname routeId;
		@Order(3) public Integer year;
		@Order(4) public Integer linelength;
		@Order(5) public String scientificName;
		@Order(6) public String birdCode;
		@Order(7) public int innerCount = 0;
		@Order(8) public int outerCount = 0;
	}

	public class Observation {
		@Order(0) public Qname unitId;
		@Order(1) public Qname eventId;
		@Order(2) public Qname routeId;
		@Order(3) public Integer year;
		@Order(4) public Qname biotopeStripId;
		@Order(5) public Integer startMeters;
		@Order(6) public Integer endMeters;
		@Order(7) public Integer meters;
		@Order(8) public String biotope;
		@Order(9) public String biotopeSpecifier;
		@Order(10) public Integer area;
		@Order(11) public Integer height;
		@Order(12) public Integer startHour;
		@Order(13) public Integer startMinute;
		@Order(14) public Integer endHour;
		@Order(15) public Integer endMinute;
		@Order(16) public Integer stripStartHour;
		@Order(17) public Integer stripStartMinute;
		@Order(18) public String scientificName;
		@Order(19) public Qname taxonId;
		@Order(20) public String birdCode;
		@Order(21) public String reportedName;
		@Order(22) public String type;
		@Order(23) public Integer pairCount;
		@Order(24) public Integer individualCount;
		@Order(25) public Integer broodSize;
		@Order(26) public Boolean migrant;
		@Order(27) public String field;
		@Order(28) public String quality;
		@Order(29) public String qualityNotes;
		@Order(30) public String notes;
		@Order(31) public String issues;
	}

}
