package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class CompleteListReportGenerator extends CensusReportGenerator {
	private static final String FILENAME_PREFIX = "completeList";
	
	private static final String GATHERING_SQL = "" +
			" SELECT    g.document_id, g.gathering_id, collection.id as collection_id, collection.name as collection_name, " +
			"           g.latitudecenter, g.longitudecenter, g.euref_wkt, g.municipality_verbatim as municipality,  g.locality, " +
			"           g.year, g.month, g.day, timeStart.value as timeStart, timeEnd.value as timeEnd, g.dayofyearbegin, g.dayofyearend, " +
			"           team.team_text as team, g.secured, g.complete_list_type, g.notes" +
			" FROM      <SCHEMA>.gathering g " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" LEFT JOIN <SCHEMA>.df timeStart ON (timeStart.document_key = g.document_key AND timeStart.property = 'http://tun.fi/MY.timeStart') " +
			" LEFT JOIN <SCHEMA>.df timeEnd ON (timeEnd.document_key = g.document_key AND timeEnd.property = 'http://tun.fi/MY.timeEnd') " +
			" LEFT JOIN <DIMENSION_SCHEMA>.team ON (g.team_key = team.key) " +
			" ORDER BY datebegin_key";
	
	private static final String UNIT_SQL = "" +
			" SELECT    u.gathering_id, u.unit_id, taxon.id as taxon_id, taxon.scientific_name, taxon.name_finnish, " +
			"           u.individualcount, taxonconfidence.id as confidence, lifestage.id as lifestage, u.notes " +
			" FROM      <SCHEMA>.unit u " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (u.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN      <DIMENSION_SCHEMA>.target ON (u.target_key = target.key) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxon ON (target.taxon_key = taxon.key) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.lifestage ON (u.lifestage_key = lifestage.key) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxonconfidence ON (u.taxonconfidence_key = taxonconfidence.key) " +
			" ORDER BY datebegin_key";
	
	private static final String DOCUMENT_SECURE_REASON_SQL = "" +
			" SELECT    d.document_id, securereason.id as secureReason " +
			" FROM      <SCHEMA>.document d " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (d.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" LEFT JOIN <SCHEMA>.document_securereason AS sr ON d.key = sr.document_key " +
			" LEFT JOIN <DIMENSION_SCHEMA>.securereason ON sr.securereason_key = securereason.key " +
			"WHERE secureReason.id IS NOT NULL";
	
	public CompleteListReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, FILENAME_PREFIX, Const.COMPLETE_LIST_COLLECTION, errorReporter, dao);
	}

	protected Map<Qname, Set<String>> getDocumentSecureReasons() {
		Map<Qname, Set<String>> secureReasons = new HashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(DOCUMENT_SECURE_REASON_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname eventId = qname(row[0]);
				String reason = string(row[1]);
				
				if (!secureReasons.containsKey(eventId)) {
					secureReasons.put(eventId, new HashSet<>());
				}
				secureReasons.get(eventId).add(reason);
			}
		}
		System.out.println("Document secured reasons: " + secureReasons.size());
		return secureReasons;
	}

	@Override
	public void generateActual() {
		System.out.println(FILENAME_PREFIX +" starts");

		writeEvents();

		writeObservations();

		System.out.println(FILENAME_PREFIX + " completed");
	}

	private void writeEvents() {
		File eventFile = getFile("events");
		eventFile.write(header(Event.class));

		ObserverIds observerIds = getObserverIds();
		Map<Qname, Set<String>> secureReasons = getDocumentSecureReasons();

		try (ResultStream<Object[]> results = getCustomQuery(GATHERING_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. event line " + count);
				Object[] row = i.next();
				Event event = buildEvent(row, observerIds, secureReasons);
				eventFile.write(line(event));
			}
		}
	}

	private void writeObservations() {
		File observationFile = getFile("observations");
		observationFile.write(header(Observation.class));
		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				Observation event = buildObservation(row);
				observationFile.write(line(event));
			}
		}	
	}
	
	private Event buildEvent(Object[] row, ObserverIds observerIds, Map<Qname, Set<String>> secureReasons) {
		int i = 0;
		Event e = new Event();
		e.eventId = qname(row[i++]);
		e.gatheringId = qname(row[i++]);
		e.collectionId = qname(row[i++]);
		e.collectionName = string(row[i++]);
		e.latitudeCenter = doubleVal(row[i++]);
		e.longitudeCenter = doubleVal(row[i++]); 
		e.eurefWKT = string(row[i++]);
		e.municipality = string(row[i++]);
		e.locality = string(row[i++]);
		e.year = integer(row[i++]);
		e.month = integer(row[i++]);
		e.day = integer(row[i++]);
		Time startTime = time(row[i++], "Invalid star time " + e.eventId);
		Time endTime = time(row[i++], "Invalid end time " + e.eventId);
		if (startTime != null) {
			e.startHour = startTime.hour;
			e.startMinute = startTime.minute;
		}
		if (endTime != null) {
			e.endHour = endTime.hour;
			e.endMinute = endTime.minute;
		}
		e.dayOfYearStart = integer(row[i++]);
		e.dayOfYearEnd = integer(row[i++]);
		e.observers = string(row[i++]);
		e.userIds = observerIds.userIds.get(e.eventId);
		e.secured = bool(row[i++]);
		e.secureReasons = secureReasons.get(e.eventId);
		e.completeListType = qname(row[i++]);
		e.notes = string(row[i++]);
		return e;
	}
	
	private Observation buildObservation(Object[] row) {
		int i = 0;
		Observation obs = new Observation();
		obs.gatheringId = qname(row[i++]);
		obs.unitId = qname(row[i++]);
		obs.taxonId =  qname(row[i++]);
		obs.scientificName = string(row[i++]);
		obs.finnishName = string(row[i++]);
		obs.individualCount = integer(row[i++]);
		obs.taxonConfidence = string(row[i++]);
		obs.plantLifeStage = string(row[i++]);
		obs.notes = string(row[i++]);
		return obs;
	}

	public class Observation {
		@Order(0) public Qname gatheringId;
		@Order(1) public Qname unitId;
		@Order(2) public Qname taxonId;
		@Order(3) public String scientificName;
		@Order(4) public String finnishName;
		@Order(5) public Integer individualCount;
		@Order(6) public String taxonConfidence;
		@Order(7) public String plantLifeStage;
		@Order(8) public String notes;
	}
	
	public class Event {
		@Order(0) public Qname eventId;
		@Order(1) public Qname gatheringId;
		@Order(2) public Qname collectionId;
		@Order(3) public String collectionName;
		@Order(4) public Double latitudeCenter;
		@Order(5) public Double longitudeCenter;
		@Order(6) public String eurefWKT;
		@Order(7) public String municipality;
		@Order(8) public String locality;
		@Order(9) public Integer year;
		@Order(10) public Integer month;
		@Order(11) public Integer day;
		@Order(12) public Integer startHour;
		@Order(13) public Integer startMinute;
		@Order(14) public Integer endHour;
		@Order(15) public Integer endMinute;
		@Order(16) public Integer dayOfYearStart;
		@Order(17) public Integer dayOfYearEnd;
		@Order(18) public String observers;
		@Order(19) public Set<String> userIds;
		@Order(20) public Boolean secured;
		@Order(21) public Set<String> secureReasons;
		@Order(22) public Qname completeListType;
		@Order(23) public String notes;
	}
}

