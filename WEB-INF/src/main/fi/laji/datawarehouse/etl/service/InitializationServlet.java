package fi.laji.datawarehouse.etl.service;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.annotation.WebServlet;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.threads.ETLThread;
import fi.laji.datawarehouse.etl.threads.PullReaders;
import fi.laji.datawarehouse.etl.threads.PullReaders.PullReaderGenerator;
import fi.laji.datawarehouse.etl.threads.custom.HelsinkiPullReader;
import fi.laji.datawarehouse.etl.threads.custom.KastikkaPullReader;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader;
import fi.laji.datawarehouse.etl.threads.custom.TamperePullReader;
import fi.laji.datawarehouse.etl.threads.custom.TiiraAtlasPullReader;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.containers.rdf.Qname;

@WebServlet(name="ThreadStarterServlet", loadOnStartup=1, urlPatterns={""})
public class InitializationServlet extends ETLBaseServlet {

	private static final long serialVersionUID = 540372653608445414L;

	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);

	@Override
	protected void applicationInit() {
		registerPullReaders();

		if (getConfig().defines("NOSTART")) return;
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				getThreadStatuses().getThreadStatusReporterFor(InitializationServlet.class).setStatus("Initializing Laji-ETL...");
				getDao().logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " === LAJI-ETL STARTING UP  === ");

				initThreads();

				try {
					getDao().getVerticaDimensionsDAO(); // To prevent a deadlock (maybe not needed anymore?)
				} catch (Throwable e) {
					logError(e, "init vertica dimensions");
				}

				startDataLoads();

				try {
					startScheduledTasks();
				} catch (Throwable e) {
					logError(e, "scheduling tasks");
				}

				try {
					getDao().getETLDAO().markOutPipeErroneousForReattempt();
				} catch (Throwable e) {
					logError(e, "outpipe error 1->0");
				}

				getThreadStatuses().reportThreadDead(InitializationServlet.class);
			}

		});
		t.setName("Initialization Thread");
		t.start();
	}

	private void logError(Throwable e, String identifier) {
		getErrorReporter().report(identifier, e);
		getDao().logError(Const.LAJI_ETL_QNAME, InitializationServlet.class, identifier, e);
	}


	private void registerPullReaders() {
		try {
			PullReaders.register(KastikkaPullReader.SOURCE, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new KastikkaPullReader(getDao(), getConfig(), getErrorReporter(), getThreadHandler(), reporterFor(KastikkaPullReader.SOURCE, KastikkaPullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register kastikka pull reader");
		}

		try {
			PullReaders.register(LajiGISPullReader.SOURCE, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new LajiGISPullReader(getConfig(), getDao(), getErrorReporter(), getThreadHandler(), reporterFor(LajiGISPullReader.SOURCE, LajiGISPullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register lajigis pull reader");
		}

		try {
			Qname tiiraSystemid = TiiraAtlasPullReader.source(getConfig());
			PullReaders.register(tiiraSystemid, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new TiiraAtlasPullReader(getConfig(), getDao(), getErrorReporter(), getThreadHandler(), reporterFor(tiiraSystemid, TiiraAtlasPullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register tiira atlas pull reader");
		}

		try {
			PullReaders.register(SykeZoobenthosPullReader.SOURCE, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new SykeZoobenthosPullReader(getDao(), getErrorReporter(), getThreadHandler(), reporterFor(SykeZoobenthosPullReader.SOURCE, SykeZoobenthosPullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register zoobenthos pull reader");
		}

		try {
			Qname tampereSystemid = TamperePullReader.source(getConfig());
			PullReaders.register(tampereSystemid, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new TamperePullReader(getConfig(), getDao(), getErrorReporter(), getThreadHandler(), reporterFor(tampereSystemid, TamperePullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register tampere pull reader");
		}

		try {
			Qname helsinkiSystemid = HelsinkiPullReader.source(getConfig());
			PullReaders.register(helsinkiSystemid, new PullReaderGenerator() {
				@Override
				public ETLThread generate() {
					return new HelsinkiPullReader(getConfig(), getDao(), getErrorReporter(), getThreadHandler(), reporterFor(helsinkiSystemid, HelsinkiPullReader.class));
				}
			});
		} catch (Throwable t ) {
			logError(t, "register helsinki pull reader");
		}
	}

	private ThreadStatusReporter reporterFor(Qname source, Class<?> threadClass) {
		return getThreadStatuses().getThreadStatusReporterFor(source, threadClass);
	}

	private void startDataLoads() {
		try { getDao().getTaxon(new Qname("MX.1")); } catch (Exception e) {}
		try { getDao().getTaxonLinkingService(); } catch (Exception e) {}
		try { getDao().getPerson(new Qname("MA.1")); } catch (Exception e) {}
		try { getDao().getPersonLookupStructure(); } catch (Exception e) {}
	}

	private void startScheduledTasks() {
		try { startNightlyScheduler(); } catch (Exception e) { }
		try { startNightlyDelayedScheduler(); } catch (Exception e) { }
		try { startTenMinuteScheduler(); } catch (Exception e) { }
		try { startWeeklyScheduler(); } catch (Exception e) { }
	}

	// First minute of Monday
	private void startWeeklyScheduler() {
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		int dayOfWeek = now.get(Calendar.DAY_OF_WEEK);
		int delayInDays = DELAY_DAYS.get(dayOfWeek);
		long delayTill12am = calculateInitialDelayTill(0);
		long minutesInDay = 24*60;
		long delayMinutes = (delayInDays*minutesInDay) + delayTill12am;
		long minutesInWeek = minutesInDay * 7;
		getDao().logMessage(Const.LAJI_ETL_QNAME, InitializationServlet.class,
				"Scheduling weekly tasks to run in " + delayMinutes +  " minutes from now (" + delayInDays + " days " + delayTill12am + " minutes)");
		scheduler.scheduleAtFixedRate(new WeeklyScheduledTasks(this), delayMinutes, minutesInWeek, TimeUnit.MINUTES);
	}

	private static Map<Integer, Integer> DELAY_DAYS;
	static {
		DELAY_DAYS = new HashMap<>();
		DELAY_DAYS.put(Calendar.MONDAY, 5);
		DELAY_DAYS.put(Calendar.TUESDAY, 4);
		DELAY_DAYS.put(Calendar.WEDNESDAY, 3);
		DELAY_DAYS.put(Calendar.THURSDAY, 2);
		DELAY_DAYS.put(Calendar.FRIDAY, 1);
		DELAY_DAYS.put(Calendar.SATURDAY, 0);
		DELAY_DAYS.put(Calendar.SUNDAY, 6);
	}

	private void startTenMinuteScheduler() {
		getDao().logMessage(Const.LAJI_ETL_QNAME, InitializationServlet.class,
				"Scheduling ten minute tasks to run in " + 10 +  " minutes from now");
		scheduler.scheduleAtFixedRate(new TenMinuteTasks(this), 10, 10, TimeUnit.MINUTES);
	}

	private void startNightlyScheduler() {
		long repeatPeriod = TimeUnit.DAYS.toMinutes(1);
		long initialDelay = calculateInitialDelayTill(3);
		getDao().logMessage(Const.LAJI_ETL_QNAME, InitializationServlet.class,
				"Scheduling nightly tasks to run in " + initialDelay +  " minutes from now");
		scheduler.scheduleAtFixedRate(new NightlyScheduledTasks(this), initialDelay, repeatPeriod, TimeUnit.MINUTES);
	}

	private void startNightlyDelayedScheduler() {
		long repeatPeriod = TimeUnit.DAYS.toMinutes(1);
		long initialDelay = calculateInitialDelayTill(9);
		getDao().logMessage(Const.LAJI_ETL_QNAME, InitializationServlet.class,
				"Scheduling nightly delayed tasks to run in " + initialDelay +  " minutes from now");
		scheduler.scheduleAtFixedRate(new NightlyScheduledDelayedTasks(this), initialDelay, repeatPeriod, TimeUnit.MINUTES);
	}

	private long calculateInitialDelayTill(int hour) {
		ZonedDateTime now = ZonedDateTime.now();
		ZonedDateTime nextRun = now.withHour(hour).withMinute(0).withSecond(0);
		if (now.compareTo(nextRun) > 0)
			nextRun = nextRun.plusDays(1);

		Duration duration = Duration.between(now, nextRun);
		return duration.getSeconds() / 60;
	}

	private void initThreads() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					startReadersForSourcesWithSomethingUnprosessed(getDao());
				} catch (Exception e) {
					logError(e, "start threads with unprocessed");
				}
			}

		}).start();
	}

	// We don't have any pull APIs to read at the moment
	//	private void startPullAPIReaders(DAO dao) throws Exception {
	//		List<PullAPIData> pullAPIDatas = dao.getPullApis();
	//		for (PullAPIData pullAPIData : pullAPIDatas) {
	//			ReaderThread pullApiReader = new PullApiReader(pullAPIData, dao);
	//			getThreadHandler().startPullReader(pullApiReader, dao);
	//		}
	//	}

	void startReadersForSourcesWithSomethingUnprosessed(DAO dao) {
		Set<Qname> sources = new HashSet<>(dao.getETLDAO().getUnprosessedSources());
		sources.addAll(dao.getETLDAO().getUnprosessedSources());
		for (Qname source : sources) {
			getThreadHandler().runInPipe(source);
			getThreadHandler().runOutPipe(source);
		}
	}

	@Override
	protected void applicationDestroy() {
		System.out.println("Laji-ETL: Stopping reader threads...");
		try {
			if (scheduler != null) scheduler.shutdownNow();
		} catch (Throwable e) {}
		int i = 5;
		do {
			getThreadHandler().stopAll();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
			if (i-- < 0) {
				System.out.println("Laji-ETL: Failed to stop readers in time, forcing exit.");
				break;
			}
		} while (getThreadHandler().somethingRunning());
		System.out.println("Laji-ETL: Threads stopped!");
		super.applicationDestroy();
	}

}
