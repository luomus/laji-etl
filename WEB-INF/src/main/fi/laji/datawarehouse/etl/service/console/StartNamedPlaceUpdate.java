package fi.laji.datawarehouse.etl.service.console;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.NamedPlaceUpdater;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-named-place-update"})
public class StartNamedPlaceUpdate extends UIBaseServlet {

	private static final long serialVersionUID = -7495759069753451794L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					List<Qname> ids = new NamedPlaceUpdater(getDao()).update();
					int updated = getDao().getETLDAO().markReprocessOutPipeByNamedPlaceIds(ids);
					getDao().logMessage(Const.LAJI_ETL_QNAME, StartNamedPlaceUpdate.class, "Set " + updated + " documents to be reprocessed");
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartNamedPlaceUpdate.class, null, e);
				}
			}
		}).start();
		return ok(res);
	}

}
