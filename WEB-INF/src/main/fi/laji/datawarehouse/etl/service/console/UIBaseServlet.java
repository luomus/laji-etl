package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.User;
import fi.laji.datawarehouse.etl.service.ETLBaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.URIBuilder;

public abstract class UIBaseServlet extends ETLBaseServlet {

	private static final long serialVersionUID = -404883700747317837L;

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		URIBuilder uri = new URIBuilder(getConfig().baseURL() + "/console/login");
		String next = getNext(req);
		uri.addParameter("next", next);
		return redirectTo(uri.toString());
	}

	public static String getNext(HttpServletRequest req) {
		String servletPath = req.getServletPath();   // /servlet/MyServlet
		String pathInfo = req.getPathInfo();         // /a/b;c=123
		String queryString = req.getQueryString();   // d=789

		// Reconstruct original requesting URL
		StringBuilder url = new StringBuilder();
		url.append(servletPath);
		if (pathInfo != null) {
			url.append(pathInfo);
		}
		if (queryString != null) {
			url.append("?").append(queryString);
		}
		return url.toString();
	}

	@Override
	protected boolean authorized(HttpServletRequest req) {
		SessionHandler session = getSession(req, false);
		if (!session.hasSession()) return false;
		return session.isAuthenticatedFor(getConfig().systemId());
	}

	protected ResponseData initResponseData(HttpServletRequest req) {
		ResponseData responseData = new ResponseData().setDefaultLocale("en");
		SessionHandler session = getSession(req, false);
		if (session.hasSession() && session.isAuthenticatedFor(getConfig().systemId())) {
			responseData.setData("user", getUser(session));
			responseData.setData("flashMessage", session.getFlash());
			responseData.setData("successMessage", session.getFlashSuccess());
			responseData.setData("errorMessage", session.getFlashError());
			responseData.setData("sourceDescriptions", getDao().getSources());
		}
		return responseData;
	}

	protected User getUser(HttpServletRequest req) {
		return getUser(getSession(req));
	}

	private User getUser(SessionHandler session) {
		return new User(session.get("user_qname"), session.userName());
	}

	@Override
	protected String getId(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return null;
		}
		path = path.substring(path.lastIndexOf("/")+1);
		return path;
	}
}
