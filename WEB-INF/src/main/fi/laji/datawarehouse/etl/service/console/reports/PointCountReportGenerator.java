package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class PointCountReportGenerator extends CensusReportGenerator {

	private static final String FILENAME_PREFIX = "pistelaskenta";

	private static final String DOCUMENT_SQL = "" +
			" SELECT    d.document_id, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje, np.alternative_id as oldRouteId," +
			"	        min(g.datebegin_key) as date, " +
			"	        timeStart.value as timeStart, timeEnd.value as timeEnd, hindered.value as hindered " +
			" FROM      <SCHEMA>.document d" +
			" JOIN      <SCHEMA>.gathering g ON (g.document_key = d.key)" +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (d.namedplace_id = np.id)" +
			" LEFT JOIN <SCHEMA>.df timeStart ON (timeStart.document_key = d.key AND timeStart.property = 'http://tun.fi/MY.timeStart')" +
			" LEFT JOIN <SCHEMA>.df timeEnd ON (timeEnd.document_key = d.key AND timeEnd.property = 'http://tun.fi/MY.timeEnd')" +
			" LEFT JOIN <SCHEMA>.df hindered ON (hindered.document_key = d.key AND hindered.property = 'http://tun.fi/MY.censusHinderedByEnviromentalFactors')" +
			" WHERE     d.collection_key = (select key from <DIMENSION_SCHEMA>.collection where id = "+COLLECTIONS+") " +
			" GROUP BY  d.document_id, routeId, routeName, municipality, ykjn, ykje, oldRouteId, timeStart, timeEnd, hindered " +
			" ORDER BY  routeName, routeId, date ";

	private static final String GATHERING_SQL = "" +
			" SELECT    g.document_id, g.gathering_id, team.team_text as team,  " +
			"           gathering_order, biotope.value as biotope, ykjnmin, ykjnmax, ykjemin, ykjemax, notes, " +
			"           document_issue_message, gathering_issue_message, timeissue_message, locationissue_message " +
			" FROM      <SCHEMA>.gathering g  " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (g.namedplace_id = np.id) " +
			" LEFT JOIN <DIMENSION_SCHEMA>.team ON (g.team_key = team.key)  " +
			" LEFT JOIN <SCHEMA>.gf biotope ON (biotope.gathering_key = g.key AND biotope.property = 'http://tun.fi/MY.pointCountHabitat')  " +
			" WHERE     g.collection_key = (select key from <DIMENSION_SCHEMA>.collection where id = "+COLLECTIONS+")			  " +
			" ORDER BY  g.document_id, gathering_order";

	private static final String UNIT_SQL = "" +
			" SELECT    unit.document_id, unit.gathering_id, unit.unit_id,  " +
			"           unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,  " +
			"           pairInner.value_integer as pairInner,  pairOuter.value_integer as pairOuter,  flock.value as flock, unit_issue_message " +
			" FROM      <SCHEMA>.unit  " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (unit.namedplace_id = np.id)  " +
			" JOIN      <DIMENSION_SCHEMA>.target ON unit.target_key = target.key  " +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxon ON target.taxon_key = taxon.key  " +
			" LEFT JOIN <SCHEMA>.uf pairInner ON (pairInner.unit_key = unit.key and pairInner.property = 'http://tun.fi/MY.pairCountOuter')  " +
			" LEFT JOIN <SCHEMA>.uf pairOuter ON (pairOuter.unit_key = unit.key and pairOuter.property = 'http://tun.fi/MY.pairCountInner')  " +
			" LEFT JOIN <SCHEMA>.uf flock ON (flock.unit_key = unit.key and flock.property = 'http://tun.fi/MY.pointCountFlock')  " +
			" WHERE     unit.collection_key = (select key from <DIMENSION_SCHEMA>.collection where id = "+COLLECTIONS+")			  " +
			" ORDER BY  np.name, np.id, unit.datebegin_key, unit.document_id, unit.gathering_id, unit.unit_order";

	public PointCountReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, FILENAME_PREFIX, Const.BIRD_POINT_COUNT_COLLECTION, errorReporter, dao);
	}

	@Override
	public void generateActual() {
		System.out.println("Point count report starts");

		Map<Qname, AnnotationData> annotations = getAnnotations();

		Map<Qname, Event> events = getEvents(annotations);
		System.out.println("Events: " + events.size());

		Map<Qname, Point> points = getPointsFillObserverNames(events);
		System.out.println("Points: " + points.size());

		writeEvents(events);
		System.out.println("Event file written");

		writePoints(points);
		System.out.println("Biotope file written");

		writeEventSumsAndObservations(events, points, annotations);
		System.out.println("Observations files written");

		writeErrors();

		System.out.println("Point count report completed");
	}

	private void writePoints(Map<Qname, Point> points) {
		File pointFile = getFile("pisteet");
		pointFile.write(header(Point.class));
		for (Point point : points.values()) {
			pointFile.write(line(point));
		}
	}

	private void writeEvents(Map<Qname, Event> events) {
		File eventFile = getFile("laskennat");
		eventFile.write(header(Event.class));
		for (Event event : events.values()) {
			eventFile.write(line(event));
		}
	}

	private void writeEventSumsAndObservations(Map<Qname, Event> events, Map<Qname, Point> points, Map<Qname, AnnotationData> annotations) {
		File eventSumFile = getFile("summat");
		eventSumFile.write(header(EventSums.class));
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));

		Qname currentEventId = new Qname("");
		Map<String, EventSums> taxaSums = new LinkedHashMap<>();
		Map<Integer, List<Observation>> eventObservations = new TreeMap<>();

		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			Iterator<Object[]> i = results.iterator();
			int count = 0;
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				//			 SELECT    unit.document_id, unit.gathering_id, unit.unit_id,
				//			           unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,
				//		               pairInner.value_integer as pairInner,  pairOuter.value_integer as pairOuter,  flock.value as flock
				Qname eventId = qname(row[0]);
				Qname gatheringId = qname(row[1]);
				Event event = events.get(eventId);
				if (event == null) {
					error("Missing event: " + gatheringId + " / " + eventId);
					continue;
				}
				Point point = points.get(gatheringId);
				if (point == null) {
					error("Missing point: " + gatheringId + " / " + eventId);
					continue;
				}
				Observation observation = buildObservation(row, event, point, annotations);
				if (!currentEventId.equals(observation.eventId)) {
					writeSums(eventSumFile, taxaSums);
					writeObservations(observationsFile, eventObservations);
					taxaSums.clear();
					eventObservations.clear();
					currentEventId = observation.eventId;
				}
				addSums(observation, taxaSums, event);
				addObservation(eventObservations, point, observation);
			}
			if (!taxaSums.isEmpty()) {
				writeSums(eventSumFile, taxaSums);
			}
			if (!eventObservations.isEmpty()) {
				writeObservations(observationsFile, eventObservations);
			}
		}
	}

	private void addObservation(Map<Integer, List<Observation>> eventObservations, Point point, Observation observation) {
		if (!eventObservations.containsKey(point.pointOrder)) {
			eventObservations.put(point.pointOrder, new ArrayList<Observation>());
		}
		eventObservations.get(point.pointOrder).add(observation);
	}

	private void writeSums(File eventSumFile, Map<String, EventSums> taxaSums) {
		for (EventSums sums : taxaSums.values()) {
			eventSumFile.write(line(sums));
		}
	}

	private void writeObservations(File observationsFile, Map<Integer, List<Observation>> eventObservations) {
		for (Map.Entry<Integer, List<Observation>> e : eventObservations.entrySet()) {
			for (Observation o : e.getValue()) {
				observationsFile.write(line(o));
			}
		}
	}

	private void addSums(Observation observation, Map<String, EventSums> taxaSums, Event event) {
		String key = scientificName(observation);
		if (!taxaSums.containsKey(key)) {
			taxaSums.put(key, buildEventSums(observation, event));
		}
		if (observation.pairCountInner != null) {
			taxaSums.get(key).innerCount += observation.pairCountInner;
		}
		if (observation.pairCountOuter != null) {
			taxaSums.get(key).outerCount += observation.pairCountOuter;
		}
	}

	private String scientificName(Observation observation) {
		if (observation.scientificName !=  null) return observation.scientificName;
		return observation.reportedName;
	}

	private EventSums buildEventSums(Observation observation, Event event) {
		EventSums sums = new EventSums();
		sums.eventId = event.eventId;
		sums.routeId = event.routeId;
		sums.year = event.year;
		sums.scientificName = scientificName(observation);
		sums.birdCode = observation.birdCode;
		return sums;
	}

	private Observation buildObservation(Object[] row, Event event, Point point, Map<Qname, AnnotationData> annotations) {
		//  SELECT    unit.document_id, unit.gathering_id, unit.unit_id,
		//			  unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,
		//		      pairInner.value_integer as pairInner,  pairOuter.value_integer as pairOuter, flock.value as flock
		Observation obs = new Observation();
		obs.eventId = event.eventId;
		obs.routeId = event.routeId;
		obs.year = event.year;
		obs.gatheringId = point.gatheringId;
		obs.pointOrder = point.pointOrder;
		obs.biotope = point.biotope;
		int i = 2;
		obs.unitId = qname(row[i++]);
		obs.reportedName = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		String birdLifeCode = string(row[i++]);
		String euringCode = string(row[i++]);
		obs.birdCode = birdLifeCode != null ? birdLifeCode : euringCode;
		obs.scientificName = string(row[i++]);

		obs.pairCountInner = integer(row[i++]);
		obs.pairCountOuter = integer(row[i++]);
		obs.flock = string(row[i++]);

		if (annotations.containsKey(obs.unitId)) {
			obs.quality = annotations.get(obs.unitId).getTags();
			obs.qualityNotes = annotations.get(obs.unitId).getNotes();
		}

		obs.issues = issues(point.issues, row, i);
		return obs;
	}

	private Map<Qname, Point> getPointsFillObserverNames(Map<Qname, Event> events) {
		Map<Qname, Point> points = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(GATHERING_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Point point = buildPointFillObserverName(row, events);
				points.put(point.gatheringId, point);
			}
		}
		return points;
	}

	private Point buildPointFillObserverName(Object[] row, Map<Qname, Event> events) {
		//		SELECT    g.document_id, g.gathering_id, team.team_text as team,
		//		           gathering_order, biotope.value as biotope, ykjnmin, ykjnmax, ykjemin, ykjemax, notes
		Point point = new Point();
		int i = 0;
		point.eventId = qname(row[i++]);
		Event event = events.get(point.eventId);

		point.gatheringId = qname(row[i++]);
		point.routeId = event.routeId;

		String team = string(row[i++]);
		if (given(team)) {
			event.observers = team;
		}

		point.year = event.year;

		point.pointOrder = integer(row[i++]);
		point.biotope = string(row[i++]);
		if (point.biotope != null) point.biotope = point.biotope.replace("http://tun.fi/MY.pointCountHabitat", "");
		point.ykjLatMin = integer(row[i++]);
		point.ykjLatMax = integer(row[i++]);
		point.ykjLonMin = integer(row[i++]);
		point.ykjLonMax = integer(row[i++]);
		point.notes = string(row[i++]);
		point.issues = issues(row, i);
		return point;
	}

	private Map<Qname, Event> getEvents(Map<Qname, AnnotationData> annotations) {
		Set<Qname> mammaliaTaxonCensusEvents = getMammaliaTaxonCensusEvents();
		Map<Qname, List<String>> taxonCensusTaxonSets = getTaxonCensusTaxonSets();

		// TODO waterbird, seagul census
		ObserverIds observerIds = getObserverIds();
		Map<Qname, String> notes = getDocumentNotes();

		Map<Qname, Event> events = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(DOCUMENT_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Event event = buildEvent(row);
				if (mammaliaTaxonCensusEvents.contains(event.eventId) || taxonCensusTaxonSets.containsKey(event.eventId)) {
					List<String> census = taxonCensusTaxonSets.get(event.eventId);
					if (census == null) census = new ArrayList<>();
					if (mammaliaTaxonCensusEvents.contains(event.eventId)) {
						census.add("Mammalia");
					}
					Collections.sort(census);
					event.taxonCensus = census.toString();
				}
				event.userIds = observerIds.userIds.get(event.eventId);
				event.personIds = observerIds.personIds.get(event.eventId);
				event.lintuvaaraIds = observerIds.lintuvaaraIds.get(event.eventId);
				event.notes = notes.get(event.eventId);
				if (annotations.containsKey(event.eventId)) {
					event.quality = annotations.get(event.eventId).getTags();
					event.qualityNotes = annotations.get(event.eventId).getNotes();
				}
				events.put(event.eventId, event);
			}
		}
		return events;
	}

	private Event buildEvent(Object[] row) {
		//		SELECT  d.document_id, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, np.ykj_10km_n as ykjn, np.ykj_10km_e as ykje, np.alternative_id,
		//		        min(g.datebegin_key) as date, " +
		//		        timeStart.value as timeStart, timeEnd.value as timeEnd, hindered.value as hindered " +
		Event event = new Event();
		int i = 0;
		event.eventId = qname(row[i++]);
		event.routeId = qname(row[i++]);
		event.routeName = string(row[i++]);
		event.municipality = string(row[i++]);
		event.ykjLat = integer(row[i++]);
		event.ykjLon = integer(row[i++]);
		event.oldRouteId = string(row[i++]);

		Integer dateKey = integer(row[i++]);
		if (dateKey != null) {
			try {
				event.year = Integer.valueOf(dateKey.toString().substring(0, 4));
				event.month = Integer.valueOf(dateKey.toString().substring(4, 6));
				event.day = Integer.valueOf(dateKey.toString().substring(6));
			} catch (Exception e) {
				error("Invalid date " + dateKey + " for " + event.eventId);
			}
		}

		Time startTime = time(row[i++], "Invalid star time " + event.eventId);
		Time endTime = time(row[i++], "Invalid end time " + event.eventId);
		if (startTime != null) {
			event.startHour = startTime.hour;
			event.startMinute = startTime.minute;
		}
		if (endTime != null) {
			event.endHour = endTime.hour;
			event.endMinute = endTime.minute;
		}

		event.hindered = bool(row[i++]);
		return event;
	}

	public class Event {
		@Order(1) public Qname eventId;
		@Order(4) public Qname routeId;
		@Order(6) public String municipality;
		@Order(7.0) public String routeName;
		@Order(7.5) public String oldRouteId;
		@Order(8) public Integer ykjLat;
		@Order(9) public Integer ykjLon;
		@Order(11) public Integer year;
		@Order(12) public Integer month;
		@Order(13) public Integer day;
		@Order(14) public Integer startHour;
		@Order(15) public Integer startMinute;
		@Order(16) public Integer endHour;
		@Order(17) public Integer endMinute;
		@Order(19) public Boolean hindered;
		@Order(22) public String taxonCensus;
		@Order(25) public String quality;
		@Order(26) public String qualityNotes;
		@Order(27) public Set<String> userIds;
		@Order(28) public Set<String> personIds;
		@Order(29) public Set<Integer> lintuvaaraIds;
		@Order(30) public String observers;
		@Order(31) public String notes;
	}

	public class Point {
		@Order(1) public Qname eventId;
		@Order(2) public Qname gatheringId;
		@Order(3) public Qname routeId;
		@Order(4) public Integer year;
		@Order(5) public Integer pointOrder;
		@Order(9) public String biotope;
		@Order(19) public Integer ykjLatMin;
		@Order(20) public Integer ykjLatMax;
		@Order(21) public Integer ykjLonMin;
		@Order(22) public Integer ykjLonMax;
		@Order(30) public String notes;
		@Order(31) public String issues;
	}

	public class EventSums {
		@Order(1) public Qname eventId;
		@Order(2) public Qname routeId;
		@Order(3) public Integer year;
		@Order(4) public String scientificName;
		@Order(5) public String birdCode;
		@Order(6) public int innerCount = 0;
		@Order(7) public int outerCount = 0;
	}

	public class Observation {
		@Order(0) public Qname unitId;
		@Order(1) public Qname gatheringId;
		@Order(2) public Qname eventId;
		@Order(3) public Qname routeId;
		@Order(4) public Integer year;
		@Order(5) public Integer pointOrder;
		@Order(8) public String biotope;
		@Order(18) public String scientificName;
		@Order(19) public Qname taxonId;
		@Order(20) public String birdCode;
		@Order(21) public String reportedName;
		@Order(22) public Integer pairCountInner;
		@Order(23) public Integer pairCountOuter;
		@Order(24) public String flock;
		@Order(28) public String quality;
		@Order(29) public String qualityNotes;
		@Order(29) public String issues;
	}

}
