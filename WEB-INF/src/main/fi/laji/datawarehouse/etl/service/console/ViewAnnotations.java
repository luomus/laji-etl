package fi.laji.datawarehouse.etl.service.console;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/annotations/*"})
public class ViewAnnotations extends UIBaseServlet {

	private static final long serialVersionUID = 6133000448325515615L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData(req)
				.setViewName("annotations")
				.setData("annotations", getAnnotations());
	}

	private List<Annotation> getAnnotations() {
		return getDao().getETLDAO().getTopAnnotations(100);
	}

}
