package fi.laji.datawarehouse.etl.service.console.reports;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.service.console.UIBaseServlet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/reports/*"})
public class ReportsPage extends UIBaseServlet {

	private static final long serialVersionUID = -1031371548384715354L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData(req)
				.setViewName("reports")
				.setData("producedFiles", getProducedFiles());
	}

	private List<String> getProducedFiles() {
		List<String> files = new ArrayList<>();
		try {
			File reportFolder =  new java.io.File(getConfig().baseFolder() + getConfig().get("ReportFolder"));
			reportFolder.mkdirs();
			for (File f : reportFolder.listFiles()) {
				if (f.getName().endsWith(".zip")) {
					files.add(f.getName());
				}
			}
			Collections.reverse(files);
		} catch (Exception e) {
			getErrorReporter().report(files.toString(), e);
		}
		Collections.sort(files, Collections.reverseOrder());
		return files;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String reportName = getId(req);
		ReportGenerator reportGenerator = getGenerator(reportName);
		if (reportGenerator == null) {
			getSession(req).setFlashError("Unknown report " + reportName);
		} else {
			reportGenerator.startToGenerateInBackground();
			getSession(req).setFlashSuccess("Report " + reportName + " started. Please refresh page to see when it is ready");
		}
		return new ResponseData().setRedirectLocation(getConfig().baseURL() + "/console/reports");
	}

	private ReportGenerator getGenerator(String reportName) {
		if ("birdlinetransect".equals(reportName)) return new LineTransectReportGenerator(getConfig(), getErrorReporter(), getDao());
		if ("winterbird".equals(reportName)) return new WinterBirdReportGenerator(getConfig(), getErrorReporter(), getDao());
		if ("birdpointcount".equals(reportName)) return new PointCountReportGenerator(getConfig(), getErrorReporter(), getDao());
		if ("waterbird".equals(reportName)) return new WaterBirdReportGenerator(getConfig(), getErrorReporter(), getDao());
		if ("sykebutterfly".equals(reportName))
			return new SykeSchemeReportGenerator(getConfig(), getErrorReporter(), getDao(), Utils.set(new Qname("HR.3431"), new Qname("HR.4131")), "syke_butterfly");
		if ("sykebumblebee".equals(reportName))
			return new SykeSchemeReportGenerator(getConfig(), getErrorReporter(), getDao(), Utils.set(new Qname("HR.3911")), "syke_bumblebee");
		if ("sykepöly".equals(reportName))
			return new SykeSchemeReportGenerator(getConfig(), getErrorReporter(), getDao(), Utils.set(new Qname("HR.4612")), "syke_pöly");
		if ("springmonitoring".equals(reportName)) return new SpringMonitoringReportGenerator(null, getConfig(), getErrorReporter(), getDao());
		if ("springmonitoring_currentyear".equals(reportName)) return new SpringMonitoringReportGenerator(DateUtils.getCurrentYear(), getConfig(), getErrorReporter(), getDao());
		if ("invasiveprevention".equals(reportName)) return new InvasivePreventionReportGenerator(getConfig(), getErrorReporter(), getDao());
		if ("completelist".equals(reportName)) return new CompleteListReportGenerator(getConfig(), getErrorReporter(), getDao());
		return null;
	}

}
