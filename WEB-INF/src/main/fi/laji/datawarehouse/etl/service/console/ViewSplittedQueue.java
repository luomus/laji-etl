package fi.laji.datawarehouse.etl.service.console;

import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/splitted-queue/*"})
public class ViewSplittedQueue extends UIBaseServlet {

	private static final long serialVersionUID = 6224239997092477911L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return initResponseData(req)
				.setViewName("splitted")
				.setData("splitted", getSplitted());
	}

	private List<QueueData> getSplitted() {
		return getDao().getETLDAO().getTopSplittedDocumentsFromQueue(100);
	}

}
