package fi.laji.datawarehouse.etl.service.console;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.URIBuilder;

@WebServlet(urlPatterns = {"/console/public/view", "/console/private/view"})
public class ViewDocument extends UIBaseServlet {

	private static final long serialVersionUID = -2367987879084810172L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("document");
		responseData.setData("collections", getDao().getCollections());
		responseData.setData("areas", getAreaDescriptions());

		Qname documentId = parseDocumentId(req);
		Concealment concealment = parseConcealment(req);

		OutPipeData outPipeData = getDao().getETLDAO().getFromOutPipe(documentId);
		Document document = getVerticaDao(concealment).getQueryDAO().get(documentId);
		if (document != null) {
			InPipeData inPipeData = outPipeData == null ? null : getDao().getETLDAO().getFromInPipe(outPipeData.getInPipeId());
			return responseData
					.setData("concealment", concealment.toString())
					.setData("document", document)
					.setData("outPipeEntry", outPipeData)
					.setData("inPipeEntry", inPipeData)
					.setData("json", ModelToJson.toJson(document));
		}
		if (outPipeData == null) {
			return responseData;
		}
		URIBuilder uri = new URIBuilder(getConfig().baseURL()+"/console/out/entry/" + outPipeData.getId()).addParameter("missingFrom", concealment.toString());
		return responseData.setRedirectLocation(uri.toString());
	}

	private Map<String, String> getAreaDescriptions() {
		return Util.getAreaDescriptions(getDao().getAreas().values(), false);
	}

	private VerticaDAO getVerticaDao(Concealment concealment) {
		if (concealment == Concealment.PUBLIC) return getDao().getPublicVerticaDAO();
		return getDao().getPrivateVerticaDAO();
	}

	private Concealment parseConcealment(HttpServletRequest req) {
		if (req.getRequestURI().contains("/public/")) return Concealment.PUBLIC;
		return Concealment.PRIVATE;
	}

	private Qname parseDocumentId(HttpServletRequest req) {
		String documentId = req.getParameter("documentId").trim();
		if (documentId.startsWith("http:")) {
			return Qname.fromURI(documentId);
		}
		return new Qname(documentId);
	}

}
