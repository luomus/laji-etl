package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.threads.custom.HelsinkiPullReader;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/helsinki-sync"})
public class StartHelsinkiSync extends UIBaseServlet {

	private static final long serialVersionUID = 8857540926310671288L;
												 

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		getThreadHandler().runPullReader(HelsinkiPullReader.source(getConfig()));
		return ok(res);
	}

}
