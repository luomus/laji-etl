package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.threads.custom.TamperePullReader;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/tampere-sync"})
public class StartTampereSync extends UIBaseServlet {

	private static final long serialVersionUID = 3795420075926762617L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		getThreadHandler().runPullReader(TamperePullReader.source(getConfig()));
		return ok(res);
	}

}
