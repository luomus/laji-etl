package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/start-linking-reprocess"})
public class StartTaxonPersonLinkingReprocessing extends UIBaseServlet {

	private static final long serialVersionUID = 7435351966264216628L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					getDao().getVerticaDimensionsDAO().startTaxonAndPersonReprosessing();
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartTaxonPersonLinkingReprocessing.class, null, e);
				}
			}
		}).start();
		return ok(res);
	}

}
