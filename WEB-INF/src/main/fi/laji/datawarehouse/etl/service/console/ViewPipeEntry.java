package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;

@WebServlet(urlPatterns = {"/console/in/entry/*", "/console/out/entry/*"})
public class ViewPipeEntry extends UIBaseServlet {

	private static final long serialVersionUID = -8419379102466872956L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("entry");
		int id = Integer.valueOf(getId(req));

		ETLDAO dao = getDao().getETLDAO();
		if (req.getRequestURI().contains("/in/")) {
			responseData.setData("inPipeEntry", dao.getFromInPipe(id));
		} else {
			OutPipeData outPipeData = dao.getFromOutPipe(id);
			if (outPipeData != null) {
				InPipeData inPipeData = dao.getFromInPipe(outPipeData.getInPipeId());
				responseData.setData("outPipeEntry", outPipeData);
				responseData.setData("inPipeEntry", inPipeData);
				responseData.setData("annotations", dao.getAnnotations(Utils.set(outPipeData.getDocumentId())).get(outPipeData.getDocumentId()));
			}
		}

		responseData.setData("missingFrom", req.getParameter("missingFrom"));
		return responseData;
	}

}
