package fi.laji.datawarehouse.etl.service.console;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.ETLDAO.PipeSearchParams;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/in/*", "/console/out/*"})
public class ViewSource extends UIBaseServlet {

	private static final long serialVersionUID = -8419379102466872956L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		ResponseData responseData = initResponseData(req).setViewName("source");

		Qname source = new Qname(getId(req));
		if (!given(source)) return redirectTo(getConfig().baseURL());

		responseData.setData("source", source);
		PipeSearchParams searchParams = getSearchType(req);
		if (req.getRequestURI().contains("/in/")) {
			responseData.setData("inPipeEntries", getDao().getETLDAO().getFromInPipeForSource(source, searchParams));
		} else {
			responseData.setData("outPipeEntries", getDao().getETLDAO().getFromOutPipeForSource(source, searchParams));
		}
		return responseData;
	}

	private PipeSearchParams getSearchType(HttpServletRequest req) {
		String uri = req.getRequestURI();
		if (uri.contains("/received/")) {
			return ETLDAO.PipeSearchParams.RECEIVED;
		} else if (uri.contains("/processed/")) {
			return ETLDAO.PipeSearchParams.PROCESSED;
		} else if (uri.contains("/waiting/")) {
			return ETLDAO.PipeSearchParams.WAITING;
		} else if (uri.contains("/error/")) {
			return ETLDAO.PipeSearchParams.IN_ERROR;
		} else if (uri.contains("/deleted/")) {
			return ETLDAO.PipeSearchParams.DELETED;
		}
		throw new UnsupportedOperationException("Unknown path " + uri);
	}

}
