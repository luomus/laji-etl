package fi.laji.datawarehouse.etl.service.console;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.VerticaQueryDAO.VerticaCustomQueriesDAO;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/console/documentid-sync"})
public class StartDocumentIdSyncScript extends UIBaseServlet {

	private static final long serialVersionUID = -9118496662033772978L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					doit();
				} catch (Exception e) {
					getDao().logError(Const.LAJI_ETL_QNAME, StartDocumentIdSyncScript.class, null, e);
				}
			}

		}).start();

		return ok(res);
	}

	private void doit() {
		try {
			DAO dao = getDao();
			log(StartDocumentIdSyncScript.class.getSimpleName() + " started!", dao);

			log(" ... from out_pipe to Vertica", dao);
			try (ResultStream<String> res = dao.getETLDAO().getAllDocumentIds()) {
				dao.getVerticaDimensionsDAO().storeDocumentIds(res.iterator());
			}
			log(" ... from spitted documents to Vertica", dao);
			dao.getVerticaDimensionsDAO().storeSplittedDocumentIds(dao.getETLDAO().getAllSplittedIdToOriginalId().values());

			VerticaCustomQueriesDAO privateDao = dao.getPrivateVerticaDAO().getQueryDAO().getCustomQueries();
			VerticaCustomQueriesDAO publicDao = dao.getPublicVerticaDAO().getQueryDAO().getCustomQueries();

			dao.getETLDAO().clearReprocessDocumentIds();

			log(" ... from Vertica private to reprocess_temp", dao);
			try (ResultStream<String> res = privateDao.getAllDocumentIds()) {
				dao.getETLDAO().storeReprocessDocumentIds(res);
			}

			log(" ... from Vertica public to reprocess_temp", dao);
			try (ResultStream<String> res = publicDao.getAllDocumentIds()) {
				Stream<String> filtered = StreamSupport.stream(res.spliterator(), false).filter(documentId->!looksLikeSplitted(documentId));
				Iterable<String> iterable = filtered::iterator;
				dao.getETLDAO().storeReprocessDocumentIds(iterable);
			}

			log("Exiting", dao);
		} finally {
			getThreadStatuses().reportThreadDead(StartDocumentIdSyncScript.class);
		}
	}

	private boolean looksLikeSplitted(String documentId) {
		return documentId.startsWith("http://tun.fi/A.");
	}

	private void log(String message, DAO dao) {
		dao.logMessage(Const.LAJI_ETL_QNAME, StartDocumentIdSyncScript.class, message);
		status(message);
	}

	private void status(String message) {
		getThreadStatuses().getThreadStatusReporterFor(StartDocumentIdSyncScript.class).setStatus(message);
	}

}
