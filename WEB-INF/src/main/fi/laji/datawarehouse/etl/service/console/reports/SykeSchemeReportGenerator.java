package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class SykeSchemeReportGenerator extends CensusReportGenerator {

	private static final String DOCUMENT_SQL = "" +
			" SELECT " +
			" 	collection.id as collection, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, " +
			" 	g.document_id, " +
			" 	min(g.datebegin_key) as date, " +
			" 	nounits.value as nounits, " +
			" 	min(g.ykjnmin) ykjnmin, min(g.ykjnmax) ykjnmax, min(g.ykjemin) ykjemin, min(g.ykjemax) ykjemax " +
			" FROM 		<SCHEMA>.gathering g " +
			" JOIN 		<DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN 		<DIMENSION_SCHEMA>.namedplace np ON (g.namedplace_id = np.id) " +
			" LEFT JOIN	<SCHEMA>.df nounits ON (nounits.document_key = g.document_key AND nounits.property = 'http://tun.fi/MY.acknowledgeNoUnitsInCensus') " +
			" GROUP BY collection, routeId, routeName, municipality, g.document_id, nounits " +
			" ORDER BY	routeId, date";

	private static final String UNIT_SQL = "" +
			" SELECT " +
			" 	collection.id as collection, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, " +
			" 	u.document_id, " +
			" 	u.datebegin_key as date, timeStart.value as timeStart, timeEnd.value as timeEnd, " +
			" 	nounits.value as nounits, temps.value as temps, tempe.value as tempe, clouds.value as clouds, cloude.value as cloude, winds.value as winds, winde.value as winde, " +
			" 	u.gathering_id, section.value as section, " +
			" 	u.ykjnmin, u.ykjnmax, u.ykjemin, u.ykjemax, u.euref_wkt, " +
			" 	sunniness.value as sunniness, wind.value as wind, " +
			" 	u.unit_id, u.unit_order as uorder, u.taxon_verbatim, taxon.key, taxon.scientific_name, u.individualcount, " +
			" 	document_issue_message, gathering_issue_message, timeissue_message, locationissue_message, unit_issue_message " +
			" FROM 		<SCHEMA>.unit u " +
			" JOIN 		<DIMENSION_SCHEMA>.collection ON (u.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+")) " +
			" JOIN 		<DIMENSION_SCHEMA>.namedplace np ON (u.namedplace_id = np.id) " +
			" JOIN 		<DIMENSION_SCHEMA>.target ON (u.target_key = target.key) " +
			" LEFT JOIN	<DIMENSION_SCHEMA>.taxon ON (target.taxon_key = taxon.key) " +
			" JOIN		<SCHEMA>.df timeStart ON (timeStart.document_key = u.document_key AND timeStart.property = 'http://tun.fi/MY.timeStart') " +
			" JOIN		<SCHEMA>.df timeEnd ON (timeEnd.document_key = u.document_key AND timeEnd.property = 'http://tun.fi/MY.timeEnd') " +
			" LEFT JOIN	<SCHEMA>.df nounits ON (nounits.document_key = u.document_key AND nounits.property = 'http://tun.fi/MY.acknowledgeNoUnitsInCensus') " +
			" LEFT JOIN	<SCHEMA>.df temps ON (temps.document_key = u.document_key AND temps.property = 'http://tun.fi/MY.temperatureStart') " +
			" LEFT JOIN	<SCHEMA>.df tempe ON (tempe.document_key = u.document_key AND tempe.property = 'http://tun.fi/MY.temperatureEnd') " +
			" LEFT JOIN	<SCHEMA>.df clouds ON (clouds.document_key = u.document_key AND clouds.property = 'http://tun.fi/MY.cloudCoverStart') " +
			" LEFT JOIN	<SCHEMA>.df cloude ON (cloude.document_key = u.document_key AND cloude.property = 'http://tun.fi/MY.cloudCoverEnd') " +
			" LEFT JOIN	<SCHEMA>.df winds ON (winds.document_key = u.document_key AND winds.property = 'http://tun.fi/MY.sykeButterFlyCensusWindStart') " +
			" LEFT JOIN	<SCHEMA>.df winde ON (winde.document_key = u.document_key AND winde.property = 'http://tun.fi/MY.sykeButterFlyCensusWindEnd') " +
			" LEFT JOIN	<SCHEMA>.gf section ON (section.gathering_key = u.gathering_key AND section.property = 'http://tun.fi/MY.section') " +
			" LEFT JOIN	<SCHEMA>.gf sunniness ON (sunniness.gathering_key = u.gathering_key AND sunniness.property = 'http://tun.fi/MY.sunniness') " +
			" LEFT JOIN	<SCHEMA>.gf wind ON (wind.gathering_key = u.gathering_key AND wind.property = 'http://tun.fi/MY.sykeButterFlyCensusWind') " +
			" ORDER BY	routeId, date, section, uorder";

	private final String reportName;

	public SykeSchemeReportGenerator(Config config, ErrorReporter errorReporter, DAO dao, Set<Qname> collections, String reportName) {
		super(config, reportName, collections, errorReporter, dao);
		this.reportName = reportName;
	}

	@Override
	public void generateActual() {
		System.out.println(reportName +" starts");

		Map<Qname, AnnotationData> annotations = getAnnotations();

		writeEvents();

		writeEventSumsAndObservations(annotations);

		writeErrors();

		System.out.println(reportName + " completed");
	}

	private void writeEvents() {
		File eventFile = getFile("laskennat");
		eventFile.write(header(Event.class));

		try (ResultStream<Object[]> results = getCustomQuery(DOCUMENT_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. event line " + count);
				Object[] row = i.next();
				Event event = buildEvent(row);
				eventFile.write(line(event));
			}
		}
	}

	private void writeEventSumsAndObservations(Map<Qname, AnnotationData> annotations) {
		File eventSumFile = getFile("summat");
		eventSumFile.write(header(EventSums.class));
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));

		Qname currentEventId = new Qname("");
		Map<String, EventSums> taxaSums = new LinkedHashMap<>();

		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				Observation observation = buildObservation(row, annotations);
				if (!currentEventId.equals(observation.eventId)) {
					writeSums(eventSumFile, taxaSums);
					taxaSums.clear();
					currentEventId = observation.eventId;
				}
				addSums(observation, taxaSums);
				observationsFile.write(line(observation));
			}
		}
		if (!taxaSums.isEmpty()) {
			writeSums(eventSumFile, taxaSums);
		}
	}

	private void addSums(Observation observation, Map<String, EventSums> taxaSums) {
		if (observation.count == null || observation.count < 1) return;
		String taxonId = taxonId(observation);
		if (!taxaSums.containsKey(taxonId)) {
			taxaSums.put(taxonId, buildEventSums(taxonId, observation));
		}
		EventSums sums = taxaSums.get(taxonId);
		sums.count += 1;
		sums.individualCount += observation.count;
	}

	private EventSums buildEventSums(String taxonId, Observation observation) {
		EventSums sums = new EventSums();
		sums.collection = observation.collection;
		sums.eventId = observation.eventId;
		sums.routeId = observation.routeId;
		sums.routeNumber = observation.routeNumber;
		sums.routename = observation.routeName;
		sums.date = observation.date;
		sums.day = observation.day;
		sums.month = observation.month;
		sums.year = observation.year;
		sums.taxon = taxonId;
		sums.count = 0;
		sums.individualCount = 0;
		return sums;
	}

	private String taxonId(Observation observation) {
		if (observation.scientificName !=  null) return observation.scientificName;
		return observation.reportedName;
	}

	private Event buildEvent(Object[] row) {
		int i = 0;
		Event e = new Event();
		//			" 	collection.id as collection, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, " +
		e.collection = collection(qname(row[i++]));
		e.routeId = qname(row[i++]);
		e.routeName = string(row[i++]);
		e.routeNumber = parseRouteNumber(e.routeName);
		e.municipality = string(row[i++]);
		//			" 	document_id, " +
		e.eventId = qname(row[i++]);
		//			" 	u.datebegin_key as date, timeStart.value as timeStart, timeEnd.value as timeEnd, " +
		Integer dateKey = integer(row[i++]);
		if (dateKey != null) {
			e.date = dateKey;
			try {
				e.year = Integer.valueOf(dateKey.toString().substring(0, 4));
				e.month = Integer.valueOf(dateKey.toString().substring(4, 6));
				e.day = Integer.valueOf(dateKey.toString().substring(6));
			} catch (Exception ex) {
				error("Invalid date " + dateKey + " for " + e.eventId);
			}
		}
		//			" 	nounits.value as nounits
		e.nothingFound = bool(row[i++]);
		//			" 	u.ykjnmin, u.ykjnmax, u.ykjemin, u.ykjemax, sunniness.value as sunniness, wind.value as wind, " +
		e.ykjLatMin = integer(row[i++]);
		e.ykjLatMax = integer(row[i++]);
		e.ykjLonMin = integer(row[i++]);
		e.ykjLonMax = integer(row[i++]);
		return e;
	}

	private String collection(Qname qname) {
		if ("HR.3911".equals(qname.toString())) return "kimalais";
		if ("HR.3431".equals(qname.toString())) return "uusi";
		if ("HR.4131".equals(qname.toString())) return "vanha";
		if ("HR.4612".equals(qname.toString())) return "pöly";
		return "tuntematon";
	}

	private Observation buildObservation(Object[] row, Map<Qname, AnnotationData> annotations) {
		int i = 0;
		Observation obs = new Observation();
		//			" 	collection.id as collection, np.id as routeId, np.name as routeName, np.municipality_displayname as municipality, " +
		obs.collection = collection(qname(row[i++]));
		obs.routeId = qname(row[i++]);
		obs.routeName = string(row[i++]);
		obs.routeNumber = parseRouteNumber(obs.routeName);
		obs.municipality = string(row[i++]);
		//			" 	document_id, " +
		obs.eventId = qname(row[i++]);
		//			" 	u.datebegin_key as date, timeStart.value as timeStart, timeEnd.value as timeEnd, " +
		Integer dateKey = integer(row[i++]);
		if (dateKey != null) {
			obs.date = dateKey;
			try {
				obs.year = Integer.valueOf(dateKey.toString().substring(0, 4));
				obs.month = Integer.valueOf(dateKey.toString().substring(4, 6));
				obs.day = Integer.valueOf(dateKey.toString().substring(6));
			} catch (Exception e) {
				error("Invalid date " + dateKey + " for " + obs.eventId);
			}
		}
		Time startTime = time(row[i++], "Invalid star time " + obs.eventId);
		Time endTime = time(row[i++], "Invalid end time " + obs.eventId);
		if (startTime != null) {
			obs.startHour = startTime.hour;
			obs.startMinute = startTime.minute;
		}
		if (endTime != null) {
			obs.endHour = endTime.hour;
			obs.endMinute = endTime.minute;
		}
		//			" 	nounits.value as nounits, temps.value as temps, tempe.value as tempe, clouds.value as clouds, cloude.value as cloude, winds.value as winds, winde.value as winde, " +
		obs.nothingFound = bool(row[i++]);
		obs.tempStart = doubleVal(row[i++]);
		obs.tempEnd = doubleVal(row[i++]);
		obs.cloudStart = string(row[i++]);
		if (obs.cloudStart != null) obs.cloudStart = obs.cloudStart.replace("http://tun.fi/MY.cloudCoverOktaEnum", "");
		obs.cloudEnd = string(row[i++]);
		if (obs.cloudEnd != null) obs.cloudEnd = obs.cloudEnd.replace("http://tun.fi/MY.cloudCoverOktaEnum", "");
		obs.windStart = string(row[i++]);
		if (obs.windStart != null) obs.windStart = obs.windStart.replace("http://tun.fi/MY.sykeButterFlyCensusWindEnum", "");
		obs.windEnd = string(row[i++]);
		if (obs.windEnd != null) obs.windEnd = obs.windEnd.replace("http://tun.fi/MY.sykeButterFlyCensusWindEnum", "");
		//			" 	gathering_id, section.value as section, " +
		obs.section_id = qname(row[i++]);
		obs.section_number = integer(row[i++]);
		//			" 	u.ykjnmin, u.ykjnmax, u.ykjemin, u.ykjemax, u.euref_wkt,
		obs.section_ykjLatMin = integer(row[i++]);
		obs.section_ykjLatMax = integer(row[i++]);
		obs.section_ykjLonMin = integer(row[i++]);
		obs.section_ykjLonMax = integer(row[i++]);
		obs.eurefWKT = string(row[i++]);
		//			" 	sunniness.value as sunniness, wind.value as wind, " +
		obs.section_sun = integer(row[i++]);
		obs.section_wind = string(row[i++]);
		if (obs.section_wind != null) obs.section_wind = obs.section_wind.replace("http://tun.fi/MY.sykeButterFlyCensusWindEnum", "");
		//			" 	u.unit_id, u.unit_order as uorder, u.taxon_verbatim, taxon.key, taxon.scientific_name, u.individualcount " +
		obs.obs_id = qname(row[i++]);
		obs.obs_order = integer(row[i++]);
		obs.reportedName = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		obs.scientificName = string(row[i++]);
		obs.count = integer(row[i++]);
		if (annotations.containsKey(obs.obs_id)) {
			obs.quality = annotations.get(obs.obs_id).getTags();
			obs.qualityNotes = annotations.get(obs.obs_id).getNotes();
		}
		obs.issues = issues(row, i);
		return obs;
	}

	private Integer parseRouteNumber(String routeName) {
		// 29, Turku, Moisio
		if (routeName == null) return null;
		try {
			String part = routeName.split(Pattern.quote(","))[0];
			return Integer.valueOf(part.trim());
		} catch (Exception e) {
			return null;
		}
	}

	private void writeSums(File eventSumFile, Map<String, EventSums> taxaSums) {
		for (EventSums sums : taxaSums.values()) {
			eventSumFile.write(line(sums));
		}
	}

	public class EventSums {
		@Order(0) public String collection;
		@Order(1) public Qname routeId;
		@Order(2) public Integer routeNumber;
		@Order(3) public String routename;
		@Order(4) public Qname eventId;
		@Order(5) public Integer date;
		@Order(6) public Integer year;
		@Order(7) public Integer month;
		@Order(8) public Integer day;
		@Order(9) public String taxon;
		@Order(10) public int count = 0;
		@Order(11) public int individualCount = 0;
	}

	public class Observation {
		@Order(0) public String collection;
		@Order(0.1) public Qname routeId;
		@Order(0.2) public Integer routeNumber;
		@Order(1) public String routeName;
		@Order(2) public String municipality;

		@Order(5) public Qname eventId;
		@Order(6) public Integer date;
		@Order(7) public Integer year;
		@Order(8) public Integer month;
		@Order(9) public Integer day;
		@Order(10) public Integer startHour;
		@Order(11) public Integer startMinute;
		@Order(12) public Integer endHour;
		@Order(13) public Integer endMinute;

		@Order(14) public Boolean nothingFound;
		@Order(15) public Double tempStart;
		@Order(16) public Double tempEnd;
		@Order(17) public String cloudStart;
		@Order(18) public String cloudEnd;
		@Order(19) public String windStart;
		@Order(20) public String windEnd;

		@Order(21) public Qname section_id;
		@Order(22) public Integer section_number;

		@Order(23.1) public Integer section_ykjLatMin;
		@Order(23.2) public Integer section_ykjLatMax;
		@Order(23.3) public Integer section_ykjLonMin;
		@Order(23.4) public Integer section_ykjLonMax;
		@Order(24) public String eurefWKT;
		@Order(27) public Integer section_sun;
		@Order(28) public String section_wind;

		@Order(29) public Qname obs_id;
		@Order(30) public Integer obs_order;
		@Order(31) public String reportedName;
		@Order(32) public Qname taxonId;
		@Order(33) public String scientificName;
		@Order(34) public Integer count;

		@Order(35) public String quality;
		@Order(36) public String qualityNotes;

		@Order(37) public String issues;
	}

	public class Event {
		@Order(0) public String collection;
		@Order(0.1) public Qname routeId;
		@Order(0.2) public Integer routeNumber;
		@Order(1) public String routeName;
		@Order(2) public String municipality;

		@Order(5) public Qname eventId;
		@Order(6) public Integer date;
		@Order(7) public Integer year;
		@Order(8) public Integer month;
		@Order(9) public Integer day;

		@Order(14) public Boolean nothingFound;

		@Order(23) public Integer ykjLatMin;
		@Order(24) public Integer ykjLatMax;
		@Order(25) public Integer ykjLonMin;
		@Order(26) public Integer ykjLonMax;
	}

}

