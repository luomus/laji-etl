package fi.laji.datawarehouse.etl.service.console.reports;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class WaterBirdReportGenerator extends CensusReportGenerator {


	private static final String FILENAME_PREFIX = "vesilintu";

	private static final String GATHERING_SQL = "" +
			" SELECT    g.document_id, g.gathering_id, collection.id as colId, f.id as formId, np.id as routeId, np.name as routeName, np.alternative_ids as oldRouteIds, " +
			"           np.municipality_displayname as municipality, g.ykj_10km_n_center as ykjn, g.ykj_10km_e_center as ykje, " +
			"           g.eurefnmin, g.eurefnmax, g.eurefemin, g.eurefemax, g.euref_wkt, " +
			"           g.year, g.month, g.day, g.hourbegin, g.hourend, team.team_text as team, g.notes, " +
			"           f1.value as gtype, f2.value as hindered, f3.value as wind, f4.value as rain, f5.value as fog, " +
			"           f6.value as skipped, f7.value as nounits, f8.value as binoculars, f9.value as skope,  " +
			"           f10.value as wayoftravel, f11.value as habitat, f12.value_integer as shoreline, f13.value_integer as area, f14.value as period, " +
			"           document_issue_message, gathering_issue_message, timeissue_message, locationissue_message " +
			" FROM      <SCHEMA>.gathering g  " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (g.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+") ) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (g.namedplace_id = np.id) " +
			" JOIN      <DIMENSION_SCHEMA>.form f ON f.key = g.form_key " +
			" LEFT JOIN <SCHEMA>.gf f1 ON g.key = f1.gathering_key AND f1.property = 'http://tun.fi/MY.gatheringType' " +
			" LEFT JOIN <SCHEMA>.gf f2 ON g.key = f2.gathering_key AND f2.property = 'http://tun.fi/MY.censusHinderedByEnviromentalFactors' " +
			" LEFT JOIN <SCHEMA>.gf f3 ON g.key = f3.gathering_key AND f3.property = 'http://tun.fi/MY.censusHinderedByWind' " +
			" LEFT JOIN <SCHEMA>.gf f4 ON g.key = f4.gathering_key AND f4.property = 'http://tun.fi/MY.censusHinderedByRain' " +
			" LEFT JOIN <SCHEMA>.gf f5 ON g.key = f5.gathering_key AND f5.property = 'http://tun.fi/MY.censusHinderedByFog' " +
			" LEFT JOIN <SCHEMA>.gf f6 ON g.key = f6.gathering_key AND f6.property = 'http://tun.fi/MY.skipped' " +
			" LEFT JOIN <SCHEMA>.gf f7 ON g.key = f7.gathering_key AND f7.property = 'http://tun.fi/MY.acknowledgeNoUnitsInCensus' " +
			" LEFT JOIN <SCHEMA>.gf f8 ON g.key = f8.gathering_key AND f8.property = 'http://tun.fi/MY.binocularsUsed' " +
			" LEFT JOIN <SCHEMA>.gf f9 ON g.key = f9.gathering_key AND f9.property = 'http://tun.fi/MY.spottingScopeUsed' " +
			" LEFT JOIN <SCHEMA>.gf f10 ON g.key = f10.gathering_key AND f10.property = 'http://tun.fi/WBC.wayOfTravel' " +
			" LEFT JOIN <SCHEMA>.gf f11 ON g.key = f11.gathering_key AND f11.property = 'http://tun.fi/MY.waterbirdHabitat' " +
			" LEFT JOIN <SCHEMA>.gf f12 ON g.key = f12.gathering_key AND f12.property = 'http://tun.fi/MY.shorelineLengthMeters' " +
			" LEFT JOIN <SCHEMA>.gf f13 ON g.key = f13.gathering_key AND f13.property = 'http://tun.fi/MY.censusAreaHectares' " +
			" LEFT JOIN <SCHEMA>.df f14 ON g.document_key = f14.document_key AND f14.property = 'http://tun.fi/MY.waterbirdCensusPeriod' " +
			" LEFT JOIN <DIMENSION_SCHEMA>.team ON (g.team_key = team.key)  " +
			" ORDER BY  routeName, routeId, datebegin_key ";

	private static final String UNIT_SQL = "" +
			" SELECT unit.document_id, unit.unit_id, " +
			"	       unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name, " +
			"	       unit.abundancestring, sex.id, unit.notes," +
			"	       f1.value as female, f2.value_integer as juvInd, f3.value as juvAcc, f4.value as juvAge, f5.value as use, " +
			"	       f6.value_integer as pairCount, f7.value_integer as pairCountOpinion, f8.value as opinionReasoning, f9.value as shortHand, " +
			"	       unit_issue_message " +
			" FROM  <SCHEMA>.unit " +
			" JOIN      <DIMENSION_SCHEMA>.collection ON (unit.collection_key = collection.key AND collection.id IN ("+COLLECTIONS+") ) " +
			" JOIN      <DIMENSION_SCHEMA>.namedplace np ON (unit.namedplace_id = np.id) " +
			" JOIN      <DIMENSION_SCHEMA>.target ON unit.target_key = target.key " +
			" LEFT JOIN <DIMENSION_SCHEMA>.sex ON unit.sex_key = sex.key" +
			" LEFT JOIN <DIMENSION_SCHEMA>.taxon ON target.taxon_key = taxon.key " +
			" LEFT JOIN <SCHEMA>.uf f1 ON unit.key = f1.unit_key AND f1.property = 'http://tun.fi/MY.waterbirdFemale' " +
			" LEFT JOIN <SCHEMA>.uf f2 ON unit.key = f2.unit_key AND f2.property = 'http://tun.fi/MY.juvenileIndividualCount' " +
			" LEFT JOIN <SCHEMA>.uf f3 ON unit.key = f3.unit_key AND f3.property = 'http://tun.fi/MY.waterbirdJuvenileCountAccurate' " +
			" LEFT JOIN <SCHEMA>.uf f4 ON unit.key = f4.unit_key AND f4.property = 'http://tun.fi/MY.waterbirdJuvenileAgeClass' " +
			" LEFT JOIN <SCHEMA>.uf f5 ON unit.key = f5.unit_key AND f5.property = 'http://tun.fi/MY.waterbirdObserverOpinionSelectedCensus' " +
			" LEFT JOIN <SCHEMA>.uf f6 ON unit.key = f6.unit_key AND f6.property = 'http://tun.fi/MY.pairCount' " +
			" LEFT JOIN <SCHEMA>.uf f7 ON unit.key = f7.unit_key AND f7.property = 'http://tun.fi/MY.pairCountOpinion' " +
			" LEFT JOIN <SCHEMA>.uf f8 ON unit.key = f8.unit_key AND f8.property = 'http://tun.fi/MY.waterbirdPairCountOpinionReasoning' " +
			" LEFT JOIN <SCHEMA>.uf f9 ON unit.key = f9.unit_key AND f9.property = 'http://tun.fi/MY.shortHandText' " +
			" ORDER BY  np.name, np.id, unit.datebegin_key, unit.unit_order ";

	public WaterBirdReportGenerator(Config config, ErrorReporter errorReporter, DAO dao) {
		super(config, FILENAME_PREFIX, Const.WATERBIRD_COLLECTION, errorReporter, dao);
	}

	@Override
	public void generateActual() {
		System.out.println("Water bird report starts");

		Map<Qname, Event> events = getEvents();
		System.out.println("Events: " + events.size());

		writeEvents(events.values());
		System.out.println("Event file written");

		writeObservations(events);
		System.out.println("Observations file written");

		writeErrors();

		System.out.println("Water bird report completed");
	}

	private void writeEvents(Collection<Event> events) {
		File eventFile = getFile("paikan_laskennat");
		eventFile.write(header(Event.class));
		for (Event event : events) {
			eventFile.write(line(event));
		}
	}

	private void writeObservations(Map<Qname, Event> events) {
		File observationsFile = getFile("havainnot");
		observationsFile.write(header(Observation.class));
		try (ResultStream<Object[]> results = getCustomQuery(UNIT_SQL)) {
			int count = 0;
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				count++;
				if (count % 10000 == 0) System.out.println(" .. observation line " + count);
				Object[] row = i.next();
				// SELECT    SELECT unit.document_id, unit.unit_id,
				// ORDER BY  np.name, np.id, unit.datebegin_key, unit.gathering_order, unit.unit_order
				Qname eventId = qname(row[0]);
				Event event = events.get(eventId);
				Observation observation = buildObservation(row, event);
				observationsFile.write(line(observation));
			}
		}
	}

	private Observation buildObservation(Object[] row, Event event) {
		//   SELECT unit.document_id, unit.unit_id,
		//	       unit.taxon_verbatim, taxon.key, taxon.birdlife_code, taxon.euring_code, taxon.scientific_name,
		//	       unit.abundancestring, sex.id, unit.notes,
		//	       f1.value as female, f2.value_integer as juvInd, f3.value as juvAcc, f4.value as juvAge, f5.value as use,
		//	       f6.value_integer as pairCount, f7.value_integer as pairCountOpinion, f8.value as opinionReasoning, f9.value as shortHand
		Observation obs = new Observation();
		obs.form = event.form;
		obs.period = event.period;
		obs.eventId = event.eventId;
		obs.gatheringId = event.gatheringId;
		obs.routeId = event.routeId;
		obs.routeName = event.routeName;
		obs.oldRouteIds = event.oldRouteIds;
		obs.collection = event.collection;
		obs.year = event.year;
		obs.month = event.month;
		obs.day = event.day;

		int i = 1;
		obs.unitId = qname(row[i++]);
		obs.reportedName = string(row[i++]);
		Integer taxonkey = integer(row[i++]);
		if (taxonkey !=  null) {
			obs.taxonId = new Qname("MX." + taxonkey);
		}
		String birdLifeCode = string(row[i++]);
		String euringCode = string(row[i++]);
		obs.birdCode = birdLifeCode != null ? birdLifeCode : euringCode;
		obs.scientificName = string(row[i++]);

		obs.abundance = string(row[i++]);
		obs.sex = string(row[i++]);
		obs.notes = string(row[i++]);
		obs.femalePresent = string(row[i++]);
		if (obs.femalePresent != null) obs.femalePresent = obs.femalePresent.replace("http://tun.fi/MY.waterbirdFemaleEnum", "");
		obs.juvCount = integer(row[i++]);
		obs.juvAccurate = bool(row[i++]);
		obs.juvAge = string(row[i++]);
		if (obs.juvAge != null) obs.juvAge = obs.juvAge.replace("http://tun.fi/MY.waterbirdJuvenileAgeClass", "");
		obs.useOpinion = bool(row[i++]);
		obs.pairCount = integer(row[i++]);
		obs.pairCountOpinion = integer(row[i++]);
		obs.opinionReasoning = string(row[i++]);
		if (obs.opinionReasoning != null) obs.opinionReasoning = obs.opinionReasoning.replace("http://tun.fi/MY.waterbirdPairCountOpinionReasoningEnum", "");
		String shortHand = string(row[i++]);
		if (shortHand != null && obs.abundance == null) obs.abundance = shortHand;
		obs.issues = issues(event.issues, row, i);
		return obs;
	}

	private Map<Qname, Event> getEvents() {
		ObserverIds observerIds = getObserverIds();
		Map<Qname, AnnotationData> documentAnnotations = getAnnotations();
		Map<Qname, List<String>> taxonCensusTaxonSets = getTaxonCensusTaxonSets();

		Map<Qname, Event> events = new LinkedHashMap<>();
		try (ResultStream<Object[]> results = getCustomQuery(GATHERING_SQL)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Event event = buildEvent(row);
				event.userIds = observerIds.userIds.get(event.eventId);
				event.personIds = observerIds.personIds.get(event.eventId);
				event.lintuvaaraIds = observerIds.lintuvaaraIds.get(event.eventId);
				event.omaRiistaIds = observerIds.omaRiistaIds.get(event.eventId);
				if (documentAnnotations.containsKey(event.eventId)) {
					event.errors = documentAnnotations.get(event.eventId).getTags();
					event.errorNotes = documentAnnotations.get(event.eventId).getNotes();
				}
				if (taxonCensusTaxonSets.containsKey(event.gatheringId)) {
					event.taxonCensus = taxonCensusTaxonSets.get(event.gatheringId).toString();
				}
				events.put(event.eventId, event);
			}
		}
		return events;
	}

	private Event buildEvent(Object[] row) {
		// SELECT    g.document_id, g.gathering_id, collection.id, f.id, np.id as routeId, np.name as routeName, np.alternative_ids as oldRouteIds,
		//           np.municipality_displayname as municipality, g.ykj_10km_n_center as ykjn, g.ykj_10km_e_center as ykje,
		//           g.eurefnmin, g.eurefnmax, g.eurefemin, g.eurefemax, g.euref_wkt,
		//           g.year, g.month, g.day, g.hourbegin, g.hourend, team.team_text as team, g.notes,
		//           f1.value as gtype, f2.value as hindered, f3.value as wind, f4.value as rain, f5.value as fog,
		//           f6.value as skipped, f7.value as nounits, f8.value as binoculars, f9.value as skope,
		//           f10.value as wayoftravel, f11.value as habitat, f12.value_integer as shoreline, f13.value_integer as area,
		//           f14.value as period
		Event event = new Event();
		int i = 0;
		event.eventId = qname(row[i++]);
		event.gatheringId = qname(row[i++]);
		event.collection = collection(qname(row[i++]));
		String formId = string(row[i++]);
		if (formId != null) {
			event.form = Qname.fromURI(formId).equals(Const.WATERBIRD_PAIRCOUNT_FORM_ID) ? "Parilaskenta" : "Poikuelaskenta";
		}
		event.routeId = qname(row[i++]);
		event.routeName = string(row[i++]);
		event.oldRouteIds = string(row[i++]);
		event.municipality = string(row[i++]);
		event.ykjLat = integer(row[i++]);
		event.ykjLon = integer(row[i++]);
		event.euref_N_min = integer(row[i++]);
		event.euref_N_max = integer(row[i++]);
		event.euref_E_min = integer(row[i++]);
		event.euref_E_max = integer(row[i++]);
		event.eurefWKT = string(row[i++]);
		event.year = integer(row[i++]);
		event.month = integer(row[i++]);
		event.day = integer(row[i++]);
		event.startHour = integer(row[i++]);
		event.endHour = integer(row[i++]);
		event.observers = string(row[i++]);
		event.notes = string(row[i++]);
		event.type  = string(row[i++]);
		if (event.type != null) event.type = event.type.replace("http://tun.fi/MY.gatheringTypeWaterbird", "");
		event.hindered = bool(row[i++]);
		event.wind = bool(row[i++]);
		event.rain = bool(row[i++]);
		event.fog = bool(row[i++]);
		event.skipped = bool(row[i++]);
		event.noUnits = bool(row[i++]);
		event.binoculars = bool(row[i++]);
		event.skope = bool(row[i++]);
		event.wayOfTravel = string(row[i++]);
		if (event.wayOfTravel != null) event.wayOfTravel = event.wayOfTravel.replace("http://tun.fi/WBC.wayOfTravelEnum", "");
		event.habitat = string(row[i++]);
		if (event.habitat != null) event.habitat = event.habitat.replace("http://tun.fi/MY.waterbirdHabitatEnum", "");
		event.shoreline = integer(row[i++]);
		event.area = integer(row[i++]);
		event.period = string(row[i++]);
		if (event.period != null) event.period = event.period.replace("http://tun.fi/MY.waterbirdCensusPeriodEnum", "");
		event.issues = issues(row, i);
		return event;
	}

	private String collection(Qname qname) {
		if (Const.WATERBIRD_LUOMUS_COLLECTION.equals(qname)) return "Luomus";
		if (Const.WATERBIRD_LUKE_COLLECTION.equals(qname)) return "Luke";
		if (Const.WATERBIRD_TOP_COLLECTION.equals(qname)) return "Top";
		return "Unknown";
	}

	public class Event {
		@Order(0) public Qname eventId;
		@Order(1) public Qname gatheringId;
		@Order(2) public Qname routeId;
		@Order(3) public String routeName;
		@Order(3.5) public String oldRouteIds;
		@Order(4) public String collection;
		@Order(5) public Integer year;
		@Order(6) public Integer month;
		@Order(7) public Integer day;
		@Order(8) public String form;
		@Order(9) public String period;

		@Order(10) public String municipality;
		@Order(11.1) public Integer euref_N_min;
		@Order(11.2) public Integer euref_N_max;
		@Order(11.3) public Integer euref_E_min;
		@Order(11.4) public Integer euref_E_max;
		@Order(12.1) public Integer ykjLat;
		@Order(12.2) public Integer ykjLon;
		@Order(13) public String eurefWKT;
		@Order(14) public Integer startHour;
		@Order(15) public Integer endHour;
		@Order(16) public String taxonCensus;

		@Order(25.0) public String errors;
		@Order(25.1) public String errorNotes;
		@Order(26.0) public Set<String> userIds;
		@Order(26.1) public Set<String> personIds;
		@Order(26.2) public Set<Integer> lintuvaaraIds;
		@Order(26.3) public Set<Integer> omaRiistaIds;
		@Order(27) public String observers;
		@Order(31) public String type;
		@Order(32) public Boolean hindered;
		@Order(33) public Boolean wind;
		@Order(34) public Boolean rain;
		@Order(35) public Boolean fog;
		@Order(36) public Boolean skipped;
		@Order(37) public Boolean noUnits;
		@Order(38) public Boolean binoculars;
		@Order(39) public Boolean skope;
		@Order(40) public String wayOfTravel;
		@Order(41) public String habitat;
		@Order(42) public Integer shoreline;
		@Order(43) public Integer area;
		@Order(45) public String notes;
		@Order(46) public String issues;
	}

	public class Observation {

		@Order(0) public Qname unitId;
		@Order(1) public Qname eventId;
		@Order(2) public Qname gatheringId;
		@Order(3) public Qname routeId;
		@Order(4) public String routeName;
		@Order(4.5) public String oldRouteIds;
		@Order(5) public String collection;
		@Order(6) public Integer year;
		@Order(7) public Integer month;
		@Order(8) public Integer day;
		@Order(9) public String form;
		@Order(10) public String period;

		@Order(20) public String scientificName;
		@Order(21) public Qname taxonId;
		@Order(22) public String birdCode;
		@Order(23) public String reportedName;

		@Order(30) public String sex;
		@Order(31) public String abundance;

		@Order(40) public Integer pairCount;
		@Order(41) public Integer pairCountOpinion;
		@Order(42) public String opinionReasoning;
		@Order(43) public Boolean useOpinion;

		@Order(50) public Integer juvCount;
		@Order(51) public Boolean juvAccurate;
		@Order(52) public String juvAge;

		@Order(60) public String femalePresent;
		@Order(61) public String notes;
		@Order(62) public String issues;
	}

}
