package fi.laji.datawarehouse.etl.service;

import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/user-linking/*"})
public class UserIdToPersonIdAPI extends ETLBaseServlet {

	private static final long serialVersionUID = 1326919665539467118L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			Map<String, Qname> linkings = getDao().getPersonLookupStructure();
			return jsonResponse(generateResponse(linkings));
		} catch (Throwable e) {
			return loggedSystemError500(e, UserIdToPersonIdAPI.class, res, req);
		}
	}

	private JSONArray generateResponse(Map<String, Qname> linkings) {
		JSONArray a = new JSONArray();
		for (Map.Entry<String, Qname> e : linkings.entrySet()) {
			String userId = e.getKey();
			Qname personId = e.getValue();
			if (userIdIsPersonId(userId, personId)) continue;
			a.appendObject(new JSONObject().setString("userId", userId).setString("personId", personId.toString()));
		}
		return a;
	}

	private boolean userIdIsPersonId(String userId, Qname personId) {
		return userId.equals(personId.toString()) || userId.equals(personId.toURI());
	}

}
