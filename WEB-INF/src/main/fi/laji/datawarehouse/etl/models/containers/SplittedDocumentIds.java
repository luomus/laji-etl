package fi.laji.datawarehouse.etl.models.containers;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;

public class SplittedDocumentIds {

	// original document id -> original unit id -> splitted document id
	private Map<Qname, Map<Qname, Qname>> data = new HashMap<>();

	public Collection<Qname> getSplittedDocumentIdsFor(Qname originalDocumentId) {
		if (!data.containsKey(originalDocumentId)) return Collections.emptyList();
		return data.get(originalDocumentId).values();
	}

	public Qname getSplittedDocumentIdFor(Qname originalUnitId) {
		for (Map<Qname, Qname> m : data.values()) {
			Qname splittedDocumentId = m.get(originalUnitId);
			if (splittedDocumentId != null) return splittedDocumentId;
		}
		return null;
	}

	public Qname getSplittedDocumentIdFor(Qname originalDocumentId, Qname originalUnitId) {
		if (!data.containsKey(originalDocumentId)) return null;
		return data.get(originalDocumentId).get(originalUnitId);
	}

	public void add(Qname originalDocumentId, Qname originalUnitId, Qname splittedDocumentId) {
		if (!data.containsKey(originalDocumentId)) {
			data.put(originalDocumentId, new HashMap<>());
		}
		data.get(originalDocumentId).put(originalUnitId, splittedDocumentId);
	}

	@Override
	public String toString() {
		return data.toString();
	}

}
