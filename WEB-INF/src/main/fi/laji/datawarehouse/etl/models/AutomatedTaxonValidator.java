package fi.laji.datawarehouse.etl.models;

import java.awt.Polygon;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.FinlandAreaUtil;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class AutomatedTaxonValidator {

	private final ValidationRules rules;

	public AutomatedTaxonValidator(ValidationRules rules) {
		this.rules = rules;
	}

	public static enum Pass {
		TRUE,
		FALSE,
		NOT_VALIDATED
	}

	public Pass passesPeriodCheck(Qname taxonId, LifeStage lifeStage, DateRange date) {
		if (taxonId == null || date == null) return Pass.NOT_VALIDATED;

		PeriodRule rule = rules.getPeriodRule(taxonId, lifeStage);
		if (rule == null) return Pass.NOT_VALIDATED;

		DateValue begin = DateUtils.convertToDateValue(date.getBegin());
		DateValue end = DateUtils.convertToDateValue(date.getEnd());

		int beginYear = begin.getYearAsInt();
		int endYear = end.getYearAsInt();
		if (beginYear != endYear && beginYear != endYear-1) return Pass.NOT_VALIDATED;

		int seasonBegin = Converter.season(begin);
		int seasonEnd = Converter.season(end);

		if (seasonBegin <= 101 && seasonEnd >= 1231) return Pass.NOT_VALIDATED;

		return rule.passes(seasonBegin, seasonEnd) ? Pass.TRUE : Pass.FALSE;
	}

	public Pass passesDistributionCheck(Qname taxonId, DateRange date, Coordinates wgs84Coordinates) {
		if (taxonId == null || date == null || wgs84Coordinates == null) return Pass.NOT_VALIDATED;

		DistributionRule rule = rules.getDistributionRule(taxonId);
		if (rule == null) return Pass.NOT_VALIDATED;

		DateValue begin = DateUtils.convertToDateValue(date.getBegin());
		DateValue end = DateUtils.convertToDateValue(date.getEnd());

		int beginYear = begin.getYearAsInt();
		int endYear = end.getYearAsInt();

		for (int year=beginYear; year<=endYear; year++) {
			if (rule.passes(year, wgs84Coordinates)) return Pass.TRUE;
		}
		return Pass.FALSE;
	}

	public static class ValidationRules {

		private final Map<Qname, Map<LifeStage, PeriodRule>> periodRules = new HashMap<>();
		private final Map<Qname, DistributionRule> distributionRules = new HashMap<>();

		public ValidationRules rule(Qname taxonId, Distribution ...allowed) {
			if (distributionRules.containsKey(taxonId)) throw new ValidationRuleException("Rule already exists", taxonId);
			distributionRules.put(taxonId, new DistributionRule(taxonId, allowed).validate());
			return this;
		}

		public ValidationRules rule(Qname taxonId, LifeStage lifeStage, Period ... allowed) {
			if (!periodRules.containsKey(taxonId)) periodRules.put(taxonId, new HashMap<>());
			if (periodRules.get(taxonId).containsKey(lifeStage)) throw new ValidationRuleException("Rule already exists for life stage '" + lifeStage + "'", taxonId);
			periodRules.get(taxonId).put(lifeStage, new PeriodRule(taxonId, allowed).validate());
			return this;
		}

		public PeriodRule getPeriodRule(Qname taxonId, LifeStage lifeStage) {
			if (!periodRules.containsKey(taxonId)) return null;
			PeriodRule rule = periodRules.get(taxonId).get(lifeStage);
			if (rule != null) return rule;
			if (lifeStage == null) {
				rule = periodRules.get(taxonId).get(LifeStage.ADULT);
				if (rule != null) return rule;
			}
			rule = periodRules.get(taxonId).get(null);
			if (rule != null) return rule;
			return null;
		}

		public DistributionRule getDistributionRule(Qname taxonId) {
			return distributionRules.get(taxonId);
		}

	}

	public static class Period {

		private final TreeSet<Integer> allowed;

		/**
		 * Format d.M or dd.MM
		 * @param allowedSeasonBegin
		 * @param allowedSeasonEnd
		 */
		public Period(String allowedSeasonBegin, String allowedSeasonEnd) {
			allowed = new TreeSet<>();
			try {
				int seasonBegin = Converter.season(date(allowedSeasonBegin));
				int seasonEnd = Converter.season(date(allowedSeasonEnd));
				if (seasonBegin <= seasonEnd) {
					for (int i = seasonBegin; i <= seasonEnd; i++) {
						allowed.add(i);
					}
				} else {
					for (int i = seasonBegin; i <= 1231; i++) {
						allowed.add(i);
					}
					for (int i = 101; i <= seasonEnd; i++) {
						allowed.add(i);
					}
				}
			} catch (Exception e) {
				// validation will fail - no need to handle exception here
			}
		}

		private DateValue date(String date) {
			String[] parts = date.split(Pattern.quote("."));
			return new DateValue(parts[0], parts[1], -1);
		}

		public boolean includes(int seasonBegin, int seasonEnd) {
			if (allowed.contains(seasonBegin)) return true;
			if (allowed.contains(seasonEnd)) return true;
			for (int i = seasonBegin; i <= seasonEnd; i++) {
				if (allowed.contains(i)) return true;
			}
			return false;
		}

		public void validate(Qname taxonId) {
			if (allowed.isEmpty()) throw new ValidationRuleException("Invalid season", taxonId);
			if (allowed.first() < 101) throw new ValidationRuleException("Invalid season", taxonId);
			if (allowed.last() > 1231) throw new ValidationRuleException("Invalid season", taxonId);
		}

	}

	private static class PeriodRule {

		private final Qname taxonId;
		private final Period[] allowedPeriods;

		public PeriodRule(Qname taxonId, Period ...allowed) {
			this.taxonId = taxonId;
			this.allowedPeriods = allowed;
		}

		public PeriodRule validate() {
			if (allowedPeriods == null || allowedPeriods.length == 0) throw new ValidationRuleException("No rules", taxonId);
			for (Period p : allowedPeriods) {
				p.validate(taxonId);
			}
			return this;
		}

		public boolean passes(int seasonBegin, int seasonEnd) {
			for (Period allowed : allowedPeriods) {
				if (allowed.includes(seasonBegin, seasonEnd)) return true;
			}
			return false;
		}

	}

	public static class Distribution {

		private final Integer beginYear;
		private final Integer endYear;
		private final String allowedAreaWkt;
		private final boolean allFinland;
		private List<Polygon> polygons = null;

		public Distribution(String allowedAreaWkt) {
			this.beginYear = null;
			this.endYear = null;
			this.allowedAreaWkt = allowedAreaWkt;
			this.allFinland = false;
		}

		public Distribution(Integer beginYear, Integer endYear, String allowedAreaWkt) {
			this.beginYear = beginYear;
			this.endYear = endYear;
			this.allowedAreaWkt = allowedAreaWkt;
			this.allFinland = false;
		}

		private Distribution(Integer beginYear, Integer endYear) {
			this.beginYear = beginYear;
			this.endYear = endYear;
			this.allowedAreaWkt = null;
			this.allFinland = true;
		}

		private Distribution() {
			this.beginYear = null;
			this.endYear = null;
			this.allowedAreaWkt = null;
			this.allFinland = true;
		}

		public static Distribution allFinland(Integer beginYear, Integer endYear) {
			return new Distribution(beginYear, endYear);
		}

		public static Distribution allFinland() {
			return new Distribution();
		}

		public boolean passes(Coordinates coordinates) {
			if (allFinland) return true;
			if (coordinates.checkIfIsPoint()) {
				for (Polygon polygon : getPolygons()) {
					if (polygon.contains(coordinates.getLatMin()*1000, coordinates.getLonMin()*1000)) return true;
				}
				return false;
			}
			Rectangle2D rect = toRectangle2D(coordinates);
			for (Polygon polygon : getPolygons()) {
				if (polygon.intersects(rect)) return true;
			}
			return false;
		}

		private Rectangle2D toRectangle2D(Coordinates coordinates) {
			return new Rectangle2D.Double(
					coordinates.getLatMin()*1000,
					coordinates.getLonMin()*1000,
					(coordinates.getLatMax()-coordinates.getLatMin())*1000,
					(coordinates.getLonMax()-coordinates.getLonMin())*1000);
		}

		private void validateAllowedAreas(Qname taxonId) {
			if (allFinland) return;
			if (allowedAreaWkt == null || allowedAreaWkt.isEmpty()) throw new ValidationRuleException("Allowed area not defined", taxonId);
			try {
				getPolygons();
			} catch (Exception e) {
				throw new ValidationRuleException("Invalid geometry: " + e.getMessage(), taxonId);
			}
		}

		private List<Polygon> getPolygons() {
			if (polygons == null) {
				polygons = parsePolygons();
			}
			return polygons;
		}

		private List<Polygon> parsePolygons() {
			List<Polygon> polygons = new ArrayList<>();
			try {
				Geo geo = Geo.fromWKT(allowedAreaWkt, Type.WGS84);
				for (Feature f : geo.getFeatures()) {
					if (f instanceof fi.laji.datawarehouse.etl.models.dw.geo.Polygon) {
						fi.laji.datawarehouse.etl.models.dw.geo.Polygon dwPolygon = (fi.laji.datawarehouse.etl.models.dw.geo.Polygon) f;
						polygons.add(toPolygon(dwPolygon));
					} else {
						throw new IllegalArgumentException("Only polygons allowed");
					}
				}
			} catch (DataValidationException e) {
				throw new IllegalArgumentException("Invalid wkt");
			}
			return polygons;
		}

		private Polygon toPolygon(Feature f) {
			Polygon polygon = new Polygon();
			boolean inFinland = false;
			for (Point p : f) {
				if (!inFinland) {
					inFinland = inFinland(p);
				}
				polygon.addPoint((int)(p.getLat()*1000), (int)(p.getLon()*1000));
			}
			if (!inFinland) {
				double centerLat = (f.getLatMax() + f.getLatMin()) / 2.0;
				double centerLon = (f.getLonMax() + f.getLonMin()) / 2.0;
				inFinland = inFinland(centerLat, centerLon);
				if (!inFinland) throw new IllegalArgumentException("Allowed area outside Finland");
			}
			return polygon;
		}

		private boolean inFinland(double lat, double lon) {
			return FinlandAreaUtil.isInsideGeneralFinlandArea(lat, lon);
		}

		private boolean inFinland(Point p) {
			return FinlandAreaUtil.isInsideGeneralFinlandArea(p.getLat(), p.getLon());
		}

	}

	private static class DistributionRule {

		private final Qname taxonId;
		private final List<Distribution> allowedDistributions;

		public DistributionRule(Qname taxonId, Distribution[] allowed) {
			this.taxonId = taxonId;
			this.allowedDistributions = Arrays.asList(allowed);
		}

		public DistributionRule validate() {
			if (allowedDistributions == null || allowedDistributions.isEmpty()) throw new ValidationRuleException("No rules", taxonId);
			Iterator<Distribution> i = allowedDistributions.iterator();
			Distribution prev = null;
			Integer minYear = null;
			while (i.hasNext()) {
				Distribution d = i.next();
				boolean last = !i.hasNext();
				boolean first = prev == null;
				if (d.beginYear == null && !first) throw new ValidationRuleException("Unbounded begin year in middle of rules", taxonId);
				if (d.endYear == null && !last) throw new ValidationRuleException("Unbounded end year in middle of rules", taxonId);
				if (invalid(d.beginYear)) throw new ValidationRuleException("Invalid year " + d.beginYear, taxonId);
				if (invalid(d.endYear)) throw new ValidationRuleException("Invalid year " + d.endYear, taxonId);
				if (d.beginYear != null && d.endYear != null && d.beginYear > d.endYear) throw new ValidationRuleException("End before begin", taxonId);
				if (minYear != null) {
					if (d.beginYear < minYear) throw new ValidationRuleException("Ranges not in ascending order", taxonId);
				}
				if (prev != null) {
					if (prev.endYear != null && prev.endYear >= d.beginYear) throw new ValidationRuleException("Ranges overlap" , taxonId);
				}
				if (!d.allFinland) {
					d.validateAllowedAreas(taxonId);
				}
				prev = d;
				minYear = d.beginYear;
			}
			return this;
		}

		private boolean invalid(Integer year) {
			if (year == null) return false;
			return year < 1500 || year > 2100;
		}

		public boolean passes(int year, Coordinates coordinates) {
			Distribution d = getForYear(year);
			if (d == null) return false;
			return d.passes(coordinates);
		}

		private Distribution getForYear(int year) {
			for (Distribution d : allowedDistributions) {
				int begin = d.beginYear == null ? 0 : d.beginYear;
				int end = d.endYear == null ? 9999 : d.endYear;
				if (year >= begin && year <= end) return d;
			}
			return null;
		}

	}

	public static class ValidationRuleException extends ETLException {
		private static final long serialVersionUID = 5827276790350251040L;
		public ValidationRuleException(String message, Qname taxonId) {
			super(message + " for " + taxonId);
		}
	}

}
