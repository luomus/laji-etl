package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="namedplace")
public class NamedPlaceEntity extends NameableEntity {

	private Qname municipalityId;
	private String municipalityDisplayName;
	private Qname birdAssociationAreaId;
	private String birdAssociationAreaDisplayName;
	private Qname collectionId;
	private SingleCoordinates ykj10km;
	private SingleCoordinates wgs84CenterPoint;
	private String wgs84WKT;
	private String alternativeId;
	private String alternativeIds;
	private List<Qname> tags;

	public NamedPlaceEntity() {}

	public NamedPlaceEntity(String uri) {
		setId(uri);
	}

	@Override
	@StatisticsQueryAlllowedField
	public String getId() {
		return super.getId();
	}

	@Override
	@StatisticsQueryAlllowedField
	public String getName() {
		return super.getName();
	}

	@Column(name="collection_id")
	@StatisticsQueryAlllowedField
	public String getCollectionId() {
		if (collectionId == null) return null;
		return collectionId.toURI();
	}

	public void setCollectionId(String collectionId) {
		if (collectionId == null) {
			this.collectionId = null;
		} else {
			this.collectionId = Qname.fromURI(collectionId);
		}
	}

	@Column(name="bird_association_area_id")
	@StatisticsQueryAlllowedField
	public String getBirdAssociationAreaId() {
		if (birdAssociationAreaId == null) return null;
		return birdAssociationAreaId.toURI();
	}

	public void setBirdAssociationAreaId(String birdAssociationAreaId) {
		if (birdAssociationAreaId == null) {
			this.birdAssociationAreaId = null;
		} else {
			this.birdAssociationAreaId = Qname.fromURI(birdAssociationAreaId);
		}
	}

	@Column(name="municipality_id")
	@StatisticsQueryAlllowedField
	public String getMunicipalityId() {
		if (municipalityId == null) return null;
		return municipalityId.toURI();
	}

	public void setMunicipalityId(String municipalityId) {
		if (municipalityId == null) {
			this.municipalityId = null;
		} else {
			this.municipalityId = Qname.fromURI(municipalityId);
		}
	}

	@Column(name="municipality_displayname")
	@StatisticsQueryAlllowedField
	public String getMunicipalityDisplayName() {
		return municipalityDisplayName;
	}

	public void setMunicipalityDisplayName(String municipalityDisplayName) {
		this.municipalityDisplayName = Util.trimToByteLength(municipalityDisplayName, 1000);
	}

	@Column(name="bird_association_area_displayname")
	@StatisticsQueryAlllowedField
	public String getBirdAssociationAreaDisplayName() {
		return birdAssociationAreaDisplayName;
	}

	public void setBirdAssociationAreaDisplayName(String birdAssociationAreaDisplayName) {
		this.birdAssociationAreaDisplayName = Util.trimToByteLength(birdAssociationAreaDisplayName, 1000);
	}

	@AttributeOverrides({
		@AttributeOverride(name="lat", column=@Column(name="ykj_10km_n")),
		@AttributeOverride(name="lon", column=@Column(name="ykj_10km_e"))
	})
	@StatisticsQueryAlllowedField
	public SingleCoordinates getYkj10km() {
		return ykj10km;
	}

	public void setYkj10km(SingleCoordinates ykj10km) {
		if (ykj10km != null) {
			ykj10km.setType(Type.YKJ);
		}
		this.ykj10km = ykj10km;
	}

	@AttributeOverrides({
		@AttributeOverride(name="lat", column=@Column(name="latitudecenter")),
		@AttributeOverride(name="lon", column=@Column(name="longitudecenter"))
	})
	public SingleCoordinates getWgs84CenterPoint() {
		return wgs84CenterPoint;
	}

	public void setWgs84CenterPoint(SingleCoordinates wgs84CenterPoint) {
		if (wgs84CenterPoint != null) {
			wgs84CenterPoint.setType(Type.WGS84);
		}
		this.wgs84CenterPoint = wgs84CenterPoint;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public String getWgs84WKT() {
		return wgs84WKT;
	}

	public void setWgs84WKT(String wgs84WKT) {
		this.wgs84WKT = wgs84WKT;
	}

	@Column(name="alternative_id")
	public String getAlternativeId() {
		return alternativeId;
	}

	public void setAlternativeId(String alternativeId) {
		this.alternativeId = Util.trimToByteLength(alternativeId, 40);
	}

	@Column(name="alternative_ids")
	public String getAlternativeIds() {
		return alternativeIds;
	}

	public void setAlternativeIds(String alternativeIds) {
		this.alternativeIds = Util.trimToByteLength(alternativeIds, 1000);
	}

	@Transient
	public List<Qname> getTags() {
		return tags;
	}

	public void setTags(ArrayList<Qname> tags) {
		this.tags = tags;
	}

	public void addTag(Qname tag) {
		if (this.tags == null) {
			this.tags = new ArrayList<>();
		}
		if (!this.tags.contains(tag)) {
			this.tags.add(tag);
			Collections.sort(this.tags);
		}
	}

	@Column(name="tags")
	@FieldDefinition(ignoreForQuery=true)
	public String getTagsString( ) {
		if (tags == null) return null;
		if (tags.isEmpty()) return null;
		return Util.trimToByteLength(tags.stream().map(t->t.toString()).collect(Collectors.joining("|", "|", "|")), 1000);
	}

	public void setTagsString(String tags) {
		setTags(null);
		if (tags == null) return;
		for (String tag : tags.split(Pattern.quote("|"))) {
			if (tag.isEmpty()) continue;
			addTag(new Qname(tag));
		}
	}

	@Override
	public String toString() { // Must only include fields stored to Vertica: Used for comparison between current values and existing values
		return "NamedPlaceEntity [getId()=" + getId() + ", getName()=" + getName() + ", getCollectionId()=" + getCollectionId() + ", getBirdAssociationAreaId()="
		+ getBirdAssociationAreaId() + ", getMunicipalityId()=" + getMunicipalityId() + ", getMunicipalityDisplayName()=" + getMunicipalityDisplayName()
		+ ", getBirdAssociationAreaDisplayName()=" + getBirdAssociationAreaDisplayName() + ", getYkj10km()=" + getYkj10km() + ", getWgs84CenterPoint=" + getWgs84CenterPoint()
		+ ", getAlternativeId()=" + getAlternativeId() + ", getAlternativeIds()=" + getAlternativeIds() + ", getTagsString()=" + getTagsString() +"]";
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) throw new NullPointerException();
		if (!(o instanceof NamedPlaceEntity)) throw new UnsupportedOperationException("Can only compare to " + NamedPlaceEntity.class);
		return this.toString().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

}
