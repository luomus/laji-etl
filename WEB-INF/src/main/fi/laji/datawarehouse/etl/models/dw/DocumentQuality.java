package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import fi.laji.datawarehouse.query.download.model.FileDownloadField;

@Embeddable
public class DocumentQuality {

	private Quality issue;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="message", column=@Column(name="document_issue_message"))
	})
	@FileDownloadField(name="Issue", order=2)
	public Quality getIssue() {
		return issue;
	}

	public DocumentQuality setIssue(Quality issue) {
		this.issue = issue;
		return this;
	}

	public boolean hasIssues() {
		return issue != null;
	}

	public DocumentQuality conseal() {
		DocumentQuality consealed = new DocumentQuality();
		if (issue != null) consealed.setIssue(issue.conceal());
		return consealed;
	}

}
