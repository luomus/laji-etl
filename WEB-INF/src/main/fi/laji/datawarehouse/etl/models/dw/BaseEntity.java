package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;

@MappedSuperclass
public class BaseEntity {

	private Long key;
	private String id;

	public BaseEntity() {}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@FieldDefinition(ignoreForQuery=true)
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getId() {
		return id;
	}

	public BaseEntity setId(String id) {
		this.id = Util.trimToByteLength(id, 1000);
		return this;
	}

}