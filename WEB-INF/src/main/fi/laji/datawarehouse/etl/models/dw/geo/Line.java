package fi.laji.datawarehouse.etl.models.dw.geo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;

public class Line implements Feature {

	protected Geometry geometry;

	Line(Coordinate[] coordinates) {
		geometry = new GeometryFactory().createLineString(coordinates);
	}

	Line(Geometry geometry) {
		if (geometry == null) throw new IllegalArgumentException("Null geometry");
		this.geometry = geometry;
	}

	public static Feature from(int[] ... latLonPairs) {
		return from(toPoints(latLonPairs));
	}

	public static Feature from(double[] ... latLonPairs) {
		return from(toPoints(latLonPairs));
	}

	protected static List<Point> toPoints(int[][] latLonPairs) {
		List<Point> points = new ArrayList<>(latLonPairs.length);
		for (int i = 0; i<latLonPairs.length; i++) {
			int[] coord = latLonPairs[i];
			if (coord.length == 2) {
				points.add(new Point(coord[0], coord[1]));
			} else {
				throw new IllegalArgumentException("Coordinate pair must have two ints");
			}
		}
		return points;
	}

	public static Feature from(List<Point> points) {
		List<Point> fixedPoints = fixPoints(points);
		if (fixedPoints.isEmpty()) throw new IllegalArgumentException("Feature must not be empty");
		if (fixedPoints.size() == 1) return fixedPoints.get(0);
		return new Line(toCoordinates(fixedPoints));
	}

	protected static List<Point> fixPoints(List<Point> points) {
		List<Point> fixedPoints = new ArrayList<>(points.size());
		Point prev = null;
		for (Point p : points) {
			if (p == null) continue;
			if (prev != null && p.equals(prev)) continue;
			fixedPoints.add(p);
			prev = p;
		}
		return fixedPoints;
	}

	protected static List<Point> toPoints(double[][] latLonPairs) {
		List<Point> points = new ArrayList<>(latLonPairs.length);
		for (int i = 0; i<latLonPairs.length; i++) {
			double[] coord = latLonPairs[i];
			if (coord.length == 2) {
				points.add(new Point(coord[0], coord[1]));
			} else {
				throw new IllegalArgumentException("Coordinate pair must have two doubles");
			}
		}
		return points;
	}

	protected static Coordinate[] toCoordinates(List<Point> points) {
		Coordinate[] coordinates = new Coordinate[points.size()];
		int i = 0;
		for (Point p : points) {
			coordinates[i++] = p.asCoordinate();
		}
		return coordinates;
	}

	protected static Coordinate[] toCoordinates(int[][] ... coordinates) {
		List<Point> points = new ArrayList<>(coordinates.length);
		return toCoordinates(points);
	}

	@Override
	public Line validate() throws DataValidationException {
		if (geometry.getCoordinates().length < 1) throw new DataValidationException("Line must not be empty");
		if (geometry.getCoordinates().length == 1) throw new DataValidationException("Line must have at least two points");
		return this;
	}

	@Override
	public Double getLatMin(){
		return geometry.getEnvelopeInternal().getMinY();
	}

	@Override
	public Double getLatMax() {
		return geometry.getEnvelopeInternal().getMaxY();
	}

	@Override
	public Double getLonMin() {
		return geometry.getEnvelopeInternal().getMinX();
	}

	@Override
	public Double getLonMax()  {
		return geometry.getEnvelopeInternal().getMaxX();
	}

	@Override
	public String getWKT() {
		return geometry.toText();
	}

	@Override
	public JSONObject getGeoJSON() {
		JSONObject json = new JSONObject().setString("type", "LineString");
		JSONArray coordinates = json.getArray("coordinates");
		for (Coordinate c : geometry.getCoordinates()) {
			coordinates.appendArray(new JSONArray()
					.appendDouble(c.x)
					.appendDouble(c.y));
		}
		return json;
	}

	public static Feature fromGeoJSON(JSONObject geometry) throws DataValidationException {
		List<JSONArray> coordinateArrays = geometry.getArray("coordinates").iterateAsArray();
		List<Point> lineCoordinates = new ArrayList<>(coordinateArrays.size());
		for (JSONArray coordinatePairArray : coordinateArrays) {
			List<Double> coordinatePair = coordinatePairArray.iterateAsDouble();
			lineCoordinates.add(Point.from(coordinatePair.get(1), coordinatePair.get(0)));
		}
		return from(lineCoordinates).validate();
	}

	public double getLength() {
		return geometry.getLength();
	}

	public List<Point> getPoints() {
		List<Point> points = new ArrayList<>();
		for (Coordinate c : geometry.getCoordinates()) {
			points.add(new Point(c.y, c.x));
		}
		return points;
	}

	@Override
	public Iterator<Point> iterator() {
		return getPoints().iterator();
	}

	@Override
	public String toString() {
		List<String> s = new ArrayList<>();
		for (Point p : this) {
			s.add(p.toString());
		}
		return s.toString();
	}

	@Override
	public int size() {
		return geometry.getCoordinates().length;
	}

	public Polygon convexHull() {
		if (geometry.getCoordinates().length < 1) throw new IllegalStateException("Must have at least 1 point");
		ConvexHull convexHull = new ConvexHull(geometry.getCoordinates(), new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING_SINGLE)));
		return new Polygon(convexHull.getConvexHull().getCoordinates());
	}

	public Polygon convexHullAndSimplify(Type type) {
		if (geometry.getCoordinates().length < 1) throw new IllegalStateException("Must have at least 1 point");
		ConvexHull convexHull = new ConvexHull(geometry.getCoordinates(), new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING_SINGLE)));
		Geometry hullGeom = convexHull.getConvexHull();
		Geometry simplified = simplifyInternal(type, hullGeom);
		return new Polygon(simplified.getCoordinates());
	}

	public Polygon simplify(Type type, int toleranceMeters) {
		if (geometry.getCoordinates().length < 1) throw new IllegalStateException("Must have at least 1 point");
		Geometry simplified = simplifyInternal(type, geometry, toleranceMeters);
		return new Polygon(simplified.getCoordinates());
	}

	public Line simplify(Type type) {
		return new Line(simplifyInternal(type, geometry));
	}

	protected Geometry simplifyInternal(Type type, Geometry geometry) {
		return simplifyInternal(type, geometry, 10);
	}

	protected Geometry simplifyInternal(Type type, Geometry geometry, int toleranceMeters) {
		if (geometry.getCoordinates().length < 20) return geometry;
		double tolerance = type == Type.WGS84 ? (0.000001 * toleranceMeters) : toleranceMeters;
		return DouglasPeuckerSimplifier.simplify(geometry, tolerance);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((geometry.getCoordinates().length == 0) ? 0 : getPoints().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Line other = (Line) obj;
		return this.getPoints().equals(other.getPoints());
	}

}
