package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;

public class CollectionUpdater {

	private static final EnumToProperty ENUM_TO_PROPERTY = EnumToProperty.getInstance();

	private final DAO dao;

	public CollectionUpdater(DAO dao) {
		this.dao = dao;
	}

	public void update() {
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to update collections");
		List<BaseEntity> entitiesToUpdate =  new ArrayList<>();
		List<BaseEntity> entitiesToInsert =  new ArrayList<>();
		VerticaDimensionsDAO dimensionsDAO = dao.getVerticaDimensionsDAO();
		List<NameableEntity> existingEntities = dimensionsDAO.getEntities(CollectionEntity.class);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loaded existing from Vertica");
		Collection<CollectionMetadata> collections = dao.getCollections().values();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loaded from Lajistore, comparing..");
		for (CollectionMetadata collection : collections) {
			NameableEntity existing = getExisting(existingEntities, collection.getQname());
			NameableEntity current = toEntity(collection, dimensionsDAO);
			if (existing == null) {
				entitiesToInsert.add(current);
			} else if (!existing.equals(current)) {
				current.setKey(existing.getKey());
				entitiesToUpdate.add(current);
			}
		}

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Update: " + entitiesToUpdate.size() + " Insert: " + entitiesToInsert.size());

		if (!entitiesToUpdate.isEmpty()) {
			dimensionsDAO.updateEntities(entitiesToUpdate);
		}
		if (!entitiesToInsert.isEmpty()) {
			dimensionsDAO.insertEntities(entitiesToInsert);
		}

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Finished updating collections");
	}

	private NameableEntity toEntity(CollectionMetadata collection, VerticaDimensionsDAO dimensionsDAO) {
		CollectionEntity e = new CollectionEntity(collection.getQname().toURI());
		e.setName(collection.getName().forLocale("fi"));
		CollectionQuality collectionQuality = (CollectionQuality) ENUM_TO_PROPERTY.get(collection.getCollectionQuality());
		e.setCollectionQualityKey(dimensionsDAO.getEnumKey(collectionQuality));
		return e;
	}

	private CollectionEntity getExisting(List<NameableEntity> existingEntities, Qname id) {
		for (NameableEntity e : existingEntities) {
			if (e.getId().equals(id.toURI())) return (CollectionEntity) e;
		}
		return null;
	}

}
