package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer.GISLayerData;
import fi.laji.datawarehouse.etl.threads.custom.HelsinkiPullReader;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class MunicipalityGISHarmonizer extends BaseHarmonizer<GISLayerData> {

	public static class GISLayerData {
		public Qname collectionId;
		public String idPrefix;
		public JSONObject json;
		public GISLayerData(Qname collectionId, String idPrefix, JSONObject json) {
			this.collectionId = collectionId;
			this.idPrefix = idPrefix;
			this.json = json;
		}
	}

	@Override
	public List<DwRoot> harmonize(GISLayerData data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		DwRoot root = createRoot(data, source);
		try {
			parseRoot(data.json, root);
		} catch (Exception e) {
			Quality issue = new Quality(Issue.ETL_ISSUE, Source.AUTOMATED_FINBIF_VALIDATION, LogUtils.buildStackTrace(e, 10));
			if (root.getPrivateDocument() == null) root.createPrivateDocument();
			if (root.getPublicDocument() == null) root.createPublicDocument();
			root.getPrivateDocument().createQuality().setIssue(issue);
			root.getPublicDocument().createQuality().setIssue(issue);
		}
		return Utils.singleEntryList(root);
	}

	private DwRoot createRoot(GISLayerData data, Qname source) throws CriticalParseFailure {
		Qname documentId = parseDocumentId(data);
		DwRoot root = new DwRoot(documentId, source);
		root.setCollectionId(data.collectionId);
		return root;
	}

	private Qname parseDocumentId(GISLayerData data) {
		if (given(data.idPrefix)) {
			return Qname.fromURI(data.collectionId.toURI() + "/" + data.idPrefix + id(data.json));
		}
		return Qname.fromURI(data.collectionId.toURI() + "/" + id(data.json));
	}

	private String id(JSONObject data) {
		return data.getObject("properties").getString("id");
	}

	private void parseRoot(JSONObject json, DwRoot root) throws CriticalParseFailure {
		parseDocument(json, root.createPrivateDocument());
		parseDocument(json, root.createPublicDocument());
	}

	private void parseDocument(JSONObject json, Document d) throws CriticalParseFailure {
		JSONObject properties = json.getObject("properties");
		Gathering g = new Gathering(new Qname(d.getDocumentId() + "_G"));
		Unit u = new Unit(new Qname(d.getDocumentId() + "_U"));
		d.addGathering(g);
		g.addUnit(u);

		d.addKeyword(id(json));

		try {
			JSONObject geoJson = json.getObject("geometry");
			geoJson.setString(Geo.CRS, Type.EUREF.toString());
			Geo geo = Geo.fromGeoJSON(geoJson)
					.validate()
					.convertComplex();
			g.setGeo(geo);
		} catch (DataValidationException e) {
			createGeoIssue(g, e);
		}

		if (!d.isPublic()) {
			d.setDataSource(s("data_source", properties));
		}
		parseEventDateTime(s("date_begin", properties), g);

		g.addTeamMember(s("team", properties));
		g.setLocality(s("locality", properties));

		if (!d.isPublic()) {
			String privateTeam = s("private_team", properties);
			String privateLocality = s("private_locality", properties);
			if (given(privateTeam)) {
				g.getTeam().clear();
				g.addTeamMember(privateTeam);
			}
			if (given(privateLocality)) {
				g.setLocality(privateLocality);
			}
		}

		g.addFact(new Qname("MY.coordinateNotes").toURI(), s("accuracy_notes", properties));
		g.addFact(new Qname("MY.habitatDescription").toURI(), s("habitat_description", properties));

		g.setNotes(notes("accuracy_notes", properties, g.getNotes()));
		g.setNotes(notes("habitat_description", properties, g.getNotes()));

		String finnishName = s("finnish_name", properties);
		String scientificName = s("scientific_name", properties);
		u.setTaxonVerbatim(given(scientificName) ? scientificName : finnishName);

		Qname taxonId = new Qname(s("taxon_id", properties));
		u.setReportedTaxonId(taxonId);

		String recordBasis = s("record_base", properties);
		u.setRecordBasis(getRecordBasis(recordBasis));
		u.addFact(new Qname("MY.recordBasis").toURI(), recordBasis);
		u.setAbundanceString(s("abundance_string", properties));
		if (!d.getCollectionId().equals(HelsinkiPullReader.HELSINKI_INVASIVE_COLL_ID) && "0".equals(u.getAbundanceString())) { // TODO For now Tampere data uses 0 for missing value -- this maybe will be fixed in their end at some point; if this harmonizer is used for some other data than Tampere, removing zeros has to be moved to Tampere Pull Reader
			u.setAbundanceString(null);
		}
		if (given(u.getAbundanceString())) {
			u.setAbundanceUnit(getAbundanceUnit(recordBasis));
		}

		u.setLifeStage(getLifeStage(s("lifestage", properties)));

		u.addFact(new Qname("MY.decayStage").toURI(), s("decaystage", properties));
		u.addFact(new Qname("MY.substrateSpecies").toURI(), s("substrate", properties));
		u.addFact(new Qname("MY.substrateNotes").toURI(), s("substratenotes", properties));
		u.addFact(new Qname("MY.substrateSpeciesID").toURI(), s("substratespeciesinformation", properties));
		u.addFact(new Qname("MY.lifeStageDescription").toURI(), s("lifestage", properties));

		if (d.isPublic()) {
			u.setNotes(s("notes", properties));
		} else {
			u.setNotes(s("private_notes", properties));
		}

		try {
			String v = s("invasive_control", properties);
			if (v != null) {
				if (!d.getCollectionId().equals(HelsinkiPullReader.HELSINKI_INVASIVE_COLL_ID)) {
					Integer.valueOf(v);
					u.addSourceTag(Tag.INVASIVE_FULL);
				}
				u.setNotes(notes("Torjuttu " + v, u.getNotes()));
			}
		} catch (Exception e) {}
	}

	private static final Map<String, AbundanceUnit> RECORD_B_TO_ABUNDANCE_UNIT;
	static {
		RECORD_B_TO_ABUNDANCE_UNIT = new HashMap<>();
		RECORD_B_TO_ABUNDANCE_UNIT.put("SN", AbundanceUnit.INDIVIDUAL_COUNT); // suora näköhavainto
		RECORD_B_TO_ABUNDANCE_UNIT.put("SJ", AbundanceUnit.DROPPINGS); // suora jätöshavainto
		RECORD_B_TO_ABUNDANCE_UNIT.put("EJ", AbundanceUnit.DROPPINGS); // epäsuora jätöshavainto
		RECORD_B_TO_ABUNDANCE_UNIT.put("SP", AbundanceUnit.NESTS); // suora pesähavainto
		RECORD_B_TO_ABUNDANCE_UNIT.put("EP", AbundanceUnit.NESTS); // epäsuora pesähavainto
		RECORD_B_TO_ABUNDANCE_UNIT.put("SR", AbundanceUnit.FEEDING_SITES); // suora ruokailupaikka
		RECORD_B_TO_ABUNDANCE_UNIT.put("ER", AbundanceUnit.FEEDING_SITES); // epäsuora ruokailupaikka
		RECORD_B_TO_ABUNDANCE_UNIT.put("SK", AbundanceUnit.INDIVIDUAL_COUNT); // suora kuulohavainto
	}

	private AbundanceUnit getAbundanceUnit(String recordBasis) {
		return RECORD_B_TO_ABUNDANCE_UNIT.get(recordBasis);
	}

	private String notes(String notes, String originalNotes) {
		if (notes == null) return originalNotes;
		if (originalNotes == null) return notes;
		return originalNotes + "; " + notes;
	}

	private String notes(String field, JSONObject properties, String originalNotes) {
		return notes(s(field, properties), originalNotes);
	}

	private String s(String field, JSONObject properties) {
		if (!properties.hasKey(field)) return null;
		String v = properties.getString(field);
		if (!given(v)) return null;
		return v;
	}

}
