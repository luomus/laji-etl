package fi.laji.datawarehouse.etl.models;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.ParallelUtil;
import fi.laji.datawarehouse.etl.utils.ParallelUtil.Task;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class Securer {

	private static final String TAXON_FIELD_NATURA_AREA_SECURE_LEVEL = "naturaAreaSecureLevel";
	private static final String TAXON_FIELD_NEST_SITE_SECURE_LEVEL = "nestSiteSecureLevel";
	private static final String TAXON_FIELD_WINTERING_SECURE_LEVEL = "winteringSecureLevel";
	private static final String TAXON_FIELD_BREEDING_SECURE_LEVEL = "breedingSecureLevel";
	private static final String TAXON_FIELD_SECURE_LEVEL = "secureLevel";

	public enum SecureLevel { NOSHOW, HIGHEST, KM100, KM50, KM25, KM10, KM5, KM1, NONE;  // These must be in descending conceal order

		public static SecureLevel maxOf(SecureLevel level1, SecureLevel level2) {
			if (level1 == null) return level2;
			if (level2 == null) return level1;
			for (SecureLevel secureLevel : SecureLevel.values()) {
				if (secureLevel == level1) return level1;
				if (secureLevel == level2) return level2;
			}
			throw new IllegalStateException();
		}
	}

	public enum SecureReason {
		DEFAULT_TAXON_CONSERVATION, BREEDING_SITE_CONSERVATION,
		NATURA_AREA_CONSERVATION, WINTER_SEASON_TAXON_CONSERVATION,
		BREEDING_SEASON_TAXON_CONSERVATION, CUSTOM,
		USER_HIDDEN, ADMIN_HIDDEN, DATA_QUARANTINE_PERIOD, ONLY_PRIVATE,
		USER_PERSON_NAMES_HIDDEN, USER_HIDDEN_LOCATION, USER_HIDDEN_TIME
	}

	private static final Set<Integer> WINTERING_SEASON_MONTHS = Utils.set(11, 12, 1, 2, 3);
	private static final Set<Integer> DEFAULT_BREEDING_SEASON_MONTHS = Utils.set(4, 5, 6, 7, 8);

	private static final Map<Qname, SecureLevel> PRIVATE_SECURED_TAXA;
	static {
		PRIVATE_SECURED_TAXA = new HashMap<>();
		PRIVATE_SECURED_TAXA.put(new Qname("MX.26825"), SecureLevel.HIGHEST); // tunturihaukka
		PRIVATE_SECURED_TAXA.put(new Qname("MX.28987"), SecureLevel.HIGHEST); // tunturipöllö
		PRIVATE_SECURED_TAXA.put(new Qname("MX.46542"), SecureLevel.KM25);  // naali
	}

	/**
	 * This is the area inside which we do securing.
	 * Java doesn't have double polygon so we do this by multiplying coordinates by 100. So 59.51 ~ 5951.
	 */
	private static final Polygon SECURE_AREA = initSecureAreaPolygon();

	private static final int SECURE_AREA_FIRST_LON = 6965;
	private static final int SECURE_AREA_FIRST_LAT = 4464;

	private static Polygon initSecureAreaPolygon() {
		Polygon secureArea = new Polygon();
		secureArea.addPoint(SECURE_AREA_FIRST_LAT, SECURE_AREA_FIRST_LON);
		secureArea.addPoint(5312, 3102);
		secureArea.addPoint(5274, 1063);
		secureArea.addPoint(5746,  105);
		secureArea.addPoint(7034,  694);
		secureArea.addPoint(7291, 2821);
		secureArea.addPoint(SECURE_AREA_FIRST_LAT, SECURE_AREA_FIRST_LON);
		return secureArea;
	}

	private boolean isInsideSecureAreaWgs84(Coordinates wgs84Coordinates) {
		double latmin = wgs84Coordinates.getLatMin()*100; // Java doesn't have double polygon so area is in ints multiplied by 100
		double latmax = wgs84Coordinates.getLatMax()*100;
		double lonmin = wgs84Coordinates.getLonMin()*100;
		double lonmax = wgs84Coordinates.getLonMax()*100;
		if (SECURE_AREA.contains(latmin, lonmin)) return true;
		if (SECURE_AREA.contains(latmin, lonmax)) return true;
		if (SECURE_AREA.contains(latmax, lonmin)) return true;
		if (SECURE_AREA.contains(latmax, lonmax)) return true;
		return false;
	}

	private boolean isSurroundingCountry(Qname country) {
		return Const.COUNTRIES_SURROUNDING_FINLAND.contains(country);
	}

	public static SecureReason toPublicSecureReason(SecureReason secureReason) {
		if (secureReason == SecureReason.BREEDING_SITE_CONSERVATION) {
			return SecureReason.DEFAULT_TAXON_CONSERVATION;
		}
		if (secureReason == SecureReason.NATURA_AREA_CONSERVATION) {
			return SecureReason.DEFAULT_TAXON_CONSERVATION;
		}
		return secureReason;
	}

	private final DAO dao;
	private final SecureLevelAndReasonTask task = new SecureLevelAndReasonTask();
	private ParallelUtil<Pair<Gathering, Map<Qname, SecureLevelAndReasons>>> parallel;

	public Securer(DAO dao) {
		this.dao = dao;
	}

	private static class SecureLevelAndReasons {
		private SecureLevel level;
		private final Set<SecureReason> reasons = new HashSet<>();
		public SecureLevelAndReasons(SecureLevel level, SecureReason reason) {
			this.level = level;
			addReason(reason);
		}
		public SecureLevelAndReasons addReason(SecureReason reason) {
			if (reason != null) {
				reasons.add(reason);
			}
			return this;
		}
		public boolean isHighest() {
			return level == SecureLevel.NOSHOW || level == SecureLevel.HIGHEST;
		}
		@Override
		public String toString() {
			return level + " " + reasons;
		}
	}

	public void secure(DwRoot dwRoot) {
		securePublic(dwRoot);
		securePrivate(dwRoot);
	}

	private void securePrivate(DwRoot dwRoot) {
		Document privateDocument = dwRoot.getPrivateDocument();
		if (privateDocument == null) {
			// There is no private version of the document.
			// If there would be anything to secure in the original document, there WOULD be a private version.
			// Thus, there is nothing to do here -> private dw will contain the public document as it is.
			return;
		}

		if (SecureLevel.NOSHOW == privateDocument.getSecureLevel()) {
			delete(dwRoot);
			return;
		}

		TreeSet<SecureLevel> secureLevels = getSpeciesSecureLevels(privateDocument);
		if (secureLevels.isEmpty()) return;
		SecureLevel highestSpeciesLevel = secureLevels.iterator().next();
		if (highestSpeciesLevel == SecureLevel.NONE) return;

		if (secureLevels.size() == 1) {
			// All secured with same secure level
			Document securedPrivate = privateDocument.concealPublicData(highestSpeciesLevel, Utils.set(SecureReason.DEFAULT_TAXON_CONSERVATION));
			securedPrivate.setConcealment(Concealment.PRIVATE);
			dwRoot.setPrivateDocument(securedPrivate);
			return;
		}

		List<Gathering> splittedGatherings = new ArrayList<>();
		for (Gathering gathering : privateDocument.getGatherings()) {
			Iterator<Unit> i = gathering.getUnits().iterator();
			while (i.hasNext()) {
				Unit unit = i.next();
				SecureLevel level = getPrivateSecureLevel(unit, gathering);
				if (level == SecureLevel.NONE) continue;
				i.remove();

				Gathering secured = gathering.copy();
				secured.getUnits().clear();
				secured.addUnit(unit);
				secured = secured.concealPublicData(level, Utils.set(SecureReason.DEFAULT_TAXON_CONSERVATION), privateDocument.getSourceId());
				try {
					secured.setGatheringId(new Qname(securedGatheringId(gathering, unit)));
				} catch (CriticalParseFailure e) {
					throw new ETLException("Securing private causes invalid gathering id: "  + securedGatheringId(gathering, unit));
				}
				splittedGatherings.add(secured);
			}
		}
		privateDocument.getGatherings().removeIf(g->g.getUnits().isEmpty());
		splittedGatherings.forEach(privateDocument::addGathering);
	}

	private String securedGatheringId(Gathering gathering, Unit unit) {
		return (gathering.getGatheringId().toString()+"_"+unit.getUnitId().toString()).replace("#", "_");
	}

	private TreeSet<SecureLevel> getSpeciesSecureLevels(Document privateDocument) {
		TreeSet<SecureLevel> secureLevels = new TreeSet<>();
		for (Gathering g : privateDocument.getGatherings()) {
			for (Unit u : g.getUnits()) {
				secureLevels.add(getPrivateSecureLevel(u, g));
			}
		}
		return secureLevels;
	}

	private SecureLevel getPrivateSecureLevel(Unit unit, Gathering gathering) {
		Set<Taxon> taxa = getTaxa(unit, gathering);
		SecureLevel max = SecureLevel.NONE;
		for (Taxon taxon  : taxa) {
			if (PRIVATE_SECURED_TAXA.containsKey(taxon.getId())) {
				SecureLevel level = PRIVATE_SECURED_TAXA.get(taxon.getId());
				max = maxOf(max, level);
				if (Boolean.TRUE.equals(unit.isBreedingSite())) max = SecureLevel.HIGHEST;
			}
		}
		return max;
	}

	private void securePublic(DwRoot dwRoot) {
		Document publicDocument = dwRoot.getPublicDocument();
		if (publicDocument == null) {
			dwRoot.getPrivateDocument().addSecureReason(SecureReason.ONLY_PRIVATE);
			return; // Only private document provided: no need to secure public
		}

		// Copying must be done now before making any changes to the public document (copy is possibly needed)
		Document privateDocument = getPrivateOrCopyPublic(dwRoot);

		// Rings are secured so that "ringers can not try to guess rings of recoveries based on data/other recoveries"
		boolean individualIdsWereSecured = secureIndividualIds(publicDocument);

		boolean issuesWereSecured = secureIssuesIfCustomSecured(publicDocument);

		// Has provided document been marked to be secured at certain level?
		SecureLevel customDocumentSecureLevel = publicDocument.getSecureLevel();
		if (customDocumentSecureLevel == SecureLevel.NOSHOW) {
			delete(dwRoot);
			return;
		}
		// Document may need to be secured because the collection has a data quarantine period
		SecureLevel dataQuarantineSecureLevel = determineDataQuarantineSecureLevel(publicDocument);

		SecureLevel collectionSecureLevel = determineCollectionSecureLevel(publicDocument);

		SecureLevel factBasedSecureLevel = determineFactBasedSecureLevel(publicDocument);

		// This is the highest document based secure level. Individual units may get a higher secure level.
		SecureLevel maxDocumentBasedSecureLevel = maxOf(customDocumentSecureLevel, dataQuarantineSecureLevel, factBasedSecureLevel, collectionSecureLevel);

		// Secure level and reasons of each unit (id -> secureLevelAndReason)
		Map<Qname, SecureLevelAndReasons> unitSecureLevelsAndReasons = determineUnitSecureLevelsAndReasons(publicDocument);

		// This is the highest unit based secure level
		SecureLevel maxUnitBasedSecureLevel = getMaxSecureLevel(unitSecureLevelsAndReasons);

		// This is max of document and unit based secure levels
		SecureLevel maxOfBoth = maxOf(maxUnitBasedSecureLevel, maxDocumentBasedSecureLevel);

		if (maxOfBoth == SecureLevel.NOSHOW) {
			delete(dwRoot);
			return; // Delete entire document from public and private
		}

		if (maxOfBoth == SecureLevel.NONE) {
			if (individualIdsWereSecured || issuesWereSecured) {
				// There was something done to the public document. Set unaltered copy of public or the original private document as the private document
				dwRoot.setPrivateDocument(privateDocument);
				// but nothing further to secure in public document
				return;
			}
			// Nothing was done to public document and there is nothing to secure
			// If private document was not set, leave private empty (public will be stored to private)
			return;
		}

		// Secure level is higher than NONE

		Set<SecureReason> reasons = determineReasons(publicDocument, customDocumentSecureLevel, dataQuarantineSecureLevel, collectionSecureLevel, factBasedSecureLevel, unitSecureLevelsAndReasons.values());

		// Set original private document or copy of unaltered public as private document and add secure reasons also to private document
		for (SecureReason r : reasons) {
			privateDocument.addSecureReason(r);
		}
		dwRoot.setPrivateDocument(privateDocument);

		if (maxOfBoth == maxDocumentBasedSecureLevel || unitSecureLevelsAndReasons.size() == 1) {
			// Document based secure level is as high or higher as highest unit based level or there is only one unit in the document.
			//   --> Entire document can be secured using secure level of the document (no need to split units to separated documents).
			dwRoot.setPublicDocument(publicDocument.concealPublicData(maxOfBoth, reasons)); // Conceal returns a copy which is now set to the root
			return;
		}

		// Some of the units of the document need to be split to separate documents

		publicDocument.setPartial(true);

		Set<SecureReason> documentBasedReasons = determineReasons(publicDocument, customDocumentSecureLevel, dataQuarantineSecureLevel, collectionSecureLevel, factBasedSecureLevel);

		List<SplittedData> unitsToSplit = new ArrayList<>();
		Iterator<Gathering> gatherings = publicDocument.getGatherings().iterator();
		while (gatherings.hasNext()) {
			Gathering gathering = gatherings.next();
			Iterator<Unit> units = gathering.getUnits().iterator();
			boolean removedUnit = false;
			while (units.hasNext()) {
				Unit unit = units.next();
				SecureLevelAndReasons unitSecure = unitSecureLevelsAndReasons.get(unit.getUnitId());
				if (unitSecure.level == SecureLevel.NONE) {
					// This unit does not need securing: it can remain in the original document
					continue;
				}
				if (maxOf(unitSecure.level, maxDocumentBasedSecureLevel) == maxDocumentBasedSecureLevel) {
					// This unit must be concealed at the same level or lower level as the document: it can remain in the original document
					continue;
				}

				// This unit must be split from the original document
				units.remove(); // remove from original document
				unitsToSplit.add(new SplittedData(unitSecure, gathering, unit));
				removedUnit = true;
			}
			if (removedUnit && gathering.getUnits().isEmpty()) {
				// All units were split from the gathering; remove also the gathering
				gatherings.remove();
			}
		}

		// Prefetch all splitted document ids
		SplittedDocumentIds splittedDocumentIds = getSplittedDocumentIds(publicDocument, unitsToSplit);

		Document baseForSplit = publicDocument.copy(Concealment.PUBLIC);
		baseForSplit.getGatherings().clear();

		for (SplittedData splitted : unitsToSplit) {
			// Create splitted document, conceal it and add to root
			Qname splittedDocumentId = splittedDocumentIds.getSplittedDocumentIdFor(splitted.unit.getUnitId());
			if (splittedDocumentId == null) throw failedPrefetch(publicDocument, splitted.unit);
			dwRoot.addSplittedPublicDocument(
					split(baseForSplit, splitted.gathering, splitted.unit, splittedDocumentId)
					.concealPublicData(splitted.unitSecure.level, union(splitted.unitSecure.reasons, documentBasedReasons)));
		}

		if (maxDocumentBasedSecureLevel != SecureLevel.NONE) {
			// Conceal document with the remaining (unsplitted) documents using document based secure level
			dwRoot.setPublicDocument(publicDocument.concealPublicData(maxDocumentBasedSecureLevel, reasons)); // Conceal returns a copy which is now set to the root
		}
	}

	private SecureLevel determineFactBasedSecureLevel(Document publicDocument) {
		boolean insideFinlandOrClose = false;
		boolean winteringSeason = false;
		boolean naturaArea = false;
		Set<Integer> gatheringMonths = new HashSet<>();
		for (Gathering gathering : publicDocument.getGatherings()) {
			if (insideFinlandOrClose(gathering)) insideFinlandOrClose = true;
			if (isWinteringSeason(gathering)) winteringSeason = true;
			if (inNaturaArea(gathering)) naturaArea = true;
			gatheringMonths.addAll(getMonths(gathering));
		}

		SecureLevelAndReasons levelAndReason = new SecureLevelAndReasons(SecureLevel.NONE, null);
		determineFactBasedSecurelevel(publicDocument.getFacts(), gatheringMonths, winteringSeason, naturaArea, null, levelAndReason);

		for (Gathering gathering : publicDocument.getGatherings()) {
			determineFactBasedSecurelevel(gathering.getFacts(),
					getMonths(gathering),
					isWinteringSeason(gathering),
					inNaturaArea(gathering),
					coordinates(gathering),
					levelAndReason);
		}

		if (shouldSecure(insideFinlandOrClose, false, levelAndReason)) {
			return levelAndReason.level;
		}
		return SecureLevel.NONE;
	}

	private void determineFactBasedSecurelevel(List<Fact> facts, Collection<Integer> gatheringMonths, boolean winteringSeason, boolean naturaArea, Coordinates coordinates, SecureLevelAndReasons levelAndReason) {
		for (Fact fact : facts) {
			if (shouldDetermineTaxon(fact)) {
				determineFactBasedSecureLevel(fact.getValue(), gatheringMonths, winteringSeason, naturaArea, coordinates, levelAndReason);
			}
		}
	}

	private static final Set<String> WHITELIST_FACTS = Utils.set(
			new Qname("MY.locality").toURI(),
			new Qname("MY.administrativeProvince").toURI(),
			new Qname("MY.biologicalProvince").toURI(),
			new Qname("MY.country").toURI(),
			new Qname("MY.county").toURI(),
			new Qname("MY.higherGeography").toURI(),
			new Qname("MY.localityVerbatim").toURI(),
			new Qname("MY.municipality").toURI(),
			new Qname("MY.province").toURI());

	private boolean shouldDetermineTaxon(Fact fact) {
		if (fact.getValue().length() <= 4) return false;
		if (fact.getValue().length() > 30) return false;
		if (WHITELIST_FACTS.contains(fact.getFact())) return false;
		if (isMunicipality(fact.getValue())) return false;
		return true;
	}

	private boolean isMunicipality(String value) {
		try {
			return dao.resolveMunicipalitiesByName(value).size() > 0;
		} catch (Exception e) {
			dao.getErrorReporter().report("is municipality for string " + value, e);
			return false;
		}
	}

	private void determineFactBasedSecureLevel(String value, Collection<Integer> gatheringMonths, boolean winteringSeason, boolean naturaArea, Coordinates coordinates, SecureLevelAndReasons levelAndReason) {
		Set<Taxon> taxa = cleanAndAddParents(dao.getAllTaxonMatches(value));
		if (taxa.isEmpty()) return;
		boolean breedingSeason = isBreedingSeason(gatheringMonths, taxa);
		determineSecureLevel(taxa, breedingSeason, winteringSeason, naturaArea, true, coordinates, levelAndReason);
	}

	private void delete(DwRoot dwRoot) {
		Document deletedDocument = Document.createDeletedDocument(dwRoot);
		dwRoot.setPublicDocument(deletedDocument);
		dwRoot.setPrivateDocument(deletedDocument);
		dwRoot.setDeleteRequest(true);
	}

	private boolean secureIssuesIfCustomSecured(Document publicDocument) {
		if (publicDocument.getSecureReasons().isEmpty()) return false;
		boolean securedIssues = false;
		for (Gathering g : publicDocument.getGatherings()) {
			if (g.getQuality() == null) continue;
			if (g.getQuality().hasIssues()) {
				g.setQuality(g.getQuality().conseal());
				securedIssues = true;
			}
		}
		return securedIssues;
	}

	private SplittedDocumentIds getSplittedDocumentIds(Document document, List<SplittedData> unitsToSplit) {
		return dao.getETLDAO().getOrGenerateSplittedDocumentIds(document.getDocumentId(), getUnitIds(unitsToSplit));
	}

	private List<Qname> getUnitIds(List<SplittedData> unitsToSplit) {
		return unitsToSplit.stream().map(u->u.unit.getUnitId()).collect(Collectors.toList());
	}

	private static class SplittedData {
		private final SecureLevelAndReasons unitSecure;
		private final Gathering gathering;
		private final Unit unit;
		public SplittedData(SecureLevelAndReasons unitSecure, Gathering gathering, Unit unit) {
			this.unitSecure = unitSecure;
			this.gathering = gathering;
			this.unit = unit;
		}
	}

	private Set<SecureReason> union(Set<SecureReason> unitReasons, Set<SecureReason> documentBasedReasons) {
		Set<SecureReason> reasons = new HashSet<>(documentBasedReasons);
		reasons.addAll(unitReasons);
		return reasons;
	}

	private Document split(Document document, Gathering gathering, Unit unit, Qname splittedDocumentId) {
		Document splittedDocument = document.copy(Concealment.PUBLIC);
		Gathering splittedGathering = gathering.copy();
		splittedGathering.getUnits().clear();
		splittedDocument.addGathering(splittedGathering);
		splittedGathering.addUnit(unit);

		Qname splittedGatheringId = new Qname(splittedDocumentId.toString()+"#G");
		Qname splittedUnitId = new Qname(splittedDocumentId.toString()+"#U");
		try {
			splittedDocument.setDocumentId(splittedDocumentId);
			splittedGathering.setGatheringId(splittedGatheringId);
			unit.setUnitId(splittedUnitId);
		} catch (CriticalParseFailure e) {
			// Impossible state
			throw new ETLException(e);
		}
		return splittedDocument;
	}

	private ETLException failedPrefetch(Document document, Unit unit) {
		return new ETLException("Prefetching splitted document id for document id " +
				document.getDocumentId().toURI() + " and unit id " + unit.getUnitId().toURI() + " has failed");
	}

	private Set<SecureReason> determineReasons(Document publicDocument, SecureLevel customDocumentSecureLevel, SecureLevel dataQuarantineSecureLevel,  SecureLevel collectionSecureLevel, SecureLevel factBasedSecureLevel) {
		return determineReasons(publicDocument, customDocumentSecureLevel, dataQuarantineSecureLevel, collectionSecureLevel, factBasedSecureLevel, null);
	}

	private Set<SecureReason> determineReasons(Document publicDocument, SecureLevel customDocumentSecureLevel, SecureLevel dataQuarantineSecureLevel, SecureLevel collectionSecureLevel, SecureLevel factBasedSecureLevel, Collection<SecureLevelAndReasons> unitBasedSecureLevelsAndReasons) {
		Set<SecureReason> reasons = new HashSet<>();
		reasons.addAll(publicDocument.getSecureReasons());
		if (unitBasedSecureLevelsAndReasons != null) {
			for (SecureLevelAndReasons unitSecure : unitBasedSecureLevelsAndReasons) {
				if (unitSecure.level != SecureLevel.NONE) {
					reasons.addAll(unitSecure.reasons);
				}
			}
		}
		if (customDocumentSecureLevel != SecureLevel.NONE) {
			if (reasons.isEmpty()) {
				reasons.add(SecureReason.CUSTOM);
			}
		}
		if (dataQuarantineSecureLevel != SecureLevel.NONE) {
			reasons.add(SecureReason.DATA_QUARANTINE_PERIOD);
		}
		if (collectionSecureLevel != SecureLevel.NONE) {
			reasons.add(SecureReason.CUSTOM);
		}
		if (factBasedSecureLevel != SecureLevel.NONE) {
			reasons.add(SecureReason.DEFAULT_TAXON_CONSERVATION);
		}
		return reasons;
	}

	private SecureLevel getMaxSecureLevel(Map<Qname, SecureLevelAndReasons> unitSecureLevelsAndReasons) {
		SecureLevel maxUnitBasedSecureLevel = SecureLevel.NONE;
		for (SecureLevelAndReasons s : unitSecureLevelsAndReasons.values()) {
			maxUnitBasedSecureLevel = maxOf(maxUnitBasedSecureLevel, s.level);
		}
		return maxUnitBasedSecureLevel;
	}

	private Map<Qname, SecureLevelAndReasons> determineUnitSecureLevelsAndReasons(Document publicDocument) {
		if (publicDocument.getGatherings() == null || publicDocument.getGatherings().isEmpty()) return Collections.emptyMap();

		if (publicDocument.getGatherings().size() == 1) {
			Gathering gathering = publicDocument.getGatherings().get(0);
			Map<Qname, SecureLevelAndReasons> map = new HashMap<>();
			determineUnitSecureLevelsAndReasons(gathering, map);
			return map;
		}
		return determineUnitSecureLevelsAndReasonsInParallel(publicDocument);
	}

	private void determineUnitSecureLevelsAndReasons(Gathering gathering, Map<Qname, SecureLevelAndReasons> secureLevelsAndReasons) {
		boolean insideFinlandOrClose = insideFinlandOrClose(gathering);
		boolean winteringSeason = isWinteringSeason(gathering);
		boolean naturaArea = inNaturaArea(gathering);
		for (Unit unit : gathering.getUnits()) {
			if (secureLevelsAndReasons.containsKey(unit.getUnitId())) throw new ETLException("Unit id twice in same document: " + unit.getUnitId());
			SecureLevelAndReasons secureLevelAndReason = determineSecureLevelAndReason(unit, gathering, winteringSeason, naturaArea);
			if (shouldSecure(insideFinlandOrClose, isBreedingSite(unit), secureLevelAndReason)) {
				// inside or close to Finland or marked as breeding site - highest secure level taxa always secured
				secureLevelsAndReasons.put(unit.getUnitId(), secureLevelAndReason);
			} else {
				// we will not secure regardless of secure level (outside of secure area and not breeding site and not highest secure level)
				secureLevelsAndReasons.put(unit.getUnitId(), new SecureLevelAndReasons(SecureLevel.NONE, null));
			}
		}
	}

	private class SecureLevelAndReasonTask implements Task<Pair<Gathering, Map<Qname, SecureLevelAndReasons>>> {
		@Override
		public void execute(Pair<Gathering, Map<Qname, SecureLevelAndReasons>> datas) throws Exception {
			Gathering gathering = datas.getKey();
			Map<Qname, SecureLevelAndReasons> secureLevelsAndReasons = datas.getValue();
			determineUnitSecureLevelsAndReasons(gathering, secureLevelsAndReasons);
		}
	}

	private Map<Qname, SecureLevelAndReasons> determineUnitSecureLevelsAndReasonsInParallel(Document publicDocument) {
		Map<Qname, SecureLevelAndReasons> secureLevelsAndReasons = new ConcurrentHashMap<>();
		if (parallel == null) {
			parallel = new ParallelUtil<>(dao.getSharedThreadPool());
		}
		parallel.execute(task, parallelDatas(publicDocument, secureLevelsAndReasons));
		return secureLevelsAndReasons;
	}

	private List<Pair<Gathering, Map<Qname, SecureLevelAndReasons>>> parallelDatas(Document publicDocument, Map<Qname, SecureLevelAndReasons> secureLevelsAndReasons) {
		List<Pair<Gathering, Map<Qname, SecureLevelAndReasons>>> datas = new ArrayList<>();
		for (Gathering gathering : publicDocument.getGatherings()) {
			datas.add(new Pair<>(gathering, secureLevelsAndReasons));
		}
		return datas;
	}

	private boolean inNaturaArea(Gathering gathering) {
		if (gathering.getInterpretations() == null) return false;
		if (gathering.getInterpretations().getNaturaAreas() == null) return false;
		return !gathering.getInterpretations().getNaturaAreas().isEmpty();
	}

	private boolean shouldSecure(boolean inSecureArea, boolean breedingSite, SecureLevelAndReasons secureLevelAndReason) {
		if (secureLevelAndReason.isHighest()) return true; // always secure highest
		if (breedingSite && secureLevelAndReason.level != SecureLevel.NONE) return true; // always secure breeding sites of secured species even outside Finland
		return inSecureArea; // secure those that are inside defined secure area
	}

	private boolean insideFinlandOrClose(Gathering gathering) {
		if (gathering.getInterpretations() == null) {
			// no geo information: assuming is in secure area
			return true;
		}

		Coordinates coordinates = gathering.getInterpretations().getCoordinates();
		Qname countryId = gathering.getInterpretations().getCountry();
		if (coordinates != null) {
			return isInsideSecureArea(coordinates);
		}
		if (countryId != null) {
			if (Const.FINLAND.equals(countryId)) return true;
			return isSurroundingCountry(countryId);
		}

		// no coordinates or country: assuming is in secure area
		return true;
	}

	private boolean isInsideSecureArea(Coordinates coordinates) {
		Coordinates wgs84Coordinates = null;
		if (coordinates.getType() == Type.WGS84) {
			wgs84Coordinates = coordinates;
		} else {
			try {
				wgs84Coordinates = CoordinateConverter.convert(coordinates).getWgs84();
			} catch (Exception e) {
				return true; // this problem will be raised again in converter, we can ignore it here and assume that is in secure area for purposes of securing
			}
		}
		return isInsideSecureAreaWgs84(wgs84Coordinates);
	}

	private SecureLevelAndReasons determineSecureLevelAndReason(Unit unit, Gathering gathering, boolean winteringSeason, boolean naturaArea) {
		SecureLevelAndReasons levelAndReason = new SecureLevelAndReasons(SecureLevel.NONE, null);

		if (isUserHidden(unit)) {
			levelAndReason = new SecureLevelAndReasons(SecureLevel.KM100, SecureReason.USER_HIDDEN);
		}

		Set<Taxon> taxa = getTaxa(unit, gathering);
		if (taxa.isEmpty()) return levelAndReason;

		boolean isBreedingSite = isBreedingSite(unit);
		boolean breedingSeason = isBreedingSeason(gathering, taxa);

		return determineSecureLevel(taxa, breedingSeason, winteringSeason, naturaArea, isBreedingSite, coordinates(gathering), levelAndReason);
	}

	private Coordinates coordinates(Gathering gathering) {
		GatheringInterpretations i = gathering.getInterpretations();
		if (i == null) return null;
		if (!Const.FINLAND.equals(i.getCountry())) return null;
		return gathering.getInterpretations().getCoordinates();
	}

	private SecureLevelAndReasons determineSecureLevel(Set<Taxon> taxa, boolean breedingSeason, boolean winteringSeason, boolean naturaArea, boolean isBreedingSite, Coordinates coordinates, SecureLevelAndReasons levelAndReason) {
		for (Taxon taxon: taxa) {
			if (naturaArea) {
				determineSecureLevel(taxon, TAXON_FIELD_NATURA_AREA_SECURE_LEVEL, levelAndReason, SecureReason.NATURA_AREA_CONSERVATION);
			}

			if (hasCustomRules(taxon)) {
				determineCustomSecureLevel(taxon, breedingSeason, isBreedingSite, coordinates, levelAndReason);
			} else {
				determineSecureLevel(taxon, TAXON_FIELD_SECURE_LEVEL, levelAndReason, SecureReason.DEFAULT_TAXON_CONSERVATION);

				if (breedingSeason) {
					determineSecureLevel(taxon, TAXON_FIELD_BREEDING_SECURE_LEVEL, levelAndReason, SecureReason.BREEDING_SEASON_TAXON_CONSERVATION);
				}

				if (isBreedingSite) {
					determineSecureLevel(taxon, TAXON_FIELD_NEST_SITE_SECURE_LEVEL, levelAndReason, SecureReason.BREEDING_SITE_CONSERVATION);
				}
			}

			if (winteringSeason) {
				determineSecureLevel(taxon, TAXON_FIELD_WINTERING_SECURE_LEVEL, levelAndReason, SecureReason.WINTER_SEASON_TAXON_CONSERVATION);
			}
		}
		return levelAndReason;
	}

	private interface CustomRules {
		void determineCustomSecureLevel(boolean breedingSeason, boolean isBreedingSite, Integer ykj10kmLat, SecureLevelAndReasons levelAndReason);
	}

	private static final Map<Qname, CustomRules> CUSTOM_RULES;
	static {
		CUSTOM_RULES = new HashMap<>();

		// kuukkeli MX.37095
		CUSTOM_RULES.put(new Qname("MX.37095"), new CustomRules() {
			@Override
			public void determineCustomSecureLevel(boolean breedingSeason, boolean isBreedingSite, Integer ykj10kmLat, SecureLevelAndReasons levelAndReason) {
				if (ykj10kmLat == null || ykj10kmLat <= 710) addLevel(levelAndReason, SecureReason.DEFAULT_TAXON_CONSERVATION, SecureLevel.KM100);
			}
		});

		// suosirri MX.27699
		CUSTOM_RULES.put(new Qname("MX.27699"), new CustomRules() {
			@Override
			public void determineCustomSecureLevel(boolean breedingSeason, boolean isBreedingSite, Integer ykj10kmLat, SecureLevelAndReasons levelAndReason) {
				if (ykj10kmLat == null || ykj10kmLat <= 731) {
					if (breedingSeason) addLevel(levelAndReason, SecureReason.BREEDING_SEASON_TAXON_CONSERVATION, SecureLevel.KM25);
					if (isBreedingSite) addLevel(levelAndReason, SecureReason.BREEDING_SITE_CONSERVATION, SecureLevel.KM25);
				}
			}
		});

		// merikotka MX.26530
		CUSTOM_RULES.put(new Qname("MX.26530"), new CustomRules() {
			@Override
			public void determineCustomSecureLevel(boolean breedingSeason, boolean isBreedingSite, Integer ykj10kmLat, SecureLevelAndReasons levelAndReason) {
				SecureLevel level = SecureLevel.KM50;
				if (ykj10kmLat != null && ykj10kmLat < 703) level = SecureLevel.KM10;
				if (breedingSeason) addLevel(levelAndReason, SecureReason.BREEDING_SEASON_TAXON_CONSERVATION, level);
				if (isBreedingSite) addLevel(levelAndReason, SecureReason.BREEDING_SITE_CONSERVATION, level);
			}
		});
	}
	private boolean hasCustomRules(Taxon taxon) {
		return CUSTOM_RULES.containsKey(taxon.getId());
	}

	private void determineCustomSecureLevel(Taxon taxon, boolean breedingSeason, boolean isBreedingSite, Coordinates coordinates, SecureLevelAndReasons levelAndReason) {
		Integer ykj10kmLat = ykj10kmLat(coordinates);
		CUSTOM_RULES.get(taxon.getId()).determineCustomSecureLevel(breedingSeason, isBreedingSite, ykj10kmLat, levelAndReason);
	}

	private Integer ykj10kmLat(Coordinates coordinates) {
		if (coordinates == null) return null;
		if (coordinates.getType() != Type.YKJ) {
			try {
				coordinates = CoordinateConverter.convert(coordinates).getYkj();
			} catch (DataValidationException e) {
				return null;
			}
		}
		return Coordinates.convertYkj10kmCenter(coordinates).getLat().intValue();
	}
	private Set<Taxon> getTaxa(Unit unit, Gathering gathering) {
		Set<Qname> matchingTaxonIds = new HashSet<>();
		matchingTaxonIds.addAll(dao.getAllTaxonMatches(unit.getTaxonVerbatim()));
		matchingTaxonIds.add(unit.getReportedTaxonId());
		matchingTaxonIds.add(dao.getTaxonLinkingService().getTaxonId(unit, gathering, true));

		return cleanAndAddParents(matchingTaxonIds);
	}

	private Set<Taxon> cleanAndAddParents(Set<Qname> matchingTaxonIds) {
		matchingTaxonIds.remove(null);
		Set<Taxon> taxa = new HashSet<>();
		for (Qname taxonId : matchingTaxonIds) {
			if (!dao.hasTaxon(taxonId)) continue;
			Taxon taxon = dao.getTaxon(taxonId);
			taxa.add(taxon);
			while (taxon.isSpecies()) {
				taxa.add(taxon);
				if (!taxon.hasParent()) break;
				taxon = taxon.getParent();
			}
		}
		return taxa;
	}

	private boolean isBreedingSite(Unit unit) {
		return Boolean.TRUE.equals(unit.isBreedingSite());
	}

	public static Document getPrivateOrCopyPublic(DwRoot dwRoot) {
		if (dwRoot.getPrivateDocument() != null) {
			return dwRoot.getPrivateDocument();
		}
		Document privateDocument = dwRoot.getPublicDocument().copy(Concealment.PRIVATE);
		return privateDocument;
	}

	private boolean isUserHidden(Unit unit) {
		return isHashMarked(unit);
	}

	private boolean isHashMarked(Unit unit) {
		if (unit.getTaxonVerbatim() == null) return false;
		return unit.getTaxonVerbatim().contains("#");
	}

	private boolean secureIndividualIds(Document document) {
		boolean securedSomething = false;
		for (Gathering g : document.getGatherings()) {
			for (Unit u : g.getUnits()) {
				boolean securedThisUnit = secureIndividualId(u);
				if (securedThisUnit) {
					securedSomething = true;
				}
			}
		}
		return securedSomething;
	}

	private boolean secureIndividualId(Unit unit) {
		String individualIdURL = unit.getIndividualId();
		if (individualIdURL == null || individualIdURL.isEmpty()) return false;
		Qname individualId = Qname.fromURI(individualIdURL);
		unit.setIndividualIdUsingQname(secureIndividualId(individualId));
		return true;
	}

	private Qname secureIndividualId(Qname individualId) {
		return dao.getETLDAO().secureIndividualId(individualId);
	}

	private SecureLevel determineCollectionSecureLevel(Document publicDocument) {
		Qname collectionId = publicDocument.getCollectionId();
		CollectionMetadata collection = dao.getCollections().get(collectionId.toURI());
		if (collection == null) throw new ETLException("Collection metadata is missing for " + collectionId);
		return toSecureLevel(collection.getSecureLevel());
	}

	private SecureLevel determineDataQuarantineSecureLevel(Document publicDocument) {
		Qname collectionId = publicDocument.getCollectionId();
		CollectionMetadata collection = dao.getCollections().get(collectionId.toURI());
		if (collection == null) throw new ETLException("Collection metadata is missing for " + collectionId);
		Double quarantineInYears = collection.getDataQuarantinePeriod();
		if (quarantineInYears == null) return SecureLevel.NONE;

		Date latestEventDate = getLatestEventDate(publicDocument);
		if (latestEventDate == null) return SecureLevel.NONE;

		Date lastQuarantineDate = calculateLastQuarantineDate(quarantineInYears);
		if (latestEventDate.after(lastQuarantineDate)) {
			return hasAtlasCodes(publicDocument) ? SecureLevel.KM10 : SecureLevel.KM100;
		}
		return SecureLevel.NONE;
	}

	private boolean hasAtlasCodes(Document d) {
		for (Gathering g : d.getGatherings()) {
			for (Unit u : g.getUnits()) {
				if (u.getAtlasCode() != null || u.getAtlasClass() != null) return true;
			}
		}
		return false;
	}

	private Date calculateLastQuarantineDate(Double quarantineInYears) {
		return calculateLastQuarantineDate(quarantineInYears, new Date());
	}

	public static Date calculateLastQuarantineDate(Double quarantineInYears, Date from) {
		int fullYears = (int) Math.floor(quarantineInYears);
		double fractionOfYear = quarantineInYears - fullYears;
		int remainingDaysAbout = (int) Math.ceil(365*fractionOfYear);

		Calendar c = Calendar.getInstance();
		c.setTime(from);
		c.add(Calendar.YEAR, -1 * fullYears);
		c.add(Calendar.DAY_OF_YEAR, -1 * remainingDaysAbout);
		Date lastQuarantineDate = c.getTime();
		return lastQuarantineDate;
	}

	private Date getLatestEventDate(Document publicDocument) {
		Date latest = null;
		for (Gathering gathering : publicDocument.getGatherings()) {
			if (gathering.getEventDate() == null) continue;
			Date eventDate = gathering.getEventDate().getEnd();
			if (eventDate == null) eventDate = gathering.getEventDate().getBegin();
			if (eventDate == null) continue;
			if (latest == null || eventDate.after(latest)) {
				latest = eventDate;
			}
		}
		return latest;
	}

	private void determineSecureLevel(Taxon taxon, String secureReasonFieldName, SecureLevelAndReasons levelAndReason, SecureReason reason) {
		SecureLevel secureLevel = getSecureLevel(secureReasonFieldName, taxon);
		addLevel(levelAndReason, reason, secureLevel);
	}

	private static void addLevel(SecureLevelAndReasons levelAndReason, SecureReason reason, SecureLevel secureLevel) {
		SecureLevel max = maxOf(secureLevel, levelAndReason.level);
		if (max != levelAndReason.level) {
			levelAndReason.level = max;
		}
		if (secureLevel != SecureLevel.NONE) {
			levelAndReason.addReason(reason);
		}
	}

	private SecureLevel getSecureLevel(String secureReasonFieldName, Taxon taxon) {
		try {
			Qname secureLevelResourceId = (Qname) ReflectionUtil.getGetter(secureReasonFieldName, Taxon.class).invoke(taxon);
			SecureLevel secureLevel = toSecureLevel(secureLevelResourceId);
			return secureLevel;
		} catch (Exception e) {
			throw new ETLException("Reflection exception", e);
		}
	}

	private SecureLevel toSecureLevel(Qname secureLevel) {
		if (secureLevel == null) return SecureLevel.NONE;
		String s = secureLevel.toString().replace("MX.secureLevel", "").toUpperCase();
		SecureLevel enumLevel = SecureLevel.valueOf(s);
		return enumLevel;
	}

	private boolean isWinteringSeason(Gathering gathering) {
		return monthIn(WINTERING_SEASON_MONTHS, getMonths(gathering));
	}

	private boolean isBreedingSeason(Gathering gathering, Set<Taxon> taxa) {
		Collection<Integer> gatheringMonths = getMonths(gathering);
		return monthIn(getBreedingSeason(taxa), gatheringMonths);
	}

	private Collection<Integer> getMonths(Gathering gathering) {
		DateRange eventDate = gathering.getEventDate();
		if (eventDate == null) return Collections.emptyList();
		Collection<Integer> gatheringMonths = Util.getMonths(eventDate);
		return gatheringMonths;
	}

	private boolean isBreedingSeason(Collection<Integer> gatheringMonths, Set<Taxon> taxa) {
		return monthIn(getBreedingSeason(taxa), gatheringMonths);
	}

	private static final Map<Qname, List<Integer>> CUSTOM_BREEDING_SEASONS;
	static {
		CUSTOM_BREEDING_SEASONS = new HashMap<>();
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.26647"), Utils.list(3, 4, 5, 6, 7, 8)); // kanahaukka
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.26727"), Utils.list(2, 3, 4, 5, 6, 7, 8)); // maakotka
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.28965"), Utils.list(2, 3, 4, 5, 6, 7, 8)); // huuhkaja
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.30438"), Utils.list(3, 4, 5, 6, 7, 8)); // valkoselkätikka
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.28965"), Utils.list(2, 3, 4, 5, 6, 7, 8)); // huuhkaja
		CUSTOM_BREEDING_SEASONS.put(new Qname("MX.26530"), Utils.list(2, 3, 4, 5, 6, 7, 8)); // merikotka
	}

	private Set<Integer> getBreedingSeason(Set<Taxon> taxa) {
		Set<Integer> months = null;
		for (Taxon taxon : taxa) {
			List<Integer> season = CUSTOM_BREEDING_SEASONS.get(taxon.getId());
			if (season != null) {
				if (months == null) months = new HashSet<>();
				months.addAll(season);
			}
		}
		if (months != null) return months;
		return DEFAULT_BREEDING_SEASON_MONTHS;
	}

	private boolean monthIn(Set<Integer> months, Collection<Integer> gatheringMonths) {
		for (Integer gatheringMonth : gatheringMonths) {
			if (months.contains(gatheringMonth)) return true;
		}
		return false;
	}

	private static SecureLevel maxOf(SecureLevel level1, SecureLevel level2) {
		return SecureLevel.maxOf(level1, level2);
	}

	private SecureLevel maxOf(SecureLevel ... levels) {
		SecureLevel max = SecureLevel.NONE;
		for (SecureLevel l : levels) {
			max = SecureLevel.maxOf(max, l);
		}
		return max;
	}

}
