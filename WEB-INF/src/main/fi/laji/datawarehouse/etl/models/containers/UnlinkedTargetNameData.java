package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class UnlinkedTargetNameData {

	private final String targetName;
	private final Qname reference;
	private final Boolean notExactMatch;
	private final Qname taxonId;
	private final int count;

	public UnlinkedTargetNameData(String targetName, Qname reference, Boolean notExactMatch, Qname taxonId, int count) {
		this.targetName = targetName;
		this.reference = reference;
		this.notExactMatch = notExactMatch;
		this.taxonId = taxonId;
		this.count = count;
	}

	public String getTargetName() {
		return targetName;
	}

	public Qname getReference() {
		return reference;
	}

	public Boolean getNotExactMatch() {
		return notExactMatch;
	}

	public int getCount() {
		return count;
	}

	public Qname getTaxonId() {
		return taxonId;
	}

}
