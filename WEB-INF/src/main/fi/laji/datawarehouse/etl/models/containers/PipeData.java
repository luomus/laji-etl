package fi.laji.datawarehouse.etl.models.containers;

import java.util.Date;

import fi.luomus.commons.containers.rdf.Qname;

public abstract class PipeData {

	private final long id;
	private final Qname source;
	private final String data;
	private final Date timestamp;
	private final String errorMessage;
	protected int attemptCount = 0;

	public PipeData(long id, Qname source, String data) {
		this(id, source, data, null, null);
	}

	public PipeData(long id, Qname source, String data, Date timestamp, String errorMessage) {
		this.id = id;
		this.timestamp = timestamp;
		this.source = source;
		this.data = data;
		this.errorMessage = errorMessage;
	}

	public long getId() {
		return id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Qname getSource() {
		return source;
	}

	public String getData() {
		return data;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean isInError() {
		return errorMessage != null;
	}

	public int getAttemptCount() {
		return attemptCount;
	}

	@Override
	public String toString() {
		return "PipeData [id=" + id + ", source=" + source + ", data=" + data + ", timestamp=" + timestamp + ", errorMessage=" + errorMessage + ", attemptCount=" + attemptCount + "]";
	}

}
