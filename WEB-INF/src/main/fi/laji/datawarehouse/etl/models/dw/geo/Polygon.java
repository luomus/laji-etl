package fi.laji.datawarehouse.etl.models.dw.geo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.locationtech.jts.algorithm.Area;
import org.locationtech.jts.algorithm.Orientation;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;

public class Polygon extends Line {

	private static final double SQRT_OF_TWO = Math.sqrt(2);
	private static final int EARTH_RADIUS = 6378137;

	Polygon(Coordinate[] coordinates) {
		super(new GeometryFactory().createPolygon(coordinates));
	}

	Polygon(Geometry geometry) {
		super(geometry);
	}

	public static Feature from(int[] ... latLonPairs) {
		return from(toPoints(latLonPairs));
	}

	public static Feature from(double[] ... latLonPairs) {
		return from(toPoints(latLonPairs));
	}

	public static Feature from(List<Point> points) {
		List<Point> fixedPoints = fixPoints(points);
		if (fixedPoints.isEmpty()) throw new IllegalArgumentException("Feature must not be empty");
		if (fixedPoints.size() == 1) return fixedPoints.get(0);
		if (verySmallArea(fixedPoints)) return fixedPoints.get(0);
		if (fixedPoints.size() == 2) return Line.from(fixedPoints);

		Point first = fixedPoints.get(0);
		Point last = fixedPoints.get(fixedPoints.size()-1);
		if (!first.equals(last)) {
			fixedPoints.add(Point.from(first.getLat(), first.getLon()));
		}

		if (fixedPoints.size() < 4) return Line.from(fixedPoints);

		Coordinate[] c = toCoordinates(fixedPoints);
		if (pointsAreCollinear(c)) {
			return straightMaximumLine(c);
		}
		return new Polygon(c);
	}

	private static Feature straightMaximumLine(Coordinate[] c) {
		Coordinate startPoint = c[0];
		Coordinate endPoint = c[0];
		for (Coordinate coordinate : c) {
			if (coordinate.x < startPoint.x || (coordinate.x == startPoint.x && coordinate.y < startPoint.y)) {
				startPoint = coordinate;
			}
			if (coordinate.x > endPoint.x || (coordinate.x == endPoint.x && coordinate.y > endPoint.y)) {
				endPoint = coordinate;
			}
		}
		return new Line(new Coordinate[] {startPoint, endPoint});
	}

	public static boolean pointsAreCollinear(Coordinate[] coordinates) {
		for (int i = 0; i < coordinates.length - 2; i++) {
			if (Orientation.index(coordinates[i], coordinates[i + 1], coordinates[i + 2]) != 0) {
				return false;
			}
		}
		return true;
	}

	private static boolean verySmallArea(List<Point> fixedPoints) {
		double size = size(fixedPoints);
		if (isMetric(fixedPoints)) {
			if (size <= 3) return true;
		}
		return size < 0.00005;
	}

	private static double size(List<Point> points) {
		double minX = points.get(0).getLon();
		double maxX = points.get(0).getLon();
		double minY = points.get(0).getLat();
		double maxY = points.get(0).getLat();

		for (Point pair : points) {
			minX = Math.min(minX, pair.getLon());
			maxX = Math.max(maxX, pair.getLon());
			minY = Math.min(minY, pair.getLat());
			maxY = Math.max(maxY, pair.getLat());
		}

		double maxXDifference = maxX - minX;
		double maxYDifference = maxY - minY;

		return Math.max(maxXDifference, maxYDifference);
	}

	private static boolean isMetric(List<Point> fixedPoints) {
		for (Point p : fixedPoints) {
			if (p.getLat() > 200) return true;
			if (p.getLon() > 200) return true;
		}
		return false;
	}

	@Override
	public Polygon validate() throws DataValidationException {
		if (geometry.getCoordinates().length == 0) throw new DataValidationException("Polygon must not be empty");
		if (geometry.getCoordinates().length < 3) throw new DataValidationException("Polygon must have at least three points");
		if (!geometry.isValid()) {
			try {
				Polygon convexHull = convexHull();
				convexHull.validate();
				geometry = convexHull.geometry;
			} catch (DataValidationException | IllegalArgumentException e) {
				throw new DataValidationException("Polygon is not valid even as convex hull");
			}
		}
		if (!isClockWiseValidate()) {
			reverse();
		}
		return this;
	}

	private boolean isClockWiseValidate() throws DataValidationException {
		double orientation = Area.ofRingSigned(geometry.getCoordinates());
		if (orientation == 0) throw new DataValidationException("Polygon is not valid");
		return orientation > 0;
	}

	private void reverse() {
		geometry = geometry.reverse();
	}

	@Override
	public Polygon simplify(Type type) {
		return new Polygon(simplifyInternal(type, geometry));
	}

	@Override
	public JSONObject getGeoJSON() {
		JSONObject json = new JSONObject().setString("type", "Polygon");
		JSONArray coordinates = new JSONArray();
		json.getArray("coordinates").appendArray(coordinates);
		List<Point> points = getPoints();
		Collections.reverse(points);
		for (Point p : points) {
			coordinates.appendArray(p.getJsonCoordinatesArray());
		}
		return json;
	}

	public static Feature fromGeoJSON(JSONObject geometry) throws DataValidationException {
		JSONArray coordinates = geometry.getArray("coordinates").iterateAsArray().get(0);
		return fromCoordinateArray(coordinates);
	}

	public static Feature fromCoordinateArray(JSONArray coordinates) throws DataValidationException {
		List<JSONArray> coordinateArrays = coordinates.iterateAsArray();
		List<Point> polygonCoordinates = new ArrayList<>(coordinateArrays.size());
		for (JSONArray coordinatePairArray : coordinates.iterateAsArray()) {
			List<Double> coordinatePair = coordinatePairArray.iterateAsDouble();
			polygonCoordinates.add(Point.from(coordinatePair.get(1), coordinatePair.get(0)));
		}
		Collections.reverse(polygonCoordinates);
		return from(polygonCoordinates).validate();
	}

	public static Polygon fromGeoJSONPointWithRadius(JSONObject geometry, Type type) throws DataValidationException {
		Point point = (Point) Point.fromGeoJSON(geometry);
		double lat = point.getLat();
		double lon = point.getLon();
		double offsetInMeters = geometry.getDouble("radius");

		double offsetLat = calculateOffSetLat(offsetInMeters, type);
		double offsetLon = calculateOffsetLon(lat, offsetInMeters, type);

		double offsetHalfLat = offsetLat / SQRT_OF_TWO;
		double offsetHalfLon = offsetLon / SQRT_OF_TWO;

		Coordinate n = new Coordinate(lon, lat + offsetLat);
		Coordinate ne = new Coordinate(lon + offsetHalfLon, lat + offsetHalfLat);
		Coordinate e = new Coordinate(lon + offsetLon, lat);
		Coordinate se = new Coordinate(lon + offsetHalfLon, lat - offsetHalfLat);
		Coordinate s = new Coordinate(lon, lat - offsetLat);
		Coordinate sw = new Coordinate(lon - offsetHalfLon, lat- offsetHalfLat);
		Coordinate w = new Coordinate(lon - offsetLon, lat);
		Coordinate nw = new Coordinate(lon - offsetHalfLon, lat + offsetHalfLat);

		Coordinate[] coordinates = new Coordinate[9];
		coordinates[0] = n;
		coordinates[1] = ne;
		coordinates[2] = e;
		coordinates[3] = se;
		coordinates[4] = s;
		coordinates[5] = sw;
		coordinates[6] = w;
		coordinates[7] = nw;
		coordinates[8] = n;
		return new Polygon(coordinates);
	}

	private static double calculateOffsetLon(double lat, double offsetInMeters, Type type) {
		if (type == Type.WGS84) {
			double offsetRadLon = offsetInMeters / (EARTH_RADIUS * Math.cos(Math.toRadians(lat)));
			double offsetLon = Math.toDegrees(offsetRadLon);
			return offsetLon;
		}
		return offsetInMeters;
	}

	private static double calculateOffSetLat(double offsetInMeters, Type type) {
		if (type == Type.WGS84) {
			double offsetRadLat = offsetInMeters / EARTH_RADIUS;
			double offsetLat = Math.toDegrees(offsetRadLat);
			return offsetLat;
		}
		return offsetInMeters;
	}

}
