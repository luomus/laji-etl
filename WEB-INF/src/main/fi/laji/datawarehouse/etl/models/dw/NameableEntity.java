package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class NameableEntity extends BaseEntity {

	private String name;

	public String getName() {
		return name;
	}

	public NameableEntity setName(String name) {
		this.name = name;
		return this;
	}

}