package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;

@Embeddable
public class UnitInterpretations {

	public enum RecordQuality {	EXPERT_VERIFIED, COMMUNITY_VERIFIED, NEUTRAL, UNCERTAIN, ERRONEOUS }
	public enum Reliability { RELIABLE, UNDEFINED, UNRELIABLE }
	public enum InvasiveControl { FULL, PARTIAL, NO_EFFECT, NOT_FOUND }

	private int individualCount = 1;
	private Integer pairCount;

	private Qname annotatedTaxonId;

	private List<Tag> effectiveTags;

	private RecordQuality recordQuality;
	private Double recordQualityNumeric;
	private Reliability reliability;
	private Boolean needsIdentification;
	private Boolean needsCheck;
	private String collectionAndRecordQuality;
	private InvasiveControl invasiveControlEffectiveness;

	@Transient
	@FileDownloadField(name="Tags")
	public List<Tag> getEffectiveTags() {
		return effectiveTags;
	}

	public void setEffectiveTags(ArrayList<Tag> effectiveTags) {
		this.effectiveTags = effectiveTags;
	}

	@Transient
	@FileDownloadField(name="RecordQuality")
	public RecordQuality getRecordQuality() {
		return recordQuality;
	}

	public void setRecordQuality(RecordQuality recordQuality) {
		this.recordQuality = recordQuality;
	}

	@Column(name="recordquality_numeric")
	public Double getRecordQualityNumeric() {
		return recordQualityNumeric;
	}

	public void setRecordQualityNumeric(Double recordQualityNumeric) {
		this.recordQualityNumeric = recordQualityNumeric;
	}

	@Transient
	public Reliability getReliability() {
		return reliability;
	}

	public void setReliability(Reliability reliability) {
		this.reliability = reliability;
	}

	@Column(name="needs_det")
	public Boolean getNeedsIdentification() {
		return needsIdentification;
	}

	public void setNeedsIdentification(Boolean needsIdentification) {
		this.needsIdentification = needsIdentification;
	}

	@Column(name="needs_check")
	public Boolean getNeedsCheck() {
		return needsCheck;
	}

	public void setNeedsCheck(Boolean needsCheck) {
		this.needsCheck = needsCheck;
	}

	@Column(name="col_and_rec_quality")
	public String getCollectionAndRecordQuality() {
		return collectionAndRecordQuality;
	}

	public void setCollectionAndRecordQuality(String collectionAndRecordQuality) {
		this.collectionAndRecordQuality = collectionAndRecordQuality;
	}

	@FileDownloadField(name="IndividualCount", order=2)
	@Column(name="individualcount")
	public int getIndividualCount() {
		return individualCount;
	}

	public void setIndividualCount(int individualCount) {
		this.individualCount = individualCount;
	}

	@FileDownloadField(name="AnnotatedTaxonId", order=1)
	@Transient
	public Qname getAnnotatedTaxonId() {
		return annotatedTaxonId;
	}

	public void setAnnotatedTaxonId(Qname annotatedTaxonId) {
		this.annotatedTaxonId = annotatedTaxonId;
	}

	@FileDownloadField(name="PairCount", order=3)
	@Column(name="paircount")
	public Integer getPairCount() {
		return pairCount;
	}

	public void setPairCount(Integer pairCount) {
		this.pairCount = pairCount;
	}

	@Transient
	@FileDownloadField(name="InvasiveControl", order=900)
	public InvasiveControl getInvasiveControlEffectiveness() {
		return invasiveControlEffectiveness;
	}

	public void setInvasiveControlEffectiveness(InvasiveControl invasiveControlEffectiveness) {
		this.invasiveControlEffectiveness = invasiveControlEffectiveness;
	}

	@Column(name="invasivecontrolled")
	public Boolean isInvasiveControlled() {
		return invasiveControlEffectiveness == InvasiveControl.FULL || invasiveControlEffectiveness == InvasiveControl.NOT_FOUND;
	}

	@Deprecated // for hibernate
	public void setInvasiveControlled(@SuppressWarnings("unused") Boolean invasiveControlled) {}

}
