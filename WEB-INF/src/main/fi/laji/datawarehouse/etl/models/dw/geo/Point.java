package fi.laji.datawarehouse.etl.models.dw.geo;

import java.util.Iterator;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;

import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class Point implements Feature {

	private final Geometry geometry;

	Point(int lat, int lon) {
		this((double)lat, (double)lon);
	}

	Point(double lat, double lon) {
		lat = Utils.round(lat, 6);
		lon = Utils.round(lon, 6);
		geometry = new GeometryFactory().createPoint(new Coordinate(lon, lat));
	}

	Point(Geometry point) {
		if (point == null) throw new IllegalArgumentException("Null geometry");
		geometry = point;
	}

	public static Point from(int lat, int lon) {
		return new Point(lat, lon);
	}

	public static Point from(double lat, double lon) {
		return new Point(lat, lon);
	}

	@Override
	public Double getLatMin() {
		return geometry.getCoordinate().y;
	}

	@Override
	public Double getLatMax() {
		return geometry.getCoordinate().y;
	}

	@Override
	public Double getLonMin() {
		return geometry.getCoordinate().x;
	}

	@Override
	public Double getLonMax() {
		return geometry.getCoordinate().x;
	}

	public Double getLat() {
		return geometry.getCoordinate().y;
	}

	public Double getLon() {
		return geometry.getCoordinate().x;
	}

	public Coordinate asCoordinate() {
		return geometry.getCoordinate();
	}

	@Override
	public String getWKT() {
		return geometry.toText();
	}

	@Override
	public JSONObject getGeoJSON() {
		return new JSONObject()
				.setString("type", "Point")
				.setArray("coordinates", getJsonCoordinatesArray());
	}

	public JSONArray getJsonCoordinatesArray() {
		return new JSONArray()
				.appendDouble(this.getLon())
				.appendDouble(this.getLat());
	}

	public static Feature fromGeoJSON(JSONObject geometry) throws DataValidationException {
		JSONArray coordinateArray = geometry.getArray("coordinates");
		return fromCoordinateArray(coordinateArray);
	}

	public static Feature fromCoordinateArray(JSONArray coordinateArray) throws DataValidationException {
		List<Double> l = coordinateArray.iterateAsDouble();
		if (l.size() != 2) throw new DataValidationException("Invalid coordinate pair " + coordinateArray);
		Point point = new Point(l.get(1), l.get(0));
		return point.validate();
	}

	@Override
	public Feature validate() throws DataValidationException {
		if (geometry.getCoordinates().length != 1) throw new DataValidationException("Must have one pair of coordinates");
		return this;
	}

	@Override
	public Iterator<Point> iterator() {
		return Utils.singleEntryList(this).iterator();
	}

	@Override
	public String toString() {
		return "{" + getLat() + ", " + getLon() +"}";
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getLat() == null) ? 0 : getLat().hashCode());
		result = prime * result + ((getLon() == null) ? 0 : getLon().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (getLat() == null) {
			if (other.getLat() != null)
				return false;
		} else if (!getLat().equals(other.getLat()))
			return false;
		if (getLon() == null) {
			if (other.getLon() != null)
				return false;
		} else if (!getLon().equals(other.getLon()))
			return false;
		return true;
	}

}
