package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.List;

import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class LajistoreSecondaryDataHarmonizer extends LajistoreHarmonizer {

	public LajistoreSecondaryDataHarmonizer(ContextDefinitions contextDefinitions) {
		super(contextDefinitions, null);
	}

	@Override
	protected DwRoot parseRoot(JSONObject documentJson, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		Qname collectionId = super.parseCollectionId(documentJson);
		if (collectionId == null) throw new CriticalParseFailure("No collection id");

		String id = documentJson.getString(ID);
		try {
			Qname documentId = new Qname(collectionId.toString() + "/" + id);
			documentJson.setString(ID, documentId.toString());

			List<JSONObject> gatheringJsons = getGatherings(documentJson);
			if (gatheringJsons.size() == 0) {
				return DwRoot.createDeleteRequest(documentId, source);
			}
			if (gatheringJsons.size() == 1) {
				for (JSONObject gatheringJson : gatheringJsons) {
					gatheringJson.setString(ID, documentId.toString() + "#G");
					List<JSONObject> unitJsons = getUnits(gatheringJson);
					if (unitJsons.size() == 1) {
						for (JSONObject unitJson : unitJsons) {
							unitJson.setString(ID, documentId.toString() + "#U");
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof CriticalParseFailure) throw e;
			throw new CriticalParseFailure("Filling ids fails", e);
		}

		DwRoot root = super.parseRoot(documentJson, source);
		addIdAsKeyword(root.getPublicDocument(), id);
		addIdAsKeyword(root.getPrivateDocument(), id);
		return root;
	}

	private void addIdAsKeyword(Document document, String id) {
		if (document == null) return;
		document.addKeyword(id);
	}

}