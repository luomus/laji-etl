package fi.laji.datawarehouse.etl.models.containers;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

@Entity
@Table(name="log")
public class LogEntry {

	private int id;
	private long timestamp;
	private Qname source;
	private String phase;
	private String type;
	private String identifier;
	private String message;
	private String fullMessage;

	public LogEntry() {}

	public LogEntry(Qname source, Class<?> phase, String type, String identifier, String message) {
		this.source = source;
		setPhase(phase.getSimpleName());
		this.type = type;
		this.timestamp = System.currentTimeMillis();
		setIdentifier(identifier);
		setMessage(message);
		setFullMessage(message);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Transient
	public Date getDatetime() {
		return new Date(timestamp);
	}
	@Transient
	public Qname getSource() {
		return source;
	}
	public void setSource(Qname source) {
		this.source = source;
	}
	@Column(name="source")
	public String getSourceQname() {
		return source == null ? null : source.toString();
	}
	public void setSourceQname(String source) {
		this.source = source == null ? null : new Qname(source);
	}
	@Column
	public String getPhase() {
		return phase;
	}
	public void setPhase(String phase) {
		this.phase = Utils.trimToLength(phase, 400);
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = Utils.trimToLength(identifier, 400);
	}
	@Column
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = Utils.trimToLength(message, 3000);
	}
	@Column(name="full_message")
	public String getFullMessage() {
		return fullMessage;
	}
	public void setFullMessage(String message) {
		this.fullMessage = message;
	}

	@Override
	public String toString() {
		// note missing id and timestamp (to string is used only for tests)
		return "LogEntry [source=" + source + ", phase=" + phase + ", type=" + type + ", identifier=" + identifier + ", message=" + message + "]";
	}

}
