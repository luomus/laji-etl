package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class LajiETLJSONHarmonizer implements Harmonizer<JSONObject> {

	@Override
	public List<DwRoot> harmonize(JSONObject json, Qname source) throws CriticalParseFailure {
		List<DwRoot> roots = new ArrayList<>();
		if (json.hasKey("roots")) {
			for (JSONObject root : json.getArray("roots").iterateAsObject()) {
				roots.add(parse(root, source));
			}
		} else {
			roots.add(parse(json, source));
		}
		return roots;

	}

	private DwRoot parse(JSONObject root, Qname source) throws CriticalParseFailure {
		try {
			DwRoot dwRoot = JsonToModel.rootFromJson(root);
			if (!source.equals(dwRoot.getSourceId())) throw new IllegalStateException("Json source = " + dwRoot.getSourceId() + " but harmonized for source " + source);
			convertComplexToConvexHull(dwRoot);
			return dwRoot;
		} catch (Exception e) {
			throw new CriticalParseFailure(JsonToModel.class.getName() + " failed" + message(e), e);
		}
	}

	private void convertComplexToConvexHull(DwRoot dwRoot) throws DataValidationException {
		convertComplex(dwRoot.getPublicDocument());
		convertComplex(dwRoot.getPrivateDocument());
	}

	private void convertComplex(Document document) throws DataValidationException {
		if (document == null) return;
		for (Gathering g : document.getGatherings()) {
			if (g.getGeo() != null) {
				g.getGeo().convertComplex();
			}
		}
	}

	private String message(Exception e) {
		if (e.getMessage() == null) return "";
		return ": " + e.getMessage();
	}


}
