package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class UnlinkedUserIdsData {

	private final String userId;
	private final Qname sourceId;
	private final int count;

	public UnlinkedUserIdsData(String userId, Qname sourceId, int count) {
		this.userId = userId;
		this.sourceId = sourceId;
		this.count = count;
	}

	public String getUserId() {
		return userId;
	}

	public Qname getSourceId() {
		return sourceId;
	}

	public int getCount() {
		return count;
	}


}
