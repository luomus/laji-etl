package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class Sample extends BaseModel {

	private static final Set<String> SECURED_FACTS = Utils.set(
			new Qname("MZ.dateCreated").toURI(),
			new Qname("MZ.dateEdited").toURI(),
			new Qname("MF.qualityCheckDate").toURI(),
			new Qname("MF.notes").toURI(),
			new Qname("MF.preparationDate").toURI(),
			new Qname("MF.preparationProcessNotes").toURI(),
			new Qname("MZ.creator").toURI(),
			new Qname("MZ.editor").toURI());

	private Qname sampleId;
	private Qname collectionId;
	private List<String> keywords = new ArrayList<>();
	private Boolean multiple = false;
	private Qname type;
	private Qname quality;
	private Qname status;
	private Qname material;
	private int sampleOrder;

	public Sample(Qname sampleId) throws CriticalParseFailure {
		Util.validateId(sampleId, "sampleId");
		this.sampleId = sampleId;
	}

	@Deprecated
	public Sample() {
		// for hibernate
	}

	@Transient
	@FileDownloadField(name="SampleID", order=-1)
	public Qname getSampleId() {
		return sampleId;
	}

	@Transient
	@FileDownloadField(name="CollectionID", order=1)
	public Qname getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Qname collectionId) {
		this.collectionId = collectionId;
	}

	@Transient
	@FileDownloadField(name="Keywords", order=1.1)
	public List<String> getKeywords() {
		return keywords;
	}

	public void addKeyword(String keyword) {
		if (keyword == null || keyword.trim().isEmpty()) return;
		keyword = Util.trimToByteLength(keyword, 1000);
		if (!this.keywords.contains(keyword)) {
			this.keywords.add(keyword);
		}
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	@Column(name="multiple")
	@FileDownloadField(name="Multiple", order=5)
	public Boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}

	@Column(name="type")
	@FileDownloadField(name="Type", order=2.0)
	public String getType() {
		if (type == null) return null;
		return type.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setType(String type) {
		if (type == null) {
			this.type = null;
		} else {
			setTypeUsingQname(Qname.fromURI(type));
		}
	}

	@Transient
	public void setTypeUsingQname(Qname type) {
		this.type = type;
	}

	@Column(name="quality")
	@FileDownloadField(name="Quality", order=4.0)
	public String getQuality() {
		if (quality == null) return null;
		return quality.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setQuality(String quality) {
		if (quality == null) {
			this.quality = null;
		} else {
			setQualityUsingQname(Qname.fromURI(quality));
		}
	}

	@Transient
	public void setQualityUsingQname(Qname quality) {
		this.quality = quality;
	}

	@Column(name="status")
	@FileDownloadField(name="Status", order=4.1)
	public String getStatus() {
		if (status == null) return null;
		return status.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setStatus(String status) {
		if (status == null) {
			this.status = null;
		} else {
			setStatusUsingQname(Qname.fromURI(status));
		}
	}

	@Transient
	public void setStatusUsingQname(Qname status) {
		this.status = status;
	}

	@Column(name="material")
	@FileDownloadField(name="Material", order=4.2)
	public String getMaterial() {
		if (material == null) return null;
		return material.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setMaterial(String material) {
		if (material == null) {
			this.material = null;
		} else {
			setMaterialUsingQname(Qname.fromURI(material));
		}
	}

	@Transient
	public void setMaterialUsingQname(Qname material) {
		this.material = material;
	}

	@Column(name="sample_order")
	@FileDownloadField(name="SampleOrder", order=10)
	@FieldDefinition(ignoreForETL=true)
	public int getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(int sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public Sample copy() {
		try {
			JSONObject sampleJSON = ModelToJson.toJson(this);
			Sample copy = JsonToModel.sampleFromJson(sampleJSON);
			return copy;
		} catch (Exception e) {
			throw new IllegalStateException();
		}
	}

	public Sample concealPublicData(SecureLevel secureLevel) {
		if (secureLevel == SecureLevel.NONE) throw new IllegalArgumentException("No point concealing with secure level " + secureLevel);
		Sample publicSample = this.copy();
		for (String factName : SECURED_FACTS) {
			publicSample.removeFact(factName);
		}
		publicSample.setNotes(null);
		return publicSample;
	}

	public void setSampleId(Qname sampleId) throws CriticalParseFailure {
		Util.validateId(sampleId, "sampleId");
		this.sampleId = sampleId;
	}

	public static Sample emptySample() {
		return new Sample();
	}

}
