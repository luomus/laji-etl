package fi.laji.datawarehouse.etl.models.harmonizers;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class DarwinCoreArchiveHarmonizer extends BaseHarmonizer<String> {

	private final CSVFormat format;

	public DarwinCoreArchiveHarmonizer(CSVFormat format) {
		this.format = format;
	}

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		data = data.replace("\r", "");
		List<DwRoot> roots = new ArrayList<>();
		CSVParser records = readCSV(data);
		for (CSVRecord record : records) {
			DwRoot root = parse(record, source);
			if (root != null) {
				roots.add(root);
			}
		}
		return roots;
	}

	private CSVParser readCSV(String data) throws CriticalParseFailure {
		CSVFormat csvFormat = format.withFirstRecordAsHeader()
				.withIgnoreEmptyLines(true)
				.withTrim();
		CSVParser records;
		try {
			records = csvFormat.parse(new StringReader(data));
		} catch (Exception e) {
			throw new CriticalParseFailure("Unable to parse CSV", e);
		}
		return records;
	}

	private DwRoot parse(CSVRecord record, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		DwRoot root = new DwRoot(buildDocumentUriIdentifier(source, get(record, "occurrenceID")), source);
		root.setCollectionId(parseCollection(record));
		try {
			boolean validUnit = parse(root, record);
			if (!validUnit) return null;
		} catch (Exception e) {
			handleException(e);
		}
		return root;
	}

	private Qname parseCollection(CSVRecord record) {
		String collectionId = get(record, "collectionId");
		if (!given(collectionId)) return null;
		if (collectionId.startsWith("http")) return Qname.fromURI(collectionId);
		return new Qname(collectionId);
	}

	private boolean parse(DwRoot root, CSVRecord record) throws CriticalParseFailure {
		String scientificName = get(record, "scientificName");
		if (!given(scientificName)) return false;

		Document document = root.createPublicDocument();
		Gathering gathering = new Gathering(Qname.fromURI(root.getDocumentId().toURI() + "_Gathering"));
		Unit unit = new Unit(Qname.fromURI(root.getDocumentId().toURI() + "_Unit"));

		parseKeywords(record, document);
		parseTeam(record, gathering);
		parsePlaces(record, gathering);
		parseCoordinates(record, gathering);
		parseDate(record, gathering);

		parseRecordBasis(record, unit);
		unit.setTaxonVerbatim(scientificName);
		parseAuthor(record, unit);
		parseAbundance(record, unit);
		parseSex(record, unit);
		parseLifeStage(record, unit);
		parseNotes(record, unit);

		parseDynamicProperties(record, unit);

		document.addGathering(gathering);
		gathering.addUnit(unit);

		return true;
	}

	private void parseDynamicProperties(CSVRecord record, Unit unit) {
		String dynamicProperties = get(record, "dynamicProperties");
		if (!given(dynamicProperties)) return;
		try {
			JSONObject json = new JSONObject(dynamicProperties);
			for (String key : json.getKeys()) {
				String value = json.getString(key);
				unit.addFact(key, value);
			}
		} catch (Exception e) {

		}
	}

	private void parseLifeStage(CSVRecord record, Unit unit) {
		unit.setLifeStage(getLifeStage(get(record, "lifeStage")));
	}

	private void parseSex(CSVRecord record, Unit unit) {
		unit.setSex(getSex(get(record, "sex")));
	}

	private void parseAuthor(CSVRecord record, Unit unit) {
		unit.setAuthor(get(record, "scientificNameAuthorship"));
	}

	private void parseRecordBasis(CSVRecord record, Unit unit) {
		String basisOfRecord = get(record, "basisOfRecord");
		unit.setRecordBasis(getRecordBasis(basisOfRecord));
	}

	private void parseAbundance(CSVRecord record, Unit unit) {
		unit.setAbundanceString(get(record, "individualCount"));
	}

	private void parseNotes(CSVRecord record, Unit unit) {
		unit.setNotes(get(record, "occurrenceRemarks"));
	}

	private void parseCoordinates(CSVRecord record, Gathering gathering) {
		String decimalLatitude = get(record, "decimalLatitude");
		String decimalLongitude = get(record, "decimalLongitude");
		if (given(decimalLatitude, decimalLongitude)) {
			try {
				gathering.setCoordinates(new Coordinates(Double.valueOf(decimalLatitude),Double.valueOf(decimalLongitude), Type.WGS84));
				String coordinateUncertaintyInMeters = get(record, "coordinateUncertaintyInMeters");
				if (given(coordinateUncertaintyInMeters)) {
					gathering.getCoordinates().setAccuracyInMeters(Integer.valueOf(coordinateUncertaintyInMeters));
				}
			} catch (Exception e) {
				createCoordinateIssue(gathering, Type.WGS84, new DataValidationException("Invalid coordinates", e));
			}
		}
	}

	private void parsePlaces(CSVRecord record, Gathering gathering) {
		gathering.setLocality(get(record, "locality"));
		gathering.setCountry(get(record, "countryCode"));
		if (!given(gathering.getCountry())) {
			gathering.setCountry(get(record, "country"));
		}
		gathering.setProvince(get(record, "stateProvince"));
	}

	private void parseTeam(CSVRecord record, Gathering gathering) {
		gathering.addTeamMember(get(record, "recordedBy"));
	}

	private void parseKeywords(CSVRecord record, Document document) {
		document.addKeyword(get(record, "catalogNumber"));
		document.addKeyword(get(record, "recordNumber"));
		String otherCatalogNumbers = get(record, "otherCatalogNumbers");
		if (given(otherCatalogNumbers)) {
			for (String cn : otherCatalogNumbers.split(Pattern.quote(","))) {
				document.addKeyword(cn);
			}
		}
	}

	private void parseDate(CSVRecord record, Gathering gathering) {
		String eventDate = get(record, "eventDate");
		String verbatimEventDate = get(record, "verbatimEventDate");
		String year = get(record, "year");

		if (given(eventDate)) {
			parseDate(gathering, eventDate);
			return;
		}
		if (given(verbatimEventDate)) {
			parseDate(gathering, verbatimEventDate);
			return;
		}
		if (given(year)) {
			parseDate(gathering, verbatimEventDate);
		}
	}

	private String get(CSVRecord record, String field) {
		try {
			String s = record.get(field);
			if (given(s)) return s;
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	private void parseDate(Gathering gathering, String eventDate) {
		try {
			gathering.setEventDate(new DateRange(parseDate(eventDate)));
		} catch (DateValidationException e) {
			createTimeIssue(gathering, e);
		} catch (Exception e) {
			String message = "Unparsable date \"" + eventDate + "\".";
			if (given(e.getMessage())) {
				message += " Cause: " + e.getMessage();
			}
			createTimeIssue(gathering, Quality.Issue.INVALID_DATE, message);
		}
	}

}
