package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class ThreadStatusData {

	private final Qname source;
	private final String threadName;
	private final String status;

	public ThreadStatusData(Qname source, String threadName, String status) {
		this.source = source;
		this.threadName = threadName;
		this.status = status;
	}

	public Qname getSource() {
		return source;
	}

	public String getThreadName() {
		return threadName;
	}

	public String getStatus() {
		return status;
	}

}
