package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class SimpleDarwinRecordSetHarmonizer extends BaseHarmonizer<fi.luomus.commons.xml.Document> {

	public static final Qname INSECT_DATABASE_VIRTALA = new Qname("KE.423");
	public static final Qname BUTTEFLY_ATLAS = new Qname("KE.461");

	public static Map<Qname, Qname> SOURCE_TO_COLLECTION_MAP = initSourceToCollectionMap();

	private static Map<Qname, Qname> initSourceToCollectionMap() {
		Map<Qname, Qname> map = new HashMap<>();
		map.put(new Qname("KE.421"),	new Qname("HR.205"));
		map.put(new Qname("KE.422"),	new Qname("HR.627"));
		map.put(INSECT_DATABASE_VIRTALA,	new Qname("HR.200"));
		map.put(new Qname("KE.621"), 	new Qname("HR.2009"));
		return map;
	}

	private static Set<String> KNOWN_FIELDS = Utils.set(
			"dwc:basisOfRecord", "dwc:scientificName", "dwc:individualCount", "dwc:lifeStage",
			"dwc:eventDate", "dwc:country", "dwc:municipality", "dwc:locality", "dwc:stateProvince",
			"dwc:geodeticDatum", "dwc:decimalLatitude", "dwc:decimalLongitude", "dwc:verbatimCoordinates",
			"dwc:recordedBy", "dc:created", "dc:modified", "dwc:occurrenceID", "dwc:collectionID", "dwc:dynamicProperties", "dwc:dynamicProperty",
			"dwc:eventRemarks", "dwc:occurrenceRemarks");

	private static Set<String> KNOWN_PROPERTIES = Utils.set("owner", "biogeographicalProvince", "det", "publicityRestrictions", "notes");

	@Override
	public List<DwRoot> harmonize(fi.luomus.commons.xml.Document simpleDarwinRecordSet, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> roots = new ArrayList<>();
		for (Node simpleDarwinRecord : simpleDarwinRecordSet.getRootNode().getChildNodes("SimpleDarwinRecord")) {
			roots.add(parse(simpleDarwinRecord, source));
		}
		return roots;
	}

	private DwRoot parse(Node simpleDarwinRecord, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		DwRoot root = parseDocumentId(simpleDarwinRecord, source);
		root.setCollectionId(parseCollectionId(simpleDarwinRecord, source));
		try {
			parse(simpleDarwinRecord, root);
		} catch (Exception e) {
			handleException(e);
		}
		return root;
	}

	private void parse(Node simpleDarwinRecord, DwRoot root) throws CriticalParseFailure {
		Document document = parseDocument(simpleDarwinRecord, root);

		Gathering gathering = parseGathering(simpleDarwinRecord, root);
		document.addGathering(gathering);

		Unit unit = parseUnit(simpleDarwinRecord, root);
		gathering.addUnit(unit);

		parseFacts(simpleDarwinRecord, unit);
		parseDynamicProperties(simpleDarwinRecord, document, gathering, unit);

		if (root.getSourceId().equals(BUTTEFLY_ATLAS)) {
			moveTeamToNotes(gathering);
		}
	}

	private void moveTeamToNotes(Gathering gathering) {
		if (gathering.getTeam().isEmpty()) return;
		String team = gathering.getTeam().stream().collect(Collectors.joining(", "));
		gathering.getTeam().clear();
		if (gathering.getNotes() == null) {
			gathering.setNotes(team);
		} else {
			gathering.setNotes(team + " " + gathering.getNotes());
		}
	}

	private void parseDynamicProperties(Node simpleDarwinRecord, Document document, Gathering gathering, Unit unit) throws CriticalParseFailure {
		String dynamicProperties = getContents("dwc:dynamicProperties", simpleDarwinRecord);
		if (!given(dynamicProperties))  {
			dynamicProperties = getContents("dwc:dynamicProperty", simpleDarwinRecord);
		}
		if (!given(dynamicProperties)) return;

		JSONObject properties = null;
		try {
			properties = new JSONObject(dynamicProperties);
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid JSON: " + dynamicProperties);
		}
		for (String key : properties.getKeys()) {
			String value = properties.getString(key);
			if (KNOWN_PROPERTIES.contains(key)) {
				if ("owner".equals(key)) {
					document.addEditorUserId(value);
					continue;
				}
				if ("biogeographicalProvince".equals(key)) {
					gathering.setBiogeographicalProvince(value);
					continue;
				}
				if ("det".endsWith(key)) {
					unit.setDet(value);
					continue;
				}
				if ("publicityRestrictions".equals(key)) {
					if (document.getSourceId().equals(INSECT_DATABASE_VIRTALA)) {
						document.setSecureLevel(SecureLevel.KM10);
						document.addSecureReason(SecureReason.USER_HIDDEN);
					} else {
						document.setSecureLevel(SecureLevel.KM100);
						document.addSecureReason(SecureReason.CUSTOM);
					}
					continue;
				}
				if ("notes".equals(key)) {
					if (given(unit.getNotes())) {
						unit.setNotes(unit.getNotes() + ", " + value);
					} else {
						unit.setNotes(value);
					}
				}
			} else {
				unit.addFact(key, value);
			}
		}
	}

	private void parseFacts(Node simpleDarwinRecord, Unit unit) {
		for (Node n : simpleDarwinRecord) {
			if (!KNOWN_FIELDS.contains(n.getName())) {
				unit.addFact(n.getName(), n.getContents());
			}
		}
	}

	private Unit parseUnit(Node simpleDarwinRecord, DwRoot root) throws CriticalParseFailure {
		Unit unit = new Unit(Qname.fromURI(root.getDocumentId().toURI() + "#Unit"));
		unit.setRecordBasis(getRecordBasis(getContents("dwc:basisOfRecord", simpleDarwinRecord)));
		unit.setTaxonVerbatim(getContents("dwc:scientificName", simpleDarwinRecord));
		unit.setAbundanceString(getContents("dwc:individualCount", simpleDarwinRecord));
		parseLifeStage(simpleDarwinRecord, unit);

		String remarks = getContents("dwc:occurrenceRemarks", simpleDarwinRecord);
		String associatedReferences = getContents("dwc:associatedReferences", simpleDarwinRecord);

		String notes = "";
		if (given(remarks)) notes = remarks;
		if (given(associatedReferences)) {
			if (given(notes)) notes +="\n";
			notes += associatedReferences;
		}
		unit.setNotes(notes);

		return unit;
	}

	private void parseLifeStage(Node simpleDarwinRecord, Unit unit) {
		String lifeStage = getContents("dwc:lifeStage", simpleDarwinRecord);
		if (given(lifeStage)) {
			lifeStage = lifeStage.toLowerCase();
			if (lifeStage.contains("/")) {
				for (String s : lifeStage.split(Pattern.quote("/"))) {
					LifeStage resolved = getLifeStage(s);
					if (resolved != null) {
						unit.setLifeStage(resolved);
						return;
					}
				}
			}
			unit.setLifeStage(getLifeStage(lifeStage));
		}
	}

	private Gathering parseGathering(Node simpleDarwinRecord, DwRoot root) throws CriticalParseFailure {
		Gathering gathering = new Gathering(Qname.fromURI(root.getDocumentId().toURI() + "#Gathering"));

		String eventDateTime = getContents("dwc:eventDate", simpleDarwinRecord);
		if (!given(eventDateTime)) eventDateTime = getContents("dwc:verbatimEventDate", simpleDarwinRecord);
		parseEventDateTime(eventDateTime, gathering);

		gathering.setCountry(getContents("dwc:country", simpleDarwinRecord));

		String municipality = getContents("dwc:municipality", simpleDarwinRecord);
		if (given(municipality)) {
			gathering.setMunicipality(municipality);
		} else {
			gathering.setMunicipality(getContents("dwc:county", simpleDarwinRecord));
		}

		gathering.setLocality(getContents("dwc:locality", simpleDarwinRecord));
		gathering.setProvince(getContents("dwc:stateProvince", simpleDarwinRecord));

		parseCoordinates(simpleDarwinRecord, gathering);

		gathering.addTeamMember(getContents("dwc:recordedBy", simpleDarwinRecord));

		gathering.setNotes(getContents("dwc:eventRemarks", simpleDarwinRecord));

		return gathering;
	}

	private void parseCoordinates(Node simpleDarwinRecord, Gathering gathering) {
		String coordinateSystem = getContents("dwc:geodeticDatum", simpleDarwinRecord);
		if ("WGS84".equals(coordinateSystem)) {
			String decimalLatitude = getContents("dwc:decimalLatitude", simpleDarwinRecord);
			String decimalLongitude = getContents("dwc:decimalLongitude", simpleDarwinRecord);
			if (given(decimalLatitude, decimalLongitude)) {
				Double lat = Double.valueOf(decimalLatitude.replace(",", "."));
				Double lon = Double.valueOf(decimalLongitude.replace(",", "."));
				gathering.setCoordinatesVerbatim(lat + ", " + lon + " WGS84");
				try {
					gathering.setCoordinates(new Coordinates(lat, lon, Type.WGS84));
				} catch (DataValidationException e) {
					createCoordinateIssue(gathering, Type.WGS84, e);
				}
			}
		} else if ("FI KKJ27".equals(coordinateSystem)) {
			String ykjGrid = getContents("dwc:verbatimCoordinates", simpleDarwinRecord);
			gathering.setCoordinatesVerbatim(ykjGrid + " FI KKJ27");
			try {
				gathering.setCoordinates(parseYKJCoordinates(ykjGrid));
			} catch (DataValidationException e) {
				createCoordinateIssue(gathering, Type.YKJ, e);
			}
		}
	}

	private Document parseDocument(Node simpleDarwinRecord, DwRoot root) throws CriticalParseFailure {
		Document document = root.createPublicDocument();
		try {
			document.setCreatedDate(parseDate(getContents("dc:created", simpleDarwinRecord)));
		} catch (DateValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_CREATED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
		try {
			document.setModifiedDate(parseDate(getContents("dc:modified", simpleDarwinRecord)));
		} catch (DateValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_MODIFIED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
		if (document.getModifiedDate() != null && document.getCreatedDate() == null) {
			try {
				document.setCreatedDate(document.getModifiedDate());
			} catch (DateValidationException e) {
				throw new ETLException("Impossible state");
			}
		}
		return document;
	}

	private Qname parseCollectionId(Node simpleDarwinRecord, Qname source) throws CriticalParseFailure {
		if (simpleDarwinRecord.hasChildNodes("dwc:collectionID")) {
			return Qname.fromURI(simpleDarwinRecord.getNode("dwc:collectionID").getContents());
		}
		Qname collection = SOURCE_TO_COLLECTION_MAP.get(source);
		if (collection == null) throw new CriticalParseFailure("Unknown dwc-source: " + source);
		return collection;
	}

	private DwRoot parseDocumentId(Node simpleDarwinRecord, Qname source) throws CriticalParseFailure {
		try {
			String documentId = simpleDarwinRecord.getNode("dwc:occurrenceID").getContents();
			if (given(documentId)) {
				Qname id = buildDocumentUriIdentifier(source, documentId);
				return new DwRoot(id, source);
			}
			throw new CriticalParseFailure("No document id");
		}
		catch (Exception e) {
			throw new CriticalParseFailure("No document id", e);
		}
	}

}
