package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

public class Identification extends BaseModel {

	private Qname id;
	private String taxon;
	private String author;
	private Qname taxonId;
	private String taxonSpecifier;
	private String taxonSpecifierAuthor;
	private IdentificationDwLinkings linkings;

	public Identification() {}

	public Identification(String taxon, Qname taxonId) {
		setTaxon(taxon);
		setTaxonID(taxonId);
	}

	public Identification(Qname taxonId) {
		this(null, taxonId);
	}

	public Identification(String taxon) {
		this(taxon, null);
	}

	public String getTaxon() {
		return taxon;
	}

	public void setTaxon(String taxon) {
		this.taxon = Util.trimToByteLength(taxon, 5000);
	}

	public Qname getTaxonID() {
		return taxonId;
	}

	public void setTaxonID(Qname taxonId) {
		if (given(taxonId)) {
			this.taxonId = taxonId;
		} else {
			this.taxonId = null;
		}
	}

	public String getTaxonSpecifier() {
		return taxonSpecifier;
	}

	public void setTaxonSpecifier(String taxonSpecifier) {
		this.taxonSpecifier = taxonSpecifier;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTaxonSpecifierAuthor() {
		return taxonSpecifierAuthor;
	}

	public void setTaxonSpecifierAuthor(String taxonSpecifierAuthor) {
		this.taxonSpecifierAuthor = taxonSpecifierAuthor;
	}

	public Qname getId() {
		return id;
	}

	public void setId(Qname id) {
		if (given(id)) {
			this.id = id;
		} else {
			this.id = null;
		}
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public IdentificationDwLinkings getLinkings() {
		return linkings;
	}

	public void setLinkings(IdentificationDwLinkings linkings) {
		this.linkings = linkings;
	}

	public boolean hasIdentification() {
		if (given(taxon)) return true;
		if (taxonId != null && taxonId.isSet()) return true;
		return false;
	}

	@Override
	public String toString() {
		return "Identification [id=" + id + ", taxon=" + taxon + ", author=" + author + ", taxonId=" + taxonId + ", taxonSpecifier=" + taxonSpecifier + ", taxonSpecifierAuthor="
				+ taxonSpecifierAuthor + "]";
	}

}
