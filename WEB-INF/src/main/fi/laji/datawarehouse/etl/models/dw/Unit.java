package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class Unit extends BaseModel {

	public static enum TaxonConfidence { SURE, UNSURE, SUBSPECIES_UNSURE }

	public static enum Sex { MALE, FEMALE, WORKER, UNKNOWN, NOT_APPLICABLE, GYNANDROMORPH, MULTIPLE, CONFLICTING }

	public static enum LifeStage {
		ADULT, JUVENILE, IMMATURE, EGG, TADPOLE, PUPA, NYMPH, SUBIMAGO, LARVA, SNAG, EMBRYO, SUBADULT, MATURE,
		STERILE, FERTILE, SPROUT, DEAD_SPROUT, BUD, FLOWER, WITHERED_FLOWER, SEED, RIPENING_FRUIT, RIPE_FRUIT,
		SUBTERRANEAN, GALL, MARKS, TRIUNGULIN
	}

	public static enum AbundanceUnit { // When adding these remember to check unit individual count interpreter
		OCCURS_DOES_NOT_OCCUR,
		INDIVIDUAL_COUNT,
		PAIRCOUNT, NESTS, BREEDING_SITES, FEEDING_SITES, COLONIES, QUEENS,
		FRUITBODIES, SPROUTS, HUMMOCKS, THALLI, FLOWERS, SPOTS, TRUNKS,
		SHELLS, DROPPINGS, FEEDING_MARKS, INDIRECT_MARKS,
		SQUARE_DM, SQUARE_M, RELATIVE_DENSITY
	}

	public static enum RecordBasis {
		PRESERVED_SPECIMEN, LIVING_SPECIMEN, FOSSIL_SPECIMEN, SUBFOSSIL_SPECIMEN, SUBFOSSIL_AMBER_INCLUSION_SPECIMEN, MICROBIAL_SPECIMEN,
		HUMAN_OBSERVATION_UNSPECIFIED, HUMAN_OBSERVATION_SEEN, HUMAN_OBSERVATION_HEARD, HUMAN_OBSERVATION_PHOTO,
		HUMAN_OBSERVATION_INDIRECT, HUMAN_OBSERVATION_HANDLED,
		HUMAN_OBSERVATION_VIDEO, HUMAN_OBSERVATION_RECORDED_AUDIO,
		MACHINE_OBSERVATION_UNSPECIFIED, MACHINE_OBSERVATION_PHOTO, MACHINE_OBSERVATION_VIDEO, MACHINE_OBSERVATION_AUDIO,
		MACHINE_OBSERVATION_GEOLOGGER, MACHINE_OBSERVATION_SATELLITE_TRANSMITTER,
		LITERATURE,
		MATERIAL_SAMPLE, MATERIAL_SAMPLE_AIR, MATERIAL_SAMPLE_SOIL, MATERIAL_SAMPLE_WATER
	}

	private static final Map<RecordBasis, RecordBasis> RECORD_BASIS_TO_SUPER = new HashMap<>();
	static {
		RECORD_BASIS_TO_SUPER.put(RecordBasis.PRESERVED_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.LIVING_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.FOSSIL_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.SUBFOSSIL_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.SUBFOSSIL_AMBER_INCLUSION_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MICROBIAL_SPECIMEN, RecordBasis.PRESERVED_SPECIMEN);

		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_SEEN, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_HEARD, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_PHOTO, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_INDIRECT, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_HANDLED, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_VIDEO, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.HUMAN_OBSERVATION_RECORDED_AUDIO, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);

		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_PHOTO, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_VIDEO, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_AUDIO, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_GEOLOGGER, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MACHINE_OBSERVATION_SATELLITE_TRANSMITTER, RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);

		RECORD_BASIS_TO_SUPER.put(RecordBasis.LITERATURE, RecordBasis.LITERATURE);

		RECORD_BASIS_TO_SUPER.put(RecordBasis.MATERIAL_SAMPLE, RecordBasis.MATERIAL_SAMPLE);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MATERIAL_SAMPLE_AIR, RecordBasis.MATERIAL_SAMPLE);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MATERIAL_SAMPLE_SOIL, RecordBasis.MATERIAL_SAMPLE);
		RECORD_BASIS_TO_SUPER.put(RecordBasis.MATERIAL_SAMPLE_WATER, RecordBasis.MATERIAL_SAMPLE);
	}

	private Qname unitId;
	private List<MediaObject> media = new ArrayList<>();
	private List<Annotation> annotations = new ArrayList<>();
	private List<String> keywords = new ArrayList<>();
	private List<Sample> samples = new ArrayList<>();
	private List<Tag> sourceTags = new ArrayList<>();
	private List<IdentificationEvent> identifications = new ArrayList<>();
	private List<TypeSpecimen> types = new ArrayList<>();
	private Qname reportedTaxonId;
	private Qname reportedInformalTaxonGroup;
	private Qname autocompleteSelectedTaxonId;
	private String taxonVerbatim;
	private String author;
	private TaxonConfidence reportedTaxonConfidence;
	private String abundanceString;
	private AbundanceUnit abundanceUnit;
	private Integer individualCountMale;
	private Integer individualCountFemale;
	private Qname individualId;
	private Sex sex;
	private LifeStage lifeStage;
	private RecordBasis recordBasis;
	private String det;
	private Boolean typeSpecimen = false;
	private Boolean primarySpecimen;
	private Qname samplingMethod;
	private List<Qname> identificationBasis;
	private Boolean breedingSite;
	private Boolean wild;
	private Boolean alive;
	private Boolean local;
	private Qname plantStatusCode;
	private Qname atlasCode;
	private Qname atlasClass;
	private Integer externalMediaCount;
	private UnitInterpretations interpretations;
	private UnitQuality quality;
	private Qname referencePublication;
	private UnitDWLinkings linkings;
	private int unitOrder;
	private int sampleOrderSeq = 0;

	public Unit(Qname unitId) throws CriticalParseFailure {
		Util.validateId(unitId, "unitId");
		this.unitId = unitId;
	}

	@Deprecated
	public Unit() {
		// for hibernate
	}

	public static Unit emptyUnit() {
		return new Unit();
	}

	@Transient
	@FileDownloadField(name="UnitID", order=-1)
	public Qname getUnitId() {
		return unitId;
	}

	@Column(name="taxon_verbatim")
	@FileDownloadField(name="TaxonVerbatim", order=1)
	public String getTaxonVerbatim() {
		return taxonVerbatim;
	}

	public void setTaxonVerbatim(String taxonVerbatim) {
		this.taxonVerbatim = Util.trimToByteLength(taxonVerbatim, 1000);
		if (taxonVerbatim != null && taxonVerbatim.contains("?") && this.getReportedTaxonConfidence() == null) {
			this.setReportedTaxonConfidence(TaxonConfidence.UNSURE);
		}
	}

	@Transient
	public Qname getReportedTaxonId() {
		return reportedTaxonId;
	}

	public void setReportedTaxonId(Qname taxonId) {
		this.reportedTaxonId = taxonId;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Qname getAutocompleteSelectedTaxonId() {
		return autocompleteSelectedTaxonId;
	}

	public void setAutocompleteSelectedTaxonId(Qname autocompleteSelectedTaxonId) {
		this.autocompleteSelectedTaxonId = autocompleteSelectedTaxonId;
	}

	@Transient
	public Qname getReportedInformalTaxonGroup() {
		return reportedInformalTaxonGroup;
	}

	public void setReportedInformalTaxonGroup(Qname reportedInformalTaxonGroup) {
		this.reportedInformalTaxonGroup = reportedInformalTaxonGroup;
	}

	@Transient
	@FileDownloadField(name="NameAccordingTo", order=5)
	public Qname getReferencePublication() {
		return referencePublication;
	}

	public void setReferencePublication(Qname referencePublication) {
		this.referencePublication = referencePublication;
	}

	@Transient
	@FileDownloadField(name="Sex", order=31)
	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	@Transient
	@FileDownloadField(name="ReportedTaxonConfidence", order=2.1)
	public TaxonConfidence getReportedTaxonConfidence() {
		return reportedTaxonConfidence;
	}

	public void setReportedTaxonConfidence(TaxonConfidence taxonConfidence) {
		this.reportedTaxonConfidence = taxonConfidence;
	}

	@Transient
	@FileDownloadField(name="LifeStage", order=32)
	public LifeStage getLifeStage() {
		return lifeStage;
	}

	public void setLifeStage(LifeStage lifeStage) {
		this.lifeStage = lifeStage;
	}

	@Transient
	@FileDownloadField(name="DetailedRecordBasis", order=21)
	public RecordBasis getRecordBasis() {
		return recordBasis;
	}

	public void setRecordBasis(RecordBasis recordBasis) {
		this.recordBasis = recordBasis;
	}

	@Transient
	@FileDownloadField(name="RecordBasis", order=20)
	@FieldDefinition(ignoreForETL=true)
	public RecordBasis getSuperRecordBasis() {
		if (recordBasis == null) return null;
		RecordBasis superRecordBasis = RECORD_BASIS_TO_SUPER.get(recordBasis);
		if (superRecordBasis == null) throw new IllegalStateException("Unmapped super record basis: " + recordBasis);
		return superRecordBasis;
	}

	@Column(name="det_verbatim")
	@FileDownloadField(name="Det", order=23)
	public String getDet() {
		return det;
	}

	public void setDet(String det) {
		this.det = Util.trimToByteLength(det, 1000);
	}

	@Column(name="typespecimen")
	@FileDownloadField(name="TypeSpecimen", order=22)
	public Boolean isTypeSpecimen() {
		return typeSpecimen;
	}

	public void setTypeSpecimen(Boolean typeSpecimen) {
		this.typeSpecimen = typeSpecimen;
	}

	@Column(name="primary_specimen")
	public Boolean isPrimarySpecimen() {
		return primarySpecimen;
	}

	public void setPrimarySpecimen(Boolean primarySpecimen) {
		this.primarySpecimen = primarySpecimen;
	}

	@Column(name="sampling_method")
	public String getSamplingMethod() {
		if (samplingMethod == null) return null;
		return samplingMethod.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setSamplingMethod(String samplingMethod) {
		if (samplingMethod == null) {
			this.samplingMethod = null;
		} else {
			setSamplingMethodUsingQname(Qname.fromURI(samplingMethod));
		}
	}

	@Transient
	public void setSamplingMethodUsingQname(Qname samplingMethod) {
		this.samplingMethod = samplingMethod;
	}

	@Transient
	public List<Qname> getIdentificationBasis() {
		return identificationBasis;
	}

	public void setIdentificationBasis(ArrayList<Qname> identificationBasis) {
		this.identificationBasis = identificationBasis;
	}

	public Unit addIdentificationBasis(Qname identificationBasis) {
		if (this.identificationBasis == null) {
			this.identificationBasis = new ArrayList<>();
		}
		if (!this.identificationBasis.contains(identificationBasis)) {
			this.identificationBasis.add(identificationBasis);
			Collections.sort(this.identificationBasis);
		}
		return this;
	}

	@Column(name="identification_basis")
	@FieldDefinition(ignoreForETL=true, ignoreForQuery=true)
	public String getIdentificationBasisString() {
		if (identificationBasis == null) return null;
		if (identificationBasis.isEmpty()) return null;
		return Util.trimToByteLength(identificationBasis.stream().map(t->t.toString()).collect(Collectors.joining("|", "|", "|")), 1000);
	}

	public void setIdentificationBasisString(String basis) {
		setIdentificationBasis(null);
		if (basis == null) return;
		for (String b : basis.split(Pattern.quote("|"))) {
			if (b.isEmpty()) continue;
			addIdentificationBasis(new Qname(b));
		}
	}

	@Column(name="author_verbatim")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = Util.trimToByteLength(author, 1000);
	}

	@FileDownloadField(name="Abundance", order=10)
	public String getAbundanceString() {
		return abundanceString;
	}

	public void setAbundanceString(String abundanceString) {
		this.abundanceString = Util.trimToByteLength(abundanceString, 1000);
	}

	@Transient
	@FileDownloadField(name="AbundanceUnit", order=10.1)
	public AbundanceUnit getAbundanceUnit() {
		return abundanceUnit;
	}

	public void setAbundanceUnit(AbundanceUnit abundanceUnit) {
		this.abundanceUnit = abundanceUnit;
	}

	@Column(name="individual_id")
	@FileDownloadField(name="IndividualID", order=100)
	public String getIndividualId() {
		if (individualId == null) return null;
		return individualId.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setIndividualId(String individualId) {
		if (individualId == null) {
			this.individualId = null;
		} else {
			setIndividualIdUsingQname(Qname.fromURI(individualId));
		}
	}

	@Transient
	public void setIndividualIdUsingQname(Qname individualId) {
		this.individualId = individualId;
	}

	@Column(name="breedingsite")
	@FileDownloadField(name="BreedingSite", order=51)
	public Boolean isBreedingSite() {
		return breedingSite;
	}

	public void setBreedingSite(Boolean breedingSite) {
		this.breedingSite = breedingSite;
	}

	@Embedded
	@FileDownloadField(name="Interpretations", order=11)
	@FieldDefinition(ignoreForETL=true)
	public UnitInterpretations getInterpretations() {
		return interpretations;
	}

	public void setInterpretations(UnitInterpretations interpretations) {
		this.interpretations = interpretations;
	}

	public UnitInterpretations createInterpretations() {
		if (interpretations == null) {
			interpretations = new UnitInterpretations();
		}
		return interpretations;
	}

	@Transient
	public List<MediaObject> getMedia() {
		return media;
	}

	public void addMedia(MediaObject media) {
		this.media.add(media);
	}

	public void setMedia(ArrayList<MediaObject> media) {
		this.media = media;
	}

	@Column(name="unit_mediacount")
	@FileDownloadField(name="MediaCount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getMediaCount() {
		return media.size();
	}

	@Deprecated
	public void setMediaCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	@Column(name="unit_imagecount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getImageCount() {
		return countByType(MediaType.IMAGE);
	}

	private int countByType(MediaType type) {
		return media.stream().filter(m->m.getMediaType() == type).collect(Collectors.counting()).intValue();
	}

	@Deprecated
	public void setImageCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	@Column(name="unit_audiocount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getAudioCount() {
		return countByType(MediaType.AUDIO);
	}

	@Deprecated
	public void setAudioCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	@Column(name="unit_videocount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getVideoCount() {
		return countByType(MediaType.VIDEO);
	}

	@Deprecated
	public void setVideoCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	@Column(name="unit_modelcount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getModelCount() {
		return media.stream().filter(MODEL_OR_VIDEO_OF_MODEL_FILTER).collect(Collectors.counting()).intValue();
	}

	private static final Predicate<? super MediaObject> MODEL_OR_VIDEO_OF_MODEL_FILTER = m->
	m.getMediaType() == MediaType.MODEL ||
	(m.getMediaType() == MediaType.VIDEO && Boolean.TRUE.equals(m.getFullResolutionMediaAvailable()));

	@Deprecated
	public void setModelCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	public Unit addIdentification(IdentificationEvent identification) {
		this.identifications.add(identification);
		Collections.sort(this.identifications);
		return this;
	}

	@Transient
	public List<IdentificationEvent> getIdentifications() {
		return identifications;
	}

	public void setIdentifications(ArrayList<IdentificationEvent> identifications) {
		this.identifications = identifications;
	}

	public Unit addType(TypeSpecimen type) {
		this.types.add(type);
		Collections.sort(this.types);
		return this;
	}

	@Transient
	public List<TypeSpecimen> getTypes() {
		return types;
	}

	public void setTypes(ArrayList<TypeSpecimen> types) {
		this.types = types;
	}

	public Unit addSample(Sample sample) {
		this.samples.add(sample);
		sample.setSampleOrder(sampleOrderSeq++);
		return this;
	}

	@Transient
	public List<Sample> getSamples() {
		return samples;
	}

	public void setSamples(ArrayList<Sample> samples) {
		this.sampleOrderSeq = 0;
		for (Sample s : samples) {
			s.setSampleOrder(sampleOrderSeq++);
		}
		this.samples = samples;
	}

	@Transient
	public List<Tag> getSourceTags() {
		return sourceTags;
	}

	public void setSourceTags(ArrayList<Tag> sourceTags) {
		this.sourceTags = sourceTags;
	}

	public Unit addSourceTag(Qname tag) {
		this.sourceTags.add(Annotation.toTag(tag));
		return this;
	}

	public Unit addSourceTag(Tag tag) {
		this.sourceTags.add(tag);
		return this;
	}

	@Column(name="unit_samplecount")
	@FileDownloadField(name="SampleCount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getSampleCount() {
		return samples.size();
	}

	@Deprecated
	public void setSampleCount(@SuppressWarnings("unused") Integer sampleCount) { // for hibernate

	}

	public Unit copy() {
		try {
			JSONObject unitJSON = ModelToJson.toJson(this);
			Unit copy = JsonToModel.unitFromJson(unitJSON);
			return copy;
		} catch (Exception e) {
			throw new IllegalStateException();
		}
	}

	public Unit concealPublicData(SecureLevel secureLevel, Set<SecureReason> secureReasons, Qname sourceId) {
		if (secureLevel == SecureLevel.NONE) throw new IllegalArgumentException("No point concealing with secure level " + secureLevel);
		Unit publicUnit = this.copy();
		publicUnit.setNotes(null);
		if (secureReasons.contains(SecureReason.DATA_QUARANTINE_PERIOD)) {
			publicUnit.getFacts().clear();
		} else {
			publicUnit.setFacts(concealFacts(this.getFacts()));
		}
		publicUnit.getKeywords().clear();

		if (shouldSecureMedia(sourceId)) {
			publicUnit.getMedia().clear();
			for (MediaObject media : this.getMedia()) {
				publicUnit.addMedia(media.concealPublicData());
			}
		}

		publicUnit.getSamples().clear();
		for (Sample s : this.getSamples()) {
			publicUnit.addSample(s.concealPublicData(secureLevel));
		}
		publicUnit.getIdentifications().clear();
		for (IdentificationEvent e : this.getIdentifications()) {
			publicUnit.addIdentification(e.concealPublicData());
		}
		return publicUnit;
	}

	private boolean shouldSecureMedia(Qname sourceId) {
		return !Const.NO_MEDIA_SECURE.contains(sourceId);
	}

	public static ArrayList<Fact> concealFacts(List<Fact> facts) {
		ArrayList<Fact> concealedFacts = new ArrayList<>();
		for (Fact f : facts) {
			if (shouldConceal(f)) {
				continue;
			}
			concealedFacts.add(f);
		}
		return concealedFacts;
	}

	private static boolean shouldConceal(Fact f) {
		if (isNumeric(f)) return false; // Numbers don't need to be hidden
		String value = f.getValue();
		if (value.startsWith("http:")) { // We have pretty long uri identifiers and don't want to hide those (they are not comments) so split to qname
			String[] parts = value.split(Pattern.quote("/"));
			value = parts[parts.length - 1];
		}
		if (value.length() < 8) return false; // Too short to contain anything that should be hidden
		if (containsNumbers(value)) return true; // Can be an address
		return value.length() >= 30; // Long texts are comments of sorts and can contain something that should be hidden
	}

	private static boolean containsNumbers(String value) {
		for (char c : value.toCharArray()) {
			if (Character.isDigit(c)) return true;
		}
		return false;
	}

	private static boolean isNumeric(Fact f) {
		return f.getDecimalValue() != null;
	}

	public void setUnitId(Qname unitId) throws CriticalParseFailure {
		Util.validateId(unitId, "unitId");
		this.unitId = unitId;
	}

	@Transient
	@FileDownloadField(name="Linkings", order=3)
	@FieldDefinition(ignoreForETL=true)
	public UnitDWLinkings getLinkings() {
		return linkings;
	}

	public void setLinkings(UnitDWLinkings linkings) {
		this.linkings = linkings;
	}

	public UnitDWLinkings createLinkings( ) {
		if (linkings == null) {
			linkings = new UnitDWLinkings();
		}
		return linkings;
	}

	@Column(name="wild")
	@FileDownloadField(name="Wild", order=41.1)
	public Boolean isWild() {
		return wild;
	}

	public void setWild(Boolean wild) {
		this.wild = wild;
	}

	@Column(name="alive")
	@FileDownloadField(name="Alive", order=41.2)
	public Boolean isAlive() {
		return alive;
	}

	public void setAlive(Boolean alive) {
		this.alive = alive;
	}

	@Column(name="local")
	@FileDownloadField(name="Local", order=41.2)
	public Boolean isLocal() {
		return local;
	}

	public void setLocal(Boolean local) {
		this.local = local;
	}

	@Column(name="plant_status")
	@FileDownloadField(name="PlantStatusCode", order=52)
	public String getPlantStatusCode() {
		if (plantStatusCode == null) return null;
		return plantStatusCode.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setPlantStatusCode(String plantStatusCode) {
		if (plantStatusCode == null) {
			this.plantStatusCode = null;
		} else {
			setPlantStatusCodeUsingQname(Qname.fromURI(plantStatusCode));
		}
	}

	@Transient
	public void setPlantStatusCodeUsingQname(Qname plantStatusCode) {
		this.plantStatusCode = plantStatusCode;
	}

	@Column(name="atlas_code")
	@FileDownloadField(name="AtlasCode", order=53.1)
	@StatisticsQueryAlllowedField(allowFor=Base.UNIT)
	public String getAtlasCode() {
		if (atlasCode == null) return null;
		return atlasCode.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setAtlasCode(String atlasCode) {
		if (atlasCode == null) {
			this.atlasCode = null;
		} else {
			setAtlasCodeUsingQname(Qname.fromURI(atlasCode));
		}
	}

	@Transient
	public void setAtlasCodeUsingQname(Qname atlasCode) {
		this.atlasCode = atlasCode;
	}

	@Column(name="atlas_class")
	@FileDownloadField(name="AtlasClass", order=53.2)
	@StatisticsQueryAlllowedField(allowFor=Base.UNIT)
	public String getAtlasClass() {
		if (atlasClass == null) return null;
		return atlasClass.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setAtlasClass(String atlasClass) {
		if (atlasClass == null) {
			this.atlasClass = null;
		} else {
			setAtlasClassUsingQname(Qname.fromURI(atlasClass));
		}
	}

	@Transient
	public void setAtlasClassUsingQname(Qname atlasClass) {
		this.atlasClass = atlasClass;
	}

	@Transient
	public Integer getExternalMediaCount() {
		return externalMediaCount;
	}

	public void setExternalMediaCount(Integer externalMediaCount) {
		this.externalMediaCount = externalMediaCount;
	}

	@Embedded
	@FileDownloadField(name="Quality", order=2.0)
	public UnitQuality getQuality() {
		return quality;
	}

	public void setQuality(UnitQuality quality) {
		this.quality = quality;
	}

	public UnitQuality createQuality() {
		if (quality == null) {
			quality = new UnitQuality();
		}
		return quality;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public Unit addAnnotation(Annotation annotation) {
		annotations.add(annotation);
		return this;
	}

	public void setAnnotations(ArrayList<Annotation> annotations) {
		this.annotations = annotations;
	}

	@Column(name="unit_annotationcount")
	@FileDownloadField(name="AnnotationCount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getAnnotationCount() {
		return annotations.stream().filter(a->!a.isDeleted()).collect(Collectors.counting()).intValue();
	}

	@Deprecated
	public void setAnnotationCount(@SuppressWarnings("unused") Integer count) { // for hibernate

	}

	@Transient
	@FileDownloadField(name="Keywords", order=50)
	public List<String> getKeywords() {
		return keywords;
	}

	public void addKeyword(String keyword) {
		if (keyword == null || keyword.trim().isEmpty()) return;
		keyword = Util.trimToByteLength(keyword, 1000);
		if (!this.keywords.contains(keyword)) {
			this.keywords.add(keyword);
		}
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	@Column(name="individualcount_male")
	public Integer getIndividualCountMale() {
		return individualCountMale;
	}

	public void setIndividualCountMale(Integer individualCountMale) {
		this.individualCountMale = individualCountMale;
	}

	@Column(name="individualcount_female")
	public Integer getIndividualCountFemale() {
		return individualCountFemale;
	}

	public void setIndividualCountFemale(Integer individualCountFemale) {
		this.individualCountFemale = individualCountFemale;
	}

	@Column(name="unit_order")
	@FileDownloadField(name="UnitOrder", order=-1)
	@FieldDefinition(ignoreForETL=true)
	public int getUnitOrder() {
		return unitOrder;
	}

	public void setUnitOrder(int unitOrder) {
		this.unitOrder = unitOrder;
	}

}
