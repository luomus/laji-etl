package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.MediaDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;

public class RdfXmlHarmonizer extends BaseHarmonizer<String> {

	private static final Predicate<String> UNRELIABLE_TAXON_FILTER = Pattern.compile("^MYGathering\\[0\\]\\[MYUnit\\]\\[0\\]\\[MYIdentification\\]\\[\\d+\\]\\[MYTaxon\\]$").asPredicate();
	private static final Qname LABEL_TYPE = new Qname("MM.typeEnumLabel");
	private static final Qname HABITAT_TYPE = new Qname("MM.typeEnumHabitat");
	private static final String MY_TYPE_NOTES = "MY.typeNotes";
	private static final String MY_TYPE_PUBL = "MY.typePubl";
	private static final String MY_TYPE_BASIONYME_PUBL = "MY.typeBasionymePubl";
	private static final String MY_TYPE_VERIFICATION = "MY.typeVerification";
	private static final String MY_TYPE_STATUS = "MY.typeStatus";
	private static final String MY_TYPIF_DATE = "MY.typifDate";
	private static final String MY_TYPIF = "MY.typif";
	private static final String MY_TYPE_SUBSPECIES_AUTHOR = "MY.typeSubspeciesAuthor";
	private static final String MY_TYPE_AUTHOR = "MY.typeAuthor";
	private static final String MY_TYPE_SUBSPECIES = "MY.typeSubspecies";
	private static final String MY_TYPE_SPECIES = "MY.typeSpecies";
	private static final String MF_MATERIAL = "MF.material";
	private static final String MF_QUALITY_NOTES = "MF.qualityNotes";
	private static final String MF_CONDITION = "MF.condition";
	private static final String MF_NOTES = "MF.notes";
	private static final String MF_PUBLICATION = "MF.publication";
	private static final String MF_ADDITIONAL_IDS = "MF.additionalIDs";
	private static final String MF_DATASET_ID = "MF.datasetID";
	private static final String MF_STATUS = "MF.status";
	private static final String MF_QUALITY = "MF.quality";
	private static final String MF_PREPARATION_TYPE = "MF.preparationType";
	private static final String MF_COLLECTION_ID = "MF.collectionID";
	private static final String MF_INDIVIDUALS_IN_PREPARATION = "MF.individualsInPreparation";
	private static final String MULTIPLE = new Qname("MF.individualsInPreparationMultiple").toURI();
	private static final String MY_PRIMARY_SPECIMEN = "MY.primarySpecimen";
	private static final String WILD_WILD = new Qname("MY.wildWild").toURI();
	private static final String WILD_NON_WILD = new Qname("MY.wildNonWild").toURI();
	private static final String MY_WILD = "MY.wild";
	private static final String MY_PLANT_STATUS_CODE = "MY.plantStatusCode";
	private static final String MY_SAMPLING_METHOD = "MY.samplingMethod";
	private static final String MF_SAMPLE = "MF.sample";
	private static final String MY_TAXON_URI = "MY.taxonURI";
	private static final String MY_VERIFICATION_STATUS = "MY.verificationStatus";
	private static final String MY_VERIFICATION_STATUS_OK = new Qname("MY.verificationStatusOk").toURI();
	private static final String MY_VERIFICATION_STATUS_VERIFY = new Qname("MY.verificationStatusVerify").toURI();
	private static final String MY_UNRELIABLE_FIELDS = "MY.unreliableFields";
	private static final String MZ_QUALITY_ISSUE = "MZ.qualityIssue";
	public static final Qname INFRARANK_MORPHA = new Qname("MY.infraRankMorpha");
	public static final Qname INFRARANK_HYDRIB = new Qname("MY.infraRankHybrid");
	public static final Qname INFRARANK_FORMA = new Qname("MY.infraRankForma");
	public static final Qname INFRARANK_B = new Qname("MY.infraRankB");
	public static final Qname INFRARANK_VAR = new Qname("MY.infraRankVar");
	public static final Qname INFRARANK_SSP = new Qname("MY.infraRankSsp");
	public static final Qname INFRARANK_FSP = new Qname("MY.infraRankFsp");
	private static final String MY_INFRA_EPITHET = "MY.infraEpithet";
	private static final String MY_INFRA_RANK = "MY.infraRank";
	private static final String TRUE = "true";
	private static final String SCHEDULED_FOR_DELETION = "MZ.scheduledForDeletion";
	private static final String MY_UNIT_GATHERING = "MY.unitGathering";
	private static final String MY_WGS84_LATITUDE = "MY.wgs84Latitude";
	private static final String MY_WGS84_LONGITUDE = "MY.wgs84Longitude";
	private static final String MY_LATITUDE = "MY.latitude";
	private static final String MY_LONGITUDE = "MY.longitude";
	private static final String MY_DATE_BEGIN = "MY.dateBegin";
	private static final String MY_DATE_END = "MY.dateEnd";
	private static final String MY_DATE_VERBATIM = "MY.dateVerbatim";
	private static final String MY_POPULATION_ABUNDANCE = "MY.populationAbundance";
	private static final String MY_COUNT = "MY.count";
	private static final String MY_ABUNDANCE_STRING = "MY.abundanceString";
	private static final String MY_INDIVIDUAL_COUNT = "MY.individualCount";
	private static final String MY_INFORMAL_NAME_STRING = "MY.informalNameString";
	private static final String MY_TYPE_SPECIMEN = "MY.typeSpecimen";
	private static final String MY_IDENTIFICATION = "MY.identification";
	private static final String YES = "yes";
	private static final String MY_PREFERRED_IDENTIFICATION = "MY.preferredIdentification";
	private static final String MY_LIFE_STAGE = "MY.lifeStage";
	private static final String MY_PLANT_LIFE_STAGE = "MY.plantLifeStage";
	private static final String MY_ALIVE = "MY.alive";
	private static final String MY_SEX = "MY.sex";
	private static final String MY_RECORD_BASIS = "MY.recordBasis";
	private static final String MY_TAXON_CONFIDENCE = "MY.taxonConfidence";
	private static final String MY_TAXON = "MY.taxon";
	private static final String MY_AUTHOR = "MY.author";
	private static final String MY_INFRA_AUTHOR = "MY.infraAuthor";
	private static final String MY_DET = "MY.det";
	private static final String MY_DET_DATE = "MY.detDate";
	private static final String MY_IDENTIFICATION_NOTES = "MY.identificationNotes";
	private static final String MY_NOTES = "MY.notes";
	private static final String MY_KEYWORD = "MY.keyword";
	private static final String MY_KEYWORDS = "MY.keywords";
	private static final String MY_TAG = "MY.tag";
	private static final String MY_DATASET_ID = "MY.datasetID";
	private static final String MY_ADDITIONAL_IDS = "MY.additionalIDs";
	private static final String MY_ORIGINAL_SPECIMEN_ID = "MY.originalSpecimenID";
	private static final String MZ_EDITOR = "MZ.editor";
	private static final String MZ_CREATOR = "MZ.creator";
	private static final String MY_LEG = "MY.leg";
	private static final String MY_COLLECTION_ID = "MY.collectionID";
	private static final String RDF_RESOURCE = "rdf:resource";
	private static final String MY_COORDINATE_SYSTEM = "MY.coordinateSystem";
	private static final String MY_COORDINATE_RADIUS = "MY.coordinateRadius";
	private static final String MY_COORDINATE_SYSTEM_ETRS_TM35FIN = "MY.coordinateSystemEtrs-tm35fin";
	private static final String MY_COORDINATE_SYSTEM_YKJ = "MY.coordinateSystemYkj";
	private static final String MY_COORDINATES_GRID_YKJ = "MY.coordinatesGridYKJ";
	private static final String MY_UNIT = "MY.unit";
	private static final String MY_LOCALITY = "MY.locality";
	private static final String MY_ADMINISTRATIVE_PROVINCE = "MY.administrativeProvince";
	private static final String MY_BIOLOGICAL_PROVINCE = "MY.biologicalProvince";
	private static final String MY_MUNICIPALITY = "MY.municipality";
	private static final String MY_COUNTY = "MY.county";
	private static final String MY_COUNTRY = "MY.country";
	private static final String MY_HIGHER_GEOGRAPHY = "MY.higherGeography";
	private static final String MZ_IMAGES = "MZ.images";
	private static final String MZ_DATE_EDITED = "MZ.dateEdited";
	private static final String MZ_DATE_CREATED = "MZ.dateCreated";
	private static final String MY_GATHERING = "MY.gathering";
	private static final String MY_DOCUMENT = "MY.document";
	private static final String MY_DYNAMIC_PROPERTIES = "MY.dynamicProperties";
	private static final String RDF_ABOUT = "rdf:about";
	private static final String MZ_HAS_PART = "MZ.hasPart";
	private static final String MZ_PUBLICITY_RESTRICTIONS = "MZ.publicityRestrictions";
	private static final String MZ_PUBLICITY_RESTRICTIONS_PROTECTED = "MZ.publicityRestrictionsProtected";
	private static final String MZ_PUBLICITY_RESTRICTIONS_PRIVATE = "MZ.publicityRestrictionsPrivate";
	private static final String PUBLICITY_PRIVATE = new Qname(MZ_PUBLICITY_RESTRICTIONS_PRIVATE).toURI();
	private static final String PUBLICITY_PROTECTED = new Qname(MZ_PUBLICITY_RESTRICTIONS_PROTECTED).toURI();
	private static final String PRIMARY = "primary";
	private static final String HABITAT = "habitat";
	private static final String LABEL = "label";
	private static final String MY_LEGUSERID = "MY.legUserID";
	private static final Map<Qname, String> INFRA_RANKS = initInfraRanks();
	private static final Set<String> IGNORED_FACTS = Utils.set(
			MZ_HAS_PART, "MZ.scheduledForDeletion", "MY.inMustikka", MY_COLLECTION_ID, "MY.ispartOf", MY_LEGUSERID,
			MY_NOTES, MY_IDENTIFICATION_NOTES, MY_DET, MY_LEG,
			MY_TYPE_NOTES, MY_TYPE_PUBL, MY_TYPE_BASIONYME_PUBL, MY_TYPE_VERIFICATION, MY_TYPE_STATUS,
			MY_TYPIF_DATE, MY_TYPIF, MY_TYPE_SUBSPECIES_AUTHOR, MY_TYPE_AUTHOR, MY_TYPE_SUBSPECIES, MY_TYPE_SPECIES,
			MY_TAXON_URI, MY_AUTHOR, MY_INFRA_AUTHOR, MY_PREFERRED_IDENTIFICATION, MY_TAXON,
			MF_QUALITY_NOTES, MF_NOTES, MF_CONDITION, MY_KEYWORD, MY_KEYWORDS,
			MY_COUNTRY, MY_DATE_BEGIN, MY_DATE_END, MY_MUNICIPALITY, MY_LATITUDE, MY_LONGITUDE, MY_COORDINATE_SYSTEM,
			MY_BIOLOGICAL_PROVINCE, MY_WGS84_LATITUDE, MY_WGS84_LONGITUDE, MY_LOCALITY, MY_ADMINISTRATIVE_PROVINCE,
			MY_HIGHER_GEOGRAPHY, MY_COORDINATE_RADIUS, MY_DATASET_ID, "MY.datatype", MZ_DATE_EDITED, MZ_DATE_CREATED,
			MZ_CREATOR, MZ_EDITOR, MY_ADDITIONAL_IDS, MY_ORIGINAL_SPECIMEN_ID, "MY.language", "MY.temp",
			MY_RECORD_BASIS, MY_COUNT, MY_INDIVIDUAL_COUNT, MY_LIFE_STAGE, MY_PLANT_LIFE_STAGE, MY_SEX,
			MY_PLANT_STATUS_CODE, MY_ALIVE, MY_WILD,
			MZ_PUBLICITY_RESTRICTIONS, "MY.editNotes", "MY.legVerbatim", "MY.coordinatesVerbatim",
			"MY.ring", MZ_IMAGES
			);

	private static Map<Qname, String> initInfraRanks() {
		Map<Qname, String> map = new HashMap<>();
		map.put(INFRARANK_SSP, "subsp.");
		map.put(INFRARANK_VAR, "var.");
		map.put(INFRARANK_B, "b.");
		map.put(INFRARANK_FORMA, "f.");
		map.put(INFRARANK_HYDRIB, "x");
		map.put(INFRARANK_MORPHA, "m.");
		map.put(INFRARANK_FSP, "f.sp.");
		return map;
	}

	private final MediaDAO mediaDao;

	public RdfXmlHarmonizer(MediaDAO mediaDao) {
		this.mediaDao = mediaDao;
	}

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> roots = new ArrayList<>();

		fi.luomus.commons.xml.Document rdfDocument = parse(data);
		for (Node rdfDocumentNode : rdfDocument.getRootNode().getChildNodes(MY_DOCUMENT)) {
			roots.add(parse(rdfDocumentNode, source));
		}

		return roots;
	}

	private fi.luomus.commons.xml.Document parse(String data) throws CriticalParseFailure {
		try {
			fi.luomus.commons.xml.Document rdfDocument = new XMLReader().parse(data);
			return rdfDocument;
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid application/rdf+xml", e);
		}

	}

	private DwRoot parse(Node rdfDocumentNode, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		DwRoot root = parseDocumentId(rdfDocumentNode, source);

		if (deleted(rdfDocumentNode)) {
			return DwRoot.createDeleteRequest(root.getDocumentId(), source);
		}

		try {
			parse(rdfDocumentNode, root);
		} catch (Exception e) {
			handleException(e);
		}
		return root;
	}

	private boolean deleted(Node rdfDocumentNode) {
		return rdfDocumentNode.hasChildNodes(SCHEDULED_FOR_DELETION) && rdfDocumentNode.getNode(SCHEDULED_FOR_DELETION).getContents().equals(TRUE);
	}

	private DwRoot parseDocumentId(Node document, Qname source) throws CriticalParseFailure {
		try {
			String documentId = document.getAttribute(RDF_ABOUT);
			if (given(documentId)) {
				return new DwRoot(Qname.fromURI(documentId), source);
			}
			throw new CriticalParseFailure("No document id");
		}
		catch (Exception e) {
			throw new CriticalParseFailure("No document id", e);
		}
	}

	private void parse(Node rdfDocumentNode, DwRoot root) throws CriticalParseFailure  {
		parseCollectionId(rdfDocumentNode, root);
		Document publicData = root.createPublicDocument();
		parse(rdfDocumentNode, publicData);
		if (shouldConceal(rdfDocumentNode)) {
			publicData.setSecureLevel(SecureLevel.KM100);
			publicData.addSecureReason(SecureReason.CUSTOM);
		}
	}

	private void parse(Node rdfDocumentNode, Document document) throws CriticalParseFailure  {
		parseKeywords(rdfDocumentNode, document);
		parseNotes(rdfDocumentNode, document);
		parseFacts(rdfDocumentNode, document);
		for (Node rdfHasPartNode : rdfDocumentNode.getChildNodes(MZ_HAS_PART)) {
			for (Gathering g : parseGatherings(rdfHasPartNode.getNode(MY_GATHERING))) {
				document.addGathering(g);
			}
		}

		Tag sourceTag = parseSourceTag(rdfDocumentNode);
		if (sourceTag != null) {
			addSourceTagToAllUnits(document, sourceTag);
		}

		List<String> unreliableTaxonFields = parseUnreliableTaxonFields(rdfDocumentNode);
		if (unreliableTaxonFields != null) {
			List<IdentificationEvent> identifications = getIdentificationsIfOnlyOneUnit(document);
			if (identifications != null && identifications.size() == unreliableTaxonFields.size()) {
				addSourceTagToAllUnits(document, Tag.EXPERT_TAG_UNCERTAIN);
			}
		}

		parseEditors(rdfDocumentNode, document);
		try {
			document.setCreatedDate(parseDate(getContents(MZ_DATE_CREATED, rdfDocumentNode)));
		} catch (DataValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_CREATED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
		try {
			document.setModifiedDate(parseDate(getContents(MZ_DATE_EDITED, rdfDocumentNode)));
		} catch (DataValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_MODIFIED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}

		parseExplicitlySetMediaIds(rdfDocumentNode, document.getMedia());
		Set<Qname> explicitlySetMediaIds = getMediaIds(document);

		parseMediaViaDocumentIdBasedOnLabel(document, explicitlySetMediaIds);

		parseDocumentIssues(rdfDocumentNode, document);
	}

	private void addSourceTagToAllUnits(Document document, Tag sourceTag) {
		document.getGatherings().forEach(g->g.getUnits().forEach(u->u.addSourceTag(sourceTag)));
	}

	private List<IdentificationEvent> getIdentificationsIfOnlyOneUnit(Document document) {
		List<Gathering> gatherings = document.getGatherings();
		if (gatherings.size() != 1) return null;
		List<Unit> units = gatherings.get(0).getUnits();
		if (units.size() != 1) return null;
		return units.get(0).getIdentifications();
	}

	private List<String> parseUnreliableTaxonFields(Node rdfDocumentNode) {
		List<String> unreliableFields = parseUnreliableFields(rdfDocumentNode);
		if (unreliableFields == null) return null;
		return unreliableFields.stream().filter(UNRELIABLE_TAXON_FILTER).collect(Collectors.toList());
	}

	private List<String> parseUnreliableFields(Node rdfDocumentNode) {
		for (Node n : rdfDocumentNode.getChildNodes(MY_UNRELIABLE_FIELDS)) {
			String val = n.getContents();
			return Arrays.asList(val.split(" "));
		}
		return null;
	}

	private Tag parseSourceTag(Node rdfDocumentNode) {
		for (Node n : rdfDocumentNode.getChildNodes(MY_VERIFICATION_STATUS)) {
			String val = getResource(n);
			if (val.equals(MY_VERIFICATION_STATUS_OK)) return Tag.EXPERT_TAG_VERIFIED;
			if (val.equals(MY_VERIFICATION_STATUS_VERIFY)) return Tag.EXPERT_TAG_UNCERTAIN;
		}
		return null;
	}

	private Set<Qname> getMediaIds(Document document) {
		Set<Qname> ids = new HashSet<>();
		getMediaIds(document.getMedia(), ids);
		for (Gathering g : document.getGatherings()) {
			getMediaIds(g.getMedia(), ids);
			for (Unit u : g.getUnits()) {
				getMediaIds(u.getMedia(), ids);
			}
		}
		return ids;
	}

	private void getMediaIds(List<MediaObject> media, Set<Qname> ids) {
		if (media == null) return;
		media.stream().map(m->m.getId()).forEach(ids::add);
	}

	private void parseDocumentIssues(Node rdfDocumentNode, Document document) {
		if (rdfDocumentNode.hasChildNodes(MZ_QUALITY_ISSUE)) {
			Node issueNode = rdfDocumentNode.getNode(MZ_QUALITY_ISSUE);
			document.createQuality().setIssue(parseIssue(issueNode));
		}
	}

	private Quality parseIssue(Node issueNode) {
		if (issueNode.hasAttribute(RDF_RESOURCE)) {
			Quality.Issue issue = (Quality.Issue) EnumToProperty.getInstance().get(Qname.fromURI(issueNode.getAttribute(RDF_RESOURCE)));
			return new Quality(issue, Quality.Source.ORIGINAL_DOCUMENT);
		}
		Quality.Issue issue = (Quality.Issue) EnumToProperty.getInstance().get(new Qname(issueNode.getContents()));
		return new Quality(issue, Quality.Source.ORIGINAL_DOCUMENT);
	}

	private void parseExplicitlySetMediaIds(Node rdfNode, List<MediaObject> media) {
		for (Node mediaNode : rdfNode.getChildNodes(MZ_IMAGES)) {
			if (!mediaNode.hasAttribute(RDF_RESOURCE)) continue;
			Qname mediaId = Qname.fromURI(mediaNode.getAttribute(RDF_RESOURCE));
			if (mediaId.isSet()) {
				MediaObject m = getMediaObject(mediaId);
				if (m != null) {
					media.add(m);
				}
			}
		}
	}

	private MediaObject getMediaObject(Qname mediaId) {
		try {
			return mediaDao.getMediaObject(mediaId);
		} catch (Exception e) {
			throw new ETLException("Failed to load media metadata: " + mediaId, e);
		}
	}

	private void parseMediaViaDocumentIdBasedOnLabel(Document document, Set<Qname> explicitlySetMediaIds) {
		List<MediaObject> media = mediaDao.getMediaObjects(document.getDocumentId());
		media.removeIf(m->explicitlySetMediaIds.contains(m.getId()));
		if (media.isEmpty()) return;

		addDocumentMedia(document, media);
		addGatheringMedia(document, media);
		addUnitMedia(document, media);
	}

	private void addUnitMedia(Document document, List<MediaObject> media) {
		List<MediaObject> unitMedia = filterUnitMedia(media);
		if (unitMedia.isEmpty()) return;
		boolean hadSpecimen = false;
		for (MediaObject m : unitMedia) {
			for (Gathering g : document.getGatherings()) {
				for (Unit u : g.getUnits()) {
					if (u.getSuperRecordBasis() == RecordBasis.PRESERVED_SPECIMEN) {
						hadSpecimen = true;
						u.addMedia(m);
					}
				}
			}
		}
		if (hadSpecimen) return;
		for (MediaObject m : unitMedia) {
			for (Gathering g : document.getGatherings()) {
				for (Unit u : g.getUnits()) {
					u.addMedia(m);
				}
			}
		}
	}

	private List<MediaObject> filterUnitMedia(List<MediaObject> media) {
		List<MediaObject> primary = new ArrayList<>();
		List<MediaObject> other = new ArrayList<>();
		for (MediaObject m : media) {
			if (isLabel(m) || isHabitat(m)) continue;
			if (isPrimary(m)) {
				primary.add(m);
			} else {
				other.add(m);
			}
		}
		primary.addAll(other);
		return primary;
	}

	private void addGatheringMedia(Document document, List<MediaObject> media) {
		media.stream().filter(m->isHabitat(m)).forEach(m->{
			for (Gathering g : document.getGatherings()) {
				g.addMedia(m);
			}
		});
	}

	private void addDocumentMedia(Document document, List<MediaObject> media) {
		media.stream().filter(m->isLabel(m)).forEach(document::addMedia);
	}

	private boolean isPrimary(MediaObject m) {
		return hasKeyword(PRIMARY, m);
	}

	private boolean isHabitat(MediaObject m) {
		return HABITAT_TYPE.equals(m.getType()) || hasKeyword(HABITAT, m);
	}

	private boolean isLabel(MediaObject m) {
		return LABEL_TYPE.equals(m.getType()) || hasKeyword(LABEL, m);
	}

	private boolean hasKeyword(String keywordToMatch, MediaObject media) {
		if (media.getKeywords().contains(keywordToMatch)) return true;
		if (fileNameHasKeyword(keywordToMatch, media.getFullURL())) return true;
		return false;
	}

	private boolean fileNameHasKeyword(String keywordToMatch, String filename) {
		if (!given(filename)) return false;
		return filename.toLowerCase().contains(keywordToMatch);
	}

	private List<Gathering> parseGatherings(Node rdfGatheringNode) throws CriticalParseFailure  {
		List<Gathering> gatherings = new ArrayList<>();

		Gathering gathering = new Gathering(parseId(rdfGatheringNode));
		parseAgents(rdfGatheringNode, gathering);

		gathering.setHigherGeography(getContents(MY_HIGHER_GEOGRAPHY, rdfGatheringNode));
		gathering.setCountry(getContents(MY_COUNTRY, rdfGatheringNode));
		gathering.setMunicipality(getContents(MY_MUNICIPALITY, rdfGatheringNode));
		if (!given(gathering.getMunicipality())) {
			gathering.setMunicipality(getContents(MY_COUNTY, rdfGatheringNode));
		}
		gathering.setBiogeographicalProvince(getContents(MY_BIOLOGICAL_PROVINCE, rdfGatheringNode));
		gathering.setProvince(getContents(MY_ADMINISTRATIVE_PROVINCE, rdfGatheringNode));
		gathering.setLocality(getContents(MY_LOCALITY, rdfGatheringNode));

		parseNotes(rdfGatheringNode, gathering);
		parseFacts(rdfGatheringNode, gathering);
		parseGatheringIssues(rdfGatheringNode, gathering);
		taxonCensusFromFacts(gathering);

		try {
			parseEventDate(rdfGatheringNode, gathering);
		} catch (DateValidationException e) {
			createEventDateIssue(gathering, e);
		}
		parseCoordinates(rdfGatheringNode, gathering);

		int unitGatheringIndex = 0;

		String samplingMethod = getResource(MY_SAMPLING_METHOD, rdfGatheringNode);

		parseExplicitlySetMediaIds(rdfGatheringNode, gathering.getMedia());

		for (Node rdfHasPartNode : rdfGatheringNode.getChildNodes(MZ_HAS_PART)) {
			for (Node rdfUnitNode : rdfHasPartNode.getChildNodes(MY_UNIT)) {
				if (rdfUnitNode.hasChildNodes(MY_UNIT_GATHERING)) {
					unitGatheringIndex++;
					Node unitGatheringNode = rdfUnitNode.getNode(MY_UNIT_GATHERING);
					Gathering unitGathering = gathering.copy();
					unitGathering.getUnits().clear();
					unitGathering.setGatheringId(getUnitGatheringId(gathering, unitGatheringIndex));
					gatherings.add(unitGathering);
					parseUnitGathering(unitGatheringNode, unitGathering);
					unitGathering.addUnit(parseUnit(rdfUnitNode, samplingMethod));
				} else {
					gathering.addUnit(parseUnit(rdfUnitNode, samplingMethod));
				}
			}
		}

		if (unitGatheringIndex == 0 || !gathering.getUnits().isEmpty()) {
			gatherings.add(gathering);
		}

		return sortUnits(gatherings);
	}

	private List<Gathering> sortUnits(List<Gathering> gatherings) {
		for (Gathering g : gatherings) {
			g.setUnits(sort(g.getUnits()));
		}
		return gatherings;
	}

	static ArrayList<Unit> sort(List<Unit> units) {
		if (units == null) throw new IllegalStateException("no units");
		if (units.size() < 2) return (ArrayList<Unit>) units;
		ArrayList<Unit> sorted = new ArrayList<>(units.size());
		Iterator<Unit> i = units.iterator();
		while (i.hasNext()) {
			Unit u = i.next();
			if (Boolean.TRUE.equals(u.isPrimarySpecimen())) {
				sorted.add(u);
				i.remove();
			}
		}
		i = units.iterator();
		while (i.hasNext()) {
			Unit u = i.next();
			if (RecordBasis.PRESERVED_SPECIMEN.equals(u.getRecordBasis())) {
				sorted.add(u);
				i.remove();
			}
		}
		sorted.addAll(units);
		return sorted;
	}

	private void parseGatheringIssues(Node rdfGatheringNode, Gathering gathering) {
		for (Node issueNode : rdfGatheringNode.getChildNodes(MZ_QUALITY_ISSUE)) {
			Quality issue = parseIssue(issueNode);
			if (isTimeIssue(issue)) gathering.createQuality().setTimeIssue(issue);
			else if (isLocationIssue(issue)) gathering.createQuality().setLocationIssue(issue);
			else gathering.createQuality().setIssue(issue);
		}
	}

	private boolean isLocationIssue(Quality issue) {
		return Quality.LOCATION_ISSUES.contains(issue.getIssue());
	}

	private boolean isTimeIssue(Quality issue) {
		return Quality.TIME_ISSUES.contains(issue.getIssue());
	}

	private void parseUnitGathering(Node unitGatheringNode, Gathering unitGathering) {
		if (unitGatheringNode.hasChildNodes(MY_DATE_BEGIN) || unitGatheringNode.hasChildNodes(MY_DATE_END)) {
			unitGathering.setEventDate(null);
			if (unitGathering.getQuality() != null) {
				unitGathering.getQuality().setTimeIssue(null);
			}
			try {
				parseEventDate(unitGatheringNode, unitGathering);
			} catch (DateValidationException e) {
				createEventDateIssue(unitGathering, e);
			}
		}
		if (unitGatheringNode.hasChildNodes(MY_COORDINATES_GRID_YKJ)) {
			unitGathering.setCoordinates(null);
			parseCoordinates(unitGatheringNode, unitGathering);
		}
	}

	private Qname getUnitGatheringId(Gathering gathering, int unitGatheringIndex) {
		return new Qname(gathering.getGatheringId().toString()+"#"+unitGatheringIndex);
	}

	private void parseCoordinates(Node rdfGatheringNode, Gathering gathering) {
		if (rdfGatheringNode.hasChildNodes(MY_COORDINATES_GRID_YKJ)) {
			String gridCoordinates = rdfGatheringNode.getNode(MY_COORDINATES_GRID_YKJ).getContents();
			gathering.setCoordinatesVerbatim(gridCoordinates + " FI KKJ27");
			try {
				gathering.setCoordinates(parseYKJCoordinates(gridCoordinates));
			} catch (DataValidationException e) {
				createCoordinateIssue(gathering, Type.YKJ, e);
			}
			return;
		}

		Qname coordinateSystem = getCoordinateSystem(rdfGatheringNode);
		Coordinates coordinates = parseCoordinates(rdfGatheringNode, gathering, coordinateSystem);
		if (coordinates != null) {
			parseCoordinateAccuracy(rdfGatheringNode, coordinates, gathering);
			gathering.setCoordinates(coordinates);
		}
	}

	private Coordinates parseCoordinates(Node rdfGatheringNode, Gathering gathering, Qname coordinateSystem) {
		if (coordinateSystem == null) return parseWgs84Coordinates(rdfGatheringNode, gathering);
		if (MY_COORDINATE_SYSTEM_YKJ.equals(coordinateSystem.toString())) {
			return parseYKJCoordinates(rdfGatheringNode, gathering);
		}
		if (MY_COORDINATE_SYSTEM_ETRS_TM35FIN.equals(coordinateSystem.toString())) {
			return parseEurefCoordinates(rdfGatheringNode, gathering);
		}
		return parseWgs84Coordinates(rdfGatheringNode, gathering);
	}

	private void parseCoordinateAccuracy(Node rdfGatheringNode, Coordinates coordinates, Gathering gathering) {
		if (rdfGatheringNode.hasChildNodes(MY_COORDINATE_RADIUS)) {
			String accuracy = rdfGatheringNode.getNode(MY_COORDINATE_RADIUS).getContents();
			try {
				coordinates.setAccuracyInMeters(Integer.valueOf(accuracy));
			} catch (NumberFormatException e) {
				// leave empty
			}
			String coordVerbatim = gathering.getCoordinatesVerbatim();
			if (coordVerbatim == null) coordVerbatim = "";
			gathering.setCoordinatesVerbatim(coordVerbatim + " (" + accuracy + "m)");
		}
	}

	private Qname getCoordinateSystem(Node rdfGatheringNode) {
		Qname coordinateSystem = null;
		if (rdfGatheringNode.hasChildNodes(MY_COORDINATE_SYSTEM)) {
			coordinateSystem = Qname.fromURI(rdfGatheringNode.getNode(MY_COORDINATE_SYSTEM).getAttribute(RDF_RESOURCE));
		}
		return coordinateSystem;
	}

	private Coordinates parseWgs84Coordinates(Node rdfGatheringNode, Gathering gathering) {
		if (rdfGatheringNode.hasChildNodes(MY_WGS84_LONGITUDE) && rdfGatheringNode.hasChildNodes(MY_WGS84_LATITUDE)) {
			String lat = rdfGatheringNode.getNode(MY_WGS84_LATITUDE).getContents();
			String lon = rdfGatheringNode.getNode(MY_WGS84_LONGITUDE).getContents();
			gathering.setCoordinatesVerbatim(lat + " " + lon + " " + Type.WGS84);
			lat = cleanCoordinate(lat);
			lon = cleanCoordinate(lon);
			try {
				return new Coordinates(Double.valueOf(lat), Double.valueOf(lon), Type.WGS84);
			} catch (NumberFormatException e) {
				createCoordinateIssue(gathering, Type.WGS84, new DataValidationException(e.getMessage()));
			} catch (DataValidationException e) {
				createCoordinateIssue(gathering, Type.WGS84, e);
			}
		}
		return null;
	}

	private Coordinates parseYKJCoordinates(Node rdfGatheringNode, Gathering gathering) {
		try {
			return parseMetricCoordinates(rdfGatheringNode, gathering, Type.YKJ);
		} catch (DataValidationException e) {
			createCoordinateIssue(gathering, Type.YKJ, e);
			return null;
		}
	}

	private Coordinates parseEurefCoordinates(Node rdfGatheringNode, Gathering gathering) {
		try {
			return parseMetricCoordinates(rdfGatheringNode, gathering, Type.EUREF);
		} catch (DataValidationException e) {
			createCoordinateIssue(gathering, Type.EUREF, e);
			return null;
		}
	}

	private Coordinates parseMetricCoordinates(Node rdfGatheringNode, Gathering gathering, Type type) throws DataValidationException {
		if (rdfGatheringNode.hasChildNodes(MY_LONGITUDE) && rdfGatheringNode.hasChildNodes(MY_LATITUDE)) {
			String lat = rdfGatheringNode.getNode(MY_LATITUDE).getContents().replace(",", ".");
			String lon = rdfGatheringNode.getNode(MY_LONGITUDE).getContents().replace(",", ".");
			if (!given(lat) || !given(lon)) return null;
			gathering.setCoordinatesVerbatim(lat + ":" + lon + " " + type.name());
			lat = cleanCoordinate(lat);
			lon = cleanCoordinate(lon);
			if (type == Type.YKJ) {
				return parseYKJCoordinates(lat+":"+lon);
			}
			return parseEurefCoordinates(lat+":"+lon);
		}
		return null;
	}



	private String cleanCoordinate(String c) {
		if (c == null) return null;
		return Utils.removeWhitespace(c.replace("E", "").replace("N", "").replace("~", ""));
	}

	private void parseEventDate(Node rdfGatheringNode, Gathering gathering) throws DateValidationException {
		if (rdfGatheringNode.hasChildNodes(MY_DATE_BEGIN)) {
			Date dateBegin = parseDate(rdfGatheringNode.getNode(MY_DATE_BEGIN).getContents());
			if (dateBegin != null) {
				if (rdfGatheringNode.hasChildNodes(MY_DATE_END)) {
					Date dateEnd = parseDate(rdfGatheringNode.getNode(MY_DATE_END).getContents());
					if (dateEnd != null) {
						gathering.setEventDate(new DateRange(dateBegin, dateEnd));
					} else {
						gathering.setEventDate(new DateRange(dateBegin));
					}
				} else {
					gathering.setEventDate(new DateRange(dateBegin));
				}
			}
		} else if (rdfGatheringNode.hasChildNodes(MY_DATE_END)) {
			Date dateEnd = parseDate(rdfGatheringNode.getNode(MY_DATE_END).getContents());
			if (dateEnd != null) {
				gathering.setEventDate(new DateRange(null, dateEnd));
			}
		} else if (rdfGatheringNode.hasChildNodes(MY_DATE_VERBATIM)) {
			String verbatim = rdfGatheringNode.getNode(MY_DATE_VERBATIM).getContents();
			if (verbatim.length() < 4) return;
			if (looksLikeIsoDate(verbatim)) {
				parseEventDateTime(verbatim, gathering);
			} else {
				Date date = parseDate(verbatim);
				if (date != null) {
					gathering.setEventDate(new DateRange(date));
				}
			}
		}
	}

	private boolean looksLikeIsoDate(String verbatim) {
		if (Utils.countNumberOf("-", verbatim) == 2) {
			String[] parts = verbatim.split(Pattern.quote("-"));
			if (parts.length == 3) {
				if (parts[0].length() == 4 && parts[1].length() == 2 && parts[2].length() == 2) return true;
			}
		}
		if (!verbatim.contains("/")) return false;
		String[] parts = verbatim.split(Pattern.quote("/"));
		if (parts.length != 2) return false;
		return parts[0].length() >= 4;
	}

	private Unit parseUnit(Node rdfUnitNode, String samplingMethod) throws IllegalArgumentException, CriticalParseFailure {
		Unit unit = new Unit(parseId(rdfUnitNode));
		parseIdentifications(rdfUnitNode, unit);
		parseTypes(rdfUnitNode, unit);
		parseTaxonIdOrVerbatim(rdfUnitNode, unit);
		parseIsTypeSpecimen(unit);
		parseRecordBasis(rdfUnitNode, unit);
		parsePrimarySpecimen(rdfUnitNode, unit);
		parseTaxonConfidence(rdfUnitNode, unit);
		parseAbundance(rdfUnitNode, unit);
		parseSex(rdfUnitNode, unit);
		parseLifeStageAlive(rdfUnitNode, unit);
		parseWild(rdfUnitNode, unit);
		parsePlantStatusCode(rdfUnitNode, unit);
		parseNotes(rdfUnitNode, unit);
		parseFacts(rdfUnitNode, unit);
		parseUnitIssues(rdfUnitNode, unit);
		parseSamples(rdfUnitNode, unit);
		parseKeywords(rdfUnitNode, unit);
		if (given(samplingMethod)) {
			unit.setSamplingMethodUsingQname(Qname.fromURI(samplingMethod));
		}
		parseExplicitlySetMediaIds(rdfUnitNode, unit.getMedia());
		return unit;
	}

	private Qname parseId(Node node) {
		return Qname.fromURI(node.getAttribute(RDF_ABOUT));
	}

	private void parseIdentifications(Node rdfUnitNode, Unit unit) throws CriticalParseFailure {
		ArrayList<IdentificationEvent> identifications = new ArrayList<>();
		for (Node rdfHasPartNode : rdfUnitNode.getChildNodes(MZ_HAS_PART)) {
			for (Node rdfIdentificationNode : rdfHasPartNode.getChildNodes(MY_IDENTIFICATION)) {
				if (rdfIdentificationNode.hasChildNodes()) {
					identifications.add(parseIdentification(rdfIdentificationNode));
				}
			}
		}
		if (identifications.isEmpty()) return;
		Collections.sort(identifications);
		unit.setIdentifications(identifications);
	}

	private IdentificationEvent parseIdentification(Node rdfIdentificationNode) throws CriticalParseFailure {
		IdentificationEvent identification = new IdentificationEvent();
		identification.setId(parseId(rdfIdentificationNode));
		if (preferred(rdfIdentificationNode)) {
			identification.setPreferred(true);
		}
		identification.setTaxonID(Qname.fromURI(getResource(MY_TAXON_URI, rdfIdentificationNode)));
		identification.setTaxon(parseTaxonName(rdfIdentificationNode));
		identification.setAuthor(getContents(MY_AUTHOR, rdfIdentificationNode));
		identification.setTaxonSpecifier(parseTaxonInfraName(rdfIdentificationNode));
		identification.setTaxonSpecifierAuthor(getContents(MY_INFRA_AUTHOR, rdfIdentificationNode));

		identification.setDet(getContents(MY_DET, rdfIdentificationNode));
		identification.setDetDate(getContents(MY_DET_DATE, rdfIdentificationNode));

		identification.setNotes(getContents(MY_IDENTIFICATION_NOTES, rdfIdentificationNode));

		parseFacts(rdfIdentificationNode, identification);
		return identification;
	}

	private void parseTypes(Node rdfUnitNode, Unit unit) throws CriticalParseFailure {
		ArrayList<TypeSpecimen> types = new ArrayList<>();
		for (Node rdfHasPartNode : rdfUnitNode.getChildNodes(MZ_HAS_PART)) {
			if (rdfHasPartNode.hasChildNodes(MY_TYPE_SPECIMEN)) {
				Node rdfTypeNode = rdfHasPartNode.getNode(MY_TYPE_SPECIMEN);
				if (rdfTypeNode.hasChildNodes()) {
					types.add(parseType(rdfTypeNode));
				}
			}
		}
		if (types.isEmpty()) return;
		Collections.sort(types);
		unit.setTypes(types);
	}

	private TypeSpecimen parseType(Node rdfTypeNode) throws CriticalParseFailure {
		TypeSpecimen type = new TypeSpecimen();
		type.setId(parseId(rdfTypeNode));
		String taxonName = getContents(MY_TYPE_SPECIES, rdfTypeNode);
		String subspecies = getContents(MY_TYPE_SUBSPECIES, rdfTypeNode);

		type.setTaxon(taxonName);
		type.setAuthor(getContents(MY_TYPE_AUTHOR, rdfTypeNode));
		type.setTaxonSpecifier(subspecies);
		type.setTaxonSpecifierAuthor(getContents(MY_TYPE_SUBSPECIES_AUTHOR, rdfTypeNode));

		type.setTypif(getContents(MY_TYPIF, rdfTypeNode));
		type.setTypifDate(getContents(MY_TYPIF_DATE, rdfTypeNode));

		type.setStatus(Qname.fromURI(getResource(MY_TYPE_STATUS, rdfTypeNode)));
		type.setVerification(Qname.fromURI(getResource(MY_TYPE_VERIFICATION, rdfTypeNode)));

		type.setBasionymePublication(getContents(MY_TYPE_BASIONYME_PUBL, rdfTypeNode));
		type.setPublication(getContents(MY_TYPE_PUBL, rdfTypeNode));

		type.setNotes(getContents(MY_TYPE_NOTES, rdfTypeNode));

		parseFacts(rdfTypeNode, type);
		return type;
	}

	private void parsePlantStatusCode(Node rdfUnitNode, Unit unit) {
		String code = getResource(MY_PLANT_STATUS_CODE, rdfUnitNode);
		if (code == null) return;
		unit.setPlantStatusCodeUsingQname(Qname.fromURI(code));
	}

	private void parseWild(Node rdfUnitNode, Unit unit) {
		String wild = getResource(MY_WILD, rdfUnitNode);
		if (wild == null) return;
		if (WILD_WILD.equals(wild)) {
			unit.setWild(true);
		}
		if (WILD_NON_WILD.equals(wild)) {
			unit.setWild(false);
		}
	}

	private void parsePrimarySpecimen(Node rdfUnitNode, Unit unit) {
		String val = getContents(MY_PRIMARY_SPECIMEN, rdfUnitNode);
		if (val == null) return;
		unit.setPrimarySpecimen("true".equals(val));
	}


	private void parseTaxonIdOrVerbatim(Node rdfUnitNode, Unit unit) {
		IdentificationEvent preferredIdentification = resolveIdentificationToUse(unit);
		TypeSpecimen preferredType = resolveTypeToUse(unit);

		if (preferredIdentification != null) {
			unit.setReportedTaxonId(preferredIdentification.getTaxonID());
			if (given(preferredIdentification.getTaxonSpecifier())) {
				unit.setTaxonVerbatim(preferredIdentification.getTaxon() + " " + preferredIdentification.getTaxonSpecifier());
				if (given(preferredIdentification.getTaxonSpecifierAuthor())) {
					unit.setAuthor(preferredIdentification.getTaxonSpecifierAuthor());
				} else {
					unit.setAuthor(preferredIdentification.getAuthor());
				}
			} else {
				unit.setTaxonVerbatim(preferredIdentification.getTaxon());
				unit.setAuthor(preferredIdentification.getAuthor());
			}
			unit.setDet(preferredIdentification.getDet());
			return;
		}
		if (rdfUnitNode.hasChildNodes(MY_INFORMAL_NAME_STRING)) {
			String informalNameString =  rdfUnitNode.getNode(MY_INFORMAL_NAME_STRING).getContents();
			if (given(informalNameString)) {
				unit.setTaxonVerbatim(informalNameString);
				return;
			}
		}
		if (preferredType != null) {
			if (given(preferredType.getTaxonSpecifier())) {
				unit.setTaxonVerbatim(preferredType.getTaxon() + " " + preferredType.getTaxonSpecifier());
				if (given(preferredType.getTaxonSpecifierAuthor())) {
					unit.setAuthor(preferredType.getTaxonSpecifierAuthor());
				} else {
					unit.setAuthor(preferredType.getAuthor());
				}
			} else {
				unit.setTaxonVerbatim(preferredType.getTaxon());
				unit.setAuthor(preferredType.getAuthor());
			}
			return;
		}
		unit.setTaxonVerbatim("Biota");
	}

	private void parseAbundance(Node rdfUnitNode, Unit unit) {
		unit.setAbundanceString(getContents(MY_INDIVIDUAL_COUNT, rdfUnitNode));
		if (!given(unit.getAbundanceString())) {
			unit.setAbundanceString(getContents(MY_COUNT, rdfUnitNode));
		}
		if (!given(unit.getAbundanceString())) {
			setAbundanceIfNumericElseToNotes(unit, getContents(MY_ABUNDANCE_STRING, rdfUnitNode));
		}
		if (!given(unit.getAbundanceString())) {
			setAbundanceIfNumericElseToNotes(unit, getContents(MY_POPULATION_ABUNDANCE, rdfUnitNode));
		}
	}

	private void parseSamples(Node rdfUnitNode, Unit unit) throws IllegalArgumentException, CriticalParseFailure {
		for (Node rdfHasPartNode : rdfUnitNode.getChildNodes(MZ_HAS_PART)) {
			for (Node rdfSampleNode : rdfHasPartNode.getChildNodes(MF_SAMPLE)) {
				unit.addSample(parseSample(rdfSampleNode));
			}
		}
	}

	private Sample parseSample(Node rdfSampleNode) throws IllegalArgumentException, CriticalParseFailure {
		Sample sample = new Sample(parseId(rdfSampleNode));

		if (rdfSampleNode.hasChildNodes(MF_INDIVIDUALS_IN_PREPARATION)) {
			sample.setMultiple(getResource(MF_INDIVIDUALS_IN_PREPARATION, rdfSampleNode).equals(MULTIPLE));
		}
		for (Node collection : rdfSampleNode.getChildNodes(MF_COLLECTION_ID)) {
			sample.setCollectionId(Qname.fromURI(getResource(collection)));
		}
		for (Node type : rdfSampleNode.getChildNodes(MF_PREPARATION_TYPE)) {
			sample.setTypeUsingQname(Qname.fromURI(getResource(type)));
		}
		for (Node quality : rdfSampleNode.getChildNodes(MF_QUALITY)) {
			sample.setQualityUsingQname(Qname.fromURI(getResource(quality)));
		}
		for (Node status : rdfSampleNode.getChildNodes(MF_STATUS)) {
			sample.setStatusUsingQname(Qname.fromURI(getResource(status)));
		}
		for (Node material : rdfSampleNode.getChildNodes(MF_MATERIAL)) {
			sample.setMaterialUsingQname(Qname.fromURI(getResource(material)));
		}
		for (Node datasetId : rdfSampleNode.getChildNodes(MF_DATASET_ID)) {
			sample.addKeyword(getResource(datasetId));
		}
		for (Node additionalIds : rdfSampleNode.getChildNodes(MF_ADDITIONAL_IDS)) {
			sample.addKeyword(additionalIds.getContents());
		}
		for (Node publication : rdfSampleNode.getChildNodes(MF_PUBLICATION)) {
			sample.addKeyword(publication.getContents());
		}

		sample.setNotes(getNotes(rdfSampleNode, MF_NOTES, MF_CONDITION, MF_QUALITY_NOTES));

		parseFacts(rdfSampleNode, sample);
		for (Node hasPart : rdfSampleNode.getChildNodes(MZ_HAS_PART)) {
			for (Node rdfObject : hasPart.getChildNodes()) {
				parseFacts(rdfObject, sample);
			}
		}
		return sample;
	}

	private String getNotes(Node rdfSampleNode, String ... fields) {
		List<String> notes = new ArrayList<>();
		for (String field : fields) {
			for (Node node : rdfSampleNode.getChildNodes(field)) {
				if (given(node.getContents())) {
					notes.add(node.getContents());
				}
			}
		}
		return Utils.catenate(notes, ", ");
	}

	private void setAbundanceIfNumericElseToNotes(Unit unit, String s) {
		if (!given(s)) return;
		if (isInteger(s)) {
			unit.setAbundanceString(s);
		} else {
			unit.setNotes(unit.getNotes() == null ? "" : unit.getNotes() + "\n" + s);
		}
	}

	private boolean isInteger(String s) {
		try {
			Integer.valueOf(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void parseUnitIssues(Node rdfUnitNode, Unit unit) {
		if (rdfUnitNode.hasChildNodes(MY_TAXON_CONFIDENCE)) {
			Node confidenceNode = rdfUnitNode.getNode(MY_TAXON_CONFIDENCE);
			unit.setReportedTaxonConfidence(toConfidence(confidenceNode));
		}

	}

	private TaxonConfidence toConfidence(Node confidenceNode) {
		if (confidenceNode.hasAttribute(RDF_RESOURCE)) {
			return (TaxonConfidence) EnumToProperty.getInstance().get(Qname.fromURI(confidenceNode.getAttribute(RDF_RESOURCE)));
		}
		return (TaxonConfidence) EnumToProperty.getInstance().get(new Qname(confidenceNode.getContents()));
	}

	private void parseIsTypeSpecimen(Unit unit) {
		for (TypeSpecimen t : unit.getTypes()) {
			if (t.verified()) {
				unit.setTypeSpecimen(true);
				return;
			}
		}
	}

	private TypeSpecimen resolveTypeToUse(Unit unit) {
		if (unit.getTypes().isEmpty()) return null;
		for (TypeSpecimen t : unit.getTypes()) {
			if (t.hasIdentification()) return t;
		}
		return null;
	}

	private IdentificationEvent resolveIdentificationToUse(Unit unit) {
		if (unit.getIdentifications().isEmpty()) return null;
		for (IdentificationEvent e : unit.getIdentifications()) {
			if (e.hasIdentification()) return e;
		}
		return null;
	}

	private boolean preferred(Node rdfIdentificationNode) {
		return rdfIdentificationNode.hasChildNodes(MY_PREFERRED_IDENTIFICATION) && rdfIdentificationNode.getNode(MY_PREFERRED_IDENTIFICATION).getContents().equalsIgnoreCase(YES);
	}

	private void parseLifeStageAlive(Node rdfUnitNode, Unit unit) {
		Qname lifeStage = Qname.fromURI(getResource(MY_LIFE_STAGE, rdfUnitNode));
		Qname plantLifeStage = Qname.fromURI(getResource(MY_PLANT_LIFE_STAGE, rdfUnitNode));
		String alive = getContents(MY_ALIVE, rdfUnitNode);
		parseLifeStageAlive(unit, lifeStage, plantLifeStage, alive);
	}

	private void parseSex(Node rdfUnitNode, Unit unit) {
		Sex sex = getSex(getResource(MY_SEX, rdfUnitNode));
		unit.setSex(sex);
	}

	private void parseTaxonConfidence(Node rdfUnitNode, Unit unit) {
		unit.setReportedTaxonConfidence(getTaxonConfidence(getResource(MY_TAXON_CONFIDENCE, rdfUnitNode)));
	}

	private void parseRecordBasis(Node rdfUnitNode, Unit unit) {
		unit.setRecordBasis(getRecordBasis(getResource(MY_RECORD_BASIS, rdfUnitNode)));
	}

	private String parseTaxonName(Node rdfIdentificationNode) {
		return getContents(MY_TAXON, rdfIdentificationNode);
	}

	private String parseTaxonInfraName(Node rdfIdentificationNode) {
		Qname infraRank = Qname.fromURI(getResource(MY_INFRA_RANK, rdfIdentificationNode));
		String infraEpithet = getContents(MY_INFRA_EPITHET, rdfIdentificationNode);
		if (!given(infraEpithet)) return null;
		String infraRankString = INFRA_RANKS.get(infraRank);
		if (!given(infraRankString)) return null;
		return infraRankString + " " + infraEpithet;
	}

	private void parseNotes(Node rdfNode, BaseModel dwModel) {
		dwModel.setNotes(getContents(MY_NOTES, rdfNode));
	}

	private void parseKeywords(Node rdfDocumentNode, Document document) {
		parseKeywords(rdfDocumentNode).forEach(k->document.addKeyword(k));
	}

	private void parseKeywords(Node rdfUnitNode, Unit unit) {
		parseKeywords(rdfUnitNode).forEach(k->unit.addKeyword(k));
	}

	private List<String> parseKeywords(Node rdfNode) {
		List<String> keywords = new ArrayList<>();
		for (Node keyword : rdfNode.getChildNodes(MY_KEYWORD)) {
			parseKeyword(keywords, keyword);
		}
		for (Node keyword : rdfNode.getChildNodes(MY_KEYWORDS)) {
			parseKeyword(keywords, keyword);
		}
		for (Node tag : rdfNode.getChildNodes(MY_TAG)) {
			parseKeyword(keywords, tag);
		}
		for (Node dataset : rdfNode.getChildNodes(MY_DATASET_ID)) {
			parseKeyword(keywords, dataset);
		}
		for (Node additionalId : rdfNode.getChildNodes(MY_ADDITIONAL_IDS)) {
			parseKeyword(keywords, additionalId);
		}
		for (Node originalSpecimenId : rdfNode.getChildNodes(MY_ORIGINAL_SPECIMEN_ID)) {
			parseKeyword(keywords, originalSpecimenId);
		}
		return keywords;
	}

	private void parseKeyword(List<String> keywords, Node keyword) {
		if (isResource(keyword)) {
			keywords.add(getResource(keyword));
		} else {
			keywords.add(keyword.getContents());
		}
	}

	private void parseFacts(Node rdfNode, BaseModel dwModel) throws CriticalParseFailure {
		if (rdfNode == null) return;
		for (Node n : rdfNode.getChildNodes()) {
			if (IGNORED_FACTS.contains(n.getName())) continue;
			if (n.getName().equals(MY_DYNAMIC_PROPERTIES)) {
				addDynamicProperties(n.getContents(), dwModel);
				continue;
			}
			String fact = new Qname(n.getName()).toURI();
			if (isResource(n)) {
				dwModel.addFact(fact, getResource(n));
			} else {
				dwModel.addFact(fact, n.getContents());
			}
		}
	}

	private void addDynamicProperties(String contents, BaseModel dwModel) throws CriticalParseFailure {
		JSONObject json = null;
		try {
			json = new JSONObject(contents);
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid JSON: " + contents);
		}
		for (String key : json.getKeys()) {
			dwModel.addFact(key, json.getString(key));
		}
	}

	private boolean isResource(Node n) {
		return n.hasAttribute(RDF_RESOURCE);
	}

	private void parseEditors(Node rdfDocumentNode, Document dwDocument) {
		Set<String> editors = new HashSet<>();
		for (Node editor : rdfDocumentNode.getChildNodes(MZ_EDITOR)) {
			if (isResource(editor)) {
				editors.add(getResource(editor));
			} else {
				editors.add(editor.getContents());
			}
		}
		for (Node creator : rdfDocumentNode.getChildNodes(MZ_CREATOR)) {
			editors.add(getResource(creator));
		}
		for (String s : editors) {
			dwDocument.addEditorUserId(s);
		}
	}

	private void parseAgents(Node rdfNode, Gathering gathering) {
		for (Node leg : rdfNode.getChildNodes(MY_LEG)) {
			gathering.addTeamMember(leg.getContents());
		}
		for (Node legUserId : rdfNode.getChildNodes(MY_LEGUSERID)) {
			gathering.addObserverUserId(legUserId.getContents());
		}
	}

	private void parseCollectionId(Node document, DwRoot dwModel) throws CriticalParseFailure {
		dwModel.setCollectionId(Qname.fromURI(getResource(MY_COLLECTION_ID, document)));
	}

	private boolean shouldConceal(Node document) {
		String publicityRestrictions = getResource(MZ_PUBLICITY_RESTRICTIONS, document);
		if (publicityRestrictions == null) return false;
		return PUBLICITY_PRIVATE.equals(publicityRestrictions) || PUBLICITY_PROTECTED.equals(publicityRestrictions);
	}

	private String getResource(Node n) {
		return n.getAttribute(RDF_RESOURCE);
	}

	private String getResource(String field, Node n) {
		if (!n.hasChildNodes(field)) return null;
		Node r = n.getNode(field);
		if (r.hasAttribute(RDF_RESOURCE)) {
			return r.getAttribute(RDF_RESOURCE);
		}
		return null;
	}
}
