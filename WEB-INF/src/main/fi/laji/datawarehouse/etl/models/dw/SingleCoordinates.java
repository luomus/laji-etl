package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.utils.Utils;

@Embeddable
public class SingleCoordinates {

	private Double lat;
	private Double lon;
	private Type type;

	public SingleCoordinates() {}

	public SingleCoordinates(double lat, double lon, Type type) {
		setLat(lat);
		setLon(lon);
		this.type = type;
	}

	@FileDownloadField(name="Lat(N)", order=1)
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		if (lat == null) {
			this.lat = null;
		} else {
			this.lat = Utils.round(lat, 6);
		}
	}

	@FileDownloadField(name="Lon(E)", order=2)
	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		if (lon == null) {
			this.lon = null;
		} else {
			this.lon = Utils.round(lon, 6);
		}
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public String toString() {
		if (type == null) {
			return Utils.removeWhitespace(Utils.debugS(lat, lon));
		}
		return Utils.removeWhitespace(Utils.debugS(lat, lon, type));
	}

}
