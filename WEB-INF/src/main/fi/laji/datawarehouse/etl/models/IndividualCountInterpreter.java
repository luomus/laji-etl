package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fi.luomus.commons.utils.Utils;

public class IndividualCountInterpreter {

	public static final int MAX_VALID_COUNT = 300000;
	public static final String ATLAS_CODE = "ATL:";
	public static final String DEAD_CODE = "XXX:";
	private static final String BIRD_CALENDAR_YEAR_CODE = "kv";

	public static class AbundanceStringPart {
		public String prefix = "";
		public String number = "";
		public String postfix = "";
		@Override
		public String toString() {
			return "[prefix=" + prefix + " number=" + number + " postfix=" + postfix + "]";
		}
	}

	public static int interpret(String abudanceString) {
		if (!given(abudanceString)) return 1;
		abudanceString = abudanceString.trim();

		if (abudanceString.equals("absent")) return 0;

		if (abudanceString.length() > 30) return 1;

		if (isInteger(abudanceString)) {
			Integer i = toValidCount(Integer.valueOf(abudanceString));
			if (i == null) return 1;
			return i;
		}

		if (containsRoadString(abudanceString)) return 1;

		abudanceString = abudanceString.replace("n.", "");
		abudanceString = abudanceString.replace(">", "");
		abudanceString = abudanceString.replace("<", "");
		abudanceString = abudanceString.replace("[", "");
		abudanceString = abudanceString.replace("]", "");
		abudanceString = removeDoubles(abudanceString);

		List<AbundanceStringPart> parts = toParts(abudanceString);
		if (parts.isEmpty()) return 1;

		List<Integer> numbers = toNumbers(parts);
		if (numbers.isEmpty()) {
			return 1;
		}
		int sum = sum(numbers);
		if (sum > 10000 && !isInteger(abudanceString)) {
			return 1;
		}
		return sum;
	}

	private static String removeDoubles(String abudanceString) {
		abudanceString = abudanceString.replace(",", ".");
		abudanceString = Utils.removeNumbersAroundChar(abudanceString, '.');
		return abudanceString;
	}

	private static final List<String> ROAD_STRINGS = Utils.list("tie", "katu", "raitti", "linja", "polku", "kuja", "ranta", "kaari");

	private static boolean isInteger(String abundanceString) {
		if (abundanceString.startsWith("0") && !abundanceString.equals("0")) return false;
		try {
			if (abundanceString.startsWith("-")) return false;
			Integer.valueOf(abundanceString);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static boolean containsRoadString(String abudanceString) {
		abudanceString = abudanceString.toLowerCase();
		for (String roadString : ROAD_STRINGS) {
			if (abudanceString.contains(roadString)) return true;
		}
		return false;
	}

	private static Integer sum(List<Integer> numbers) {
		int c = 0;
		boolean hasInvalid = false;
		for (Integer i : numbers) {
			i = toValidCount(i);
			if (i != null) {
				c += i;
			} else {
				hasInvalid = true;
			}
		}
		if (hasInvalid) return 1;
		return c;
	}

	private static List<Integer> toNumbers(List<AbundanceStringPart> parts) {
		List<Integer> numbersOutsideBrackets = new ArrayList<>();
		List<Integer> numbersInsideBrackets = new ArrayList<>();
		boolean insideBrackets = false;
		List<Integer> listToUse = numbersOutsideBrackets;

		List<Integer> minOfNumericRanges = new ArrayList<>();

		boolean precededByUnitOfArea = false;
		int i = -1;
		for (AbundanceStringPart part : parts) {
			i++;

			if (part.prefix.contains("(")) {
				insideBrackets = true;
				listToUse = numbersInsideBrackets;
			}
			if (insideBrackets && part.prefix.contains(")")) {
				insideBrackets = false;
				listToUse = numbersOutsideBrackets;
			}
			if (precededByUnitOfArea) {
				precededByUnitOfArea = false;
				continue;
			}
			if (isHtmlEntity(part)) continue;
			if (possiblePostcode(part)) continue;
			if (possibleMultiplication(part)) continue; // area (10 x 5)
			if (possibleTime(part)) continue;
			if (startsWithUnitOfArea(i, part)) continue;
			if (followedByUnitOfArea(part, parts, i)) {
				precededByUnitOfArea = true;
				continue;
			}
			if (codePart(part)) continue;
			if (birdCalendarYearCode(part)) continue;
			if (birdFlockCount(part)) continue;

			int c;
			try {
				c = Integer.valueOf(part.number);
			} catch (NumberFormatException e) {
				continue; // Too large number
			}

			if (part.prefix.endsWith("-") && minOfNumericRanges.isEmpty()) {
				minOfNumericRanges.add(1);
			}
			if (part.postfix.startsWith("-") || part.prefix.endsWith("-")) {
				minOfNumericRanges.add(c);
			} else {
				if (!minOfNumericRanges.isEmpty()) {
					int min = min(minOfNumericRanges);
					listToUse.add(min);
					minOfNumericRanges.clear();
				}
				listToUse.add(c);
			}
		}
		if (!minOfNumericRanges.isEmpty()) {
			int min = min(minOfNumericRanges);
			listToUse.add(min);
		}

		if (numbersOutsideBrackets.isEmpty()) {
			return numbersInsideBrackets;
		}
		return numbersOutsideBrackets;
	}

	private static boolean isHtmlEntity(AbundanceStringPart part) {
		return part.prefix.contains("&#");
	}

	private static boolean birdFlockCount(AbundanceStringPart part) {
		return part.postfix.equals("a");
	}

	private static boolean birdCalendarYearCode(AbundanceStringPart part) {
		return part.postfix.equalsIgnoreCase(BIRD_CALENDAR_YEAR_CODE) || part.postfix.equalsIgnoreCase("-"+BIRD_CALENDAR_YEAR_CODE);
	}

	private static boolean codePart(AbundanceStringPart part) {
		return part.prefix.equalsIgnoreCase(ATLAS_CODE) || part.prefix.equalsIgnoreCase(DEAD_CODE);
	}

	private static boolean startsWithUnitOfArea(int i, AbundanceStringPart part) {
		return i == 0 && part.number.equals("2") &&
				(part.prefix.equalsIgnoreCase("M") || part.prefix.equalsIgnoreCase("M^") || part.prefix.equalsIgnoreCase("DM") || part.prefix.equalsIgnoreCase("DM^"));
	}

	private static boolean possibleTime(AbundanceStringPart part) {
		return part.prefix.equals(":") || part.postfix.equals(":") || part.prefix.equals(".") || part.postfix.equals(".");
	}

	private static boolean possibleMultiplication(AbundanceStringPart part) {
		return part.prefix.equalsIgnoreCase("x") || part.postfix.equalsIgnoreCase("x");
	}

	private static boolean possiblePostcode(AbundanceStringPart part) {
		return part.number.startsWith("0") && !part.number.equals("0");
	}

	private static int min(List<Integer> rangeNumbers) {
		int min = rangeNumbers.get(0);
		for (int i : rangeNumbers) {
			min = Math.min(min, i);
		}
		return min;
	}

	private static boolean followedByUnitOfArea(AbundanceStringPart part, List<AbundanceStringPart> parts, int i) {
		if (part.postfix.toLowerCase().startsWith("m") || part.postfix.toLowerCase().startsWith("m^") || part.postfix.toLowerCase().startsWith("dm") || part.postfix.toLowerCase().startsWith("dm^")) {
			if (parts.size() > i+1) {
				AbundanceStringPart next = parts.get(i+1);
				if (next.number.equals("2")) {
					if (next.postfix.isEmpty() || containsNonAlphas(next.postfix)) {
						return true;
					}
				}
			}
		}
		if (
				part.postfix.contains("aari") ||
				part.postfix.contains("acre") ||
				part.postfix.contains("tunnland") ||
				part.postfix.contains("neliö") ) {
			return true;
		}
		return false;
	}

	private static boolean containsNonAlphas(String s) {
		for (char c : s.toCharArray()) {
			if (!Character.isAlphabetic(c)) return true;
		}
		return false;
	}

	public static List<AbundanceStringPart> toParts(String abudanceString) {
		if (!given(abudanceString)) return Collections.emptyList();
		List<AbundanceStringPart> parts = new ArrayList<>();

		String currentPrefix = "";
		String currentNumber = "";
		boolean inDigit = false;
		for (char c : abudanceString.toCharArray()) {
			if (Character.isDigit(c)) {
				currentNumber += c;
				inDigit = true;
			} else {
				if (inDigit) {
					finishPart(parts, currentPrefix, currentNumber);
					currentPrefix = "";
					currentNumber = "";
				}
				currentPrefix += c;
				inDigit = false;
			}
		}

		finishPart(parts, currentPrefix, currentNumber);

		if (inDigit) currentPrefix = "";

		if (parts.isEmpty()) return parts;

		AbundanceStringPart prev = null;
		for (AbundanceStringPart part : parts) {
			if (prev != null) {
				prev.postfix = part.prefix;
			}
			prev = part;
		}

		AbundanceStringPart last = parts.get(parts.size() - 1);
		last.postfix = currentPrefix.trim();
		return parts;
	}

	private static void finishPart(List<AbundanceStringPart> parts, String currentPrefix, String currentNumber) {
		if (given(currentNumber)) {
			AbundanceStringPart part = new AbundanceStringPart();
			part.prefix = currentPrefix.trim();
			part.number = currentNumber;
			parts.add(part);
		}
	}

	private static Integer toValidCount(int i) {
		if (i < 0) return null;
		if (i > MAX_VALID_COUNT) return null;
		return i;
	}

	private static boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

}
