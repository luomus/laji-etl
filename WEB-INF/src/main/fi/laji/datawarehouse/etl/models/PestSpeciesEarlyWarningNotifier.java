package fi.laji.datawarehouse.etl.models;

import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;

public class PestSpeciesEarlyWarningNotifier extends InvasiveSpeciesEarlyWarningNotifier {

	public PestSpeciesEarlyWarningNotifier(DAO dao, ErrorReporter errorReporter, Config config) {
		super(dao, errorReporter, config);
	}

	@Override
	public int sendWarnings() throws Exception {
		Set<Qname> notifiedUnits = dao.getPestSpeciesEarlyWarningReportedUnits();

		Set<Qname> newNotifications = sendWarnings(notifiedUnits);

		notifiedUnits.addAll(newNotifications);
		dao.setPestSpeciesEarlyWarningReportedUnits(notifiedUnits);

		return newNotifications.size();
	}

	@Override
	protected List<JoinedRow> getFromPrivateDw() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PRIVATE)
				.setDefaultFilters(false)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.build(),
				1, 10000);
		Filters filters = query.getFilters()
				.setFirstLoadedSameOrAfter(oneYearAgo())
				.setTime("-1825/0")
				.setIndividualCountMin(1)
				.setCountryId(Const.FINLAND);
		Set<Qname> warningTaxa = dao.getTaxonomyDAO().getTaxonContainer().getAdminStatusFilter().getFilteredTaxa(new Qname("MX.quarantinePlantPest"));
		if (warningTaxa.isEmpty()) throw new ETLException("No pest species early warning species defined!");
		for (Qname taxonId : warningTaxa) {
			filters.setTaxonId(taxonId);
		}
		return dao.getPrivateVerticaDAO().getQueryDAO().getRawList(query);
	}

	@Override
	protected String title() {
		return "LAJI.FI varhaisvaroitus karanteenituhoojista";
	}

	@Override
	protected String receiverAddress() {
		return config.get("PestSpeciesEarlyWarningsTo");
	}

}
