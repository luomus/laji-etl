package fi.laji.datawarehouse.etl.models.dw.geo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class Geo implements Iterable<Feature> {

	private static final int NUMBER_OF_POINTS_IN_LINE_LIMIT = 500;
	private static final int NUMBER_OF_POINTS_IN_POLYGON_LIMIT = 100;
	private static final String WKT_GEOMETRYCOLLECTION = "GEOMETRYCOLLECTION";
	private static final String GEOMETRY_COLLECTION = "GeometryCollection";
	public static final String MULTIPOLYGON = "MultiPolygon";
	public static final String POLYGON = "Polygon";
	public static final String LINE_STRING = "LineString";
	public static final String RADIUS = "radius";
	public static final String MULTIPOINT = "MultiPoint";
	public static final String POINT = "Point";
	public static final String FEATURES = "features";
	public static final String GEOMETRY = "geometry";
	public static final String GEOMETRIES = "geometries";
	public static final String CRS = "crs";
	public static final String ACCURACY_IN_METERS = "accuracyInMeters";
	public static final String FEATURE_COLLECTION = "FeatureCollection";
	public static final String TYPE = "type";

	private final Type crs;
	private final List<Feature> features = new ArrayList<>();
	private Integer accuracyInMeters;

	public Geo(Type crs) {
		this.crs = crs;
	}

	public List<Feature> getFeatures() {
		return features;
	}

	public Geo addFeature(Feature feature) {
		if (feature == null) throw new IllegalArgumentException("Null feature");
		this.features.add(feature);
		return this;
	}

	public Type getCRS() {
		return crs;
	}

	@Override
	public Iterator<Feature> iterator() {
		return features.iterator();
	}

	public JSONObject getGeoJSON() {
		JSONObject json = new JSONObject().setString(TYPE, FEATURE_COLLECTION).setString(CRS, crs.name());
		if (accuracyInMeters != null) {
			json.setInteger(ACCURACY_IN_METERS, accuracyInMeters);
		}
		for (Feature f : features) {
			JSONObject featureJson = new JSONObject().setString(TYPE, "Feature");
			featureJson.setObject(GEOMETRY, f.getGeoJSON());
			json.getArray(FEATURES).appendObject(featureJson);
		}
		return json;
	}

	public String getWKT() {
		if (features.size() == 0) return "GEOMETRYCOLLECTION EMPTY";
		if (features.size() == 1) return features.get(0).getWKT();
		StringBuilder b = new StringBuilder();
		b.append(WKT_GEOMETRYCOLLECTION).append("(");
		Iterator<Feature> i = features.iterator();
		while (i.hasNext()) {
			Feature f = i.next();
			b.append(f.getWKT());
			if (i.hasNext()) b.append(", ");
		}
		b.append(")");
		return b.toString();
	}

	public List<String> getWKTList() {
		List<String> list = new ArrayList<>();
		for (Feature f : features) {
			list.add(f.getWKT());
		}
		return list;
	}

	public static Geo getBoundingBox(Coordinates coordinates) {
		if (coordinates.getType() == Type.YKJ) {
			coordinates = coordinates.toFullYkjLength();
		}
		if (coordinates.checkIfIsPoint()) {
			return new Geo(coordinates.getType()).addFeature(new Point(coordinates.getLatMin(), coordinates.getLonMin()));
		}
		double latMin = coordinates.getLatMin();
		double latMax = coordinates.getLatMax();
		double lonMin = coordinates.getLonMin();
		double lonMax = coordinates.getLonMax();
		if (latMin == latMax || lonMin == lonMax) {
			return new Geo(coordinates.getType()).addFeature(new Line(new Coordinate[] {
					new Coordinate(lonMin, latMin),
					new Coordinate(lonMax, latMax)
			}));
		}
		Polygon polygon = new Polygon(new Coordinate[] {
				new Coordinate(lonMin, latMin),
				new Coordinate(lonMin, latMax),
				new Coordinate(lonMax, latMax),
				new Coordinate(lonMax, latMin),
				new Coordinate(lonMin, latMin)
		});
		return new Geo(coordinates.getType()).addFeature(polygon);
	}

	public Geo validate() throws DataValidationException {
		if (features.isEmpty()) throw new DataValidationException("Geography does not have any features!");
		return this;
	}

	public static Geo fromWKT(String wkt, Type crs) throws DataValidationException {
		Geo geo = new Geo(crs);
		try {
			Geometry g = new WKTReader().read(wkt);
			for (Geometry subGeometry : getFlatGeometries(g)) {
				addFeature(geo, subGeometry);
			}
		} catch (ParseException | IllegalArgumentException e) {
			if (given(e.getMessage())) throw new DataValidationException("Invalid WKT: " + e.getMessage(), e);
			throw new DataValidationException("Invalid WKT", e);
		}
		return geo.validate();
	}

	private static void addFeature(Geo geo, Geometry subGeometry) throws DataValidationException {
		if (POINT.equals(subGeometry.getGeometryType())) {
			geo.addFeature(new Point(subGeometry).validate());
		} else if (LINE_STRING.equals(subGeometry.getGeometryType())) {
			geo.addFeature(new Line(subGeometry).validate());
		} else {
			geo.addFeature(new Polygon(removeInteriorRing(subGeometry)).validate());
		}
	}

	private static Geometry removeInteriorRing(Geometry geometry) {
		if (geometry instanceof org.locationtech.jts.geom.Polygon) {
			org.locationtech.jts.geom.Polygon polygon = (org.locationtech.jts.geom.Polygon) geometry;
			if (polygon.getNumInteriorRing() > 0) {
				return polygon.getFactory().createPolygon(polygon.getExteriorRing());
			}
		}
		return geometry;
	}

	private static List<Geometry> getFlatGeometries(Geometry geometry) {
		List<Geometry> geometries = new ArrayList<>();
		int numGeometries = geometry.getNumGeometries();
		if (numGeometries > 1) {
			for (int i = 0; i < numGeometries; i++) {
				Geometry subGeometry = geometry.getGeometryN(i);
				geometries.addAll(getFlatGeometries(subGeometry));
			}
		} else {
			geometries.add(geometry);
		}
		return geometries;
	}

	public static Geo fromGeoJSON(JSONObject geoJSON) throws DataValidationException {
		Type crs = null;
		try {
			crs = Type.valueOf(geoJSON.getString(CRS));
		} catch (Exception e) {
			throw new DataValidationException("GeoJSON is missing the required \"" + CRS + "\" expansion parameter. Valid values are: " + Utils.debugS((Object[])Type.values()));
		}
		Integer accuracyInMeters = null;
		if (geoJSON.hasKey(ACCURACY_IN_METERS)) {
			accuracyInMeters = geoJSON.getInteger(ACCURACY_IN_METERS);
		}
		Geo geo = new Geo(crs).setAccuracyInMeters(accuracyInMeters);
		if (geoJSON.hasKey(FEATURES)) {
			for (JSONObject feature : geoJSON.getArray(FEATURES).iterateAsObject()) {
				JSONObject geometry = feature.getObject(GEOMETRY);
				parseGeometry(geo, geometry);
			}
		} else if (geoJSON.hasKey(TYPE)) {
			parseGeometry(geo, geoJSON);
		}
		return geo.validate();
	}

	private static void parseGeometry(Geo geo, JSONObject geometry) throws DataValidationException {
		String type = geometry.getString(TYPE);
		Type crs = geo.getCRS();
		if (type.equals(GEOMETRY_COLLECTION)) {
			for (JSONObject innerGeometry : geometry.getArray(GEOMETRIES).iterateAsObject()) {
				parseGeometry(geo, innerGeometry);
			}
			return;
		}
		if (type.equals(MULTIPOLYGON)) {
			parseMultiPolygon(geometry, geo);
			return;
		}
		if (type.equals(MULTIPOINT)) {
			parseMultiPoint(geometry, geo);
			return;
		}
		Feature f = parseFeature(geometry, crs);
		if (f != null) geo.addFeature(f);
	}

	private static void parseMultiPoint(JSONObject geometry, Geo geo) throws DataValidationException {
		for (JSONArray pointArray : geometry.getArray("coordinates").iterateAsArray()) {
			geo.addFeature(Point.fromCoordinateArray(pointArray));
		}
	}

	private static void parseMultiPolygon(JSONObject geometry, Geo geo) throws DataValidationException {
		for (JSONArray polygonArray : geometry.getArray("coordinates").iterateAsArray()) {
			geo.addFeature(Polygon.fromCoordinateArray(polygonArray.iterateAsArray().get(0)));
		}
	}

	public static Feature parseFeature(JSONObject geometry, Type crs) throws DataValidationException {
		String type = geometry.getString(TYPE);
		if (type.equals(POINT)) {
			if (isPointWithRadius(geometry)) {
				return Polygon.fromGeoJSONPointWithRadius(geometry, crs);
			}
			return Point.fromGeoJSON(geometry);
		}
		if (type.equals(LINE_STRING)) return Line.fromGeoJSON(geometry);
		if (type.equals(POLYGON)) return Polygon.fromGeoJSON(geometry);
		throw new DataValidationException("Unknown feature type " + type);
	}

	private static boolean isPointWithRadius(JSONObject geometry) {
		if (!geometry.hasKey(RADIUS)) return false;
		try {
			double radius = geometry.getDouble("radius");
			return radius > 0;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String toString() {
		List<String> s = new ArrayList<>();
		for (Feature f : features) {
			s.add(f.toString());
		}
		return s.toString();
	}

	public int size() {
		int c = 0;
		for (Feature f : getFeatures()) {
			c += f.size();
		}
		return c;
	}

	public Coordinates getBoundingBox() throws DataValidationException {
		if (this.getFeatures().isEmpty()) throw new IllegalStateException("Defined geography must not be empty!");
		double latMin = Double.MAX_VALUE;
		double latMax = -1*Double.MAX_VALUE; // Note: Double.MIN_VALUE is not negative!!! (it represents the smallest possible positive non zero. DUH!)
		double lonMin = Double.MAX_VALUE;
		double lonMax = -1*Double.MAX_VALUE;
		for (Feature feature : this) {
			latMin = Math.min(latMin, feature.getLatMin());
			latMax = Math.max(latMax, feature.getLatMax());
			lonMin = Math.min(lonMin, feature.getLonMin());
			lonMax = Math.max(lonMax, feature.getLonMax());
		}
		return getBoundingBox(latMin, latMax, lonMin, lonMax);
	}

	private Coordinates getBoundingBox(double latMin, double latMax, double lonMin, double lonMax) throws DataValidationException {
		try {
			Coordinates c;
			if (latMin == latMax && lonMin == lonMax) {
				c = new Coordinates(latMin, lonMin, this.getCRS());
			} else {
				c = new Coordinates(latMin, latMax, lonMin, lonMax, this.getCRS());
			}
			int bbAccuracy = c.calculateBoundingBoxAccuracy();
			if (accuracyInMeters == null) {
				c.setAccuracyInMeters(bbAccuracy);
			} else {
				c.setAccuracyInMeters(Math.max(bbAccuracy, accuracyInMeters));
			}
			return c;
		} catch (Exception e) {
			throw new DataValidationException("Coordinates defined in geography are invalid: " + Utils.debugS(latMin, latMax, lonMin, lonMax, this.getCRS()), e);
		}
	}

	public Integer getAccuracyInMeters() {
		return accuracyInMeters;
	}

	public Geo setAccuracyInMeters(Integer accuracyInMeters) {
		this.accuracyInMeters = accuracyInMeters;
		return this;
	}

	public Geo convertComplex() throws DataValidationException {
		validate();
		ListIterator<Feature> i = getFeatures().listIterator();
		int complexFeatureCount = 0;
		while (i.hasNext()) {
			Feature f = i.next();
			if (f instanceof Polygon) {
				Polygon polygon = (Polygon) f;
				if (isTooComplex(polygon)) {
					convertComplexPolygon(i, polygon);
				}
				complexFeatureCount++;
			} else if (f instanceof Line) {
				Line line = (Line) f;
				if (isTooComplex(line)) {
					convertComplexLine(i, line);
				}
				complexFeatureCount++;
			}
		}
		if (complexFeatureCount > 10) {
			return Geo.getBoundingBox(this.getBoundingBox());
		}
		return this;
	}

	private boolean isTooComplex(Line line) {
		return line.size() > NUMBER_OF_POINTS_IN_LINE_LIMIT;
	}

	private boolean isTooComplex(Polygon polygon) {
		return polygon.size() > NUMBER_OF_POINTS_IN_POLYGON_LIMIT;
	}

	private void convertComplexLine(ListIterator<Feature> i, Line l) {
		try {
			Line simplified = l.simplify(crs).validate();
			i.set(simplified);
		} catch (DataValidationException e) {
			// leave original
		}
	}

	private void convertComplexPolygon(ListIterator<Feature> i, Polygon p) {
		try {
			Polygon simplified = p.simplify(crs).validate();
			if (!isTooComplex(simplified)) {
				i.set(simplified);
				return;
			}
		} catch (DataValidationException e) {
			// try convex hull
		}
		try {
			Polygon convexHull = p.convexHullAndSimplify(crs).validate();
			i.set(convexHull);
		} catch (DataValidationException e2) {
			// leave original
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accuracyInMeters == null) ? 0 : accuracyInMeters.hashCode());
		result = prime * result + ((crs == null) ? 0 : crs.hashCode());
		result = prime * result + ((features == null) ? 0 : features.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Geo other = (Geo) obj;
		if (accuracyInMeters == null) {
			if (other.accuracyInMeters != null)
				return false;
		} else if (!accuracyInMeters.equals(other.accuracyInMeters))
			return false;
		if (crs != other.crs)
			return false;
		if (features == null) {
			if (other.features != null)
				return false;
		} else if (!features.equals(other.features))
			return false;
		return true;
	}

	private static boolean given(String message) {
		return message != null && !message.isEmpty();
	}

}
