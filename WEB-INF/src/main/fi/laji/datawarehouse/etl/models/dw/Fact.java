package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.utils.Utils;

@MappedSuperclass
public class Fact {

	private String fact;
	private String value;
	private Integer integerValue;
	private Double decimalValue;

	public Fact(String fact, String value) {
		setFact(fact);
		setValue(value);
		setDecimalIntegerValue(value);
	}

	private void setDecimalIntegerValue(String value) {
		if (value.contains("e")) return; // Scientific notation not desirable
		try {
			Double decimalValue = Double.valueOf(value.replace(",", "."));
			if (decimalValue.longValue() >= Integer.MAX_VALUE) return; // large numbers are not desirable
			setDecimalValue(decimalValue);
			int intValue = Integer.valueOf(value);
			setIntegerValue(intValue);
		} catch (Exception e) {}
	}

	@Deprecated // for Hibernate
	public Fact() {}

	@Id @Column(name="value_text") // Other parts of the composite key are defined in the entity
	@FileDownloadField(name="Value", order=2)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		value = Util.trimToByteLength(value, 1000);
		this.value = value;
	}

	@Column(name="value_integer")
	@FileDownloadField(name="IntValue", order=3)
	@FieldDefinition(ignoreForETL=true)
	public Integer getIntegerValue() {
		return integerValue;
	}

	public void setIntegerValue(Integer integerValue) {
		this.integerValue = integerValue;
	}

	@Column(name="value_decimal")
	@FileDownloadField(name="DecimalValue", order=4)
	@FieldDefinition(ignoreForETL=true)
	public Double getDecimalValue() {
		return decimalValue;
	}

	public void setDecimalValue(Double decimalValue) {
		if (decimalValue != null && decimalValue < DOUBLE_VERTICA_MAX_VALUE) {
			this.decimalValue = decimalValue;
		} else {
			this.decimalValue = null;
		}
	}

	@Transient
	@FileDownloadField(name="Fact", order=1)
	public String getFact() {
		return fact;
	}

	public void setFact(String fact) {
		fact = Util.trimToByteLength(fact, 1000);
		this.fact = fact;
	}

	private static final double DOUBLE_VERTICA_MAX_VALUE = Double.valueOf("1E+30").doubleValue();

	@Override
	public String toString() {
		return Utils.debugS(fact, value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fact == null) ? 0 : fact.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fact other = (Fact) obj;
		if (fact == null) {
			if (other.fact != null)
				return false;
		} else if (!fact.equals(other.fact))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}