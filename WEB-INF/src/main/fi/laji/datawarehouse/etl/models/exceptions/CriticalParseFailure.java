package fi.laji.datawarehouse.etl.models.exceptions;

public class CriticalParseFailure extends Exception {

	private static final long serialVersionUID = 4434305063817896426L;

	private String fields = "";

	public CriticalParseFailure(String message) {
		super(message);
	}

	public CriticalParseFailure(String message, Throwable cause) {
		super(message, cause);
	}

	public CriticalParseFailure(Throwable cause) {
		super(cause);
	}

	public String getFields() {
		return fields;
	}

	public CriticalParseFailure addField(String field) {
		if (this.fields.isEmpty()) {
			this.fields = field;
		} else {
			this.fields = field + "." + this.fields;
		}
		return this;
	}

	@Override
	public String getMessage() {
		if (fields.isEmpty()) return super.getMessage();
		return fields + ": " + super.getMessage();
	}

}
