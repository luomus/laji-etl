package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;

/**
 * Convert incoming data to Laji-ETL internal document structure.
 * @param <K> The data which may be string, JsonObject or something else
 */
public interface Harmonizer<K> {

	/**
	 * Convert data (possibly containing multiple documents) in harmonized Data Warehouse format
	 * @param data
	 * @return
	 * @throws CriticalParseFailure if parsing failed so critically, that all entries can not be saved to out pipe: 1) Invalid document structure or 2) missing document ids. These will not be reattempted.
	 * @throws UnknownHarmonizingFailure if parsing failed for some unknown reason (like unable to call APIs). These will be reattempted.
	 */
	List<DwRoot> harmonize(K data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure;

}
