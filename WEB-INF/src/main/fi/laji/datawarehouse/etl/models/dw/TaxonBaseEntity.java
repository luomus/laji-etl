package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

@MappedSuperclass
public class TaxonBaseEntity {

	private Long key;
	private Qname id;
	private Qname nameAccordingTo;
	private Qname taxonRank;
	private String scientificName;
	private String scientificNameDisplayName;
	private String author;
	private String nameFinnish;
	private String nameSwedish;
	private String nameEnglish;
	private Qname redListStatus;
	private Qname redListStatusGroup;
	private boolean finnish = false;
	private boolean invasive = false;
	private boolean species = false;
	private boolean cursiveName = false;
	private boolean sensitive = false;
	private boolean virva = false;
	private Long taxonomicOrder;

	private Long parentKey;

	private Long superdomainKey;
	private Long domainKey;
	private Long kingdomKey;
	private Long subkingdomKey;
	private Long infrakingdomKey;
	private Long superphylumKey;
	private Long phylumKey;
	private Long subphylumKey;
	private Long infraphylumKey;
	private Long superdivisionKey;
	private Long divisionKey;
	private Long subdivisionKey;
	private Long infradivisionKey;
	private Long superclassKey;
	private Long classKey;
	private Long subclassKey;
	private Long infraclassKey;
	private Long parvclassKey;
	private Long superorderKey;
	private Long orderKey;
	private Long suborderKey;
	private Long infraorderKey;
	private Long parvorderKey;
	private Long superfamilyKey;
	private Long familyKey;
	private Long subfamilyKey;
	private Long tribeKey;
	private Long subtribeKey;
	private Long supergenusKey;
	private Long genusKey;
	private Long nothogenusKey;
	private Long subgenusKey;
	private Long sectionKey;
	private Long subsectionKey;
	private Long seriesKey;
	private Long subseriesKey;
	private Long infragenericTaxonKey;
	private Long speciesAggregateKey;
	private Long speciesKey;
	private Long infraspecificTaxonKey;
	private Long subspecificAggregateKey;
	private Long subspeciesKey;
	private Long varietyKey;
	private Long subvarietyKey;
	private Long formKey;
	private Long subformKey;
	private Long hybridKey;
	private Long anamorphKey;
	private Long aggregateKey;
	private Long ecotypeKey;
	private Long populationGroupKey;
	private Long intergenericHybridKey;
	private Long infragenericHybridKey;
	private Long cultivarKey;
	private Long nothospeciesKey;
	private Long nothosubspeciesKey;
	private Long groupKey;
	private Long grexKey;

	private String speciesScientificName;
	private String speciesNameFinnish;
	private String speciesNameSwedish;
	private String speciesNameEnglish;
	private Long speciesTaxonomicOrder;

	private String euringCode;
	private String birdlifeCode;
	private Integer euringNumber;

	private String primaryHabitat;
	private Integer occurrenceCount;
	private Integer occurrenceCountFinland;

	public TaxonBaseEntity() {}

	public TaxonBaseEntity(Qname id) {
		this.id = id;
		this.key = parseTaxonKey(id);
	}

	public static Long parseTaxonKey(Qname id) {
		if (id == null) return null;
		try {
			return Long.valueOf(id.toString().replace("MX.", ""));
		} catch (Exception e) {
			return -1L;
		}
	}

	@Id
	@FieldDefinition(ignoreForQuery=true)
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getId() {
		if (id == null) return null;
		return id.toURI();
	}

	public void setId(String id) {
		if (id == null) {
			this.id = null;
		} else {
			this.id = Qname.fromURI(id);
		}
	}

	@Column(name="display_name")
	public String getScientificNameDisplayName() {
		return scientificNameDisplayName;
	}

	public void setScientificNameDisplayName(String scientificNameDisplayName) {
		this.scientificNameDisplayName = trim(scientificNameDisplayName);
	}

	@Column(name="species")
	public boolean isSpecies() {
		return species;
	}

	public void setSpecies(boolean isSpecies) {
		species = isSpecies;
	}

	@Column(name="cursivename")
	public boolean isCursiveName() {
		return cursiveName;
	}

	public void setCursiveName(boolean isCursiveName) {
		cursiveName = isCursiveName;
	}

	@Column(name="sensitive")
	public boolean isSensitive() {
		return sensitive;
	}

	public void setSensitive(boolean isSensitive) {
		sensitive = isSensitive;
	}

	@Column(name="virva")
	public boolean isVirva() {
		return virva;
	}

	public void setVirva(boolean isVirva) {
		virva = isVirva;
	}

	@Column(name="taxon_rank")
	public String getTaxonRank() {
		if (taxonRank == null) return null;
		return taxonRank.toURI();
	}

	@Deprecated // for hibernate; use setTaxonRank(Qname)
	public void setTaxonRank(String taxonRankURI) {
		if (taxonRankURI == null) {
			this.taxonRank = null;
		}
		else {
			this.taxonRank = Qname.fromURI(taxonRankURI);
		}
	}

	@Transient
	public void setTaxonRankUsingQname(Qname taxonRank) {
		this.taxonRank = taxonRank;
	}

	@Column(name="scientific_name")
	public String getScientificName() {
		return scientificName;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = trim(scientificName);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = trim(author);
	}

	@Column(name="name_finnish")
	public String getNameFinnish() {
		return nameFinnish;
	}

	public void setNameFinnish(String nameFinnish) {
		this.nameFinnish = trim(nameFinnish);
	}

	@Column(name="name_swedish")
	public String getNameSwedish() {
		return nameSwedish;
	}

	public void setNameSwedish(String nameSwedish) {
		this.nameSwedish = trim(nameSwedish);
	}

	@Column(name="name_english")
	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = trim(nameEnglish);
	}

	@Column(name="superdomain_key")
	public Long getSuperdomainId() {
		return superdomainKey;
	}

	public void setSuperdomainId(Long superdomainKey) {
		this.superdomainKey = superdomainKey;
	}

	@Column(name="domain_key")
	public Long getDomainId() {
		return domainKey;
	}

	public void setDomainId(Long domainKey) {
		this.domainKey = domainKey;
	}

	@Column(name="kingdom_key")
	public Long getKingdomId() {
		return kingdomKey;
	}

	public void setKingdomId(Long kingdomKey) {
		this.kingdomKey = kingdomKey;
	}

	@Column(name="subkingdom_key")
	public Long getSubkingdomId() {
		return subkingdomKey;
	}

	public void setSubkingdomId(Long subkingdomKey) {
		this.subkingdomKey = subkingdomKey;
	}

	@Column(name="superphylum_key")
	public Long getSuperphylumId() {
		return superphylumKey;
	}

	public void setSuperphylumId(Long superphylumKey) {
		this.superphylumKey = superphylumKey;
	}

	@Column(name="infrakingdom_key")
	public Long getInfrakingdomId() {
		return infrakingdomKey;
	}

	public void setInfrakingdomId(Long infrakingdomKey) {
		this.infrakingdomKey = infrakingdomKey;
	}

	@Column(name="superdivision_key")
	public Long getSuperdivisionId() {
		return superdivisionKey;
	}

	public void setSuperdivisionId(Long superdivisionKey) {
		this.superdivisionKey = superdivisionKey;
	}

	@Column(name="infraphylum_key")
	public Long getInfraphylumId() {
		return infraphylumKey;
	}

	public void setInfraphylumId(Long infraphylumKey) {
		this.infraphylumKey = infraphylumKey;
	}

	@Column(name="infradivision_key")
	public Long getInfradivisionId() {
		return infradivisionKey;
	}

	public void setInfradivisionId(Long infradivisionKey) {
		this.infradivisionKey = infradivisionKey;
	}

	@Column(name="phylum_key")
	public Long getPhylumId() {
		return phylumKey;
	}

	public void setPhylumId(Long phylumKey) {
		this.phylumKey = phylumKey;
	}

	@Column(name="division_key")
	public Long getDivisionId() {
		return divisionKey;
	}

	public void setDivisionId(Long divisionKey) {
		this.divisionKey = divisionKey;
	}

	@Column(name="subphylum_key")
	public Long getSubphylumId() {
		return subphylumKey;
	}

	public void setSubphylumId(Long subphylumKey) {
		this.subphylumKey = subphylumKey;
	}

	@Column(name="subdivision_key")
	public Long getSubdivisionId() {
		return subdivisionKey;
	}

	public void setSubdivisionId(Long subdivisionKey) {
		this.subdivisionKey = subdivisionKey;
	}

	@Column(name="superclass_key")
	public Long getSuperclassId() {
		return superclassKey;
	}

	public void setSuperclassId(Long superclassKey) {
		this.superclassKey = superclassKey;
	}

	@Column(name="class_key")
	public Long getClassId() {
		return classKey;
	}

	public void setClassId(Long classKey) {
		this.classKey = classKey;
	}

	@Column(name="subclass_key")
	public Long getSubclassId() {
		return subclassKey;
	}

	public void setSubclassId(Long subclassKey) {
		this.subclassKey = subclassKey;
	}

	@Column(name="infraclass_key")
	public Long getInfraclassId() {
		return infraclassKey;
	}

	public void setInfraclassId(Long infraclassKey) {
		this.infraclassKey = infraclassKey;
	}

	@Column(name="superorder_key")
	public Long getSuperorderId() {
		return superorderKey;
	}

	public void setSuperorderId(Long superorderKey) {
		this.superorderKey = superorderKey;
	}

	@Column(name="order_key")
	public Long getOrderId() {
		return orderKey;
	}

	public void setOrderId(Long orderKey) {
		this.orderKey = orderKey;
	}

	@Column(name="suborder_key")
	public Long getSuborderId() {
		return suborderKey;
	}

	public void setSuborderId(Long suborderKey) {
		this.suborderKey = suborderKey;
	}

	@Column(name="infraorder_key")
	public Long getInfraorderId() {
		return infraorderKey;
	}

	public void setInfraorderId(Long infraorderKey) {
		this.infraorderKey = infraorderKey;
	}

	@Column(name="superfamily_key")
	public Long getSuperfamilyId() {
		return superfamilyKey;
	}

	public void setSuperfamilyId(Long superfamilyKey) {
		this.superfamilyKey = superfamilyKey;
	}

	@Column(name="family_key")
	public Long getFamilyId() {
		return familyKey;
	}

	public void setFamilyId(Long familyKey) {
		this.familyKey = familyKey;
	}

	@Column(name="subfamily_key")
	public Long getSubfamilyId() {
		return subfamilyKey;
	}

	public void setSubfamilyId(Long subfamilyKey) {
		this.subfamilyKey = subfamilyKey;
	}

	@Column(name="tribe_key")
	public Long getTribeId() {
		return tribeKey;
	}

	public void setTribeId(Long tribeKey) {
		this.tribeKey = tribeKey;
	}

	@Column(name="subtribe_key")
	public Long getSubtribeId() {
		return subtribeKey;
	}

	public void setSubtribeId(Long subtribeKey) {
		this.subtribeKey = subtribeKey;
	}

	@Column(name="supergenus_key")
	public Long getSupergenusId() {
		return supergenusKey;
	}

	public void setSupergenusId(Long supergenusKey) {
		this.supergenusKey = supergenusKey;
	}

	@Column(name="genus_key")
	public Long getGenusId() {
		return genusKey;
	}

	public void setGenusId(Long genusKey) {
		this.genusKey = genusKey;
	}

	@Column(name="nothogenus_key")
	public Long getNothogenusId() {
		return nothogenusKey;
	}

	public void setNothogenusId(Long nothogenusKey) {
		this.nothogenusKey = nothogenusKey;
	}

	@Column(name="subgenus_key")
	public Long getSubgenusId() {
		return subgenusKey;
	}

	public void setSubgenusId(Long subgenusKey) {
		this.subgenusKey = subgenusKey;
	}

	@Column(name="infragenerictaxon_key")
	public Long getInfragenericTaxonId() {
		return infragenericTaxonKey;
	}

	public void setInfragenericTaxonId(Long infragenericTaxonKey) {
		this.infragenericTaxonKey = infragenericTaxonKey;
	}

	@Column(name="speciesaggregate_key")
	public Long getSpeciesAggregateId() {
		return speciesAggregateKey;
	}

	public void setSpeciesAggregateId(Long speciesAggregateKey) {
		this.speciesAggregateKey = speciesAggregateKey;
	}

	@Column(name="species_key")
	public Long getSpeciesId() {
		return speciesKey;
	}

	public void setSpeciesId(Long speciesKey) {
		this.speciesKey = speciesKey;
	}

	@Column(name="infraspecifictaxon_key")
	public Long getInfraspecificTaxonId() {
		return infraspecificTaxonKey;
	}

	public void setInfraspecificTaxonId(Long infraspecificTaxonKey) {
		this.infraspecificTaxonKey = infraspecificTaxonKey;
	}

	@Column(name="subspecificaggregate_key")
	public Long getSubspecificAggregateId() {
		return subspecificAggregateKey;
	}

	public void setSubspecificAggregateId(Long subspecificAggregateKey) {
		this.subspecificAggregateKey = subspecificAggregateKey;
	}

	@Column(name="subspecies_key")
	public Long getSubspeciesId() {
		return subspeciesKey;
	}

	public void setSubspeciesId(Long subspeciesKey) {
		this.subspeciesKey = subspeciesKey;
	}

	@Column(name="variety_key")
	public Long getVarietyId() {
		return varietyKey;
	}

	public void setVarietyId(Long varietyKey) {
		this.varietyKey = varietyKey;
	}

	@Column(name="form_key")
	public Long getFormId() {
		return formKey;
	}

	public void setFormId(Long formKey) {
		this.formKey = formKey;
	}

	@Column(name="hybrid_key")
	public Long getHybridId() {
		return hybridKey;
	}

	public void setHybridId(Long hybridKey) {
		this.hybridKey = hybridKey;
	}

	@Column(name="anamorph_key")
	public Long getAnamorphId() {
		return anamorphKey;
	}

	public void setAnamorphId(Long anamorphKey) {
		this.anamorphKey = anamorphKey;
	}

	@Column(name="aggregate_key")
	public Long getAggregateId() {
		return aggregateKey;
	}

	public void setAggregateId(Long aggregateKey) {
		this.aggregateKey = aggregateKey;
	}

	@Column(name="ecotype_key")
	public Long getEcotypeId() {
		return ecotypeKey;
	}

	public void setEcotypeId(Long ecotypeKey) {
		this.ecotypeKey = ecotypeKey;
	}

	@Column(name="populationgroup_key")
	public Long getPopulationGroupId() {
		return populationGroupKey;
	}

	public void setPopulationGroupId(Long populationGroupKey) {
		this.populationGroupKey = populationGroupKey;
	}

	@Column(name="intergenerichybrid_key")
	public Long getIntergenericHybridId() {
		return intergenericHybridKey;
	}

	public void setIntergenericHybridId(Long intergenericHybridKey) {
		this.intergenericHybridKey = intergenericHybridKey;
	}

	@Column(name="infragenerichybrid_key")
	public Long getInfragenericHybridId() {
		return infragenericHybridKey;
	}

	public void setInfragenericHybridId(Long infragenericHybridKey) {
		this.infragenericHybridKey = infragenericHybridKey;
	}

	@Column(name="cultivar_key")
	public Long getCultivarId() {
		return cultivarKey;
	}

	public void setCultivarId(Long cultivarKey) {
		this.cultivarKey = cultivarKey;
	}

	@Column(name="series_key")
	public Long getSeriesId() {
		return seriesKey;
	}

	public void setSeriesId(Long seriesKey) {
		this.seriesKey = seriesKey;
	}

	@Column(name="section_key")
	public Long getSectionId() {
		return sectionKey;
	}

	public void setSectionId(Long sectionKey) {
		this.sectionKey = sectionKey;
	}

	@Column(name="subsection_key")
	public Long getSubsectionId() {
		return subsectionKey;
	}

	public void setSubsectionId(Long subsectionKey) {
		this.subsectionKey = subsectionKey;
	}

	@Column(name="subseries_key")
	public Long getSubseriesId() {
		return subseriesKey;
	}

	public void setSubseriesId(Long subseriesKey) {
		this.subseriesKey = subseriesKey;
	}

	@Column(name="subvariety_key")
	public Long getSubvarietyId() {
		return subvarietyKey;
	}

	public void setSubvarietyId(Long subvarietyKey) {
		this.subvarietyKey = subvarietyKey;
	}

	@Column(name="subform_key")
	public Long getSubformId() {
		return subformKey;
	}

	public void setSubformId(Long subformKey) {
		this.subformKey = subformKey;
	}

	@Column(name="parent_key")
	public Long getParentId() {
		return parentKey;
	}

	public void setParentId(Long parentKey) {
		this.parentKey = parentKey;
	}

	@Column(name="parvclass_key")
	public Long getParvclassId() {
		return parvclassKey;
	}

	public void setParvclassId(Long parvclassKey) {
		this.parvclassKey = parvclassKey;
	}

	@Column(name="parvorder_key")
	public Long getParvorderId() {
		return parvorderKey;
	}

	public void setParvorderId(Long parvorderKey) {
		this.parvorderKey = parvorderKey;
	}

	@Column(name="nothospecies_key")
	public Long getNothospeciesId() {
		return nothospeciesKey;
	}

	public void setNothospeciesId(Long nothospeciesKey) {
		this.nothospeciesKey = nothospeciesKey;
	}

	@Column(name="nothosubspecies_key")
	public Long getNothosubspeciesId() {
		return nothosubspeciesKey;
	}

	public void setNothosubspeciesId(Long nothosubspeciesKey) {
		this.nothosubspeciesKey = nothosubspeciesKey;
	}

	@Column(name="group_key")
	public Long getGroupId() {
		return groupKey;
	}

	public void setGroupId(Long groupKey) {
		this.groupKey = groupKey;
	}

	@Column(name="grex_key")
	public Long getGrexId() {
		return grexKey;
	}

	public void setGrexId(Long grexKey) {
		this.grexKey = grexKey;
	}

	@Column(name="red_list_status")
	public String getRedListStatus() {
		if (redListStatus == null) return null;
		return redListStatus.toURI();
	}

	@Deprecated // for hibernate; use setTaxonRank(Qname)
	public void setRedListStatus(String redListStatusURI) {
		if (redListStatusURI == null) {
			this.redListStatus = null;
		}
		else {
			this.redListStatus = Qname.fromURI(redListStatusURI);
		}
	}

	@Transient
	public void setRedListStatusUsingQname(Qname redListStatus) {
		this.redListStatus = redListStatus;
	}

	@Column(name="red_list_status_group")
	public String getRedListStatusGroup() {
		if (redListStatusGroup == null) return null;
		return redListStatusGroup.toURI();
	}

	@Deprecated // for hibernate; use setTaxonRank(Qname)
	public void setRedListStatusGroup(String redListStatusGroupURI) {
		if (redListStatusGroupURI == null) {
			this.redListStatusGroup = null;
		}
		else {
			this.redListStatusGroup = Qname.fromURI(redListStatusGroupURI);
		}
	}

	@Transient
	public void setRedListStatusGroupUsingQname(Qname redListStatusGroup) {
		this.redListStatusGroup = redListStatusGroup;
	}

	@Column(name="finnish")
	public boolean isFinnish() {
		return finnish;
	}

	public void setFinnish(boolean finnish) {
		this.finnish = finnish;
	}

	@Column(name="invasive")
	public boolean isInvasive() {
		return invasive;
	}

	public void setInvasive(boolean invasive) {
		this.invasive = invasive;
	}

	@Column(name="species_name_swedish")
	public String getSpeciesNameSwedish() {
		return speciesNameSwedish;
	}

	public void setSpeciesNameSwedish(String speciesNameSwedish) {
		this.speciesNameSwedish = trim(speciesNameSwedish);
	}

	@Column(name="species_name_finnish")
	public String getSpeciesNameFinnish() {
		return speciesNameFinnish;
	}

	public void setSpeciesNameFinnish(String speciesNameFinnish) {
		this.speciesNameFinnish = trim(speciesNameFinnish);
	}

	@Column(name="species_name_english")
	public String getSpeciesNameEnglish() {
		return speciesNameEnglish;
	}

	public void setSpeciesNameEnglish(String speciesNameEnglish) {
		this.speciesNameEnglish = trim(speciesNameEnglish);
	}

	@Column(name="species_scientific_name")
	public String getSpeciesScientificName() {
		return speciesScientificName;
	}

	public void setSpeciesScientificName(String speciesScientificName) {
		this.speciesScientificName = trim(speciesScientificName);
	}

	@Column(name="taxonomic_order")
	public Long getTaxonomicOrder() {
		return taxonomicOrder;
	}

	public void setTaxonomicOrder(Long taxonomicOrder) {
		this.taxonomicOrder = taxonomicOrder;
	}

	@Column(name="species_taxonomic_order")
	public Long getSpeciesTaxonomicOrder() {
		return speciesTaxonomicOrder;
	}

	public void setSpeciesTaxonomicOrder(Long taxonomicOrder) {
		this.speciesTaxonomicOrder = taxonomicOrder;
	}

	@Column(name="euring_code")
	public String getEuringCode() {
		return euringCode;
	}

	public void setEuringCode(String euringCode) {
		this.euringCode = trim(euringCode);
	}

	@Column(name="birdlife_code")
	public String getBirdlifeCode() {
		return birdlifeCode;
	}

	public void setBirdlifeCode(String birdlifeCode) {
		this.birdlifeCode = trim(birdlifeCode);
	}

	@Column(name="euring_number")
	public Integer getEuringNumber() {
		return euringNumber;
	}

	public void setEuringNumber(Integer euringNumber) {
		this.euringNumber = euringNumber;
	}

	@Column(name="primary_habitat")
	public String getPrimaryHabitat() {
		return primaryHabitat;
	}

	public void setPrimaryHabitat(String primaryHabitat) {
		this.primaryHabitat = trim(primaryHabitat);
	}

	@Column(name="occurrence_count")
	public Integer getOccurrenceCount() {
		return occurrenceCount;
	}

	public void setOccurrenceCount(Integer occurrenceCount) {
		this.occurrenceCount = occurrenceCount;
	}

	@Column(name="occurrence_count_finland")
	public Integer getOccurrenceCountFinland() {
		return occurrenceCountFinland;
	}

	public void setOccurrenceCountFinland(Integer occurrenceCountFinland) {
		this.occurrenceCountFinland = occurrenceCountFinland;
	}

	@Column(name="checklist")
	public String getNameAccordingTo() {
		if (nameAccordingTo == null) return null;
		return nameAccordingTo.toURI();
	}

	@Deprecated // for hibernate; use setTaxonRank(Qname)
	public void setNameAccordingTo(String nameAccordingTo) {
		if (nameAccordingTo == null) {
			this.nameAccordingTo = null;
		}
		else {
			this.nameAccordingTo = Qname.fromURI(nameAccordingTo);
		}
	}

	@Transient
	public void setNameAccordingTo(Qname nameAccordingTo) {
		this.nameAccordingTo = nameAccordingTo;
	}

	private String trim(String s) {
		return Util.trimToByteLength(s, 1000);
	}

}