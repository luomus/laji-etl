package fi.laji.datawarehouse.etl.models.exceptions;

import fi.laji.datawarehouse.etl.models.dw.Quality;

public class DataValidationException extends Exception {	
	
	private static final long serialVersionUID = 1657382966296659632L;

	public DataValidationException(String message) {
		super(message);
	}
	
	public DataValidationException(String message, Throwable t) {
		super(message, t);
	}
	
	public static class DateValidationException extends DataValidationException {

		private static final long serialVersionUID = -5018040299097948527L;
		private final Quality.Issue issue;
		
		public DateValidationException(Quality.Issue issue, String message) {
			super(message);
			this.issue = issue;
		}

		public Quality.Issue getIssue() {
			return issue;
		}
		
	}
}
