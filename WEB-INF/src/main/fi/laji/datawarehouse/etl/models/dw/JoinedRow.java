package fi.laji.datawarehouse.etl.models.dw;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;

public class JoinedRow {

	private Unit unit;
	private Gathering gathering;
	private Document document;
	private Annotation annotation;
	private MediaObject media;
	private Sample sample;

	public JoinedRow(Unit unit, Gathering gathering, Document document) {
		this.unit = unit;
		this.gathering = gathering;
		this.document = document;
	}

	@FieldDefinition(order=3.0)
	public Unit getUnit() {
		return unit;
	}

	public JoinedRow setUnit(Unit unit) {
		this.unit = unit;
		return this;
	}

	@FieldDefinition(order=2.0)
	public Gathering getGathering() {
		return gathering;
	}

	public JoinedRow setGathering(Gathering gathering) {
		this.gathering = gathering;
		return this;
	}

	@FieldDefinition(order=1.0)
	public Document getDocument() {
		return document;
	}

	public JoinedRow setDocument(Document document) {
		this.document = document;
		return this;
	}

	@FieldDefinition(order=4.1)
	public Annotation getAnnotation() {
		return annotation;
	}

	public JoinedRow setAnnotation(Annotation annotation) {
		this.annotation = annotation;
		return this;
	}

	@FieldDefinition(order=4.2)
	public MediaObject getMedia() {
		return media;
	}

	public JoinedRow setMedia(MediaObject media) {
		this.media = media;
		return this;
	}

	@FieldDefinition(order=4.3)
	public Sample getSample() {
		return sample;
	}

	public JoinedRow setSample(Sample sample) {
		this.sample = sample;
		return this;
	}

}
