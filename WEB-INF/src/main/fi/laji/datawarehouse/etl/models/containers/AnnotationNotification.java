package fi.laji.datawarehouse.etl.models.containers;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.luomus.commons.containers.rdf.Qname;

public class AnnotationNotification {

	public static enum NotificationReason { MY_DOCUMENT_ANNOTATED, ANNOTATED_DOCUMENT_ANNOTATED }

	private long id;
	private Qname personId;
	private Annotation annotation;
	private NotificationReason notificationReason;

	public AnnotationNotification(Qname personId, Annotation annotation, NotificationReason notificationReason) {
		this(-1, personId, annotation, notificationReason);
	}

	public AnnotationNotification(long id, Qname personId, Annotation annotation, NotificationReason notificationReason) {
		this.id = id;
		this.personId = personId;
		this.annotation = annotation;
		this.notificationReason = notificationReason;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Qname getPersonId() {
		return personId;
	}

	public void setPersonId(Qname personId) {
		this.personId = personId;
	}

	public Annotation getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	public NotificationReason getNotificationReason() {
		return notificationReason;
	}

	public void setNotificationReason(NotificationReason notificationReason) {
		this.notificationReason = notificationReason;
	}

	@Override
	public String toString() {
		return "AnnotationNotification [personId=" + personId + ", annotation=" + annotation + ", notificationReason=" + notificationReason + "]";
	}

}
