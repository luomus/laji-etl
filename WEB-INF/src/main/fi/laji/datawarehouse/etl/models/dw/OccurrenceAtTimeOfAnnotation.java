package fi.laji.datawarehouse.etl.models.dw;

import java.util.Date;

import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class OccurrenceAtTimeOfAnnotation {

	private Qname taxonId;
	private String taxonVerbatim;
	private Date dateBegin;
	private Date dateEnd;
	private Double wgs84centerPointLat;
	private Double wgs84centerPointLon;
	private String countryVerbatim;
	private String locality;
	private String municipalityVerbatim;
	private IdentificationDwLinkings linkings;

	public Qname getTaxonId() {
		return taxonId;
	}
	public void setTaxonId(Qname taxonId) {
		this.taxonId = taxonId;
	}
	public String getTaxonVerbatim() {
		return taxonVerbatim;
	}
	public void setTaxonVerbatim(String taxonVerbatim) {
		this.taxonVerbatim = taxonVerbatim;
	}
	public Date getDateBegin() {
		return dateBegin;
	}
	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public Double getWgs84centerPointLat() {
		return wgs84centerPointLat;
	}
	public void setWgs84centerPointLat(Double wgs84centerPointLat) {
		this.wgs84centerPointLat = wgs84centerPointLat;
	}
	public Double getWgs84centerPointLon() {
		return wgs84centerPointLon;
	}
	public void setWgs84centerPointLon(Double wgs84centerPointLon) {
		this.wgs84centerPointLon = wgs84centerPointLon;
	}
	public String getCountryVerbatim() {
		return countryVerbatim;
	}
	public void setCountryVerbatim(String countryVerbatim) {
		this.countryVerbatim = countryVerbatim;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getMunicipalityVerbatim() {
		return municipalityVerbatim;
	}
	public void setMunicipalityVerbatim(String municipalityVerbatim) {
		this.municipalityVerbatim = municipalityVerbatim;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public IdentificationDwLinkings getLinkings() {
		return linkings;
	}

	public void setLinkings(IdentificationDwLinkings linkings) {
		this.linkings = linkings;
	}

	@Override
	public String toString() {
		return "OccurrenceAtTimeOfAnnotation [taxonId=" + taxonId + ", taxonVerbatim=" + taxonVerbatim + ", dateBegin=" + date(dateBegin) + ", dateEnd=" + date(dateEnd) + ", wgs84centerPointLat=" + round(wgs84centerPointLat) + ", wgs84centerPointLon=" + round(wgs84centerPointLon) + ", countryVerbatim=" + countryVerbatim + ", locality=" + locality + ", municipalityVerbatim=" + municipalityVerbatim + "]";
	}

	private Double round(Double coord) {
		if (coord == null) return null;
		return Utils.round(coord, 6);
	}

	private DateValue date(Date date) {
		if (date == null) return null;
		return DateUtils.convertToDateValue(date);
	}
	public boolean hasValues() {
		if (given(taxonVerbatim)) return true;
		if (given(taxonId)) return true;
		if (given(dateBegin)) return true;
		if (given(wgs84centerPointLat)) return true;
		if (given(countryVerbatim)) return true;
		if (given(municipalityVerbatim)) return true;		
		return false;
	}

	private boolean given(Object o) {
		return o != null && !o.toString().isEmpty();
	}
}
