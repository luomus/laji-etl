package fi.laji.datawarehouse.etl.models.dw;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.MinMax;
import fi.luomus.commons.utils.Utils;

@Embeddable
public class Coordinates {

	public static enum Type { WGS84, EUREF, YKJ }

	private static final double YKJ_LAT_MIN = 6400000.0;
	private static final double YKJ_LON_MIN = 2900000.0;
	private static final double YKJ_LAT_MAX = 7910000.0;
	private static final double YKJ_LON_MAX = 3990000.0;

	private static final double EUREF_LAT_MIN = 6300000.0;
	private static final double EUREF_LON_MIN = -100000.0;
	private static final double EUREF_LAT_MAX = 8100000.0;
	private static final double EUREF_LON_MAX =  990000.0;

	private static final double WGS84_LAT_MIN = -90.0;
	private static final double WGS84_LAT_MAX = 90.0;
	private static final double WGS84_LON_MIN = -180.0;
	private static final double WGS84_LON_MAX = 180.0;

	private Double latMin;
	private Double latMax;
	private Double lonMin;
	private Double lonMax;
	private Type type;
	private Integer accuracyInMeters;

	@Override
	public String toString() {
		return Utils.debugS(latMin, latMax, lonMin, lonMax, type, accuracyInMeters);
	}

	public Coordinates() {}

	public Coordinates(SingleCoordinates centerPoint) throws DataValidationException {
		this(centerPoint.getLat(), centerPoint.getLon(), centerPoint.getType());
	}

	public Coordinates(double lat, double lon, Type type) throws DataValidationException {
		this(init(lat, lon, type));
	}

	public Coordinates(double latMin, double latMax, double lonMin, double lonMax, Type type) throws DataValidationException {
		this(latMin, latMax, lonMin, lonMax, type, false);
	}

	private Coordinates(double latMin, double latMax, double lonMin, double lonMax, Type type, boolean allowInvalid) throws DataValidationException {
		if (type == Type.EUREF || type == Type.YKJ) {
			latMin = Math.round(latMin);
			latMax = Math.round(latMax);
			lonMin = Math.round(lonMin);
			lonMax = Math.round(lonMax);
		}
		setLatMin(latMin);
		setLatMax(latMax);
		setLonMin(lonMin);
		setLonMax(lonMax);

		if (latMax < latMin) throw new DataValidationException("Lat max is smaller than min: " + debug(latMin) +"-"+ debug(latMax));
		if (lonMax < lonMin) throw new DataValidationException("Lon max is smaller than min: " + debug(lonMin) +"-"+ debug(lonMax));

		this.type = type;

		if (allowInvalid) return;
		if (type == Type.YKJ) {
			validateYKJ(latMin, latMax, lonMin, lonMax, type);
		} else if (type == Type.EUREF) {
			validateEuref(latMin, latMax, lonMin, lonMax, type);
		} else if (type == Type.WGS84) {
			validateWGS84(latMin, latMax, lonMin, lonMax, type);
		} else {
			throw new IllegalArgumentException("Invalid type: " + type);
		}
	}

	private String debug(double coord) {
		DecimalFormat df = new DecimalFormat("#");
		df.setMaximumFractionDigits(6);
		df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
		return df.format(coord);
	}

	private String debug(double latMin, double latMax, double lonMin, double lonMax, Type type) {
		return Utils.debugS(debug(latMin), debug(latMax), debug(lonMin), debug(lonMax), type);
	}

	private void validateWGS84(double latMin, double latMax, double lonMin, double lonMax, Type type) throws DataValidationException {
		if (intLength(latMin) > 3 || intLength(lonMin) > 3 || intLength(latMax) > 3 || intLength(lonMax) > 3) {
			throw new DataValidationException("Invalid WGS84 coordinates: " + debug(latMin, latMax, lonMin, lonMax, type));
		}
		validateWGS84(latMin, WGS84_LAT_MIN, WGS84_LAT_MAX);
		validateWGS84(latMax, WGS84_LAT_MIN, WGS84_LAT_MAX);
		validateWGS84(lonMin, WGS84_LON_MIN, WGS84_LON_MAX);
		validateWGS84(lonMax, WGS84_LON_MIN, WGS84_LON_MAX);
		if (latMin == 0 && latMax == 0 && lonMin == 0 && lonMax == 0) {
			throw new DataValidationException("Coordinates in 0,0");
		}
	}

	private void validateWGS84(double coord, double min, double max) throws DataValidationException {
		if (coord > max) throw new DataValidationException("Too large value: " + debug(coord));
		if (coord < min) throw new DataValidationException("Too small value: " + debug(coord));
	}

	private void validateEuref(double latMin, double latMax, double lonMin, double lonMax, Type type) throws DataValidationException {
		try {
			validateEuref(latMin, 7, 7, EUREF_LAT_MIN, EUREF_LAT_MAX);
			validateEuref(latMax, 7, 7, EUREF_LAT_MIN, EUREF_LAT_MAX);
			validateEuref(lonMin, 6, 1, EUREF_LON_MIN, EUREF_LON_MAX);
			validateEuref(lonMax, 6, 1, EUREF_LON_MIN, EUREF_LON_MAX);
		} catch (Exception e) {
			throw new DataValidationException("Invalid Euref coordinates: " + debug(latMin, latMax, lonMin, lonMax, type), e);
		}
	}

	private void validateEuref(double euref, int maxLength, int minLength, double min, double max) throws DataValidationException {
		int length = intLength(euref);
		if (length > maxLength) {
			throw new DataValidationException("Too long: " + debug(euref) + ", must not be longer than " + maxLength);
		}
		if (length < minLength) {
			throw new DataValidationException("Too short: " + debug(euref) + ", must not be shorter than " + maxLength);
		}
		if (euref > max) throw new DataValidationException("Too large value: " + debug(euref));
		if (euref < min) throw new DataValidationException("Too small value: " + debug(euref));
	}

	private void validateYKJ(double latMin, double latMax, double lonMin, double lonMax, Type type) throws DataValidationException {
		try {
			int sameLengthForAll = intLength(latMin);
			validateYKJ(latMin, sameLengthForAll, YKJ_LAT_MIN, YKJ_LAT_MAX);
			validateYKJ(latMax, sameLengthForAll, YKJ_LAT_MIN, YKJ_LAT_MAX);
			validateYKJ(lonMin, sameLengthForAll, YKJ_LON_MIN, YKJ_LAT_MAX);
			validateYKJ(lonMax, sameLengthForAll, YKJ_LON_MIN, YKJ_LON_MAX);
		} catch (Exception e) {
			throw new DataValidationException("Invalid YKJ coordinates (" + e.getMessage() + "): " + debug(latMin, latMax, lonMin, lonMax, type));
		}
	}

	private void validateYKJ(double ykj, int sameLengthForAll, double min, double max) throws DataValidationException {
		int length = intLength(ykj);
		if (length != sameLengthForAll) throw new DataValidationException("YKJ coordinates must be reported using same precision");
		if (length > 7) throw new DataValidationException("Too long: " + debug(ykj));
		if (length < 2) throw new DataValidationException("Too short: " + debug(ykj));
		ykj = addYKJEndZeros(ykj);
		if (ykj > max) throw new DataValidationException("Too large value: " + debug(ykj));
		if (ykj < min) throw new DataValidationException("Too small value: " + debug(ykj));
	}

	private double addYKJEndZeros(double ykj) throws DataValidationException {
		if (ykj < 0) throw new DataValidationException("Too small value: " + debug(ykj));
		String s = String.valueOf((int) ykj);
		while (s.length() < 7) {
			s += "0";
		}
		return Double.valueOf(s);
	}

	private int intLength(double coord) {
		return String.valueOf(Math.round(Math.abs(coord))).length();
	}

	public Coordinates(Coordinates coordinates) throws DataValidationException {
		this(coordinates.getLatMin(), coordinates.getLatMax(), coordinates.getLonMin(), coordinates.getLonMax(), coordinates.getType());
		this.accuracyInMeters = coordinates.getAccuracyInMeters();
	}

	private static Coordinates init(double lat, double lon, Type type) throws DataValidationException {
		if (type == Type.YKJ || type == Type.EUREF) {
			return new Coordinates(lat, lat+1, lon, lon+1, type);
		}
		return new Coordinates(lat, lat, lon, lon, type);
	}

	private static class SecureData {
		private final int coeff;
		private final int accuracyInMeters;
		private final int metricSystemLength;
		public SecureData(int coeff, int accuracyInMeters, int metricSystemLength) {
			this.coeff = coeff;
			this.accuracyInMeters = accuracyInMeters;
			this.metricSystemLength = metricSystemLength;
		}
	}

	private static final Map<SecureLevel, SecureData> SECURE_DATA;
	static {
		SECURE_DATA = new HashMap<>();
		SECURE_DATA.put(SecureLevel.KM100, new SecureData(1, 100000, 2));
		SECURE_DATA.put(SecureLevel.KM50, new SecureData(2, 50000, 2));
		SECURE_DATA.put(SecureLevel.KM25, new SecureData(4, 25000, 2));
		SECURE_DATA.put(SecureLevel.KM10, new SecureData(10, 10000, 3));
		SECURE_DATA.put(SecureLevel.KM5, new SecureData(20, 5000, 3));
		SECURE_DATA.put(SecureLevel.KM1, new SecureData(100, 1000, 4));
	}

	public Coordinates conceal(SecureLevel secureLevel, Type originalType) {
		if (secureLevel == SecureLevel.HIGHEST) return null;
		if (secureLevel == SecureLevel.NONE) throw new IllegalStateException("No point concealing with secure level none!");
		return getConcealed(secureLevel, originalType);
	}

	private Coordinates getConcealed(SecureLevel secureLevel, Type originalType) {
		Coordinates concealed = null;
		if (type == Type.WGS84) {
			concealed = concealWgs84(secureLevel);
		} else if (type == Type.YKJ) {
			concealed = concealYkj(secureLevel, originalType);
		} else {
			throw new UnsupportedOperationException("Conceal not defined for " + type);
		}
		int secureLevelAccuracy = SECURE_DATA.get(secureLevel).accuracyInMeters;
		int calculatedAccuracyOfConcealed = concealed.calculateBoundingBoxAccuracyForConcelment(originalType);
		int originalAccuracy = this.getAccuracyInMeters() != null ? this.getAccuracyInMeters().intValue() : 1;
		concealed.setAccuracyInMeters(Math.max(originalAccuracy, Math.max(secureLevelAccuracy, calculatedAccuracyOfConcealed)));
		return concealed;
	}

	private Coordinates concealYkj(SecureLevel secureLevel, Type originalType) {
		List<MinMax<Integer>> concealed = concealMetric(secureLevel, originalType);
		int latMin = concealed.get(0).getMin();
		int latMax = concealed.get(0).getMax();
		int lonMin = concealed.get(1).getMin();
		int lonMax = concealed.get(1).getMax();
		try {
			return new Coordinates(latMin, latMax, lonMin, lonMax, Type.YKJ, true);
		} catch (DataValidationException e) {
			throw new ETLException("Impossible state in coordinate concealment", e);
		}
	}

	private List<MinMax<Integer>> concealMetric(SecureLevel secureLevel, Type originalType) {
		secureLevel = resolveSecureLevel(secureLevel, originalType);
		int metricSystemLength = SECURE_DATA.get(secureLevel).metricSystemLength;
		SingleCoordinates centerPoint = this.toFullYkjLength().convertCenterPoint();
		if (secureLevel == SecureLevel.KM100 || secureLevel == SecureLevel.KM10 || secureLevel == SecureLevel.KM1) {
			MinMax<Integer> lat = metricEvenRound(centerPoint.getLat(), metricSystemLength);
			MinMax<Integer> lon = metricEvenRound(centerPoint.getLon(), metricSystemLength);
			return Utils.list(lat, lon);
		}
		if (secureLevel == SecureLevel.KM50 || secureLevel == SecureLevel.KM5) {
			MinMax<Integer> lat = metricKmHalfRound(centerPoint.getLat(), metricSystemLength);
			MinMax<Integer> lon = metricKmHalfRound(centerPoint.getLon(), metricSystemLength);
			return Utils.list(lat, lon);
		}
		if (secureLevel == SecureLevel.KM25) {
			MinMax<Integer> lat = metricKm25Round(centerPoint.getLat(), metricSystemLength);
			MinMax<Integer> lon = metricKm25Round(centerPoint.getLon(), metricSystemLength);
			return Utils.list(lat, lon);
		}
		throw new UnsupportedOperationException(secureLevel.name());
	}

	private SecureLevel resolveSecureLevel(SecureLevel secureLevel, Type originalType) {
		SecureLevel byCoordinateAccuracy = resolveSecureLevelForCoordinateAccuracy(originalType);
		return SecureLevel.maxOf(secureLevel, byCoordinateAccuracy);
	}

	private SecureLevel resolveSecureLevelForCoordinateAccuracy(Type originalType) {
		int originalAccuracy = this.calculateBoundingBoxAccuracyForConcelment(originalType);
		if (originalAccuracy >= 100000) return SecureLevel.KM100;
		if (originalAccuracy >= 50000) return SecureLevel.KM50;
		if (originalAccuracy >= 25000) return SecureLevel.KM25;
		if (originalAccuracy >= 10000) return SecureLevel.KM10;
		if (originalAccuracy >= 5000) return SecureLevel.KM5;
		return SecureLevel.KM1;
	}

	private MinMax<Integer> metricEvenRound(Double original, int metricSystemLength) {
		int min = Integer.valueOf(String.valueOf(original.intValue()).substring(0, metricSystemLength));
		int max = min + 1;
		return new MinMax<>(min, max);
	}

	private MinMax<Integer> metricKmHalfRound(Double original, int metricSystemLength) {
		String base = String.valueOf(original.intValue()).substring(0, metricSystemLength);
		int nextNumber = getNumber(metricSystemLength + 1, original);
		if (nextNumber < 5) {
			return new MinMax<>(Integer.valueOf(base+"0"), Integer.valueOf(base+"5"));
		}
		return new MinMax<>(Integer.valueOf(base+"5"), (Integer.valueOf(base)+1)*10);
	}

	private MinMax<Integer> metricKm25Round(Double original, int metricSystemLength) {
		String base = String.valueOf(original.intValue()).substring(0, metricSystemLength);
		int nextNumber = getNumbers(metricSystemLength + 1, 2, original);
		if (nextNumber < 25) {
			return new MinMax<>(Integer.valueOf(base+"00"), Integer.valueOf(base+"25"));
		}
		if (nextNumber < 50) {
			return new MinMax<>(Integer.valueOf(base+"25"), Integer.valueOf(base+"50"));
		}
		if (nextNumber < 75) {
			return new MinMax<>(Integer.valueOf(base+"50"), Integer.valueOf(base+"75"));
		}
		return new MinMax<>(Integer.valueOf(base+"75"), (Integer.valueOf(base)+1)*100);
	}

	private int getNumber(int positionOfNumber, Double coord) {
		return getNumbers(positionOfNumber, 1, coord);
	}

	private int getNumbers(int positionOfNumber, int count, Double coord) {
		return getNumbers(positionOfNumber, count, String.valueOf(coord.intValue()));
	}

	public static int getNumbers(int positionOfNumber, int count, String s) {
		if (positionOfNumber == 0 || count == 0) throw new IllegalArgumentException();
		String numbers = "";
		for (int i = positionOfNumber; i < positionOfNumber + count; i++) {
			if (s.length() < i) {
				throw new IllegalArgumentException();
			}
			numbers += s.charAt(i - 1);
		}
		return Integer.valueOf(numbers);
	}

	private Coordinates concealWgs84(SecureLevel secureLevel) {
		secureLevel = resolveSecureLevel(secureLevel, Type.WGS84);
		int coeff = SECURE_DATA.get(secureLevel).coeff;
		double latMin = concealWgs84Min(this.latMin, coeff);
		double lonMin = concealWgs84Min(this.lonMin, coeff);
		double latMax = concealWgs84Max(this.latMax, coeff);
		double lonMax = concealWgs84Max(this.lonMax, coeff);
		try {
			return new Coordinates(latMin, latMax, lonMin, lonMax, Type.WGS84);
		} catch (DataValidationException e) {
			throw new ETLException("Impossible state in coordinate concealment", e);
		}
	}

	private double concealWgs84Max(Double max, int coeff) {
		return Math.ceil((max * coeff)+0.0000000001) / coeff;
	}

	private double concealWgs84Min(Double min, int coeff) {
		return Math.floor(min * coeff) / coeff;
	}

	@FileDownloadField(name="LatMin(N)", order=1)
	public Double getLatMin() {
		return latMin;
	}

	public void setLatMin(double latMin) {
		this.latMin = Utils.round(latMin, 6);
	}

	@FileDownloadField(name="LatMax(N)", order=2)
	public Double getLatMax() {
		return latMax;
	}

	public void setLatMax(double latMax) {
		this.latMax = Utils.round(latMax, 6);
	}

	@FileDownloadField(name="LonMin(E)", order=3)
	public Double getLonMin() {
		return lonMin;
	}

	public void setLonMin(double lonMin) {
		this.lonMin = Utils.round(lonMin, 6);
	}

	@FileDownloadField(name="LonMax(E)", order=4)
	public Double getLonMax() {
		return lonMax;
	}

	public void setLonMax(double lonMax) {
		this.lonMax = Utils.round(lonMax, 6);
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Integer getAccuracyInMeters() {
		return accuracyInMeters;
	}

	public Coordinates setAccuracyInMeters(Integer accuracyInMeters) {
		this.accuracyInMeters = accuracyInMeters;
		return this;
	}

	public Coordinates copy() {
		try {
			return new Coordinates(this);
		} catch (DataValidationException e) {
			throw new ETLException(e);
		}
	}

	public boolean notInitialized() {
		return type == null || latMin == null || latMax == null || lonMin == null || latMin == null;
	}

	public static SingleCoordinates convertYkj100kmCenter(Coordinates ykj) {
		return convertYkjCenter(ykj, 2);
	}

	public static SingleCoordinates convertYkj50kmCenter(Coordinates ykj) {
		SingleCoordinates ykj100kmCenter = convertYkjCenter(ykj, 2);
		SingleCoordinates ykj10kmCenter = convertYkjCenter(ykj, 3);
		double ykj50kmLat = lastNumber(ykj10kmCenter.getLat().intValue()) < 5 ? ykj100kmCenter.getLat().intValue() * 10 : ykj100kmCenter.getLat().intValue() * 10 + 5;
		double ykj50kmLon = lastNumber(ykj10kmCenter.getLon().intValue()) < 5 ? ykj100kmCenter.getLon().intValue() * 10 : ykj100kmCenter.getLon().intValue() * 10 + 5;
		return new SingleCoordinates(ykj50kmLat, ykj50kmLon, Type.YKJ);
	}

	public static SingleCoordinates convertYkj10kmCenter(Coordinates ykj) {
		return convertYkjCenter(ykj, 3);
	}

	public static SingleCoordinates convertYkj1kmCenter(Coordinates ykj) {
		return convertYkjCenter(ykj, 4);
	}

	private static SingleCoordinates convertYkjCenter(Coordinates ykj, int length) {
		Double latCenter = Utils.avg(ykj.getLatMin(), ykj.getLatMax());
		Double lonCenter = Utils.avg(ykj.getLonMin(), ykj.getLonMax());
		Integer gridLat = trimGrid(latCenter.intValue(), length);
		Integer gridLon = trimGrid(lonCenter.intValue(), length);
		return new SingleCoordinates(gridLat, gridLon, Type.YKJ);
	}

	public static SingleCoordinates convertYkj100km(Coordinates ykj) {
		int latMin = ykj.getLatMin().intValue();
		int latMax = ykj.getLatMax().intValue() - 1;
		int lonMin = ykj.getLonMin().intValue();
		int lonMax = ykj.getLonMax().intValue() - 1;

		Integer grid100kmLat = trimGrid(latMin, latMax, 2);
		Integer grid100kmLon = trimGrid(lonMin, lonMax, 2);
		if (given(grid100kmLat, grid100kmLon)) {
			return new SingleCoordinates(grid100kmLat, grid100kmLon, Type.YKJ);
		}
		return null;
	}

	public static SingleCoordinates convertYkj50km(Coordinates ykj) {
		int latMin = ykj.getLatMin().intValue();
		int latMax = ykj.getLatMax().intValue() - 1;
		int lonMin = ykj.getLonMin().intValue();
		int lonMax = ykj.getLonMax().intValue() - 1;

		Integer grid100kmLat = trimGrid(latMin, latMax, 2);
		Integer grid100kmLon = trimGrid(lonMin, lonMax, 2);
		Integer grid10kmLat = trimGrid(latMin, latMax, 3);
		Integer grid10kmLon = trimGrid(lonMin, lonMax, 3);
		Integer grid50kmLat;
		Integer grid50kmLon;
		if (!given(grid100kmLat, grid100kmLon, grid10kmLat, grid10kmLon)) return null;
		if (lastNumber(grid10kmLat) < 5) {
			grid50kmLat = Integer.valueOf(grid100kmLat.toString() + 0);
		} else {
			grid50kmLat = Integer.valueOf(grid100kmLat.toString() + 5);
		}
		if (lastNumber(grid10kmLon) < 5) {
			grid50kmLon = Integer.valueOf(grid100kmLon.toString() + 0);
		} else {
			grid50kmLon = Integer.valueOf(grid100kmLon.toString() + 5);
		}
		return new SingleCoordinates(grid50kmLat, grid50kmLon, Type.YKJ);
	}

	private static int lastNumber(Integer i) {
		String s = i.toString();
		return Integer.valueOf(s.substring(s.length()-1));
	}

	public static SingleCoordinates convertYkj10km(Coordinates ykj) {
		int latMin = ykj.getLatMin().intValue();
		int latMax = ykj.getLatMax().intValue() - 1;
		int lonMin = ykj.getLonMin().intValue();
		int lonMax = ykj.getLonMax().intValue() - 1;

		Integer grid10kmLat = trimGrid(latMin, latMax, 3);
		Integer grid10kmLon = trimGrid(lonMin, lonMax, 3);
		if (given(grid10kmLat, grid10kmLon)) {
			return new SingleCoordinates(grid10kmLat, grid10kmLon, Type.YKJ);
		}
		return null;
	}

	public static SingleCoordinates convertYkj1km(Coordinates ykj) {
		int latMin = ykj.getLatMin().intValue();
		int latMax = ykj.getLatMax().intValue() - 1;
		int lonMin = ykj.getLonMin().intValue();
		int lonMax = ykj.getLonMax().intValue() - 1;

		Integer grid1kmLat = trimGrid(latMin, latMax, 4);
		Integer grid1kmLon = trimGrid(lonMin, lonMax, 4);
		if (given(grid1kmLat, grid1kmLon)) {
			return new SingleCoordinates(grid1kmLat, grid1kmLon, Type.YKJ);
		}
		return null;
	}

	private static boolean given(Integer ...ints) {
		for (Integer i : ints) {
			if (i == null) return false;
		}
		return true;
	}

	private static Integer trimGrid(int min, int max, int length) {
		Integer minTrimmed = trimGrid(min, length);
		Integer maxTrimmed = trimGrid(max, length);
		if (minTrimmed == null || maxTrimmed == null) return null;
		if (minTrimmed.equals(maxTrimmed)) return minTrimmed;
		return null;
	}

	private static Integer trimGrid(int i, int length) {
		try {
			return Integer.valueOf(String.valueOf(i).substring(0, length));
		} catch (StringIndexOutOfBoundsException e) {
			return null;
		}
	}

	public long calculateBoundingBoxAreaInSquareMeters() {
		if (this.type == Type.WGS84) throw new UnsupportedOperationException("Not timplemetend for " + this.type);
		long area = Double.valueOf((getLatMax() - getLatMin()) * (getLonMax() - getLonMin())).longValue();
		if (area < 1) return 1;
		return area;
	}

	public int calculateBoundingBoxAccuracyForConcelment(Type originalType) {
		if (this.type == Type.WGS84) return calculateWgs84BoundingBoxAccuracy();
		if (this.type == Type.YKJ) return calculateYkjBoundingBoxAccuracyForConcelment(originalType);
		if (this.type == Type.EUREF) return calculateMetricAccuracyForConcelment(this, originalType);
		throw new UnsupportedOperationException("Unknown type " + this.type);
	}

	public int calculateBoundingBoxAccuracy() {
		if (this.type == Type.WGS84) return calculateWgs84BoundingBoxAccuracy();
		if (this.type == Type.YKJ) return calculateYkjBoundingBoxAccuracy();
		if (this.type == Type.EUREF) return calculateMetricAccuracy(this);
		throw new UnsupportedOperationException("Unknown type " + this.type);
	}

	private int calculateWgs84BoundingBoxAccuracy() {
		if (getLatMax().equals(getLatMin()) && getLonMax().equals(getLonMin())) {
			// Point
			return 1;
		}
		double differenceLat = BigDecimal.valueOf(getLatMin()).subtract(BigDecimal.valueOf(getLatMax())).abs().doubleValue() / 2;
		double differenceLon = BigDecimal.valueOf(getLonMin()).subtract(BigDecimal.valueOf(getLonMax())).abs().doubleValue();
		double largestDifference = Math.max(differenceLat, differenceLon);

		if (largestDifference <= 0.00005) return 1;
		if (largestDifference <= 0.0002) return 10;
		if (largestDifference <= 0.002) return 100;
		if (largestDifference <= 0.02) return 1000;
		if (largestDifference <= 0.10) return 5000;
		if (largestDifference <= 0.20) return 10000;
		if (largestDifference <= 0.5) return 25000;
		if (largestDifference <= 1.0) return 50000;
		return 100000;
	}

	private int calculateYkjBoundingBoxAccuracy() {
		Coordinates fullLength = this.toFullYkjLength();
		return calculateMetricAccuracy(fullLength);
	}

	private int calculateYkjBoundingBoxAccuracyForConcelment(Type originalType) {
		Coordinates fullLength = this.toFullYkjLength();
		return calculateMetricAccuracyForConcelment(fullLength, originalType);
	}

	private int calculateMetricAccuracy(Coordinates c) {
		double differenceLat = Math.abs(c.getLatMin() - c.getLatMax());
		double differenceLon = Math.abs(c.getLonMin() - c.getLonMax());
		double largestDifference = Math.max(differenceLat, differenceLon);
		if (largestDifference <= 9) return 1;
		if (largestDifference <= 30) return 10;
		if (largestDifference <= 200) return 100;
		if (largestDifference <= 2500) return 1000;
		if (largestDifference <= 7500) return 5000;
		if (largestDifference <= 12000) return 10000;
		if (largestDifference <= 30000) return 25000;
		if (largestDifference <= 60000) return 50000;
		return 100000;
	}

	private int calculateMetricAccuracyForConcelment(Coordinates c, Type originalType) {
		double differenceLat = Math.abs(c.getLatMin() - c.getLatMax());
		double differenceLon = Math.abs(c.getLonMin() - c.getLonMax());
		double largestDifference = Math.max(differenceLat, differenceLon);

		if (originalType == Type.WGS84) {
			if (largestDifference <= 2000) return 1000;
			if (largestDifference <= 7500) return 5000;
			if (largestDifference <= 12000) return 10000;
			if (largestDifference <= 30000) return 25000;
			if (largestDifference <= 60000) return 50000;
			return 100000;
		}

		if (largestDifference <= 1100) return 1000;
		if (largestDifference <= 6000) return 5000;
		if (largestDifference <= 11000) return 10000;
		if (largestDifference <= 26000) return 25000;
		if (largestDifference <= 55000) return 50000;
		return 100000;
	}

	public Coordinates toFullYkjLength() {
		double latMin = toFullYkjLength(getLatMin());
		double latMax = toFullYkjLength(getLatMax());
		double lonMin = toFullYkjLength(getLonMin());
		double lonMax = toFullYkjLength(getLonMax());
		try {
			return new Coordinates(latMin, latMax, lonMin, lonMax, Type.YKJ).setAccuracyInMeters(this.getAccuracyInMeters());
		} catch (DataValidationException e) {
			throw new ETLException(e);
		}
	}

	public static double toFullYkjLength(double ykjD) {
		String ykj = Integer.toString((int) ykjD);
		while (ykj.length() < 7) {
			ykj += "0";
		}
		return Double.valueOf(ykj);
	}

	public boolean checkIfIsYkjGrid() {
		if (getType() != Type.YKJ) return false;
		return checkIfIsYkjGrid(getLatMin()) && checkIfIsYkjGrid(getLonMin()) && checkIfIsYkjGrid(getLatMax()) && checkIfIsYkjGrid(getLonMax());
	}

	private boolean checkIfIsYkjGrid(double ykjD) {
		String ykj = Integer.toString((int) ykjD);
		return ykj.length() < 7;
	}

	public boolean checkIfIsPoint() {
		if (type == Type.WGS84) {
			return latMin.equals(latMax) && lonMin.equals(lonMax);
		}
		if (checkIfIsYkjGrid()) return false;
		if (latMin.equals(latMax) && lonMin.equals(lonMax)) return true;
		return latMin.equals(latMax-1) && lonMin.equals(lonMax-1);
	}

	public SingleCoordinates convertCenterPoint() {
		double centerPointLat = Utils.avg(getLatMin(), getLatMax());
		double centerPointLon = Utils.avg(getLonMin(), getLonMax());
		if (getType() != Type.WGS84) {
			centerPointLat = Math.round(centerPointLat);
			centerPointLon = Math.round(centerPointLon);
		}
		return new SingleCoordinates(centerPointLat, centerPointLon, getType());
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return toString().equals(obj.toString());
	}

}
