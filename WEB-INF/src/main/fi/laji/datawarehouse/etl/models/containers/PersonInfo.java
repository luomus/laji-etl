package fi.laji.datawarehouse.etl.models.containers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.containers.rdf.Qname;

public class PersonInfo {

	public enum SystemId {

		VANHAHATIKKA("vanhahatikka:"),
		HATIKKA("hatikka:"),
		LINTUVAARA("lintuvaara:"),
		VIRTALA("virtala:"),
		INATURALIST("inaturalist:"),
		OMARIISTA("omariista:"),
		KASTIKKA(""),
		LOYDOS("loydos:");

		private String userIdPrefix;

		SystemId(String userIdPrefx) {
			this.userIdPrefix = userIdPrefx;
		}

		public String getUserIdPrefix() {
			return userIdPrefix;
		}

	}

	private final Qname id;
	private final String email;
	private final String fullName;
	private final Map<SystemId, Collection<String>> systemIds = new HashMap<>(5);

	public PersonInfo(Qname id, String email, String fullName, String preferredName, String inheritedName) {
		this.id = id;
		this.email = email;
		if (given(fullName)) {
			this.fullName = fullName;
		} else {
			if (given(preferredName)) fullName = preferredName;
			if (given(inheritedName)) fullName += " " + inheritedName;
			if (given(fullName)) {
				this.fullName = fullName.trim();
			} else {
				this.fullName = null;
			}
		}
	}

	public void addSystemId(SystemId system, String id) {
		if (!given(id)) return;
		if (!systemIds.containsKey(system)) {
			systemIds.put(system, new ArrayList<String>());
		}
		if (systemIds.get(system).contains(id)) return;
		systemIds.get(system).add(id);
	}

	private boolean given(String s) {
		return s != null && !s.trim().isEmpty();
	}

	public Qname getId() {
		return id;
	}

	public String getEmail() {
		if (!given(email)) throw new ETLException("No valid email for " + id);
		return email;
	}

	public String getFullName() {
		return fullName;
	}

	public Map<SystemId, Collection<String>> getSystemIds() {
		return systemIds;
	}

	@Override
	public String toString() {
		return "PersonInfo [id=" + id + ", email=" + email + ", fullName=" + fullName + ", systemIds=" + systemIds + "]";
	}

}
