package fi.laji.datawarehouse.etl.models.dw;

import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

public class Person {

	private final Qname id;
	private final String userId; 
	private final String fullName;

	public Person(Qname qname, String userId, String fullName) {
		this.userId = userId;
		this.id = qname;
		this.fullName = Util.trimToByteLength(fullName, 200);
	}

	public Person(String userId) {
		this.userId = userId;
		this.id = null;
		this.fullName = null;
	}

	public String getUserId() {
		return userId;
	}

	public Qname getId() {
		return id;
	}

	public String getFullName() {
		return fullName;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", userId=" + userId + ", fullName=" + fullName + "]";
	}

}
