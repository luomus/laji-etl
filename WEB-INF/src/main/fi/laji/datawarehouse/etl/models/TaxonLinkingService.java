package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.TaxonLookupNameGenerator;
import fi.laji.datawarehouse.dao.vertica.TargetChecklistTaxaEntity;
import fi.laji.datawarehouse.dao.vertica.TargetEntity;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class TaxonLinkingService {

	private static final String HTTP_PLACEHOLDER = "<HTTP>";
	private static final String HTTP = "http://";
	public static final String COUNTRY_FI = "COUNTRY-FI";
	private final Map<String, Qname> lookup;

	public TaxonLinkingService(Map<String, Qname> lookup) {
		this.lookup = lookup;
	}

	/**
	 * Resolve master checklist taxon id from given target name. Will not try to get partial/non exact matches.
	 * @param targetName verbatim, qname or uri
	 * @return id or null if not resolved to master checklist
	 */
	public Qname getTaxonId(String targetName) {
		return lookup.get(toTargetLookupString(targetName));
	}

	/**
	 * Resolve master checklist taxon id from given taxon id.
	 * Note: taxon id that is not part of master checklist or not synonym of a master checklist taxon will not be resolved.
	 * For taxon ids that belong to a synonym of a master checklist taxon, the id of the master checklist taxon (synonym parent) is returned.
	 * @param taxonId
	 * @return id or null if not resolved to master checklist
	 */
	public Qname getTaxonId(Qname taxonId) {
		if (taxonId == null) return null;
		return lookup.get(toTargetLookupString(taxonId.toString()));
	}

	/**
	 * Resolve master checklist taxon id from given occurrence information.
	 * Prefers annotated taxon id (if flag set), then reported taxon id and finally taxon verbatim.
	 * Will not try to get partial/non exact matches for taxon verbatim.
	 * @param unit
	 * @param gathering
	 * @param useAnnotations
	 * @return id or null if not resolved to master checklist
	 */
	public Qname getTaxonId(Unit unit, Gathering gathering, boolean useAnnotations) {
		if (unit == null) throw new IllegalArgumentException("Null unit");
		if (gathering == null) throw new IllegalArgumentException("Null gathering");

		if (useAnnotations) {
			Qname annotatedTaxonId = unit.getInterpretations() == null ? null : unit.getInterpretations().getAnnotatedTaxonId();
			if (given(annotatedTaxonId)) {
				Qname taxonId = getTaxonId(annotatedTaxonId);
				if (given(taxonId)) return taxonId;
			}
		}

		if (given(unit.getReportedTaxonId())) {
			Qname taxonId = getTaxonId(unit.getReportedTaxonId());
			if (given(taxonId)) return taxonId;
		}

		if (given(unit.getTaxonVerbatim())) {
			Qname countryId = gathering.getInterpretations() == null ? null : gathering.getInterpretations().getCountry();
			Qname taxonId = lookup.get(toTargetLookupString(unit.getTaxonVerbatim(), unit.getReferencePublication(), unit.getAutocompleteSelectedTaxonId(), countryId));
			if (taxonId != null) return taxonId;

			if (given(unit.getAutocompleteSelectedTaxonId())) {
				// try if there is a match without autocomplete selected taxon id
				taxonId = lookup.get(toTargetLookupString(unit.getTaxonVerbatim(), unit.getReferencePublication(), null, countryId));
				if (taxonId != null) return taxonId;
			}

			if (isFinland(countryId)) {
				// try if there is a match without country (a non-finnish taxon from occurrence reported in Finland)
				taxonId = lookup.get(toTargetLookupString(unit.getTaxonVerbatim(), unit.getReferencePublication(), unit.getAutocompleteSelectedTaxonId(), null));
				if (taxonId != null) return taxonId;
			}

			if (given(unit.getAutocompleteSelectedTaxonId()) && isFinland(countryId)) {
				// try without both
				return lookup.get(toTargetLookupString(unit.getTaxonVerbatim(), unit.getReferencePublication(), null, null));
			}

		}

		return null;
	}

	private boolean isFinland(Qname countryId) {
		return countryId != null && Const.FINLAND.equals(countryId);
	}

	/**
	 * Create target entity and try to resolve master checklist taxon linking from the given occurrence information.
	 * Prefers annotated taxon id (if flag set), then reported taxon id and finally taxon verbatim.
	 * Will try to get partial/non exact matches for taxon verbatim in case exact verbatim does not cause a match.
	 * @param unit
	 * @param gathering
	 * @param useAnnotations
	 * @return target entity that may or may not have taxon key set; the taxon key may be non-exact match
	 */
	public TargetEntity getTargetEntity(Unit unit, Gathering gathering, boolean useAnnotations, Long referenceKey) {
		if (unit == null) throw new IllegalArgumentException("Null unit");
		if (gathering == null) throw new IllegalArgumentException("Null gathering");

		if (useAnnotations) {
			Qname annotatedTaxonId = unit.getInterpretations() == null ? null : unit.getInterpretations().getAnnotatedTaxonId();
			if (given(annotatedTaxonId)) {
				Qname taxonId = getTaxonId(annotatedTaxonId);
				if (given(taxonId)) return createTargetEntity(annotatedTaxonId, taxonId, referenceKey);
			}
		}

		if (given(unit.getReportedTaxonId())) {
			Qname taxonId = getTaxonId(unit.getReportedTaxonId());
			if (given(taxonId)) return createTargetEntity(unit.getReportedTaxonId(), taxonId, referenceKey);
		}

		if (!given(unit.getTaxonVerbatim())) return null;

		String targetName = unit.getTaxonVerbatim();
		Qname reference = unit.getReferencePublication();
		Qname autocompleteSelectedTaxonId = unit.getAutocompleteSelectedTaxonId();
		Qname countryId = gathering.getInterpretations() == null ? null : gathering.getInterpretations().getCountry();

		return getTargetEntity(targetName, reference, autocompleteSelectedTaxonId, countryId, referenceKey);
	}

	private TargetEntity getTargetEntity(String targetName, Qname reference, Qname autocompleteSelectedTaxonId, Qname countryId, Long referenceKey) {
		String targetLookupString = toTargetLookupString(targetName, reference, autocompleteSelectedTaxonId, countryId);

		TargetEntity e = new TargetEntity();
		e.setId(toOriginalReference(targetName, reference, autocompleteSelectedTaxonId, countryId));
		e.setTargetlookup(targetLookupString);
		e.setTargetLowercase(cleanTargetName(targetName));
		e.setReferenceKey(referenceKey);

		Qname taxonId = lookup.get(targetLookupString);
		if (taxonId != null) {
			e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
			return e;
		}

		if (given(autocompleteSelectedTaxonId)) {
			// try if there is a match without autocomplete selected taxon id
			taxonId = lookup.get(toTargetLookupString(targetName, reference, null, countryId));
			if (taxonId != null) {
				e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
				return e;
			}
		}

		if (isFinland(countryId)) {
			// try if there is a match without country (a non-finnish taxon from occurrence reported in Finland)
			taxonId = lookup.get(toTargetLookupString(targetName, reference, autocompleteSelectedTaxonId, null));
			if (taxonId != null) {
				e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
				return e;
			}
		}

		if (given(autocompleteSelectedTaxonId) && isFinland(countryId)) {
			// try without both
			taxonId = lookup.get(toTargetLookupString(targetName, reference, null, null));
			if (taxonId != null) {
				e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
				return e;
			}
		}

		if (referenceKey != null) return e; // only exact matches allowed for names that are supposed to be 'according to some checklist'

		taxonId = getNonExactMatch(targetName);
		if (taxonId != null) {
			e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
			e.setNotExactTaxonMatch(true);
		}
		return e;
	}

	private TargetEntity createTargetEntity(Qname givenTaxonId, Qname resolvedTaxonId, Long referenceKey) {
		TargetEntity e = new TargetEntity();
		e.setId(toOriginalReference(givenTaxonId.toString(), null, null, null));
		e.setTargetlookup(toTargetLookupString(givenTaxonId.toString()));
		e.setTargetLowercase(cleanTargetName(givenTaxonId.toString()));
		e.setReferenceKey(referenceKey);
		e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(resolvedTaxonId));
		return e;
	}

	private Qname getNonExactMatch(String targetName) {
		targetName = cleanTargetName(targetName);
		if (!targetName.contains(" ")) return null;
		if (shouldNotTroTySplit(targetName)) return null;
		String[] parts = targetName.split(Pattern.quote(" "));
		if (parts.length > 2) {
			if (parts[0].length() < 3 || parts[1].length() < 3) return null;
			String possibleSpeciesName = parts[0] + " " + parts[1];
			return getTaxonId(possibleSpeciesName);
		}
		return null;
	}

	private static boolean shouldNotTroTySplit(String targetName) {
		return targetName.contains(" coll") ||
				targetName.contains("ryhmä") ||
				targetName.contains("group") ||
				targetName.contains(TaxonLookupNameGenerator.HYDRIB_MARK_MULTIPLICATION) ||
				targetName.contains(TaxonLookupNameGenerator.HYDRIB_MARK_X) ||
				targetName.contains(" agg.") ||
				targetName.contains("/");
	}

	/**
	 * Create a new TargetEntity with the given target's info (key, id, lowercase name) and try to resolve master checklist taxon linking using the given target's lookup string and lowercase name
	 * @param original The original target
	 * @return TargetEntity with the original info, that may or may not have taxon key set; the taxon key may be non-exact match
	 */
	public TargetEntity getTargetEntity(TargetEntity original) {
		if (original == null) throw new IllegalArgumentException("Null target");
		TargetLookupStringParts parts = reverseParse(original);

		TargetEntity resolved = getTargetEntity(parts.targetName, parts.reference, parts.autocompleteSelectedTaxonId, parts.countryId, original.getReferenceKey());
		resolved.setKey(original.getKey());
		resolved.setId(original.getId());
		resolved.setTargetLowercase(original.getTargetLowercase());
		resolved.setTargetlookup(original.getTargetlookup());
		resolved.setReferenceKey(original.getReferenceKey());
		return resolved;
	}

	/**
	 * Create target entity and try to resolve taxon linking from the given checklist's lookup using the given target's lookup string
	 * @param checklist
	 * @param target
	 * @return
	 */
	public TargetChecklistTaxaEntity getTargetChecklistEntity(Qname checklist, TargetEntity target) {
		if (checklist == null) throw new IllegalArgumentException("Null checklist");
		if (target == null) throw new IllegalArgumentException("Null target");
		TargetChecklistTaxaEntity e = new TargetChecklistTaxaEntity();
		e.setChecklist(checklist.toURI());
		e.setTargetKey(target.getKey());

		Qname taxonId = lookup.get(toTargetLookupString(target.getTargetLowercase(), checklist));
		if (taxonId != null) {
			e.setTaxonKey(TaxonBaseEntity.parseTaxonKey(taxonId));
		}
		return e;
	}

	private static Set<Character> ALLOWED = " :/×0123456789abcdefghijklmnopqrstuvwxyzåäö".chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
	private static Set<Character> ALLOWED_WILDCARD = Utils.set(ALLOWED, '*');

	private static Map<Character, String> REPLACE;
	static {
		REPLACE = new HashMap<>();
		REPLACE.put('\t', " ");
		REPLACE.put('(', " ");
		REPLACE.put(')', " ");
		REPLACE.put(',', " ");
		REPLACE.put('á', "a");
		REPLACE.put('ã', "a");
		REPLACE.put('æ', "ae");
		REPLACE.put('ç', "c");
		REPLACE.put('è', "e");
		REPLACE.put('é', "e");
		REPLACE.put('ë', "e");
		REPLACE.put('í', "i");
		REPLACE.put('ï', "i");
		REPLACE.put('ò', "o");
		REPLACE.put('ó', "o");
		REPLACE.put('õ', "o");
		REPLACE.put('ø', "o");
		REPLACE.put('ú', "u");
		REPLACE.put('ü', "u");
		REPLACE.put('ě', "e");
		REPLACE.put('ķ', "k");
		REPLACE.put('ĺ', "l");
		REPLACE.put('ý', "y");
	}

	public static String cleanTargetNameAllowWildcard(String targetName) {
		return cleanTargetName(targetName, ALLOWED_WILDCARD);
	}

	public static String cleanTargetName(String targetName) {
		return cleanTargetName(targetName, ALLOWED);
	}

	private static String cleanTargetName(String targetName, Set<Character> allowed) {
		if (targetName == null) return "";
		String cleanedName = "";
		for (char c : targetName.toLowerCase().toCharArray()) {
			String replace = REPLACE.get(c);
			if (replace != null) {
				cleanedName += replace;
			} else {
				if (allowed.contains(c)) {
					cleanedName += c;
				}
			}
		}
		while (cleanedName.contains("  ")) {
			cleanedName = cleanedName.replace("  ", " ");
		}
		return cleanedName.trim();
	}

	public static String toTargetLookupString(String targetName) {
		return toTargetLookupString(targetName, null);
	}

	public static Collection<String> toTargetLookupStrings(String targetName, Qname reference, Qname taxonId, boolean finnishTaxon) {
		if (targetName == null) throw new IllegalArgumentException("Null targetName");
		if (taxonId == null) throw new IllegalArgumentException("Null taxonId");

		List<String> lookupStrings = new ArrayList<>();

		if (isMasterChecklist(reference)) {
			lookupStrings.addAll(toTargetLookupStrings(targetName, null, taxonId, finnishTaxon));
		}

		lookupStrings.add(toTargetLookupString(targetName, reference));
		lookupStrings.add(toTargetLookupString(targetName, reference, taxonId, finnishTaxon));
		if (finnishTaxon)
			lookupStrings.add(toTargetLookupString(targetName, reference, taxonId, false));
		lookupStrings.add(toTargetLookupString(targetName, reference, null, finnishTaxon));
		if (finnishTaxon)
			lookupStrings.add(toTargetLookupString(targetName, reference, null, false));

		return lookupStrings;
	}

	public static Collection<String> toTaxonIdLookupStrings(Qname taxonId, Qname reference) {
		if (taxonId == null) throw new IllegalArgumentException("Null taxonId");

		List<String> lookupStrings = new ArrayList<>();
		lookupStrings.addAll(toTargetLookupStrings(taxonId.toString(), reference, taxonId, false));
		lookupStrings.addAll(toTargetLookupStrings(taxonId.toURI(), reference, taxonId, false));
		return lookupStrings;
	}

	private static String toTargetLookupString(String targetName, Qname reference) {
		return cleanTargetName(targetName).replace(" ", "") + ":" + handle(reference);
	}

	public static String toTargetLookupString(String targetName, Qname reference, Qname autocompleteSelectedTaxonId, boolean finnishTaxon) {
		return toTargetLookupString(targetName, reference, autocompleteSelectedTaxonId, country(finnishTaxon));
	}

	private static String toTargetLookupString(String targetName, Qname reference, Qname autocompleteSelectedTaxonId, Qname country) {
		return toTargetLookupString(targetName, reference) + ":" + handle(autocompleteSelectedTaxonId) + ":" + country(country);
	}

	private static String toOriginalReference(String targetName, Qname reference, Qname autocompleteSelectedTaxonId, Qname countryId) {
		return cleanTargetName(targetName) + ":" + handle(reference) + ":" + handle(autocompleteSelectedTaxonId) + ":" + country(countryId);
	}

	private static class TargetLookupStringParts {
		String targetName;
		Qname reference;
		Qname autocompleteSelectedTaxonId;
		Qname countryId;
	}

	private static TargetLookupStringParts reverseParse(TargetEntity target) {
		TargetLookupStringParts parts = new TargetLookupStringParts();
		String[] a = reverseParseTargetLookupParts(target.getTargetlookup());
		parts.targetName = target.getTargetLowercase();
		if (a.length > 1) parts.reference = toQname(a[1]);
		if (a.length > 2) parts.autocompleteSelectedTaxonId = toQname(a[2]);
		if (a.length > 3) parts.countryId = toCountryQname(a[3]);
		return parts;
	}

	private static String[] reverseParseTargetLookupParts(String targetLookupString) {
		if (!targetLookupString.startsWith(HTTP)) {
			return targetLookupString.split(Pattern.quote(":"));
		}
		String[] a = targetLookupString.replace(HTTP, HTTP_PLACEHOLDER).split(Pattern.quote(":")); // if targetname is "http://tunfi/mx123" we don't want to split from http:
		if (a.length > 0) a[0] = a[0].replace(HTTP_PLACEHOLDER, HTTP);
		return a;
	}

	private static String country(Qname country) {
		if (Const.FINLAND.equals(country)) return COUNTRY_FI;
		return null;
	}

	private static Qname country(boolean finnishTaxon) {
		if (finnishTaxon) return Const.FINLAND;
		return null;
	}

	private static boolean isMasterChecklist(Qname reference) {
		return Const.MASTER_CHECKLIST_QNAME.equals(reference);
	}

	private static boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	private static boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private static String handle(Qname qname) {
		if (qname == null) return null;
		if (!qname.isSet()) return null;
		return qname.toString();
	}

	private static Qname toQname(String string) {
		if (string == null) return null;
		if (string.equals("null")) return null;
		Qname q = new Qname(string);
		if (q.isSet()) return q;
		return null;
	}

	private static Qname toCountryQname(String string) {
		if (COUNTRY_FI.equals(string)) return Const.FINLAND;
		return null;
	}

	public List<String> getLookUpNames(Qname taxonId) {
		List<String> names = new ArrayList<>();
		for (Map.Entry<String, Qname> e : lookup.entrySet()) {
			if (e.getValue().equals(taxonId)) names.add(e.getKey());
		}
		return names;
	}

}
