package fi.laji.datawarehouse.etl.models;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.Utils;

public class InvasiveSpeciesEarlyWarningNotifier {

	protected final DAO dao;
	protected final Config config;
	private final ErrorReporter errorReporter;

	public InvasiveSpeciesEarlyWarningNotifier(DAO dao, ErrorReporter errorReporter, Config config) {
		this.dao = dao;
		this.errorReporter = errorReporter;
		this.config = config;
	}

	public int sendWarnings() throws Exception {
		Set<Qname> notifiedUnits = dao.getInvasiveSpeciesEarlyWarningReportedUnits();

		Set<Qname> newNotifications = sendWarnings(notifiedUnits);

		notifiedUnits.addAll(newNotifications);
		dao.setInvasiveSpeciesEarlyWarningReportedUnits(notifiedUnits);

		return newNotifications.size();
	}

	protected Set<Qname> sendWarnings(Set<Qname> notifiedUnits) throws Exception {
		List<JoinedRow> results = getFromPrivateDw();
		results.removeIf(r->notifiedUnits.contains(r.getUnit().getUnitId()));
		sendWarnings(results);
		return results.stream().map(r->r.getUnit().getUnitId()).collect(Collectors.toSet());
	}

	protected List<JoinedRow> getFromPrivateDw() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PRIVATE)
				.setDefaultFilters(false)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.build(),
				1, 10000);
		Filters filters = query.getFilters()
				.setFirstLoadedSameOrAfter(oneYearAgo())
				.setTime("-1825/0")
				.setIndividualCountMin(1)
				.setCountryId(Const.FINLAND)
				.setCountryId(Const.RUSSIA)
				.setCountryId(Const.SWEDEN)
				.setCountryId(Const.ESTONIA)
				.setCountryId(Const.NORWAY)
				.setCountryId(new Qname("ML.116")) // Latvia
				.setCountryId(new Qname("ML.122")); // Liettua
		Set<Qname> warningTaxa = dao.getTaxonomyDAO().getTaxonContainer().getInvasiveSpeciesEarlyWarningFilter();
		if (warningTaxa.isEmpty()) throw new ETLException("No invasive species early warning species defined!");
		for (Qname taxonId : warningTaxa) {
			filters.setTaxonId(taxonId);
		}
		return dao.getPrivateVerticaDAO().getQueryDAO().getRawList(query);
	}

	protected Date oneYearAgo() {
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.YEAR, -1);
		return calendar.getTime();
	}

	protected void sendWarnings(List<JoinedRow> results) throws Exception {
		if (results.isEmpty()) return;
		String to = receiverAddress();
		String from = "noreply@laji.fi";
		String content = generateContent(results);
		String title = title() + " (" + results.size() + ")";
		if (config.stagingMode()) {
			title += " STAGING";
		}
		if (config.developmentMode()) {
			title += " DEV";
			Utils.debug(to, from, title, content);
		} else {
			EmailUtil emailUtil = new EmailUtil("localhost");
			emailUtil.send(to, from, title, content);
			emailUtil.send("info@laji.fi", from, title, content);
		}
	}

	protected String title() {
		return "LAJI.FI Varhaisvaroitus";
	}

	protected String receiverAddress() {
		return config.get("InvasiveSpeciesEarlyWarningsTo");
	}

	private String generateContent(List<JoinedRow> results) {
		StringBuilder b = new StringBuilder();
		for (JoinedRow row : results) {
			generateContent(b, row);
		}
		return b.toString();
	}

	private void generateContent(StringBuilder b, JoinedRow row) {
		b.append(row.getDocument().getDocumentId().toURI()).append("\n");
		try {
			Taxon taxon = row.getUnit().getLinkings().getTaxon();
			generateTaxonInfo(b, taxon);
		} catch (Exception e) {
			String taxonId = null;
			try {
				taxonId = row.getUnit().getLinkings().getTaxon().getId().toString();
			} catch (Exception e2) {}
			errorReporter.report(this.getClass().getSimpleName() + ": Generate taxon info: " + taxonId, e);
			b.append(row.getUnit().getTaxonVerbatim());
		}
		b.append("\n\n");
	}

	private void generateTaxonInfo(StringBuilder b, Taxon taxon) {
		if (given(taxon.getScientificName())) {
			b.append(taxon.getScientificName()).append(" ");
		}
		if (given(taxon.getVernacularName().forLocale("fi"))) {
			b.append(taxon.getVernacularName().forLocale("fi")).append(" ");
		}
		b.append("(").append(taxon.getQname().toURI()).append(")");
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

}
