package fi.laji.datawarehouse.etl.models.dw;

import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.taxonomy.Taxon;

public class UnitDWLinkings {

	private Taxon taxon;
	private Taxon originalTaxon;

	@FileDownloadField(name="Taxon", order=1)
	@StatisticsQueryAlllowedField
	public Taxon getTaxon() {
		return taxon;
	}

	public void setTaxon(Taxon taxon) {
		this.taxon = taxon;
	}

	@StatisticsQueryAlllowedField
	public Taxon getOriginalTaxon() {
		return originalTaxon;
	}

	public void setOriginalTaxon(Taxon taxon) {
		this.originalTaxon = taxon;
	}
}
