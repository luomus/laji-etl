package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.etl.utils.Util.DateValidateMode;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.Subject;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

@Embeddable
public class Document extends BaseModel {

	public static enum Concealment { PUBLIC, PRIVATE }

	private Concealment concealment;
	private Qname sourceId;
	private Qname documentId;
	private Qname collectionId;
	private Qname licenseId;
	private Qname namedPlaceId;
	private Qname formId;
	private List<Gathering> gatherings = new ArrayList<>();
	private List<String> keywords = new ArrayList<>();
	private List<String> editorUserIds = new ArrayList<>();
	private List<MediaObject> media = new ArrayList<>();
	private List<Annotation> annotations = new ArrayList<>();
	private List<Tag> sourceTags = new ArrayList<>();
	private Date createdDate;
	private Date modifiedDate;
	private Long loadTime;
	private Long firstLoadTime;
	private SecureLevel secureLevel;
	private List<SecureReason> secureReasons = new ArrayList<>();
	private String dataSource;
	private String siteType;
	private String siteStatus;
	private Boolean siteDead;
	private Qname completeListTaxonId;
	private Qname completeListType;
	private String referenceURL;
	private DocumentDWLinkings linkings;
	private DocumentQuality quality;
	private NamedPlaceEntity namedPlace;
	private Boolean deleted;
	private boolean partial = false;
	private int gatheringOrderSeq = 0;

	public Document(Concealment concealment, Qname sourceId, Qname documentId, Qname collectionId) throws CriticalParseFailure {
		Util.validateId(documentId, "documentId");
		Util.validateId(sourceId, "sourceId");
		Util.validateId(collectionId, "collectionId");
		this.concealment = concealment;
		this.sourceId = sourceId;
		this.documentId = documentId;
		this.collectionId = collectionId;
		this.secureLevel = SecureLevel.NONE;
	}

	public static Document createDeletedDocument(DwRoot root) {
		Document document = new Document();
		try {
			document.setDocumentId(root.getDocumentId());
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException(); // Doc id is already validaded because it comes from root
		}
		document.setSourceId(root.getSourceId());
		document.setDeleted(true);
		return document;
	}

	public static Document fromRoot(Concealment concealment, DwRoot root) throws CriticalParseFailure {
		return new Document(concealment, root.getSourceId(), root.getDocumentId(), root.getCollectionId());
	}

	private static Document fromDocument(Concealment concealment, Document document) {
		try {
			return new Document(concealment, document.getSourceId(), document.getDocumentId(), document.getCollectionId());
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException(); // source, document and collection ids are already validated because they come from document
		}
	}

	@Deprecated
	public Document() {
		// for hibernate
	}

	public Document copy(Concealment concealment) {
		try {
			JSONObject documentJSON = ModelToJson.toJson(this);
			Document copy = JsonToModel.documentFromJson(documentJSON);
			copy.setConcealment(concealment);
			return copy;
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	/**
	 * Sequence generator for gatherings and units of this document
	 */
	private int seq = 1;

	public int nextSeqValue() {
		return seq++;
	}

	public Document concealPublicData(SecureLevel secureLevel, Set<SecureReason> secureReasons) {
		if (secureLevel == SecureLevel.NONE) throw new IllegalArgumentException("No point concealing with secure level " + secureLevel);
		Document publicDocument = Document.fromDocument(Concealment.PUBLIC, this);
		for (Gathering gathering : this.getGatherings()) {
			publicDocument.addGathering(gathering.concealPublicData(secureLevel, secureReasons, sourceId));
		}
		publicDocument.setLicenseIdUsingQname(this.licenseId);
		publicDocument.setFormIdUsingQname(this.formId);
		publicDocument.setSiteDead(this.siteDead);
		publicDocument.setSiteStatus(this.siteStatus);
		publicDocument.setSiteType(this.siteType);
		publicDocument.setSourceTags(new ArrayList<>(this.sourceTags));
		publicDocument.clearLoadTimes();
		publicDocument.setPartial(this.isPartial());
		publicDocument.setSecureLevel(secureLevel);
		if (this.getQuality() != null) {
			publicDocument.setQuality(this.getQuality().conseal());
		}
		setConcealedSecureReasons(secureReasons, publicDocument);
		return publicDocument;
	}

	private void setConcealedSecureReasons(Set<SecureReason> secureReasons, Document publicDocument) {
		for (SecureReason secureReason : secureReasons) {
			publicDocument.addSecureReason(Securer.toPublicSecureReason(secureReason));
		}
	}

	public void clearLoadTimes() {
		this.loadTime = null;
		this.firstLoadTime = null;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true, ignoreForQuery=true)
	public boolean isPublic() {
		return concealment == Concealment.PUBLIC;
	}

	@Transient
	@FileDownloadField(name="Keywords", order=50)
	public List<String> getKeywords() {
		return keywords;
	}

	public void addKeyword(String keyword) {
		if (keyword == null || keyword.trim().isEmpty()) return;
		keyword = Util.trimToByteLength(keyword, 1000);
		if (!this.keywords.contains(keyword)) {
			this.keywords.add(keyword);
		}
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public List<Gathering> getGatherings() {
		return gatherings;
	}

	public Document addGathering(Gathering gathering) {
		gathering.setGatheringOrder(gatheringOrderSeq++);
		this.gatherings.add(gathering);
		return this;
	}

	public void setGatherings(ArrayList<Gathering> gatherings) {
		this.gatheringOrderSeq = 0;
		for (Gathering g : gatherings) {
			g.setGatheringOrder(gatheringOrderSeq++);
		}
		this.gatherings = gatherings;
	}

	@Transient
	public List<String> getEditorUserIds() {
		return editorUserIds;
	}

	public void addEditorUserId(String userId) {
		if (userId == null || userId.trim().isEmpty()) return;
		if (userId.startsWith("MA.")) {
			userId = new Qname(userId).toURI();
		}
		editorUserIds.add(Util.trimToByteLength(userId, 1000));
	}

	public void setEditorUserIds(ArrayList<String> userIds) {
		this.editorUserIds = userIds;
	}

	@Transient
	public List<MediaObject> getMedia() {
		return media;
	}

	public void addMedia(MediaObject media) {
		this.media.add(media);
	}

	public void setMedia(ArrayList<MediaObject> media) {
		this.media = media;
	}

	@Column(name="document_mediacount")
	@FileDownloadField(name="MediaCount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getMediaCount() {
		return media.size();
	}

	@Deprecated
	public void setMediaCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	@Column(name="load_date")
	@FileDownloadField(name="LoadDate", order=60.2)
	@FieldDefinition(ignoreForETL=true)
	public Date getLoadDate() {
		if (loadTime == null) {
			return null;
		}
		return new Date(loadTime*1000);
	}

	@Deprecated // only for hibenate
	public void setLoadDate(@SuppressWarnings("unused") Date loadDate) {

	}

	@Transient
	@FieldDefinition(ignoreForETL=true, ignoreForQuery=true)
	public Long getLoadTime() {
		return loadTime;
	}

	public void setLoadTime(Long loadTime) {
		this.loadTime = loadTime;
	}

	@Column(name="first_load_date")
	@FileDownloadField(name="FirstLoadDate", order=60.1)
	@FieldDefinition(ignoreForETL=true)
	public Date getFirstLoadDate() {
		if (firstLoadTime == null) return null;
		return new Date(firstLoadTime*1000);
	}

	@Deprecated // only for hibenate
	public void setFirstLoadDate(@SuppressWarnings("unused") Date loadDate) {

	}

	@Transient
	@FieldDefinition(ignoreForETL=true, ignoreForQuery=true)
	public Long getFirstLoadTime() {
		return firstLoadTime;
	}

	public void setFirstLoadTime(Long firstLoadTime) {
		this.firstLoadTime = firstLoadTime;
	}

	@Transient
	public void setLoadTimeNow() {
		this.loadTime = DateUtils.getCurrentEpoch();
	}

	@Column(name="created_date")
	@FileDownloadField(name="Created", order=61)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) throws DateValidationException {
		if (createdDate != null) Util.validate(createdDate, "createdDate", DateValidateMode.STRICT);
		this.createdDate = createdDate;
	}

	@Column(name="modified_date")
	@FileDownloadField(name="Modified", order=61)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) throws DateValidationException {
		if (modifiedDate != null) Util.validate(modifiedDate, "modifiedDate", DateValidateMode.STRICT);
		this.modifiedDate = modifiedDate;
	}

	@Transient
	@FileDownloadField(name="DataSecureLevel", order=2)
	public SecureLevel getSecureLevel() {
		return secureLevel;
	}

	public void setSecureLevel(SecureLevel secureLevel) {
		this.secureLevel = secureLevel;
	}

	@Transient
	@FileDownloadField(name="DataSecureReasons", order=3)
	public List<SecureReason> getSecureReasons() {
		return secureReasons;
	}

	public void setSecureReasons(ArrayList<SecureReason> secureReasons) {
		this.secureReasons = secureReasons;
	}

	public Document addSecureReason(SecureReason secureReason) {
		if (!this.secureReasons.contains(secureReason)) {
			this.secureReasons.add(secureReason);
			Collections.sort(this.secureReasons);
		}
		return this;
	}

	@Column(name="secured")
	@FieldDefinition(ignoreForETL=true)
	public boolean isSecured() {
		return this.secureLevel != SecureLevel.NONE || !this.secureReasons.isEmpty();
	}

	@Deprecated // for hibernate
	public void setSecured(@SuppressWarnings("unused") Boolean secured) {}

	@Column(name="partial")
	@FileDownloadField(name="PartialDocument", order=4)
	@FieldDefinition(ignoreForETL=true)
	public boolean isPartial() {
		return partial;
	}

	public void setPartial(boolean partial) {
		this.partial = partial;
	}

	@Transient
	@FileDownloadField(name="DocumentID", order=-1)
	@StatisticsQueryAlllowedField
	public Qname getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Qname documentId) throws CriticalParseFailure {
		Util.validateId(documentId, "documentId");
		this.documentId = documentId;
	}

	@FieldDefinition(ignoreForETL=true)
	@Column(name="document_id_prefix")
	@StatisticsQueryAlllowedField
	public String getPrefix() {
		if (documentId == null) return null;
		Subject s = new Subject(documentId);
		String prefix = s.getNamespacePrefix().isEmpty() ? s.getSubNamespacePrefix() : s.getNamespacePrefix() + ":" + s.getSubNamespacePrefix();
		return Util.trimToByteLength(prefix, 50);
	}

	@Deprecated // for hibernate
	public void setPrefix(@SuppressWarnings("unused")String prefix) {

	}

	@Transient
	@FileDownloadField(name="SourceID", order=12)
	public Qname getSourceId() {
		return sourceId;
	}

	public void setSourceId(Qname sourceId) {
		this.sourceId = sourceId;
	}

	@Transient
	@FileDownloadField(name="CollectionID", order=10.1)
	public Qname getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Qname collectionId) {
		this.collectionId = collectionId;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Concealment getConcealment() {
		return concealment;
	}

	public void setConcealment(Concealment concealment) {
		this.concealment = concealment;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	@FileDownloadField(name="Linkings", order=11)
	public DocumentDWLinkings getLinkings() {
		return linkings;
	}

	public void setLinkings(DocumentDWLinkings linkings) {
		this.linkings = linkings;
	}

	public DocumentDWLinkings createLinkings() {
		if (linkings == null) {
			linkings = new DocumentDWLinkings();
		}
		return linkings;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public NamedPlaceEntity getNamedPlace() {
		return namedPlace;
	}

	public void setNamedPlace(NamedPlaceEntity namedPlace) {
		this.namedPlace = namedPlace;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name="namedplace_id")
	@FileDownloadField(name="NamedPlaceID", order=20)
	@StatisticsQueryAlllowedField
	public String getNamedPlaceId() {
		if (namedPlaceId == null) return null;
		return namedPlaceId.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setNamedPlaceId(String namedPlaceId) {
		if (namedPlaceId == null) {
			this.namedPlaceId = null;
		} else {
			setNamedPlaceIdUsingQname(Qname.fromURI(namedPlaceId));
		}
	}

	@Transient
	public void setNamedPlaceIdUsingQname(Qname namedPlaceId) {
		this.namedPlaceId = namedPlaceId;
	}

	@Column(name="form_id")
	public String getFormId() {
		if (formId == null) return null;
		return formId.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setFormId(String formId) {
		if (formId == null) {
			this.formId = null;
		} else {
			setFormIdUsingQname(Qname.fromURI(formId));
		}
	}

	@Transient
	public void setFormIdUsingQname(Qname formId) {
		this.formId = formId;
	}

	@Embedded
	@FileDownloadField(name="Quality", order=11)
	public DocumentQuality getQuality() {
		return quality;
	}

	public void setQuality(DocumentQuality quality) {
		this.quality = quality;
	}

	public DocumentQuality createQuality() {
		if (quality == null) {
			quality = new DocumentQuality();
		}
		return quality;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public Document addAnnotation(Annotation annotation) {
		annotations.add(annotation);
		return this;
	}

	public void setAnnotations(ArrayList<Annotation> annotations) {
		this.annotations = annotations;
	}

	@Transient
	public List<Tag> getSourceTags() {
		return sourceTags;
	}

	public void setSourceTags(ArrayList<Tag> sourceTags) {
		this.sourceTags = sourceTags;
	}

	public Document addSourceTag(Qname tag) {
		this.sourceTags.add(Annotation.toTag(tag));
		return this;
	}

	public Document addSourceTag(Tag tag) {
		this.sourceTags.add(tag);
		return this;
	}

	@Column(name="license")
	@FileDownloadField(name="License", order=10.2)
	public String getLicenseId() {
		if (licenseId == null) return null;
		return licenseId.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setLicenseId(String licenseId) {
		if (licenseId == null) {
			this.licenseId = null;
		} else {
			setLicenseIdUsingQname(Qname.fromURI(licenseId));
		}
	}

	@Transient
	public void setLicenseIdUsingQname(Qname licenseId) {
		this.licenseId = licenseId;
	}

	@Column(name="data_source")
	@FileDownloadField(name="OriginalDataSource", order=10.3)
	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = Util.trimToByteLength(dataSource, 1000);
	}

	@Column(name="site_type")
	@FileDownloadField(name="MonitoringSiteType", order=20.2)
	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = Util.trimToByteLength(siteType, 1000);
	}

	@Column(name="site_status")
	@FileDownloadField(name="MonitoringSiteStatus", order=20.3)
	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = Util.trimToByteLength(siteStatus, 1000);
	}

	@Column(name="site_dead")
	public Boolean getSiteDead() {
		return siteDead;
	}

	public void setSiteDead(Boolean siteDead) {
		this.siteDead = siteDead;
	}

	@Column(name="complete_list_taxon_id")
	public String getCompleteListTaxonId() {
		if (completeListTaxonId == null) return null;
		return completeListTaxonId.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setCompleteListTaxonId(String completeListTaxonId) {
		if (completeListTaxonId == null) {
			this.completeListTaxonId = null;
		} else {
			setCompleteListTaxonIdUsingQname(Qname.fromURI(completeListTaxonId));
		}
	}

	@Transient
	public void setCompleteListTaxonIdUsingQname(Qname completeListTaxonId) {
		this.completeListTaxonId = completeListTaxonId;
	}

	@Column(name="complete_list_type")
	public String getCompleteListType() {
		if (completeListType == null) return null;
		return completeListType.toURI();
	}

	@Deprecated // for hibernate; use set qname
	public void setCompleteListType(String completeListType) {
		if (completeListType == null) {
			this.completeListType = null;
		} else {
			setCompleteListTypeUsingQname(Qname.fromURI(completeListType));
		}
	}

	@Transient
	public void setCompleteListTypeUsingQname(Qname completeListType) {
		this.completeListType = completeListType;
	}

	@Transient
	public String getReferenceURL() {
		return referenceURL;
	}

	public void setReferenceURL(String referenceURL) {
		this.referenceURL = referenceURL;
	}

	public JSONObject toJSON() {
		return ModelToJson.toJson(this);
	}

	public void validateIncomingIds() throws CriticalParseFailure {
		Set<Qname> ids = new HashSet<>();
		Util.validateIncomingDocumentId(getDocumentId());
		validateIdUniqueness(ids, getDocumentId());
		for (Annotation a : getAnnotations()) {
			validateIncomingAnnotationIds(a);
		}
		for (Gathering g : getGatherings()) {
			Util.validateIncomingId(g.getGatheringId(), "gatheringId");
			validateIdUniqueness(ids, g.getGatheringId());
			for (Unit u : g.getUnits()) {
				Util.validateIncomingId(u.getUnitId(), "unitId");
				validateIdUniqueness(ids, u.getUnitId());
				for (Sample s : u.getSamples()) {
					Util.validateIncomingId(s.getSampleId(), "sampleId");
					validateIdUniqueness(ids, s.getSampleId());
				}
				for (Annotation a : u.getAnnotations()) {
					validateIncomingAnnotationIds(a);
				}
			}
		}
	}

	private void validateIncomingAnnotationIds(Annotation a) throws CriticalParseFailure {
		Util.validateIncomingId(a.getId(), "annotationId");
		Util.validateIncomingId(a.getRootID(), "rootId");
		if (a.getTargetID() != null) {
			Util.validateIncomingId(a.getTargetID(), "targetId");
		}
	}

	private void validateIdUniqueness(Set<Qname> ids, Qname id) throws CriticalParseFailure {
		if (ids.contains(id)) {
			throw new CriticalParseFailure("Id exists in same document multiple times: " + id);
		}
		ids.add(id);
	}

}
