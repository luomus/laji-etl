package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Interpreter;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;

@Embeddable
public class GatheringInterpretations {

	private Qname country;
	private Interpreter.GeoSource sourceOfCountry;

	private Qname biogeographicalProvince;
	private Interpreter.GeoSource sourceOfBiogeographicalProvince;
	private List<Qname> biogeographicalProvinces;

	private Qname finnishMunicipality;
	private Interpreter.GeoSource sourceOfFinnishMunicipality;
	private List<Qname> finnishMunicipalities;

	private Coordinates coordinates;
	private Interpreter.GeoSource sourceOfCoordinates;

	private Integer coordinateAccuracy;

	private String municipalityDisplayname;
	private String countryDisplayname;
	private String biogeographicalProvinceDisplayname;

	private List<Qname> naturaAreas;
	
	public GatheringInterpretations() {}

	@Transient
	public Qname getCountry() {
		return country;
	}

	public void setCountry(Qname country) {
		this.country = country;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		if (coordinates == null) {
			this.coordinates = null;
			this.coordinateAccuracy = null;
		} else {
			this.coordinates = coordinates;
			this.coordinateAccuracy = coordinates.getAccuracyInMeters();
		}
	}

	@FileDownloadField(name="CoordinateAccuracy", order=-2)
	public Integer getCoordinateAccuracy() {
		return coordinateAccuracy;
	}

	@Deprecated
	public void setCoordinateAccuracy(Integer accuracyInMeters) {
		// for hibernate
		this.coordinateAccuracy = accuracyInMeters;
	}

	@Transient
	public Qname getBiogeographicalProvince() {
		return biogeographicalProvince;
	}

	public void setBiogeographicalProvince(Qname biogeographicalProvince) {
		this.biogeographicalProvince = biogeographicalProvince;
	}

	@Transient
	public Qname getFinnishMunicipality() {
		return finnishMunicipality;
	}

	public void setFinnishMunicipality(Qname finnishMunicipality) {
		this.finnishMunicipality = finnishMunicipality;
	}

	@Transient
	@FileDownloadField(name="CountrySource", order=11)
	public Interpreter.GeoSource getSourceOfCountry() {
		return sourceOfCountry;
	}

	public void setSourceOfCountry(Interpreter.GeoSource sourceOfCountry) {
		this.sourceOfCountry = sourceOfCountry;
	}

	@Transient
	@FileDownloadField(name="BioProvinceSource", order=12)
	public Interpreter.GeoSource getSourceOfBiogeographicalProvince() {
		return sourceOfBiogeographicalProvince;
	}

	public void setSourceOfBiogeographicalProvince(GeoSource sourceOfbiogeographicalProvince) {
		this.sourceOfBiogeographicalProvince = sourceOfbiogeographicalProvince;
	}

	@Transient
	@FileDownloadField(name="MunicipalitySource", order=13)
	public Interpreter.GeoSource getSourceOfFinnishMunicipality() {
		return sourceOfFinnishMunicipality;
	}

	public void setSourceOfFinnishMunicipality(Interpreter.GeoSource sourceOfFinnishMunicipality) {
		this.sourceOfFinnishMunicipality = sourceOfFinnishMunicipality;
	}

	@Transient
	@FileDownloadField(name="CoordinateSource", order=-1)
	public Interpreter.GeoSource getSourceOfCoordinates() {
		return sourceOfCoordinates;
	}

	public void setSourceOfCoordinates(Interpreter.GeoSource sourceOfCoordinates) {
		this.sourceOfCoordinates = sourceOfCoordinates;
	}

	@FileDownloadField(name="Municipality", order=3)
	@Column(name="municipality_displayname")
	public String getMunicipalityDisplayname() {
		return municipalityDisplayname;
	}

	public void setMunicipalityDisplayname(String municipalityDisplayname) {
		this.municipalityDisplayname = Util.trimToByteLength(municipalityDisplayname, 1000);
	}

	@FileDownloadField(name="Country", order=1)
	@Column(name="country_displayname")
	public String getCountryDisplayname() {
		return countryDisplayname;
	}

	public void setCountryDisplayname(String countryDisplayname) {
		this.countryDisplayname = Util.trimToByteLength(countryDisplayname, 1000);
	}

	@FileDownloadField(name="Bioprovince", order=2)
	@Column(name="bioprovince_displayname")
	public String getBiogeographicalProvinceDisplayname() {
		return biogeographicalProvinceDisplayname;
	}

	public void setBiogeographicalProvinceDisplayname(String biogeographicalProvinceDisplayname) {
		this.biogeographicalProvinceDisplayname = Util.trimToByteLength(biogeographicalProvinceDisplayname, 1000);
	}

	@Transient
	public List<Qname> getBiogeographicalProvinces() {
		return biogeographicalProvinces;
	}

	public void setBiogeographicalProvinces(ArrayList<Qname> biogeographicalProvinces) {
		this.biogeographicalProvinces = biogeographicalProvinces;
	}

	public void addToBiogeographicalProvinces(Qname area) {
		if (this.biogeographicalProvinces == null) {
			this.biogeographicalProvinces = new ArrayList<>();
		}
		if (!this.biogeographicalProvinces.contains(area)) {
			this.biogeographicalProvinces.add(area);
			Collections.sort(this.biogeographicalProvinces);
		}
	}

	@Transient
	public List<Qname> getFinnishMunicipalities() {
		return finnishMunicipalities;
	}

	public void setFinnishMunicipalities(ArrayList<Qname> finnishMunicipalities) {
		this.finnishMunicipalities = finnishMunicipalities;
	}

	public void addToFinnishMunicipalities(Qname area) {
		if (this.finnishMunicipalities == null) {
			this.finnishMunicipalities = new ArrayList<>();
		}
		if (!this.finnishMunicipalities.contains(area)) {
			this.finnishMunicipalities.add(area);
			Collections.sort(this.finnishMunicipalities);
		}
	}

	public void addToNaturaAreas(Qname area) {
		if (this.naturaAreas == null) {
			this.naturaAreas = new ArrayList<>();
		}
		this.naturaAreas.add(area);
	}
	
	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public List<Qname> getNaturaAreas() {
		return naturaAreas;
	}

	public void setNaturaAreas(ArrayList<Qname> naturaAreas) {
		this.naturaAreas = naturaAreas;
	}
	
}
