package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class QuarantineReleaser {

	private static final Selected SELECTED = initSelected();
	private static final int LIMIT = 1000;
	private final DAO dao;
	private final ErrorReporter errorReporter;
	private final ThreadHandler threadHandler;

	public QuarantineReleaser(DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler) {
		this.dao = dao;
		this.errorReporter = errorReporter;
		this.threadHandler = threadHandler;
	}

	private static Selected initSelected() {
		try {
			return new Selected("document.documentId", "document.sourceId");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	public int release(Date date) {
		try {
			int count = 0;
			for (CollectionMetadata collection : dao.getCollections().values()) {
				if (collection.getDataQuarantinePeriod() != null) {
					count += release(collection.getQname(), collection.getDataQuarantinePeriod(), date);
				}
			}
			return count;
		} catch (Exception ex) {
			errorReporter.report(LogUtils.buildStackTrace(ex));
			dao.logError(Const.LAJI_ETL_QNAME, QuarantineReleaser.class, date.toString(), ex);
			return -1;
		}
	}

	private int release(Qname collectionId, Double quarantinePeriod, Date date) {
		Date releaseDate = Securer.calculateLastQuarantineDate(quarantinePeriod, date);
		List<JoinedRow> documentsToRelease = getDocumentsToRelease(collectionId, releaseDate);
		release(documentsToRelease);
		return documentsToRelease.size();
	}

	private void release(List<JoinedRow> documentsToRelease) {
		Set<Qname> sources = new HashSet<>();
		for (JoinedRow row : documentsToRelease) {
			boolean docFound = release(row);
			if (docFound) {
				sources.add(row.getDocument().getSourceId());
			}
		}
		for (Qname source : sources) {
			threadHandler.runOutPipe(source);
		}
	}

	private boolean release(JoinedRow row) {
		try {
			int i = dao.getETLDAO().markReprocessOutPipe(row.getDocument().getDocumentId());
			if (i != 1) {
				String message = "Entry set for quarantine reprocess was not found from out pipe: " + debug(row);
				errorReporter.report(message);
				dao.logError(Const.LAJI_ETL_QNAME, QuarantineReleaser.class, row.getDocument().getDocumentId().toURI(), new IllegalStateException(message));
				return false;
			}
			return true;
		} catch (Exception e) {
			String debugData = debug(row);
			Exception withData = new Exception(debugData, e);
			errorReporter.report(LogUtils.buildStackTrace(withData));
			dao.logError(Const.LAJI_ETL_QNAME, QuarantineReleaser.class, row.getDocument().getDocumentId().toURI(), withData);
			return false;
		}
	}

	private String debug(JoinedRow row) {
		String debugData = row.getDocument() == null ? "null document!" : Utils.debugS(row.getDocument().getDocumentId().toURI(), row.getDocument().getSourceId());
		return debugData;
	}

	private List<JoinedRow> getDocumentsToRelease(Qname collectionId, Date releaseDate) {
		VerticaQueryDAO queryDAO = dao.getPrivateVerticaDAO().getQueryDAO();
		List<JoinedRow> all = new ArrayList<>();
		int page = 1;
		while (true) {
			List<JoinedRow> partialResult = queryDAO.getRawList(buildQuery(collectionId, releaseDate, page));
			if (partialResult.isEmpty()) break;
			all.addAll(partialResult);
			page++;
		}
		return all;
	}

	private ListQuery buildQuery(Qname collectionId, Date releaseDate, int page) {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PRIVATE)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.setDefaultFilters(false).build(),
				page,
				LIMIT)
				.setSelected(SELECTED);
		Filters filters = query.getFilters();
		filters.setSecureReason(SecureReason.DATA_QUARANTINE_PERIOD);
		filters.setCollectionId(collectionId);
		filters.setTime("1700-01-01/"+DateUtils.format(releaseDate, "yyyy-MM-dd"));
		return query;
	}

}
