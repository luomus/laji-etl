package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class LajistoreAnnotationHarmonizer implements Harmonizer<JSONObject> {

	private static final String OCCURRENCE_AT_TIME_OF_ANNOTATION = "occurrenceAtTimeOfAnnotation";
	private static final String BY_ROLE = "byRole";
	private static final String ATLAS_CODE = "atlasCode";
	private static final String REMOVED_TAGS = "removedTags";
	private static final String ADDED_TAGS = "addedTags";
	private static final String IDENTIFICATION = "identification";
	private static final String NOTES = "notes";
	private static final String ANNOTATION_BY_PERSON = "annotationByPerson";
	private static final String ANNOTATION_BY_SYSTEM = "annotationBySystem";
	private static final String DELETED_BY_PERSON =  "deletedByPerson";
	private static final String CREATED = "created";
	private static final String DELETED_AT = "deletedDateTime";
	private static final String TARGET_ID = "targetID";
	private static final String ROOT_ID = "rootID";
	private static final String ID = "id";

	@Override
	public List<DwRoot> harmonize(JSONObject jsonObject, Qname source) throws CriticalParseFailure {
		DwRoot root = DwRoot.forAnnotations(source);
		for (JSONObject i : jsonObject.getArray("roots").iterateAsObject()) {
			JSONObject annotation = i.getObject("annotation");
			try {
				root.addAnnotation(parse(annotation));
			} catch (Exception e) {
				throw new CriticalParseFailure("Parsing annotation", e);
			}
		}
		return Utils.singleEntryList(root);
	}

	private Annotation parse(JSONObject json) throws CriticalParseFailure, DataValidationException {
		Qname id = qname(ID, json);
		Qname documentId = qname(ROOT_ID, json);
		Qname targetId = qname(TARGET_ID, json);
		String created = json.getString(CREATED);

		Annotation annotation = new Annotation(id, documentId, targetId, Util.toTimestamp(created));

		if (json.hasKey(ANNOTATION_BY_PERSON)) {
			annotation.setAnnotationByPerson(qname(ANNOTATION_BY_PERSON, json));
		}

		if (json.hasKey(ANNOTATION_BY_SYSTEM)) {
			annotation.setAnnotationBySystem(qname(ANNOTATION_BY_SYSTEM, json));
		}

		if (json.hasKey(IDENTIFICATION)) {
			Identification identification = JsonToModel.objectFromJson(json.getObject(IDENTIFICATION), new Identification());
			if (identification.hasIdentification()) {
				annotation.setIdentification(identification);
			}
		}

		if (json.hasKey(OCCURRENCE_AT_TIME_OF_ANNOTATION)) {
			OccurrenceAtTimeOfAnnotation o = JsonToModel.objectFromJson(json.getObject(OCCURRENCE_AT_TIME_OF_ANNOTATION), new OccurrenceAtTimeOfAnnotation());
			annotation.setOccurrenceAtTimeOfAnnotation(o);
		}

		String notes = json.getString(NOTES);
		if (given(notes)) {
			annotation.setNotes(notes);
		}

		for (String tag : json.getArray(ADDED_TAGS).iterateAsString()) {
			annotation.addTag(new Qname(tag));
		}
		for (String tag : json.getArray(REMOVED_TAGS).iterateAsString()) {
			annotation.removeTag(new Qname(tag));
		}

		if (json.hasKey(ATLAS_CODE)) {
			annotation.setAtlasCode(qname(ATLAS_CODE, json));
		}

		if (json.hasKey("deleted")) {
			annotation.setDeleted(json.getBoolean("deleted"));
		}
		String deletedAt = json.getString(DELETED_AT);
		if (given(deletedAt)) {
			annotation.setDeletedTimestamp(Util.toTimestamp(deletedAt));
		}
		if (json.hasKey(DELETED_BY_PERSON)) {
			annotation.setDeletedByPerson(qname(DELETED_BY_PERSON, json));
		}

		if (json.hasKey(BY_ROLE)) {
			annotation.setByRole(qname(BY_ROLE, json));
		}

		return annotation;
	}

	private Qname qname(String field, JSONObject json) {
		String value = json.getString(field);
		if (!given(value)) return null;
		if (value.startsWith("http://")) {
			return Qname.fromURI(value);
		}
		return new Qname(value);
	}

	private boolean given(Object o) {
		return o != null && o.toString().trim().length() > 0;
	}

}
