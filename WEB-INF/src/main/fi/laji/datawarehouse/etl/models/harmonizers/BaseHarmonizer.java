package fi.laji.datawarehouse.etl.models.harmonizers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

import com.google.common.math.IntMath;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateParserUtil;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public abstract class BaseHarmonizer<K> implements Harmonizer<K> {

	private static final String DD_MM_YYYY = "dd.MM.yyyy";
	private static final String YYYY_MM_DD = "yyyy-MM-dd";
	private static final int FULL_ISO_DATE_LENGTH = YYYY_MM_DD.length();
	protected static final String HTTP = "http://";

	private static final Map<String, String> ROMAN_NUMERAL_MONTHS;
	static {
		ROMAN_NUMERAL_MONTHS = new LinkedHashMap<>(); // order of keys is important for replace to work as needed
		ROMAN_NUMERAL_MONTHS.put("VIII", "8");
		ROMAN_NUMERAL_MONTHS.put("VII", "7");
		ROMAN_NUMERAL_MONTHS.put("VI", "6");
		ROMAN_NUMERAL_MONTHS.put("IV", "4");
		ROMAN_NUMERAL_MONTHS.put("V", "5");
		ROMAN_NUMERAL_MONTHS.put("XII", "12");
		ROMAN_NUMERAL_MONTHS.put("XI", "11");
		ROMAN_NUMERAL_MONTHS.put("IX", "9");
		ROMAN_NUMERAL_MONTHS.put("X", "10");
		ROMAN_NUMERAL_MONTHS.put("III", "3");
		ROMAN_NUMERAL_MONTHS.put("II", "2");
		ROMAN_NUMERAL_MONTHS.put("I", "1");
	}

	private static final String MY_LIFE_STAGE_ALIVE = "MY.lifeStageAlive";
	private static final String MY_LIFE_STAGE_DEAD = "MY.lifeStageDead";
	private static final String MY_PLANT_LIFE_STAGE_DEAD_PLANT = "MY.plantLifeStageDeadPlant";
	private static final String MY_PLANT_LIFE_STAGE_LIVING_PLANT = "MY.plantLifeStageLivingPlant";

	private static <T extends Enum<T>> void addEnumLinkings(Class<T> enumType, Map<String, T> map) {
		EnumToProperty enumToProperty = EnumToProperty.getInstance();
		for (T enumeration : enumType.getEnumConstants()) {
			for (Qname qname : enumToProperty.getAll(enumeration)) {
				map.put(qname.toURI().toLowerCase(), enumeration);
				map.put(qname.toString().toLowerCase(), enumeration);
				map.put(enumeration.name().toLowerCase(), enumeration);
			}
		}
	}

	private static Map<String, RecordBasis> RECORD_BASIS_MAP = initRecordBasisMap();

	protected RecordBasis getRecordBasis(String value) {
		if (value == null) return null;
		return RECORD_BASIS_MAP.get(value.toLowerCase());
	}

	private static Map<String, RecordBasis> initRecordBasisMap() {
		Map<String, RecordBasis> map = new HashMap<>();

		addEnumLinkings(RecordBasis.class, map);

		map.put("preservedspecimen", RecordBasis.PRESERVED_SPECIMEN);
		map.put("livingspecimen", RecordBasis.LIVING_SPECIMEN);
		map.put("fossilespecimen", RecordBasis.FOSSIL_SPECIMEN);
		map.put("otherspecimen", RecordBasis.PRESERVED_SPECIMEN);
		map.put("humanobservation", RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		map.put("drawingorphotograph", RecordBasis.HUMAN_OBSERVATION_PHOTO);
		map.put("multimediaobject", RecordBasis.HUMAN_OBSERVATION_VIDEO);
		map.put("subfossilspecimen", RecordBasis.SUBFOSSIL_SPECIMEN);
		map.put("indirectsampleNest", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		map.put("indirectsampleFaeces", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		map.put("indirectsampleFeedingMarks", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		map.put("indirectobservation", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		map.put("literature", RecordBasis.LITERATURE);
		map.put("machineobservation", RecordBasis.MACHINE_OBSERVATION_UNSPECIFIED);
		map.put("otherspecimen", RecordBasis.PRESERVED_SPECIMEN);
		map.put("materialsample", RecordBasis.MATERIAL_SAMPLE);

		add(RecordBasis.HUMAN_OBSERVATION_SEEN, map, "SN"); // suora näköhavainto
		add(RecordBasis.HUMAN_OBSERVATION_SEEN, map, "SJ"); // suora jätöshavainto
		add(RecordBasis.HUMAN_OBSERVATION_INDIRECT, map, "EJ"); // epäsuora jätöshavainto
		add(RecordBasis.HUMAN_OBSERVATION_SEEN, map, "SP"); // suora pesähavainto
		add(RecordBasis.HUMAN_OBSERVATION_INDIRECT, map, "EP"); // epäsuora pesähavainto
		add(RecordBasis.HUMAN_OBSERVATION_SEEN, map, "SR"); // suora ruokailupaikka
		add(RecordBasis.HUMAN_OBSERVATION_INDIRECT, map, "ER"); // epäsuora ruokailupaikka
		add(RecordBasis.HUMAN_OBSERVATION_HEARD, map, "SK"); // suora kuulohavainto

		return map;
	}

	private static void add(RecordBasis recordBasis, Map<String, RecordBasis> map, String  ... values) {
		for (String v : values) {
			map.put(v.toLowerCase(), recordBasis);
		}
	}

	private static final Map<String, Unit.LifeStage> LIFE_STAGES = initLifeStages();

	protected LifeStage getLifeStage(Qname value) {
		return getLifeStage(value.toString());
	}

	protected LifeStage getLifeStage(String value) {
		if (value == null) return null;
		return LIFE_STAGES.get(value.toLowerCase());
	}

	private static Map<String, LifeStage> initLifeStages() {
		Map<String, LifeStage> map = new HashMap<>();

		addEnumLinkings(LifeStage.class, map);

		add(LifeStage.ADULT, map, "aikuinen", "aikuista", "image", "ad", "ad.");
		add(LifeStage.JUVENILE, map, "juv", "juv.", "nuori");
		add(LifeStage.IMMATURE, map, "keskenkasvuinen", "poikanen", "poikanen (linnut)", "pullus", "pull", "MY.lifeStagePullus", new Qname("MY.lifeStagePullus").toURI());
		add(LifeStage.EGG, map, "muna", "munasto", "kuoriutunut muna", "hatched egg", "MY.lifeStageHatchedEgg", new Qname("MY.lifeStageHatchedEgg").toURI());
		add(LifeStage.TADPOLE, map, "nuijapää");
		add(LifeStage.PUPA, map, "kotelo", "kuoriutunut kotelo", "kotelokehto", "koppa", "pupae", "hatched pupa", "MY.lifeStageHatchedPupa", new Qname("MY.lifeStageHatchedPupa").toURI());
		add(LifeStage.NYMPH, map, "nymfi");
		add(LifeStage.SUBIMAGO, map, "esiaikuinen");
		add(LifeStage.LARVA, map, "larvae", "toukka", "toukkaa");
		add(LifeStage.SNAG, map, "TODO");
		add(LifeStage.EMBRYO, map, "alkio", "sikiö", "alkio tai sikiö", "fetus", "embryo or fetus");
		add(LifeStage.SUBADULT, map, "TODO");
		add(LifeStage.MATURE, map, "sukukypsä");
		add(LifeStage.STERILE, map, "steriili");
		add(LifeStage.FERTILE, map, "fertiili");
		add(LifeStage.GALL, map, "äkämä");
		add(LifeStage.TRIUNGULIN, map, "triunguliini");

		add(LifeStage.SPROUT, map, "verso");
		add(LifeStage.DEAD_SPROUT, map, "kuollut verso", "dead sprout");
		add(LifeStage.BUD, map, "nuppu", "osa nupulla", "nupulla");
		add(LifeStage.FLOWER, map, "kukka", "kukkii", "osa kukkii", "muutama kukki", "kukkii osittain");
		add(LifeStage.WITHERED_FLOWER, map, "kuihtunut kukka", "on kukkinut", "kukkinut", "kukki");
		add(LifeStage.SEED, map, "siemen", "itiö", "hedelmä", "seed", "spore", "fruit");
		add(LifeStage.RIPENING_FRUIT, map, "kypsyviä siemeniä", "kypsyviä hedelmiä", "kypsyviä itiöitä", "kypsyvä siemen", "kypsyvä hedelmä", "kypsyvä itiö", "ripening seed", "ripening fruit", "ripening spore");
		add(LifeStage.RIPE_FRUIT, map, "kypsiä siemeniä", "kypsiä hedelmiä", "kypsiä itiöitä", "kypsä siemen", "kypsä hedelmä", "kypsä itiö", "ripe seed", "ripe fruit", "ripe spore");
		add(LifeStage.SUBTERRANEAN, map, "mukula", "sipuli", "juuri");

		add(LifeStage.MARKS, map, "jäljet", "syömäjäljet", "jäljet (syömäjäljet tms.)", "feeding marks", "marks (feeding marks etc.)");
		return map;
	}

	private static void add(LifeStage lifeStage, Map<String, LifeStage> map, String ... values) {
		for (String v : values) {
			map.put(v.toLowerCase(), lifeStage);
		}
	}

	protected void parseLifeStageAlive(Unit unit, Qname lifeStageId, Qname plantLifeStageId, String alive) {
		parseLifeStageAlive(unit, lifeStageId == null ? null : lifeStageId.toString(), plantLifeStageId == null ? null : plantLifeStageId.toString(), alive);
	}

	protected void parseLifeStageAlive(Unit unit, String lifeStageId, String plantLifeStageId, String alive) {
		if (given(alive)) {
			unit.setAlive(Boolean.valueOf(alive));
		}

		if (given(plantLifeStageId)) {
			LifeStage plantLifeStage = getLifeStage(plantLifeStageId);
			if (plantLifeStage != null) {
				unit.setLifeStage(plantLifeStage);
				return;
			}
			if (unit.isAlive() == null) {
				if (MY_PLANT_LIFE_STAGE_LIVING_PLANT.equals(plantLifeStageId)) {
					unit.setAlive(true);
				}
				if (MY_PLANT_LIFE_STAGE_DEAD_PLANT.equals(plantLifeStageId)) {
					unit.setAlive(false);
				}
			}
		}

		if (!given(lifeStageId)) return;

		LifeStage lifeStage = getLifeStage(lifeStageId);
		if (lifeStage != null) {
			unit.setLifeStage(lifeStage);
			return;
		}

		if (unit.isAlive() == null) {
			if (MY_LIFE_STAGE_DEAD.equals(lifeStageId)) {
				unit.setAlive(false);
			}
			if (MY_LIFE_STAGE_ALIVE.equals(lifeStageId)) {
				unit.setAlive(true);
			}
		}
	}

	private static final Map<String, Unit.Sex> SEXES = initSexes();

	protected Sex getSex(String value) {
		if (value == null) return null;
		return SEXES.get(value.toLowerCase());
	}

	private static Map<String, Sex> initSexes() {
		Map<String, Sex> map = new HashMap<>();
		addEnumLinkings(Sex.class, map);

		map.put("unknowable", Sex.UNKNOWN);
		map.put("female", Sex.FEMALE);
		map.put("male", Sex.MALE);
		map.put("gynandromorph", Sex.GYNANDROMORPH);

		return map;
	}

	private static final Map<String, TaxonConfidence> TAXON_CONFIDENCES = initTaxonConfidences();

	protected TaxonConfidence getTaxonConfidence(String value) {
		if (value == null) return null;
		return TAXON_CONFIDENCES.get(value.toLowerCase());
	}

	private static Map<String, TaxonConfidence> initTaxonConfidences() {
		Map<String, TaxonConfidence> map = new HashMap<>();
		addEnumLinkings(TaxonConfidence.class, map);
		return map;
	}

	protected void handleException(Exception e) throws UnknownHarmonizingFailure, CriticalParseFailure {
		if (e instanceof CriticalParseFailure) throw (CriticalParseFailure) e;
		throw new UnknownHarmonizingFailure(e);
	}

	protected Qname buildDocumentUriIdentifier(Qname source, String documentId) throws CriticalParseFailure {
		if (!given(documentId)) throw new CriticalParseFailure("Document ID is not given.");
		if (documentId.startsWith(HTTP)) return Qname.fromURI(documentId);
		return Qname.fromURI(source.toURI() + "/" + documentId);
	}

	protected Qname buildUriIdentifier(Document parentDocument) {
		return Qname.fromURI(parentDocument.getDocumentId().toURI() + "#" + parentDocument.nextSeqValue());
	}

	protected String buildUriIdentifier(String gatheringOrUnitId, Document parentDocument) {
		if (given(gatheringOrUnitId) && gatheringOrUnitId.startsWith(HTTP)) return gatheringOrUnitId;
		return parentDocument.getDocumentId() + "#" + parentDocument.nextSeqValue();
	}

	public static Coordinates parseYKJCoordinates(String ykj) throws DataValidationException {
		if (!given(ykj)) return null;
		if (ykj.startsWith("/")) ykj = ykj.substring(1);
		if (!given(ykj)) return null;
		if (ykj.endsWith("/")) ykj = ykj.substring(0, ykj.length()-1);
		if (!given(ykj)) return null;
		if (!ykj.contains(":")) throw new DataValidationException("Invalid YKJ grid " + ykj);
		try {
			if (ykj.contains("/")) return parseYKJCoordinatesGridMany(ykj);
			if (ykj.contains("-")) return parseYKJCoordinatesGridSlide(ykj);
			return parseYKJCoordinatesGrid(ykj);
		} catch (DataValidationException e) {
			throw e;
		} catch (NumberFormatException e) {
			throw new DataValidationException("Invalid number: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new DataValidationException("Invalid coordinates: " + ykj, e);
		}
	}

	public static Coordinates parseEurefCoordinates(String euref) throws DataValidationException {
		if (!given(euref)) return null;
		if (!euref.contains(":")) throw new DataValidationException("Invalid EUREF grid" + euref);
		try {
			return parseEurefCoordinatesGrid(euref);
		} catch (DataValidationException e) {
			throw e;
		} catch (NumberFormatException e) {
			throw new DataValidationException("Invalid number: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new DataValidationException("Invalid coordinates: " + euref, e);
		}
	}

	private static Coordinates parseEurefCoordinatesGrid(String euref) throws DataValidationException {
		String[] parts = euref.split(Pattern.quote(":"));
		if (parts.length != 2) {
			throw new DataValidationException("Invalid euref coordinates grid: '" + euref + "'; expected 'lat:lon'");
		}
		String lat = parts[0];
		String lon = parts[1];
		if (lon.startsWith("8") && lon.length() > 1) lon = lon.substring(1);
		int latMin = Double.valueOf(lat).intValue();
		int lonMin = Double.valueOf(lon).intValue();
		int latMax = latMin + 1;
		int lonMax = lonMin + 1;
		if (lat.length() < 7) {
			int multiplier = IntMath.pow(10, 7 - lat.length());
			latMin *= multiplier;
			lonMin *= multiplier;
			latMax *= multiplier;
			lonMax *= multiplier;
		}
		return new Coordinates(latMin, latMax, lonMin, lonMax, Type.EUREF);
	}

	private static Coordinates parseYKJCoordinatesGridSlide(String ykj) throws DataValidationException {
		String[] parts = ykj.split(Pattern.quote(":"));
		String[] latParts = parts[0].split(Pattern.quote("-"));
		String[] lonParts = parts[1].split(Pattern.quote("-"));

		int latMin = Integer.valueOf(latParts[0]);
		int latMax = ykjSlideMax(latParts, latMin);
		int lonMin = Integer.valueOf(lonParts[0]);
		int lonMax = ykjSlideMax(lonParts, lonMin);

		if (latMax < latMin) throw new DataValidationException("Lat max is smaller than min: " + ykj);
		if (lonMax < lonMin) throw new DataValidationException("Lon max is smaller than min: " + ykj);

		return new Coordinates(latMin, latMax+1, lonMin, lonMax+1, Type.YKJ);
	}

	private static int ykjSlideMax(String[] parts, int min) throws DataValidationException {
		// 673-674 -> 675
		// 673-4 -> 675
		// 673-85 -> 686
		// 331-9 -> 340
		// 331-41 -> 342
		// 331-49 -> 350
		// 331-99 -> 400
		int max = parts.length > 1 ? Integer.valueOf(parts[1]) : min;
		if (length(min) < length(max)) throw new DataValidationException("Invalid slide: " + min + "-" + max);
		if (length(max) < length(min)) {
			int exponent = length(min) - (length(min) - length(max));
			max = Integer.valueOf(String.valueOf((int) (min / Math.pow(10, exponent))) + max);
		}
		return max;
	}

	private static int length(int numeric) {
		return String.valueOf(numeric).length();
	}

	private static Coordinates parseYKJCoordinatesGridMany(String ykj) throws DataValidationException {
		String[] coordinatePairs = ykj.split(Pattern.quote("/"));
		SortedSet<Integer> lats = new TreeSet<>();
		SortedSet<Integer> lons = new TreeSet<>();
		for (String coordinatePair : coordinatePairs) {
			if (!given(coordinatePair)) continue;

			String[] parts = coordinatePair.split(Pattern.quote(":"));

			if (parts[0].length() != 7) throw new DataValidationException("When reporting YKJ Grid (separated by /), length of coordinates must be 7: " + parts[0]);
			if (parts[1].length() != 7) throw new DataValidationException("When reporting YKJ Grid (separated by /), length of coordinates must be 7: " + parts[1]);

			Coordinates one = parseYKJCoordinates(coordinatePair);
			lats.add(toYKJLength(one.getLatMin().intValue()));
			lats.add(toYKJLength(one.getLatMax().intValue()-1));
			lons.add(toYKJLength(one.getLonMin().intValue()));
			lons.add(toYKJLength(one.getLonMax().intValue()-1));
		}
		Coordinates boundingBox = new Coordinates(lats.first(), lats.last(), lons.first(), lons.last(), Type.YKJ);
		return boundingBox;
	}

	private static int toYKJLength(int ykj) {
		String ykjS = String.valueOf(ykj);
		while (ykjS.length() < 7) {
			ykjS = ykjS + "0";
		}
		return Integer.valueOf(ykjS);
	}

	private static Coordinates parseYKJCoordinatesGrid(String ykj) throws DataValidationException {
		String[] parts = ykj.split(Pattern.quote(":"));
		if (parts.length != 2) {
			throw new DataValidationException("Invalid ykj coordinates grid: '" + ykj + "'; expected 'lat:lon'");
		}
		String lat = parts[0];
		String lon = parts[1];
		lon = fixLonYKJCoordinateFront3(lat, lon);
		return new Coordinates(Integer.valueOf(lat), Integer.valueOf(lon), Type.YKJ);
	}

	private static String fixLonYKJCoordinateFront3(String lat, String lon) {
		if (lon.startsWith("3")) return lon;
		if (lat.length() - lon.length() == 1) {
			return "3"+lon;
		}
		if (lat.length() - lon.length() == 2) {
			return "30"+lon;
		}
		return lon;
	}

	public static Date parseDate(String dateString) {
		if (!given(dateString)) return null;
		// 2013-11-01T08:11:30+0200
		// 01.10.2015
		// 1.10.2015
		// 2.7.510
		// 2008-10-25 00:00:00
		// 18/7 1951 (quite common format)
		if (dateString.length() < 8) {
			return null;
		}
		dateString = fixDateString(dateString);
		try {
			DateParserUtil util = new DateParserUtil(DateParserUtil.STRICT_MODE);
			String isoDate = util.parseDate(dateString);
			return DateUtils.convertToDate(isoDate, YYYY_MM_DD);
		} catch (Exception e) {
			return null;
		}
	}

	private static String fixDateString(String dateString) {
		if (dateString == null) return null;
		if (dateString.contains("/") && dateString.contains(" ")) {
			String[] parts = dateString.split(Pattern.quote(" "));
			if (parts.length == 2) {
				if (parts[1].length() == 4 && !parts[1].contains("/")) {
					if (parts[0].contains("/")) {
						String fixed = dateString.replace("/", ".").replace(" ", ".");
						if (Utils.countNumberOf(".", fixed) == 2) return fixed;
					}
				}
			}
		}
		return dateString;
	}

	private static final String NULL_NULL = "null null";
	private static final String NULL = "null";

	protected static boolean given(String s) {
		if (s == null) return false;
		s = s.trim();
		return s.length() > 0  && !s.equalsIgnoreCase(NULL) && !s.equalsIgnoreCase(NULL_NULL);
	}

	protected static boolean given(Object ... objects) {
		for (Object o : objects) {
			if (o == null) return false;
			if (!given(o.toString())) return false;
		}
		return true;
	}

	protected String getContents(String field, Node root) {
		if (root.hasChildNodes(field)) {
			String contents = root.getNode(field).getContents();
			if (!given(contents)) return null;
			return contents;
		}
		return null;
	}

	protected List<String> getAllContents(String field, Node root) {
		if (!root.hasChildNodes(field)) return Collections.emptyList();
		List<String> list = new ArrayList<>();
		for (Node n : root.getChildNodes(field)) {
			String contents = n.getContents();
			if (!given(contents)) continue;
			list.add(contents);
		}
		return list;
	}

	public static Gathering parseEventDateTime(String isoDateTime, Gathering gathering) {
		if (!given(isoDateTime)) return null;
		isoDateTime = Utils.removeWhitespace(isoDateTime);
		if (isoDateTime.endsWith("/")) isoDateTime = isoDateTime.substring(0, isoDateTime.length()-1);
		if (!given(isoDateTime)) return null;

		String isoDateTimeOrig = isoDateTime;
		String dateMask = "";
		try {
			dateMask = generateDateMask(isoDateTime);
			if (dateMask.equals("D")) {
				return parseDateTime(isoDateTime, null, null, null, gathering);
			}
			if (dateMask.equals("/D")) {
				return parseDateTime(null, isoDateTime.replace("/", ""), null, null, gathering);
			}
			if (dateMask.equals("DT")) {
				String[] parts = isoDateTime.split(Pattern.quote("T"));
				return parseDateTime(parts[0], null, parts[1], null, gathering);
			}
			if (dateMask.equals("/DT")) {
				String[] parts = isoDateTime.replace("/", "").split(Pattern.quote("T"));
				return parseDateTime(null, parts[0], null, parts[1], gathering);
			}
			if (dateMask.equals("D/D")) {
				String[] parts = isoDateTime.split(Pattern.quote("/"));
				return parseDateTime(parts[0], parts[1], null, null, gathering);
			}
			if (dateMask.equals("DT/DT")) {
				String[] parts = isoDateTime.split(Pattern.quote("/"));
				String[] beginParts = parts[0].split(Pattern.quote("T"));
				String[] endParts = parts[1].split(Pattern.quote("T"));
				return parseDateTime(beginParts[0], endParts[0], beginParts[1], endParts[1], gathering);
			}
			if (dateMask.equals("DT/T")) {
				String[] parts = isoDateTime.split(Pattern.quote("/"));
				String[] beginParts = parts[0].split(Pattern.quote("T"));
				String startDate = beginParts[0];
				String startTime = beginParts[1];
				String endTime = parts[1];
				return parseDateTime(startDate, null, startTime, endTime, gathering);
			}
			if (dateMask.equals("DT/D")) {
				String[] parts = isoDateTime.split(Pattern.quote("/"));
				String[] beginParts = parts[0].split(Pattern.quote("T"));
				String startDate = beginParts[0];
				String startTime = beginParts[1];
				String endDate = parts[1];
				return parseDateTime(startDate, endDate, startTime, null, gathering);
			}
			if (dateMask.equals("D/DT")) {
				String[] parts = isoDateTime.split(Pattern.quote("/"));
				String startDate = parts[0];
				String[] endParts = parts[1].split(Pattern.quote("T"));
				String endDate = endParts[0];
				String endTime = endParts[1];
				return parseDateTime(startDate, endDate, null, endTime, gathering);
			}
			if (dateMask.equals("DT/T")) {
				String[] parts = isoDateTime.split(Pattern.quote("T"));
				String date = parts[0];
				String[] timeParts = parts[1].split(Pattern.quote("/"));
				String startTime = timeParts[0];
				String endTime = timeParts[1];
				return parseDateTime(date, date, startTime, endTime, gathering);
			}
		} catch (DateValidationException e) {
			return createTimeIssue(gathering, e);
		} catch (Exception e) {
			String message = "Unparsable date \"" + isoDateTimeOrig + "\".";
			if (given(e.getMessage())) {
				message += " Cause: " + e.getMessage();
			}
			message += " Interpreted date format: " + dateMask;
			return createTimeIssue(gathering, Quality.Issue.INVALID_DATE, message);
		}
		return createTimeIssue(gathering, Quality.Issue.INVALID_DATE, "Unsupported date format \"" + isoDateTimeOrig + "\". Interpreted date format: " + dateMask);
	}

	public static Gathering createTimeIssue(Gathering gathering, DateValidationException e) {
		return createTimeIssue(gathering, e.getIssue(), e.getMessage());
	}

	public static Gathering createTimeIssue(Gathering gathering, Quality.Issue timeIssue, String message) {
		try {
			gathering.setEventDate(null);
			gathering.setHourBegin(null);
			gathering.setHourEnd(null);
			gathering.setMinutesBegin(null);
			gathering.setMinutesEnd(null);
		} catch (DateValidationException e) {
			throw new IllegalStateException("Nulls should not cause validation exceptions" , e);
		}
		gathering.createQuality().setTimeIssue(new Quality(timeIssue, Quality.Source.AUTOMATED_FINBIF_VALIDATION, message));
		return gathering;
	}

	private static Gathering parseDateTime(String dateBegin, String dateEnd, String timeBegin, String timeEnd, Gathering gathering) throws ParseException, DateValidationException {
		Date begin = null;
		Date end = null;
		dateBegin = fixDates(dateBegin);
		dateEnd = fixDates(dateEnd);
		if (dateBegin != null) {
			if (dateBegin.contains("-")) {
				String fullDateBegin = toFullIsoDate(dateBegin);
				dateEnd = toFullIsoDate(dateEnd, dateBegin);
				begin = DateUtils.convertToDate(fullDateBegin, YYYY_MM_DD);
				end = dateEnd == null ? null : DateUtils.convertToDate(dateEnd, YYYY_MM_DD);
			} else if (dateBegin.contains(".")) {
				begin = DateUtils.convertToDate(dateBegin, DD_MM_YYYY);
				end = dateEnd == null ? null : DateUtils.convertToDate(dateEnd, DD_MM_YYYY);
			} else {
				begin = DateUtils.convertToDate("1.1."+dateBegin);
				end = dateEnd == null ? DateUtils.convertToDate("31.12."+dateBegin) : DateUtils.convertToDate("31.12."+dateEnd);
			}
		} else if (dateEnd != null) {
			if (dateEnd.contains("-")) {
				end = DateUtils.convertToDate(toFullIsoDate(dateEnd, dateBegin), YYYY_MM_DD);
			} else if (dateEnd.contains(".")) {
				end = DateUtils.convertToDate(dateEnd, DD_MM_YYYY);
			} else {
				end = DateUtils.convertToDate("31.12."+dateEnd);
			}
		}

		gathering.setEventDate(new DateRange(begin, end));
		if (timeEnd == null && timeBegin != null && timeBegin.contains("-")) {
			String[] parts = timeBegin.split(Pattern.quote("-"));
			timeBegin = parts[0];
			timeEnd = parts[1];
		}
		Integer hourBegin = parseHour(timeBegin);
		Integer hourEnd = parseHour(timeEnd);
		Integer minutesBegin = parseMinutes(timeBegin);
		Integer minutesEnd = parseMinutes(timeEnd);
		if (hourEnd != null && minutesEnd == null) {
			if (hourEnd.intValue() == 24 || hourEnd.intValue() == 2400) {
				hourEnd = 23;
				minutesEnd = 59;
			}
		}
		if (minutesBegin == null && isHourWithMinutes(hourBegin)) {
			hourBegin = fixHourWithMinutes(hourBegin);
			minutesBegin = fixHourWithMinutesMinutes(hourBegin);
		}
		if (minutesEnd == null && isHourWithMinutes(hourEnd)) {
			if (hourEnd != null) {
				hourEnd = fixHourWithMinutes(hourEnd);
				minutesEnd = fixHourWithMinutesMinutes(hourEnd);
			}
		}
		gathering.setHourBegin(hourBegin);
		gathering.setHourEnd(hourEnd);
		gathering.setMinutesBegin(minutesBegin);
		gathering.setMinutesEnd(minutesEnd);
		return gathering;
	}

	private static Integer fixHourWithMinutesMinutes(int hourWithMinutes) {
		int hours = fixHourWithMinutes(hourWithMinutes);
		return hourWithMinutes - (hours * 100);
	}

	private static Integer fixHourWithMinutes(int hourWithMinutes) {
		return (int) Math.ceil(hourWithMinutes / 100);
	}

	private static boolean isHourWithMinutes(Integer hour) {
		if (hour == null) return false;
		return hour.intValue() >= 100 && hour.intValue() <= 2359;
	}

	private static String fixDates(String date) {
		if (date == null) return null;
		for (String romanNumeral : ROMAN_NUMERAL_MONTHS.keySet()) {
			if (date.contains(romanNumeral)) {
				date = date.replace(romanNumeral, ROMAN_NUMERAL_MONTHS.get(romanNumeral));
			}
		}
		if (date.contains(".")) {
			String[] parts = date.split(Pattern.quote("."));
			if (parts.length == 3 && parts[0].length() == 4) {
				return date.replace(".", "-");
			}
		}
		if (date.endsWith("Z")) date = date.replace("Z", "");
		return date;
	}

	private static String toFullIsoDate(String dateEnd, String originalDateBegin) {
		if (dateEnd == null) {
			if (originalDateBegin.length() == FULL_ISO_DATE_LENGTH) {
				return null;
			}
			// begin = 1960-05
			String[] parts = originalDateBegin.split(Pattern.quote("-"));
			if (parts.length != 2) throw new IllegalStateException();
			int year = Integer.parseInt(parts[0]);
			int month = Integer.parseInt(parts[1]);
			int daysInMonth = getNumberOfDaysInMonth(year, month);
			return originalDateBegin + "-" + daysInMonth;
		}

		if (dateEnd.length() == FULL_ISO_DATE_LENGTH) return dateEnd;

		String[] parts = dateEnd.split(Pattern.quote("-"));
		if (parts.length == 1 && parts[0].length() == 4) {
			// end = 1960
			return dateEnd + "-12-31";
		}

		if (parts.length == 2 && parts[0].length() == 4) {
			// end = 1960-07
			int year = Integer.parseInt(parts[0]);
			int month = Integer.parseInt(parts[1]);
			int daysInThisMonth = getNumberOfDaysInMonth(year, month);
			return dateEnd + "-" + daysInThisMonth;
		}

		if (parts[0].length() == 2) {
			//  1999-01-01/02-05 or
			//  1999-01-01/05
			if (originalDateBegin == null) throw new IllegalArgumentException("Shorthand end date without begin date");
			String fullDateBegin = toFullIsoDate(originalDateBegin);
			String commonPart = fullDateBegin.substring(0, FULL_ISO_DATE_LENGTH - dateEnd.length());
			return commonPart + dateEnd;
		}
		throw new IllegalArgumentException("Unparseable iso date " + dateEnd);
	}

	private static String toFullIsoDate(String dateBegin) {
		if (dateBegin.length() == FULL_ISO_DATE_LENGTH) {
			return dateBegin;
		}
		return  dateBegin + "-01";
	}

	public static String generateDateMask(String isoDateTime) {
		// 2007-03-24
		// 2007-03-24/2007-03-25
		// 1995-02-25T09:01
		// 2014-04-30T17:00/2014-05-01T03:00
		// 1995-02-25T09:01/14:01
		// 25.7.2012T15.00
		// 12.11.2012/17.11.2012T10
		// /25.07.2012
		// 2012-11-12T10:35/2012-11-17
		// 1940/1955
		// 2011-04-04T15.25/16.00

		String[] parts = isoDateTime.split(Pattern.quote("/"));
		if (parts.length == 1) return maskPart(isoDateTime);
		if (parts.length != 2) return "INVALID";
		String mask = maskPart(parts[0]) + "/" + maskPart(parts[1]);
		return mask;
	}

	private static String maskPart(String string) {
		if (string.isEmpty()) return "";
		if (string.contains("T")) return "DT";
		if (string.contains("-")) return "D";
		if (Utils.countNumberOf(".", string) == 2) return "D";
		if (onlyNumbers(string)) return "D";
		return "T";
	}

	private static boolean onlyNumbers(String string) {
		try {
			Integer.valueOf(string);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static int getNumberOfDaysInMonth(int year, int month) {
		Calendar mycal = new GregorianCalendar(year, month-1, 1);
		return mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	private static Integer parseHour(String time) {
		if (!given(time)) return null;
		if (time.contains(":")) time = time.split(Pattern.quote(":"))[0];
		else if (time.contains(".")) time = time.split(Pattern.quote("."))[0];
		else if (time.contains(",")) time = time.split(Pattern.quote(","))[0];
		else if (time.length() == 4) time = time.substring(0, 2);
		return Integer.valueOf(time);
	}

	private static Integer parseMinutes(String time) {
		if (!given(time)) return null;
		String minutes = null;
		if (time.contains(":")) minutes = time.split(Pattern.quote(":"))[1];
		else if (time.contains(".")) minutes = time.split(Pattern.quote("."))[1];
		else if (time.contains(",")) minutes = time.split(Pattern.quote(","))[1];
		else if (time.length() == 4) minutes = time.substring(2, 4);
		if (minutes != null) return Integer.valueOf(minutes);
		return null;
	}

	protected void createCoordinateIssue(Gathering gathering, Type type, DataValidationException e) {
		Quality.Issue issue = coordinateIssueByType(type);
		gathering.createQuality().setLocationIssue(
				new Quality(
						issue,
						Quality.Source.AUTOMATED_FINBIF_VALIDATION,
						e.getMessage()));
	}

	private Quality.Issue coordinateIssueByType(Type type) {
		if (type == Type.EUREF) return Quality.Issue.INVALID_EUREF_COORDINATES;
		if (type == Type.YKJ) return Quality.Issue.INVALID_YKJ_COORDINATES;
		if (type == Type.WGS84) return Quality.Issue.INVALID_WGS84_COORDINATES;
		throw new UnsupportedOperationException("Unknown coordinate type " + type);
	}

	protected void createGeoIssue(Gathering gathering, DataValidationException e) {
		gathering.createQuality().setLocationIssue(
				new Quality(
						Quality.Issue.INVALID_GEO,
						Quality.Source.AUTOMATED_FINBIF_VALIDATION,
						e.getMessage()));
	}

	protected void createEventDateIssue(Gathering gathering, DateValidationException e) {
		gathering.createQuality().setTimeIssue(
				new Quality(
						e.getIssue(),
						Quality.Source.AUTOMATED_FINBIF_VALIDATION,
						e.getMessage()));
	}

	private static final Map<String, Qname> TAXON_CENSUS_TAXAS;
	static {
		TAXON_CENSUS_TAXAS = new HashMap<>();
		TAXON_CENSUS_TAXAS.put("Aves", new Qname("MX.37580"));
		TAXON_CENSUS_TAXAS.put("Mammalia", new Qname("MX.37612"));
		TAXON_CENSUS_TAXAS.put("Rhopalocera", new Qname("MX.60720"));
	}

	protected void taxonCensusFromFacts(Gathering gathering) {
		for (String factValue : gathering.factValues("TaxonCensus")) {
			Qname taxonId = TAXON_CENSUS_TAXAS.get(factValue);
			if (taxonId != null) {
				gathering.addTaxonCensus(new TaxonCensus(taxonId, new Qname("MY.taxonCensusTypeCounted")));
			}
		}
	}

}
