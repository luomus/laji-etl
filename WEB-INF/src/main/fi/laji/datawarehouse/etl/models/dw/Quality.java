package fi.laji.datawarehouse.etl.models.dw;

import java.util.Set;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.utils.Utils;

@Embeddable
public class Quality {

	public enum Issue {
		REPORTED_UNRELIABLE, MEDIA_ISSUE, INVALID_CREATED_DATE, INVALID_MODIFIED_DATE,
		COORDINATES_COUNTRY_MISMATCH, COORDINATES_MUNICIPALITY_MISMATCH, TOO_LARGE_AREA,
		INVALID_GEO, INVALID_YKJ_COORDINATES, INVALID_EUREF_COORDINATES, INVALID_WGS84_COORDINATES,
		DATE_END_BEFORE_BEGIN, DATE_END_GIVEN_WITHOUT_BEGIN, DATE_IN_FUTURE, DATE_TOO_FAR_IN_THE_PAST, INVALID_DATE,
		RECORD_BASIS_MISSING, INVALID_HOUR, INVALID_MINUTE, TIME_END_BEFORE_BEGIN, INVALID_COORDINATES,
		ETL_ISSUE
	}

	public enum Source { AUTOMATED_FINBIF_VALIDATION, ORIGINAL_DOCUMENT, QUALITY_CONTROL }

	public static final Set<Issue> TIME_ISSUES = Utils.set(
			Issue.DATE_END_BEFORE_BEGIN, Issue.DATE_END_GIVEN_WITHOUT_BEGIN,
			Issue.DATE_IN_FUTURE, Issue.DATE_TOO_FAR_IN_THE_PAST, Issue.INVALID_DATE,
			Issue.INVALID_HOUR, Issue.INVALID_MINUTE, Issue.TIME_END_BEFORE_BEGIN);

	public static final Set<Issue> LOCATION_ISSUES = Utils.set(
			Issue.COORDINATES_COUNTRY_MISMATCH, Issue.COORDINATES_MUNICIPALITY_MISMATCH, Issue.INVALID_GEO,
			Issue.INVALID_YKJ_COORDINATES, Issue.INVALID_EUREF_COORDINATES, Issue.INVALID_WGS84_COORDINATES, Issue.INVALID_COORDINATES);

	public static boolean hasIssues(Document document, Gathering gathering, Unit unit) {
		if (document.getQuality() != null && document.getQuality().hasIssues()) {
			return true;
		}
		if (gathering.getQuality() != null && gathering.getQuality().hasIssues()) {
			return true;
		}
		if (unit.getQuality() != null && unit.getQuality().hasIssues()) {
			return true;
		}
		return false;
	}

	private Issue issue;
	private Source source;
	private String message;

	@Deprecated // for hibernate
	public Quality() {}

	public Quality(Issue issue, Source source, String message) {
		this.issue = issue;
		this.source = source;
		this.message = Util.trimToByteLength(message, 5000);
	}

	public Quality(Issue issue, Source source) {
		this(issue, source, null);
	}

	@Transient
	@FileDownloadField(name="Issue", order=1)
	public Issue getIssue() {
		return issue;
	}

	public Quality setIssue(Issue issue) {
		this.issue = issue;
		return this;
	}

	@Transient
	@FileDownloadField(name="Source", order=2)
	public Source getSource() {
		return source;
	}

	public Quality setSource(Source source) {
		this.source = source;
		return this;
	}

	@FileDownloadField(name="Message", order=3)
	public String getMessage() {
		return message;
	}

	public Quality setMessage(String message) {
		this.message = Util.trimToByteLength(message, 5000);
		return this;
	}

	@Override
	public String toString() {
		return "Quality [issue=" + issue + ", source=" + source + ", message=" + message + "]";
	}

	public String humanReadable() {
		return issue + " source: " + source + " message: '" + message + "'";
	}

	public Quality conceal() {
		Quality concealed = new Quality(this.issue, this.source);
		return concealed;
	}

	public Quality copy() {
		return new Quality(this.getIssue(), this.getSource(), this.getMessage());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((issue == null) ? 0 : issue.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quality other = (Quality) obj;
		if (issue != other.issue)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (source != other.source)
			return false;
		return true;
	}

}
