package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class QueueData extends PipeData {

	private final long expires;

	public QueueData(long id, Qname source, long expires, String data) {
		super(id, source, data);
		this.expires = expires;
	}

	public long getExpires() {
		return expires;
	}

	public String expiryAsDateString() {
		return DateUtils.format(expires*1000, "yyyy-MM-dd");
	}

}
