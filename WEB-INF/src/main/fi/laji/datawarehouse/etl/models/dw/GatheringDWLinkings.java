package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;

public class GatheringDWLinkings {

	private List<Person> observers = new ArrayList<>();

	public List<Person> getObservers() {
		return observers;
	}

	public void setObservers(List<Person> observers) {
		this.observers = observers;
	}

}
