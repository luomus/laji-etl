package fi.laji.datawarehouse.etl.models.harmonizers;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class TiiraHarmonizer extends BaseHarmonizer<String> {

	public static final Qname COLLECTION = new Qname("HR.4412");

	//	id	laji	koord_n	koord_e	tarkuus_a	tarkkus_y	pvm_ala	pvm_yla	atlaskoodi	ilmoittaja
	//	61d629fa45705	LOXCUR	675	342	0	50	20220105	20220106	2	tiira.fi

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		String[] parts = data.split(Pattern.quote("\t"));
		if (parts.length == 0) throw new CriticalParseFailure("No data");
		String id = parts[0];
		DwRoot root = parseDocumentId(id, source);
		root.setCollectionId(COLLECTION);
		try {
			parse(parts, root);
		} catch (Exception e) {
			handleException(e);
		}
		return Utils.singleEntryList(root);
	}

	private void parse(String[] parts, DwRoot root) throws CriticalParseFailure {
		Document d = root.createPublicDocument();

		Gathering g = new Gathering(new Qname(d.getDocumentId()+"_G"));
		Integer ykjn = i(parts, 2);
		Integer ykje = i(parts, 3);
		if (given(ykjn, ykje)) {
			try {
				g.setCoordinates(new Coordinates(ykjn, ykje, Type.YKJ));
			} catch (DataValidationException e) {
				createCoordinateIssue(g, Type.YKJ, e);
			}
			g.setCoordinatesVerbatim(ykjn + ":" + ykje);
		}
		g.addFact("tarkkuus_a", s(parts, 4));
		g.addFact("tarkkuus_y", s(parts, 5));
		parseDate(parts, g);
		String team = s(parts, 9);
		if (given(team) && !"tiira.fi".equals(team)) {
			g.addTeamMember(team);
		}
		String birdAssociationArea = s(parts, 10);
		if (given(birdAssociationArea)) g.addTeamMember(birdAssociationArea);

		d.addGathering(g);

		String target = s(parts, 1);
		if (!given(target)) return;
		Unit u = new Unit(new Qname(d.getDocumentId()+"_U"));
		u.setTaxonVerbatim(target);
		Integer atlasCode = i(parts, 8);
		if (given(atlasCode)) {
			u.setAtlasCodeUsingQname(new Qname("MY.atlasCodeEnum"+atlasCode));
		}
		g.addUnit(u);
	}

	private void parseDate(String[] parts, Gathering g) {
		try {
			Date dateBegin = date(parts, 6);
			Date dateEnd = date(parts, 7);
			if (given(dateBegin, dateEnd)) {
				g.setEventDate(new DateRange(dateBegin, dateEnd));
			} else if (given(dateBegin)) {
				g.setEventDate(new DateRange(dateBegin));
			}
		} catch (DateValidationException e) {
			createEventDateIssue(g, e);
		} catch (ParseException e) {
			createTimeIssue(g, Issue.INVALID_DATE, e.getMessage());
		}
	}

	private Date date(String[] parts, int i) throws ParseException {
		String s = s(parts, i);
		if (s == null) return null;
		if (s.length() == 8) {
			return DateUtils.convertToDate(s, "yyyyMMdd");
		}
		return null;
	}

	private String s(String[] parts, int i) {
		if (parts.length <= i) return null;
		return parts[i];
	}

	private Integer i(String[] parts, int i) {
		if (parts.length <= i) return null;
		return Integer.valueOf(parts[i]);
	}

	private DwRoot parseDocumentId(String id, Qname source) throws CriticalParseFailure {
		if (!given(id)) throw new CriticalParseFailure("No document id");
		try {
			return new DwRoot(Qname.fromURI(COLLECTION.toURI()+"/"+id), source);
		} catch (Exception e) {
			throw new CriticalParseFailure(e);
		}
	}

}
