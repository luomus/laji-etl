package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ParallelUtil;
import fi.laji.datawarehouse.etl.utils.ParallelUtil.Task;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.Utils;

public class Interpreter {

	public enum GeoSource { COORDINATES, COORDINATE_CENTERPOINT, REPORTED_VALUE, FINNISH_MUNICIPALITY, OLD_FINNISH_MUNICIPALITY }

	public static final Qname SPRING_MONITORING = new Qname("HR.206");

	private static final Set<LifeStage> BREEDING_SITE_LIFESTAGES = Utils.set(LifeStage.EGG, LifeStage.TADPOLE);

	private static final Set<String> BREEDING_SITE_ATLAS_CLASSES = Utils.set(new Qname("MY.atlasClassEnumC").toURI(), new Qname("MY.atlasClassEnumD").toURI());

	private static final Set<String> NON_WILD_PLANT_STATUS_CODES = Utils.set(
			new Qname("MY.plantStatusCodeR").toURI(),
			new Qname("MY.plantStatusCodeC").toURI(),
			new Qname("MY.plantStatusCodeH").toURI(),
			new Qname("MY.plantStatusCodeG").toURI(),
			new Qname("MY.plantStatusCodeF").toURI()
			);

	private static final Set<String> PAIR_COUNT_FACTS;
	static {
		Qname pairCount = new Qname("MY.pairCount");
		Qname pairCountOuter = new Qname("MY.pairCountOuter");
		Qname pairCountInner = new Qname("MY.pairCountInner");
		PAIR_COUNT_FACTS = Utils.set(
				pairCount.toURI(), pairCount.toString(), "pairCount",
				pairCountInner.toURI(), pairCountInner.toString(), "pairCountInner",
				pairCountOuter.toURI(), pairCountOuter.toString(), "pairCountOuter");
	}

	private static final String OBS_TYPE_BROOD = new Qname("MY.lineTransectObsTypeSeenBrood").toURI();
	private static final String OBS_TYPE_PAIR = new Qname("MY.lineTransectObsTypeSeenPair").toURI();
	private static final String OBS_TYPE_SEEN_NEST = new Qname("MY.lineTransectObsTypeSeenNest").toURI();
	private static final String LIKELY_MIGRANT_FACT = new Qname("MY.likelyMigrant").toURI();
	private static final String OBS_TYPE_FACT = new Qname("MY.lineTransectObsType").toURI();
	private static final String BROOD_SIZE_FACT = new Qname("MY.broodSize").toURI();
	private static final String SHORTHAND_FACT = new Qname("MY.shortHandText").toURI();
	private static final String PAIRCOUNT_FACT = new Qname("MY.pairCount").toURI();
	private static final String COUNT_FACT = new Qname("MY.count").toURI();
	private static final String JUVENILE_IND_COUNT_FACT = new Qname("MY.juvenileIndividualCount").toURI();
	private static final String WATERBIRD_FEMALE_FACT = new Qname("MY.waterbirdFemale").toURI();
	private static final String WATERBIRD_FEMALE_PANIC_FACT = new Qname("MY.waterbirdFemaleEnumLonelyPanicking").toURI();
	private static final String WATERBIRD_FEMALE_YES_FACT = new Qname("MY.waterbirdFemaleEnumYes").toURI();

	private static final Qname AVES = new Qname("MX.37580");
	private static final Qname TAXON_RANK_CLASS = new Qname("MX.class");

	private static final Set<String> UNIT_KEYWORD_FACTS;

	static {
		Qname additionalIDs = new Qname("MY.additionalIDs");
		Qname keywords = new Qname("MY.keywords");
		UNIT_KEYWORD_FACTS = Utils.set(
				additionalIDs.toURI(), additionalIDs.toString(),
				keywords.toURI(), keywords.toString(),
				"additionalIDs", "keywords");
	}

	private class InterpretGatheringTask implements Task<Pair<Gathering, Document>> {
		@Override
		public void execute(Pair<Gathering, Document> datas) throws Exception {
			Gathering gathering = datas.getKey();
			Document document = datas.getValue();
			interpret(gathering, document);
		}
	}

	private final InterpretGatheringTask task = new InterpretGatheringTask();
	private ParallelUtil<Pair<Gathering, Document>> parallel;
	private final DAO dao;
	private final InterpreterForAreas areaInterpreter;
	private final InterpreterForQualityControl qualityInterpreter;
	private final AutomatedTaxonValidator taxonValidator;

	public Interpreter(DAO dao) {
		this.dao = dao;
		this.areaInterpreter = new InterpreterForAreas(dao);
		this.taxonValidator = new AutomatedTaxonValidator(AutomatedTaxonValidatorHardCodedRules.RULES);
		this.qualityInterpreter = new InterpreterForQualityControl(dao, this, taxonValidator);
	}

	public void interpret(DwRoot dwRoot) {
		Document privateDocument = dwRoot.getPrivateDocument();
		Document publicDocument = dwRoot.getPublicDocument();

		if (privateDocument != null) {
			interpret(privateDocument);
		}
		if (publicDocument != null) {
			interpret(publicDocument);
		}
	}

	public void interpret(Document document) {
		if (document == null) return;
		fillInLicenseIfNeeded(document);
		if (notGiven(document.getGatherings())) return;

		if (document.getGatherings().size() == 1) {
			Gathering gathering = document.getGatherings().get(0);
			interpret(gathering, document);
		} else {
			interpretInParallel(document);
		}

		if (isSpringMonitoring(document)) {
			hackSpringMonitoringCoordinateAccuracy(document);
		}
	}

	private void interpretInParallel(Document document) {
		if (parallel == null) {
			parallel = new ParallelUtil<>(dao.getSharedThreadPool());
		}
		parallel.execute(task, parallelDatas(document));
	}

	private List<Pair<Gathering, Document>> parallelDatas(Document document) {
		List<Pair<Gathering, Document>> datas = new ArrayList<>();
		for (Gathering gathering : document.getGatherings()) {
			datas.add(new Pair<>(gathering, document));
		}
		return datas;
	}

	private void fillInLicenseIfNeeded(Document document) {
		if (document.getLicenseId() != null) return;
		CollectionMetadata collectionMetadata = dao.getCollections().get(document.getCollectionId().toURI());
		if (collectionMetadata == null) throw new ETLException("Collection metadata is missing for " + document.getCollectionId());
		document.setLicenseIdUsingQname(collectionMetadata.getIntellectualRights());
	}

	private void hackSpringMonitoringCoordinateAccuracy(Document document) {
		for (Gathering gathering : document.getGatherings()) {
			Coordinates coordinates = gathering.getInterpretations().getCoordinates();
			if (coordinates != null && coordinates.getAccuracyInMeters().intValue() < 10000) {
				coordinates.setAccuracyInMeters(10000);
				gathering.getInterpretations().setCoordinates(coordinates);
			}
		}
	}

	private boolean isSpringMonitoring(Document document) {
		return document.getCollectionId().equals(SPRING_MONITORING);
	}

	private void interpret(Gathering gathering, Document document) {
		fillNamedPlaceInfo(gathering, document);
		areaInterpreter.interpret(gathering, document.getDocumentId());
		interpretTeam(gathering);
		validateTimes(gathering);
		interpretUnits(gathering, document);
	}

	private void interpretUnits(Gathering gathering, Document document) {
		Iterator<Unit> i = gathering.getUnits().iterator();
		while (i.hasNext()) {
			Unit unit = i.next();
			if (noTarget(unit)) {
				i.remove();
				continue;
			}
			interpretUnit(unit, gathering, document);
			interpretUnitAnnotations(document, unit);
			interpretInvasiveControl(unit);
		}
	}

	private void interpretUnitAnnotations(Document document, Unit unit) {
		for (Annotation a : unit.getAnnotations()) {
			interpreterAnnotation(a);
		}
		if (annotatedAsSpam(unit)) {
			document.setSecureLevel(SecureLevel.NOSHOW);
		}
		if (annotatedToCoarse(unit)) {
			document.setSecureLevel(SecureLevel.KM100);
			document.addSecureReason(SecureReason.ADMIN_HIDDEN);
		}
		if (annotatedNonWild(unit)) {
			unit.setWild(false);
		}
	}

	private void interpreterAnnotation(Annotation annotation) {
		if (annotation.getAnnotationByPerson() != null) {
			String personName = dao.getPerson(annotation.getAnnotationByPerson()).getFullName();
			annotation.setAnnotationByPersonName(personName);
		}
		if (annotation.getDeletedByPerson() != null) {
			String personName = dao.getPerson(annotation.getDeletedByPerson()).getFullName();
			annotation.setDeletedByPersonName(personName);
		}
		if (annotation.getAnnotationBySystem() != null) {
			String systemName = annotation.getAnnotationBySystem().toURI();
			Source source = dao.getSources().get(systemName);
			if (source != null && given(source.getName())) {
				systemName = source.getName();
			}
			annotation.setAnnotationBySystemName(systemName);
		}
	}

	private void interpretInvasiveControl(Unit unit) {
		if (unit.getInterpretations().getEffectiveTags() == null) return;
		for (Tag t : unit.getInterpretations().getEffectiveTags()) {
			InvasiveControl c = Annotation.toInvasiveControl(t);
			if (c != null) {
				unit.getInterpretations().setInvasiveControlEffectiveness(c);
				return;
			}
		}
	}

	private boolean annotatedNonWild(Unit unit) {
		return effectiveTagsContains(unit, Tag.ADMIN_MARKED_NON_WILD);
	}

	private boolean annotatedToCoarse(Unit unit) {
		return effectiveTagsContains(unit, Tag.ADMIN_MARKED_COARSE);
	}

	private boolean annotatedAsSpam(Unit unit) {
		return effectiveTagsContains(unit, Tag.ADMIN_MARKED_SPAM);
	}

	private boolean effectiveTagsContains(Unit unit, Tag tag) {
		if (unit.getInterpretations().getEffectiveTags() == null) return false;
		return unit.getInterpretations().getEffectiveTags().contains(tag);
	}

	private void fillNamedPlaceInfo(Gathering gathering, Document document) {
		if (notGiven(document.getNamedPlaceId())) return;

		NamedPlaceEntity namedPlace = dao.getNamedPlaces().get(Qname.fromURI(document.getNamedPlaceId()));
		if (namedPlace == null) return;

		if (notGiven(gathering.getMunicipality())) gathering.setMunicipality(namedPlace.getMunicipalityDisplayName());
		if (notGiven(gathering.getLocality())) gathering.setLocality(namedPlace.getName());
		if (hasCoordinatesOrGeo(gathering)) return;
		if (namedPlace.getYkj10km() != null) {
			try {
				gathering.setCoordinates(new Coordinates(namedPlace.getYkj10km()));
			} catch (DataValidationException e) {
				throw new ETLException("Impossible state", e);
			}
			return;
		}
		if (namedPlace.getWgs84WKT() != null) {
			try {
				gathering.setGeo(Geo.fromWKT(namedPlace.getWgs84WKT(), Type.WGS84));
			} catch (DataValidationException e) {
				throw new ETLException("Impossible state", e);
			}
		}
	}

	private boolean hasCoordinatesOrGeo(Gathering gathering) {
		return gathering.getCoordinates() != null || gathering.getGeo() != null;
	}

	private void validateTimes(Gathering gathering) {
		if (gathering.getQuality() != null && gathering.getQuality().getTimeIssue() != null) return; // do not override existing date issue
		if (gathering.getEventDate() == null) {
			if (atLeastOneGiven(gathering.getHourBegin(), gathering.getHourEnd(), gathering.getMinutesBegin(), gathering.getMinutesEnd())) {
				gathering.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, "Hours/minutes given without a date"));
			}
			return;
		}
		if (gathering.getMinutesBegin() != null && gathering.getHourBegin() == null) {
			gathering.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_MINUTE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, "Minutes given without hour"));
			return;
		}
		if (gathering.getMinutesEnd() != null && gathering.getHourEnd() == null) {
			gathering.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_MINUTE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, "Minutes given without hour"));
			return;
		}
	}

	private boolean atLeastOneGiven(Integer ...integers) {
		for (Integer i : integers) {
			if (i != null) return true;
		}
		return false;
	}

	private boolean noTarget(Unit u) {
		return notGiven(u.getTaxonVerbatim()) && notGiven(u.getReportedTaxonId()) && notGiven(u.getReportedInformalTaxonGroup());
	}

	private void interpretTeam(Gathering gathering) {
		ArrayList<String> team = new ArrayList<>();
		List<Qname> observerUserIds = new ArrayList<>();
		for (String teamMember : gathering.getTeam()) {
			if (teamMember.startsWith("MA.")) {
				Qname qname = new Qname(teamMember);
				team.add(getPersonFullName(qname));
				observerUserIds.add(qname);
			} else if (teamMember.startsWith("http://")) {
				try {
					Qname qname = Qname.fromURI(teamMember);
					team.add(getPersonFullName(qname));
					observerUserIds.add(qname);
				} catch (IllegalArgumentException e) {
					// invalid uri
				}
			} else {
				teamMember = teamMember.replace(".", ". ");
				while (teamMember.contains("  ")) {
					teamMember = teamMember.replace("  ", " ");
				}
				teamMember = teamMember.trim();
				team.add(teamMember);
			}
		}
		gathering.getTeam().clear();
		for (String name : team) {
			gathering.addTeamMember(name);
		}
		for (Qname userId : observerUserIds) {
			if (!gathering.getObserverUserIds().contains(userId.toURI())) {
				gathering.addObserverUserId(userId.toURI());
			}
		}
	}

	private String getPersonFullName(Qname personId) {
		return dao.getPerson(personId).getFullName();
	}

	private void interpretUnit(Unit unit, Gathering gathering, Document document) {
		UnitInterpretations interpretations = unit.createInterpretations();

		if (notGiven(unit.getRecordBasis())) {
			unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		}

		if ("x".equalsIgnoreCase(unit.getAbundanceString())) {
			unit.setAbundanceUnit(AbundanceUnit.OCCURS_DOES_NOT_OCCUR);
		}
		interpretations.setIndividualCount(interpretIndividualCount(unit, gathering));
		interpretPairCountFromFacts(unit, interpretations);
		interpretUnitKeywordsFromFacts(unit);

		if (looksLikeDeadCode(unit.getAbundanceString()) && unit.isAlive() == null) {
			unit.setAlive(false);
		}

		qualityInterpreter.interpret(unit, gathering, document);

		interpretBreedingSiteAndAtlasCode(unit, document.getCollectionId(), formId(document));
		interpretLocalFromAtlasCode(unit);

		interpretWild(unit, gathering); // must do this after annotations so annotated taxon ids are used
	}

	private Qname formId(Document document) {
		Qname formId = document.getFormId() != null ? Qname.fromURI(document.getFormId()) : null;
		return formId;
	}

	private void interpretBreedingSiteAndAtlasCode(Unit unit, Qname collectionId, Qname formId) {
		if (BREEDING_SITE_LIFESTAGES.contains(unit.getLifeStage())) {
			unit.setBreedingSite(true);
		}

		if (!given(unit.getAtlasCode())) {
			if (looksLikeAtlasCode(unit.getNotes())) {
				unit.setAtlasCodeUsingQname(parseAtlasCode(unit.getNotes()));
			} else if (looksLikeAtlasCode(unit.getAbundanceString())) {
				unit.setAtlasCodeUsingQname(parseAtlasCode(unit.getAbundanceString()));
			} else if (pointCount(collectionId)) {
				unit.setAtlasCodeUsingQname(parsePointCountAtlasCode(unit));
			} else if (lineTransectCount(collectionId)) {
				unit.setAtlasCodeUsingQname(parseLineTransectCountAtlasCode(unit));
			} else if (waterbirdCount(collectionId)) {
				unit.setAtlasCodeUsingQname(parseWaterbirdCountAtlasCode(unit, formId));
			}
		}

		validateAtlasCode(unit);

		if (given(unit.getAtlasCode())) {
			unit.setAtlasClassUsingQname(toAtlasClass(unit.getAtlasCode()));
			if (BREEDING_SITE_ATLAS_CLASSES.contains(unit.getAtlasClass())) {
				unit.setBreedingSite(true);
			}
		}
	}

	private void interpretLocalFromAtlasCode(Unit unit) {
		if (given(unit.getAtlasCode()) && !unit.getAtlasCode().equals(new Qname("MY.atlasCodeEnum1").toURI())) {
			unit.setLocal(true);
		}
	}

	private boolean waterbirdCount(Qname collectionId) {
		return Const.WATERBIRD_COLLECTION.contains(collectionId);
	}

	private boolean lineTransectCount(Qname collectionId) {
		return Const.BIRD_LINE_TRANSECT_COLLECTION.contains(collectionId);
	}

	private boolean pointCount(Qname collectionId) {
		return Const.BIRD_POINT_COUNT_COLLECTION.contains(collectionId);
	}

	private Qname parseWaterbirdCountAtlasCode(Unit unit, Qname formId) {
		if (formId == null) return null;
		if (formId.equals(Const.WATERBIRD_PAIRCOUNT_FORM_ID)) {
			// parilaskenta
			String shortHand = unit.factValue(SHORTHAND_FACT);
			if (given(shortHand) && shortHand.toLowerCase().contains("k") && shortHand.toLowerCase().contains("n")) return atlasCode(3);
			Integer pairCount = unit.factIntValue(PAIRCOUNT_FACT);
			if (pairCount != null && pairCount > 0) return atlasCode(2);
			return atlasCode(1);
		}
		// poikuelaskenta
		Integer juvCount = unit.factIntValue(JUVENILE_IND_COUNT_FACT);
		if (juvCount != null && juvCount > 0) return atlasCode(73);
		String female = unit.factValue(WATERBIRD_FEMALE_FACT);
		if (WATERBIRD_FEMALE_PANIC_FACT.equals(female)) return atlasCode(63);
		if (WATERBIRD_FEMALE_YES_FACT.equals(female)) return atlasCode(2);
		Integer count = unit.factIntValue(COUNT_FACT);
		if (count != null && count > 0) return atlasCode(2);
		return null;
	}

	private Qname parsePointCountAtlasCode(Unit unit) {
		if (zeroPairCount(unit)) return null;
		BirdClass birdClass = getBirdClass(unit);
		if (birdClass == BirdClass.NOT_BIRD) return null;
		if (birdClass == BirdClass.EARLY_BREEDER) return atlasCode(1);
		return atlasCode(2);
	}

	private Qname parseLineTransectCountAtlasCode(Unit unit) {
		BirdClass birdClass = getBirdClass(unit);
		if (birdClass == BirdClass.NOT_BIRD) return null;

		Integer broodSize = unit.factIntValue(BROOD_SIZE_FACT);
		String obsType = unit.factValue(OBS_TYPE_FACT);

		if (broodSize != null && broodSize > 0) return atlasCode(82);
		if ("true".equals(unit.factValue(LIKELY_MIGRANT_FACT))) return atlasCode(1);
		if (zeroPairCount(unit) && "0".equals(unit.getAbundanceString())) return atlasCode(1);
		if (OBS_TYPE_SEEN_NEST.equals(obsType)) return atlasCode(8);
		if (birdClass == BirdClass.EARLY_BREEDER) return atlasCode(1);
		if (OBS_TYPE_PAIR.equals(obsType)) return atlasCode(3);
		if (OBS_TYPE_BROOD.equals(obsType)) return atlasCode(73);
		return atlasCode(2);
	}

	private static final Set<Qname> EARLY_BREEDERS = Utils.set(
			new Qname("MX.36358"), // Loxia curvirostra
			new Qname("MX.36356"), // Loxia pytyopsittacus
			new Qname("MX.36359"), // Loxia leucoptera
			new Qname("MX.36817")); // Sturnus vulgaris

	private static enum BirdClass { NORMAL, EARLY_BREEDER, NOT_BIRD }

	private BirdClass getBirdClass(Unit unit) {
		Qname taxonId = dao.getTaxonLinkingService().getTaxonId(unit.getTaxonVerbatim());
		if (taxonId == null) return BirdClass.NOT_BIRD;
		if (!dao.hasTaxon(taxonId)) return BirdClass.NOT_BIRD;
		if (EARLY_BREEDERS.contains(taxonId)) return BirdClass.EARLY_BREEDER;
		Taxon taxon = dao.getTaxon(taxonId);
		Taxon classTaxon = taxon.getParentOfRank(TAXON_RANK_CLASS);
		if (classTaxon == null) return BirdClass.NOT_BIRD;
		if (AVES.equals(classTaxon.getId())) return BirdClass.NORMAL;
		return BirdClass.NOT_BIRD;
	}

	private boolean zeroPairCount(Unit unit) {
		if (unit.getInterpretations() == null) return false;
		return Integer.valueOf(0).equals(unit.getInterpretations().getPairCount());
	}

	private void validateAtlasCode(Unit unit) {
		String code = unit.getAtlasCode();
		if (!given(code)) return;
		if (isValidAtlasCode(code)) return;
		code = code.substring(0, code.length()-1);
		if (isValidAtlasCode(code)) {
			unit.setAtlasCodeUsingQname(Qname.fromURI(code));
		} else {
			unit.setAtlasCodeUsingQname(null);
		}
	}

	private boolean isValidAtlasCode(String code) {
		Qname qname = null;
		try {
			qname = Qname.fromURI(code);
		} catch (Exception e) {
			return false;
		}
		return dao.isValidEnumeration(qname);
	}

	private void interpretWild(Unit unit, Gathering gathering) {
		Boolean isWildByCodes = isWild(null, unit);
		if (isWildByCodes != null) {
			unit.setWild(isWildByCodes);
			return;
		}
		Qname taxonId = dao.getTaxonLinkingService().getTaxonId(unit, gathering, true);
		unit.setWild(isWild(taxonId, unit));
	}

	public Boolean isWild(Qname taxonId, Unit unit) {
		if (unit.isWild() != null) return unit.isWild();
		if (NON_WILD_PLANT_STATUS_CODES.contains(unit.getPlantStatusCode())) {
			return false;
		}
		Taxon taxon = getTaxon(taxonId);
		if (taxon == null) return null;
		if (taxon.isAutoNonWild()) return false;
		return null;
	}

	private Taxon getTaxon(Qname taxonId) {
		if (taxonId == null) return null;
		if (!dao.hasTaxon(taxonId)) return null;
		return dao.getTaxon(taxonId);
	}

	private boolean looksLikeAtlasCode(String s) {
		return given(s) && s.toUpperCase().startsWith(IndividualCountInterpreter.ATLAS_CODE);
	}

	private boolean looksLikeDeadCode(String s) {
		return given(s) && s.toUpperCase().contains(IndividualCountInterpreter.DEAD_CODE);
	}

	private Qname parseAtlasCode(String s) {
		s = s.toUpperCase();
		s = s.replace(IndividualCountInterpreter.ATLAS_CODE, "");
		try {
			int code = Integer.valueOf(s);
			return atlasCode(code);
		} catch (Exception e) {
			return null;
		}
	}

	private Qname atlasCode(int code) {
		return Qname.fromURI(ATLAS_CODE_PREFIX+code);
	}

	private static final String ATLAS_CODE_PREFIX = new Qname("MY.atlasCodeEnum").toURI();
	private static final Map<String, Qname> ATLAS_CLASS;
	static {
		ATLAS_CLASS =  new HashMap<>();
		ATLAS_CLASS.put("1", new Qname("MY.atlasClassEnumA"));
		ATLAS_CLASS.put("2", new Qname("MY.atlasClassEnumB"));
		ATLAS_CLASS.put("3", new Qname("MY.atlasClassEnumB"));
		ATLAS_CLASS.put("4", new Qname("MY.atlasClassEnumC"));
		ATLAS_CLASS.put("5", new Qname("MY.atlasClassEnumC"));
		ATLAS_CLASS.put("6", new Qname("MY.atlasClassEnumC"));
		ATLAS_CLASS.put("7", new Qname("MY.atlasClassEnumD"));
		ATLAS_CLASS.put("8", new Qname("MY.atlasClassEnumD"));
	}

	private Qname toAtlasClass(String atlasCode) {
		if (!given(atlasCode)) return null;
		String code = atlasCode.replace(ATLAS_CODE_PREFIX, "");
		if (code.isEmpty()) return null;
		code = "" + code.charAt(0);
		return ATLAS_CLASS.get(code);
	}

	private static final String LARVA_INDV_COUNT = new Qname("MY.larvaIndividualCount").toURI();

	private int interpretIndividualCount(Unit unit, Gathering gathering) {
		if (given(unit.getAbundanceString())) {
			int interpretedAbundanceString = IndividualCountInterpreter.interpret(unit.getAbundanceString());

			if (interpretedAbundanceString == 0) return 0;

			if (nonIndividualCountAbundanceUnit(unit.getAbundanceUnit())) return 1;
			return interpretedAbundanceString;
		}

		int sum = indVal(unit.getIndividualCountFemale()) + indVal(unit.getIndividualCountMale()) + indVal(unit, LARVA_INDV_COUNT);
		if (sum > 0) return sum;

		if (acknowledgeNoUnitsInCensus(gathering)) {
			return 0;
		}

		return 1;
	}

	private int indVal(Unit unit, String factName) {
		Integer v = unit.factIntValue(factName);
		if (v == null) return 0;
		return v;
	}

	private int indVal(Integer individualCount) {
		if (individualCount == null) return 0;
		return individualCount;
	}

	private static final String NO_UNITS = new Qname("MY.acknowledgeNoUnitsInCensus").toURI();

	private boolean acknowledgeNoUnitsInCensus(Gathering gathering) {
		return "true".equals(gathering.factValue(NO_UNITS));
	}

	private static Set<AbundanceUnit> NON_INDIVIDUAL_COUNT_ABUNDANCE_UNITS = Utils.set(
			AbundanceUnit.PAIRCOUNT, AbundanceUnit.NESTS, AbundanceUnit.BREEDING_SITES, AbundanceUnit.FEEDING_SITES, AbundanceUnit.COLONIES,
			AbundanceUnit.DROPPINGS, AbundanceUnit.FEEDING_MARKS, AbundanceUnit.INDIRECT_MARKS,
			AbundanceUnit.SQUARE_DM, AbundanceUnit.SQUARE_M, AbundanceUnit.RELATIVE_DENSITY
			);

	private boolean nonIndividualCountAbundanceUnit(AbundanceUnit abundanceUnit) {
		return NON_INDIVIDUAL_COUNT_ABUNDANCE_UNITS.contains(abundanceUnit);
	}

	private void interpretPairCountFromFacts(Unit unit, UnitInterpretations interpretations) {
		for (Fact f : unit.getFacts()) {
			if (!isPairCountFact(f)) continue;
			Integer i = f.getIntegerValue();
			if (i == null) continue;
			setOrIncreasePairCount(interpretations, i);
		}
		if (AbundanceUnit.PAIRCOUNT == unit.getAbundanceUnit()) {
			try {
				Integer i = Integer.valueOf(unit.getAbundanceString());
				setOrIncreasePairCount(interpretations, i);
			} catch (Exception e) {}
		}
	}

	private void setOrIncreasePairCount(UnitInterpretations interpretations, Integer i) {
		if (interpretations.getPairCount() == null) {
			interpretations.setPairCount(i);
		} else {
			interpretations.setPairCount(interpretations.getPairCount() + i);
		}
	}

	private boolean isPairCountFact(Fact f) {
		return PAIR_COUNT_FACTS.contains(f.getFact());
	}

	private void interpretUnitKeywordsFromFacts(Unit unit) {
		for (Fact fact : unit.getFacts()) {
			if (UNIT_KEYWORD_FACTS.contains(fact.getFact())) {
				unit.addKeyword(fact.getValue());
			}
		}
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private boolean notGiven(String s) {
		return !given(s);
	}

	private boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	private boolean notGiven(Qname qname) {
		return !given(qname);
	}

	private boolean given(Enum<?> e) {
		return e != null;
	}

	private boolean notGiven(Enum<?> e) {
		return !given(e);
	}

	private boolean given(Collection<?> c) {
		return c != null && !c.isEmpty();
	}

	private boolean notGiven(Collection<?> c) {
		return !given(c);
	}



}
