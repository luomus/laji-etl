package fi.laji.datawarehouse.etl.models.dw.geo;

import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.json.JSONObject;

public interface Feature extends Iterable<Point> {

	Double getLatMin();

	Double getLatMax();

	Double getLonMin();

	Double getLonMax();

	JSONObject getGeoJSON();

	String getWKT();

	Feature validate() throws DataValidationException;

	int size();

}
