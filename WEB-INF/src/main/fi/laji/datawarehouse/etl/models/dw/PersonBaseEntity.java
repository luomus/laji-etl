package fi.laji.datawarehouse.etl.models.dw;

import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

@MappedSuperclass
public class PersonBaseEntity  {

	private Long key;
	private Qname id;
	private String name;
	private String email;
	private Integer lintuvaaraId;
	private Integer omaRiistaId;

	public PersonBaseEntity() {}

	public PersonBaseEntity(Qname id) {
		this.id = id;
		setKey(parsePersonKey(id));
	}

	public static Long parsePersonKey(Qname id) {
		if (id == null) return null;
		try {
			String[] parts = id.toString().split(Pattern.quote("MA."));
			return Long.valueOf(parts[1]);
		} catch (Exception e) {
			return null;
		}
	}

	@Id
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getId() {
		if (id == null) {
			return null;
		}
		return id.toURI();

	}

	public void setId(String id) {
		if (id == null) {
			this.id = null;
		} else {
			this.id = Qname.fromURI(id);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = Util.trimToByteLength(name, 1000);
	}

	@Column(name="lintuvaara_id")
	public Integer getLintuvaaraId() {
		return lintuvaaraId;
	}

	public void setLintuvaaraId(Integer lintuvaaraId) {
		this.lintuvaaraId = lintuvaaraId;
	}

	@Column(name="omariista_id")
	public Integer getOmaRiistaId() {
		return omaRiistaId;
	}

	public void setOmaRiistaId(Integer omaRiistaId) {
		this.omaRiistaId = omaRiistaId;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
