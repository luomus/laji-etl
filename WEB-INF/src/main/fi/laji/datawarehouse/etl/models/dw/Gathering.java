package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

@Embeddable
public class Gathering extends BaseModel {

	private Qname gatheringId;
	private List<Unit> units = new ArrayList<>();
	private List<String> team = new ArrayList<>();
	private List<String> observerUserIds = new ArrayList<>();
	private List<MediaObject> media = new ArrayList<>();
	private List<TaxonCensus> taxonCensus = new ArrayList<>();
	private String higherGeography;
	private String country;
	private String province;
	private String biogeographicalProvince;
	private String municipality;
	private String locality;
	private Coordinates coordinates;
	private Geo geo;
	private Boolean accurateArea;
	private DateRange eventDate;
	private Integer hourBegin;
	private Integer hourEnd;
	private Integer minutesBegin;
	private Integer minutesEnd;
	private GatheringInterpretations interpretations;
	private GatheringConversions conversions;
	private GatheringQuality quality;
	private String coordinatesVerbatim;
	private Integer section;
	private Boolean stateLand;
	private GatheringDWLinkings linkings;
	private int unitOrderSeq = 0;
	private int gatheringOrder;

	public Gathering(Qname gatheringId) throws CriticalParseFailure {
		Util.validateId(gatheringId, "gatheringId");
		this.gatheringId = gatheringId;
	}

	@Deprecated
	public Gathering() {
		// for hibernate
	}

	@Column(name="highergeography_verbatim")
	@FileDownloadField(name="HigherGeographyVerbatim", order=51)
	public String getHigherGeography() {
		return higherGeography;
	}

	public void setHigherGeography(String higherGeography) {
		this.higherGeography = Util.trimToByteLength(higherGeography, 1000);
	}

	@Column(name="country_verbatim")
	@FileDownloadField(name="CountryVerbatim", order=52)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = Util.trimToByteLength(country, 1000);
	}

	@Column(name="municipality_verbatim")
	@FileDownloadField(name="MunicipalityVerbatim", order=53)
	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = Util.trimToByteLength(municipality, 1000);
	}

	@Column(name="bioprovince_verbatim")
	@FileDownloadField(name="BioProvinceVerbatim", order=54)
	public String getBiogeographicalProvince() {
		return biogeographicalProvince;
	}

	public void setBiogeographicalProvince(String biogeographicalProvince) {
		this.biogeographicalProvince = Util.trimToByteLength(biogeographicalProvince, 1000);
	}

	@Column(name="province_verbatim")
	@FileDownloadField(name="ProvinceVerbatim", order=55)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = Util.trimToByteLength(province, 1000);
	}

	@FileDownloadField(name="LocalityVerbatim", order=56)
	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = Util.trimToByteLength(locality, 1000);
	}

	@Transient
	@FileDownloadField(name="TeamMembers", order=9)
	public List<String> getTeam() {
		return team;
	}

	// use addTeamMeber
	public void setTeam(ArrayList<String> teamMembers) {
		this.team = teamMembers;
	}

	public void addTeamMember(String teamMember) {
		if (teamMember == null) return;
		String cleanedName = cleanTeamText(teamMember);
		String[] names = cleanedName.split(Pattern.quote(";"));
		for (String name : names) {
			if (name == null) continue;
			name = name.trim();
			if (!given(name)) continue;
			name = Util.trimToByteLength(name, 200);
			if (!this.team.contains(name)) {
				this.team.add(name);
			}
		}
	}

	private String cleanTeamText(String agent) {
		agent = agent.trim();
		agent = agent.replace("\r", ";").replace("\n", ";");
		while (agent.contains(";;")) {
			agent = agent.replace(";;", ";");
		}
		if (agent.startsWith(";") || agent.startsWith(",")) {
			agent = agent.substring(1).trim();
		}
		if (agent.endsWith(";") || agent.endsWith(",")) {
			agent = agent.substring(0, agent.length() -1).trim();
		}
		return agent.trim();
	}

	public Gathering addUnit(Unit unit) {
		this.units.add(unit);
		unit.setUnitOrder(unitOrderSeq++);
		return this;
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public List<Unit> getUnits() {
		return units;
	}

	public void setUnits(ArrayList<Unit> units) {
		this.unitOrderSeq = 0;
		for (Unit u : units) {
			u.setUnitOrder(unitOrderSeq++);
		}
		this.units = units;
	}

	@Transient
	@FieldDefinition(order=1.11, ignoreForQuery=true)
	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		if (geo != null && coordinates != null) throw new IllegalStateException("Should not have both coordinates and geography!");
		this.coordinates = coordinates;
	}

	@Column(name="coordinates_verbatim")
	public String getCoordinatesVerbatim() {
		return coordinatesVerbatim;
	}

	public void setCoordinatesVerbatim(String coordinatesVerbatim) {
		this.coordinatesVerbatim = Util.trimToByteLength(coordinatesVerbatim, 65000);
	}

	@Transient
	@FileDownloadField(name="Date", order=2.0)
	@StatisticsQueryAlllowedField(allowFor=Base.GATHERING)
	public DateRange getEventDate() {
		return eventDate;
	}

	public void setEventDate(DateRange eventDate) {
		this.eventDate = eventDate;
	}

	public void setInterpretations(GatheringInterpretations interpretations) {
		this.interpretations = interpretations;
	}

	@Embedded
	@FileDownloadField(name="Interpretations", order=11)
	@FieldDefinition(ignoreForETL=true)
	public GatheringInterpretations getInterpretations() {
		return interpretations;
	}

	public GatheringInterpretations createInterpretations() {
		if (interpretations == null) {
			interpretations = new GatheringInterpretations();
		}
		return interpretations;
	}

	public GatheringConversions createConversions() {
		if (this.conversions == null) {
			this.conversions = new GatheringConversions();
		}
		return this.conversions;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="wgs84.latMin", column=@Column(name="latitudemin")),
		@AttributeOverride(name="wgs84.latMax", column=@Column(name="latitudemax")),
		@AttributeOverride(name="wgs84.lonMin", column=@Column(name="longitudemin")),
		@AttributeOverride(name="wgs84.lonMax", column=@Column(name="longitudemax")),
		@AttributeOverride(name="ykj.latMin", column=@Column(name="ykjnmin")),
		@AttributeOverride(name="ykj.latMax", column=@Column(name="ykjnmax")),
		@AttributeOverride(name="ykj.lonMin", column=@Column(name="ykjemin")),
		@AttributeOverride(name="ykj.lonMax", column=@Column(name="ykjemax")),
		@AttributeOverride(name="euref.latMin", column=@Column(name="eurefnmin")),
		@AttributeOverride(name="euref.latMax", column=@Column(name="eurefnmax")),
		@AttributeOverride(name="euref.lonMin", column=@Column(name="eurefemin")),
		@AttributeOverride(name="euref.lonMax", column=@Column(name="eurefemax")),
		@AttributeOverride(name="wgs84CenterPoint.lat", column=@Column(name="latitudecenter")),
		@AttributeOverride(name="wgs84CenterPoint.lon", column=@Column(name="longitudecenter")),
		@AttributeOverride(name="eurefCenterPoint.lat", column=@Column(name="eurefncenter")),
		@AttributeOverride(name="eurefCenterPoint.lon", column=@Column(name="eurefecenter")),
		@AttributeOverride(name="wgs84Grid1.lat", column=@Column(name="latitude1")),
		@AttributeOverride(name="wgs84Grid1.lon", column=@Column(name="longitude1")),
		@AttributeOverride(name="wgs84Grid05.lat", column=@Column(name="latitude05")),
		@AttributeOverride(name="wgs84Grid05.lon", column=@Column(name="longitude05")),
		@AttributeOverride(name="wgs84Grid01.lat", column=@Column(name="latitude01")),
		@AttributeOverride(name="wgs84Grid01.lon", column=@Column(name="longitude01")),
		@AttributeOverride(name="wgs84Grid005.lat", column=@Column(name="latitude005")),
		@AttributeOverride(name="wgs84Grid005.lon", column=@Column(name="longitude005")),
		@AttributeOverride(name="ykj100km.lat", column=@Column(name="ykj_100km_n")),
		@AttributeOverride(name="ykj100km.lon", column=@Column(name="ykj_100km_e")),
		@AttributeOverride(name="ykj50km.lat", column=@Column(name="ykj_50km_n")),
		@AttributeOverride(name="ykj50km.lon", column=@Column(name="ykj_50km_e")),
		@AttributeOverride(name="ykj10km.lat", column=@Column(name="ykj_10km_n")),
		@AttributeOverride(name="ykj10km.lon", column=@Column(name="ykj_10km_e")),
		@AttributeOverride(name="ykj1km.lat", column=@Column(name="ykj_1km_n")),
		@AttributeOverride(name="ykj1km.lon", column=@Column(name="ykj_1km_e")),
		@AttributeOverride(name="ykj100kmCenter.lat", column=@Column(name="ykj_100km_n_center")),
		@AttributeOverride(name="ykj100kmCenter.lon", column=@Column(name="ykj_100km_e_center")),
		@AttributeOverride(name="ykj50kmCenter.lat", column=@Column(name="ykj_50km_n_center")),
		@AttributeOverride(name="ykj50kmCenter.lon", column=@Column(name="ykj_50km_e_center")),
		@AttributeOverride(name="ykj10kmCenter.lat", column=@Column(name="ykj_10km_n_center")),
		@AttributeOverride(name="ykj10kmCenter.lon", column=@Column(name="ykj_10km_e_center")),
		@AttributeOverride(name="ykj1kmCenter.lat", column=@Column(name="ykj_1km_n_center")),
		@AttributeOverride(name="ykj1kmCenter.lon", column=@Column(name="ykj_1km_e_center")),
		@AttributeOverride(name="boundingBoxAreaInSquareMeters", column=@Column(name="bbox_area")),
		@AttributeOverride(name="linelengthInMeters", column=@Column(name="linelength")),
		@AttributeOverride(name="eurefWKT", column=@Column(name="euref_wkt"))
	})
	@FileDownloadField(name="Conversions", order=10)
	@FieldDefinition(ignoreForETL=true)
	public GatheringConversions getConversions() {
		return conversions;
	}

	public void setConversions(GatheringConversions conversions) {
		this.conversions = conversions;
	}

	@FileDownloadField(name="HourBegin", order=2.1)
	public Integer getHourBegin() {
		return hourBegin;
	}

	public void setHourBegin(Integer hourBegin) throws DateValidationException {
		Util.validateHour(hourBegin, "hourBegin");
		this.hourBegin = hourBegin;
	}

	@FileDownloadField(name="HourEnd", order=2.2)
	public Integer getHourEnd() {
		return hourEnd;
	}

	public void setHourEnd(Integer hourEnd) throws DateValidationException {
		Util.validateHour(hourEnd, "hourEnd");
		this.hourEnd = hourEnd;
	}

	public Integer getMinutesBegin() {
		return minutesBegin;
	}

	public void setMinutesBegin(Integer minutesBegin) throws DateValidationException {
		Util.validateMinutes(minutesBegin, "minutesBegin");
		this.minutesBegin = minutesBegin;
	}

	public Integer getMinutesEnd() {
		return minutesEnd;
	}

	public void setMinutesEnd(Integer minutesEnd) throws DateValidationException {
		Util.validateMinutes(minutesEnd, "minutesEnd");
		this.minutesEnd = minutesEnd;
	}

	@FileDownloadField(name="DisplayDateTime", order=4)
	@FieldDefinition(ignoreForETL=true)
	public String getDisplayDateTime() {
		return Util.getDisplayDateTime(eventDate, hourBegin, minutesBegin, hourEnd, minutesEnd);
	}

	@Deprecated
	public void setDisplayDateTime(@SuppressWarnings("unused") String displayDateTime) {
		// for hibernate
	}

	public Gathering concealPublicData(SecureLevel secureLevel, Set<SecureReason> secureReasons, Qname sourceId) {
		if (secureLevel == SecureLevel.NONE) throw new IllegalArgumentException("No point concealing with secure level " + secureLevel);
		Gathering publicGathering;
		try {
			publicGathering = new Gathering(this.getGatheringId());
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException();
		}

		publicGathering.setHigherGeography(this.getHigherGeography());
		publicGathering.setCountry(this.getCountry());

		if (this.getInterpretations() != null) {
			GatheringInterpretations interpretations = this.getInterpretations();
			GatheringInterpretations publicInterpretations = publicGathering.createInterpretations();

			if (interpretations.getCoordinates() != null) {
				Coordinates concealedCoordinates = concealCoordinates(secureLevel);
				if (concealedCoordinates != null) {
					publicInterpretations.setCoordinates(concealedCoordinates);
					publicInterpretations.setSourceOfCoordinates(interpretations.getSourceOfCoordinates());
				}
			}

			if (secureLevel == SecureLevel.KM1 || secureLevel == SecureLevel.KM5) {
				publicInterpretations.setFinnishMunicipality(interpretations.getFinnishMunicipality());
				publicInterpretations.setFinnishMunicipalities((ArrayList<Qname>) interpretations.getFinnishMunicipalities());
				publicInterpretations.setSourceOfFinnishMunicipality(interpretations.getSourceOfFinnishMunicipality());
			}
			if (secureLevel == SecureLevel.KM1  || secureLevel == SecureLevel.KM5 || secureLevel == SecureLevel.KM10) {
				publicInterpretations.setBiogeographicalProvince(interpretations.getBiogeographicalProvince());
				publicInterpretations.setBiogeographicalProvinces((ArrayList<Qname>) interpretations.getBiogeographicalProvinces());
				publicInterpretations.setSourceOfBiogeographicalProvince(interpretations.getSourceOfBiogeographicalProvince());
			}
			publicInterpretations.setCountry(interpretations.getCountry());
			publicInterpretations.setSourceOfCountry(interpretations.getSourceOfCountry());
		}

		if (secureLevel == SecureLevel.KM1 || secureLevel == SecureLevel.KM5) {
			publicGathering.setMunicipality(this.getMunicipality());
		}

		if (secureLevel == SecureLevel.KM1  || secureLevel == SecureLevel.KM5 || secureLevel == SecureLevel.KM10) {
			publicGathering.setBiogeographicalProvince(this.getBiogeographicalProvince());
			publicGathering.setProvince(this.getProvince());
		}

		if (this.getEventDate() != null) {
			publicGathering.setEventDate(this.getEventDate().conceal(secureLevel));
		}

		if (this.getQuality() != null) {
			publicGathering.setQuality(this.getQuality().conseal());
		}

		for (Unit privateUnit : this.getUnits()) {
			Unit publicUnit = privateUnit.concealPublicData(secureLevel, secureReasons, sourceId);
			publicGathering.addUnit(publicUnit);
		}

		return publicGathering;
	}

	private Coordinates concealCoordinates(SecureLevel secureLevel) {
		if (Const.FINLAND.equals(interpretations.getCountry())) {
			return concealAsYKJ(secureLevel);
		}
		return concealAsWGS84(secureLevel);
	}

	private Coordinates concealAsYKJ(SecureLevel secureLevel) {
		if (interpretations.getCoordinates().getType() == Type.YKJ) {
			return interpretations.getCoordinates().conceal(secureLevel, Type.YKJ);
		}
		try {
			return CoordinateConverter.convert(interpretations.getCoordinates()).getYkj().conceal(secureLevel, interpretations.getCoordinates().getType());
		} catch (Exception e) {
			return concealAsWGS84(secureLevel);
		}
	}

	private Coordinates concealAsWGS84(SecureLevel secureLevel) {
		if (interpretations.getCoordinates().getType() == Type.WGS84) {
			return interpretations.getCoordinates().conceal(secureLevel, Type.WGS84);
		}
		try {
			return CoordinateConverter.convert(interpretations.getCoordinates()).getWgs84().conceal(secureLevel, Type.WGS84);
		} catch (DataValidationException e) {
			throw new ETLException("Coordinate concealment exception", e);
		}
	}

	public Gathering copy() {
		try {
			JSONObject gatheringJSON = ModelToJson.toJson(this);
			Gathering copy = JsonToModel.gatheringFromJson(gatheringJSON);
			return copy;
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException();
		}
	}

	@Transient
	@FileDownloadField(name="GatheringID", order=-1)
	public Qname getGatheringId() {
		return gatheringId;
	}

	@Transient
	public List<MediaObject> getMedia() {
		return media;
	}

	public void addMedia(MediaObject media) {
		this.media.add(media);
	}

	public void setMedia(ArrayList<MediaObject> media) {
		this.media = media;
	}

	@Column(name="gathering_mediacount")
	@FileDownloadField(name="MediaCount")
	@FieldDefinition(ignoreForETL=true)
	public Integer getMediaCount() {
		return media.size();
	}

	@Deprecated
	public void setMediaCount(@SuppressWarnings("unused") Integer mediaCount) { // for hibernate

	}

	public void setGatheringId(Qname gatheringId) throws CriticalParseFailure {
		Util.validateId(gatheringId, "gatheringId");
		this.gatheringId = gatheringId;
	}

	@Transient
	@FieldDefinition(ignoreForETL=true)
	public GatheringDWLinkings getLinkings() {
		return linkings;
	}

	public void setLinkings(GatheringDWLinkings linkings) {
		this.linkings = linkings;
	}

	public GatheringDWLinkings createLinkings() {
		if (linkings == null) {
			linkings = new GatheringDWLinkings();
		}
		return linkings;
	}

	@Transient
	@FieldDefinition(order=1.11)
	public Geo getGeo() {
		return geo;
	}

	public void setGeo(Geo geo) {
		if (geo != null && coordinates != null) throw new IllegalStateException("Should not have both coordinates and geography!");
		this.geo = geo;
	}

	public static Gathering emptyGathering() {
		return new Gathering();
	}

	@Embedded
	@FileDownloadField(name="Quality", order=0)
	public GatheringQuality getQuality() {
		return quality;
	}

	public void setQuality(GatheringQuality quality) {
		this.quality = quality;
	}

	public GatheringQuality createQuality() {
		if (quality == null) {
			quality = new GatheringQuality();
		}
		return quality;
	}

	@Transient
	public List<String> getObserverUserIds() {
		return observerUserIds;
	}

	public void addObserverUserId(String userId) {
		if (userId == null || userId.trim().isEmpty()) return;
		if (userId.startsWith("MA.")) {
			userId = new Qname(userId).toURI();
		}
		observerUserIds.add(Util.trimToByteLength(userId, 1000));
	}

	public void setObserverUserIds(ArrayList<String> ownerUserIds) {
		this.observerUserIds = ownerUserIds;
	}

	@Transient
	public List<TaxonCensus> getTaxonCensus() {
		return taxonCensus;
	}

	public void addTaxonCensus(TaxonCensus taxonCensus) {
		this.taxonCensus.add(taxonCensus);
	}

	public void setTaxonCensus(ArrayList<TaxonCensus> taxonCensus) {
		this.taxonCensus = taxonCensus;
	}

	@Column(name="gathering_order")
	@FileDownloadField(name="GatheringOrder", order=-1)
	@FieldDefinition(ignoreForETL=true)
	public int getGatheringOrder() {
		return gatheringOrder;
	}

	public void setGatheringOrder(int gatheringOrder) {
		this.gatheringOrder = gatheringOrder;
	}

	@Column(name="gathering_section")
	@FileDownloadField(name="GatheringSection", order=-1)
	@StatisticsQueryAlllowedField
	public Integer getGatheringSection() {
		return section;
	}

	public void setGatheringSection(Integer gatheringSection) {
		this.section = gatheringSection;
	}

	@Column(name="state_land")
	@FileDownloadField(name="StateLand", order=12.1)
	public Boolean getStateLand() {
		return stateLand;
	}

	public void setStateLand(Boolean stateLand) {
		this.stateLand = stateLand;
	}

	@Column(name="accurate_area")
	@FileDownloadField(name="AccurateArea", order=12.2)
	public Boolean getAccurateArea() {
		return accurateArea;
	}

	public void setAccurateArea(Boolean accurateArea) {
		this.accurateArea = accurateArea;
	}

	// Equals and hashcode: ignore following:
	// conversions, gatheringId, gatheringOrder, interpretations, linkings, unitOrderSeq, units

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((biogeographicalProvince == null) ? 0 : biogeographicalProvince.hashCode());
		result = prime * result + ((coordinates == null) ? 0 : coordinates.hashCode());
		result = prime * result + ((coordinatesVerbatim == null) ? 0 : coordinatesVerbatim.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((eventDate == null) ? 0 : eventDate.hashCode());
		result = prime * result + ((geo == null) ? 0 : geo.hashCode());
		result = prime * result + ((higherGeography == null) ? 0 : higherGeography.hashCode());
		result = prime * result + ((hourBegin == null) ? 0 : hourBegin.hashCode());
		result = prime * result + ((hourEnd == null) ? 0 : hourEnd.hashCode());
		result = prime * result + ((locality == null) ? 0 : locality.hashCode());
		result = prime * result + ((media == null) ? 0 : media.hashCode());
		result = prime * result + ((minutesBegin == null) ? 0 : minutesBegin.hashCode());
		result = prime * result + ((minutesEnd == null) ? 0 : minutesEnd.hashCode());
		result = prime * result + ((municipality == null) ? 0 : municipality.hashCode());
		result = prime * result + ((observerUserIds == null) ? 0 : observerUserIds.hashCode());
		result = prime * result + ((province == null) ? 0 : province.hashCode());
		result = prime * result + ((quality == null) ? 0 : quality.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		result = prime * result + ((taxonCensus == null) ? 0 : taxonCensus.hashCode());
		result = prime * result + ((team == null) ? 0 : team.hashCode());
		result = prime * result + ((stateLand == null) ? 0 : stateLand.hashCode());
		result = prime * result + ((accurateArea == null) ? 0 : accurateArea.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gathering other = (Gathering) obj;
		if (biogeographicalProvince == null) {
			if (other.biogeographicalProvince != null)
				return false;
		} else if (!biogeographicalProvince.equals(other.biogeographicalProvince))
			return false;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		if (coordinatesVerbatim == null) {
			if (other.coordinatesVerbatim != null)
				return false;
		} else if (!coordinatesVerbatim.equals(other.coordinatesVerbatim))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (eventDate == null) {
			if (other.eventDate != null)
				return false;
		} else if (!eventDate.equals(other.eventDate))
			return false;
		if (geo == null) {
			if (other.geo != null)
				return false;
		} else if (!geo.equals(other.geo))
			return false;
		if (higherGeography == null) {
			if (other.higherGeography != null)
				return false;
		} else if (!higherGeography.equals(other.higherGeography))
			return false;
		if (hourBegin == null) {
			if (other.hourBegin != null)
				return false;
		} else if (!hourBegin.equals(other.hourBegin))
			return false;
		if (hourEnd == null) {
			if (other.hourEnd != null)
				return false;
		} else if (!hourEnd.equals(other.hourEnd))
			return false;
		if (locality == null) {
			if (other.locality != null)
				return false;
		} else if (!locality.equals(other.locality))
			return false;
		if (media == null) {
			if (other.media != null)
				return false;
		} else if (!media.equals(other.media))
			return false;
		if (minutesBegin == null) {
			if (other.minutesBegin != null)
				return false;
		} else if (!minutesBegin.equals(other.minutesBegin))
			return false;
		if (minutesEnd == null) {
			if (other.minutesEnd != null)
				return false;
		} else if (!minutesEnd.equals(other.minutesEnd))
			return false;
		if (municipality == null) {
			if (other.municipality != null)
				return false;
		} else if (!municipality.equals(other.municipality))
			return false;
		if (observerUserIds == null) {
			if (other.observerUserIds != null)
				return false;
		} else if (!observerUserIds.equals(other.observerUserIds))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		if (quality == null) {
			if (other.quality != null)
				return false;
		} else if (!quality.equals(other.quality))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		if (taxonCensus == null) {
			if (other.taxonCensus != null)
				return false;
		} else if (!taxonCensus.equals(other.taxonCensus))
			return false;
		if (team == null) {
			if (other.team != null)
				return false;
		} else if (!team.equals(other.team))
			return false;
		if (stateLand == null) {
			if (other.stateLand != null)
				return false;
		} else if (!stateLand.equals(other.stateLand))
			return false;
		if (accurateArea == null) {
			if (other.accurateArea != null)
				return false;
		} else if (!accurateArea.equals(other.accurateArea))
			return false;
		return true;
	}

}
