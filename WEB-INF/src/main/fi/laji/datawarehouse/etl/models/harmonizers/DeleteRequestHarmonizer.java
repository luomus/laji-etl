package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.rdf.Qname;

public class DeleteRequestHarmonizer implements Harmonizer<String> {

	private static final String HTTP = "http://";

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure {
		if (data == null || data.length() < 8) throw new CriticalParseFailure("Invalid delete request");
		data = data.replace("\r", "");
		while (data.contains("  ")) {
			data = data.replace("  ", " ");
		}
		List<DwRoot> roots = new ArrayList<>();
		for (String line : data.split(Pattern.quote("\n"))) {
			line = line.trim();
			if (line.isEmpty()) continue;
			roots.add(parse(line, source));
		}
		return roots;
	}

	private DwRoot parse(String data, Qname source) throws CriticalParseFailure {
		if (!data.startsWith("DELETE ")) throw new CriticalParseFailure("Invalid delete request");
		String documentId = data.replace("DELETE ", "").trim();

		if (!documentId.startsWith(HTTP)) {
			documentId = source.toURI() + "/" + documentId;
		}

		return DwRoot.createDeleteRequest(Qname.fromURI(documentId), source);
	}

}
