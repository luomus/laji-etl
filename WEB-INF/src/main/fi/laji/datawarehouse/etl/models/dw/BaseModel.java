package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;

@MappedSuperclass
public abstract class BaseModel {

	private List<Fact> facts = new ArrayList<>();

	private String notes;

	@FileDownloadField(name="Notes", order=Integer.MAX_VALUE-1)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = Util.trimToByteLength(notes, 5000);
	}

	public void addFact(String fact, String value) {
		if (!given(value)) return;
		if (!given(fact)) return;
		if (factValues(fact).contains(value)) return;
		facts.add(new Fact(fact, value));
	}

	protected boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

	protected boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	public void setFacts(ArrayList<Fact> facts) {
		if (facts == null) {
			this.facts = new ArrayList<>();
		} else {
			this.facts = facts;
			this.facts.removeIf(f->!given(f.getValue()) || !given(f.getFact()));
		}
	}

	public void removeFact(String factName) {
		facts.removeIf(f->f.getFact().equals(factName));
	}

	@Transient
	public List<Fact> getFacts() {
		return facts;
	}

	public String factValue(String factName) {
		for (Fact f : facts) {
			if (f.getFact().equals(factName)) return f.getValue();
		}
		return null;
	}

	public Integer factIntValue(String factName) {
		for (Fact f : facts) {
			if (f.getFact().equals(factName) && f.getIntegerValue() != null) return f.getIntegerValue();
		}
		return null;
	}

	public Set<String> factValues(String factName) {
		Set<String> values = null;
		for (Fact f : facts) {
			if (f.getFact().equals(factName)) {
				if (values == null) values = new LinkedHashSet<>();
				values.add(f.getValue());
			}
		}
		if (values == null) return Collections.emptySet();
		return values;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((facts == null) ? 0 : facts.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseModel other = (BaseModel) obj;
		if (facts == null) {
			if (other.facts != null)
				return false;
		} else if (!facts.equals(other.facts))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		return true;
	}


}
