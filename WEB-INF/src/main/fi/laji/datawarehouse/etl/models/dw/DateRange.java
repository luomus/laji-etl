package fi.laji.datawarehouse.etl.models.dw;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.etl.utils.Util.DateValidateMode;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;

@Embeddable
public class DateRange {

	private static final Date OLDEST_ALLOWED_DATE;
	static {
		try {
			OLDEST_ALLOWED_DATE = DateUtils.convertToDate("1500-01-01", "yyyy-MM-dd");
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	private Date begin;
	private Date end;

	@Deprecated // use constructors that validate; this is for reflection
	public DateRange() {}

	public DateRange(Date date) throws DateValidationException {
		this(date, date);
	}

	public DateRange(Date begin, Date end) throws DateValidationException {
		this(begin, end, DateValidateMode.ALLOW_YEAR_END_FUTURE);
	}

	private DateRange(Date begin, Date end, DateValidateMode mode) throws DateValidationException {
		if (begin == null && end == null) throw new IllegalArgumentException("Begin and end are both null");
		if (end == null) end = begin;
		if (begin != null && end != null) {
			if (end.before(begin)) {
				Date temp = end;
				end = begin;
				begin = temp;
			}
		}
		validate(begin, end, mode);
		this.begin = begin;
		this.end = end;
	}

	private void validate(Date begin, Date end, DateValidateMode mode) throws DateValidationException {
		Util.validate(begin, "begin", DateValidateMode.STRICT);
		Util.validate(end, "end", mode);
	}

	public void validate() throws DateValidationException {
		validate(this.begin, this.end, DateValidateMode.ALLOW_ALL_FUTURE);
	}

	@Transient
	@FileDownloadField(name="Begin", order=1)
	public Date getBegin() {
		return begin;
	}

	@Transient
	@FileDownloadField(name="End", order=2)
	public Date getEnd() {
		return end;
	}

	@Deprecated // use constructors that validate; this is for reflection
	public void setBegin(Date begin) {
		this.begin = begin;
	}

	@Deprecated // use constructors that validate; this is for reflection
	public void setEnd(Date end) {
		this.end = end;
	}

	public DateRange conceal(SecureLevel secureLevel) {
		if (secureLevel == SecureLevel.NONE) throw new IllegalArgumentException("No point concealing with secure level " + secureLevel);
		if (begin == null || end == null) return null;

		int startYear = DateUtils.convertToDateValue(begin).getYearAsInt();
		int endYear = DateUtils.convertToDateValue(end).getYearAsInt();

		// Conceal to year accuracy
		try {
			return new DateRange(DateUtils.convertToDate(new DateValue(1, 1, startYear)), DateUtils.convertToDate(new DateValue(31, 12, endYear)), DateValidateMode.ALLOW_ALL_FUTURE);
		} catch (DateValidationException e) {
			throw new ETLException(e);
		}
	}

	@Override
	public String toString() {
		return "DateRange [begin=" + toString(begin) + ", end=" + toString(end) + "]";
	}

	private String toString(Date d) {
		if (d == null) return null;
		return DateUtils.format(d, "yyyy-MM-dd");
	}

	public static Date oldestAllowedDate() {
		return OLDEST_ALLOWED_DATE;
	}

	public DateRange copy() {
		try {
			return new DateRange(this.begin, this.end, DateValidateMode.ALLOW_ALL_FUTURE);
		} catch (DateValidationException e) {
			throw new IllegalStateException("Impossible state");
		}
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateRange other = (DateRange) obj;
		return this.toString().equals(other.toString());
	}

}
