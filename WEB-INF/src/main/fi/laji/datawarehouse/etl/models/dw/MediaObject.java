package fi.laji.datawarehouse.etl.models.dw;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.luomus.commons.containers.rdf.Qname;

@MappedSuperclass
public class MediaObject implements Serializable {

	private static final long serialVersionUID = -8648074743823876066L;
	private static final Qname LICENSE_FOR_CONCEALED = new Qname("MZ.intellectualRightsARR");

	public static enum MediaType { IMAGE, AUDIO, VIDEO, MODEL }

	private MediaType mediaType;
	private Qname id;
	private URI fullURL;
	private URI thumbnailURL;
	private URI squareThumbnailURL;
	private URI mp3URL;
	private URI wavURL;
	private URI videoURL;
	private URI lowDetailModelURL;
	private URI highDetailModelURL;
	private String author;
	private String copyrightOwner;
	private Qname licenseId;
	private String caption;
	private List<String> keywords;
	private Qname type;
	private Boolean fullResolutionMediaAvailable;

	@Deprecated // for hibernate
	public MediaObject() {}

	public MediaObject(MediaType mediaType) {
		setMediaType(mediaType);
	}

	public MediaObject(MediaType mediaType, String fullURL) throws DataValidationException {
		setMediaType(mediaType);
		setFullURL(fullURL);
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true)
	public Qname getId() {
		return id;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	@Transient
	@FileDownloadField(name="Type", order=1)
	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	@Column(name="full_url")
	@FileDownloadField(name="URL", order=2)
	public String getFullURL() { // Note: full URL is used as identifying column for the media in Vertica (not each media has URI Identifier, such as those coming from iNat)
		if (lowDetailModelURL != null) return lowDetailModelURL.toString();
		if (highDetailModelURL != null) return highDetailModelURL.toString();
		if (videoURL != null) return videoURL.toString();
		if (fullURL != null) return fullURL.toString();
		if (mp3URL != null) return mp3URL.toString();
		if (wavURL != null) return wavURL.toString();
		return null;
	}

	public void setFullURL(String fullURL) throws DataValidationException {
		if (fullURL == null) {
			this.fullURL = null;
		} else {
			this.fullURL = toURL(fullURL);
		}
	}

	@Transient
	public String getMp3URL() {
		if (mp3URL == null) return null;
		return mp3URL.toString();
	}

	public void setMp3URL(String mp3URL) throws DataValidationException {
		if (mp3URL == null) {
			this.mp3URL = null;
		} else {
			this.mp3URL = toURL(mp3URL);
		}
	}

	@Transient
	public String getWavURL() {
		if (wavURL == null) return null;
		return wavURL.toString();
	}

	public void setWavURL(String wavURL) throws DataValidationException {
		if (wavURL == null) {
			this.wavURL = null;
		} else {
			this.wavURL = toURL(wavURL);
		}
	}

	@Transient
	public String getVideoURL() {
		if (videoURL == null) return null;
		return videoURL.toString();
	}

	public void setVideoURL(String videoURL) throws DataValidationException {
		if (videoURL == null) {
			this.videoURL = null;
		} else {
			this.videoURL = toURL(videoURL);
		}
	}

	@Transient
	public String getLowDetailModelURL() {
		if (lowDetailModelURL == null) return null;
		return lowDetailModelURL.toString();
	}

	public void setLowDetailModelURL(String lowDetailModelURL) throws DataValidationException {
		if (lowDetailModelURL == null) {
			this.lowDetailModelURL = null;
		} else {
			this.lowDetailModelURL = toURL(lowDetailModelURL);
		}
	}

	@Transient
	public String getHighDetailModelURL() {
		if (highDetailModelURL == null) return null;
		return highDetailModelURL.toString();
	}

	public void setHighDetailModelURL(String highDetailModelURL) throws DataValidationException {
		if (highDetailModelURL == null) {
			this.highDetailModelURL = null;
		} else {
			this.highDetailModelURL = toURL(highDetailModelURL);
		}
	}

	@Transient
	@FileDownloadField(name="ThumbnailURL", order=3)
	public String getThumbnailURL() {
		if (thumbnailURL == null) return null;
		return thumbnailURL.toString();
	}

	public void setThumbnailURL(String thumbnailURL) throws DataValidationException {
		if (thumbnailURL == null) {
			this.thumbnailURL = null;
		} else {
			this.thumbnailURL = toURL(thumbnailURL);
		}
	}

	@Transient
	public String getSquareThumbnailURL() {
		if (squareThumbnailURL == null) return null;
		return squareThumbnailURL.toString();
	}

	public void setSquareThumbnailURL(String squareThumbnailURL) throws DataValidationException {
		if (squareThumbnailURL == null) {
			this.squareThumbnailURL = null;
		} else {
			this.squareThumbnailURL = toURL(squareThumbnailURL);
		}
	}

	@Column(name="author")
	@FileDownloadField(name="Author", order=4)
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = Util.trimToByteLength(author, 5000);
	}

	@Column(name="license_id")
	@FileDownloadField(name="License", order=6)
	public String getLicenseId() {
		if (licenseId == null) return null;
		return licenseId.toURI();
	}

	@Deprecated // for hibernate; use setLicenceIdUsingQname(Qname)
	public void setLicenseId(String licenseURI) {
		if (licenseURI == null) {
			this.licenseId = null;
		}
		else {
			this.licenseId = Qname.fromURI(licenseURI);
		}
	}

	@Transient
	public void setLicenseIdUsingQname(Qname licenseId) {
		this.licenseId = licenseId;
	}

	@Transient
	@FileDownloadField(name="Caption", order=5)
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = Util.trimToByteLength(caption, 5000);
	}

	@Column(name="copyright_owner")
	@FileDownloadField(name="CopyrightOwner", order=9)
	public String getCopyrightOwner() {
		return copyrightOwner;
	}

	public void setCopyrightOwner(String copyrightOwner) {
		this.copyrightOwner = Util.trimToByteLength(copyrightOwner, 5000);
	}

	@Transient
	@FieldDefinition(ignoreForQuery=true, ignoreForETL=true)
	public List<String> getKeywords() {
		if (keywords == null) return Collections.emptyList();
		return keywords;
	}

	public MediaObject addKeyword(String keyword) {
		if (keyword == null || keyword.trim().isEmpty()) return this;
		if (this.keywords == null) this.keywords = new ArrayList<>();
		keyword = Util.trimToByteLength(keyword, 200);
		if (!this.keywords.contains(keyword)) {
			this.keywords.add(keyword);
		}
		return this;
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	@Transient
	public Qname getType() {
		return type;
	}

	public void setType(Qname type) {
		this.type = type;
	}

	@Transient
	public Boolean getFullResolutionMediaAvailable() {
		return fullResolutionMediaAvailable;
	}

	public void setFullResolutionMediaAvailable(Boolean fullResolutionMediaAvailable) {
		this.fullResolutionMediaAvailable = fullResolutionMediaAvailable;
	}

	@Override
	public String toString() {
		return "MediaObject [id="+id+", mediaType=" + mediaType + ", fullURL=" + fullURL + ", thumbnailURL=" + thumbnailURL + ", squareThumbnailURL=" + squareThumbnailURL + ", mp3URL="
				+ mp3URL + ", wavURL=" + wavURL + ", videoURL=" + videoURL + ", lowDetailModelURL=" + lowDetailModelURL + ", highDetailModelURL=" + highDetailModelURL + ", author="
				+ author + ", copyrightOwner=" + copyrightOwner + ", licenseId=" + licenseId + ", caption=" + caption
				+ ", keywords=" + keywords + ", type=" + type +", fullResolutionMediaAvailable=" + fullResolutionMediaAvailable +  "]";
	}

	public MediaObject concealPublicData() {
		try {
			MediaObject concealed = new MediaObject(this.getMediaType());
			concealed.setLicenseIdUsingQname(LICENSE_FOR_CONCEALED);
			concealed.setFullURL(this.getFullURL());
			concealed.setSquareThumbnailURL(this.getSquareThumbnailURL());
			concealed.setThumbnailURL(this.getThumbnailURL());
			concealed.setWavURL(this.getWavURL());
			concealed.setMp3URL(this.getMp3URL());
			concealed.setVideoURL(this.getVideoURL());
			concealed.setLowDetailModelURL(this.getLowDetailModelURL());
			concealed.setHighDetailModelURL(this.getHighDetailModelURL());
			this.getKeywords().forEach(concealed::addKeyword);
			concealed.setType(this.getType());
			return concealed;
		} catch (DataValidationException e) {
			throw new ETLException("Impossible state", e);
		}
	}

	private URI toURL(String url) throws DataValidationException {
		if (url == null || url.isEmpty()) return null;
		try {
			url = url.replace("[", "%5B").replace("]", "%5D");
			if (!url.startsWith("http://") && !url.startsWith("https://")) {
				throw new DataValidationException("Invalid URL: " + url);
			}
			if (url.startsWith("http://")) {
				if (url.startsWith("http://tun.fi")) return new URI(url);
				return new URI(url.replace("http://", "https://"));
			}
			return new URI(url);
		} catch (Exception e) {
			throw new DataValidationException("Invalid URL: " + url, e);
		}
	}

	public boolean hasMediaURLsSet() {
		return getFullURL() != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((caption == null) ? 0 : caption.hashCode());
		result = prime * result + ((copyrightOwner == null) ? 0 : copyrightOwner.hashCode());
		result = prime * result + ((fullURL == null) ? 0 : fullURL.hashCode());
		result = prime * result + ((highDetailModelURL == null) ? 0 : highDetailModelURL.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((keywords == null) ? 0 : keywords.hashCode());
		result = prime * result + ((licenseId == null) ? 0 : licenseId.hashCode());
		result = prime * result + ((lowDetailModelURL == null) ? 0 : lowDetailModelURL.hashCode());
		result = prime * result + ((mediaType == null) ? 0 : mediaType.hashCode());
		result = prime * result + ((mp3URL == null) ? 0 : mp3URL.hashCode());
		result = prime * result + ((squareThumbnailURL == null) ? 0 : squareThumbnailURL.hashCode());
		result = prime * result + ((thumbnailURL == null) ? 0 : thumbnailURL.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((videoURL == null) ? 0 : videoURL.hashCode());
		result = prime * result + ((wavURL == null) ? 0 : wavURL.hashCode());
		result = prime * result + ((fullResolutionMediaAvailable == null) ? 0 : fullResolutionMediaAvailable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaObject other = (MediaObject) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (caption == null) {
			if (other.caption != null)
				return false;
		} else if (!caption.equals(other.caption))
			return false;
		if (copyrightOwner == null) {
			if (other.copyrightOwner != null)
				return false;
		} else if (!copyrightOwner.equals(other.copyrightOwner))
			return false;
		if (fullURL == null) {
			if (other.fullURL != null)
				return false;
		} else if (!fullURL.equals(other.fullURL))
			return false;
		if (highDetailModelURL == null) {
			if (other.highDetailModelURL != null)
				return false;
		} else if (!highDetailModelURL.equals(other.highDetailModelURL))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (keywords == null) {
			if (other.keywords != null)
				return false;
		} else if (!keywords.equals(other.keywords))
			return false;
		if (licenseId == null) {
			if (other.licenseId != null)
				return false;
		} else if (!licenseId.equals(other.licenseId))
			return false;
		if (lowDetailModelURL == null) {
			if (other.lowDetailModelURL != null)
				return false;
		} else if (!lowDetailModelURL.equals(other.lowDetailModelURL))
			return false;
		if (mediaType != other.mediaType)
			return false;
		if (mp3URL == null) {
			if (other.mp3URL != null)
				return false;
		} else if (!mp3URL.equals(other.mp3URL))
			return false;
		if (squareThumbnailURL == null) {
			if (other.squareThumbnailURL != null)
				return false;
		} else if (!squareThumbnailURL.equals(other.squareThumbnailURL))
			return false;
		if (thumbnailURL == null) {
			if (other.thumbnailURL != null)
				return false;
		} else if (!thumbnailURL.equals(other.thumbnailURL))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (videoURL == null) {
			if (other.videoURL != null)
				return false;
		} else if (!videoURL.equals(other.videoURL))
			return false;
		if (wavURL == null) {
			if (other.wavURL != null)
				return false;
		} else if (!wavURL.equals(other.wavURL))
			return false;
		if (fullResolutionMediaAvailable == null) {
			if (other.fullResolutionMediaAvailable != null)
				return false;
		} else if (!fullResolutionMediaAvailable.equals(other.fullResolutionMediaAvailable))
			return false;
		return true;
	}

}
