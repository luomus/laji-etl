package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import fi.laji.datawarehouse.query.download.model.FileDownloadField;

public class DocumentDWLinkings {

	public static enum CollectionQuality { PROFESSIONAL, HOBBYIST, AMATEUR }
	
	private CollectionQuality collectionQuality;
	private List<Person> editors = new ArrayList<>();

	@FileDownloadField(name="CollectionQuality", order=1)
	@Transient
	public CollectionQuality getCollectionQuality() {
		return collectionQuality;
	}

	public void setCollectionQuality(CollectionQuality collectionQuality) {
		this.collectionQuality = collectionQuality;
	}
	
	public List<Person> getEditors() {
		return editors;
	}

	public void setEditors(List<Person> editors) {
		this.editors = editors;
	}

}
