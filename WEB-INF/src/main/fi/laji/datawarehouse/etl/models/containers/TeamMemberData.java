package fi.laji.datawarehouse.etl.models.containers;

public class TeamMemberData {

	private final int id;
	private final String name;
	private final int count;

	public TeamMemberData(int id, String name, int count) {
		this.id = id;
		this.name = name;
		this.count = count;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

}
