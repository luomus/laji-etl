package fi.laji.datawarehouse.etl.models.dw;

import fi.luomus.commons.taxonomy.Taxon;

public class IdentificationDwLinkings {

	private Taxon taxon;

	public Taxon getTaxon() {
		return taxon;
	}

	public void setTaxon(Taxon taxon) {
		this.taxon = taxon;
	}

}
