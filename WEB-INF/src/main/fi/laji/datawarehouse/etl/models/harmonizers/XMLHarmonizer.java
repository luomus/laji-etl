package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.XMLReader;

public class XMLHarmonizer implements Harmonizer<String> {

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		Document xmlDocument = parse(data);

		if (xmlDocument.getRootNode().getName().equals("Document")) {
			return new Fmnh2008Harmonizer().harmonize(xmlDocument, source);
		}
		if (xmlDocument.getRootNode().getName().equals("SimpleDarwinRecordSet")) {
			return new SimpleDarwinRecordSetHarmonizer().harmonize(xmlDocument, source);
		}

		throw new CriticalParseFailure("Unknown xml schema: " + xmlDocument.getRootNode().getName());
	}

	private Document parse(String data) throws CriticalParseFailure {
		try {
			return new XMLReader().parse(data);
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid application/xml", e);
		}
	}

}