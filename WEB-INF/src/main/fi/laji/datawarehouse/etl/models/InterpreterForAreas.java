package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.FinlandAreaUtil;
import fi.laji.datawarehouse.etl.utils.OldFinnishMunicipalities;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class InterpreterForAreas {

	private static final Qname MUNICIPALITY = new Qname("ML.municipality");

	private final DAO dao;

	public InterpreterForAreas(DAO dao) {
		this.dao = dao;
	}

	public void interpret(Gathering gathering, Qname documentId) {
		try {
			if (gathering.getCoordinates() == null && gathering.getGeo() == null) {
				interpretUsingReportedValues(gathering, documentId);
			} else if (gathering.getGeo() != null) {
				interpretUsingGeometry(gathering, documentId);
			} else {
				interpretUsingCoordinates(gathering, documentId);
			}
			interpretAreaDisplayNames(gathering);
		} catch (Exception e) {
			throw dao.exceptionAndReport(documentId.toURI(), InterpreterForAreas.class.getSimpleName(), e);
		}
	}

	private void interpretUsingGeometry(Gathering gathering, Qname documentId) {
		Coordinates boundingBox = getBoundingBox(gathering.getGeo());
		if (boundingBox == null) return;
		interpretUsingCoordinates(gathering, boundingBox, gathering.getGeo(), documentId);
	}

	private void interpretUsingCoordinates(Gathering gathering, Qname documentId) {
		Coordinates coordinates = gathering.getCoordinates().copy();
		interpretUsingCoordinates(gathering, coordinates, null, documentId);
	}

	private void interpretUsingReportedValues(Gathering gathering, Qname documentId) {
		GatheringInterpretations interpretations = gathering.createInterpretations();
		String reportedMunicipality = gathering.getMunicipality();
		String reportedBiogeographicalProvince = gathering.getBiogeographicalProvince();

		Area resolvedCountry = resolveCountry(gathering, documentId);
		if (resolvedCountry != null) {
			interpretations.setCountry(resolvedCountry.getQname());
			interpretations.setSourceOfCountry(GeoSource.REPORTED_VALUE);
		}

		if (reportedMunicipality != null) {
			Area resolvedMunicipality = single(dao.resolveMunicipalitiesByName(reportedMunicipality), documentId);
			if (resolvedMunicipality != null) {
				interpretations.setFinnishMunicipality(resolvedMunicipality.getQname());
				interpretations.addToFinnishMunicipalities(resolvedMunicipality.getQname());
				interpretations.setSourceOfFinnishMunicipality(GeoSource.REPORTED_VALUE);
			} else {
				Area oldMunicipalityJoinedTo = singleAllowMulti(getOldMunicipalityJoinedTo(reportedMunicipality));
				if (oldMunicipalityJoinedTo != null) {
					if (isMunicipality(oldMunicipalityJoinedTo)) {
						interpretations.setFinnishMunicipality(oldMunicipalityJoinedTo.getQname());
						interpretations.addToFinnishMunicipalities(oldMunicipalityJoinedTo.getQname());
						interpretations.setSourceOfFinnishMunicipality(GeoSource.OLD_FINNISH_MUNICIPALITY);
					} else {
						// Joined to country (Russia)
						if (interpretations.getCountry() == null) {
							interpretations.setCountry(Const.RUSSIA);
							interpretations.setSourceOfCountry(GeoSource.OLD_FINNISH_MUNICIPALITY);
						}
					}
				}
			}
		}

		if (interpretations.getFinnishMunicipality() != null && interpretations.getCountry() == null) {
			interpretations.setCountry(Const.FINLAND);
			interpretations.setSourceOfCountry(GeoSource.FINNISH_MUNICIPALITY);
		}

		if (interpretations.getFinnishMunicipality() != null && fromFinland(interpretations)) {
			Coordinates wgs84 = dao.getCoordinatesByFinnishMunicipality(interpretations.getFinnishMunicipality());
			if (wgs84 == null) throw new ETLException("No coordinates for resolved municipality " + interpretations.getFinnishMunicipality());
			wgs84.setAccuracyInMeters(wgs84.calculateBoundingBoxAccuracy());
			interpretations.setCoordinates(wgs84);
			interpretations.setSourceOfCoordinates(GeoSource.FINNISH_MUNICIPALITY);
		}

		if (fromFinland(interpretations)) {
			if (interpretations.getCoordinates() != null) {
				interpretBiogeographicalProvince(Collections.<Qname>emptySet(), gathering, interpretations.getCoordinates(), documentId);
			} else if (reportedBiogeographicalProvince != null) {
				Area resolvedBioProvince = single(dao.resolveBiogeographicalProvincesByName(reportedBiogeographicalProvince), documentId);
				if (resolvedBioProvince != null) {
					interpretations.setBiogeographicalProvince(resolvedBioProvince.getQname());
					interpretations.addToBiogeographicalProvinces(resolvedBioProvince.getQname());
					interpretations.setSourceOfBiogeographicalProvince(GeoSource.REPORTED_VALUE);
				}
			}
		}
	}

	private Area singleAllowMulti(List<Area> resolveByName) {
		if (resolveByName.size() == 1) return resolveByName.get(0);
		return null;
	}

	private Area single(List<Area> resolveByName, Qname documentId) {
		if (resolveByName.isEmpty()) return null;
		if (resolveByName.size() == 1) return resolveByName.get(0);
		List<String> debug = new ArrayList<>();
		for (Area area : resolveByName) {
			debug.add(area.getQname() + " " + area.getType());
		}
		logError(documentId, "Area name resolved to multiple areas, expected 0 or 1: " + debug, true);
		return null;
	}

	private void logError(Qname documentId, String error, boolean report) {
		if (report) {
			dao.getErrorReporter().report(documentId.toURI() + " - " + error);
		}
		dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), documentId.toURI(), new IllegalStateException(error));
	}

	private boolean fromFinland(GatheringInterpretations interpretations) {
		return Const.FINLAND.equals(interpretations.getCountry());
	}

	private List<Area> getOldMunicipalityJoinedTo(String reportedMunicipality) {
		List<Area> currentMunicipalities = new ArrayList<>();
		for (Area oldFinnishMunicipality : dao.resolveOldMunicipalitiesByName(reportedMunicipality)) {
			Area currentMunicipality = getAreaJoinedTo(oldFinnishMunicipality);
			if (!currentMunicipalities.contains(currentMunicipality)) {
				currentMunicipalities.add(currentMunicipality);
			}
		}
		return currentMunicipalities;
	}

	private Area getAreaJoinedTo(Area area) {
		if (area == null) return null;
		Qname joinedToId = area.getPartOf();
		if (joinedToId == null) return null;

		Area joinedTo = dao.getAreas().get(joinedToId);
		return joinedTo;
	}

	private boolean isMunicipality(Area joinedTo) {
		if (joinedTo == null) throw new NullPointerException();
		return MUNICIPALITY.equals(joinedTo.getType());
	}

	private void interpretUsingCoordinates(Gathering gathering, Coordinates coordinates, Geo geo, Qname documentId) {
		GatheringInterpretations interpretations = gathering.createInterpretations();

		Coordinates wgs84Coordinates = null;
		Geo wgs84Geo = null;
		try {
			wgs84Coordinates = getWgs84Coordinates(coordinates);
			if (geo != null) {
				wgs84Geo = getWgs84Geo(geo);
			}
		} catch (DataValidationException | IllegalArgumentException e) {
			Converter.createCoordinateIssue(gathering, coordinates, e);
			return;
		}

		Set<Qname> municipalities, biogeographicalProvinces, naturaAreas, concededMunicipalities;
		if (isInsideGeneralFinlandArea(wgs84Coordinates)) { // Performance tweak; no need to call APIs if area no where close to Finland
			municipalities = getFinnishMunicipalities(wgs84Coordinates, wgs84Geo);
			biogeographicalProvinces = getBiogeographicalProvinces(wgs84Coordinates, wgs84Geo);
			naturaAreas = getNaturaAreas(wgs84Coordinates, wgs84Geo);
			concededMunicipalities = getConcededFinnishMunicipalities(wgs84Coordinates);
		} else {
			municipalities = biogeographicalProvinces = naturaAreas = concededMunicipalities = Collections.emptySet();
		}

		validateMunicipalityBioprovinceCoverage(wgs84Coordinates, municipalities, biogeographicalProvinces, documentId);

		validateAndIntepreteCountryAndMunicipality(gathering, wgs84Coordinates, municipalities, concededMunicipalities, coordinates.checkIfIsYkjGrid(), documentId);

		if (given(municipalities)) {
			interpretBiogeographicalProvince(biogeographicalProvinces, gathering, wgs84Coordinates, documentId);
			setNatureAreas(interpretations, naturaAreas);
		}

		coordinates.setAccuracyInMeters(interpretCoordinateAccuracy(!municipalities.isEmpty(), gathering));
		interpretations.setCoordinates(coordinates);
		interpretations.setSourceOfCoordinates(GeoSource.REPORTED_VALUE);
	}

	private Integer interpretCoordinateAccuracy(boolean fromFinland, Gathering gathering) {
		if (Boolean.TRUE.equals(gathering.getAccurateArea())) return 1;

		Coordinates geoBbox = null;
		Coordinates coordinates = null;

		if (fromFinland) {
			if (gathering.getGeo() != null) {
				geoBbox = getMetricBoundingBox(gathering.getGeo());
			} else {
				coordinates = getMetricCoordinates(gathering.getCoordinates());
			}
		} else {
			if (gathering.getGeo() != null) {
				geoBbox = getBoundingBox(gathering.getGeo());
			} else {
				coordinates = gathering.getCoordinates();
			}
		}

		if (geoBbox != null) {
			return geoBbox.getAccuracyInMeters();
		}
		if (coordinates != null) {
			return interpretCoordinateAccuracy(gathering.getCoordinates().getAccuracyInMeters(), coordinates.calculateBoundingBoxAccuracy());
		}
		return null;
	}

	private Integer interpretCoordinateAccuracy(Integer reportedAccuracyInMeters, int calculatedBoundingBoxAccuracy) {
		if (reportedAccuracyInMeters == null) return calculatedBoundingBoxAccuracy;
		return Math.max(reportedAccuracyInMeters, calculatedBoundingBoxAccuracy);
	}

	private void setNatureAreas(GatheringInterpretations interpretations, Set<Qname> naturaAreas) {
		for (Qname id : naturaAreas) {
			interpretations.addToNaturaAreas(id);
		}
	}

	private void validateMunicipalityBioprovinceCoverage(Coordinates wgs84Coordinates, Set<Qname> municipalities, Set<Qname> biogeographicalProvinces, Qname documentId) {
		if (given(biogeographicalProvinces) && notGiven(municipalities)) {
			// Area is inside Finland (there is a biogeographicalProvince) but municipality was not resolved: This is some small area that our municipality data does not cover.
			logError(documentId, "Gap in municipality data that should be fixed. Inside biogeo area " + biogeographicalProvinces + ". " + wgs84Coordinates, false);
		} else if (given(municipalities) && notGiven(biogeographicalProvinces)) {
			// Area is inside Finland (there is a municipality) but bioprovince was not resolved: This is some small area that our bioprovince data does not cover.
			logError(documentId, "Gap in bioprovince data that should be fixed. Inside municipalities " + municipalities + ". " + wgs84Coordinates, false);
		}
	}

	private void validateAndIntepreteCountryAndMunicipality(Gathering gathering, Coordinates wgs84Coordinates, Set<Qname> municipalitiesByCoordinates, Set<Qname> concededMunicipalitiesByCoordinates, boolean originallyYKJSquare, Qname documentId) {
		GatheringInterpretations interpretations = gathering.getInterpretations();

		Area resolvedCountryByName = resolveCountry(gathering, documentId);
		Area resolvedMunicipalityByName = single(dao.resolveMunicipalitiesByName(gathering.getMunicipality()), documentId);
		Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames = new HashSet<>();
		Set<Area> resolvedConcededMunicipalitiesByName = new HashSet<>();
		for (Area oldMunicipality : dao.resolveOldMunicipalitiesByName(gathering.getMunicipality())) {
			Area joinedTo = getAreaJoinedTo(oldMunicipality);
			if (isMunicipality(joinedTo)) {
				resolvedCurrentMunicipalitiesByOldMunicipalityNames.add(joinedTo);
			} else {
				// Municipality joined to country (Russia)
				resolvedConcededMunicipalitiesByName.add(oldMunicipality);
			}
		}

		Set<Qname> municipalityMatches = resolveMatches(municipalitiesByCoordinates, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames);

		Set<Qname> concededMunicipalityMatches = new HashSet<>();
		for (Area byName : resolvedConcededMunicipalitiesByName) {
			if (matches(byName, concededMunicipalitiesByCoordinates)) {
				concededMunicipalityMatches.add(byName.getQname());
			}
		}

		boolean resolvedMunicipalityNameGiven = resolvedMunicipalityByName != null || given(resolvedCurrentMunicipalitiesByOldMunicipalityNames) || given(resolvedConcededMunicipalitiesByName);

		ExtendedMunicipalities extendedMunicipalities = new ExtendedMunicipalities(wgs84Coordinates, originallyYKJSquare, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames);

		//		System.out.println("coords         : " + wgs84Coordinates);
		//		System.out.println("m  by coord    : " + municipalitiesByCoordinates);
		//		System.out.println("cm by coord    : " + concededMunicipalitiesByCoordinates);
		//		System.out.println("em by coord    : " + extendedMunicipalities.getMunicipalities());
		//		System.out.println("m by name      : " + resolvedMunicipalityByName);
		//		System.out.println("m by old names : " + resolvedCurrentMunicipalitiesByOldMunicipalityNames);
		//		System.out.println("cm by name     : " + resolvedConcededMunicipalitiesByName);
		//		System.out.println("matches        : " + municipalityMatches);
		//		System.out.println("cmatches       : " + concededMunicipalityMatches);
		//		System.out.println("ematches       : " + extendedMunicipalities.getMatches());
		//		System.out.println("known name given: " + resolvedMunicipalityNameGiven);
		//		System.out.println("country by name:" + resolvedCountryByName);

		if (given(municipalityMatches)) {
			// Coordinates are in Finland and the municipalities resolved by name match to at least one municipality resolved by coordinates
			validateAndInterpretCountryWhenCoordinatesInFinland(gathering, resolvedCountryByName);
			interpretFinnishMunicipality(municipalitiesByCoordinates, municipalityMatches, interpretations, wgs84Coordinates);
			return;
		}

		if (notGiven(municipalitiesByCoordinates) && notGiven(concededMunicipalitiesByCoordinates)) {
			// Coordinates are not inside Finland or conceded area
			if (given(extendedMunicipalities.getMunicipalities())) {
				// Coordinates are close to Finland
				validateAndInterpretCountryWhenCoordinatesArePrettyCloseToFinlandButNotInsideFinland(gathering, resolvedCountryByName);
				if (resolvedMunicipalityNameGiven && notGiven(extendedMunicipalities.getMatches())) {
					createMunicipalityOutsideFinlandIssue(gathering, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames, resolvedConcededMunicipalitiesByName);
				}
				return;
			}
			// Coordinates nowhere near Finland
			validateAndInterpretCountryWhenCoordinatesOutsideFinland(gathering, resolvedCountryByName);
			if (givenAndNotFinland(resolvedCountryByName)) {
				// Given municipality name matches with a Finnish municipality, but a country name has been given that is not Finland
				// Other countries can have valid place names that match with Finnish municipality names
				return;
			}
			if (resolvedMunicipalityNameGiven) {
				createMunicipalityOutsideFinlandIssue(gathering, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames, resolvedConcededMunicipalitiesByName);
			}
			return;
		}

		if (notGiven(municipalitiesByCoordinates) && given(concededMunicipalityMatches)) {
			// Coordinates are in conceded area and not inside Finland and conceded municipality resolved by name matches to at least one of the conceded municipalities resolved by coordinates
			validateAndInterpretCountryWhenCoordinatesInConcededMunicipality(gathering, resolvedCountryByName);
			return;
		}

		if (given(municipalitiesByCoordinates) && given(concededMunicipalityMatches) && notGiven(extendedMunicipalities.getMatches())) {
			// Coordinates touch Finland but a conceded municipality matches both the coordinates and resolved by name and given municipality name does not match extended finnish municipalities
			validateAndInterpretCountryWhenCoordinatesInConcededMunicipality(gathering, resolvedCountryByName);
			return;
		}

		if (given(municipalitiesByCoordinates) && !resolvedMunicipalityNameGiven) {
			// Coordinates are in Finland and no need to cross check with given municipality name
			validateAndInterpretCountryWhenCoordinatesInFinland(gathering, resolvedCountryByName);
			interpretFinnishMunicipality(municipalitiesByCoordinates, municipalityMatches, interpretations, wgs84Coordinates);
			return;
		}

		if (given(municipalitiesByCoordinates)) {
			// Coordinates are in Finland and a valid municipality name was given but it does not match with the coordinates
			if (notGiven(extendedMunicipalities.getMatches()) && notGiven(concededMunicipalityMatches)) {
				createMunicipalityMismatchIssue(gathering, municipalitiesByCoordinates, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames, resolvedConcededMunicipalitiesByName);
			} else {
				validateAndInterpretCountryWhenCoordinatesInFinland(gathering, resolvedCountryByName);
				interpretFinnishMunicipality(municipalitiesByCoordinates, municipalityMatches, interpretations, wgs84Coordinates);
			}
			return;
		}

		if (given(concededMunicipalitiesByCoordinates) && !resolvedMunicipalityNameGiven) {
			// Coordinates are inside conceded area and no need to cross check with given municipality name
			validateAndInterpretCountryWhenCoordinatesInConcededMunicipality(gathering, resolvedCountryByName);
			return;
		}

		if (given(concededMunicipalitiesByCoordinates)) {
			// Coordinates are inside conceded area and a valid municipality name was given but it does not match with the coordinates
			if (notGiven(extendedMunicipalities.getMatches())) {
				createMunicipalityMismatchIssue(gathering, concededMunicipalitiesByCoordinates, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames, resolvedConcededMunicipalitiesByName);
			} else {
				validateAndInterpretCountryWhenCoordinatesArePrettyCloseToFinlandButNotInsideFinland(gathering, resolvedCountryByName);
			}
			return;
		}

		throw new IllegalStateException("No more combinations");
	}

	private boolean givenAndNotFinland(Area resolvedCountryByName) {
		return resolvedCountryByName != null && !isFinland(resolvedCountryByName);
	}

	private Set<Qname> resolveMatches(Set<Qname> municipalitiesByCoordinates, Area resolvedMunicipalityByName, Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames) {
		Set<Qname> municipalityMatches = new HashSet<>();
		if (resolvedMunicipalityByName != null) {
			if (matches(resolvedMunicipalityByName, municipalitiesByCoordinates)) {
				municipalityMatches.add(resolvedMunicipalityByName.getQname());
			}
		}
		for (Area byName : resolvedCurrentMunicipalitiesByOldMunicipalityNames) {
			if (matches(byName, municipalitiesByCoordinates)) {
				municipalityMatches.add(byName.getQname());
			}
		}
		return municipalityMatches;
	}

	private void createMunicipalityOutsideFinlandIssue(Gathering gathering, Area resolvedMunicipalityByName, Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames,
			Set<Area> resolvedConcededMunicipalitiesByName) {
		if (resolvedMunicipalityByName != null) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside municipality " + getName(resolvedMunicipalityByName) + " they are outside Finland");
		} else if (!resolvedCurrentMunicipalitiesByOldMunicipalityNames.isEmpty()) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside old municipality " + debugName(gathering.getMunicipality()) + " that has been joined to " + getNamesOfAreas(resolvedCurrentMunicipalitiesByOldMunicipalityNames) + " they are outside Finland");
		} else if (!resolvedConcededMunicipalitiesByName.isEmpty()) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside old conceded municipality " + getNamesOfAreas(resolvedConcededMunicipalitiesByName) + " they are outside Finland");
		} else {
			throw new IllegalStateException();
		}
	}

	private void createMunicipalityMismatchIssue(Gathering gathering, Set<Qname> municipalitiesByCoordinates, Area resolvedMunicipalityByName, Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames, Set<Area> resolvedConcededMunicipalitiesByName) {
		if (resolvedMunicipalityByName != null) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside municipality " + getName(resolvedMunicipalityByName) + " they are inside " + getNames(municipalitiesByCoordinates));
		} else if (!resolvedCurrentMunicipalitiesByOldMunicipalityNames.isEmpty()) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside old municipality " + debugName(gathering.getMunicipality()) + " that has been joined to " + getNamesOfAreas(resolvedCurrentMunicipalitiesByOldMunicipalityNames) + " they are inside " + getNames(municipalitiesByCoordinates));
		} else if (!resolvedConcededMunicipalitiesByName.isEmpty()) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_MUNICIPALITY_MISMATCH,
					"Coordinates are not inside old conceded municipality " + getNamesOfAreas(resolvedConcededMunicipalitiesByName) + " they are inside " + getNames(municipalitiesByCoordinates));
		} else {
			throw new IllegalStateException();
		}
	}

	private String debugName(String name) {
		if (name == null) return "";
		List<String> parts = new ArrayList<>();
		for (String s : name.split(Pattern.quote(" "))) {
			parts.add(Utils.upperCaseFirst(s.toLowerCase()) + " ");
		}
		return "[" + Utils.catenate(parts) + "]";
	}

	private Area resolveCountry(Gathering gathering, Qname documentId) {
		Area resolvedCountryByName = resolveCountry(gathering.getCountry(), documentId);
		if (resolvedCountryByName == null) {
			resolvedCountryByName = searchCountryFromLocality(gathering);
		}
		return resolvedCountryByName;
	}

	private boolean matches(Area area, Set<Qname> ids) {
		return ids.contains(area.getQname());
	}

	private Area resolveCountry(String country, Qname documentId) {
		Area area = single(dao.resolveCountiesByName(country), documentId);
		if (area == null) return null;
		if (area.getQname().equals(Const.SOVIET_UNION)) {
			return dao.getAreas().get(Const.RUSSIA);
		}
		return area;
	}

	private Area searchCountryFromLocality(Gathering gathering) {
		if (gathering.getLocality() == null) return null;
		String locality = gathering.getLocality().toLowerCase();
		if (locality.startsWith("suomi,")) {
			return dao.getAreas().get(Const.FINLAND);
		}
		if (locality.startsWith("finland,")) {
			return dao.getAreas().get(Const.FINLAND);
		}
		if (locality.startsWith("suomi ")) {
			return dao.getAreas().get(Const.FINLAND);
		}
		if (locality.startsWith("finland ")) {
			return dao.getAreas().get(Const.FINLAND);
		}
		return null;
	}

	private Set<Qname> getExpandedAreaFinnishMunicipalities(Coordinates wgs84Coordinates, boolean originallyYKJSQuare) {
		if (!isInsideGeneralFinlandArea(wgs84Coordinates)) return Collections.emptySet();
		Coordinates expandedCoordinates = expandCoordinates(wgs84Coordinates, originallyYKJSQuare);
		if (expandedCoordinates == null) return Collections.emptySet();
		return dao.getFinnishMunicipalities(expandedCoordinates);
	}

	private void validateAndInterpretCountryWhenCoordinatesArePrettyCloseToFinlandButNotInsideFinland(Gathering gathering, Area resolvedCountryByName) {
		if (resolvedCountryByName == null || isFinland(resolvedCountryByName) || isSurroundingCountry(resolvedCountryByName)) {
			// Coordinates are close to Finland and no conflict with reported country
			if (isSurroundingCountry(resolvedCountryByName)) {
				if (resolvedCountryByName != null) {
					// Trust the reported country. (Don't trust reported country Finland, because coordinates are _not_ inside Finland)
					gathering.getInterpretations().setCountry(resolvedCountryByName.getQname());
					gathering.getInterpretations().setSourceOfCountry(GeoSource.REPORTED_VALUE);
				}
			}
		} else {
			setLocationissue(gathering, Quality.Issue.COORDINATES_COUNTRY_MISMATCH,
					"Coordinates are close to Finland but reported country is not a neighbouring country: " + getName(resolvedCountryByName));
		}
	}

	private void validateAndInterpretCountryWhenCoordinatesOutsideFinland(Gathering gathering, Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return;
		if (isFinland(resolvedCountryByName)) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_COUNTRY_MISMATCH, "Country reported to be Finland but coordinates are not inside Finland");
		} else {
			gathering.getInterpretations().setCountry(resolvedCountryByName.getQname());
			gathering.getInterpretations().setSourceOfCountry(GeoSource.REPORTED_VALUE);
		}
	}

	private void validateAndInterpretCountryWhenCoordinatesInConcededMunicipality(Gathering gathering, Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return;

		if (isRussia(resolvedCountryByName) || isFinland(resolvedCountryByName) || isSweden(resolvedCountryByName))  {
			gathering.getInterpretations().setCountry(Const.RUSSIA);
			gathering.getInterpretations().setSourceOfCountry(GeoSource.COORDINATES);
			return;
		}

		if (!isSurroundingCountry(resolvedCountryByName)) {
			setLocationissue(gathering, Quality.Issue.COORDINATES_COUNTRY_MISMATCH,
					"Coordinates are inside old Finnish conceded municipality but reported country is " + getName(resolvedCountryByName));
			return;
		}

		gathering.getInterpretations().setCountry(resolvedCountryByName.getQname());
		gathering.getInterpretations().setSourceOfCountry(GeoSource.COORDINATES);
	}

	private void validateAndInterpretCountryWhenCoordinatesInFinland(Gathering gathering, Area resolvedCountryByName) {
		if (resolvedCountryByName == null || isFinland(resolvedCountryByName) || isSurroundingCountry(resolvedCountryByName)) {
			gathering.getInterpretations().setCountry(Const.FINLAND);
			gathering.getInterpretations().setSourceOfCountry(GeoSource.COORDINATES);
		} else {
			setLocationissue(gathering, Quality.Issue.COORDINATES_COUNTRY_MISMATCH,
					"Coordinates are inside Finland but reported country is " + getName(resolvedCountryByName));
		}
	}

	private Set<Qname> getConcededFinnishMunicipalities(Coordinates wgs84Coordinates) {
		return OldFinnishMunicipalities.getMunicipalities(wgs84Coordinates);
	}

	private String getName(Area area) {
		return area.getName().forLocale("en");
	}

	private String getNames(Set<Qname> areaIds) {
		List<String> names = new ArrayList<>();
		for (Qname id : areaIds) {
			names.add(getName(dao.getAreas().get(id)));
		}
		Collections.sort(names);
		return names.toString();
	}

	private String getNamesOfAreas(Set<Area> areas) {
		List<String> names = new ArrayList<>();
		for (Area area : areas) {
			names.add(area.getName().forLocale("en"));
		}
		Collections.sort(names);
		return names.toString();
	}

	private boolean isSurroundingCountry(Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return false;
		return Const.COUNTRIES_SURROUNDING_FINLAND.contains(resolvedCountryByName.getQname());
	}

	private boolean isFinland(Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return false;
		return resolvedCountryByName.getQname().equals(Const.FINLAND);
	}

	private boolean isRussia(Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return false;
		return resolvedCountryByName.getQname().equals(Const.RUSSIA) || resolvedCountryByName.getQname().equals(Const.SOVIET_UNION);
	}

	private boolean isSweden(Area resolvedCountryByName) {
		if (resolvedCountryByName == null) return false;
		return resolvedCountryByName.getQname().equals(Const.SWEDEN);
	}

	private void setLocationissue(Gathering gathering, Issue issue, String message) {
		String coordVerbatim = gathering.getCoordinatesVerbatim();
		if (given(coordVerbatim) && coordVerbatim.length() < 80) {
			message += ": " + coordVerbatim;
		} else if (gathering.getCoordinates() != null) {
			message += ": " + debug(gathering.getCoordinates());
		}
		gathering.createQuality().setLocationIssue(new Quality(issue, Quality.Source.AUTOMATED_FINBIF_VALIDATION, message));
	}

	private String debug(Coordinates coordinates) {
		if (coordinates.getType() == Type.YKJ) {
			return debugYKJ(coordinates);
		}
		StringBuilder b = new StringBuilder();
		b.append(coordinates.getLatMin()).append(", ").append(coordinates.getLonMin());
		if (!coordinates.getLatMin().equals(coordinates.getLatMax()) || !coordinates.getLonMin().equals(coordinates.getLonMax())) {
			b.append(" - ");
			b.append(coordinates.getLatMax()).append(", ").append(coordinates.getLonMax());
		}
		b.append(" ").append(coordinates.getType());
		return b.toString();
	}

	private String debugYKJ(Coordinates coordinates) {
		StringBuilder b = new StringBuilder();
		if (coordinates.getLatMin().intValue() == (coordinates.getLatMax().intValue() - 1)) {
			b.append(coordinates.getLatMin().intValue()).append(":");
		} else {
			b.append(coordinates.getLatMin().intValue()).append("-").append(coordinates.getLatMax().intValue()).append(":");
		}
		if (coordinates.getLonMin().intValue() == (coordinates.getLonMax().intValue() - 1 )) {
			b.append(coordinates.getLonMin().intValue());
		} else {
			b.append(coordinates.getLonMin().intValue()).append("-").append(coordinates.getLonMax().intValue());
		}
		b.append(" YKJ");
		return b.toString();
	}

	private Set<Qname> getBiogeographicalProvinces(Coordinates wgs84Coordinates, Geo wgs84Geo) {
		if (wgs84Geo != null) {
			return dao.getBiogeographicalProvinces(wgs84Geo);
		}
		return dao.getBiogeographicalProvinces(wgs84Coordinates);
	}

	private Set<Qname> getFinnishMunicipalities(Coordinates wgs84Coordinates, Geo wgs84Geo) {
		if (wgs84Geo != null) {
			return dao.getFinnishMunicipalities(wgs84Geo);
		}
		return dao.getFinnishMunicipalities(wgs84Coordinates);
	}

	private Set<Qname> getNaturaAreas(Coordinates wgs84Coordinates, Geo wgs84Geo) {
		if (wgs84Geo != null) {
			return dao.getNaturaAreas(wgs84Geo);
		}
		return dao.getNaturaAreas(wgs84Coordinates);
	}

	private Coordinates getCenterPoint(Coordinates wgs84Coordinates) {
		Coordinates wgs84CenterPoint;
		try {
			wgs84CenterPoint = new Coordinates(wgs84Coordinates.convertCenterPoint());
		} catch (DataValidationException e) {
			throw new ETLException(e);
		}
		return wgs84CenterPoint;
	}

	private Coordinates expandCoordinates(Coordinates c, boolean originallyYKJSQuare) {
		if (c.calculateBoundingBoxAccuracy() >= 50000) return null;

		// expand ykj square data only a little, other types more
		// reasoning: if ykj square doesn't match, it is most likely a typo - if some other geometry (chosen from map) doesn't match, it is most likely invalidly selected municipality

		if (originallyYKJSQuare) {
			Coordinates expanded;
			try {
				expanded = new Coordinates(
						c.getLatMin() - 0.2,
						c.getLatMax() + 0.2,
						c.getLonMin() - 0.4,
						c.getLonMax() + 0.4,
						Type.WGS84);
				return expanded;
			} catch (DataValidationException e) {
				return null;
			}
		}
		Coordinates expanded;
		try {
			expanded = new Coordinates(
					c.getLatMin() - 0.50,
					c.getLatMax() + 0.50,
					c.getLonMin() - 0.7,
					c.getLonMax() + 0.7,
					Type.WGS84);
			return expanded;
		} catch (DataValidationException e) {
			return null;
		}
	}

	private void interpretBiogeographicalProvince(Set<Qname> biogeographicalProvincesResolvedByCoordinates, Gathering gathering, Coordinates wgs84Coordinates, Qname documentId) {
		GatheringInterpretations interpretations = gathering.getInterpretations();

		if (given(biogeographicalProvincesResolvedByCoordinates)) {
			if (biogeographicalProvincesResolvedByCoordinates.size() == 1) {
				Qname singleProvince = biogeographicalProvincesResolvedByCoordinates.iterator().next();
				interpretations.setBiogeographicalProvince(singleProvince);
				interpretations.setSourceOfBiogeographicalProvince(GeoSource.COORDINATES);
				interpretations.addToBiogeographicalProvinces(singleProvince);
				return;
			}

			Area resolvedProvinceByName = single(dao.resolveBiogeographicalProvincesByName(gathering.getBiogeographicalProvince()), documentId);
			if (resolvedProvinceByName != null) {
				for (Qname coordinateResolvedProvince : biogeographicalProvincesResolvedByCoordinates) {
					if (coordinateResolvedProvince.equals(resolvedProvinceByName.getQname())) {
						interpretations.setBiogeographicalProvince(coordinateResolvedProvince);
						interpretations.setSourceOfBiogeographicalProvince(GeoSource.REPORTED_VALUE);
						interpretations.addToBiogeographicalProvinces(coordinateResolvedProvince);
						return;
					}
				}
			}

			for (Qname province : biogeographicalProvincesResolvedByCoordinates) {
				interpretations.addToBiogeographicalProvinces(province);
			}
		}

		Coordinates centerPoint = getCenterPoint(wgs84Coordinates);
		Set<Qname> resolvedByCenterPoint = dao.getBiogeographicalProvinces(centerPoint);

		if (notGiven(resolvedByCenterPoint)) {
			// Centerpoint is outside Finland (Enontekiö); try a bit larger bounding box
			Coordinates expandedCenterPoint = expandCoordinates(centerPoint, false);
			if (expandedCenterPoint != null) {
				resolvedByCenterPoint = dao.getBiogeographicalProvinces(expandedCenterPoint);
			}
		}
		if (given(resolvedByCenterPoint)) {
			Qname byCenterPoint = resolvedByCenterPoint.iterator().next();
			interpretations.setBiogeographicalProvince(byCenterPoint);
			if (notGiven(biogeographicalProvincesResolvedByCoordinates)) {
				interpretations.addToBiogeographicalProvinces(byCenterPoint);
			}
			interpretations.setSourceOfBiogeographicalProvince(GeoSource.COORDINATE_CENTERPOINT);
		}
	}

	private void interpretFinnishMunicipality(Set<Qname> resolvedByCoordinates, Set<Qname> coordinateNameMatches, GatheringInterpretations interpretations, Coordinates wgs84Coordinates) {
		if (resolvedByCoordinates.size() == 1) {
			Qname singleMunicipality = resolvedByCoordinates.iterator().next();
			interpretations.addToFinnishMunicipalities(singleMunicipality);
			interpretations.setFinnishMunicipality(singleMunicipality);
			interpretations.setSourceOfFinnishMunicipality(GeoSource.COORDINATES);
			return;
		}

		if (coordinateNameMatches.size() == 1) {
			Qname singleMunicipality = coordinateNameMatches.iterator().next();
			interpretations.addToFinnishMunicipalities(singleMunicipality);
			interpretations.setFinnishMunicipality(singleMunicipality);
			interpretations.setSourceOfFinnishMunicipality(GeoSource.REPORTED_VALUE);
			return;
		}

		for (Qname municipality : resolvedByCoordinates) {
			interpretations.addToFinnishMunicipalities(municipality);
		}

		Coordinates centerPoint = getCenterPoint(wgs84Coordinates);
		Set<Qname> centerPointMunicipalities = dao.getFinnishMunicipalities(centerPoint);
		if (given(centerPointMunicipalities)) {
			interpretations.setFinnishMunicipality(centerPointMunicipalities.iterator().next());
			interpretations.setSourceOfFinnishMunicipality(GeoSource.COORDINATE_CENTERPOINT);
		}
	}

	private void interpretAreaDisplayNames(Gathering gathering) {
		GatheringInterpretations interpretations = gathering.createInterpretations();
		interpretations.setCountryDisplayname(interpretAreaDisplayName(gathering.getCountry(), interpretations.getCountry(), false));
		interpretations.setBiogeographicalProvinceDisplayname(interpretAreaDisplayName(gathering.getBiogeographicalProvince(), interpretations.getBiogeographicalProvinces(), interpretations.getBiogeographicalProvince(), true));
		interpretations.setMunicipalityDisplayname(interpretAreaDisplayName(gathering.getMunicipality(), interpretations.getFinnishMunicipalities(), interpretations.getFinnishMunicipality(), false));
	}

	private String interpretAreaDisplayName(String verbatim, Qname interpreted, boolean abbrev) {
		if (given(interpreted)) {
			return getAreaName(verbatim, interpreted, abbrev);
		}
		return verbatim;
	}

	private String getAreaName(String verbatim, Qname interpreted, boolean abbrev) {
		if (interpreted == null) return verbatim;
		Area area = dao.getAreas().get(interpreted);
		if (area == null) area = dao.getAreasForceReload().get(interpreted);
		if (area == null) return verbatim;
		String name = area.getName().forLocale("fi");
		if (notGiven(name)) return verbatim;
		if (abbrev) {
			if (given(area.getAbbreviation())) {
				name += " (" + area.getAbbreviation() + ")";
			}
		}
		return name;
	}

	private String interpretAreaDisplayName(String verbatim, List<Qname> all, Qname singleInterpreted, boolean abbrev) {
		if (notGiven(all)) return verbatim;

		if (all.size() > 10) {
			if (singleInterpreted != null) return getAreaName(verbatim, singleInterpreted, abbrev);
			return verbatim;
		}

		List<String> names = new ArrayList<>();
		for (Qname id : all) {
			String name = getAreaName(verbatim, id, abbrev);
			if (notGiven(name)) continue;
			if (!names.contains(name)) {
				names.add(name);
			}
		}

		if (notGiven(names)) return verbatim;

		Collections.sort(names);
		return Util.catenate(names, ",");
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private boolean notGiven(String s) {
		return !given(s);
	}

	private boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	private boolean given(Collection<?> c) {
		return c != null && !c.isEmpty();
	}

	private boolean notGiven(Collection<?> c) {
		return !given(c);
	}

	private class ExtendedMunicipalities {
		private boolean fetched = false;
		private Set<Qname> extendedAreaMunicipalities = Collections.emptySet();
		private Set<Qname> extendedMatches = Collections.emptySet();

		private final Coordinates wgs84Coordinates;
		private final Area resolvedMunicipalityByName;
		private final Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames;
		private final boolean originallyYKJSQuare;

		public ExtendedMunicipalities(Coordinates wgs84Coordinates, boolean originallyYKJSQuare, Area resolvedMunicipalityByName, Set<Area> resolvedCurrentMunicipalitiesByOldMunicipalityNames) {
			this.wgs84Coordinates = wgs84Coordinates;
			this.resolvedMunicipalityByName = resolvedMunicipalityByName;
			this.resolvedCurrentMunicipalitiesByOldMunicipalityNames = resolvedCurrentMunicipalitiesByOldMunicipalityNames;
			this.originallyYKJSQuare = originallyYKJSQuare;
		}

		public Set<Qname> getMunicipalities() {
			if (!fetched) fetch();
			return extendedAreaMunicipalities;
		}

		public Set<Qname> getMatches() {
			if (!fetched) fetch();
			return extendedMatches;
		}

		private void fetch() {
			extendedAreaMunicipalities = getExpandedAreaFinnishMunicipalities(wgs84Coordinates, originallyYKJSQuare);
			extendedMatches = resolveMatches(extendedAreaMunicipalities, resolvedMunicipalityByName, resolvedCurrentMunicipalitiesByOldMunicipalityNames);
			fetched = true;
		}
	}

	private boolean isInsideGeneralFinlandArea(Coordinates wgs84Coordinates) {
		return FinlandAreaUtil.isInsideGeneralFinlandArea(wgs84Coordinates);
	}

	private Coordinates getWgs84Coordinates(Coordinates coordinates) throws DataValidationException {
		if (coordinates.getType() == Type.WGS84) return coordinates;
		return CoordinateConverter.convert(coordinates).getWgs84();
	}

	private Geo getWgs84Geo(Geo geo) throws DataValidationException {
		if (geo.getCRS() == Type.WGS84) return geo;
		return CoordinateConverter.convertGeo(geo, Type.WGS84);
	}

	private Coordinates getMetricCoordinates(Coordinates coordinates) {
		if (coordinates.getType() != Type.WGS84) return coordinates;
		try {
			return CoordinateConverter.convert(coordinates).getEuref();
		} catch (DataValidationException e) {
			// This will be handled by Converter
			return null;
		}
	}

	private Coordinates getMetricBoundingBox(Geo geo) {
		try {
			if (geo.getCRS() != Type.WGS84) return geo.getBoundingBox();
			return CoordinateConverter.convertGeo(geo, Type.EUREF).getBoundingBox();
		} catch (DataValidationException e) {
			// This will be handled by Converter
			return null;
		}
	}

	private Coordinates getBoundingBox(Geo geo) {
		try {
			return geo.getBoundingBox();
		} catch (DataValidationException e) {
			// Will be handled by Converter
			return null;
		}
	}

}
