package fi.laji.datawarehouse.etl.models.exceptions;

import fi.laji.datawarehouse.query.model.Base;

public class NoSuchFieldException extends Exception {

	private static final long serialVersionUID = -376250692419994985L;

	public NoSuchFieldException(String message, String fieldName) {
		super(message + ": " + fieldName);
	}

	public NoSuchFieldException(String fieldName, Base base) {
		this("No such field for base " + base, fieldName);
	}

}
