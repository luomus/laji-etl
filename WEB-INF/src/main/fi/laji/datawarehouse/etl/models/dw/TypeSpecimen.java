package fi.laji.datawarehouse.etl.models.dw;

import fi.luomus.commons.containers.rdf.Qname;

public class TypeSpecimen extends Identification implements Comparable<TypeSpecimen> {

	private static final Qname TYPE_STATUS_NOT_A_TYPE = new Qname("MY.typeStatusNo");
	private static final Qname TYPE_VERIFICATION_DOUBTFUL = new Qname("MY.typeVerificationDoubtful");

	private String typif;
	private String typifDate;
	private Qname status;
	private Qname verification;
	private String basionymePublication;
	private String publication;

	public String getTypif() {
		return typif;
	}

	public void setTypif(String typif) {
		this.typif = typif;
	}

	public String getTypifDate() {
		return typifDate;
	}

	public void setTypifDate(String typifDate) {
		this.typifDate = typifDate;
	}

	public Qname getStatus() {
		return status;
	}

	public void setStatus(Qname status) {
		if (given(status)) {
			this.status = status;
		} else {
			this.status = null;
		}
	}

	public Qname getVerification() {
		return verification;
	}

	public void setVerification(Qname verification) {
		if (given(verification)) {
			this.verification = verification;
		} else {
			this.verification = null;
		}
	}

	public String getBasionymePublication() {
		return basionymePublication;
	}

	public void setBasionymePublication(String basionymePublication) {
		this.basionymePublication = basionymePublication;
	}

	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

	public boolean verified() {
		if (TYPE_STATUS_NOT_A_TYPE.equals(status)) return false;
		if (TYPE_VERIFICATION_DOUBTFUL.equals(verification)) return false;
		return true;
	}

	@Override
	public int compareTo(TypeSpecimen o) {
		if (this.verified() && !o.verified()) return -1;
		if (o.verified() && !this.verified()) return 1;
		if (this.getId() == null && o.getId() == null) return 0;
		if (this.getId() != null && o.getId() == null) return -1;
		if (o.getId() != null && this.getId() == null) return 1;
		return this.getId().compareTo(o.getId());
	}

}
