package fi.laji.datawarehouse.etl.models.containers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fi.luomus.commons.containers.rdf.Qname;

public class SourceDocumentLoadStatisticsData {

	public enum Stat { DAY, WEEK, MONTH }

	public static class SourceCount {
		private final Qname sourceId;
		private final int count;
		public SourceCount(Qname sourceId, int count) {
			this.sourceId = sourceId;
			this.count = count;
		}
		public Qname getSourceId() {
			return sourceId;
		}
		public int getCount() {
			return count;
		}
	}

	private final Map<Stat, List<SourceCount>> stats = new TreeMap<>();

	public List<SourceCount> getStat(String stat) {
		Stat statEnum = Stat.valueOf(stat);
		if (stats.containsKey(statEnum)) {
			return stats.get(statEnum);
		}
		return Collections.emptyList();
	}

	public void addStat(Stat stat, Qname sourceId, int count) {
		if (!stats.containsKey(stat)) {
			stats.put(stat, new ArrayList<>());
		}
		stats.get(stat).add(new SourceCount(sourceId, count));
	}

}
