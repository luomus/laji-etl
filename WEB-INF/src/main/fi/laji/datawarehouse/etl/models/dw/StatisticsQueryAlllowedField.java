package fi.laji.datawarehouse.etl.models.dw;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import fi.laji.datawarehouse.query.model.Base;

@Retention(RetentionPolicy.RUNTIME)
public @interface StatisticsQueryAlllowedField {

	Base allowFor() default Base.UNIT;

}
