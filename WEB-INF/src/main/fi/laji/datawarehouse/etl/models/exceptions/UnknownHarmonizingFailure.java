package fi.laji.datawarehouse.etl.models.exceptions;

public class UnknownHarmonizingFailure extends Exception {

	private static final long serialVersionUID = -4832866535122050297L;

	public UnknownHarmonizingFailure(Exception cause) {
		super(cause.getMessage(), cause);
	}

}
