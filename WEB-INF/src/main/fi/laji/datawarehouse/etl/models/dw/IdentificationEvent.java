package fi.laji.datawarehouse.etl.models.dw;

import java.util.Date;

import fi.laji.datawarehouse.etl.models.harmonizers.BaseHarmonizer;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class IdentificationEvent extends Identification implements Comparable<IdentificationEvent> {

	private String det;
	private String detDate;
	private Boolean preferred;

	public String getDet() {
		return det;
	}

	public void setDet(String det) {
		this.det = det;
	}

	public String getDetDate() {
		return detDate;
	}

	public void setDetDate(String detDate) {
		this.detDate = detDate;
	}

	public Boolean getPreferred() {
		return preferred;
	}

	public void setPreferred(Boolean preferred) {
		this.preferred = preferred;
	}

	public boolean preferred() {
		return Boolean.TRUE.equals(preferred);
	}

	public String formattedDetDate() {
		try {
			Date date = BaseHarmonizer.parseDate(detDate);
			if (date == null) {
				if (isInteger(detDate)) {
					return detDate;
				}
				return "0";
			}
			return DateUtils.format(date, "yyyyMMdd");
		} catch (Exception e) {
			return "0";
		}
	}

	private boolean isInteger(String detDate) {
		try {
			Integer.valueOf(detDate);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public int compareTo(IdentificationEvent o) {
		if (this.preferred() && !o.preferred()) return -1;
		if (o.preferred() && !this.preferred()) return 1;
		int c = o.formattedDetDate().compareTo(this.formattedDetDate());
		if (c != 0) return c;
		if (this.getId() == null && o.getId() == null) return 0;
		if (this.getId() != null && o.getId() == null) return -1;
		if (o.getId() != null && this.getId() == null) return 1;
		return this.getId().compareTo(o.getId());
	}

	public IdentificationEvent concealPublicData() {
		IdentificationEvent publicIdentification = this.copy();
		publicIdentification.setNotes(null);
		publicIdentification.setDet(null);
		publicIdentification.setDetDate(null);
		return publicIdentification;
	}

	public IdentificationEvent copy() {
		try {
			JSONObject identificationJSON = ModelToJson.toJson(this);
			IdentificationEvent copy = JsonToModel.identificationEventFromJson(identificationJSON);
			return copy;
		} catch (Exception e) {
			throw new IllegalStateException();
		}
	}

}
