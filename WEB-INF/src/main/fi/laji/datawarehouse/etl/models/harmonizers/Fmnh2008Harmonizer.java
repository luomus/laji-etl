package fi.laji.datawarehouse.etl.models.harmonizers;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class Fmnh2008Harmonizer extends BaseHarmonizer<fi.luomus.commons.xml.Document> {

	private static final String HATIKKA_USERID_PREFIX = "hatikka:";
	private static final Qname HATIKKA_OBSERVATIONS_COLLECTION = new Qname("HR.447");
	public static Map<String, Qname> PROJECT_NAME_TO_COLLECTION_MAP = initProjectNameToCollectionMap();

	private static Map<String, Qname> initProjectNameToCollectionMap() {
		Map<String, Qname> map = new HashMap<>();
		map.put("LTKM:Talvilintulaskenta", 	new Qname("HR.39"));
		map.put("LTKM:NAFI", 				new Qname("HR.175"));
		return map;
	}

	public static Map<String, Method> AREA_CLASS_TO_SETTER_MAP = initAreaClassToSetterMap();

	private static Map<String, Method> initAreaClassToSetterMap() {
		Map<String, Method> map = new HashMap<>();
		try {
			map.put("ContinentOcean", Gathering.class.getMethod("setHigherGeography", String.class));

			map.put("Country", Gathering.class.getMethod("setCountry", String.class));
			map.put("Maa", Gathering.class.getMethod("setCountry", String.class));

			map.put("BiologicalProvince", Gathering.class.getMethod("setBiogeographicalProvince", String.class));
			map.put("Eliömaakunta", Gathering.class.getMethod("setBiogeographicalProvince", String.class));

			map.put("Osavaltio tai maakunta", Gathering.class.getMethod("setProvince", String.class));
			map.put("State or Province", Gathering.class.getMethod("setProvince", String.class));
			map.put("Maakunta", Gathering.class.getMethod("setProvince", String.class));

			map.put("County", Gathering.class.getMethod("setMunicipality", String.class));
			map.put("StateProvince", Gathering.class.getMethod("setMunicipality", String.class));
			map.put("Kunta", Gathering.class.getMethod("setMunicipality", String.class));
		} catch (Exception e) {
			throw new ETLException(e);
		}
		return map;
	}

	@Override
	public List<DwRoot> harmonize(fi.luomus.commons.xml.Document fmnh2008Document, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		Node rootNode = fmnh2008Document.getRootNode();

		DwRoot root = parseDocumentIdFromDocumentNode(rootNode, source);

		try {
			parse(rootNode, root);
		} catch (Exception e) {
			handleException(e);
		}

		return Utils.singleEntryList(root);
	}

	private DwRoot parseDocumentIdFromDocumentNode(Node documentNode, Qname source) throws CriticalParseFailure {
		try {
			if (!documentNode.getName().equals("Document")) throw new CriticalParseFailure("Expected one Document node");

			String documentId = documentNode.getAttribute("documentId");
			if (!given(documentId)) throw new CriticalParseFailure("No document id");

			Qname id = buildDocumentUriIdentifier(source, documentId);
			return new DwRoot(id, source);
		}
		catch (Exception e) {
			throw new CriticalParseFailure("No document id", e);
		}
	}

	private void parse(Node fmnh2008DocumentNode, DwRoot root) throws CriticalParseFailure  {
		Node fmnh2008Dataset = getDataSetNode(fmnh2008DocumentNode);

		Node fmnh2008Gathering = getBaseGatheringFromDatasetOrFromUnits(fmnh2008Dataset);
		root.setCollectionId(parseCollectionId(fmnh2008Gathering));

		Document document = root.createPublicDocument();

		if (fmnh2008DocumentNode.hasAttribute("userId")) {
			String userId = fmnh2008DocumentNode.getAttribute("userId");
			if (userId.startsWith("http://")) {
				document.addEditorUserId(Qname.fromURI(userId).toURI());
			} else if (userId.startsWith("MA.")) {
				document.addEditorUserId(new Qname(userId).toURI());
			} else {
				document.addEditorUserId(HATIKKA_USERID_PREFIX+userId);
			}
		}

		Gathering baseGathering = null;
		if (fmnh2008Gathering.getParent().getName().equals("DataSet")) {
			baseGathering = parseGathering(fmnh2008Gathering, document);
		}

		boolean hadUnitGatherings = false;
		for (Node units : fmnh2008Dataset.getChildNodes("Units")) {
			if (units.hasChildNodes("Gathering")) {
				Node fmnh2008UnitsGathering = units.getNode("Gathering");
				Gathering unitsGathering = parseGathering(fmnh2008UnitsGathering, document);
				if (baseGathering != null) {
					copyFromBaseGatheringToUnitGathering(baseGathering, unitsGathering);
				}
				for (Unit u : parseUnits(units, document)) {
					unitsGathering.addUnit(u);
				}
				document.addGathering(unitsGathering);
				hadUnitGatherings = true;
			} else if (baseGathering != null) {
				for (Unit u : parseUnits(units, document)) {
					baseGathering.addUnit(u);
				}
			} else {
				throw new UnsupportedOperationException("Base gathering does not exist but gathering not defined in units");
			}
		}

		if (baseGathering != null) {
			if (!hadUnitGatherings || !baseGathering.getUnits().isEmpty()) {
				document.addGathering(baseGathering);
			}
		}

		for (Gathering g : document.getGatherings()) {
			for (String editorUserId : document.getEditorUserIds()) {
				if (editorUserId.startsWith(HATIKKA_USERID_PREFIX)) {
					g.addObserverUserId(editorUserId);
				}
			}
		}
	}

	private Node getDataSetNode(Node rootNode) {
		List<Node> datasetsNodes = rootNode.getChildNodes("DataSets");
		if (datasetsNodes.size() != 1) {
			throw new IllegalStateException("Expected one DataSets node, had " + datasetsNodes.size());
		}
		Node fmnh2008Datasets = datasetsNodes.get(0);
		List<Node> datasetNodes = fmnh2008Datasets.getChildNodes("DataSet");
		if (datasetNodes.size() != 1) {
			throw new IllegalStateException("Expected one DataSet node, had " + datasetNodes.size());
		}
		Node fmnh2008Dataset = datasetNodes.get(0);
		return fmnh2008Dataset;
	}

	private ArrayList<Unit> parseUnits(Node fmnh2008Units, Document parentDocument) throws CriticalParseFailure  {
		ArrayList<Unit> units = new ArrayList<>();
		for (Node fmnh2008Unit : fmnh2008Units.getChildNodes("Unit")) {
			Unit unit = parseUnit(fmnh2008Unit, parentDocument);
			units.add(unit);
		}
		return units;
	}

	private void copyFromBaseGatheringToUnitGathering(Gathering baseGathering, Gathering unitsGathering) {
		for (String agent : baseGathering.getTeam()) {
			unitsGathering.addTeamMember(agent);
		}

		if (baseGathering.getNotes() != null) {
			if (unitsGathering.getNotes() == null) {
				unitsGathering.setNotes(baseGathering.getNotes());
			} else {
				unitsGathering.setNotes(unitsGathering.getNotes() + ", " + baseGathering.getNotes());
			}
		}

		for (Fact f : baseGathering.getFacts()) {
			unitsGathering.addFact(f.getFact(), f.getValue());
		}

		if (baseGathering.getEventDate() != null && unitsGathering.getEventDate() == null) {
			unitsGathering.setEventDate(baseGathering.getEventDate());
			try {
				unitsGathering.setHourBegin(baseGathering.getHourBegin());
				unitsGathering.setHourEnd(baseGathering.getHourEnd());
				unitsGathering.setMinutesBegin(baseGathering.getMinutesBegin());
				unitsGathering.setMinutesEnd(baseGathering.getMinutesEnd());
			} catch (DateValidationException e) {
				new IllegalStateException("Already valitated before", e);
			}
		}

		if (baseGathering.getCoordinates() != null && unitsGathering.getCoordinates() == null) {
			unitsGathering.setCoordinates(baseGathering.getCoordinates().copy());
			unitsGathering.setCoordinatesVerbatim(baseGathering.getCoordinatesVerbatim());
		}

		if (baseGathering.getBiogeographicalProvince() != null && unitsGathering.getBiogeographicalProvince() == null) {
			unitsGathering.setBiogeographicalProvince(baseGathering.getBiogeographicalProvince());
		}
		if (baseGathering.getCountry() != null && unitsGathering.getCountry() == null) {
			unitsGathering.setCountry(baseGathering.getCountry());
		}
		if (baseGathering.getHigherGeography() != null && unitsGathering.getHigherGeography() == null) {
			unitsGathering.setHigherGeography(baseGathering.getHigherGeography());
		}
		if (baseGathering.getLocality() != null && unitsGathering.getLocality() == null) {
			unitsGathering.setLocality(baseGathering.getLocality());
		}
		if (baseGathering.getMunicipality() != null && unitsGathering.getMunicipality() == null) {
			unitsGathering.setMunicipality(baseGathering.getMunicipality());
		}
		if (baseGathering.getProvince() != null && unitsGathering.getProvince() == null) {
			unitsGathering.setProvince(baseGathering.getProvince());
		}
		if (unitsGathering.getTaxonCensus().isEmpty()) {
			for (TaxonCensus census : baseGathering.getTaxonCensus()) {
				unitsGathering.addTaxonCensus(census.copy());
			}
		}
	}

	private Unit parseUnit(Node fmnh2008Unit, Document parentDocument) throws CriticalParseFailure {
		Unit unit = new Unit(buildUriIdentifier(parentDocument));
		parseFactsAndNotes(fmnh2008Unit, unit);
		if (fmnh2008Unit.hasChildNodes("RecordBasis")) {
			String recordBasis = fmnh2008Unit.getNode("RecordBasis").getContents();
			unit.setRecordBasis(getRecordBasis(recordBasis));
		}

		Iterator<Fact> i = unit.getFacts().iterator();
		while (i.hasNext()) {
			Fact fact = i.next();
			if (fact.getFact().equals("InformalNameString")) {
				unit.setTaxonVerbatim(fact.getValue());
				i.remove();
			} else if (fact.getFact().equals("Yksilömäärä")) {
				unit.setAbundanceString(fact.getValue());
				i.remove();
			} else if (fact.getFact().equals("AbundanceString")) {
				if (unit.getAbundanceString() == null) {
					unit.setAbundanceString(fact.getValue());
					i.remove();
				}
			}
		}
		return unit;
	}

	private Node getBaseGatheringFromDatasetOrFromUnits(Node fmnh2008Dataset) {
		if (fmnh2008Dataset.hasChildNodes("Gathering")) {
			return fmnh2008Dataset.getNode("Gathering");
		}
		for (Node units : fmnh2008Dataset.getChildNodes("Units")) {
			return units.getNode("Gathering");
		}
		throw new UnsupportedOperationException("Unable to resolve base bathering");
	}

	private Gathering parseGathering(Node fmnh2008Gathering, Document parentDocument) throws CriticalParseFailure  {
		Gathering gathering = new Gathering(buildUriIdentifier(parentDocument));
		parseAgents(fmnh2008Gathering, gathering);
		parseAreas(fmnh2008Gathering, gathering);
		parseFactsAndNotes(fmnh2008Gathering, gathering);
		parseEventDateTime(fmnh2008Gathering, gathering);
		parseCoordinates(fmnh2008Gathering, gathering);

		String gatheringGUID = null;
		if (fmnh2008Gathering.hasChildNodes("GatheringGUID")) {
			gatheringGUID = fmnh2008Gathering.getNode("GatheringGUID").getContents();
			gathering.addFact("GatheringGUID", gatheringGUID);
		}

		taxonCensusFromFacts(gathering);

		return gathering;
	}

	private void parseAreas(Node fmnh2008Gathering, Gathering gathering) {
		StringBuilder locality = new StringBuilder();
		for (Node namedAreas : fmnh2008Gathering.getChildNodes("NamedAreas")) {
			for (Node namedArea : namedAreas.getChildNodes("NamedArea")) {
				String areaClass = namedArea.hasChildNodes("AreaClass") ? namedArea.getNode("AreaClass").getContents() : "Locality";
				String areaName = namedArea.getNode("AreaName").getContents();
				Method m = AREA_CLASS_TO_SETTER_MAP.get(areaClass);
				if (m == null) {
					locality.append(" ").append(areaName).append(",");
				} else {
					try {
						m.invoke(gathering, areaName);
					} catch (Exception e) {
						throw new ETLException(e);
					}
				}
			}
		}
		if (locality.length() > 0) {
			Utils.removeLastChar(locality);
			gathering.setLocality(locality.toString().trim());
		}
	}

	private void parseCoordinates(Node fmnh2008Gathering, Gathering gathering) {
		if (!fmnh2008Gathering.hasChildNodes("SiteCoordinateSets")) return;

		for (Node siteCoordinates : fmnh2008Gathering.getNode("SiteCoordinateSets").getChildNodes("SiteCoordinates")) {
			if (siteCoordinates.hasChildNodes("CoordinatesGrid")) {
				parseCoordinatesGrid(gathering, siteCoordinates);
			} else if (siteCoordinates.hasChildNodes("CoordinatesLatLong")) {
				parseCoordinatesLatLong(gathering, siteCoordinates);
			} else {
				throw new UnsupportedOperationException("Unknown coordinate type: " + siteCoordinates);
			}
		}
	}

	private void parseCoordinatesLatLong(Gathering gathering, Node siteCoordinates) {
		//	      <SiteCoordinateSets>
		//	        <SiteCoordinates>
		//	          <CoordinatesLatLong>

		//	            <LongitudeDecimal>24.824</LongitudeDecimal>
		//	            <LatitudeDecimal>60.273</LatitudeDecimal>
		//
		//              TAI
		//
		//              <CoordinatesText>/+36.664701-004.454441/+36.671999-004.462681/+36.660433-004.471779/+36.656439-004.464397/</CoordinatesText>
		//
		//	          </CoordinatesLatLong>
		//	        </SiteCoordinates>
		//
		//   		<SiteCoordinates>
		//				...
		//        	</SiteCoordinates>
		//
		//	      </SiteCoordinateSets>
		Node coordinatesLatLong = siteCoordinates.getNode("CoordinatesLatLong");
		if (coordinatesLatLong.hasChildNodes("CoordinatesText")) {
			String coordinatesText = coordinatesLatLong.getNode("CoordinatesText").getContents();
			parseCoordinatesTextMulti(gathering, coordinatesText, Type.WGS84);
		} else {
			parseCoordinatesLatLongPoint(gathering, coordinatesLatLong);
		}
	}

	private void parseCoordinatesLatLongPoint(Gathering gathering, Node coordinatesLatLong) {
		double lat = Double.valueOf(coordinatesLatLong.getNode("LatitudeDecimal").getContents());
		double lon = Double.valueOf(coordinatesLatLong.getNode("LongitudeDecimal").getContents());
		String verbatim = lat + ", " + lon + " WGS84";
		setCoordinatesVerbatim(gathering, verbatim);

		Coordinates c;
		try {
			c = new Coordinates(lat, lon, Type.WGS84);
		} catch (DataValidationException e) {
			createCoordinateIssue(gathering, Type.WGS84, e);
			return;
		}

		Geo geo = Geo.getBoundingBox(c);
		setGeo(gathering, geo);
	}

	private void parseCoordinatesTextMulti(Gathering gathering, String coordinatesText, Type type) {
		if (coordinatesText.contains("NaN")) return;
		String verbatim = coordinatesText + " " + type;
		setCoordinatesVerbatim(gathering, verbatim);
		try {
			Geo geo = parseGeoFromFmnh2008CoordinateText(coordinatesText, type);
			if (geo == null) {
				throw new DataValidationException("Invalid coordinates text " + coordinatesText);
			}
			setGeo(gathering, geo);
		} catch (DataValidationException e) {
			createGeoIssue(gathering, e);
		}
	}

	private void setGeo(Gathering gathering, Geo geo) {
		if (gathering.getGeo() == null) {
			gathering.setGeo(geo);
		} else {
			for (Feature f : geo) {
				gathering.getGeo().addFeature(f);
			}
		}
	}

	private void setCoordinatesVerbatim(Gathering gathering, String verbatim) {
		if (gathering.getCoordinatesVerbatim() != null) {
			gathering.setCoordinatesVerbatim(gathering.getCoordinatesVerbatim() + " " + verbatim);
		} else {
			gathering.setCoordinatesVerbatim(verbatim);
		}
	}

	private static Double parseLat(String coordinatesText) {
		// +36.664701-004.454441
		char sign = coordinatesText.charAt(0);
		coordinatesText = coordinatesText.substring(1);
		if (coordinatesText.contains("+")) {
			return Double.valueOf(sign + coordinatesText.split(Pattern.quote("+"))[0]);
		}
		if (coordinatesText.contains("-")) {
			return Double.valueOf(sign + coordinatesText.split(Pattern.quote("-"))[0]);
		}
		return null;
	}

	private static Double parseLon(String coordinatesText) {
		// +36.664701-004.454441
		coordinatesText = coordinatesText.substring(1);
		if (coordinatesText.contains("+")) {
			return Double.valueOf(coordinatesText.split(Pattern.quote("+"))[1]);
		} if (coordinatesText.contains("-")) {
			return Double.valueOf("-" + coordinatesText.split(Pattern.quote("-"))[1]);
		}
		return null;
	}

	private static final Set<String> KNOWN_SYSTEMS = Utils.set("FI KKJ27", "FI YKJ");

	private void parseCoordinatesGrid(Gathering gathering, Node siteCoordinates) {
		//	<SiteCoordinateSets>
		//		<SiteCoordinates>
		//			<CoordinatesGrid>
		//				<GridCellSystem>FI KKJ27</GridCellSystem>
		//
		//				<GridCellCode>673:334</GridCellCode>
		//              TAI
		//				<GridCellCode>673-4:334</GridCellCode>
		//              TAI
		//				<GridCellCode>/6676342:3397130/6676614:3397518/6676510:3398022/6676146:3398226/6675818:3398030/6675746:3397578/6675950:3397242/</GridCellCode>
		//
		//			</CoordinatesGrid>
		//		</SiteCoordinates>
		//
		//   	<SiteCoordinates>
		//			...
		//      </SiteCoordinates>
		//
		//	</SiteCoordinateSets>
		Node coordinatesGrid = siteCoordinates.getNode("CoordinatesGrid");

		String gridText = coordinatesGrid.getNode("GridCellCode").getContents();
		if (gridText.length() < 3) return;
		if (gridText.contains("NaN")) return;

		String system = coordinatesGrid.getNode("GridCellSystem").getContents();
		if (!KNOWN_SYSTEMS.contains(system)) throw new UnsupportedOperationException("Unknown system " + system);

		if (cleanGridText(gridText).contains("/")) {
			parseCoordinatesTextMulti(gathering, gridText, Type.YKJ);
		} else {
			String verbatim = gridText + " " + system;
			setCoordinatesVerbatim(gathering, verbatim);
			Coordinates c;
			gridText = cleanGridText(gridText);
			try {
				c = parseYKJCoordinates(gridText);
				if (c == null) throw new DataValidationException("Invalid YKJ grid " + gridText);
			} catch (DataValidationException e) {
				createCoordinateIssue(gathering, Type.YKJ, e);
				return;
			}
			Geo geo = Geo.getBoundingBox(c);
			setGeo(gathering, geo);
		}
	}

	private static String cleanGridText(String gridText) {
		String cleaned = gridText;
		if (cleaned.startsWith("/")) cleaned = cleaned.substring(1);
		if (cleaned.endsWith("/")) cleaned = cleaned.substring(0, cleaned.length()-1);
		return cleaned;
	}

	public static Geo parseGeoFromFmnh2008CoordinateText(String text, Type type) throws DataValidationException {
		// If no front dash this is a line; if has front dash this is a polygon
		// 6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/  -> LINE
		// /6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/ -> POLYGON
		// /+36.664701-004.454441/+36.671999-004.462681/+36.660433-004.471779/+36.656439-004.464397/ -> POLYGON (wgs84)
		boolean shouldBePolygon = text.startsWith("/");
		text = cleanGridText(text);

		List<Point> points = parsePoints(text);
		if (points.isEmpty()) return null;

		Feature feature = null;
		try {
			feature = parseFeature(points, shouldBePolygon);
		} catch (Exception e) {
			if (shouldBePolygon) {
				feature = parseFeature(points, false); // store as line
			} else {
				throw e;
			}
		}
		return new Geo(type).addFeature(feature);
	}

	private static Feature parseFeature(List<Point> points, boolean shouldBePolygon) throws DataValidationException {
		if (points.size() == 1) return points.get(0).validate(); // Point
		if (points.size() == 2) return Line.from(points).validate(); // Even if supposed to be polygon
		return shouldBePolygon ? Polygon.from(points).validate() : Line.from(points).validate();
	}

	private static List<Point> parsePoints(String text) {
		List<Point> points = new ArrayList<>();
		for (String point : text.split(Pattern.quote("/"))) {
			Point p = parsePoint(point);
			if (p == null) continue;
			points.add(p);
		}
		return points;
	}

	private static Point parsePoint(String point) {
		// 6833008:3636592 or +36.664701-004.454441
		if (point.contains(":")) {
			String[] parts = point.split(Pattern.quote(":"));
			int lat = Integer.valueOf(parts[0]);
			int lon = Integer.valueOf(parts[1]);
			return Point.from(lat, lon);
		}
		Double lat = parseLat(point);
		Double lon = parseLon(point);
		if (lat == null || lon == null) return null;
		return Point.from(lat, lon);
	}

	private void parseEventDateTime(Node fmnh2008Gathering, Gathering gathering) {
		//		<Gathering>
		//	      <DateTime>
		//	        <DateText>1995-02-25T09:00/14:00</DateText>
		//	      </DateTime>
		//		</Gathering>
		if (fmnh2008Gathering.hasChildNodes("DateTime")) {
			parseEventDateTime(fmnh2008Gathering.getNode("DateTime").getNode("DateText").getContents(), gathering);
		}

	}

	private void parseFactsAndNotes(Node fmnh2008Node, BaseModel model) {
		List<String> notes = new ArrayList<>();
		for (Node siteMeasurementOrFact : fmnh2008Node.getChildNodes("SiteMeasurementsOrFacts")) {
			for (Node measurementOrFact : siteMeasurementOrFact.getChildNodes("SiteMeasurementOrFact")) {
				String note = parseFact(model, measurementOrFact);
				if (note != null) notes.add(note);
			}
		}
		for (Node unitMeasurementOrFact : fmnh2008Node.getChildNodes("MeasurementsOrFacts")) {
			for (Node measurementOrFact : unitMeasurementOrFact.getChildNodes("MeasurementOrFact")) {
				String note = parseFact(model, measurementOrFact);
				if (note != null) notes.add(note);
			}
		}
		parseNotes(model, notes);
	}

	private void parseNotes(BaseModel model, List<String> notes) {
		if (!notes.isEmpty()) {
			StringBuilder b = new StringBuilder();
			Iterator<String> i = notes.iterator();
			while (i.hasNext()) {
				String note = i.next();
				b.append(note);
				if (i.hasNext()) b.append(", ");
			}
			model.setNotes(b.toString());
		}
	}

	private static final Set<String> NOTE_FIELDS = Utils.set("Notes", "Kommentti", "Caption");

	private String parseFact(BaseModel model, Node measurementOrFact) {
		String note = null;
		for (Node measurementOrFactAtomised : measurementOrFact.getChildNodes("MeasurementOrFactAtomised")) {
			String fact = measurementOrFactAtomised.getNode("Parameter").getContents();
			String value = measurementOrFactAtomised.getNode("LowerValue").getContents();
			if (NOTE_FIELDS.contains(fact)) {
				note = value;
			} else {
				model.addFact(fact, value);
			}
		}
		return note;
	}

	private void parseAgents(Node fmnh2008Gathering, Gathering gathering) {
		//	  <Agents>
		//        <GatheringAgent>
		//          <Person>
		//            <FullName>Liukkonen, Antero</FullName>
		//          </Person>
		//        </GatheringAgent>
		//      </Agents>
		for (Node agents : fmnh2008Gathering.getChildNodes("Agents")) {
			for (Node agent : agents.getChildNodes("GatheringAgent")) {
				for (Node person : agent.getChildNodes("Person")) {
					if (person.hasChildNodes("FullName")) {
						gathering.addTeamMember(person.getNode("FullName").getContents());
					}
				}
			}
		}
	}

	private Qname parseCollectionId(Node fmnh2008Gathering) throws CriticalParseFailure {
		if (!fmnh2008Gathering.hasChildNodes("Project")) {
			return HATIKKA_OBSERVATIONS_COLLECTION;
		}
		String projectTitle = fmnh2008Gathering.getNode("Project").getNode("ProjectTitle").getContents();
		if (projectTitle.startsWith(HTTP)) {
			return Qname.fromURI(projectTitle);
		}
		if (projectTitle.startsWith("LTKM:")) {
			Qname collectionId = PROJECT_NAME_TO_COLLECTION_MAP.get(projectTitle);
			if (collectionId == null) throw new CriticalParseFailure("Unknown project: " + projectTitle);
			return collectionId;
		}
		return HATIKKA_OBSERVATIONS_COLLECTION;
	}

}
