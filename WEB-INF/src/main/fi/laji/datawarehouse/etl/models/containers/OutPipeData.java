package fi.laji.datawarehouse.etl.models.containers;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import fi.luomus.commons.containers.rdf.Qname;

public class OutPipeData extends PipeData {

	private final Qname documentId;
	private final long inPipeId;
	private Collection<Qname> splittedDocumentIds;
	private final boolean deletion;
	private final Qname collectionId;
	private final Qname namedPlaceId;

	public OutPipeData(long id, long inPipeId, Qname source, Qname documentId, Qname collectionId, Qname namedPlaceId, String data, boolean deletion) {
		this(id, inPipeId, source, documentId, collectionId, namedPlaceId, data, deletion, null, null);
	}

	public OutPipeData(long id, long inPipeId, Qname source, Qname documentId, Qname collectionId, Qname namedPlaceId, String data, boolean deletion, Date timestamp, String errorMessage) {
		super(id, source, data, timestamp, errorMessage);
		this.documentId = documentId;
		this.inPipeId = inPipeId;
		this.collectionId = collectionId;
		this.namedPlaceId = namedPlaceId;
		this.deletion = deletion;
	}

	public Qname getDocumentId() {
		return documentId;
	}

	public long getInPipeId() {
		return inPipeId;
	}

	public Collection<Qname> getSplittedDocumentIds() {
		if (splittedDocumentIds == null) return Collections.emptyList();
		return splittedDocumentIds;
	}

	public void setSplittedDocumentIds(Collection<Qname> splittedDocumentIds) {
		this.splittedDocumentIds = splittedDocumentIds;
	}

	public OutPipeData setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
		return this;
	}

	public boolean isDeletion() {
		return deletion;
	}

	public Qname getCollectionId() {
		return collectionId;
	}

	public Qname getNamedPlaceId() {
		return namedPlaceId;
	}

	@Override
	public String toString() {
		return "OutPipeData [documentId=" + documentId + ", inPipeId=" + inPipeId + ", splittedDocumentIds=" + splittedDocumentIds + ", deletion=" + deletion + ", collectionId="
				+ collectionId + ", namedPlaceId=" + namedPlaceId + "] " + super.toString();
	}

}
