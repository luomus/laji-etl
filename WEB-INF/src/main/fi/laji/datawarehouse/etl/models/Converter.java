package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fi.laji.DegreePoint;
import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.harmonizers.BaseHarmonizer;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class Converter {

	private static final String CARCASS_KEYWORD = "carcass";
	private static final Qname CARCASS_TYPE = new Qname("MM.typeEnumCarcass");
	private static final String NON_PUBLIC_KEYWORD = "non-public";
	private static final Qname NON_PUBLIC_TYPE = new Qname("MM.typeEnumNonPublic");
	public static final String ROUTE_LENGTH_FACT = new Qname("WBC.routeLength").toURI();
	private static final long SQUARE_METERS_TO_SQUARE_KILOMETERS_RATIO = 1000000;
	private static final long MAX_ALLOWED_BOUNDING_BOX_AREA = 150*150*SQUARE_METERS_TO_SQUARE_KILOMETERS_RATIO; // 150 km x 150 km
	private static final long MAX_ALLOWED_BOUNDING_BOX_DIAMETER = 150000; // 150 km

	private final DAO dao;

	public Converter(DAO dao) {
		this.dao = dao;
	}

	public void convert(DwRoot dwRoot) {
		Document publicDocument = dwRoot.getPublicDocument();
		Document privateDocument = dwRoot.getPrivateDocument();
		if (publicDocument != null) {
			convert(publicDocument);
		}
		if (privateDocument != null) {
			convert(privateDocument);
		}
		if (dwRoot.hasSplittedPublicDocuments()) {
			for (Document splitted : dwRoot.getSplittedPublicDocuments()) {
				convert(splitted);
			}
		}
	}

	public void convert(Document document) {
		if (document.getGatherings() == null) return;
		boolean hideMediaPersonInfo = shouldHideMediaPersonInfo(document);
		for (Gathering gathering : document.getGatherings()) {
			convert(gathering, hideMediaPersonInfo);
		}
		convertMedia(document.getMedia(), hideMediaPersonInfo);
		setUnitQualityValues(document);
	}

	private boolean shouldHideMediaPersonInfo(Document document) {
		return document.isPublic() && (
				document.getSecureReasons().contains(SecureReason.USER_HIDDEN)
				||
				document.getSecureReasons().contains(SecureReason.USER_PERSON_NAMES_HIDDEN));
	}

	private void setUnitQualityValues(Document document) {
		for (Gathering gathering : document.getGatherings()) {
			for (Unit unit : gathering.getUnits()) {
				UnitQuality unitQuality = unit.createQuality();
				unitQuality.setDocumentGatheringUnitQualityIssues(Quality.hasIssues(document, gathering, unit));
			}
		}
	}

	private void convert(Gathering gathering, boolean hideMediaPersonInfo) {
		gathering.createConversions();

		boolean hasGeo = convertGeo(gathering);
		if (hasGeo) {
			convertCoordinates(gathering, getYkjCoordinates(gathering));
			convertBirdAssociationArea(gathering);
		}

		convertDays(gathering);

		convertMedia(gathering.getMedia(), hideMediaPersonInfo);

		for (Unit u : gathering.getUnits()) {
			convertMedia(u.getMedia(), hideMediaPersonInfo);
		}
	}

	private void convertBirdAssociationArea(Gathering gathering) {
		if (gathering.getConversions() == null) return;
		SingleCoordinates ykjGrid = gathering.getConversions().getYkj10kmCenter();
		if (ykjGrid == null) return;
		gathering.getConversions().setBirdAssociationArea(
				dao.getBirdAssociationArea(
						ykjGrid.getLat().intValue()+":"+ykjGrid.getLon().intValue()));
	}

	private Coordinates getYkjCoordinates(Gathering gathering) {
		if (gathering.getInterpretations() == null) return null;
		if (gathering.getInterpretations().getCoordinates() == null) return null;
		Coordinates c = gathering.getInterpretations().getCoordinates();
		if (c.getType() != Type.YKJ) return null;
		return c;
	}

	private boolean convertGeo(Gathering gathering) {
		Geo geo = gathering.getGeo();

		if (geo == null) {
			if (gathering.getInterpretations() != null && gathering.getInterpretations().getCoordinates() != null) {
				Coordinates coordinates = gathering.getInterpretations().getCoordinates();
				geo = Geo.getBoundingBox(coordinates);
				geo.setAccuracyInMeters(coordinates.getAccuracyInMeters());
			}
		}

		gathering.setGeo(null);

		if (geo == null) return false;

		Qname country = gathering.getInterpretations() != null ? gathering.getInterpretations().getCountry() : null;
		GatheringConversions conversions = gathering.getConversions();

		if (foreignReportedAsWgs84(country, geo.getCRS())) {
			// No need to do conversions
			conversions.setWgs84Geo(geo);
			return true;
		}

		try {
			if (geo.getCRS() == Type.WGS84) {
				conversions.setWgs84Geo(geo);
				conversions.setYkjGeo(CoordinateConverter.convertGeo(geo, Type.YKJ));
				conversions.setEurefGeo(CoordinateConverter.convertGeo(geo, Type.EUREF));
			} else if (geo.getCRS() == Type.YKJ) {
				conversions.setYkjGeo(geo);
				conversions.setEurefGeo(CoordinateConverter.convertGeo(geo, Type.EUREF));
				conversions.setWgs84Geo(CoordinateConverter.convertGeo(geo, Type.WGS84));
			} else if (geo.getCRS() == Type.EUREF) {
				conversions.setEurefGeo(geo);
				conversions.setYkjGeo(CoordinateConverter.convertGeo(geo, Type.YKJ));
				conversions.setWgs84Geo(CoordinateConverter.convertGeo(geo, Type.WGS84));
			}
			lineLength(gathering, conversions);
		} catch (DataValidationException e) {
			gathering.createQuality().setLocationIssue(new Quality(Quality.Issue.INVALID_GEO, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
			conversions.setYkjGeo(null);
			conversions.setEurefGeo(null);
			conversions.setWgs84Geo(null);
			return false;
		}
		return true;
	}

	private void lineLength(Gathering gathering, GatheringConversions conversions) {
		Integer factLineLengthInMeters = gathering.factIntValue(ROUTE_LENGTH_FACT);
		if (factLineLengthInMeters != null) {
			conversions.setLinelengthInMeters(factLineLengthInMeters);
		} else {
			Integer calculatedLinelengthInMeters = calculateLineLength(conversions.getEurefGeo());
			conversions.setLinelengthInMeters(calculatedLinelengthInMeters);
		}
	}

	private void convertCoordinates(Gathering gathering, Coordinates originalYkjCoordinates) {
		GatheringConversions conversions = gathering.getConversions();
		conversions.setWgs84(getBoundingBox(conversions.getWgs84Geo(), gathering));
		conversions.setEuref(getBoundingBox(conversions.getEurefGeo(), gathering));
		conversions.setYkj(getBoundingBox(conversions.getYkjGeo(), gathering));

		Qname country = gathering.getInterpretations() != null ? gathering.getInterpretations().getCountry() : null;

		if (fromFinland(country)) {
			if (conversions.getEuref() == null || conversions.getYkj() == null) {
				gathering.createQuality().setLocationIssue(new Quality(Quality.Issue.INVALID_YKJ_COORDINATES, Quality.Source.AUTOMATED_FINBIF_VALIDATION, "Place is in Finland but YKJ coordinates can not be resolved. (Too large area?)"));
				return;
			}
			validateTooLargeArea(gathering);
			if (originalYkjCoordinates != null && originalYkjCoordinates.checkIfIsYkjGrid()) {
				try {
					SingleCoordinates ykjCenterPoint = originalYkjCoordinates.toFullYkjLength().convertCenterPoint();
					DegreePoint wgs84CenterPoint = CoordinateConverter.convert(ykjCenterPoint).getWgs84();
					conversions.setWgs84CenterPoint(new SingleCoordinates(wgs84CenterPoint.getLat(), wgs84CenterPoint.getLon(), Type.WGS84));
				} catch (DataValidationException e) {
					gathering.createQuality().setLocationIssue(new Quality(Quality.Issue.INVALID_GEO, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
					clearCoordinates(conversions);
				}
			}
		}
	}

	private Coordinates getBoundingBox(Geo geo, Gathering gathering) {
		if (geo == null) return null;
		try {
			Coordinates bbox = geo.getBoundingBox();
			if (gathering.getInterpretations() != null) {
				bbox.setAccuracyInMeters(gathering.getInterpretations().getCoordinateAccuracy());
			}
			return bbox;
		} catch (DataValidationException e) {
			createGeoIssue(gathering, geo, e);
			return null;
		}
	}

	private void validateTooLargeArea(Gathering gathering) {
		GatheringInterpretations interpretations = gathering.getInterpretations();
		GatheringConversions conversions = gathering.getConversions();
		if (interpretations == null) return;
		if (!isFromOriginalData(interpretations.getSourceOfCoordinates())) return;
		if (tooLargeAreaInSquareMeters(conversions.getBoundingBoxAreaInSquareMeters())) {
			gathering.createQuality().setLocationIssue(new Quality(
					Quality.Issue.TOO_LARGE_AREA,
					Quality.Source.AUTOMATED_FINBIF_VALIDATION,
					"Too large area " + (conversions.getBoundingBoxAreaInSquareMeters() / SQUARE_METERS_TO_SQUARE_KILOMETERS_RATIO) + " km^2"));
			clearCoordinates(conversions);
			return;
		}
		Coordinates euref = conversions.getEuref();
		double height = euref.getLatMax() - euref.getLatMin();
		double width = euref.getLonMax() - euref.getLonMin();
		if (tooLargeDiameter(height)) {
			gathering.createQuality().setLocationIssue(new Quality(
					Quality.Issue.TOO_LARGE_AREA,
					Quality.Source.AUTOMATED_FINBIF_VALIDATION,
					"Too large area, height " + ((int)height)));
			clearCoordinates(conversions);
			return;
		}
		if (tooLargeDiameter(width)) {
			gathering.createQuality().setLocationIssue(new Quality(
					Quality.Issue.TOO_LARGE_AREA,
					Quality.Source.AUTOMATED_FINBIF_VALIDATION,
					"Too large area, width " + ((int)width)));
			clearCoordinates(conversions);
			return;
		}
	}

	private void clearCoordinates(GatheringConversions conversions) {
		conversions.setYkjGeo(null);
		conversions.setEurefGeo(null);
		conversions.setWgs84Geo(null);
		conversions.setWgs84(null);
		conversions.setEuref(null);
		conversions.setYkj(null);
	}

	private boolean tooLargeAreaInSquareMeters(long boundingBoxArea) {
		return boundingBoxArea > MAX_ALLOWED_BOUNDING_BOX_AREA;
	}

	private boolean tooLargeDiameter(double diameter) {
		return diameter > MAX_ALLOWED_BOUNDING_BOX_DIAMETER;
	}

	private boolean isFromOriginalData(GeoSource sourceOfCoordinates) {
		return sourceOfCoordinates == GeoSource.COORDINATES || sourceOfCoordinates == GeoSource.REPORTED_VALUE;
	}

	private void convertDays(Gathering gathering) {
		GatheringConversions conversions = gathering.getConversions();

		DateRange range = gathering.getEventDate();
		if (range == null) return;

		try {
			range.validate();
		} catch (DateValidationException e) {
			BaseHarmonizer.createTimeIssue(gathering, e);
		}

		Date beginDate = range.getBegin();
		Date endDate = range.getEnd();
		if (beginDate == null) return;
		if (endDate == null) endDate = beginDate;

		DateValue begin = DateUtils.convertToDateValue(beginDate);
		DateValue end = DateUtils.convertToDateValue(endDate);

		century(conversions, begin, end);
		decade(conversions, begin, end);
		year(conversions, begin, end);
		month(conversions, begin, end);
		day(conversions, begin, end);
		if (end.getYearAsInt() - begin.getYearAsInt() <= 1) {
			conversions.setSeasonBegin(season(begin));
			conversions.setSeasonEnd(season(end));
			conversions.setDayOfYearBegin(dayOfYear(begin));
			conversions.setDayOfYearEnd(dayOfYear(end));
			if (end.getYearAsInt() != begin.getYearAsInt()) {
				if (conversions.getSeasonBegin() <= conversions.getSeasonEnd()) {
					conversions.setSeasonBegin(null);
					conversions.setSeasonEnd(null);
					conversions.setDayOfYearBegin(null);
					conversions.setDayOfYearEnd(null);
				}
			}
		}
	}

	private Integer dayOfYear(DateValue date) {
		return DateUtils.getDayOfYear(date);
	}

	public static Integer season(DateValue date) {
		String month = date.getMonth();
		String day = date.getDay();
		if (day.length() < 2) day = "0" + day;
		return Integer.valueOf(month + day);
	}

	private void day(GatheringConversions conversions, DateValue start, DateValue end) {
		if (start.getDayAsInt() == end.getDayAsInt()) {
			conversions.setDay(start.getDayAsInt());
		}
	}

	private void month(GatheringConversions conversions, DateValue start, DateValue end) {
		if (start.getMonthAsInt() == end.getMonthAsInt()) {
			conversions.setMonth(start.getMonthAsInt());
		}
	}

	private void year(GatheringConversions conversions, DateValue start, DateValue end) {
		if (start.getYearAsInt() == end.getYearAsInt()) {
			conversions.setYear(start.getYearAsInt());
		}
	}

	private void century(GatheringConversions conversions, DateValue start, DateValue end) {
		int centuryStart = century(start);
		int centuryEnd = century(end);
		if (centuryStart == centuryEnd) {
			conversions.setCentury(centuryStart);
		}
	}

	public static int decade(DateValue date) {
		return (date.getYearAsInt() / 10) * 10;
	}

	private void decade(GatheringConversions conversions, DateValue start, DateValue end) {
		int decadeStart = decade(start);
		int decadeEnd = decade(end);
		if (decadeStart == decadeEnd) {
			conversions.setDecade(decadeStart);
		}
	}

	public static int century(DateValue date) {
		return (date.getYearAsInt() / 100) * 100;
	}

	private Integer calculateLineLength(Geo eurefGeo) {
		Double linelength = null;
		for (Feature f : eurefGeo) {
			if (f.getClass().equals(Line.class)) {
				double length = ((Line) f).getLength();
				if (linelength == null) {
					linelength = length;
				} else {
					linelength += length;
				}
			}
		}
		if (linelength == null) return null;
		return linelength.intValue();
	}

	public static void createCoordinateIssue(Gathering gathering, Coordinates coordinates, Exception e) {
		createCoordinateIssue(gathering, coordinates, e.getMessage());
	}

	public static void createCoordinateIssue(Gathering gathering, Coordinates coordinates, String message) {
		Quality.Issue issue = issueByType(coordinates.getType());
		gathering.createQuality().setLocationIssue(new Quality(issue, Quality.Source.AUTOMATED_FINBIF_VALIDATION, message));
	}

	public static void createGeoIssue(Gathering gathering, Geo geo, Exception e) {
		createGeoIssue(gathering, geo, e.getMessage());
	}

	public static void createGeoIssue(Gathering gathering, Geo geo, String message) {
		Quality.Issue issue = issueByType(geo.getCRS());
		gathering.createQuality().setLocationIssue(new Quality(issue, Quality.Source.AUTOMATED_FINBIF_VALIDATION, message));
	}

	private static Issue issueByType(Type type) {
		if (type == Type.YKJ) return Quality.Issue.INVALID_YKJ_COORDINATES;
		if (type == Type.EUREF) return Quality.Issue.INVALID_EUREF_COORDINATES;
		if (type == Type.WGS84) return Quality.Issue.INVALID_WGS84_COORDINATES;
		throw new UnsupportedOperationException("Unknown coordinate type " + type);
	}

	private boolean foreignReportedAsWgs84(Qname country, Type type) {
		return type == Type.WGS84 && !fromFinland(country);
	}

	private void convertMedia(List<MediaObject> media, boolean hideMediaPersonInfo) {
		if (media == null) return;
		if (media.isEmpty()) return;
		List<MediaObject> convertedMedia = new ArrayList<>();
		for (MediaObject m : media) {
			MediaObject converted = convertMedia(m, hideMediaPersonInfo);
			if (converted != null) convertedMedia.add(converted);
		}
		media.clear();
		media.addAll(convertedMedia);
	}

	private MediaObject convertMedia(MediaObject m, boolean hideMediaPersonInfo) {
		if (onlyHasIdentifier(m)) {
			m = getDetailedInformationBasedOnUriIdentifier(m);
			if (m == null) return null;
		}
		if (!m.hasMediaURLsSet()) return null;
		if (isCarcass(m) || isNonPublicMedia(m)) return null; // Hide carcass images because we don't want to show them publicly
		if (hideMediaPersonInfo) {
			m.setAuthor(null);
			m.setCopyrightOwner(null);
		}
		return m;
	}

	private boolean onlyHasIdentifier(MediaObject m) {
		if (m.getId() != null && !m.hasMediaURLsSet()) return true;
		if (m.getFullURL() != null && m.getFullURL().startsWith("http://tun.fi")) return true;
		return false;
	}

	private boolean isCarcass(MediaObject m) {
		return CARCASS_TYPE.equals(m.getType()) || m.getKeywords().contains(CARCASS_KEYWORD);
	}
	
	private boolean isNonPublicMedia(MediaObject m) {
		return NON_PUBLIC_TYPE.equals(m.getType()) || m.getKeywords().contains(NON_PUBLIC_KEYWORD);
	}

	private MediaObject getDetailedInformationBasedOnUriIdentifier(MediaObject m) {
		try {
			Qname id = m.getId();
			if (id == null) id = Qname.fromURI(m.getFullURL());
			return dao.getMediaObject(id);
		} catch (Exception e) {
			throw new ETLException("Failed to load image info, " + m.getId(), e);
		}
	}

	private boolean fromFinland(Qname country) {
		return Const.FINLAND.equals(country);
	}

}
