package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.Pass;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class InterpreterForQualityControl {

	private static final Qname INATURALIST = new Qname("KE.901");
	private static final EnumToProperty ENUM_TO_PROPERTY = EnumToProperty.getInstance();

	private final DAO dao;
	private final AutomatedTaxonValidator taxonValidator;
	private final Interpreter interpreter;

	public InterpreterForQualityControl(DAO dao, Interpreter interpreter, AutomatedTaxonValidator taxonValidator) {
		this.dao = dao;
		this.interpreter = interpreter;
		this.taxonValidator = taxonValidator;
	}

	public static CollectionQuality collectionQuality(Document document, DAO dao) {
		CollectionMetadata collectionMetadata = dao.getCollections().get(document.getCollectionId().toURI());
		if (collectionMetadata != null) {
			Qname collectionQualityId = collectionMetadata.getCollectionQuality();
			if (collectionQualityId != null) {
				CollectionQuality collectionQuality = (CollectionQuality) ENUM_TO_PROPERTY.get(collectionMetadata.getCollectionQuality());
				if (collectionQuality != null) return collectionQuality;
			}
		}
		return CollectionQuality.AMATEUR;
	}

	public void interpret(Unit unit, Gathering gathering, Document document) {
		boolean hasIssues = Quality.hasIssues(document, gathering, unit);
		Set<Tag> effectiveTags = new HashSet<>();
		effectiveTags.addAll(maybeNull(unit.getSourceTags()));
		effectiveTags.addAll(maybeNull(document.getSourceTags()));
		Set<Qname> userAnnotatedTaxonIds = new HashSet<>();
		Qname expertAnnotatedTaxonId = null;
		Qname reportedTaxonId = getTaxonId(unit, gathering);
		Coordinates wgs84Coordinates = getWgs84Coordinates(gathering);
		SingleCoordinates wgs84CenterPoint = wgs84Coordinates == null ? null : wgs84Coordinates.convertCenterPoint();
		Qname annotatedAtlasCode = null;

		for (Annotation a : unit.getAnnotations()) {
			if (a.isDeleted()) {
				a.setValid(false); // deleted annotation should not have any effect
				continue;
			}
			Qname thisExpertAnnotatedTaxonId = expertAnnotatedTaxonId(a);
			boolean unknownExpertAnnotationTaxon = expertIdentification(a) && thisExpertAnnotatedTaxonId == null && hasTaxon(a);
			Qname userAnnotatedTaxonId = userAnnotatedTaxonId(a);
			if (changedSignificantly(a.getOccurrenceAtTimeOfAnnotation(), gathering, wgs84CenterPoint, reportedTaxonId, thisExpertAnnotatedTaxonId, userAnnotatedTaxonId)) {
				effectiveTags.add(Tag.CHANGED_DW_AUTO);
				AnnotationType type = a.resolveType();
				if (type == AnnotationType.ADMIN) {
					// spam, coarse annotations wanted to be effective always even if original occurrence has changed
				} else if  (type == AnnotationType.COMMENT) {
					a.setValid(true); // no point invalidating annotations that are just comments
					continue; // but no point proceeding with the bellow
				} else {
					a.setValid(false); // annotation that was done to significantly different data than what is in the occurrence now and should not have any effect
					continue;
				}
			}
			a.setValid(true);
			if (thisExpertAnnotatedTaxonId != null) expertAnnotatedTaxonId = thisExpertAnnotatedTaxonId; // latest expert annotated taxon id has effect
			if (userAnnotatedTaxonId != null) userAnnotatedTaxonIds.add(userAnnotatedTaxonId);

			List<Tag> tagsToAdd = new ArrayList<>(a.getAddedTags());
			if (unknownExpertAnnotationTaxon) {
				tagsToAdd.remove(Tag.EXPERT_TAG_VERIFIED);
				tagsToAdd.remove(Tag.EXPERT_TAG_ERRONEOUS);
				tagsToAdd.remove(Tag.EXPERT_TAG_UNCERTAIN);
			}

			effectiveTags.addAll(tagsToAdd);
			effectiveTags.removeAll(a.getRemovedTags());
			if (a.getAtlasCode() != null) {
				annotatedAtlasCode = a.getAtlasCode();
			}
		}

		Qname annotatedTaxonId = resolveAnnotatedTaxonId(expertAnnotatedTaxonId, reportedTaxonId, userAnnotatedTaxonIds);

		UnitInterpretations i = unit.getInterpretations();
		i.setAnnotatedTaxonId(annotatedTaxonId);

		Qname linkedTaxon = annotatedTaxonId != null ? annotatedTaxonId : reportedTaxonId;

		effectiveTags.addAll(taxonValidationRules(linkedTaxon, gathering, unit, wgs84Coordinates));

		setSortedEffectiveTags(effectiveTags, i);

		i.setNeedsCheck(needsCheck(effectiveTags));

		RecordQuality recordQuality = recordQuality(effectiveTags, reportedTaxonId, userAnnotatedTaxonIds, unit.getReportedTaxonConfidence(), hasIssues);
		i.setRecordQuality(recordQuality);
		i.setRecordQualityNumeric(recordQualityNumeric(recordQuality));
		i.setReliability(reliability(recordQuality));

		i.setCollectionAndRecordQuality(collectionAndRecordQuality(document, recordQuality));

		i.setNeedsIdentification(needsIdentification(effectiveTags, linkedTaxon, i.getReliability(), unit.getReportedTaxonConfidence(), isInaturalist(document)));

		if (recordQuality == RecordQuality.ERRONEOUS) {
			UnitQuality quality = unit.createQuality();
			if (quality.getIssue() == null) {
				quality.setIssue(new Quality(Issue.REPORTED_UNRELIABLE, Source.QUALITY_CONTROL));
			}
		}

		if (annotatedAtlasCode != null) {
			unit.setAtlasCodeUsingQname(annotatedAtlasCode);
		}
	}

	private String collectionAndRecordQuality(Document document, RecordQuality recordQuality) {
		return collectionQuality(document, dao)+":"+recordQuality;
	}

	private Collection<Tag> taxonValidationRules(Qname linkedTaxon, Gathering gathering, Unit unit, Coordinates wgs84Coordinates) {
		if (!shouldValidate(linkedTaxon, gathering, unit)) return Collections.emptyList();

		Pass periodPass = taxonValidator.passesPeriodCheck(linkedTaxon, unit.getLifeStage(), gathering.getEventDate());
		Pass distributionPass = taxonValidator.passesDistributionCheck(linkedTaxon, gathering.getEventDate(), wgs84Coordinates);

		if (periodPass == Pass.NOT_VALIDATED && distributionPass == Pass.NOT_VALIDATED) return Collections.emptyList();

		List<Tag> list = new ArrayList<>();
		if (periodPass != Pass.FALSE && distributionPass != Pass.FALSE) list.add(Tag.AUTO_VALIDATIONS_PASS);
		if (periodPass == Pass.FALSE) list.add(Tag.AUTO_PERIOD_CHECK);
		if (distributionPass == Pass.FALSE) list.add(Tag.AUTO_DISTRIBUTION_CHECK);
		return list;
	}

	private boolean shouldValidate(Qname linkedTaxon, Gathering gathering, Unit unit) {
		if (linkedTaxon == null || gathering.getEventDate() == null) return false;
		if (gathering.getInterpretations() == null) return false;
		if (!Const.FINLAND.equals(gathering.getInterpretations().getCountry())) return false;
		if (Boolean.FALSE.equals(interpreter.isWild(linkedTaxon, unit))) {
			return false;
		}
		return true;
	}

	private boolean isInaturalist(Document document) {
		return document.getSourceId().equals(INATURALIST);
	}

	private void setSortedEffectiveTags(Set<Tag> effectiveTags, UnitInterpretations i) {
		if (effectiveTags.isEmpty()) {
			i.setEffectiveTags(new ArrayList<>());
		} else {
			ArrayList<Tag> list = new ArrayList<>(effectiveTags);
			Collections.sort(list);
			i.setEffectiveTags(list);
		}
	}

	private Qname resolveAnnotatedTaxonId(Qname expertAnnotatedTaxonId, Qname reportedTaxonId, Set<Qname> userAnnotatedTaxonIds) {
		if (expertAnnotatedTaxonId != null) {
			if (expertAnnotatedTaxonId.equals(reportedTaxonId)) return null;
			return expertAnnotatedTaxonId;
		}
		if (reportedTaxonId != null) return null;
		if (userAnnotatedTaxonIds.size() == 1) {
			return userAnnotatedTaxonIds.iterator().next();
		}
		return null;
	}

	private Collection<Tag> maybeNull(List<Tag> sourceTags) {
		if (sourceTags == null) return Collections.emptyList();
		return sourceTags;
	}

	private Boolean needsIdentification(Set<Tag> effectiveTags, Qname linkedTaxon, Reliability reliability, TaxonConfidence reportedTaxonConfidence, boolean isInaturalist) {
		if (effectiveTags.contains(Tag.CHECKED_CANNOT_VERIFY)) return false;
		if (reliability == Reliability.RELIABLE) return false;
		if (reportedTaxonConfidence == TaxonConfidence.UNSURE || reportedTaxonConfidence == TaxonConfidence.SUBSPECIES_UNSURE) return true;
		if (isInaturalist) return false;
		if (!given(linkedTaxon)) return false;
		if (!dao.hasTaxon(linkedTaxon)) return false;
		return !dao.getTaxon(linkedTaxon).isSpecies();
	}

	private static final Map<RecordQuality, Reliability> QUALITY_TO_RELIABILITY;
	static {
		QUALITY_TO_RELIABILITY = new HashMap<>();
		QUALITY_TO_RELIABILITY.put(RecordQuality.EXPERT_VERIFIED, Reliability.RELIABLE);
		QUALITY_TO_RELIABILITY.put(RecordQuality.COMMUNITY_VERIFIED, Reliability.RELIABLE);
		QUALITY_TO_RELIABILITY.put(RecordQuality.NEUTRAL, Reliability.UNDEFINED);
		QUALITY_TO_RELIABILITY.put(RecordQuality.UNCERTAIN, Reliability.UNRELIABLE);
		QUALITY_TO_RELIABILITY.put(RecordQuality.ERRONEOUS, Reliability.UNRELIABLE);
	}

	private Reliability reliability(RecordQuality recordQuality) {
		Reliability r = QUALITY_TO_RELIABILITY.get(recordQuality);
		if (r != null) return r;
		return Reliability.UNDEFINED;
	}

	private static final Map<RecordQuality, Double> QUALITY_TO_NUMERIC;
	static {
		QUALITY_TO_NUMERIC = new HashMap<>();
		QUALITY_TO_NUMERIC.put(RecordQuality.EXPERT_VERIFIED, 0.9);
		QUALITY_TO_NUMERIC.put(RecordQuality.COMMUNITY_VERIFIED, 0.7);
		QUALITY_TO_NUMERIC.put(RecordQuality.NEUTRAL, 0.5);
		QUALITY_TO_NUMERIC.put(RecordQuality.UNCERTAIN, 0.25);
		QUALITY_TO_NUMERIC.put(RecordQuality.ERRONEOUS, 0.1);
	}

	public static Double recordQualityNumeric(RecordQuality recordQuality) {
		Double d = QUALITY_TO_NUMERIC.get(recordQuality);
		if (d != null) return d;
		return 0.0;
	}

	private RecordQuality recordQuality(Set<Tag> effectiveTags, Qname reportedTaxonId, Set<Qname> userAnnotatedTaxonIds, TaxonConfidence reportedTaxonConfidence, boolean hasIssues) {
		if (effectiveTags.contains(Tag.EXPERT_TAG_VERIFIED) || effectiveTags.contains(Tag.FORMADMIN_VERIFIED)) {
			if (hasIssues) return RecordQuality.NEUTRAL;
			return RecordQuality.EXPERT_VERIFIED;
		}
		if (effectiveTags.contains(Tag.FORMADMIN_UNCERTAIN)) return RecordQuality.UNCERTAIN;
		if (effectiveTags.contains(Tag.EXPERT_TAG_ERRONEOUS)) return RecordQuality.ERRONEOUS;
		if (effectiveTags.contains(Tag.EXPERT_TAG_UNCERTAIN)) return RecordQuality.UNCERTAIN;

		if (effectiveTags.contains(Tag.COMMUNITY_TAG_VERIFIED)) {
			if (hasIssues) return RecordQuality.NEUTRAL;
			return RecordQuality.COMMUNITY_VERIFIED;
		}

		if (given(reportedTaxonId) && userAnnotatedTaxonIds.size() == 1 && userAnnotatedTaxonIds.contains(reportedTaxonId)) {
			if (hasIssues) return RecordQuality.NEUTRAL;
			return RecordQuality.COMMUNITY_VERIFIED;
		}

		if (reportedTaxonConfidence == TaxonConfidence.UNSURE) return RecordQuality.UNCERTAIN;

		return RecordQuality.NEUTRAL;
	}

	private boolean changedSignificantly(OccurrenceAtTimeOfAnnotation o, Gathering gathering, SingleCoordinates coordinatesNow, Qname reportedTaxonId, Qname expertAnnotatedTaxonId, Qname userAnnotatedTaxonId) {
		if (o == null || !o.hasValues()) return false; // legacy data; can not check for changes

		if (taxonHasChanged(o, reportedTaxonId, expertAnnotatedTaxonId, userAnnotatedTaxonId)) return true;

		if (hasCoordinates(o, coordinatesNow)) {
			if (coordinatesChanged(o, coordinatesNow)) return true;
		} else {
			if (countryChanged(o, gathering)) return true;
		}

		if (timeOfYearChanged(o, gathering)) return true;

		return false;
	}

	private boolean hasCoordinates(OccurrenceAtTimeOfAnnotation o, SingleCoordinates now) {
		return o.getWgs84centerPointLat() != null && o.getWgs84centerPointLon() != null && now != null;
	}

	private boolean timeOfYearChanged(OccurrenceAtTimeOfAnnotation o, Gathering gathering) {
		if (o.getDateBegin() == null) return false;
		DateRange now = gathering.getEventDate();
		if (now == null) return true;
		DateRange atTimeOfAnnotation = null;
		try {
			atTimeOfAnnotation = new DateRange(o.getDateBegin(), o.getDateEnd());
		} catch (Exception e) {
			return false;
		}

		DateValue atTimeOfAnnotationBegin = DateUtils.convertToDateValue(atTimeOfAnnotation.getBegin());
		DateValue AtTimeOfAnnotationEnd = DateUtils.convertToDateValue(atTimeOfAnnotation.getEnd());
		if (secured(atTimeOfAnnotationBegin, AtTimeOfAnnotationEnd)) return false;

		DateValue nowBegin = DateUtils.convertToDateValue(now.getBegin());
		DateValue nowEnd = DateUtils.convertToDateValue(now.getEnd());
		if (atTimeOfAnnotationBegin.getYearAsInt() != AtTimeOfAnnotationEnd.getYearAsInt()) return false;
		if (nowBegin.getYearAsInt() != nowEnd.getYearAsInt()) return false;

		int beginDifference = Math.abs(DateUtils.getDayOfYear(atTimeOfAnnotationBegin) - DateUtils.getDayOfYear(nowBegin));
		int endDifference = Math.abs(DateUtils.getDayOfYear(AtTimeOfAnnotationEnd) - DateUtils.getDayOfYear(nowEnd));

		return beginDifference > 100 || endDifference > 100;
	}

	private boolean secured(DateValue atTimeOfAnnotationBegin, DateValue atTimeOfAnnotationEnd) {
		return
				"1".equals(atTimeOfAnnotationBegin.getDay()) &&
				"1".equals(atTimeOfAnnotationBegin.getMonth()) &&
				"31".equals(atTimeOfAnnotationEnd.getDay()) &&
				"12".equals(atTimeOfAnnotationEnd.getMonth());
	}

	private boolean coordinatesChanged(OccurrenceAtTimeOfAnnotation o, SingleCoordinates now) {
		double latDifference = Math.abs(o.getWgs84centerPointLat() - now.getLat());
		double lonDifference = Math.abs(o.getWgs84centerPointLon() - now.getLon());
		return latDifference > 2.5 || lonDifference > 5;
	}

	private Coordinates getWgs84Coordinates(Gathering gathering) {
		try {
			if (gathering.getGeo() != null) {
				if (gathering.getGeo().getCRS() == Type.WGS84) {
					return gathering.getGeo().getBoundingBox();
				}
				Geo wgs84Geo = CoordinateConverter.convertGeo(gathering.getGeo(), Type.WGS84);
				return wgs84Geo.getBoundingBox();
			}
			if (gathering.getCoordinates() != null) {
				if (gathering.getCoordinates().getType() == Type.WGS84) {
					return gathering.getCoordinates();
				}
				return CoordinateConverter.convert(gathering.getCoordinates()).getWgs84();
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private boolean countryChanged(OccurrenceAtTimeOfAnnotation o, Gathering gathering) {
		if (!given(o.getCountryVerbatim())) return false;
		Qname countryAtTimeOfAnnotation = null;
		List<Area> countries = dao.resolveCountiesByName(o.getCountryVerbatim());
		if (countries.size() == 1) countryAtTimeOfAnnotation = countries.get(0).getQname();
		if (countryAtTimeOfAnnotation == null) return false;
		Qname countryNow = gathering.getInterpretations() == null ? null : gathering.getInterpretations().getCountry();
		return !countryAtTimeOfAnnotation.equals(countryNow);
	}

	private boolean taxonHasChanged(OccurrenceAtTimeOfAnnotation o, Qname reportedTaxon, Qname expertAnnotatedTaxon, Qname userAnnotatedTaxon) {
		// If there is identification in the annotation, it's clear what taxon is meant by the annotation, even if
		// the taxon of the original occurrence has changed between time of making the annotation and now
		if (given(expertAnnotatedTaxon) || given(userAnnotatedTaxon)) return false;

		Qname taxonAtTimeOfAnnotation = resolveAnnotationTaxonId(o.getTaxonId(), o.getTaxonVerbatim());

		// nothing could be said about the taxon at time of annotation and nothing can be said about the current data -> nothing can be said about taxon change
		if (taxonAtTimeOfAnnotation == null && reportedTaxon == null) return false;

		if (taxonAtTimeOfAnnotation == null) {
			// at the time of annotation the occurrence did not resolve to any taxon
			// if it now does -> we must invalidate the annotation
			// if not -> nothing can be said about taxon change
			return reportedTaxon != null;
		}

		if (reportedTaxon == null) {
			// at the time of annotation the occurrence resolved to a taxon
			// the now reported data does not resolve to a taxon -> we must invalidate the annotation
			return true;
		}

		return !taxonAtTimeOfAnnotation.equals(reportedTaxon);
	}

	private boolean needsCheck(Set<Tag> effectiveTags) {
		if (effectiveTags.contains(Tag.CHECKED_CANNOT_VERIFY)) return false;
		for (Tag t : effectiveTags) {
			if (Annotation.tagType(t) == AnnotationType.USER_CHECK) return true;
			if (t == Tag.AUTO_DISTRIBUTION_CHECK) return true;
			if (t == Tag.AUTO_PERIOD_CHECK) return true;
		}
		return false;
	}


	private boolean hasTaxon(Annotation a) {
		if (a.getIdentification() == null) return false;
		return a.getIdentification().hasIdentification();
	}

	private Qname expertAnnotatedTaxonId(Annotation a) {
		if (!expertIdentification(a)) return null;
		return resolveAnnotationTaxonId(a);
	}

	private Qname userAnnotatedTaxonId(Annotation a) {
		if (expertIdentification(a)) return null;
		if (!a.getAddedTags().isEmpty()) return null;
		return resolveAnnotationTaxonId(a);
	}

	private Qname resolveAnnotationTaxonId(Annotation a) {
		if (a.getIdentification() == null || !a.getIdentification().hasIdentification()) return null;
		return resolveAnnotationTaxonId(a.getIdentification().getTaxonID(), a.getIdentification().getTaxon());
	}

	private boolean expertIdentification(Annotation a) {
		return a.getAddedTags().contains(Tag.EXPERT_TAG_VERIFIED);
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}


	private Qname getTaxonId(Unit unit, Gathering gathering) {
		return dao.getTaxonLinkingService().getTaxonId(unit, gathering, false);
	}

	private Qname resolveAnnotationTaxonId(Qname annotatedTaxonId, String annotatedVerbatim) {
		if (given(annotatedTaxonId)) {
			Qname resolvedTaxonId = dao.getTaxonLinkingService().getTaxonId(annotatedTaxonId);
			if (resolvedTaxonId != null) return resolvedTaxonId;
		}
		if (!given(annotatedVerbatim)) return null;
		return dao.getTaxonLinkingService().getTaxonId(annotatedVerbatim);
	}

}
