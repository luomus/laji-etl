package fi.laji.datawarehouse.etl.models;

import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;

public class ContextDefinitions {

	public static class ContextDefinition {
		private final Qname property;
		private final boolean resource;
		public ContextDefinition(Qname property, boolean resource) {
			this.property = property;
			this.resource = resource;
		}
		public Qname getProperty() {
			return property;
		}
		public boolean isResource() {
			return resource;
		}
	}

	private final Map<String, ContextDefinition> contextDefinitions;

	public ContextDefinitions(Map<String, ContextDefinition> contextDefinitions) {
		this.contextDefinitions = contextDefinitions;
	}

	public ContextDefinition getContextDefinition(String property) {
		return contextDefinitions.get(property);
	}

}
