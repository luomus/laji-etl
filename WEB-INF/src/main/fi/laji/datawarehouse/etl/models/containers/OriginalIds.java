package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class OriginalIds {

	private final Qname documentId;
	private final Qname unitId;

	public OriginalIds(Qname documentId, Qname unitId) {
		this.documentId = documentId;
		this.unitId = unitId;
	}

	public Qname getDocumentId() {
		return documentId;
	}

	public Qname getUnitId() {
		return unitId;
	}

	@Override
	public String toString() {
		return "OriginalIds [documentId=" + documentId + ", unitId=" + unitId + "]";
	}

}
