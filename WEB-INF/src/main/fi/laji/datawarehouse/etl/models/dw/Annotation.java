package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;

public class Annotation {

	public static final Qname FORM_ADMIN_ROLE = new Qname("MMAN.formAdmin");

	public enum AnnotationType { ALL, USER_EFFECTIVE, USER_CHECK, DW_AUTO, ADMIN, FORM_ADMIN, INVASIVE_CONTROL, COMMENT, DELETED }

	public enum Tag {
		ADMIN_MARKED_SPAM, ADMIN_MARKED_COARSE, ADMIN_MARKED_NON_WILD,
		EXPERT_TAG_VERIFIED, EXPERT_TAG_UNCERTAIN, EXPERT_TAG_ERRONEOUS,
		COMMUNITY_TAG_VERIFIED, AUTO_VALIDATIONS_PASS,
		CHECKED_CANNOT_VERIFY, CHANGED_OWNER_MANUAL, CHANGED_DW_AUTO,
		CHECK, CHECK_COORDINATES, CHECK_DATETIME, CHECK_LOCATION, CHECK_OBSERVER, CHECK_TAXON, CHECK_DUPLICATE, CHECK_WILDNESS, CHECK_NEEDS_INFO, CHECK_SPAM, CHECK_BREEDING_INDEX,
		AUTO_DISTRIBUTION_CHECK, AUTO_PERIOD_CHECK,
		FORMADMIN_CENSUS_COUNT_ERROR, FORMADMIN_CENSUS_INNER_COUNT_ERROR, FORMADMIN_CENSUS_OTHER_ERROR,
		FORMADMIN_VERIFIED, FORMADMIN_UNCERTAIN,
		INVASIVE_FULL, INVASIVE_PARTIAL, INVASIVE_NO_EFFECT, INVASIVE_NOT_FOUND }

	private static EnumToProperty ENUM_TO_PROP = EnumToProperty.getInstance();

	private static Map<Tag, AnnotationType> TAG_TYPE;
	static {
		TAG_TYPE = new HashMap<>();

		TAG_TYPE.put(Tag.ADMIN_MARKED_SPAM, AnnotationType.ADMIN); // admin/expert has marked as spam -> do not load entire document
		TAG_TYPE.put(Tag.ADMIN_MARKED_COARSE, AnnotationType.ADMIN); // admin/expert has marked unit to be coarsed -> secure 100km

		TAG_TYPE.put(Tag.ADMIN_MARKED_NON_WILD, AnnotationType.USER_EFFECTIVE); // admin/expert has marked unit as non wild -> change unit wildness
		TAG_TYPE.put(Tag.EXPERT_TAG_VERIFIED, AnnotationType.USER_EFFECTIVE); // expert quality rating -> effects RecordQuality
		TAG_TYPE.put(Tag.EXPERT_TAG_UNCERTAIN, AnnotationType.USER_EFFECTIVE); // expert quality rating -> effects RecordQuality
		TAG_TYPE.put(Tag.EXPERT_TAG_ERRONEOUS, AnnotationType.USER_EFFECTIVE); // expert quality rating -> effects RecordQuality
		TAG_TYPE.put(Tag.COMMUNITY_TAG_VERIFIED, AnnotationType.DW_AUTO); // these never come from annotations; always from original document as sourceTags -> effects RecordQuality

		TAG_TYPE.put(Tag.AUTO_DISTRIBUTION_CHECK, AnnotationType.DW_AUTO); // dw automatic species distribution validation -> neds check = true
		TAG_TYPE.put(Tag.AUTO_PERIOD_CHECK, AnnotationType.DW_AUTO); // dw automatic species period validation -> needs check = true
		TAG_TYPE.put(Tag.AUTO_VALIDATIONS_PASS, AnnotationType.DW_AUTO); // has passed dw automatic species validations (no effect to anything)

		TAG_TYPE.put(Tag.CHECKED_CANNOT_VERIFY, AnnotationType.USER_CHECK); // owner or expert says this case can not be resolved -> needsCheck=false, needsIdentification=false
		TAG_TYPE.put(Tag.CHANGED_OWNER_MANUAL, AnnotationType.USER_CHECK); // owner says observation modified -> previous annotations remain valid; needs check = true
		TAG_TYPE.put(Tag.CHANGED_DW_AUTO, AnnotationType.DW_AUTO); // dw deems observation modified so much that previous annotations are not valid anymore -> added if previous annotations were marked invalid

		// User(/expert) validations -> unit needs check = true
		TAG_TYPE.put(Tag.CHECK, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_COORDINATES, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_DATETIME, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_LOCATION, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_OBSERVER, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_TAXON, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_WILDNESS, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_DUPLICATE, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_NEEDS_INFO, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_SPAM, AnnotationType.USER_CHECK);
		TAG_TYPE.put(Tag.CHECK_BREEDING_INDEX, AnnotationType.USER_CHECK);

		TAG_TYPE.put(Tag.FORMADMIN_CENSUS_COUNT_ERROR, AnnotationType.FORM_ADMIN);
		TAG_TYPE.put(Tag.FORMADMIN_CENSUS_INNER_COUNT_ERROR, AnnotationType.FORM_ADMIN);
		TAG_TYPE.put(Tag.FORMADMIN_CENSUS_OTHER_ERROR, AnnotationType.FORM_ADMIN);
		TAG_TYPE.put(Tag.FORMADMIN_VERIFIED, AnnotationType.FORM_ADMIN);
		TAG_TYPE.put(Tag.FORMADMIN_UNCERTAIN, AnnotationType.FORM_ADMIN);

		TAG_TYPE.put(Tag.INVASIVE_FULL, AnnotationType.INVASIVE_CONTROL);
		TAG_TYPE.put(Tag.INVASIVE_PARTIAL, AnnotationType.INVASIVE_CONTROL);
		TAG_TYPE.put(Tag.INVASIVE_NO_EFFECT, AnnotationType.INVASIVE_CONTROL);
		TAG_TYPE.put(Tag.INVASIVE_NOT_FOUND, AnnotationType.INVASIVE_CONTROL);
	}

	private Qname id;
	private Qname rootId;
	private Qname targetId;
	private boolean deleted = false;
	private Long createdTimestamp;
	private Long deletedTimestamp;
	private Qname annotationByPerson;
	private String annotationByPersonName;
	private Qname annotationBySystem;
	private String annotationBySystemName;
	private Qname deletedByPerson;
	private String deletedByPersonName;
	private List<Tag> addedTags = new ArrayList<>();
	private List<Tag> removedTags = new ArrayList<>();
	private Qname atlasCode;
	private Qname byRole;
	private Identification identification;
	private OccurrenceAtTimeOfAnnotation occurrenceAtTimeOfAnnotation = new OccurrenceAtTimeOfAnnotation();
	private String notes;
	private Boolean valid;

	public Annotation(Qname annotationId, Qname rootId, Qname targetId, Long createdTimestamp) throws CriticalParseFailure {
		if (createdTimestamp == null) throw new CriticalParseFailure("Null created timestamp");
		setId(annotationId);
		setRootID(rootId);
		setTargetID(targetId);
		setCreatedTimestamp(createdTimestamp);
	}

	private Annotation(Qname annotationId, Qname rootId) throws CriticalParseFailure {
		setId(annotationId);
		setRootID(rootId);
		setDeleted(true);
	}

	@Deprecated // for serialization
	public Annotation() {}

	public Qname getId() {
		return id;
	}

	public void setId(Qname annotationId) throws CriticalParseFailure {
		Util.validateId(annotationId, "annotationId");
		this.id = annotationId;
	}

	@FieldDefinition(ignoreForQuery=true)
	public Qname getRootID() { // must be rootID to match notification schema
		return rootId;
	}

	public void setRootID(Qname rootId) throws CriticalParseFailure {
		Util.validateId(rootId, "rootId");
		this.rootId = rootId;
	}

	@FieldDefinition(ignoreForQuery=true)
	public Qname getTargetID() { // must be targetID to match notification schema
		return targetId;
	}

	public void setTargetID(Qname targetId) throws CriticalParseFailure {
		if (targetId != null) {
			Util.validateId(targetId, "targetId");
		}
		this.targetId = targetId;
	}

	@FieldDefinition(ignoreForQuery=true)
	public Long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Long created) {
		this.createdTimestamp = created;
	}

	public String getCreated() {
		if (createdTimestamp == null) return null;
		return Util.formatToIsoDateWithTimezone(createdTimestamp);
	}

	@Deprecated // use setCreatedTimestamp
	public void setCreated(@SuppressWarnings("unused") String created) {

	}

	public Date createdAsDate() {
		if (createdTimestamp == null) return null;
		return Util.toDate(createdTimestamp);
	}

	@FieldDefinition(ignoreForQuery=true)
	public Long getDeletedTimestamp() {
		return deletedTimestamp;
	}

	public void setDeletedTimestamp(Long deleted) {
		this.deletedTimestamp = deleted;
	}

	public String getDeletedDateTime() {
		if (deletedTimestamp == null) return null;
		return Util.formatToIsoDateWithTimezone(deletedTimestamp);
	}

	@Deprecated // use setDeletedTimestamp
	public void setDeletedDateTime(@SuppressWarnings("unused") String deleted) {

	}

	public Date deletedAsDate() {
		if (createdTimestamp == null) return null;
		return Util.toDate(createdTimestamp);
	}

	public Qname getAnnotationByPerson() {
		return annotationByPerson;
	}

	public void setAnnotationByPerson(Qname annotationByPerson) {
		this.annotationByPerson = annotationByPerson;
	}

	public Qname getAnnotationBySystem() {
		return annotationBySystem;
	}

	public void setAnnotationBySystem(Qname annotationBySystem) {
		this.annotationBySystem = annotationBySystem;
	}

	public Qname getDeletedByPerson() {
		return deletedByPerson;
	}

	public void setDeletedByPerson(Qname deletedByPerson) {
		this.deletedByPerson = deletedByPerson;
	}

	public String getAnnotationByPersonName() {
		return annotationByPersonName;
	}

	public void setAnnotationByPersonName(String annotationByPersonName) {
		this.annotationByPersonName = Util.trimToByteLength(annotationByPersonName, 1000);
	}

	public String getDeletedByPersonName() {
		return deletedByPersonName;
	}

	public void setDeletedByPersonName(String deletedByPersonName) {
		this.deletedByPersonName = Util.trimToByteLength(deletedByPersonName, 1000);
	}

	public String getAnnotationBySystemName() {
		return annotationBySystemName;
	}

	public void setAnnotationBySystemName(String annotationBySystemName) {
		this.annotationBySystemName = Util.trimToByteLength(annotationBySystemName, 1000);
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = Util.trimToByteLength(notes, 5000);
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Identification getIdentification() {
		return identification;
	}

	public void setIdentification(Identification identification) {
		this.identification = identification;
	}

	public List<Tag> getAddedTags() {
		return addedTags;
	}

	public void setAddedTags(ArrayList<Tag> addedTags) {
		this.addedTags = addedTags;
	}

	public List<Tag> getRemovedTags() {
		return removedTags;
	}

	public void setRemovedTags(ArrayList<Tag> removedTags) {
		this.removedTags = removedTags;
	}

	public Annotation addTag(Qname tag) {
		addedTags.add(toTag(tag));
		return this;
	}

	public Annotation removeTag(Qname tag) {
		removedTags.add(toTag(tag));
		return this;
	}

	public Annotation addTag(Tag tag) {
		addedTags.add(tag);
		return this;
	}

	public Annotation removeTag(Tag tag) {
		removedTags.add(tag);
		return this;
	}

	public Qname getAtlasCode() {
		return atlasCode;
	}

	public void setAtlasCode(Qname atlasCode) {
		this.atlasCode = atlasCode;
	}

	public Qname getByRole() {
		return byRole;
	}

	public void setByRole(Qname byRole) {
		this.byRole = byRole;
	}

	public OccurrenceAtTimeOfAnnotation getOccurrenceAtTimeOfAnnotation() {
		return occurrenceAtTimeOfAnnotation;
	}

	public void setOccurrenceAtTimeOfAnnotation(OccurrenceAtTimeOfAnnotation occurrenceAtTimeOfAnnotation) {
		this.occurrenceAtTimeOfAnnotation = occurrenceAtTimeOfAnnotation;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	/**
	 * Get a public version of this annotation that does not have the original document / unit id
	 * @return
	 */
	public Annotation secureAnnotation() {
		try {
			Annotation annotation = new Annotation();
			annotation.setId(this.getId());
			annotation.setCreatedTimestamp(this.getCreatedTimestamp());
			annotation.setAnnotationByPerson(this.getAnnotationByPerson());
			annotation.setAnnotationBySystem(this.getAnnotationBySystem());
			annotation.setAnnotationByPersonName(this.getAnnotationByPersonName());
			annotation.setAnnotationBySystemName(this.getAnnotationBySystemName());
			annotation.setDeletedByPerson(this.getDeletedByPerson());
			annotation.setDeletedByPersonName(this.getDeletedByPersonName());
			annotation.setDeleted(this.isDeleted());
			annotation.setDeletedTimestamp(this.getDeletedTimestamp());
			annotation.setIdentification(this.getIdentification());
			annotation.setOccurrenceAtTimeOfAnnotation(this.getOccurrenceAtTimeOfAnnotation());
			for (Tag t : this.getAddedTags()) annotation.addTag(t);
			for (Tag t : this.getRemovedTags()) annotation.removeTag(t);
			annotation.setAtlasCode(this.getAtlasCode());
			annotation.setByRole(this.getByRole());
			annotation.setNotes(this.getNotes());
			annotation.setValid(this.getValid());
			return annotation;
		} catch (CriticalParseFailure e) {
			throw new ETLException("Impossible state", e);
		}
	}

	public boolean after(Annotation other) {
		return this.createdTimestamp > other.createdTimestamp;
	}

	public static Annotation emptyAnnotation() {
		return new Annotation();
	}

	public static Annotation deletedAnnotation(Qname annotationId, Qname rootId) throws CriticalParseFailure {
		return new Annotation(annotationId, rootId);
	}

	public AnnotationType resolveType() {
		if (this.isDeleted()) return AnnotationType.DELETED;

		Set<AnnotationType> addedTagTypes = tagTypes(addedTags);
		Set<AnnotationType> removedTagTypes = tagTypes(removedTags);

		if (addedTagTypes.contains(AnnotationType.DW_AUTO)) return AnnotationType.DW_AUTO;
		if (addedTagTypes.contains(AnnotationType.FORM_ADMIN)) return AnnotationType.FORM_ADMIN;
		if (addedTagTypes.contains(AnnotationType.ADMIN)) return AnnotationType.ADMIN;
		if (addedTagTypes.contains(AnnotationType.INVASIVE_CONTROL)) return AnnotationType.INVASIVE_CONTROL;

		if (addedTagTypes.contains(AnnotationType.USER_EFFECTIVE) || removedTagTypes.contains(AnnotationType.USER_EFFECTIVE)) return AnnotationType.USER_EFFECTIVE;
		if (identification != null && identification.hasIdentification()) return AnnotationType.USER_EFFECTIVE;

		if (addedTagTypes.contains(AnnotationType.USER_CHECK)) return AnnotationType.USER_CHECK;
		if (removedTagTypes.contains(AnnotationType.DW_AUTO)) return AnnotationType.USER_CHECK;

		if (removedTagTypes.contains(AnnotationType.FORM_ADMIN)) return AnnotationType.FORM_ADMIN;
		if (removedTagTypes.contains(AnnotationType.ADMIN)) return AnnotationType.ADMIN;
		if (removedTagTypes.contains(AnnotationType.INVASIVE_CONTROL)) return AnnotationType.INVASIVE_CONTROL;
		if (removedTagTypes.contains(AnnotationType.USER_CHECK)) return AnnotationType.USER_CHECK;

		return AnnotationType.COMMENT;
	}

	private Set<AnnotationType> tagTypes(List<Tag> tags) {
		if (tags.isEmpty()) return Collections.emptySet();
		Set<AnnotationType> types = new HashSet<>();
		for (Tag t : tags) {
			types.add(tagType(t));
		}
		return types;
	}

	public static AnnotationType tagType(Tag tag) {
		if (tag == null) throw new IllegalArgumentException("Null tag");
		AnnotationType type = TAG_TYPE.get(tag);
		if (type == null) throw new IllegalStateException("Unedfined tag type " + tag);
		return type;
	}

	public static Tag toTag(Qname tag) {
		if (tag == null) throw new IllegalArgumentException("Null tag");
		Tag t = (Tag) ENUM_TO_PROP.get(tag);
		if (t == null) throw new IllegalArgumentException("Unmapped tag " + tag);
		return t;
	}

	private static BiMap<Tag, InvasiveControl> T_TO_I;
	static {
		T_TO_I = HashBiMap.create(5);
		T_TO_I.put(Tag.INVASIVE_FULL, InvasiveControl.FULL);
		T_TO_I.put(Tag.INVASIVE_PARTIAL, InvasiveControl.PARTIAL);
		T_TO_I.put(Tag.INVASIVE_NO_EFFECT, InvasiveControl.NO_EFFECT);
		T_TO_I.put(Tag.INVASIVE_NOT_FOUND, InvasiveControl.NOT_FOUND);
	}

	public static InvasiveControl toInvasiveControl(Tag t) {
		if (t == null) throw new IllegalArgumentException("Null invasive control tag");
		return T_TO_I.get(t);
	}

	public static Tag toTag(InvasiveControl i) {
		if (i == null) throw new IllegalArgumentException("Null invasive control value");
		Tag t =  T_TO_I.inverse().get(i);
		if (t == null) throw new IllegalStateException("Unmapped invasive control value " + i);
		return t;
	}

	@Override
	public String toString() {
		return "Annotation [id=" + id + "]";
	}

}
