package fi.laji.datawarehouse.etl.models.dw;

import fi.luomus.commons.containers.rdf.Qname;

public class TaxonCensus {

	private Qname taxonId;
	private Qname type;

	public TaxonCensus(Qname taxonId, Qname type) {
		this.setTaxonId(taxonId);
		this.setType(type);
	}

	@Deprecated // for hibernate
	public TaxonCensus() {}

	public Qname getTaxonId() {
		return taxonId;
	}

	public void setTaxonId(Qname taxonId) {
		this.taxonId = taxonId;
	}

	public Qname getType() {
		return type;
	}

	public void setType(Qname type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TaxonCensus [taxonId=" + taxonId + ", type=" + type + "]";
	}

	public TaxonCensus copy() {
		return new TaxonCensus(getTaxonId(), getType());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taxonId == null) ? 0 : taxonId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxonCensus other = (TaxonCensus) obj;
		if (taxonId == null) {
			if (other.taxonId != null)
				return false;
		} else if (!taxonId.equals(other.taxonId))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}

