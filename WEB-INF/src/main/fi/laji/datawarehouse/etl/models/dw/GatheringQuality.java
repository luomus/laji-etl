package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import fi.laji.datawarehouse.query.download.model.FileDownloadField;

@Embeddable
public class GatheringQuality {

	private Quality issue;
	private Quality timeIssue;
	private Quality locationIssue;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="message", column=@Column(name="gathering_issue_message"))
	})
	@FileDownloadField(name="Issue", order=0)
	public Quality getIssue() {
		return issue;
	}

	public GatheringQuality setIssue(Quality issue) {
		this.issue = issue;
		return this;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="message", column=@Column(name="timeissue_message"))
	})
	@FileDownloadField(name="TimeIssue", order=1)
	public Quality getTimeIssue() {
		return timeIssue;
	}

	public GatheringQuality setTimeIssue(Quality issue) {
		this.timeIssue = issue;
		return this;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="message", column=@Column(name="locationissue_message"))
	})
	@FileDownloadField(name="LocationIssue", order=2)
	public Quality getLocationIssue() {
		return locationIssue;
	}

	public GatheringQuality setLocationIssue(Quality issue) {
		this.locationIssue = issue;
		return this;
	}

	public boolean hasIssues() {
		return issue != null || timeIssue != null || locationIssue != null;
	}

	public GatheringQuality conseal() {
		GatheringQuality concealed = new GatheringQuality();
		if (issue != null) concealed.setIssue(issue.conceal());
		if (timeIssue != null) concealed.setTimeIssue(timeIssue.conceal());
		if (locationIssue != null) concealed.setLocationIssue(locationIssue.conceal());
		return concealed;
	}

	@Override
	public String toString() {
		return "GatheringQuality [issue=" + issue + ", timeIssue=" + timeIssue + ", locationIssue=" + locationIssue + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((issue == null) ? 0 : issue.hashCode());
		result = prime * result + ((locationIssue == null) ? 0 : locationIssue.hashCode());
		result = prime * result + ((timeIssue == null) ? 0 : timeIssue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GatheringQuality other = (GatheringQuality) obj;
		if (issue == null) {
			if (other.issue != null)
				return false;
		} else if (!issue.equals(other.issue))
			return false;
		if (locationIssue == null) {
			if (other.locationIssue != null)
				return false;
		} else if (!locationIssue.equals(other.locationIssue))
			return false;
		if (timeIssue == null) {
			if (other.timeIssue != null)
				return false;
		} else if (!timeIssue.equals(other.timeIssue))
			return false;
		return true;
	}

}
