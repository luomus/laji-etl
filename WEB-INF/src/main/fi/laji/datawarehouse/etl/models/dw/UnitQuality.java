package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import fi.laji.datawarehouse.etl.models.Interpreter;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;

@Embeddable
public class UnitQuality {

	private Quality issue;
	private Boolean issues;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="message", column=@Column(name="unit_issue_message"))
	})
	@FileDownloadField(name="Issue", order=2)
	public Quality getIssue() {
		return issue;
	}

	public UnitQuality setIssue(Quality issue) {
		this.issue = issue;
		return this;
	}

	public boolean hasIssues() {
		return issue != null;
	}

	@Override
	public String toString() {
		return "UnitQuality [issue=" + issue + ", issues=" + issues + "]";
	}

	/**
	 * Tells if document, gathering or unit has issues
	 * @return
	 */
	@Column(name="issues")
	@FieldDefinition(ignoreForETL=true)
	public Boolean isDocumentGatheringUnitQualityIssues() {
		return issues;
	}

	/**
	 * You should not set this manually. This is resolved in Interpreter.
	 * @see Interpreter
	 * @param hasIssues
	 */
	public void setDocumentGatheringUnitQualityIssues(boolean hasIssues) {
		this.issues = hasIssues;
	}

}
