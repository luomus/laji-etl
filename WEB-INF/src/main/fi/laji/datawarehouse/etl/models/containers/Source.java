package fi.laji.datawarehouse.etl.models.containers;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;

public class Source {

	private final Qname id;
	private final String name;
	private final boolean warehouseSource;
	private final boolean allowedToQueryPrivateWarehouse; 
	private final boolean allowedToUseDatawarehouseEditorOrObserverIdIsNot;

	private final Set<Qname> allowedOverridingSources = new HashSet<>();

	public Source(Qname id, String name, boolean isWarehouseSource, boolean allowedToQueryPrivateWarehouse, boolean allowedToUseDatawarehouseEditorOrObserverIdIsNot) {
		this.id = id;
		this.name = name;
		this.warehouseSource = isWarehouseSource;
		this.allowedToQueryPrivateWarehouse = allowedToQueryPrivateWarehouse;
		this.allowedToUseDatawarehouseEditorOrObserverIdIsNot = allowedToUseDatawarehouseEditorOrObserverIdIsNot;
	}

	public Qname getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isWarehouseSource() {
		return warehouseSource;
	}

	public boolean isAllowedToQueryPrivateWarehouse() {
		return allowedToQueryPrivateWarehouse;
	}

	public boolean isAllowedToUseDatawarehouseEditorOrObserverIdIsNot() {
		return allowedToUseDatawarehouseEditorOrObserverIdIsNot;
	}

	public Set<Qname> getValidOverridingSources() {
		return Collections.unmodifiableSet(allowedOverridingSources);
	}

	public Source addValidOverridingSource(Qname sourceId) {
		this.allowedOverridingSources.add(sourceId);
		return this;
	}

}
