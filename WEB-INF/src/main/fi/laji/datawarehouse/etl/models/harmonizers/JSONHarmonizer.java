package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.Collections;
import java.util.List;

import fi.laji.datawarehouse.dao.MediaDAO;
import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;

public class JSONHarmonizer implements Harmonizer<String> {

	private final ContextDefinitions contextDefinitions;
	private final MediaDAO mediaDao;

	public JSONHarmonizer(ContextDefinitions contextDefinitions, MediaDAO mediaDao) {
		this.contextDefinitions = contextDefinitions;
		this.mediaDao = mediaDao;
	}

	@Override
	public List<DwRoot> harmonize(String data, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		JSONObject jsonObject = toJsonObject(data);
		String schema = jsonObject.getString(Const.SCHEMA);
		if (schema.equals(Const.LAJI_ETL_SCHEMA)) {
			return new LajiETLJSONHarmonizer().harmonize(jsonObject, source);
		}
		if (schema.equals(Const.LAJISTORE_SCHEMA)) {
			JSONArray roots = jsonObject.getArray("roots");
			if (roots.isEmpty()) return Collections.emptyList();
			JSONObject first = roots.iterateAsObject().get(0);
			if (first.hasKey("document")) return new LajistoreHarmonizer(contextDefinitions, mediaDao).harmonize(jsonObject, source);
			if (first.hasKey("annotation")) return new LajistoreAnnotationHarmonizer().harmonize(jsonObject, source);
		}
		if (schema.equals(Const.LAJISTORE_SECONDAY_DATA_SCHEMA)) {
			JSONArray roots = jsonObject.getArray("roots");
			if (roots.isEmpty()) return Collections.emptyList();
			return new LajistoreSecondaryDataHarmonizer(contextDefinitions).harmonize(jsonObject, source);
		}
		if (schema.equals(Const.SYKE_ZOOBENTHOS_SCHEMA)) {
			return new SykeZoobenthosHarmonizer().harmonize(jsonObject, source);
		}
		throw new CriticalParseFailure("Unknown schema: " + schema);
	}

	private JSONObject toJsonObject(String data) throws CriticalParseFailure {
		try {
			return new JSONObject(data);
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid application/json", e);
		}
	}

}