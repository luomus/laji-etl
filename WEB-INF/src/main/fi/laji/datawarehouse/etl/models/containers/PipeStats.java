package fi.laji.datawarehouse.etl.models.containers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import fi.luomus.commons.containers.rdf.Qname;

public class PipeStats {

	public static class SourcePipeStats {
		private final Qname sourceId;
		private long processed = 0;
		private long waiting = 0;
		private long inError = 0;
		private long deleted = 0;
		public SourcePipeStats(Qname sourceId) {
			this.sourceId = sourceId;
		}
		public Qname getSourceId() {
			return sourceId;
		}
		public long getReceived() {
			return processed + waiting + inError;
		}
		public long getProcessed() {
			return processed;
		}
		public long getWaiting() {
			return waiting;
		}
		public long getInError() {
			return inError;
		}
		public long getDeleted() {
			return deleted;
		}
		@Override
		public String toString() {
			return "SourcePipeStats [processed=" + processed + ", waiting=" + waiting + ", inError=" + inError + ", deleted=" + deleted + "]";
		}
	}

	private final Set<Qname> sources = new TreeSet<>();
	private final Map<Qname, SourcePipeStats> in = new HashMap<>();
	private final Map<Qname, SourcePipeStats> out = new HashMap<>();

	public Collection<Qname> getSources() {
		return sources;
	}

	public SourcePipeStats getIn(Qname sourceId) {
		return in.get(sourceId);
	}

	public SourcePipeStats getOut(Qname sourceId) {
		return out.get(sourceId);
	}

	public void updateInPipeProcessed(Qname sourceId, long processed) {
		createIfNeeded(sourceId);
		in.get(sourceId).processed = processed;
	}

	public void updateInPipeWaiting(Qname sourceId, long waiting) {
		createIfNeeded(sourceId);
		in.get(sourceId).waiting = waiting;
	}

	public void updateInPipeInError(Qname sourceId, long inError) {
		createIfNeeded(sourceId);
		in.get(sourceId).inError = inError;
	}

	public void updateOutPipeProcessed(Qname sourceId, long processed) {
		createIfNeeded(sourceId);
		out.get(sourceId).processed = processed;
	}

	public void updateOutPipeWaiting(Qname sourceId, long waiting) {
		createIfNeeded(sourceId);
		out.get(sourceId).waiting = waiting;
	}

	public void updateOutPipeInError(Qname sourceId, long inError) {
		createIfNeeded(sourceId);
		out.get(sourceId).inError = inError;
	}

	public void updateOutPipeDeleted(Qname sourceId, long deleted) {
		createIfNeeded(sourceId);
		out.get(sourceId).deleted = deleted;
	}

	private void createIfNeeded(Qname sourceId) {
		if (!sources.contains(sourceId)) {
			sources.add(sourceId);
			in.put(sourceId, new SourcePipeStats(sourceId));
			out.put(sourceId, new SourcePipeStats(sourceId));
		}
	}

	@Override
	public String toString() {
		return "PipeStats [in=" + sort(in) + ", out=" + sort(out) + "]";
	}

	private String sort(Map<Qname, SourcePipeStats> map) {
		return new TreeMap<>(map).toString();
	}

}
