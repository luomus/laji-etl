package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class User {

	private final Qname qname;
	private final String fullname;

	public User(String qname, String fullname) {
		this.qname = new Qname(qname);
		this.fullname = fullname;
	}


	public Qname getQname() {
		return qname;
	}

	public String getFullname() {
		return fullname;
	}

}
