package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class SykeZoobenthosHarmonizer extends BaseHarmonizer<JSONObject> {

	private static final Qname COLLECTION_ID = new Qname("HR.3391");

	@Override
	public List<DwRoot> harmonize(JSONObject json, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> roots = new ArrayList<>();
		roots.add(parseRoot(json, source));
		return roots;
	}

	private DwRoot parseRoot(JSONObject document, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		DwRoot root = parseDocumentId(document, source);
		try {
			parseRoot(document, root);
		} catch (Exception e) {
			handleException(e);
		}
		return root;
	}

	private DwRoot parseDocumentId(JSONObject json, Qname source) throws CriticalParseFailure {
		String id = json.getString("eventID");
		if (!given(id)) throw new CriticalParseFailure("No document id");
		return new DwRoot(new Qname(source.toString() + "/" + id), source);
	}

	private void parseRoot(JSONObject json, DwRoot root) throws CriticalParseFailure  {
		root.setCollectionId(COLLECTION_ID);
		Document publicDocument = root.createPublicDocument();
		parseDocument(json, publicDocument);
		Gathering baseGathering = parseBaseGathering(json);
		parseGatherings(json, baseGathering, publicDocument);
	}

	private void parseGatherings(JSONObject eventJson, Gathering baseGathering, Document document) throws CriticalParseFailure {
		for (JSONObject subEvent : eventJson.getArray("Events").iterateAsObject()) {
			if (subEvent.getArray("Events").isEmpty()) {
				Gathering g = baseGathering.copy();
				g.setGatheringId(new Qname(document.getSourceId() + "/" + subEvent.getString("eventID")));
				parseUnits(document, g, subEvent);
				document.addGathering(g);
				continue;
			}
			Gathering subGathering = baseGathering.copy();
			parseMeasurements(subGathering, subEvent);
			parseGatherings(subEvent, subGathering, document);
		}
	}

	private void parseUnits(Document d, Gathering g, JSONObject eventJson) throws CriticalParseFailure {
		for (JSONObject occurrenceJson : eventJson.getArray("Occurrences").iterateAsObject()) {
			Unit u = new Unit(new Qname(d.getSourceId()+"/"+occurrenceJson.getString("occurrenceID")));
			String reportedTaxonId = occurrenceJson.getString("scientificNameMXID");
			if (given(reportedTaxonId) && reportedTaxonId.startsWith("tun.fi/MX.")) {
				u.setReportedTaxonId(new Qname(reportedTaxonId.replace("tun.fi/", "")));
			}
			u.setTaxonVerbatim(occurrenceJson.getString("scientificName"));
			u.setAuthor(occurrenceJson.getString("scientificNameAuthorship"));
			addFact(u, occurrenceJson, "scientificNameID");
			addFact(u, occurrenceJson, "identificationQualifier");
			addFact(u, occurrenceJson, "identificationRemarks");
			u.setAbundanceString(occurrenceJson.getString("occurrenceStatus"));
			u.setRecordBasis(getRecordBasis(occurrenceJson.getString("basisOfRecord")));
			u.setLifeStage(getLifeStage(occurrenceJson.getString("lifeStage")));
			int occurrenceTaxonId = occurrenceJson.isNull("taxonID") ? -1 : occurrenceJson.getInteger("taxonID");
			for (JSONObject measurement : eventJson.getArray("Measurements").iterateAsObject()) {
				if (measurement.isNull("taxonID")) {
					parseMeasurement(g, measurement);
				} else {
					int measurementTaxonId = measurement.getInteger("taxonID");
					if (measurementTaxonId == occurrenceTaxonId) {
						parseMeasurement(u, measurement);
					}
				}
			}
			g.addUnit(u);
		}
	}

	private static final List<String> LOCALITY_CANDIDATE_FIELDS = Utils.list(
			"observationAreaName", "surfaceWaterMonitoringStationName", "waterBodyName", "name", "lakeName", "waterManagementAreaName", "helcomSubbasinName", "helcomCoastalAreaName"
			);

	private Gathering parseBaseGathering(JSONObject rootEventJson) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname("base"));
		parseEventDateTime(rootEventJson.getString("eventDate"), g);
		parseCoordinates(rootEventJson, g);
		JSONObject stationJson = rootEventJson.getObject("BenthosStation");
		g.setLocality(getLocality(stationJson));
		g.setMunicipality(stationJson.getString("municipality"));
		return g;
	}

	private String getLocality(JSONObject stationJson) {
		for (String field : LOCALITY_CANDIDATE_FIELDS) {
			String s = stationJson.getString(field);
			if (given(s)) {
				return s;
			}
		}
		return null;
	}

	private void parseCoordinates(JSONObject rootEventJson, Gathering g) {
		if (rootEventJson.hasKey("decimalLatitude")) {
			try {
				g.setCoordinates(new Coordinates(rootEventJson.getDouble("decimalLatitude"), rootEventJson.getDouble("decimalLongitude"), Type.WGS84));
				try {
					if (!rootEventJson.isNull("coordinateUncertaintyInMeters")) {
						g.getCoordinates().setAccuracyInMeters(rootEventJson.getInteger("coordinateUncertaintyInMeters"));
					}
				} catch (Exception e) {}
			} catch (DataValidationException e) {
				createCoordinateIssue(g, Type.WGS84, e);
			}
		}
	}

	private void parseDocument(JSONObject rootEventJson, Document d) {
		if (rootEventJson.hasKey("benthosStationID")) {
			String stationId = String.valueOf(rootEventJson.getInteger("benthosStationID"));
			d.setNamedPlaceIdUsingQname(new Qname(d.getSourceId().toString()+"/BenthosStation/"+stationId));
		}
		d.addKeyword(rootEventJson.getString("datasetName"));
		addFact(d, rootEventJson, "institutionCode");
		addFact(d, rootEventJson, "type");
		modifiedDate(rootEventJson, d);
		createdDate(rootEventJson, d);
		d.setNotes(rootEventJson.getString("dataGeneralizations"));

		parseMeasurements(d, rootEventJson);
		addFact(d, rootEventJson, "habitat");
		addFact(d, rootEventJson, "minimumDepthInMeters");
		addFact(d, rootEventJson, "maximumDepthInMeters");

		JSONObject stationJson = rootEventJson.getObject("BenthosStation");
		addFact(d, stationJson, "benthosStationID");
		addFact(d, stationJson, "observationAreaID");
		addFact(d, stationJson, "observationAreaName");
		addFact(d, stationJson, "name");
		addFact(d, stationJson, "municipality");
		addFact(d, stationJson, "vegetationTypeName");
		addFact(d, stationJson, "siteTypeName");
		addFact(d, stationJson, "environmentTypeName");
		addFact(d, stationJson, "waterBasinCode");
		addFact(d, stationJson, "waterManagementAreaCode");
		addFact(d, stationJson, "waterManagementAreaName");
		addFact(d, stationJson, "waterBodyCode");
		addFact(d, stationJson, "waterBodyName");
		addFact(d, stationJson, "riverWidth");
		addFact(d, stationJson, "surfaceWaterMonitoringStationID");
		addFact(d, stationJson, "surfaceWaterMonitoringStationName");
		addFact(d, stationJson, "lakeCode");
		addFact(d, stationJson, "lakeName");
	}

	private void createdDate(JSONObject rootEventJson, Document document) {
		try {
			document.setCreatedDate(parseDate(rootEventJson.getString("ValidFrom")));
		} catch (DateValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_CREATED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
	}

	private void modifiedDate(JSONObject rootEventJson, Document document) {
		try {
			document.setModifiedDate(parseDate(rootEventJson.getString("modified")));
		} catch (DateValidationException e) {
			document.createQuality().setIssue(new Quality(Quality.Issue.INVALID_MODIFIED_DATE, Quality.Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
	}

	private void addFact(BaseModel model, JSONObject json, String field) {
		String value = json.getString(field);
		if (given(value)) model.addFact(field, value);
	}

	private void parseMeasurements(BaseModel model, JSONObject json) {
		for (JSONObject factJson : json.getArray("Measurements").iterateAsObject()) {
			parseMeasurement(model, factJson);
		}
	}

	private void parseMeasurement(BaseModel model, JSONObject factJson) {
		String type = factJson.getString("measurementType");
		String unit = factJson.getString("measurementUnit");
		String value = factJson.getString("measurementValue");
		if (!given(value)) return;
		String fact = given(unit) ? type + " (" + unit + ")" : type;
		model.addFact(fact, value);
	}

}