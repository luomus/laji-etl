package fi.laji.datawarehouse.etl.models;

import java.util.regex.Pattern;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.xml.XMLReader;

public class DataIsWellFormedValidators {

	public static interface WellFormedValidator {
		public boolean isWellFormed(String data);
	}

	public static final WellFormedValidator JSON_VALIDATOR =  new WellFormedValidator() {
		@Override
		public boolean isWellFormed(String data) {
			try {
				new JSONObject(data);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	};

	public static final WellFormedValidator XML_VALIDATOR =  new WellFormedValidator() {
		@Override
		public boolean isWellFormed(String data) {
			try {
				new XMLReader().parse(data);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	};

	public static final WellFormedValidator TEXT_PLAIN_VALIDATOR = new WellFormedValidator() {

		@Override
		public boolean isWellFormed(String data) {
			if (data == null) return false;
			if (!data.contains("DELETE ")) return false;
			data = data.replace("\r", "\n");
			for (String line : data.split(Pattern.quote("\n"))) {
				line = line.trim();
				if (line.isEmpty()) continue;
				if (!line.startsWith("DELETE ")) return false;
			}
			return true;
		}
	};

	public static final WellFormedValidator TEXT_CSV_TSV_VALIDATOR = new WellFormedValidator() {

		@Override
		public boolean isWellFormed(String data) {
			if (data == null) return false;
			return data.contains("\n");
		}
	};

}
