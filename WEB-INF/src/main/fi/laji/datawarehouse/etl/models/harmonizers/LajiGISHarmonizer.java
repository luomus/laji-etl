package fi.laji.datawarehouse.etl.models.harmonizers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer.EventOccurrences;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class LajiGISHarmonizer extends BaseHarmonizer<EventOccurrences> {

	public static final String DATA_SOURCE_1 = "Aineistolähde";
	public static final String DATA_SOURCE_2 = "Tietolähteen kuvaus";
	public static final String SITE_TYPE_1 = "Kartoituksen tarkoitus";
	public static final String SITE_TYPE_2 = "Kohteen taso";
	public static final String SITE_STATUS = "Lajinseurantakohteen tila";

	private static final String NAMEDPLACE_ID_PREFIX = "/LGNMP.";
	private static final String DOCUMENT_ID_FROM_EVENTID_PREFIX = "/LGE.";
	private static final String DOCUMENT_ID_FROM_OCCURRENCEID_PREFIX = "/LGO.";

	private enum FactTarget { DOCUMENT, GATHERING, UNIT }

	// Note that all unit facts are considered public
	private static final Set<String> PUBLIC_FACTS = Utils.set(
			"LG_KOHDE.KOHDE_TYYPPI", "LG_KOHDE.MENETELMA", "LG_KOHDE_VESI.MENETELMA_TARK_VESI", "LG_KOHDE.KOHDE_TASO",
			"LG_KOHDE_SEUR.EL_PAIKAN_TILA", "LG_KOHDE.SEURATTAVA", "LG_KERTA_VESI.MERI_ID", "LG_KART_TILA.KARTOITUKSEN_TILA", "LG_LAJIHAV.KOORD_TARKK",
			"LG_KERTA_MAA.PESINTATULOS");

	private static final Qname MISC_COLLECTION_ID = new Qname("HR.2029");
	private static final Qname SPECIES_MONITORING_SITES_COLLECTION_ID = new Qname("HR.3553");
	private static final Qname VELMU_COLLECTION_ID = new Qname("HR.3491");
	private static final Qname BIRDS_OF_PREY_COLLECTION_ID = new Qname("HR.4051");
	private static final Qname SPECIES_SURVEYS_COLLECTION_ID = new Qname("HR.4251");
	private static final Qname SEAL_COLLECTION_ID = new Qname("HR.4571");

	private static final Map<String, Qname> COLLECTION_ID_MAP;
	static {
		COLLECTION_ID_MAP = new HashMap<>();
		COLLECTION_ID_MAP.put("20", VELMU_COLLECTION_ID);
		COLLECTION_ID_MAP.put("21", VELMU_COLLECTION_ID);

		COLLECTION_ID_MAP.put("31", SPECIES_MONITORING_SITES_COLLECTION_ID);
		COLLECTION_ID_MAP.put("32", SPECIES_MONITORING_SITES_COLLECTION_ID);

		COLLECTION_ID_MAP.put("10", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("11", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("12", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("13", SPECIES_SURVEYS_COLLECTION_ID);

		COLLECTION_ID_MAP.put("46", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("50", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("51", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("52", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("61", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("62", SPECIES_SURVEYS_COLLECTION_ID);
		COLLECTION_ID_MAP.put("64", SPECIES_SURVEYS_COLLECTION_ID);

		COLLECTION_ID_MAP.put("-1", BIRDS_OF_PREY_COLLECTION_ID);

		COLLECTION_ID_MAP.put("-2", SEAL_COLLECTION_ID);
	}

	private static final String ACCURATE_AREA = "51";
	private static final String ACCURATE_LINE = "42";	

	public static class EventOccurrences {

		public Integer eventId;
		public List<OccurrenceData> occurrences;

		public EventOccurrences(Integer eventId, List<OccurrenceData> occurrences) {
			this.eventId = eventId;
			this.occurrences = occurrences;
		}

	}

	public static class OccurrenceData {

		public Integer eventId;
		public int occurrenceId;
		public JSONObject json;
		public List<String> occurrenceWKT;
		public List<String> eventWKT;
		public List<String> projectNames;

		public OccurrenceData(Integer eventId, int occurrenceId, String json) {
			this.eventId = eventId;
			this.occurrenceId = occurrenceId;
			this.json = new JSONObject(json);
		}

	}

	@Override
	public List<DwRoot> harmonize(EventOccurrences data, Qname source) throws CriticalParseFailure {
		DwRoot root = createRoot(data.eventId, data.occurrences, source);
		try {
			parseRoot(data.occurrences, root);
		} catch (Exception e) {
			Quality issue = new Quality(Issue.ETL_ISSUE, Source.AUTOMATED_FINBIF_VALIDATION, LogUtils.buildStackTrace(e, 10));
			if (root.getPrivateDocument() == null) root.createPrivateDocument();
			if (root.getPublicDocument() == null) root.createPublicDocument();
			root.getPrivateDocument().createQuality().setIssue(issue);
			root.getPublicDocument().createQuality().setIssue(issue);
		}
		return Utils.singleEntryList(root);
	}

	private DwRoot createRoot(Integer eventId, List<OccurrenceData> data, Qname source) throws CriticalParseFailure {
		Qname documentId = parseDocumentId(eventId, data, source);
		return new DwRoot(documentId, source);
	}

	private Qname parseDocumentId(Integer eventId, List<OccurrenceData> data, Qname source) throws CriticalParseFailure {
		if (eventId == null) {
			if (data.size() != 1) throw new CriticalParseFailure("No event but has " + data.size() + " occurrences");
			OccurrenceData d = data.get(0);
			if (d.occurrenceId  < 0) throw new CriticalParseFailure("No event but also no occurrenceId");
			return parseDocumentIdFromOccurrenceId(d.occurrenceId, source);
		}
		return parseDocumentIdFromEventId(eventId, source);
	}

	private Qname parseDocumentIdFromOccurrenceId(Integer occurrenceId, Qname source) {
		if (occurrenceId == null) throw new IllegalStateException();
		return Qname.fromURI(source.toURI() + DOCUMENT_ID_FROM_OCCURRENCEID_PREFIX + occurrenceId);
	}

	private Qname parseDocumentIdFromEventId(Integer eventId, Qname source) {
		if (eventId == null) throw new IllegalStateException();
		return Qname.fromURI(source.toURI() + DOCUMENT_ID_FROM_EVENTID_PREFIX + eventId);
	}

	private void parseRoot(List<OccurrenceData> data, DwRoot root) throws CriticalParseFailure {
		root.setCollectionId(parseCollectionId(data.get(0)));
		parseDocument(data, root.createPrivateDocument());
		parseDocument(data, root.createPublicDocument());
	}

	private void parseDocument(List<OccurrenceData> data, Document document) throws CriticalParseFailure {
		parseDocument(document, data.get(0));
		parseGatherings(document, data);
	}

	private Qname parseCollectionId(OccurrenceData data) {
		String namedPlaceType = data.json.getString("namedPlaceType");
		Qname collectionId = COLLECTION_ID_MAP.get(namedPlaceType);
		if (collectionId == null) return MISC_COLLECTION_ID;
		return collectionId;
	}

	private void parseDocument(Document document, OccurrenceData data) {
		if (data.eventId != null) {
			document.addKeyword(String.valueOf(data.eventId));
		}
		parseProjects(document, data);
		String namedPlaceId = data.json.getString("namedPlaceId");
		String additionalId = data.json.getString("additionalId");
		if (given(namedPlaceId)) {
			document.setNamedPlaceIdUsingQname(Qname.fromURI(document.getSourceId().toURI()+NAMEDPLACE_ID_PREFIX+namedPlaceId));
		}

		document.addKeyword(additionalId);
		String notes = notes(data, "documentNotes", "documentNotes2");
		if (given(notes)) {
			document.addSecureReason(SecureReason.CUSTOM);
			if (!document.isPublic()) {
				document.setNotes(notes);
			}
		}

		String taxonId = data.json.getString("namedPlaceTaxonId");
		if (valid(taxonId)) {
			document.addFact("Seurattava laji", new Qname(taxonId).toURI());
		} else {
			String scientificName = data.json.getString("namedPlaceTaxonScientificName");
			document.addFact("Seurattava laji", scientificName);
		}

		addFacts(document, data, FactTarget.DOCUMENT, document.getFacts());
		String dataSource1 = document.factValue(DATA_SOURCE_1);
		String dataSource2 = document.factValue(DATA_SOURCE_2);
		String siteType1 = document.factValue(SITE_TYPE_1);
		String siteType2 = document.factValue(SITE_TYPE_2);
		String siteStatus = document.factValue(SITE_STATUS);
		document.setDataSource(join(dataSource1, dataSource2));
		document.setSiteType(join(siteType1, siteType2));
		document.setSiteStatus(siteStatus);
		if (given(siteStatus)) {
			document.setSiteDead(siteStatus.startsWith("Hävinnyt"));
		}
	}

	private String join(String s1, String s2) {
		if (given(s1, s2)) {
			return s1 + ": " + s2;
		}
		if (given(s1)) return s1;
		return s2;
	}

	private void parseProjects(Document document, OccurrenceData data) {
		if (data.projectNames == null) return;
		for (String projectName : data.projectNames) {
			document.addKeyword(projectName);
		}
	}

	private void addFacts(Document document, OccurrenceData data, FactTarget acceptedTarget, List<Fact> facts) {
		for (JSONObject fact : data.json.getArray("facts").iterateAsObject()) {
			String columnName = fact.getString("columnName");
			if (matches(columnName, acceptedTarget)) {
				if (shouldIncludePublicFact(columnName, document.getCollectionId())) {
					addFact(facts, fact, columnName);
				} else {
					document.addSecureReason(SecureReason.CUSTOM);
					if (!document.isPublic()) {
						addFact(facts, fact, columnName);
					}
				}
			}
		}
	}

	private void addUnitFacts(OccurrenceData data, FactTarget acceptedTarget, List<Fact> facts) {
		for (JSONObject fact : data.json.getArray("facts").iterateAsObject()) {
			String columnName = fact.getString("columnName");
			if (matches(columnName, acceptedTarget)) {
				addFact(facts, fact, columnName);
			}
		}
	}

	private boolean shouldIncludePublicFact(String columnName, Qname collectionId) {
		if ("LG_KART_TILA.KARTOITUKSEN_TILA".equals(columnName)) {
			if (BIRDS_OF_PREY_COLLECTION_ID.equals(collectionId)) return false;
		}
		return PUBLIC_FACTS.contains(columnName);
	}

	private static Map<String, FactTarget> FACT_TARGETS;
	static {
		FACT_TARGETS = new HashMap<>();
		FACT_TARGETS.put("LG_KOHDE", FactTarget.DOCUMENT);
		FACT_TARGETS.put("LG_KART_TILA", FactTarget.DOCUMENT);
		FACT_TARGETS.put("LG_KOHDE_SEUR", FactTarget.DOCUMENT);
		FACT_TARGETS.put("LG_KERTA", FactTarget.DOCUMENT);
		FACT_TARGETS.put("LG_LAJIHAV.HAV_TLAHDE_KUVAUS", FactTarget.DOCUMENT);
		FACT_TARGETS.put("LG_LAJIHAV.NUM_LAHDE", FactTarget.DOCUMENT);

		FACT_TARGETS.put("LG_KERTA_VESI", FactTarget.GATHERING);
		FACT_TARGETS.put("LG_KERTA_MAA", FactTarget.GATHERING);
		FACT_TARGETS.put("LG_VIDEO", FactTarget.GATHERING);
		FACT_TARGETS.put("LG_LAJIHAV.KOORD_TARKK", FactTarget.GATHERING);

		FACT_TARGETS.put("LG_LAJIHAV", FactTarget.UNIT);
		FACT_TARGETS.put("LG_NAYTE", FactTarget.UNIT);
	}

	private boolean matches(String columnName, FactTarget acceptedTarget) {
		// Column name is for example LG_KERTA_VESI.PL_ARVIO_EPAVARMA
		if (!columnName.contains(".")) return false;
		if (FACT_TARGETS.containsKey(columnName)) {
			return acceptedTarget.equals(FACT_TARGETS.get(columnName));
		}
		String tableName = columnName.split(Pattern.quote("."))[0];
		return acceptedTarget.equals(FACT_TARGETS.get(tableName));
	}

	private void addFact(List<Fact> facts, JSONObject json, String columnName) {
		String factName = json.getString("fact");
		if (columnName.equals(factName)) {
			factName = columnName.split(Pattern.quote("."))[1];
		}
		String value = json.getString("value");
		if (given(value)) {
			facts.add(new Fact(factName, value));
		}
	}

	private void parseGatherings(Document document, List<OccurrenceData> data) throws CriticalParseFailure {
		List<Gathering> gatherings = new ArrayList<>();
		for (OccurrenceData d : data) {
			Gathering g = parseGathering(document, d);
			boolean found = false;
			for (Gathering existing : gatherings) {
				if (existing.equals(g)) {
					g = existing;
					found = true;
					break;
				}
			}
			if (!found) {
				gatherings.add(g);
			}
			g.addUnit(parseUnit(d, document));
		}
		int i = 1;
		for (Gathering g : gatherings) {
			g.setGatheringId(Qname.fromURI(document.getDocumentId().toURI()+"G"+i++));
			document.addGathering(g);
		}
	}

	private Gathering parseGathering(Document document, OccurrenceData d) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname("temp"));
		addFacts(document, d, FactTarget.GATHERING, g.getFacts());

		g.addTeamMember(d.json.getString("team"));
		g.addTeamMember(d.json.getString("leg"));
		if (!g.getTeam().isEmpty()) {
			if (hidePersons(d)) {
				document.addSecureReason(SecureReason.USER_PERSON_NAMES_HIDDEN);
				if (document.isPublic()) {
					g.getTeam().clear();
				}
			}
		}

		g.setLocality(d.json.getString("locality"));
		if (d.json.hasKey("onlyPrivateLocality")) {
			document.addSecureReason(SecureReason.CUSTOM);
			if (!document.isPublic()) {
				g.setLocality(d.json.getString("onlyPrivateLocality"));
			}
		}

		g.setMunicipality(d.json.getString("municipality"));
		g.setBiogeographicalProvince(d.json.getString("biogeographicalProvince"));
		String basinName = d.json.getString("basinName");
		if (given(basinName)) {
			g.addFact("Vesistöalue", basinName);
			if (!given(g.getLocality())) {
				g.setLocality(basinName);
			} else {
				g.setLocality(g.getLocality() + ", " + basinName);
			}
		}

		parseEventDateTime(parseDate("dateBegin", d)+"/"+parseDate("dateEnd", d), g);

		String wkt = getWKT(d);
		if (given(wkt)) {
			parseGeo(g, wkt, d);
		} else {
			parseCoordinates(g, d);
		}

		if (isAccurateArea(d)) {
			g.setAccurateArea(true);
		}

		if (d.json.hasKey("stateLand")) {
			g.setStateLand("1".equals(d.json.getString("stateLand")));
		}

		g.setNotes(notes(d, "gatheringNotes"));
		return g;
	}

	private boolean hidePersons(OccurrenceData d) {
		return "90".equals(d.json.getString("personHiding"));
	}

	private String getWKT(OccurrenceData d) {
		List<String> wkts = getWKTs(d);
		if (wkts.isEmpty()) return null;
		if (wkts.size() == 1) return wkts.get(0);
		return "GEOMETRYCOLLECTION(" + wkts.stream().collect(Collectors.joining(",")) + ")";
	}

	private List<String> getWKTs(OccurrenceData d) {
		if (d.occurrenceWKT != null && !d.occurrenceWKT.isEmpty()) return d.occurrenceWKT;
		if (d.eventWKT != null && !d.eventWKT.isEmpty()) return d.eventWKT;
		return Collections.emptyList();
	}

	private String parseDate(String field, OccurrenceData d) {
		String s = d.json.getString(field);
		if (!given(s)) return s;
		return s.split(Pattern.quote("T"))[0];
	}

	private static final Map<String, Integer> COORD_ACCURACY;
	static {
		COORD_ACCURACY = new HashMap<>();
		COORD_ACCURACY.put("11", 1);
		COORD_ACCURACY.put("12", 1);
		COORD_ACCURACY.put("13", 10);
		COORD_ACCURACY.put("14", 1000);
		COORD_ACCURACY.put("21", 10);
		COORD_ACCURACY.put("22", 100);
		COORD_ACCURACY.put("23", 1000);
		COORD_ACCURACY.put("24", 10000);
		COORD_ACCURACY.put("31", 100);
		COORD_ACCURACY.put("32", 1000);
		COORD_ACCURACY.put("33", 10000);
		COORD_ACCURACY.put(ACCURATE_LINE, 1);
		COORD_ACCURACY.put(ACCURATE_AREA, 1);
		COORD_ACCURACY.put("52", 1000);
	}

	private static final Map<String, Qname> ATLAS_CODES;
	static {
		ATLAS_CODES = new HashMap<>();
		ATLAS_CODES.put("2", new Qname("MY.atlasCodeEnum5"));
		ATLAS_CODES.put("3", new Qname("MY.atlasCodeEnum62"));
		ATLAS_CODES.put("9", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("4", new Qname("MY.atlasCodeEnum72"));
		ATLAS_CODES.put("5", new Qname("MY.atlasCodeEnum75"));
		ATLAS_CODES.put("6", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("7", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("8", new Qname("MY.atlasCodeEnum8"));
	}

	private void parseCoordinates(Gathering g, OccurrenceData d) {
		Type crs = null;
		String coordSystem = d.json.getString("coordSystem");
		if ("11".equals(coordSystem)) crs = Type.EUREF;
		if ("12".equals(coordSystem)) crs = Type.YKJ;
		if ("21".equals(coordSystem)) crs = Type.WGS84;
		String lat = d.json.getString("lat");
		String lon = d.json.getString("lon");
		String eurefLat = d.json.getString("latEurefCenter");
		String eurefLon = d.json.getString("lonEurefCenter");
		if (given(eurefLat, eurefLon)) {
			parseCoordinates(g, d, Type.EUREF, eurefLat, eurefLon);
			return;
		}
		if (given(lat, lon, crs)) {
			parseCoordinates(g, d, crs, lat, lon);
			return;
		}
		if (given(lat, lon)) {
			if (crs == null) {
				g.createQuality().setLocationIssue(new Quality(Issue.INVALID_COORDINATES, Source.AUTOMATED_FINBIF_VALIDATION, "Coordinates given but coordinate system is missing"));
			}
		}
	}

	private void parseCoordinates(Gathering g, OccurrenceData d, Type crs, String lat, String lon) {
		try {
			g.setCoordinates(
					new Coordinates(Double.valueOf(lat), Double.valueOf(lon), crs)
					.setAccuracyInMeters(getCoordinateAccuracy(d)));
		} catch (Exception e) {
			DataValidationException de = e instanceof DataValidationException ? (DataValidationException) e : new DataValidationException("Coord issue", e);
			createCoordinateIssue(g, crs, de);
		}
	}

	private Integer getCoordinateAccuracy(OccurrenceData d) {
		return COORD_ACCURACY.get(d.json.getString("coordinateAccuracy"));
	}

	private boolean isAccurateArea(OccurrenceData d) {
		return ACCURATE_AREA.equals(d.json.getString("coordinateAccuracy")) || ACCURATE_LINE.equals(d.json.getString("coordinateAccuracy"));
	}

	private void parseGeo(Gathering g, String wkt, OccurrenceData d) {
		try {
			g.setGeo(
					Geo.fromWKT(wkt, Type.EUREF)
					.setAccuracyInMeters(getCoordinateAccuracy(d))
					.convertComplex());
		} catch (DataValidationException e) {
			createGeoIssue(g, e);
			return;
		}
	}

	private Unit parseUnit(OccurrenceData d, Document document) throws CriticalParseFailure {
		Unit u = new Unit(parseUnitId(d, document));
		addUnitFacts(d, FactTarget.UNIT, u.getFacts()); // Note that all unit facts are considered public
		String occurrenceId = String.valueOf(d.occurrenceId);
		if (given(occurrenceId) && !"-1".equals(occurrenceId)) {
			u.addKeyword(occurrenceId);
		}
		u.addKeyword(d.json.getString("specimenUri"));
		u.addKeyword(d.json.getString("specimenAdditionalId"));

		u.setDet(d.json.getString("det"));
		if (given(u.getDet())) {
			if (hidePersons(d)) {
				document.addSecureReason(SecureReason.USER_PERSON_NAMES_HIDDEN);
				if (document.isPublic()) {
					u.setDet(null);
				}
			}
		}

		u.setTaxonVerbatim(d.json.getString("taxonVerbatim"));
		String taxonId = d.json.getString("taxonId");
		if (valid(taxonId)) {
			u.setReportedTaxonId(new Qname(taxonId));
		}
		if (!given(u.getTaxonVerbatim())) {
			u.setTaxonVerbatim(d.json.getString("scientificName"));
			u.setAuthor(d.json.getString("scientificNameAuthor"));
		}
		String herttaId = d.json.getString("herttaId");
		if (given(herttaId)) {
			u.addKeyword("el_"+herttaId);
			u.addFact("Eliölajit lajikoodi", herttaId);
		}

		Tag tag = SOURCE_TAGS.get(d.json.getString("qualityControl"));
		if (tag != null) {
			u.addSourceTag(tag);
		}

		String recordBasisCode = d.json.getString("recordBasis");
		RecordBasis recordBasis = RECORD_BASIS.get(recordBasisCode);
		if (recordBasis == null) recordBasis = RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED;
		u.setRecordBasis(recordBasis);

		if ("32".equals(recordBasisCode)) u.setLifeStage(LifeStage.MARKS);
		if (BREEDING_SITE_RECORD_BASIS.contains(recordBasisCode)) u.setBreedingSite(true);

		u.setAtlasCodeUsingQname(ATLAS_CODES.get(d.json.getString("nestingStatus")));
		String amount = d.json.getString("amount");
		String amountUnit = d.json.getString("amountUnit");
		if (given(amount, amountUnit)) {
			AmountHandler handler = AMOUNT_HANDLERS.get(amountUnit);
			amount = doubleOrInteger(amount);
			if (handler != null) {
				handler.handle(amount, u);
			} else {
				u.setAbundanceString(amount);
			}
		} else if (isZero(amount)) {
			u.setAbundanceString("0");
		}

		u.setNotes(notes(d, "unitNotes", "unitNotes2"));
		return u;
	}

	private String doubleOrInteger(String amount) {
		if (amount == null) return null;
		if (amount.isEmpty()) return null;
		try {
			double value = Double.valueOf(amount);
			if (isMathematicalInteger(value)) {
				return "" + (int) value;
			}
			return amount;
		} catch (Exception e) {
			return null;
		}
	}

	private boolean isZero(String amount) {
		if (amount == null) return false;
		if (amount.isEmpty()) return false;
		try {
			double value = Double.valueOf(amount);
			return value == 0;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean isMathematicalInteger(double d) {
		return (d % 1) == 0;
	}

	private Qname parseUnitId(OccurrenceData d, Document document) {
		if (d.occurrenceId < 0) {
			// zero occurrence from lajigis; etl generated occurrence
			return Qname.fromURI(document.getDocumentId().toURI()+"/0");
		}
		return Qname.fromURI(document.getDocumentId().toURI()+"/"+d.occurrenceId);
	}

	private String notes(OccurrenceData d, String  ... keys) {
		List<String> values = new ArrayList<>();
		for (String key : keys) {
			String value = d.json.getString(key);
			if (value == null) continue;
			value = value.trim();
			if (value.length() < 2) continue;
			values.add(value);
		}
		if (values.isEmpty()) return null;
		return values.stream().collect(Collectors.joining("; "));
	}

	private static final Set<String> BREEDING_SITE_RECORD_BASIS = Utils.set("40", "41", "42", "43", "44", "45", "46");

	private static final Map<String, RecordBasis> RECORD_BASIS;
	static {
		RECORD_BASIS = new HashMap<>();
		RECORD_BASIS.put("10", RecordBasis.HUMAN_OBSERVATION_SEEN);
		RECORD_BASIS.put("11", RecordBasis.PRESERVED_SPECIMEN);
		RECORD_BASIS.put("20", RecordBasis.HUMAN_OBSERVATION_HEARD);
		RECORD_BASIS.put("30", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("31", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("32", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("33", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("34", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("35", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("36", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("37", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("40", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("41", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("42", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("43", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("44", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("45", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("46", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		RECORD_BASIS.put("47", RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		RECORD_BASIS.put("48", RecordBasis.HUMAN_OBSERVATION_INDIRECT);
	}

	private static final Map<String, Tag> SOURCE_TAGS;
	static {
		SOURCE_TAGS = new HashMap<>();
		SOURCE_TAGS.put("20", Tag.CHECK_LOCATION);
		SOURCE_TAGS.put("30", Tag.CHECK);
		SOURCE_TAGS.put("31", Tag.CHECK_TAXON);
		SOURCE_TAGS.put("80", Tag.CHECK);
		SOURCE_TAGS.put("90", Tag.CHECK_LOCATION);
		SOURCE_TAGS.put("91", Tag.CHECK_TAXON);
		SOURCE_TAGS.put("92", Tag.CHECK_LOCATION);
		SOURCE_TAGS.put("93", Tag.CHECK_DATETIME);
	}

	private interface AmountHandler {
		void handle(String amount, Unit unit);
	}

	private static final Map<String, AmountHandler> AMOUNT_HANDLERS;
	static {
		AMOUNT_HANDLERS = new HashMap<>();

		// 1	esiaikuinen
		AMOUNT_HANDLERS.put("1", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.SUBIMAGO);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 2	itiöemä
		AMOUNT_HANDLERS.put("2", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.FRUITBODIES);
			}
		});

		// 7	koiras (aikuinen)
		AMOUNT_HANDLERS.put("7", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setIndividualCountMale(iValue(amount));
				unit.setSex(Sex.MALE);
				unit.setLifeStage(LifeStage.ADULT);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 8	kotelo tai kotelokoppa
		AMOUNT_HANDLERS.put("8", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.PUPA);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 9	kuningatar
		AMOUNT_HANDLERS.put("9", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setIndividualCountFemale(iValue(amount));
				unit.setSex(Sex.FEMALE);
				unit.setLifeStage(LifeStage.ADULT);
				unit.setAbundanceUnit(AbundanceUnit.QUEENS);
			}
		});

		// 10	kuoret
		AMOUNT_HANDLERS.put("10", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.SHELLS);
			}
		});

		// 11	laikku
		AMOUNT_HANDLERS.put("11", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.SPOTS);
			}
		});

		// 14	muna
		AMOUNT_HANDLERS.put("14", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.EGG);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 15	mätäs/tupas
		AMOUNT_HANDLERS.put("15", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.HUMMOCKS);
			}
		});

		// 17	naaras (aikuinen)
		AMOUNT_HANDLERS.put("17", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setIndividualCountFemale(iValue(amount));
				unit.setSex(Sex.FEMALE);
				unit.setLifeStage(LifeStage.ADULT);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 18	nuori yksilö
		AMOUNT_HANDLERS.put("18", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.JUVENILE);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 19	pari/reviiri
		AMOUNT_HANDLERS.put("19", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.PAIRCOUNT);
			}
		});

		// 20	pesä
		AMOUNT_HANDLERS.put("20", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.NESTS);
			}
		});

		// 21	poikanen
		AMOUNT_HANDLERS.put("21", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.JUVENILE);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 22	reviiri/elinpiiri
		AMOUNT_HANDLERS.put("22", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.BREEDING_SITES);
			}
		});

		// 23	runko
		AMOUNT_HANDLERS.put("23", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.TRUNKS);
			}
		});

		// 24	sekovarsi/yksilö
		AMOUNT_HANDLERS.put("24", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.THALLI);
			}
		});

		// 25	toukka
		AMOUNT_HANDLERS.put("25", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.LARVA);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 26	työläinen
		AMOUNT_HANDLERS.put("26", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setSex(Sex.WORKER);
				unit.setLifeStage(LifeStage.ADULT);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 27	verso
		AMOUNT_HANDLERS.put("27", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.SPROUTS);
				unit.setLifeStage(LifeStage.SPROUT);
			}
		});

		// 28	yksilö
		AMOUNT_HANDLERS.put("28", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 29	yksilö (aikuinen)
		AMOUNT_HANDLERS.put("29", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.ADULT);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 34	jätökset /jäljet
		AMOUNT_HANDLERS.put("34", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_INDIRECT);
				unit.setAbundanceUnit(AbundanceUnit.INDIRECT_MARKS);
			}
		});

		// 35	kotelo
		AMOUNT_HANDLERS.put("35", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.PUPA);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 36	syömäjäljet
		AMOUNT_HANDLERS.put("36", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.MARKS);
				unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_INDIRECT);
				unit.setAbundanceUnit(AbundanceUnit.FEEDING_MARKS);
			}
		});

		// 37	dm2 alue
		AMOUNT_HANDLERS.put("37", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.SQUARE_DM);
			}
		});

		// 38	m2 alue
		AMOUNT_HANDLERS.put("38", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.SQUARE_M);
			}
		});

		// 39	asuttu kivi/lohkare
		AMOUNT_HANDLERS.put("39", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.BREEDING_SITES);
			}
		});

		// 40	asuttu puu
		AMOUNT_HANDLERS.put("40", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.BREEDING_SITES);
			}
		});

		// 41	asuttu puu/maapuu
		AMOUNT_HANDLERS.put("41", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setBreedingSite(true);
				unit.setAbundanceUnit(AbundanceUnit.BREEDING_SITES);
			}
		});

		// 42	verso (kaikki)
		AMOUNT_HANDLERS.put("42", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.SPROUT);
				unit.setAbundanceUnit(AbundanceUnit.SPROUTS);
			}
		});

		// 43	verso (kukkiva)
		AMOUNT_HANDLERS.put("43", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.FLOWER);
				unit.setAbundanceUnit(AbundanceUnit.SPROUTS);
			}
		});

		// 44	yksilö (kaikki)
		AMOUNT_HANDLERS.put("44", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 45	yksilö (kukkiva)
		AMOUNT_HANDLERS.put("45", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.FLOWER);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 46	yksilö (toukka)
		AMOUNT_HANDLERS.put("46", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setLifeStage(LifeStage.LARVA);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}
		});

		// 47	ääntelevä koiras
		AMOUNT_HANDLERS.put("47", new AmountHandler() {
			@Override
			public void handle(String amount, Unit unit) {
				unit.setAbundanceString(amount);
				unit.setIndividualCountMale(iValue(amount));
				unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_HEARD);
				unit.setSex(Sex.MALE);
				unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
			}

		});
	}

	private static Integer iValue(String amount) {
		try {
			return Integer.valueOf(amount);
		} catch (Exception e) {
			return null;
		}
	}

	private boolean valid(String taxonId) {
		if (!given(taxonId)) return false;
		if (!taxonId.startsWith("MX.")) return false;
		String stripped = taxonId.replace("MX.", "");
		try {
			Integer.valueOf(stripped);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}