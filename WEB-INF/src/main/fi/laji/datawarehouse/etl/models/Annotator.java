package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.luomus.commons.containers.rdf.Qname;

public class Annotator {

	public interface PersonIdResolver {
		Qname resolve(String userId);
	}

	private final PersonIdResolver personIdResolver;

	public Annotator(PersonIdResolver personIdResolver) {
		this.personIdResolver = personIdResolver;
	}

	public List<AnnotationNotification> annotate(DwRoot root) {
		if (!root.hasAnnotations()) return Collections.emptyList();
		List<AnnotationNotification> notifications = new ArrayList<>();
		for (Annotation annotation : root.getAnnotations()) {
			Set<Qname> ownersAndObservers = annotate(root, annotation);
			if (!isFormAdmin(annotation)) {
				notifications.addAll(generateNotifications(root.getAnnotations(), annotation, ownersAndObservers));
			}
		}
		return notifications;
	}

	private List<AnnotationNotification> generateNotifications(List<Annotation> annotations,  Annotation annotation, Set<Qname> ownersAndObservers) {
		List<AnnotationNotification> notifications = new ArrayList<>();

		Set<Qname> previousAnnotators = previousAnnotators(annotation, annotations);
		ownersAndObservers.remove(annotation.getAnnotationByPerson());
		previousAnnotators.remove(annotation.getAnnotationByPerson());
		previousAnnotators.removeAll(ownersAndObservers);

		for (Qname ownerOrObserver : ownersAndObservers) {
			notifications.add(new AnnotationNotification(ownerOrObserver, annotation, NotificationReason.MY_DOCUMENT_ANNOTATED));
		}

		for (Qname previousAnnotator : previousAnnotators) {
			notifications.add(new AnnotationNotification(previousAnnotator, annotation, NotificationReason.ANNOTATED_DOCUMENT_ANNOTATED));
		}

		return notifications;
	}

	private Set<Qname> previousAnnotators(Annotation annotation, List<Annotation> annotations) {
		Set<Qname> previousAnnotators = new HashSet<>();
		for (Annotation other : annotations) {
			if (other.getTargetID() != null) {
				if (other.getTargetID().equals(annotation.getTargetID())) {
					if (other.getAnnotationByPerson() != null) {
						if (annotation.after(other)) {
							previousAnnotators.add(other.getAnnotationByPerson());
						}
					}
				}
			}
		}
		return previousAnnotators;
	}

	private Set<Qname> annotate(DwRoot root, Annotation annotation) {
		Document publicDocument = root.getPublicDocument();
		Document privateDocument = root.getPrivateDocument();
		Set<Qname> ownersAndObservers = new HashSet<>();

		if (isFormAdmin(annotation)) {
			if (privateDocument != null) {
				annotate(annotation, privateDocument, ownersAndObservers);
			} else {
				privateDocument = publicDocument.copy(Concealment.PRIVATE);
				root.setPrivateDocument(privateDocument);
				annotate(annotation, privateDocument, ownersAndObservers);
			}
		} else {
			if (privateDocument != null) {
				annotate(annotation, privateDocument, ownersAndObservers);
			}
			if (publicDocument != null) {
				annotate(annotation, publicDocument, ownersAndObservers);
			}
		}

		return ownersAndObservers;
	}

	private void annotate(Annotation annotation, Document document, Set<Qname> ownersAndObsevers) {
		ownersAndObsevers.addAll(annotate(document, annotation));
	}

	private boolean isFormAdmin(Annotation annotation) {
		return annotation.resolveType() == AnnotationType.FORM_ADMIN;
	}

	private Collection<Qname> annotate(Document document, Annotation annotation) {
		if (document.getDocumentId().equals(annotation.getTargetID())) {
			document.addAnnotation(annotation.secureAnnotation());
			return getOwnersAndObservers(document, document.getGatherings());
		}
		for (Gathering gathering : document.getGatherings()) {
			for (Unit unit : gathering.getUnits()) {
				if (unit.getUnitId().equals(annotation.getTargetID())) {
					unit.addAnnotation(annotation.secureAnnotation());
					return getOwnersAndObservers(document, gathering);
				}
			}
		}
		return Collections.emptyList();
	}

	private Collection<Qname> getOwnersAndObservers(Document document, Gathering gathering) {
		List<Qname> ownersAndObservers = new ArrayList<>();
		addOwners(document, ownersAndObservers);
		addObservers(gathering, ownersAndObservers);
		return ownersAndObservers;
	}

	private Collection<Qname> getOwnersAndObservers(Document document, List<Gathering> gatherings) {
		List<Qname> ownersAndObservers = new ArrayList<>();
		addOwners(document, ownersAndObservers);
		for (Gathering gathering : gatherings) {
			addObservers(gathering, ownersAndObservers);
		}
		return ownersAndObservers;
	}

	private void addObservers(Gathering gathering, List<Qname> ownersAndObservers) {
		for (String ownerUserId : gathering.getObserverUserIds()) {
			Qname personId = personIdResolver.resolve(ownerUserId);
			if (personId != null) {
				ownersAndObservers.add(personId);
			}
		}
	}

	private void addOwners(Document document, List<Qname> ownersAndObservers) {
		for (String userId : document.getEditorUserIds()) {
			Qname personId = personIdResolver.resolve(userId);
			if (personId != null) {
				ownersAndObservers.add(personId);
			}
		}
	}

}
