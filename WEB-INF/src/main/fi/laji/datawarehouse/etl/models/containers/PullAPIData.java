package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

import java.net.URI;

public class PullAPIData {

	private final Qname source;
	private final URI apiURL;

	public PullAPIData(Qname source, URI apiURL) {
		this.source = source;
		this.apiURL = apiURL;
	}

	public Qname getSource() {
		return source;
	}

	public URI getApiURL() {
		return apiURL;
	}

}
