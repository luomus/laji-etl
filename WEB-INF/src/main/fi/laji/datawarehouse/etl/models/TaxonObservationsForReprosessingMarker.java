package fi.laji.datawarehouse.etl.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class TaxonObservationsForReprosessingMarker {

	private static final Selected SELECTED = initSelected();
	private static final OrderBy ORDER_BY = initOrderBy();
	private static final int LIMIT = 1000;
	private final DAO dao;
	private final ErrorReporter errorReporter;
	private final ThreadHandler threadHandler;
	private final ThreadStatuses threadStatuses;

	public TaxonObservationsForReprosessingMarker(DAO dao, ErrorReporter errorReporter, ThreadHandler threadHandler, ThreadStatuses threadStatuses) {
		this.dao = dao;
		this.errorReporter = errorReporter;
		this.threadHandler = threadHandler;
		this.threadStatuses = threadStatuses;
	}

	private static Selected initSelected() {
		try {
			return new Selected("document.documentId", "document.sourceId");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	private static OrderBy initOrderBy() {
		try {
			return new OrderBy("document.documentId");
		} catch (NoSuchFieldException e) {
			throw new ETLException(e);
		}
	}

	public int reprocess(Qname taxonId) {
		ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
		try {
			return tryToReprocess(taxonId, statusReporter);
		} finally {
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private int tryToReprocess(Qname taxonId, ThreadStatusReporter statusReporter) {
		statusReporter.setStatus("Starting to search for rows for taxon " + taxonId);
		Set<RowInfo> rows = getDocumentsToRelease(taxonId);
		Set<Qname> sources = new HashSet<>();
		int i = 0;
		for (RowInfo row : rows) {
			statusReporter.setStatus("Found " + rows.size() + " rows for taxon " + taxonId + ": Marking for reprocess (" + (i++) + "/" +rows.size() +")");
			markForReprocess(row);
			sources.add(row.getSource());
		}
		statusReporter.setStatus("Reporting out pipe jobs");
		reportOutPipeJobs(sources);
		return rows.size();
	}

	private void reportOutPipeJobs(Set<Qname> sources) {
		for (Qname source : sources) {
			threadHandler.runOutPipe(source);
		}
	}

	private void markForReprocess(RowInfo row) {
		try {
			int i = dao.getETLDAO().markReprocessOutPipe(row.getDocumentId());
			if (i != 1) {
				String message = "Entry marked for taxon reprocess was not found from out pipe: " + debug(row);
				dao.logError(Const.LAJI_ETL_QNAME, TaxonObservationsForReprosessingMarker.class, row.getDocumentId().toURI(), new IllegalStateException(message));
			}
		} catch (Exception e) {
			String debugData = debug(row);
			Exception withData = new Exception(debugData, e);
			errorReporter.report(LogUtils.buildStackTrace(withData));
			dao.logError(Const.LAJI_ETL_QNAME, TaxonObservationsForReprosessingMarker.class, row.getDocumentId().toURI(), withData);
		}
	}

	private String debug(RowInfo row) {
		return Utils.debugS(row.getDocumentId(), row.getSource());
	}

	private static class RowInfo {
		private final Qname documentId;
		private final Qname source;
		private final String toString;
		public RowInfo(Qname documentId, Qname source) {
			this.documentId = documentId;
			this.source = source;
			this.toString = this.documentId + ";" + this.source.toString();
		}
		public Qname getDocumentId() {
			return documentId;
		}
		public Qname getSource() {
			return source;
		}
		@Override
		public int hashCode() {
			return toString.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			return ((RowInfo)obj).toString.equals(this.toString);
		}
	}
	private Set<RowInfo> getDocumentsToRelease(Qname taxonId) {
		VerticaQueryDAO queryDAO = dao.getPrivateVerticaDAO().getQueryDAO();
		Set<RowInfo> all = new HashSet<>();
		int page = 1;
		while (true) {
			List<JoinedRow> partialResult = queryDAO.getRawList(buildQuery(taxonId, page));
			if (partialResult.isEmpty()) break;
			for (JoinedRow row : partialResult) {
				all.add(new RowInfo(row.getDocument().getDocumentId(), row.getDocument().getSourceId()));
			}
			page++;
		}
		return all;
	}

	private ListQuery buildQuery(Qname taxonId, int page) {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PRIVATE)
				.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
				.setCaller(this.getClass())
				.setDefaultFilters(false).build(),
				page, LIMIT)
				.setSelected(SELECTED)
				.setOrderBy(ORDER_BY);
		query.getFilters().setTaxonId(taxonId);
		return query;
	}

}
