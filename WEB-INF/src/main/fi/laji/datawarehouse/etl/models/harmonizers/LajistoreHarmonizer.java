package fi.laji.datawarehouse.etl.models.harmonizers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.MediaDAO;
import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.ContextDefinitions.ContextDefinition;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.laji.datawarehouse.etl.utils.FinlandAreaUtil;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class LajistoreHarmonizer extends BaseHarmonizer<JSONObject> {

	private static final Qname HABITAT_TYPE = new Qname("MM.typeEnumHabitat");
	public static final Qname INSECT_DB_COLLECTION_ID = new Qname("HR.200");
	public static final Qname INVASIVE_PREVENTION_COLLECTION_ID = new Qname("HR.2049");
	protected static final String ID = "id";
	private static final Set<String> HABITAT_KEYWORDS = Utils.set("habitat", "habitaatti", "habitaten");
	private static final String TIME_START = "timeStart";
	private static final String TIME_END = "timeEnd";
	private static final String NAMED_PLACE_ID = "namedPlaceID";
	private static final String FORM_ID = "formID";
	private static final String FEMALE_INDIVIDUAL_COUNT = "femaleIndividualCount";
	private static final String MALE_INDIVIDUAL_COUNT = "maleIndividualCount";
	private static final String COUNTRY = "country";
	private static final String PROVINCE = "administrativeProvince";
	private static final String BIO_PROVINCE = "biologicalProvince";
	private static final String MUNICIPALITY = "municipality";
	private static final String HIGHER_GEOGRAPHY = "higherGeography";
	private static final String LOCALITY = "locality";
	private static final String KEYWORDS = "keywords";
	private static final String ADDITIONALIDS = "additionalIDs";
	private static final String INFORMAL_TAXON_GROUPS = "informalTaxonGroups";
	private static final String INDIVIDUAL_COUNT = "individualCount";
	private static final String UNIT_FACT = "unitFact";
	private static final String GATHERING_FACT = "gatheringFact";
	private static final String WEATHER = "weather";
	private static final String COORDINATE_VERBATIM = "coordinateVerbatim";
	private static final String TAXON_CONFIDENCE = "taxonConfidence";
	private static final String SECURE_LEVEL = "secureLevel";
	private static final String GEOMETRIES = "geometries";
	private static final String UNIT_GATHERING = "unitGathering";
	private static final String TAXON_ID = "taxonID";
	private static final String TAXON = "taxon";
	private static final String TAXON_VERBATIM = "taxonVerbatim";
	private static final String IDENTIFICATIONS = "identifications";
	private static final String CREATOR = "creator";
	private static final String IMAGES = "images";
	private static final String AUDIO = "audio";
	private static final String LOCALITY_DESCRIPTION = "localityDescription";
	private static final String UNITS = "units";
	private static final String COUNT = "count";
	private static final String NOTES = "notes";
	private static final String LEG_PUBLIC = "legPublic";
	private static final String RECORD_BASIS = "recordBasis";
	private static final String COLLECTION_ID = "collectionID";
	private static final String DATE_END = "dateEnd";
	private static final String DATE_BEGIN = "dateBegin";
	private static final String LEG = "leg";
	private static final String LEG_USER_ID = "legUserID";
	private static final String GATHERINGS = "gatherings";
	private static final String GATHERING_EVENT = "gatheringEvent";
	private static final String DATE_EDITED = "dateEdited";
	private static final String DATE_CREATED = "dateCreated";
	private static final String EDITORS = "editors";
	private static final String DOCUMENT = "document";
	private static final String ROOTS = "roots";
	private static final String TAXON_CENSUS = "taxonCensus";
	private static final String CHECKLIST_ID = "checklistID";
	private static final String LIFE_STAGE = "lifeStage";
	private static final String ALIVE = "alive";
	private static final String MOVING_STATUS = "movingStatus";
	private static final String PLANT_LIFE_STAGE = "plantLifeStage";
	private static final String PLANT_STATUS_CODE = "plantStatusCode";
	private static final String SEX = "sex";
	private static final String DET = "det";
	private static final String DET_DATE = "detDate";
	private static final String BREEDING = "breeding";
	private static final String NATIVE = "nativeStatus";
	private static final String WILD = "wild";
	private static final String COORDINATE_RADIUS = "coordinateRadius";
	private static final String SECTION = "section";
	private static final String AUTOCOMPLETE_SELECTED_TAXON_ID = "autocompleteSelectedTaxonID";
	private static final String IS_TEMPLATE = "isTemplate";
	private static final String ATLAS_CODE = "atlasCode";
	private static final String COMPLETE_LIST = "completeList";
	private static final String COMPLETE_LIST_TAXONID = "completeListTaxonID";
	private static final String COMPLETE_LIST_TYPE = "completeListType";
	private static final String DATA_SOURCE = "dataSource";
	private static final String SAMPLING_METHOD = "samplingMethod";
	private static final String IDENTIFICATION_BASIS = "identificationBasis";
	private static final String INDIRECT_OBSERVATION_TYPE = new Qname("MY.indirectObservationType").toURI();
	private static final String DROPPINGS_COUNT = new Qname("MY.lolifeDroppingsCount").toURI();
	private static final String NO_DROPPINGS = new Qname("MY.lolifeDroppingsCount0").toURI();
	private static final String DROPPINGS_1 = new Qname("MY.lolifeDroppingsCount1").toURI();
	private static final String DROPPINGS_2 = new Qname("MY.lolifeDroppingsCount2").toURI();
	private static final String DROPPINGS_25 = new Qname("MY.lolifeDroppingsCount25").toURI();
	private static final String DROPPINGS_3 = new Qname("MY.lolifeDroppingsCount3").toURI();
	private static final String DROPPINGS_4 = new Qname("MY.lolifeDroppingsCount4").toURI();
	private static final String NO_INDIRECT_OBSERVATIONS = new Qname("MY.indirectObservationTypeNone").toURI();
	private static final String NEST_COUNT = new Qname("MY.nestCount").toURI();
	private static final String INVASIVE_CONTROL = new Qname("MY.invasiveControlEffectiveness").toURI();
	private static final String PAIR_COUNT_FACT = new Qname("MY.pairCount").toURI();
	private static final String DOCUMENT_IDENTIFICATIONS = "documentIdentifications";
	private static final String CONTACTS = "contacts";
	private static final Set<String> IGNORED_FACTS = ignoredFacts();
	private static final Set<String> UNIT_GATHERING_VARIABLES = Utils.set(Geo.GEOMETRY, DATE_BEGIN, DATE_END);
	private static final EnumToProperty ENUM_TO_PROPERTY = EnumToProperty.getInstance();

	private static Set<String> ignoredFacts() {
		Set<String> ignoredFacts = allStaticStrings();
		ignoredFacts.add("unitType");
		ignoredFacts.add("typeSpecimens");
		ignoredFacts.add("publicityRestrictions");
		ignoredFacts.add("sourceID");
		ignoredFacts.add("hostID");
		ignoredFacts.add("substrateSpeciesID");
		ignoredFacts.add("acknowledgedWarnings");
		ignoredFacts.add("namedPlaceNotes");
		ignoredFacts.add("locked");
		ignoredFacts.add("editor");

		ignoredFacts.remove(TIME_START);
		ignoredFacts.remove(TIME_END);
		ignoredFacts.remove(KEYWORDS);
		ignoredFacts.remove(ADDITIONALIDS);
		ignoredFacts.remove(WILD);
		ignoredFacts.remove(NATIVE);
		ignoredFacts.remove(SECTION);
		ignoredFacts.remove(SAMPLING_METHOD);
		ignoredFacts.remove(IDENTIFICATION_BASIS);
		ignoredFacts.remove(MOVING_STATUS);
		ignoredFacts.remove(DET_DATE);
		return ignoredFacts;
	}

	private static Set<String> allStaticStrings() {
		Set<String> set = new HashSet<>();
		for (Field f : LajistoreHarmonizer.class.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers()) && Modifier.isFinal(f.getModifiers()) && f.getType() == String.class) {
				try {
					set.add((String)f.get(null));
				} catch (Exception e) {
					throw new ETLException(e);
				}
			}
		}
		return set;
	}

	private final ContextDefinitions contextDefinitions;
	private final MediaDAO mediaDao;

	public LajistoreHarmonizer(ContextDefinitions contextDefinitions, MediaDAO mediaDao) {
		if (contextDefinitions == null) {
			this.contextDefinitions = new ContextDefinitions(new HashMap<String, ContextDefinition>());
		} else {
			this.contextDefinitions = contextDefinitions;
		}
		this.mediaDao = mediaDao;
	}

	@Override
	public List<DwRoot> harmonize(JSONObject json, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> roots = new ArrayList<>();
		if (json.hasKey(ROOTS)) {
			for (JSONObject root : json.getArray(ROOTS).iterateAsObject()) {
				roots.add(parseRoot(root.getObject(DOCUMENT), source));
			}
		} else {
			roots.add(parseRoot(json.getObject(DOCUMENT), source));
		}
		return roots;

	}

	protected DwRoot parseRoot(JSONObject document, Qname source) throws CriticalParseFailure, UnknownHarmonizingFailure {
		Qname documentId = parseDocumentId(document);
		if (isTemplate(document)) {
			return DwRoot.createDeleteRequest(documentId, source);
		}
		DwRoot root = new DwRoot(documentId, source);
		try {
			parseRoot(document, root);
		} catch (Exception e) {
			handleException(e);
		}
		return root;
	}

	private Qname parseDocumentId(JSONObject json) throws CriticalParseFailure {
		String id = json.getString(ID);
		if (!given(id)) throw new CriticalParseFailure("No document id");
		return new Qname(id);
	}

	private void parseRoot(JSONObject documentJson, DwRoot root) throws CriticalParseFailure  {
		root.setCollectionId(parseCollectionId(documentJson));
		Document publicDocument = root.createPublicDocument();
		parseDocument(documentJson, publicDocument);
		SecureLevel secureLevel = getSecureLevel(documentJson);

		if (secureLevel != SecureLevel.NONE && invasiveControl(publicDocument)) {
			publicDocument.setSecureLevel(secureLevel);
			publicDocument.addSecureReason(SecureReason.USER_HIDDEN);
			return;
		}

		if (shouldHideTeam(documentJson) || secureLevel != SecureLevel.NONE || documentJson.hasKey(CONTACTS)) {
			Document privateDocument = createPrivateDocument(documentJson, publicDocument, secureLevel);
			root.setPrivateDocument(privateDocument);
		}
	}

	private Document createPrivateDocument(JSONObject documentJson, Document publicDocument, SecureLevel secureLevel) {
		Document privateDocument = publicDocument.copy(Concealment.PRIVATE);

		if (shouldHideTeam(documentJson)) {
			concealTeam(publicDocument);
			publicDocument.addSecureReason(SecureReason.USER_PERSON_NAMES_HIDDEN);
			privateDocument.addSecureReason(SecureReason.USER_PERSON_NAMES_HIDDEN);
		}

		if (secureLevel != SecureLevel.NONE) {
			concealPlace(publicDocument, secureLevel);
			publicDocument.addSecureReason(SecureReason.USER_HIDDEN_LOCATION);
			privateDocument.addSecureReason(SecureReason.USER_HIDDEN_LOCATION);
			if (insectDb(publicDocument)) {
				concealTime(publicDocument);
				publicDocument.addSecureReason(SecureReason.USER_HIDDEN_TIME);
				privateDocument.addSecureReason(SecureReason.USER_HIDDEN_TIME);
			}
		}

		for (JSONObject contactJson : documentJson.getArray(CONTACTS).iterateAsObject()) {
			parseFacts(contactJson, privateDocument.getFacts());
		}
		return privateDocument;
	}

	private boolean isTemplate(JSONObject documentJson) {
		return documentJson.hasKey(IS_TEMPLATE) && documentJson.getBoolean(IS_TEMPLATE);
	}

	private void concealTime(Document publicDocument) {
		for (Gathering g : publicDocument.getGatherings()) {
			if (g.getEventDate() != null) {
				g.setEventDate(g.getEventDate().conceal(SecureLevel.KM1));
			}
		}
	}

	private boolean insectDb(Document publicDocument) {
		return INSECT_DB_COLLECTION_ID.equals(publicDocument.getCollectionId());
	}

	private boolean invasiveControl(Document publicDocument) {
		return INVASIVE_PREVENTION_COLLECTION_ID.equals(publicDocument.getCollectionId());
	}

	private SecureLevel getSecureLevel(JSONObject json) {
		if (json.hasKey(SECURE_LEVEL)) {
			return parseSecurelevel(json.getString(SECURE_LEVEL));
		}
		return SecureLevel.NONE;
	}

	private void concealTeam(Document publicDocument) {
		for (Gathering g : publicDocument.getGatherings()) {
			g.getTeam().clear();
			g.getObserverUserIds().clear();
		}
		publicDocument.getEditorUserIds().clear();
	}

	private void concealPlace(Document publicDocument, SecureLevel secureLevel) {
		try {
			for (Gathering g : publicDocument.getGatherings()) {
				Geo geo = getGeo(g);
				g.setCoordinates(null);
				g.setGeo(null);
				g.setMunicipality(null);
				g.setCoordinatesVerbatim(null);
				g.setLocality(null);
				if (geo != null) {
					g.setCoordinates(concealCoordinates(geo, secureLevel));
				}
				g.setNotes(null);
			}
		} catch (DataValidationException e) {
			throw new ETLException("Concealing of geo caused error - should not happen", e);
		}
	}

	private Coordinates concealCoordinates(Geo geo, SecureLevel secureLevel) throws DataValidationException {
		if (geo.getCRS() == Type.WGS84) {
			return concealWGS84(geo, secureLevel);
		}
		if (geo.getCRS() == Type.YKJ) {
			return geo.getBoundingBox().conceal(secureLevel, Type.YKJ);
		}
		if (geo.getCRS() == Type.EUREF) {
			return concealEuref(geo, secureLevel);
		}
		throw new UnsupportedOperationException("Not implemented for " + geo.getCRS());
	}

	private Coordinates concealEuref(Geo geo, SecureLevel secureLevel) throws DataValidationException {
		return concealAsMetric(geo, secureLevel);
	}

	private Coordinates concealWGS84(Geo wgs84geo, SecureLevel secureLevel) throws DataValidationException {
		if (FinlandAreaUtil.isInsideGeneralFinlandArea(wgs84geo.getBoundingBox())) {
			return concealAsMetric(wgs84geo, secureLevel);
		}
		return wgs84geo.getBoundingBox().conceal(secureLevel, Type.WGS84);
	}

	private Coordinates concealAsMetric(Geo original, SecureLevel secureLevel) throws DataValidationException {
		Geo ykjGeo;
		try {
			ykjGeo = CoordinateConverter.convertGeo(original, Type.YKJ);
		} catch (Exception e) {
			// Outside YKJ range: possibly very large area that touches Finland but goes too far
			// Conceal as wgs84
			if (original.getCRS() != Type.WGS84) {
				original = CoordinateConverter.convertGeo(original, Type.WGS84);
			}
			return original.getBoundingBox().conceal(secureLevel, original.getCRS());
		}
		return ykjGeo.getBoundingBox().conceal(secureLevel, original.getCRS());
	}

	private Geo getGeo(Gathering g) {
		if (g.getGeo() != null) return g.getGeo();
		if (g.getCoordinates() != null) {
			return Geo.getBoundingBox(g.getCoordinates());
		}
		return null;
	}

	private boolean shouldHideTeam(JSONObject document) {
		JSONObject gatheringEvent = document.getObject(GATHERING_EVENT);
		if (gatheringEvent.hasKey(LEG_PUBLIC) && gatheringEvent.getBoolean(LEG_PUBLIC) == false) {
			return true;
		}
		return false;
	}

	private void parseDocument(JSONObject documentJson, Document document) throws CriticalParseFailure  {
		Set<String> editors = new LinkedHashSet<>();
		for (String editor : documentJson.getArray(EDITORS)) {
			editors.add(editor);
		}
		if (documentJson.hasKey(CREATOR)) {
			editors.add(documentJson.getString(CREATOR));
		}
		for (String editor : editors) {
			document.addEditorUserId(editor);
		}

		if (documentJson.hasKey(KEYWORDS)) {
			for (String keyword : documentJson.getArray(KEYWORDS)) {
				document.addKeyword(keyword);
			}
		}
		if (documentJson.hasKey(ADDITIONALIDS)) {
			if (documentJson.isArray(ADDITIONALIDS)) {
				for (String id : documentJson.getArray(ADDITIONALIDS)) {
					document.addKeyword(id);
				}
			}
		}

		String createdDate = documentJson.getString(DATE_CREATED);
		String editedDate = documentJson.getString(DATE_EDITED);
		if (given(createdDate)) {
			try {
				document.setCreatedDate(toDate(createdDate));
			} catch (DateValidationException e) {
				document.createQuality().setIssue(
						new Quality(
								Quality.Issue.INVALID_CREATED_DATE,
								Quality.Source.AUTOMATED_FINBIF_VALIDATION,
								e.getMessage()));
			}
		}
		if (given(editedDate)) {
			try {
				document.setModifiedDate(toDate(editedDate));
			} catch (DateValidationException e) {
				document.createQuality().setIssue(
						new Quality(
								Quality.Issue.INVALID_MODIFIED_DATE,
								Quality.Source.AUTOMATED_FINBIF_VALIDATION,
								e.getMessage()));
			}
		}

		if (documentJson.hasKey(NAMED_PLACE_ID)) {
			String namedPlaceId = documentJson.getString(NAMED_PLACE_ID);
			if (given(namedPlaceId)) document.setNamedPlaceIdUsingQname(new Qname(namedPlaceId));
		}

		if (documentJson.hasKey(FORM_ID)) {
			String formId = documentJson.getString(FORM_ID);
			if (given(formId)) document.setFormIdUsingQname(new Qname(formId));
		}

		parseFacts(documentJson, document.getFacts());

		JSONObject gatheringEventJson = documentJson.getObject(GATHERING_EVENT);
		parseFacts(gatheringEventJson, document.getFacts());

		Gathering gatheringEvent = parseGatheringEvent(gatheringEventJson);

		parseCompleteList(document, gatheringEventJson);

		parseDocumentNotes(documentJson, document);

		if (documentJson.hasKey(DATA_SOURCE)) {
			document.setDataSource(documentJson.getString(DATA_SOURCE));
		}

		DocumentIdentification documentIdentification = null;
		if (documentJson.hasKey(DOCUMENT_IDENTIFICATIONS)) {
			JSONArray ids = documentJson.getArray(DOCUMENT_IDENTIFICATIONS);
			if (!ids.isEmpty()) {
				documentIdentification = parseDocumentIdentification(ids.iterateAsObject().get(0));
			}
		}
		for (JSONObject gatheringJson : getGatherings(documentJson)) {
			for (Gathering g : parseGatherings(gatheringJson, gatheringEvent, documentIdentification)) {
				document.addGathering(g);
			}
		}
	}

	private DocumentIdentification parseDocumentIdentification(JSONObject json) {
		return new DocumentIdentification(json.getString(DET), json.getString(DET_DATE));
	}

	private void parseCompleteList(Document document, JSONObject gatheringEventJson) {
		if (gatheringEventJson.hasKey(COMPLETE_LIST)) {
			JSONObject completeList = gatheringEventJson.getObject(COMPLETE_LIST);
			String taxonId = completeList.getString(COMPLETE_LIST_TAXONID);
			String type = completeList.getString(COMPLETE_LIST_TYPE);
			if (given(taxonId)) document.setCompleteListTaxonIdUsingQname(new Qname(taxonId));
			if (given(type)) document.setCompleteListTypeUsingQname(new Qname(type));
		}
	}

	protected List<JSONObject> getGatherings(JSONObject documentJson) {
		return documentJson.getArray(GATHERINGS).iterateAsObject();
	}

	private void parseDocumentNotes(JSONObject documentJson, Document document) {
		document.setNotes(documentJson.getString(NOTES));
		String gatheringEventNotes = documentJson.getObject(GATHERING_EVENT).getString(NOTES);
		if (given(gatheringEventNotes)) {
			if (given(document.getNotes())) {
				document.setNotes(document.getNotes() + "; " + gatheringEventNotes);
			} else {
				document.setNotes(gatheringEventNotes);
			}
		}
	}

	private SecureLevel parseSecurelevel(String secureLevelQname) {
		if (!given(secureLevelQname)) return SecureLevel.NONE;
		return (SecureLevel) ENUM_TO_PROPERTY.get(new Qname(secureLevelQname));
	}

	private Gathering parseGatheringEvent(JSONObject json) {
		Gathering gatheringEvent = Gathering.emptyGathering();
		parseDate(json, gatheringEvent);
		parseTimes(json, gatheringEvent);
		parseLeg(json, gatheringEvent);
		parseLegUserId(json, gatheringEvent);
		parseTaxonCensus(json, gatheringEvent);
		return gatheringEvent;
	}

	private void parseLeg(JSONObject json, Gathering gathering) {
		for (String leg : json.getArray(LEG)) {
			gathering.addTeamMember(leg);
		}
	}

	private void parseLegUserId(JSONObject json, Gathering gathering) {
		if (!json.hasKey(LEG_USER_ID)) return;
		if (json.isArray(LEG_USER_ID)) {
			for (String userId : json.getArray(LEG_USER_ID)) {
				gathering.addObserverUserId(userId);
			}
			return;
		}
		String single = json.getString(LEG_USER_ID);
		if (given(single)) gathering.addObserverUserId(single);
	}

	private void parseDate(JSONObject json, Gathering gathering) {
		if (hasDate(json)) {
			String date = getDateString(json);
			parseEventDateTime(date, gathering);
		}
	}

	private String getDateString(JSONObject json) {
		String date = json.getString(DATE_BEGIN);
		String dateEnd = json.getString(DATE_END);

		if (given(dateEnd)) {
			date += "/" + dateEnd;
		}
		return date;
	}

	private List<Gathering> parseGatherings(JSONObject gatheringJson, Gathering gatheringEvent, DocumentIdentification documentIdentification) throws CriticalParseFailure {
		Gathering gathering = new Gathering(new Qname(gatheringJson.getString(ID)));
		if (hasDate(gatheringJson)) {
			parseDate(gatheringJson, gathering);
		} else {
			if (gatheringEvent.getEventDate() != null) {
				gathering.setEventDate(gatheringEvent.getEventDate().copy());
			}
			try {
				gathering.setHourBegin(gatheringEvent.getHourBegin());
				gathering.setHourEnd(gatheringEvent.getHourEnd());
				gathering.setMinutesBegin(gatheringEvent.getMinutesBegin());
				gathering.setMinutesEnd(gatheringEvent.getMinutesEnd());
			} catch (DateValidationException e) {
				throw new IllegalStateException("Impossible state: Already validated", e);
			}
			if (gatheringEvent.getQuality() != null && gatheringEvent.getQuality().getTimeIssue() != null) {
				gathering.createQuality().setTimeIssue(gatheringEvent.getQuality().getTimeIssue().copy());
			}
		}

		parseTimes(gatheringJson, gathering);

		if (gatheringJson.hasKey(LEG)) {
			parseLeg(gatheringJson, gathering);
		} else {
			gathering.setTeam(new ArrayList<>(gatheringEvent.getTeam()));
		}
		if (gatheringJson.hasKey(LEG_USER_ID)) {
			parseLegUserId(gatheringJson, gathering);
		} else {
			gathering.setObserverUserIds(new ArrayList<>(gatheringEvent.getObserverUserIds()));
		}

		gathering.setHigherGeography(gatheringJson.getString(HIGHER_GEOGRAPHY));
		gathering.setCountry(gatheringJson.getString(COUNTRY));
		gathering.setProvince(gatheringJson.getString(PROVINCE));
		gathering.setBiogeographicalProvince(gatheringJson.getString(BIO_PROVINCE));
		gathering.setMunicipality(gatheringJson.getString(MUNICIPALITY));
		gathering.setLocality(gatheringJson.getString(LOCALITY));

		appendToNotes(gathering, gatheringJson.getString(NOTES));
		appendToNotes(gathering, gatheringJson.getString(LOCALITY_DESCRIPTION));
		appendToNotes(gathering, gatheringJson.getString(WEATHER));

		parseGeometry(gatheringJson, gathering);

		parseTaxonCensus(gatheringJson, gathering);
		for (TaxonCensus c : gatheringEvent.getTaxonCensus()) {
			if (!gathering.getTaxonCensus().stream().anyMatch(gtc->gtc.equals(c))) {
				gathering.addTaxonCensus(c.copy());
			}
		}

		if (gatheringJson.hasKey(SECTION)) {
			gathering.setGatheringSection(gatheringJson.getInteger(SECTION));
		}

		parseMedia(gatheringJson, gathering);

		parseFacts(gatheringJson, gathering.getFacts());

		for (Fact fact : gatheringEvent.getFacts()) {
			gathering.addFact(fact.getFact(), fact.getValue());
		}

		List<Gathering> gatherings = new ArrayList<>();
		gatherings.add(gathering);

		Qname gatheringSamplingMethod = null;
		if (gatheringJson.hasKey(SAMPLING_METHOD)) {
			gatheringSamplingMethod = new Qname(gatheringJson.getString(SAMPLING_METHOD));
		}

		for (JSONObject unitJson : getUnits(gatheringJson)) {
			Unit unit = parseUnit(gathering, gatherings, documentIdentification, unitJson);
			addSamplingMethod(gatheringSamplingMethod, unit);
		}

		return gatherings;
	}

	private void addSamplingMethod(Qname gatheringSamplingMethod, Unit unit) {
		if (given(unit.getSamplingMethod())) return;
		if (given(gatheringSamplingMethod)) {
			unit.setSamplingMethodUsingQname(gatheringSamplingMethod);
		}
	}

	protected List<JSONObject> getUnits(JSONObject gatheringJson) {
		return gatheringJson.getArray(UNITS).iterateAsObject();
	}

	private void parseTimes(JSONObject json, Gathering gathering) {
		if (!json.hasKey(TIME_START) && !json.hasKey(TIME_END)) return;
		clearTimes(gathering);
		if (json.hasKey(TIME_START)) {
			String timeStart = json.getString(TIME_START);
			try {
				gathering.setHourBegin(parseHour(timeStart));
				gathering.setMinutesBegin(parseMinutes(timeStart));
			} catch (DateValidationException e) {
				createTimeIssue(gathering, e);
			}
		}
		if (json.hasKey(TIME_END)) {
			String timeEnd = json.getString(TIME_END);
			try {
				gathering.setHourEnd(parseHour(timeEnd));
				gathering.setMinutesEnd(parseMinutes(timeEnd));
			} catch (DateValidationException e) {
				createTimeIssue(gathering, e);
			}
		}
	}

	private void clearTimes(Gathering gathering) {
		try {
			gathering.setHourBegin(null);
			gathering.setHourEnd(null);
			gathering.setMinutesBegin(null);
			gathering.setMinutesEnd(null);
		} catch (DateValidationException e1) {
			throw new IllegalStateException("Impossible state");
		}

	}

	private void parseTaxonCensus(JSONObject json, Gathering gathering) {
		if (json.hasKey(TAXON_CENSUS)) {
			for (JSONObject censusInfo : json.getArray(TAXON_CENSUS).iterateAsObject()) {
				Qname taxonId = fromUriOrQname(censusInfo.getString("censusTaxonID"));
				Qname taxonSetId = fromUriOrQname(censusInfo.getString("censusTaxonSetID"));
				Qname type = new Qname(censusInfo.getString("taxonCensusType"));
				if ((given(taxonId) || given(taxonSetId)) && given(type)) {
					if (given(taxonId)) {
						gathering.addTaxonCensus(new TaxonCensus(taxonId, type));
					} else {
						// TODO add taxon ids part of the taxon set
						gathering.addFact(new Qname("MY.taxonCensus").toURI(), taxonSetId.toURI());
					}
				}
			}
		}
	}

	private Integer parseMinutes(String time) throws DateValidationException {
		if (!given(time)) return null;
		if (!time.contains(":")) return null;
		try {
			return Integer.valueOf(time.split(Pattern.quote(":"))[1]);
		} catch (Exception e) {
			throw new DateValidationException(Issue.INVALID_MINUTE, e.getMessage());
		}
	}

	private Integer parseHour(String time) throws DateValidationException {
		if (!given(time)) return null;
		try {
			if (!time.contains(":")) return Integer.valueOf(time);
			return Integer.valueOf(time.split(Pattern.quote(":"))[0]);
		} catch (Exception e) {
			throw new DateValidationException(Issue.INVALID_HOUR, e.getMessage());
		}
	}

	private void parseMedia(JSONObject json, Gathering gathering) {
		parseMedia(json, gathering.getMedia());
	}

	private boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	private void appendToNotes(Gathering gathering, String note) {
		if (!given(note)) return;
		if (given(gathering.getNotes())) note = gathering.getNotes() + "\n" + note;
		gathering.setNotes(note.trim());
	}

	private boolean hasDate(JSONObject json) {
		return json.hasKey(DATE_BEGIN) || json.hasKey(DATE_END);
	}

	private boolean hasNonEmptyUnitGathering(JSONObject unitJson) {
		if (!unitJson.hasKey(UNIT_GATHERING)) return false;
		JSONObject unitGatheringJson = unitJson.getObject(UNIT_GATHERING);
		for (String key : unitGatheringJson.getKeys()) {
			if (UNIT_GATHERING_VARIABLES.contains(key)) {
				if (isSet(unitGatheringJson, key)) return true;
			}
		}
		return false;
	}

	private boolean isSet(JSONObject json, String key) {
		if (!json.hasKey(key)) return false;
		if (json.isNull(key)) return false;
		if (json.isObject(key)) {
			return !json.getObject(key).isEmpty();
		}
		if (json.isArray(key)) {
			return !json.getArray(key).isEmpty();
		}
		return given(json.getString(key));
	}

	private void parseGeometry(JSONObject json, Gathering gathering) {
		if (!json.hasKey(Geo.GEOMETRY)) return;

		JSONObject geometry = json.getObject(Geo.GEOMETRY);
		if (geometry.isEmpty()) return;

		// when parsing a new gathering from unitgathering, remove the old copied coordinates/geo
		gathering.setCoordinates(null);
		gathering.setGeo(null);
		gathering.setCoordinatesVerbatim(null);

		Integer reportedCoordinateAccuracy = parseCoordinateAccuracy(json);
		try {
			parseGeometry(geometry, reportedCoordinateAccuracy, gathering);
		} catch (DataValidationException e) {
			createGeoIssue(gathering, e);
		}
	}

	private Integer parseCoordinateAccuracy(JSONObject json) {
		String accuracy = json.getString(COORDINATE_RADIUS);
		if (given(accuracy)) {
			try {
				return Integer.valueOf(accuracy);
			} catch (NumberFormatException e) {}
		}
		return null;
	}

	private void parseGeometry(JSONObject geometry, Integer reportedCoordinateAccuracy, Gathering gathering) throws DataValidationException {
		String singleCoordinateVerbatim = getSingleCoordinateVerbatim(geometry);

		if (given(singleCoordinateVerbatim)) {
			Coordinates c = getGridCoordinatesFromVerbatim(singleCoordinateVerbatim);
			if (c != null) {
				gathering.setCoordinates(c.setAccuracyInMeters(reportedCoordinateAccuracy));
				gathering.setCoordinatesVerbatim(singleCoordinateVerbatim);
				return;
			}
		}

		String allCoordinateVerbatim = getAllCoordinateVerbatimJoined(geometry);
		gathering.setCoordinatesVerbatim(allCoordinateVerbatim);
		Coordinates validGridCoordinates = getGridCoordinatesFromVerbatim(geometry);

		Geo geo = null;
		if (validGridCoordinates == null) {
			geo = parseGeoAs(geometry, Type.WGS84);
		} else {
			geo = parseGeoAs(geometry, validGridCoordinates.getType());
		}

		if (geo.getFeatures().isEmpty()) return;

		geo.convertComplex();
		geo.setAccuracyInMeters(reportedCoordinateAccuracy);
		gathering.setGeo(geo);
	}

	private Geo parseGeoAs(JSONObject geometry, Type targetCrs) throws DataValidationException {
		Geo geo = new Geo(targetCrs);
		parseGeoAs(geometry, targetCrs, geo);
		return geo;
	}

	private void parseGeoAs(JSONObject geometry, Type targetCrs, Geo geo) throws DataValidationException {
		if (!geometry.hasKey(GEOMETRIES)) {
			addFeature(geo, geometry, targetCrs);
		} else {
			for (JSONObject subGeo : geometry.getArray(GEOMETRIES).iterateAsObject()) {
				parseGeoAs(subGeo, targetCrs, geo);
			}
		}
	}

	private void addFeature(Geo geo, JSONObject geometry, Type targetCrs) throws DataValidationException {
		Coordinates validGridCoordinates = getGridCoordinatesFromVerbatim(geometry);
		if (validGridCoordinates != null) {
			Feature f = Geo.getBoundingBox(validGridCoordinates).getFeatures().get(0);
			geo.addFeature(convert(f, validGridCoordinates.getType(), targetCrs));
		} else {
			Feature f = Geo.parseFeature(geometry, Type.WGS84);
			geo.addFeature(convert(f, Type.WGS84, targetCrs));
		}
	}

	private Feature convert(Feature feature, Type originalCrs, Type targetCrs) throws DataValidationException {
		if (originalCrs == targetCrs) return feature;
		return CoordinateConverter.convertFeature(feature, targetCrs, originalCrs);
	}

	private String getSingleCoordinateVerbatim(JSONObject geometry) {
		if (geometry.hasKey(COORDINATE_VERBATIM)) {
			return geometry.getString(COORDINATE_VERBATIM);
		}
		if (geometry.hasKey(GEOMETRIES)) {
			List<JSONObject> geometries = geometry.getArray(GEOMETRIES).iterateAsObject();
			if (geometries.size() == 1) {
				JSONObject feature = geometries.get(0);
				if (feature.hasKey(COORDINATE_VERBATIM)) {
					return feature.getString(COORDINATE_VERBATIM);
				}
			}
		}
		return null;
	}

	private String getAllCoordinateVerbatimJoined(JSONObject geometry) {
		List<String> list = null;
		if (geometry.hasKey(COORDINATE_VERBATIM)) {
			String verbatim = geometry.getString(COORDINATE_VERBATIM);
			if (given(verbatim)) {
				list = add(list, verbatim);
			}
		}
		if (geometry.hasKey(GEOMETRIES)) {
			for (JSONObject subGeo : geometry.getArray(GEOMETRIES).iterateAsObject()) {
				String verbatim = subGeo.getString(COORDINATE_VERBATIM);
				if (given(verbatim)) {
					list = add(list, verbatim);
				}
			}
		}
		if (list == null) return null;
		return list.stream().collect(Collectors.joining("; "));
	}

	private List<String> add(List<String> list, String verbatim) {
		if (list == null) list = new ArrayList<>();
		list.add(verbatim);
		return list;
	}

	private Coordinates getGridCoordinatesFromVerbatim(JSONObject geometry) {
		if (geometry.hasKey(COORDINATE_VERBATIM)) {
			String verbatim = geometry.getString(COORDINATE_VERBATIM);
			Coordinates c = getGridCoordinatesFromVerbatim(verbatim);
			if (c != null) return c;
		}
		if (geometry.hasKey(GEOMETRIES)) {
			for (JSONObject subGeo : geometry.getArray(GEOMETRIES).iterateAsObject()) {
				String verbatim = subGeo.getString(COORDINATE_VERBATIM);
				Coordinates c = getGridCoordinatesFromVerbatim(verbatim);
				if (c != null) return c;
			}
		}
		return null;
	}

	private Coordinates getGridCoordinatesFromVerbatim(String verbatim) {
		if (looksLikeGridCoordinateString(verbatim)) {
			try {
				return parseYkjOrEurefGrid(verbatim);
			} catch (DataValidationException e) {
				// not valid grid coordinates
			}
		}
		return null;
	}

	static Coordinates parseYkjOrEurefGrid(String coordinateVerbatim) throws DataValidationException {
		try {
			if (coordinateVerbatim.contains("-") || coordinateVerbatim.contains("/")) {
				return parseYKJCoordinates(coordinateVerbatim);
			}
			String[] parts = coordinateVerbatim.split(Pattern.quote(":"));
			if (parts.length != 2)  throw new IllegalArgumentException();
			String lat = parts[0];
			String lon = parts[1];
			if (looksLikeEuref(lat, lon)) {
				return parseEurefCoordinates(coordinateVerbatim);
			}
			return parseYKJCoordinates(coordinateVerbatim);
		} catch (DataValidationException e) {
			throw e;
		} catch (Exception e) {
			throw new DataValidationException("Invalid grid coordinates: " + coordinateVerbatim, e);
		}
	}

	private static boolean looksLikeEuref(String lat, String lon) {
		if (lon.startsWith("8")) {
			return true;
		}
		return lat.length() != lon.length();
	}

	private boolean looksLikeGridCoordinateString(String coordinateVerbatim) {
		if (!given(coordinateVerbatim)) return false;
		return coordinateVerbatim.length() <= 15 &&
				!coordinateVerbatim.contains(".") &&
				!coordinateVerbatim.contains("/") &&
				!coordinateVerbatim.contains(",") &&
				Utils.countNumberOf(":", coordinateVerbatim) == 1;
	}

	private Gathering parseUnitGathering(Gathering gathering, JSONObject unitGatheringJson, Unit unit) throws CriticalParseFailure  {
		Gathering unitGathering = gathering.copy();
		unitGathering.getUnits().clear();

		if (unitGatheringJson.hasKey(ID)) {
			unitGathering.setGatheringId(new Qname(unitGatheringJson.getString(ID)));
		} else {
			unitGathering.setGatheringId(new Qname(unit.getUnitId().toString() + "_G"));
		}

		parseGeometry(unitGatheringJson, unitGathering);
		parseDate(unitGatheringJson, unitGathering);
		return unitGathering;
	}

	private void parseMedia(JSONObject json, List<MediaObject> media) {
		for (String imageId : json.getArray(IMAGES)) {
			MediaObject image = getMediaObject(imageId);
			if (image != null) media.add(image);
		}
		for (String audioId : json.getArray(AUDIO)) {
			MediaObject audio = getMediaObject(audioId);
			if (audio != null) media.add(audio);
		}
	}

	private MediaObject getMediaObject(String id) {
		if (mediaDao == null) throw new ETLException("Media DAO not initialized (secondary data upload with media?)");
		return mediaDao.getMediaObject(new Qname(id));
	}

	private Unit parseUnit(Gathering baseGathering, List<Gathering> gatherings, DocumentIdentification documentIdentification, JSONObject unitJson) throws CriticalParseFailure {
		Unit unit = new Unit(new Qname(unitJson.getString(ID)));

		parseIdentification(unitJson, unit, documentIdentification);

		if (unitJson.hasKey(INFORMAL_TAXON_GROUPS)) {
			JSONArray informalGroups = unitJson.getArray(INFORMAL_TAXON_GROUPS);
			if (!informalGroups.isEmpty()) {
				Qname informalGroup = new Qname(informalGroups.iterator().next());
				if (informalGroup.isSet()) unit.setReportedInformalTaxonGroup(informalGroup);
			}
		}

		if (unitJson.hasKey(UNIT_FACT)) {
			JSONObject facts = unitJson.getObject(UNIT_FACT);
			if (facts.hasKey(AUTOCOMPLETE_SELECTED_TAXON_ID)) {
				String autocompleteSelectedTaxonId = facts.getString(AUTOCOMPLETE_SELECTED_TAXON_ID);
				if (given(autocompleteSelectedTaxonId)) unit.setAutocompleteSelectedTaxonId(new Qname(autocompleteSelectedTaxonId));
			}
		}

		unit.setRecordBasis(parseRecordBasis(unitJson));
		if (unitJson.hasKey(TAXON_CONFIDENCE)) unit.setReportedTaxonConfidence(parseTaxonConfidence(unitJson)); // TODO should come from identifications in the future
		unit.setAbundanceString(unitJson.getString(COUNT));
		if (unitJson.hasKey(INDIVIDUAL_COUNT)) unit.setAbundanceString(Integer.toString(unitJson.getInteger(INDIVIDUAL_COUNT)));
		if (unitJson.hasKey(MALE_INDIVIDUAL_COUNT)) unit.setIndividualCountMale(unitJson.getInteger(MALE_INDIVIDUAL_COUNT));
		if (unitJson.hasKey(FEMALE_INDIVIDUAL_COUNT)) unit.setIndividualCountFemale(unitJson.getInteger(FEMALE_INDIVIDUAL_COUNT));

		parseLifeStageAlive(unit, unitJson.getString(LIFE_STAGE), unitJson.getString(PLANT_LIFE_STAGE), unitJson.getString(ALIVE));

		unit.setSex(getSex(unitJson.getString(SEX)));

		if (unitJson.hasKey(SAMPLING_METHOD)) {
			unit.setSamplingMethodUsingQname(new Qname(unitJson.getString(SAMPLING_METHOD)));
		}

		unit.setNotes(unitJson.getString(NOTES));

		if (unitJson.hasKey(BREEDING)) {
			if (unitJson.getBoolean(BREEDING)) {
				unit.setBreedingSite(true);
			}
		}

		if (unitJson.hasKey(ATLAS_CODE)) {
			unit.setAtlasCodeUsingQname(new Qname(unitJson.getString(ATLAS_CODE)));
		}

		unit.setWild(parseWild(unitJson));
		unit.setLocal(parseLocal(unitJson));

		if (unitJson.hasKey(PLANT_STATUS_CODE)) {
			unit.setPlantStatusCodeUsingQname(new Qname(unitJson.getString(PLANT_STATUS_CODE)));
		}

		parseFacts(unitJson, unit.getFacts());

		parseLineTransectInfo(unitJson, unit);

		if (unit.getAbundanceString() == null) {
			unit.setAbundanceString(abundanceFromFacts(unit));
		}
		if (unit.getAbundanceUnit() == null) {
			unit.setAbundanceUnit(abundanceUnitFromFacts(unit));
		}

		List<MediaObject> gatheringMedia = parseUnitMedia(unitJson, unit);

		Gathering targetGathering = resolveTargetGathering(baseGathering, gatherings, unitJson, gatheringMedia, unit);

		for (MediaObject m : gatheringMedia) {
			targetGathering.addMedia(m);
		}

		parseInvasiveControl(targetGathering, unit);

		targetGathering.addUnit(unit);
		return unit;
	}

	private String abundanceFromFacts(Unit unit) {
		String droppingsCount = unit.factValue(DROPPINGS_COUNT);
		if (droppingsCount != null) {
			if (NO_DROPPINGS.equals(droppingsCount)) return "0";
			if (DROPPINGS_1.equals(droppingsCount)) return "1-10";
			if (DROPPINGS_2.equals(droppingsCount)) return "10-50";
			if (DROPPINGS_25.equals(droppingsCount)) return "50-100";
			if (DROPPINGS_3.equals(droppingsCount)) return "100-500";
			if (DROPPINGS_4.equals(droppingsCount)) return ">500";
		}
		if (NO_INDIRECT_OBSERVATIONS.equals(unit.factValue(INDIRECT_OBSERVATION_TYPE))) return "0";
		Integer nestCount = unit.factIntValue(NEST_COUNT);
		if (nestCount != null) return nestCount.toString();
		return null;
	}

	private static final Map<String, AbundanceUnit> INDIRECT_OBS_TYPE_TO_ABUNDANCE = initIndObsToAbundance();
	private static Map<String, AbundanceUnit> initIndObsToAbundance() {
		Map<String, AbundanceUnit> m = new HashMap<>();
		m.put(new Qname("MY.indirectObservationTypeFeces").toURI(), AbundanceUnit.DROPPINGS);
		return m;
	}

	private AbundanceUnit abundanceUnitFromFacts(Unit unit) {
		String indirectType = unit.factValue(INDIRECT_OBSERVATION_TYPE);
		if (indirectType != null) {
			AbundanceUnit au = INDIRECT_OBS_TYPE_TO_ABUNDANCE.get(indirectType);
			if (au != null) return au;
			return AbundanceUnit.INDIRECT_MARKS;
		}

		String nestCount = unit.factValue(NEST_COUNT);
		if (nestCount != null) return AbundanceUnit.NESTS;

		return null;
	}

	private void parseIdentification(JSONObject unitJson, Unit unit, DocumentIdentification documentIdentification) {
		if (!unitJson.hasKey(IDENTIFICATIONS)) return;

		JSONArray identificationArray = unitJson.getArray(IDENTIFICATIONS);
		if (identificationArray.isEmpty()) return;

		JSONObject identification = unitJson.getArray(IDENTIFICATIONS).iterateAsObject().get(0); // TODO multiple identifications support
		String taxon = identification.getString(TAXON);
		String taxonVerbatim = identification.getString(TAXON_VERBATIM);
		String taxonId = identification.getString(TAXON_ID);
		if (given(taxon)) {
			unit.setTaxonVerbatim(taxon);
		} else {
			unit.setTaxonVerbatim(taxonVerbatim);
		}
		if (given(taxonId)) unit.setReportedTaxonId(new Qname(taxonId));
		if (unitJson.hasKey(CHECKLIST_ID)) {
			String checklist = unitJson.getString(CHECKLIST_ID);
			if (given(checklist)) unit.setReferencePublication(new Qname(checklist));
		}
		if (identification.hasKey(IDENTIFICATION_BASIS)) {
			for (String ib : identification.getArray(IDENTIFICATION_BASIS)) {
				unit.addIdentificationBasis(new Qname(ib));
			}
		}

		if (identification.hasKey(TAXON_CONFIDENCE)) unit.setReportedTaxonConfidence(parseTaxonConfidence(identification));

		unit.setDet(identification.getString(DET));
		parseFacts(identification, unit.getFacts());
		if (documentIdentification != null) {
			if (!given(unit.getDet())) unit.setDet(documentIdentification.det);
			if (given(documentIdentification.detDate) && !identification.hasKey(DET_DATE)) {
				unit.getFacts().add(toFact(DET_DATE, documentIdentification.detDate));
			}
		}
	}

	private Gathering resolveTargetGathering(Gathering baseGathering, List<Gathering> gatherings, JSONObject unitJson, List<MediaObject> gatheringMedia, Unit unit) throws CriticalParseFailure {
		if (hasNonEmptyUnitGathering(unitJson) || !gatheringMedia.isEmpty()) {
			return createUnitGathering(baseGathering, gatherings, unitJson, unit);
		}
		return baseGathering;
	}

	private Gathering createUnitGathering(Gathering gathering, List<Gathering> gatherings, JSONObject unitJson, Unit unit) throws CriticalParseFailure {
		JSONObject unitGatheringJson = unitJson.getObject(UNIT_GATHERING);
		Gathering unitGathering = parseUnitGathering(gathering, unitGatheringJson, unit);
		gatherings.add(unitGathering);
		return unitGathering;
	}

	private void parseInvasiveControl(Gathering targetGathering, Unit unit) {
		Set<String> invasiveControls = targetGathering.factValues(INVASIVE_CONTROL);
		if (!invasiveControls.isEmpty()) {
			Qname value = Qname.fromURI(invasiveControls.iterator().next());
			InvasiveControl c = (InvasiveControl) EnumToProperty.getInstance().get(value);
			if (c == null) return;
			unit.addSourceTag(Annotation.toTag(c));
		}
	}

	private void parseLineTransectInfo(JSONObject unitJson, Unit unit) {
		if (!unitJson.hasKey(UNIT_FACT)) return;
		String obsType = unitJson.getObject(UNIT_FACT).getString("lineTransectObsType");
		if (!given(obsType)) return;

		if (!unitJson.hasKey("pairCount")) {
			unit.addFact(PAIR_COUNT_FACT, "1");
		}

		if ("MY.lineTransectObsTypeSong".equals(obsType) || "MY.lineTransectObsTypeOtherSound".equals(obsType)) {
			unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_HEARD);
		} else {
			unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_SEEN);
		}

		if ("MY.lineTransectObsTypeSeenMale".equals(obsType)) {
			unit.setSex(Sex.MALE);
			unit.setLifeStage(LifeStage.ADULT);
		} else if ("MY.lineTransectObsTypeSeenFemale".equals(obsType)) {
			unit.setSex(Sex.FEMALE);
			unit.setLifeStage(LifeStage.ADULT);
		}
		if ("MY.lineTransectObsTypeSeenPair".equals(obsType)) {
			unit.setLifeStage(LifeStage.ADULT);
		}

		if ("MY.lineTransectObsTypeSeenBrood".equals(obsType) || "MY.lineTransectObsTypeSeenNest".equals(obsType)) {
			unit.setBreedingSite(true);
		}
	}

	private Boolean parseWild(JSONObject unitJson) {
		if (unitJson.hasKey(WILD)) {
			if ("MY.wildWild".equals(unitJson.getString(WILD))) return true;
			if ("MY.wildNonWild".equals(unitJson.getString(WILD))) return false;
		}
		return null;
	}

	private Boolean parseLocal(JSONObject unitJson) {
		List<String> localStatuses = Arrays.asList("P", "kiert", "Ä", "Än");

		Boolean isLocal = null;

		if (unitJson.hasKey(MOVING_STATUS)) {

			for (String status : unitJson.getArray(MOVING_STATUS)) {
				if (localStatuses.contains(status.split(" ")[0])) {
					isLocal = true;
					break;
				} else if (status.split(" ")[0].equals("M")) {
					isLocal = false;
				}
			}
		}

		if (unitJson.hasKey(BREEDING) && unitJson.getBoolean(BREEDING)) {
			isLocal = true;
		}

		return isLocal;
	}

	private List<MediaObject> parseUnitMedia(JSONObject unitJson, Unit unit) {
		parseMedia(unitJson, unit.getMedia());
		if (unit.getMedia().isEmpty()) return Collections.emptyList();
		List<MediaObject> gatheringMedia = new ArrayList<>();
		Iterator<MediaObject> i = unit.getMedia().iterator();
		while (i.hasNext()) {
			MediaObject m = i.next();
			if (isGatheringMedia(m)) {
				i.remove();
				gatheringMedia.add(m);
			}
		}
		return gatheringMedia;
	}

	private boolean isGatheringMedia(MediaObject m) {
		return HABITAT_TYPE.equals(m.getType()) || m.getKeywords().stream().anyMatch(HABITAT_KEYWORDS::contains);
	}

	private TaxonConfidence parseTaxonConfidence(JSONObject json) {
		return getTaxonConfidence(json.getString(TAXON_CONFIDENCE));
	}

	private RecordBasis parseRecordBasis(JSONObject json) {
		return getRecordBasis(json.getString(RECORD_BASIS));
	}

	private void parseFacts(JSONObject json, List<Fact> facts) {
		for (String key : json.getKeys()) {
			if (IGNORED_FACTS.contains(key)) continue;
			if (key.startsWith("@")) continue;
			if (key.startsWith("_")) continue;
			if (json.isObject(key)) continue;
			if (json.isArray(key)) {
				for (String value : json.getArray(key)) {
					if (given(value)) {
						facts.add(toFact(key, value));
					}
				}
			} else {
				String value = json.reveal().optString(key);
				if (given(value)) {
					facts.add(toFact(key, value));
				}
			}
		}
		if (json.hasKey(GATHERING_FACT)) {
			parseFacts(json.getObject(GATHERING_FACT), facts);
		}
		if (json.hasKey(UNIT_FACT)) {
			parseFacts(json.getObject(UNIT_FACT), facts);
		}
		if (json.hasKey(UNIT_GATHERING)) {
			parseFacts(json.getObject(UNIT_GATHERING), facts);
		}
	}

	private Fact toFact(String key, String value) {
		ContextDefinition contextDefinition = contextDefinitions.getContextDefinition(key);
		if (contextDefinition == null) {
			return new Fact(key, value);
		}
		if (contextDefinition.isResource()) {
			Qname resource = fromUriOrQname(value);
			return new Fact(contextDefinition.getProperty().toURI(), resource.toURI());
		}
		return new Fact(contextDefinition.getProperty().toURI(), value);
	}

	private Qname fromUriOrQname(String value) {
		return value.startsWith("http://") ? Qname.fromURI(value) : new Qname(value);
	}

	private Date toDate(String date) throws DateValidationException {
		// 2016-11-01T12:29:25+02:00
		try {
			return DateUtils.convertToDate(date.split("T")[0], "yyyy-MM-dd");
		} catch (ParseException e) {
			throw new DateValidationException(Quality.Issue.INVALID_DATE, "Invalid date " + date);
		}
	}

	protected Qname parseCollectionId(JSONObject json) {
		String collectionId = json.getString(COLLECTION_ID);
		if (!given(collectionId)) return null;
		return new Qname(collectionId);
	}

	private static class DocumentIdentification {
		private final String det;
		private final String detDate;
		private DocumentIdentification(String det, String detDate) {
			this.det = det;
			this.detDate = detDate;
		}
	}

}