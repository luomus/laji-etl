package fi.laji.datawarehouse.etl.models.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class ApiUser {

	private final String email;
	private final Qname systemId;

	public ApiUser(String email) {
		this(email, null);
	}

	public ApiUser(String email, Qname systemId) {
		this.email = email;
		this.systemId = systemId;
	}

	public String getEmail() {
		return email;
	}

	public Qname getSystemId() {
		return systemId;
	}

	public String getSystemIdOrEmail() {
		if (systemId != null) {
			return systemId.toString();
		}
		return email;
	}

	public static ApiUser invalid() {
		return new ApiUser(null);
	}

	public boolean isValid() {
		return email != null;
	}
}
