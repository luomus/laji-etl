package fi.laji.datawarehouse.etl.models.containers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="collection")
public class CollectionEntity extends NameableEntity {

	private Long collectionQualityKey;

	public CollectionEntity() {}

	public CollectionEntity(String collectionId) {
		setId(collectionId);
	}

	@Column(name="collection_quality_key")
	public Long getCollectionQualityKey() {
		return collectionQualityKey;
	}

	public void setCollectionQualityKey(Long collectionQualityKey) {
		this.collectionQualityKey = collectionQualityKey;
	}	

	@Override
	public String toString() {
		return "CollectionEntity [collectionQualityKey=" + collectionQualityKey + ", getName()=" + getName() + ", getId()=" + getId() + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) throw new NullPointerException();
		if (!(o instanceof CollectionEntity)) throw new UnsupportedOperationException("Can only compare to " + CollectionEntity.class);
		return this.toString().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

}
