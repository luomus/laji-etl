package fi.laji.datawarehouse.etl.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;

public class NamedPlaceUpdater {

	private final DAO dao;

	public NamedPlaceUpdater(DAO dao) {
		this.dao = dao;
	}

	/**
	 * Insert and update named places
	 * @return list of updated named place ids
	 */
	public List<Qname> update() {
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to update named places");
		List<BaseEntity> entitiesToUpdate =  new ArrayList<>();
		List<BaseEntity> entitiesToInsert =  new ArrayList<>();
		VerticaDimensionsDAO dimensionsDAO = dao.getVerticaDimensionsDAO();
		List<NameableEntity> existingEntities = dimensionsDAO.getEntities(NamedPlaceEntity.class);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loaded existing from Vertica");
		Collection<NamedPlaceEntity> namedPlaces = dao.getNamedPlacesForceReload().values();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loaded from Lajistore, comparing..");
		for (NamedPlaceEntity namedPlace : namedPlaces) {
			NameableEntity existing = getExisting(existingEntities, namedPlace.getId());
			if (existing == null) {
				entitiesToInsert.add(namedPlace);
			} else if (!existing.equals(namedPlace)) {
				namedPlace.setKey(existing.getKey());
				entitiesToUpdate.add(namedPlace);
			}
		}

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Update: " + entitiesToUpdate.size() + " Insert: " + entitiesToInsert.size());

		if (!entitiesToUpdate.isEmpty()) {
			dimensionsDAO.updateEntities(entitiesToUpdate);
		}
		if (!entitiesToInsert.isEmpty()) {
			dimensionsDAO.insertEntities(entitiesToInsert);
		}

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Finished updating named places");
		return entitiesToUpdate.stream().map(e->Qname.fromURI(e.getId())).collect(Collectors.toList());
	}

	private NamedPlaceEntity getExisting(List<NameableEntity> existingEntities, String id) {
		for (NameableEntity e : existingEntities) {
			if (e.getId().equals(id)) return (NamedPlaceEntity) e;
		}
		return null;
	}

}
