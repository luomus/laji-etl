package fi.laji.datawarehouse.etl.models.dw;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.query.download.model.FileDownloadField;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;

@Embeddable
public class GatheringConversions {

	private Coordinates wgs84;
	private Coordinates ykj;
	private Coordinates euref;
	private Geo wgs84Geo;
	private Geo ykjGeo;
	private Geo eurefGeo;
	private SingleCoordinates wgs84CenterPoint;
	private SingleCoordinates eurefCenterPoint;
	private SingleCoordinates wgs84Grid1;
	private SingleCoordinates wgs84Grid05;
	private SingleCoordinates wgs84Grid01;
	private SingleCoordinates wgs84Grid005;
	private SingleCoordinates ykj100km;
	private SingleCoordinates ykj50km;
	private SingleCoordinates ykj10km;
	private SingleCoordinates ykj1km;
	private SingleCoordinates ykj100kmCenter;
	private SingleCoordinates ykj50kmCenter;
	private SingleCoordinates ykj10kmCenter;
	private SingleCoordinates ykj1kmCenter;
	private Long boundingBoxAreaInSquareMeters;
	private Integer linelengthInMeters;
	private Integer century;
	private Integer decade;
	private Integer year;
	private Integer month;
	private Integer day;
	private Integer seasonBegin;
	private Integer seasonEnd;
	private Integer dayOfYearBegin;
	private Integer dayOfYearEnd;
	private Qname birdAssociationArea;

	@FileDownloadField(name="WGS84", order=6)
	public Coordinates getWgs84() {
		return wgs84;
	}

	public void setWgs84(Coordinates wgs84) {
		if (wgs84 == null || wgs84.notInitialized()) {
			this.wgs84 = null;
			return;
		}
		this.wgs84 = wgs84;
	}

	private SingleCoordinates toGrid005(double centerPointLat, double centerPointLon) {
		double grid005Lat = toGrid005(centerPointLat);
		double grid005Lon = toGrid005(centerPointLon);
		SingleCoordinates grid005 = new SingleCoordinates(grid005Lat, grid005Lon, Type.WGS84);
		return grid005;
	}

	private SingleCoordinates toGrid01(double centerPointLat, double centerPointLon) {
		double gird01Lat = toGrid01(centerPointLat);
		double gird01Lon = toGrid01(centerPointLon);
		SingleCoordinates grid01 = new SingleCoordinates(gird01Lat, gird01Lon, Type.WGS84);
		return grid01;
	}

	private SingleCoordinates toGrid05(double centerPointLat, double centerPointLon) {
		double grid05Lat = toGrid05(centerPointLat);
		double grid05Lon = toGrid05(centerPointLon);
		SingleCoordinates grid05 = new SingleCoordinates(grid05Lat, grid05Lon, Type.WGS84);
		return grid05;
	}

	private SingleCoordinates toGrid1(double centerPointLat, double centerPointLon) {
		double grid1Lat = Math.floor(centerPointLat);
		double grid1Lon = Math.floor(centerPointLon);
		SingleCoordinates grid1 = new SingleCoordinates(grid1Lat, grid1Lon, Type.WGS84);
		return grid1;
	}

	public static double toGrid01(double d) {
		return Math.floor(d*10) / 10;
	}

	public static double toGrid05(double d) {
		return Math.floor(d*2) / 2;
	}

	public static double toGrid005(double d) {
		return Math.floor(d*20) / 20;
	}

	public SingleCoordinates getYkj100km() {
		if (ykj100km != null) return ykj100km;
		if (ykj == null) return null;
		ykj100km = Coordinates.convertYkj100km(ykj);
		return ykj100km;
	}

	public void setYkj100km(SingleCoordinates ykj100km) {
		this.ykj100km = ykj100km;
	}

	public SingleCoordinates getYkj50km() {
		if (ykj50km != null) return ykj50km;
		if (ykj == null) return null;
		ykj50km = Coordinates.convertYkj50km(ykj);
		return ykj50km;
	}

	public void setYkj50km(SingleCoordinates ykj50km) {
		this.ykj50km = ykj50km;
	}

	@StatisticsQueryAlllowedField
	public SingleCoordinates getYkj10km() {
		if (ykj10km != null) return ykj10km;
		if (ykj == null) return null;
		ykj10km = Coordinates.convertYkj10km(ykj);
		return ykj10km;
	}

	public void setYkj10km(SingleCoordinates ykj10km) {
		this.ykj10km = ykj10km;
	}

	public SingleCoordinates getYkj1km() {
		if (ykj1km != null) return ykj1km;
		if (ykj == null) return null;
		ykj1km = Coordinates.convertYkj1km(ykj);
		return ykj1km;
	}

	public void setYkj1km(SingleCoordinates ykj1km) {
		this.ykj1km = ykj1km;
	}

	public SingleCoordinates getYkj100kmCenter() {
		if (ykj100kmCenter != null) return ykj100kmCenter;
		if (ykj == null) return null;
		ykj100kmCenter = Coordinates.convertYkj100kmCenter(ykj);
		return ykj100kmCenter;
	}

	public void setYkj100kmCenter(SingleCoordinates ykj100kmCenter) {
		this.ykj100kmCenter = ykj100kmCenter;
	}

	public SingleCoordinates getYkj50kmCenter() {
		if (ykj50kmCenter != null) return ykj50kmCenter;
		if (ykj == null) return null;
		ykj50kmCenter = Coordinates.convertYkj50kmCenter(ykj);
		return ykj50kmCenter;
	}

	public void setYkj50kmCenter(SingleCoordinates ykj50kmCenter) {
		this.ykj50kmCenter = ykj50kmCenter;
	}

	@FileDownloadField(name="YKJ_10KM", order=11)
	@StatisticsQueryAlllowedField
	public SingleCoordinates getYkj10kmCenter() {
		if (ykj10kmCenter != null) return ykj10kmCenter;
		if (ykj == null) return null;
		ykj10kmCenter = Coordinates.convertYkj10kmCenter(ykj);
		return ykj10kmCenter;
	}

	public void setYkj10kmCenter(SingleCoordinates ykj10kmCenter) {
		this.ykj10kmCenter = ykj10kmCenter;
	}

	@FileDownloadField(name="YKJ_1KM", order=12)
	public SingleCoordinates getYkj1kmCenter() {
		if (ykj1kmCenter != null) return ykj1kmCenter;
		if (ykj == null) return null;
		ykj1kmCenter = Coordinates.convertYkj1kmCenter(ykj);
		return ykj1kmCenter;
	}

	public void setYkj1kmCenter(SingleCoordinates ykj1kmCenter) {
		this.ykj1kmCenter = ykj1kmCenter;
	}

	@FileDownloadField(name="WGS84CenterPoint", order=7)
	public SingleCoordinates getWgs84CenterPoint() {
		if (wgs84CenterPoint != null) return wgs84CenterPoint;
		if (wgs84 == null) return null;
		wgs84CenterPoint = wgs84.convertCenterPoint();
		return wgs84CenterPoint;
	}

	public void setWgs84CenterPoint(SingleCoordinates wgs84CenterPoint) {
		this.wgs84CenterPoint = wgs84CenterPoint;
	}

	@FileDownloadField(name="ETRS-TM35FINCenterPoint", order=7.5)
	public SingleCoordinates getEurefCenterPoint() {
		return eurefCenterPoint;
	}

	public void setEurefCenterPoint(SingleCoordinates eurefCenterPoint) {
		this.eurefCenterPoint = eurefCenterPoint;
	}

	@FileDownloadField(name="YKJ", order=10)
	public Coordinates getYkj() {
		return ykj;
	}

	public void setYkj(Coordinates ykj) {
		if (ykj == null || ykj.notInitialized()) {
			this.ykj = null;
			return;
		}
		this.ykj = ykj;
	}

	@FileDownloadField(name="ETRS-TM35FIN", order=10)
	public Coordinates getEuref() {
		return euref;
	}

	public void setEuref(Coordinates euref) {
		if (euref == null || euref.notInitialized()) {
			this.euref = null;
			return;
		}
		this.euref = euref;
		this.eurefCenterPoint = euref.convertCenterPoint();
	}

	public SingleCoordinates getWgs84Grid1() {
		if (wgs84Grid1 != null) return wgs84Grid1;
		if (wgs84 == null) return null;
		if (wgs84.getAccuracyInMeters() == null || wgs84.getAccuracyInMeters() > 100000) return null;
		wgs84Grid1 = toGrid1(getWgs84CenterPoint().getLat(), getWgs84CenterPoint().getLon());
		return wgs84Grid1;
	}

	public void setWgs84Grid1(SingleCoordinates wgs84Grid1) {
		this.wgs84Grid1 = wgs84Grid1;
	}

	public SingleCoordinates getWgs84Grid05() {
		if (wgs84Grid05 != null) return wgs84Grid05;
		if (wgs84 == null) return null;
		if (wgs84.getAccuracyInMeters() == null || wgs84.getAccuracyInMeters() > 100000) return null;
		wgs84Grid05 = toGrid05(getWgs84CenterPoint().getLat(), getWgs84CenterPoint().getLon());
		return wgs84Grid05;
	}

	public void setWgs84Grid05(SingleCoordinates wgs84Grid05) {
		this.wgs84Grid05 = wgs84Grid05;
	}

	public SingleCoordinates getWgs84Grid01() {
		if (wgs84Grid01 != null) return wgs84Grid01;
		if (wgs84 == null) return null;
		if (wgs84.getAccuracyInMeters() == null || wgs84.getAccuracyInMeters() > 10000) return null;
		wgs84Grid01 = toGrid01(getWgs84CenterPoint().getLat(), getWgs84CenterPoint().getLon());
		return wgs84Grid01;
	}

	public void setWgs84Grid01(SingleCoordinates wgs84Grid01) {
		this.wgs84Grid01 = wgs84Grid01;
	}

	public SingleCoordinates getWgs84Grid005() {
		if (wgs84Grid005 != null) return wgs84Grid005;
		if (wgs84 == null) return null;
		if (wgs84.getAccuracyInMeters() == null || wgs84.getAccuracyInMeters() > 10000) return null;
		wgs84Grid005 = toGrid005(getWgs84CenterPoint().getLat(), getWgs84CenterPoint().getLon());
		return wgs84Grid005;
	}

	public void setWgs84Grid005(SingleCoordinates wgs84Grid005) {
		this.wgs84Grid005 = wgs84Grid005;
	}

	@FileDownloadField(name="Century", order=50)
	public Integer getCentury() {
		return century;
	}

	public void setCentury(Integer century) {
		this.century = century;
	}

	@FileDownloadField(name="Decade", order=51)
	public Integer getDecade() {
		return decade;
	}

	public void setDecade(Integer decade) {
		this.decade = decade;
	}

	@FileDownloadField(name="Year", order=52)
	@StatisticsQueryAlllowedField
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@FileDownloadField(name="Month", order=53)
	@StatisticsQueryAlllowedField
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@FileDownloadField(name="Day", order=54)
	@StatisticsQueryAlllowedField
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	@StatisticsQueryAlllowedField(allowFor=Base.GATHERING)
	public Integer getSeasonBegin() {
		return seasonBegin;
	}

	public void setSeasonBegin(Integer seasonBegin) {
		this.seasonBegin = seasonBegin;
	}

	@StatisticsQueryAlllowedField(allowFor=Base.GATHERING)
	public Integer getSeasonEnd() {
		return seasonEnd;
	}

	public void setSeasonEnd(Integer seasonEnd) {
		this.seasonEnd = seasonEnd;
	}

	@FileDownloadField(name="DayOfYearBegin", order=61)
	@StatisticsQueryAlllowedField(allowFor=Base.GATHERING)
	public Integer getDayOfYearBegin() {
		return dayOfYearBegin;
	}

	public void setDayOfYearBegin(Integer dayOfYearBegin) {
		this.dayOfYearBegin = dayOfYearBegin;
	}

	@FileDownloadField(name="DayOfYearEnd", order=62)
	@StatisticsQueryAlllowedField(allowFor=Base.GATHERING)
	public Integer getDayOfYearEnd() {
		return dayOfYearEnd;
	}

	public void setDayOfYearEnd(Integer dayOfYearEnd) {
		this.dayOfYearEnd = dayOfYearEnd;
	}

	@Transient
	public Geo getWgs84Geo() {
		return wgs84Geo;
	}

	public void setWgs84Geo(Geo wgs84Geo) {
		this.wgs84Geo = wgs84Geo;
	}

	@Transient
	public Geo getYkjGeo() {
		return ykjGeo;
	}

	public void setYkjGeo(Geo ykjGeo) {
		this.ykjGeo = ykjGeo;
	}

	@Transient
	public Geo getEurefGeo() {
		return eurefGeo;
	}

	public void setEurefGeo(Geo eurefGeo) {
		this.eurefGeo = eurefGeo;
	}

	@Transient
	public String getYkjWKT() {
		if (this.ykjGeo == null) return null;
		return this.ykjGeo.getWKT();
	}

	public String getEurefWKT() {
		if (this.eurefGeo == null) return null;
		return this.eurefGeo.getWKT();
	}

	@Transient
	@FileDownloadField(name="WGS84_WKT", order=63)
	public String getWgs84WKT() {
		if (this.wgs84Geo == null) return null;
		return this.wgs84Geo.getWKT();
	}

	@Deprecated // for hibernate
	public void setEurefWKT(@SuppressWarnings("unused") String wkt) {}

	public Long getBoundingBoxAreaInSquareMeters() {
		if (boundingBoxAreaInSquareMeters != null) return boundingBoxAreaInSquareMeters;
		if (ykj == null) return null;
		boundingBoxAreaInSquareMeters = ykj.calculateBoundingBoxAreaInSquareMeters();
		return boundingBoxAreaInSquareMeters;
	}

	public void setBoundingBoxAreaInSquareMeters(Long boundingBoxAreaInSquareMeters) {
		this.boundingBoxAreaInSquareMeters = boundingBoxAreaInSquareMeters;
	}

	public Integer getLinelengthInMeters() {
		return linelengthInMeters;
	}

	public void setLinelengthInMeters(Integer linelengthInMeters) {
		this.linelengthInMeters = linelengthInMeters;
	}

	@StatisticsQueryAlllowedField(allowFor=Base.UNIT)
	@Transient
	public Qname getBirdAssociationArea() {
		return birdAssociationArea;
	}

	@Transient
	public void setBirdAssociationArea(Qname birdAssociationAreaId) {
		this.birdAssociationArea = birdAssociationAreaId;
	}

}
