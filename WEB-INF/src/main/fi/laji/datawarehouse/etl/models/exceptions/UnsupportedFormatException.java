package fi.laji.datawarehouse.etl.models.exceptions;

public class UnsupportedFormatException extends Exception {

	private static final long serialVersionUID = -6672246904374282153L;

	public UnsupportedFormatException(String message) {
		super(message);
	}

}
