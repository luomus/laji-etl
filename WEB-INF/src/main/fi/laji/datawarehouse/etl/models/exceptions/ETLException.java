package fi.laji.datawarehouse.etl.models.exceptions;

public class ETLException extends RuntimeException {

	private static final long serialVersionUID = 5383143200333998086L;

	public ETLException(Throwable cause) {
		super(cause);
	}

	public ETLException(String message, Throwable cause) {
		super(message, cause);
	}

	public ETLException(String message) {
		super(message);
	}

	public ETLException(String identifier, String message) {
		super(generateMessage(identifier, message));
	}

	public ETLException(String identifier, String message, Throwable cause) {
		super(generateMessage(identifier, message), cause);
	}

	private static String generateMessage(String identifier, String message) {
		if (identifier == null) return message;
		if (message == null) return identifier;
		return identifier + ": " + message;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Exception)) return false;
		Exception other = (Exception) o;
		if (this.getMessage() == null && other.getMessage() == null) return true;
		if (this.getMessage() == null && other.getMessage() != null) return false;
		if (this.getMessage() != null && other.getMessage() == null) return false;
		return this.getMessage().equals(other.getMessage());
	}

	@Override
	public int hashCode() {
		if (this.getMessage() == null) return 0;
		return this.getMessage().hashCode();
	}

}
