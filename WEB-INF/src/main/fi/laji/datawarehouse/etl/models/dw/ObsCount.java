package fi.laji.datawarehouse.etl.models.dw;

import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;

public class ObsCount {

	public final Qname taxonId;
	public int count = 0;
	public int countFinland = 0;
	public Map<Qname, Integer> biogeographicalProvinceCounts;
	public Map<String, Integer> habitatOccurrenceCounts;

	public ObsCount(Qname taxonId) {
		this.taxonId = taxonId;
	}

	@Override
	public String toString() {
		return "ObsCount [taxonId=" + taxonId + ", count=" + count + ", countFinland=" + countFinland + ", biogeographicalProvinceCounts=" + biogeographicalProvinceCounts
				+ ", habitatOccurrenceCounts=" + habitatOccurrenceCounts + "]";
	}

	public Qname getTaxonId() {
		return taxonId;
	}

	public int getCount() {
		return count;
	}

	public int getCountFinland() {
		return countFinland;
	}

	public Map<Qname, Integer> getBiogeographicalProvinceCounts() {
		return biogeographicalProvinceCounts;
	}

	public Map<String, Integer> getHabitatOccurrenceCounts() {
		return habitatOccurrenceCounts;
	}

}