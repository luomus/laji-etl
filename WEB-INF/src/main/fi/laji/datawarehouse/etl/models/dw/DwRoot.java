package fi.laji.datawarehouse.etl.models.dw;

import java.util.ArrayList;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class DwRoot {

	private Integer processTime = (int) DateUtils.getCurrentEpoch();
	private final Qname documentId;
	private final Qname sourceId;
	private Qname collectionId;
	private Document publicDocument;
	private Document privateDocument;
	private List<Document> splittedPublicDocuments;
	private List<Annotation> annotations;
	private boolean deleteRequest = false;

	public DwRoot(Qname documentId, Qname sourceId) throws CriticalParseFailure {
		Util.validateId(documentId, "documentId");
		Util.validateId(sourceId, "sourceId");
		this.documentId = documentId;
		this.sourceId = sourceId;
	}

	private DwRoot(Qname sourceId) throws CriticalParseFailure {
		Util.validateId(sourceId, "sourceId");
		this.sourceId = sourceId;
		this.documentId = null;
	}

	public void setCollectionId(Qname collectionId) throws CriticalParseFailure {
		Util.validateId(collectionId, "collectionId");
		this.collectionId = collectionId;
	}

	public JSONObject toJSON() {
		return ModelToJson.rootToJson(this);
	}

	public static DwRoot fromJson(JSONObject json) throws CriticalParseFailure  {
		return JsonToModel.rootFromJson(json);
	}

	@FieldDefinition(order=1.0)
	public Qname getDocumentId() {
		return documentId;
	}

	@FieldDefinition(order=1.2)
	public Qname getCollectionId() {
		return collectionId;
	}

	@FieldDefinition(ignoreForETL=true)
	public Qname getNamedPlaceId() {
		if (privateDocument != null) {
			if (privateDocument.getNamedPlaceId() != null)
				return Qname.fromURI(privateDocument.getNamedPlaceId());
		}
		if (publicDocument != null) {
			if (publicDocument.getNamedPlaceId() != null)
				return Qname.fromURI(publicDocument.getNamedPlaceId());
		}
		return null;
	}

	@FieldDefinition(order=2.0)
	public Document getPublicDocument() {
		return publicDocument;
	}

	@FieldDefinition(order=2.1)
	public Document getPrivateDocument() {
		return privateDocument;
	}

	public void setPublicDocument(Document publicDocument) {
		validateConcealment(publicDocument, Concealment.PUBLIC);
		this.publicDocument = publicDocument;
	}

	public void setPrivateDocument(Document privateDocument) {
		validateConcealment(privateDocument, Concealment.PRIVATE);
		this.privateDocument = privateDocument;
	}

	private void validateConcealment(Document d, Concealment expected) {
		if (d == null) return;
		if (Boolean.TRUE.equals(d.isDeleted())) return;
		if (expected.equals(d.getConcealment())) return;
		if (expected == Concealment.PRIVATE) {
			throw new IllegalStateException("Private document is marked public");
		}
		throw new IllegalStateException("Public document is marked private");
	}

	public Document createPrivateDocument() throws CriticalParseFailure {
		if (privateDocument != null) throw new IllegalStateException("Asking to create a private document, when one already exists");
		this.privateDocument = Document.fromRoot(Concealment.PRIVATE, this);
		return privateDocument;
	}

	public Document createPublicDocument() throws CriticalParseFailure {
		if (publicDocument != null) throw new IllegalStateException("Asking to create a public document, when one already exists");
		this.publicDocument = Document.fromRoot(Concealment.PUBLIC, this);
		return publicDocument;
	}

	@FieldDefinition(order=1.1)
	public Qname getSourceId() {
		return sourceId;
	}

	@FieldDefinition(ignoreForETL=true)
	public Integer getProcessTime() {
		return processTime;
	}

	public static DwRoot createDeleteRequest(Qname documentId, Qname source) throws CriticalParseFailure {
		DwRoot deleteRequest = new DwRoot(documentId, source);
		deleteRequest.setDeleteRequest(true);
		return deleteRequest;
	}

	@FieldDefinition(order=5.0)
	public boolean isDeleteRequest() {
		return deleteRequest;
	}

	public void setDeleteRequest(boolean deleteRequest) {
		this.deleteRequest = deleteRequest;
	}

	public DwRoot addSplittedPublicDocument(Document splittedDocument) {
		if (splittedPublicDocuments == null) {
			splittedPublicDocuments = new ArrayList<>();
		}
		splittedPublicDocuments.add(splittedDocument);
		return this;
	}

	public boolean hasSplittedPublicDocuments() {
		return splittedPublicDocuments != null && !splittedPublicDocuments.isEmpty();
	}

	@FieldDefinition(ignoreForETL=true)
	public List<Document> getSplittedPublicDocuments() {
		return splittedPublicDocuments;
	}

	public DwRoot addAnnotation(Annotation annotation) {
		if (annotations == null) {
			annotations = new ArrayList<>();
		}
		annotations.add(annotation);
		return this;
	}

	public boolean hasAnnotations() {
		return annotations != null && !annotations.isEmpty();
	}

	@FieldDefinition(ignoreForETL=true)
	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public boolean hasOnlyAnnotations() {
		return hasAnnotations() && getPublicDocument() == null && getPrivateDocument() == null;
	}

	public static DwRoot forAnnotations(Qname source) throws CriticalParseFailure {
		DwRoot root = new DwRoot(source);
		return root;
	}

	public DwRoot clearProcesstime() {
		this.processTime = null;
		return this;
	}

}
