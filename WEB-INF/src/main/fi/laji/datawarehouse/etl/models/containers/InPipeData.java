package fi.laji.datawarehouse.etl.models.containers;

import java.util.Date;

import fi.luomus.commons.containers.rdf.Qname;

public class InPipeData extends PipeData {

	private final String contentType;
	private final boolean unrecoverableError;

	public InPipeData(long id, Qname source, String data, String contentType, Date timestamp, String errorMessage, boolean unrecoverableError) {
		super(id, source, data, timestamp, errorMessage);
		this.contentType = contentType;
		this.unrecoverableError = unrecoverableError;
	}

	public String getContentType() {
		return contentType;
	}

	public boolean isUnrecoverableError() {
		return unrecoverableError;
	}

	public InPipeData setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
		return this;
	}

	@Override
	public String toString() {
		return "InPipeData [contentType=" + contentType + ", unrecoverableError=" + unrecoverableError + "] " + super.toString();
	}

}
