package fi.laji.datawarehouse.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import fi.laji.datawarehouse.dao.LookupNameProvider.LookupName;
import fi.laji.datawarehouse.dao.LookupNameProvider.LookupName.Type;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;

public class TaxonLookupNameGenerator {

	private static final Qname SUBGENUS = new Qname("MX.subgenus");
	private static final Qname SPECIES = new Qname("MX.species");
	private static final Qname SUBSPECIES = new Qname("MX.subspecies");
	public static final String HYDRIB_MARK_MULTIPLICATION = "×";
	public static final String HYDRIB_MARK_X = " x ";
	private static final String SP = "sp.";
	private static final String SPP = "spp.";
	private static final String SUBSP = "subsp.";
	private static final String SSP = "ssp.";

	public void generateLookupNames(Taxon taxon, List<LookupName> lookupNames) {
		Qname taxonId = taxon.getQname();
		Qname reference = taxon.getChecklist();
		String scientificName = taxon.getScientificName();

		addTaxonId(lookupNames, taxon, taxonId);
		addScientificName(taxon, lookupNames, taxonId, scientificName, reference, Type.PRIMARY);
		String subgenusChildName = getPossibleSubgenusChildSpeciesName(taxon);
		if (subgenusChildName != null) {
			addScientificName(taxon, lookupNames, taxonId, subgenusChildName, reference, Type.PRIMARY);
		}

		for (String vernacularName : taxon.getVernacularName().getAllTexts().values()) {
			boolean valid = addNameIfValid(taxonId, lookupNames, vernacularName, reference, taxon.isFinnish(), Type.PRIMARY, false);
			if (!valid) continue;
			addVernacularNameScientificNameCombinations(taxon, lookupNames, taxonId, vernacularName, Type.PRIMARY);
		}

		List<String> vernacularNames = new ArrayList<>();
		vernacularNames.addAll(taxon.getAlternativeVernacularNames().getAllValues());
		vernacularNames.addAll(taxon.getObsoleteVernacularNames().getAllValues());
		vernacularNames.addAll(taxon.getTradeNames().getAllValues());
		for (String vernacularName : vernacularNames) {
			boolean valid = addNameIfValid(taxonId, lookupNames, vernacularName, reference, taxon.isFinnish(), Type.SECONDARY, false);
			if (!valid) continue;
			addVernacularNameScientificNameCombinations(taxon, lookupNames, taxonId, vernacularName, Type.SECONDARY);
		}

		vernacularNames.addAll(taxon.getVernacularName().getAllTexts().values());
		removeInvalidNames(vernacularNames);

		addSynonyms(taxon.getBasionyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getObjectiveSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getSubjectiveSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getHomotypicSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getHeterotypicSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getMisspelledNames(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getOrthographicVariants(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getUncertainSynonyms(), lookupNames, taxonId, reference, vernacularNames);
		addSynonyms(taxon.getAlternativeNames(), lookupNames, taxonId, reference, vernacularNames);
		addMissaplied(taxon.getMisappliedNames(), lookupNames, taxonId, reference);

		for (String akaName : taxon.getAlsoKnownAsNames()) {
			addNameIfValid(taxonId, lookupNames, akaName, reference, taxon.isFinnish(), Type.SECONDARY, false);
		}

		addNameIfValid(taxonId, lookupNames, taxon.getBirdlifeCode(), reference, taxon.isFinnish(), Type.PRIMARY, true);
		addNameIfValid(taxonId, lookupNames, taxon.getEuringCode(), reference, taxon.isFinnish(), Type.PRIMARY, true);

		for (String overridingName : taxon.getOverridingTargetNames()) {
			addNameIfValid(taxonId, lookupNames, overridingName, reference, taxon.isFinnish(), Type.PRIMARY, true);
		}
	}

	private void removeInvalidNames(List<String> names) {
		Iterator<String> i = names.iterator();
		while (i.hasNext()) {
			String s = i.next();
			if (!validCleanedName(s)) i.remove();
		}
	}

	private void addVernacularNameScientificNameCombinations(Taxon taxon, List<LookupName> lookupNames, Qname taxonId, String vernacularName, Type type) {
		String scientificName = taxon.getScientificName();
		Qname reference = taxon.getChecklist();
		if (!given(taxon.getScientificName())) return;

		addNameIfValid(taxonId, lookupNames, vernacularName + scientificName, reference, taxon.isFinnish(), type, false);
		addNameIfValid(taxonId, lookupNames, scientificName + vernacularName, reference, taxon.isFinnish(), type, false);

		String subgenusChildScientificName = getPossibleSubgenusChildSpeciesName(taxon);
		if (subgenusChildScientificName != null) {
			addNameIfValid(taxonId, lookupNames, vernacularName + subgenusChildScientificName, reference, taxon.isFinnish(), type, false);
			addNameIfValid(taxonId, lookupNames, subgenusChildScientificName + vernacularName, reference, taxon.isFinnish(), type, false);
		}

		if (!given(taxon.getScientificNameAuthorship())) return;

		addNameIfValid(taxonId, lookupNames, vernacularName + scientificName + taxon.getScientificNameAuthorship(), reference, taxon.isFinnish(), type, false);
		addNameIfValid(taxonId, lookupNames, scientificName + taxon.getScientificNameAuthorship() + vernacularName , reference, taxon.isFinnish(), type, false);
		if (subgenusChildScientificName != null) {
			addNameIfValid(taxonId, lookupNames, vernacularName + subgenusChildScientificName + taxon.getScientificNameAuthorship(), reference, taxon.isFinnish(), type, false);
			addNameIfValid(taxonId, lookupNames, subgenusChildScientificName + taxon.getScientificNameAuthorship() + vernacularName , reference, taxon.isFinnish(), type, false);
		}
	}

	private String getPossibleSubgenusChildSpeciesName(Taxon taxon) {
		if (isSpecies(taxon) && taxon.hasParent() && isSubGenus(taxon.getParent())) {
			String[] parts = taxon.getScientificName().split(Pattern.quote(" "));
			if (parts.length == 2) {
				return taxon.getParent().getScientificName() + parts[1];
			}
		}
		return null;
	}

	private void addSynonyms(Collection<Taxon> synonyms, List<LookupName> lookupNames, Qname synonymParentTaxonId, Qname reference, List<String> vernacularNames) {
		for (Taxon synonym : synonyms) {
			addTaxonId(lookupNames, synonym, synonymParentTaxonId);
			boolean valid = addScientificName(synonym, lookupNames, synonymParentTaxonId, synonym.getScientificName(), reference, Type.SECONDARY);
			if (!valid) continue;
			for (String vernacularName : vernacularNames) {
				addVernacularNameScientificNameCombinations(synonym, lookupNames, synonymParentTaxonId, vernacularName, Type.SECONDARY);
			}
		}
	}

	private void addMissaplied(Collection<Taxon> misappliedNames, List<LookupName> lookupNames, Qname synonymParentTaxonId, Qname reference) {
		for (Taxon misappliedName : misappliedNames) {
			addTaxonId(lookupNames, misappliedName, synonymParentTaxonId);

			if (!validCleanedName(misappliedName.getScientificName())) continue;

			String lookupString1 = TaxonLinkingService.toTargetLookupString(misappliedName.getScientificName(), reference, null, true);
			String lookupString2 = TaxonLinkingService.toTargetLookupString(misappliedName.getScientificName(), null, null, true);
			lookupNames.add(new LookupName(lookupString1, synonymParentTaxonId, Type.SECONDARY));
			lookupNames.add(new LookupName(lookupString2, synonymParentTaxonId, Type.SECONDARY));
		}
	}

	private void addTaxonId(List<LookupName> lookupNames, Taxon taxon, Qname taxonId) {
		addTaxonId(taxonId, lookupNames, taxon.getId(), taxon.getChecklist());

		if (taxon.getChecklist() != null && !masterChecklist(taxon.getChecklist())) {
			addTaxonId(taxonId, lookupNames, taxon.getId(), null); // For non-master checklist taxa, also add a link without checklist reference
		}
	}

	private boolean addScientificName(Taxon taxon, List<LookupName> lookupNames, Qname id, String scientificName, Qname reference, Type type) {
		if (!validCleanedName(scientificName)) return false;
		List<String> combinations = new ArrayList<>();
		combinations.add(scientificName);

		if (scientificName.contains("(") && scientificName.contains(")") && !isSubGenus(taxon)) {
			if (scientificName.indexOf("(") < scientificName.indexOf(")") && !scientificName.endsWith(")")) {
				String subgenusRemoved = scientificName.substring(0, scientificName.indexOf('(')) + scientificName.substring(scientificName.indexOf(")"));
				combinations.add(subgenusRemoved);
			}
		}

		if (!taxon.isSpecies()) {
			List<String> spCombinations = new ArrayList<>();
			for (String name : combinations) {
				spCombinations.add(name);
				spCombinations.add(name + SP);
				spCombinations.add(name + SPP);
			}
			combinations = spCombinations;
		}

		if (given(taxon.getScientificNameAuthorship())) {
			List<String> authorCombinations = new ArrayList<>();
			for (String name : combinations) {
				authorCombinations.add(name);
				authorCombinations.add(name + taxon.getScientificNameAuthorship());
			}
			combinations = authorCombinations;
		}

		if (isSubSpecies(taxon) && !scientificName.contains(SSP) && !scientificName.contains(SUBSP)) {
			String subsp = addSubSpeciesToName(scientificName);
			if (subsp != null) combinations.add(subsp);
		}

		if (scientificName.contains(SSP)) {
			List<String> sspCombinations = new ArrayList<>();
			for (String name : combinations) {
				sspCombinations.add(name);
				sspCombinations.add(name.replace(SSP, SUBSP));
			}
			combinations = sspCombinations;
		}

		if (scientificName.contains(SUBSP)) {
			List<String> subspCombinations = new ArrayList<>();
			for (String name : combinations) {
				subspCombinations.add(name);
				subspCombinations.add(name.replace(SUBSP, SSP));
			}
			combinations = subspCombinations;
		}

		if (scientificName.contains(HYDRIB_MARK_X)) {
			List<String> xCombinations = new ArrayList<>();
			for (String name : combinations) {
				xCombinations.add(name);
				xCombinations.add(name.replace(HYDRIB_MARK_X, HYDRIB_MARK_MULTIPLICATION));
			}
			combinations = xCombinations;
		}

		if (scientificName.contains(HYDRIB_MARK_MULTIPLICATION)) {
			List<String> hydribMarkCombinations = new ArrayList<>();
			for (String name : combinations) {
				hydribMarkCombinations.add(name);
				hydribMarkCombinations.add(name.replace(HYDRIB_MARK_MULTIPLICATION, HYDRIB_MARK_X));
			}
			combinations = hydribMarkCombinations;
		}

		for (String name : combinations) {
			addNameIfValid(id, lookupNames, name, reference, taxon.isFinnish(), type, false);
		}
		return true;
	}

	private String addSubSpeciesToName(String scientificName) {
		String[] parts = scientificName.split(Pattern.quote(" "));
		if (parts.length != 3) return null;
		return parts[0] + " " + parts[1] + SUBSP + " " + parts[2];
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private boolean isSubGenus(Taxon taxon) {
		return SUBGENUS.equals(taxon.getTaxonRank());
	}

	private boolean isSubSpecies(Taxon taxon) {
		return SUBSPECIES.equals(taxon.getTaxonRank());
	}

	private boolean isSpecies(Taxon taxon) {
		return SPECIES.equals(taxon.getTaxonRank());
	}

	private boolean addNameIfValid(Qname taxonId, List<LookupName> lookupNames, String name, Qname reference, boolean finnishTaxon, LookupName.Type type, boolean isOverriding) {
		if (!validCleanedName(name)) return false;
		Collection<String> targetLookupStrings = TaxonLinkingService.toTargetLookupStrings(name, reference, taxonId, finnishTaxon);
		for (String lookupString : targetLookupStrings) {
			LookupName lookupName = new LookupName(lookupString, taxonId, type);
			if (isOverriding) lookupName.setToOverride();
			lookupNames.add(lookupName);
		}
		return true;
	}

	private void addTaxonId(Qname taxonId, List<LookupName> lookupNames, Qname idToLink, Qname reference) {
		Collection<String> targetLookupStrings = TaxonLinkingService.toTaxonIdLookupStrings(idToLink, reference);
		for (String lookupString : targetLookupStrings) {
			LookupName lookupName = new LookupName(lookupString, taxonId, Type.PRIMARY);
			lookupName.setToOverride();
			lookupNames.add(lookupName);
		}
	}

	private boolean validCleanedName(String value) {
		if (!given(value)) return false;
		return given(TaxonLinkingService.cleanTargetName(value));
	}

	private boolean masterChecklist(Qname reference) {
		return Const.MASTER_CHECKLIST_QNAME.equals(reference);
	}

}