package fi.laji.datawarehouse.dao;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.luomus.commons.containers.rdf.Qname;

public interface MediaDAO {

	MediaObject getMediaObject(Qname id);

	List<MediaObject> getMediaObjects(Qname documentId);

}
