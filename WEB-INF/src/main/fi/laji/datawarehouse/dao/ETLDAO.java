package fi.laji.datawarehouse.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OriginalIds;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeStats;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.luomus.commons.containers.rdf.Qname;

public interface ETLDAO {

	public static enum PipeSearchParams { RECEIVED, WAITING, IN_ERROR, PROCESSED, DELETED }

	/**
	 * Store document ids to temp reprocess table for manual reprocess marking
	 * @param documentIds
	 */
	void storeReprocessDocumentIds(Iterable<String> documentIds);

	/**
	 * Clear temp reprocess table
	 */
	void clearReprocessDocumentIds();

	/**
	 * Store data to IN pipe
	 * @param source system-id For example "KE.1"
	 * @param data The data
	 * @param contentType Mime type
	 */
	void storeToInPipe(Qname source, String data, String contentType);

	/**
	 * Store data to IN pipe
	 * @param source source system-id For example "KE.1"
	 * @param data Batch of data
	 * @param contentType Mime type
	 */
	void storeToInPipe(Qname source, List<String> data, String contentType);

	/**
	 * Store data to IN pipe - set on hold: won't be processed
	 * @param source source system-id For example "KE.1"
	 * @param data Batch of data
	 * @param contentType Mime type
	 */
	void storeToInPipeOnHold(Qname source, List<String> data, String contentType);

	/**
	 * Release in pipe entries for source that have been uploaded but set on hold
	 * 	@param source
	 */
	void releaseInPipeHold(Qname source);

	/**
	 * If nothing existing in out pipe with document id, inserts row data. If with document id there is already data, will check
	 * if this data is newer (higher in pipe id) and replaces the old data with the given data.
	 * If this data is older, will do nothing
	 * @param data
	 * @return true if caused changes, false if not
	 */
	boolean storeToOutPipe(List<OutPipeData> data);

	/**
	 * Get list of sources with unprocessed documents
	 * @return
	 */
	Set<Qname> getUnprosessedSources();

	/**
	 * Numbers of received, processed, waiting and in error in and out pipe entries by source
	 * @param forceReload true: return up-to-date stats, false: return possibly stale stats
	 * @return
	 */
	PipeStats getPipeStats(boolean forceReload);

	/**
	 * Get latest unprocessed from in pipe that is not erroneous, starting from previously unattempted
	 * @param source
	 * @return
	 */
	List<InPipeData> getUnprocessedNotErroneousInOrderFromInPipe(Qname source);

	/**
	 * Get latest unprocess from out pipe that are not erroneous, starting from previously unattempted
	 * @param source
	 * @return
	 */
	List<OutPipeData> getUnprocessedNotErroneousInOrderFromOutPipe(Qname source);

	/**
	 * Report entry attempted
	 * @param data
	 * @param errorMessage
	 */
	void reportAttempted(List<? extends PipeData> data, String errorMessage);

	/**
	 * Report entry permanently failed: will not be re-attempted
	 * @param data
	 * @param buildStackTrace
	 */
	void reportPermantentyFailed(List<InPipeData> data, String errorMessage);

	/**
	 * Report entry processed
	 * @param data
	 */
	void reportProcessed(List<? extends PipeData> data);

	/**
	 * Get entry by id from in pipe
	 * @param id
	 * @return
	 */
	InPipeData getFromInPipe(long id);

	/**
	 * Get entry by id from out pipe
	 * @param id
	 * @return
	 */
	OutPipeData getFromOutPipe(long id);

	/**
	 * Get entry by document id form out pipe
	 * @param documentId
	 * @return
	 */
	OutPipeData getFromOutPipe(Qname documentId);

	/**
	 * Get top rows from in pipe
	 * @param source
	 * @param searchParams
	 * @return
	 */
	List<InPipeData> getFromInPipeForSource(Qname source, ETLDAO.PipeSearchParams searchParams);

	/**
	 * Get top rows from out pipe
	 * @param source
	 * @param searchParams
	 * @return
	 */
	List<OutPipeData> getFromOutPipeForSource(Qname source, ETLDAO.PipeSearchParams searchParams);

	/**
	 * Set in pipe entry to be reprocessed
	 * @param id
	 * @return
	 */
	void markReprocessInPipe(long id);

	/**
	 * Set out pipe entry to be reprocessed
	 * @param id
	 * @return
	 */
	void markReprocessOutPipe(long id);

	/**
	 * Remove entry from in pipe
	 * @param id
	 */
	void removeInPipe(long id);

	/**
	 * Remove entry from out pipe
	 * @param id
	 */
	void removeOutPipe(long id);

	/**
	 * Get erroneous data form in pipe that has been attempted less times than other erroneous
	 * @param source
	 * @return
	 */
	List<InPipeData> getFailedAttemptDataFromInPipe(Qname source);

	/**
	 * Get erroneous data from out pipe that has been attempted 0 or 1 times
	 * @param source
	 * @return
	 */
	List<OutPipeData> getFailedAttemptDataFromOutPipe(Qname source);

	/**
	 * Get single data from out pipe that has been attempted the least times
	 * @param source
	 * @return data or null if none
	 */
	OutPipeData getSingleMultiAttemptedDataFromOutPipe(Qname source);

	/**
	 * Mark out pipe entry for reprosessing using document id
	 * @param documentId
	 * @return number of affected outpipe rows
	 */
	int markReprocessOutPipe(Qname documentId);

	/**
	 * Mark out pipe entries for reprocessing using named place ids
	 * @param namedPlaceIds
	 * @return number of affected outpipe rows
	 */
	int markReprocessOutPipeByNamedPlaceIds(List<Qname> namedPlaceIds);

	/**
	 * Ask Kastikka to send latest data to in pipe
	 */
	void callKastikkaSync();

	/**
	 * Store splitted document to document queue using random expire (expire = when the load)
	 * @param source system-id For example "KE.1"
	 * @param originalDocumentId
	 * @param data
	 */
	void storeSplittedDocumentToQueue(Qname source, Qname originalDocumentId, String data);

	/**
	 * Return splitted document ids that have created based on these originalDocumentIds
	 * @param originalDocumentIds
	 * @return
	 */
	SplittedDocumentIds getSplittedDocumentIds(Set<Qname> originalDocumentIds);

	/**
	 * Remove all splitted documents from queue for theseoriginal document ids
	 * @param originalDocumentIds
	 */
	void removeSplittedDocumentsFromQueue(Set<Qname> originalDocumentIds);

	/**
	 * Get original document and unit ids for splitted document
	 * @param splittedDocumentId
	 * @return
	 */
	OriginalIds getOriginalIdsOfSplittedDocument(Qname splittedDocumentId);

	/**
	 * Get splitted document ids mapped to original document ids
	 * @return
	 */
	Map<String, SplittedDocumentIdEntity> getAllSplittedIdToOriginalId();

	/**
	 * Get existing splitted document ids or generate and persist new ids for splitted units
	 * @param documentId or original document
	 * @param unitIds to be splitted from original
	 * @return
	 */
	SplittedDocumentIds getOrGenerateSplittedDocumentIds(Qname documentId, List<Qname> unitIds);

	/**
	 * Remove splitted documents from queue by ids
	 * @param ids
	 */
	void removeSplittedQueue(List<Long> ids);

	/**
	 * Get splitted documents from queue that have expired (can be be loaded) for source
	 * @param source
	 * @return
	 */
	List<QueueData> getExpiredSplittedDocumentsFromQueue(Qname source);

	/**
	 * Set erroreous for reprocessing (InError 1 -> 0)
	 */
	void markOutPipeErroneousForReattempt();

	/**
	 * Get latest out pipe activity timestamp
	 * @return
	 */
	Date getLatestTimestampFromOutPipe();

	/**
	 * Store first load dates
	 * @param firstLoadTimesOfDocumentIds documentIds mapped to timestamps
	 */
	void storeFirstLoadedTimes(Map<Qname, Long> firstLoadTimesOfDocumentIds);

	/**
	 * Get stored first load dates
	 * @param documents
	 * @return documentIds mapped to timestamps
	 */
	Map<Qname, Long> getFirstLoadTimes(List<Document> documents);

	StatelessSession getETLEntityConnection();

	Connection getETLConnection();

	// No pull APIs active at the moment and probably not preferred to create any
	//	/**
	//	 * Get info on those sources that have pull api definitions
	//	 * @return
	//	 * @
	//	 */
	//	public List<PullAPIData> getPullApis();
	//

	/**
	 * Get last read sequence id for pull api source
	 * @param source
	 * @return
	 */
	Integer getLastReadPullApiEntrySequenceFor(Qname source);

	/**
	 * Set last read sequence id for pull api source
	 * @param source
	 * @param sequence
	 */
	void setLastReadPullApiEntrySequence(Qname source, int sequence);

	/**
	 * Get annotations for each document id
	 * @param documentIds
	 * @return
	 */
	Map<Qname, List<Annotation>> getAnnotations(Set<Qname> documentIds);

	/**
	 * Get all annotated document ids
	 * @return
	 */
	Set<Qname> getAnnotatedDocumenIds();

	/**
	 * Store annotation
	 * @param annotation
	 * @param inPipeId source in pipe id
	 */
	void storeAnnotation(Annotation annotation, long inPipeId);

	/**
	 * If annotation with this id was stored, return source in pipe id
	 * @param annotationId
	 * @return
	 */
	Long getExistingAnnotationInPipeId(Qname annotationId);

	/**
	 * Store notification to queue to be processed later. If already notified, does nothing.
	 * @param notification
	 */
	void storeNotification(AnnotationNotification notification);

	/**
	 * Mark notification sent
	 * @param id
	 */
	void markNotificationSent(long id);

	/**
	 * Get unsent notifications in queue
	 * @return
	 */
	List<AnnotationNotification> getUnsentNotifications();

	/**
	 * Remove all notifications about this annotation that have not been sent yet
	 * @param annotationId
	 */
	void removeUnsentNotifications(Qname annotationId);

	/**
	 * MD5 hash an indifidual id (such as bird ring) using a secret salt
	 * @param individualId
	 * @return
	 */
	Qname secureIndividualId(Qname individualId);

	/**
	 * Removes those in pipe data entires that are no longer referenced in out pipe
	 * @param source
	 * @return number of deleted rows
	 */
	int removeUnlinkedInPipeData(Qname source);

	List<Annotation> getTopAnnotations(int limit);

	List<AnnotationNotification> getTopSentNotifications(int limit);

	List<QueueData> getTopSplittedDocumentsFromQueue(int limit);

	ResultStream<String> getAllDocumentIds();

}
