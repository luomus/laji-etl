package fi.laji.datawarehouse.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import fi.laji.datawarehouse.dao.oracle.Oracle;
import fi.laji.datawarehouse.dao.oracle.QueryLogEntity;
import fi.laji.datawarehouse.dao.vertica.VerticaDAOImpleSharedInitialization;
import fi.laji.datawarehouse.dao.vertica.VerticaDAOPrivate;
import fi.laji.datawarehouse.dao.vertica.VerticaDAOPublic;
import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.ContextDefinitions.ContextDefinition;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo.SystemId;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.model.Filter;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperty;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.HttpClientService.NotFoundException;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;

public class DAOImple implements DAO {

	private static final int MAX_DOWNLOADS_PER_DAY = 20;
	private static final int MAX_POLYGON_SEARCHES_PER_DAY = 100;

	private static final int VALID_YKJ_10KM_LENGTH = "NNN:EEE".length();
	private static final String CONTEXT_TYPE = "@type";
	private static final String CONTEXT_ID = "@id";
	private static final String CONTEXT_CONTEXT = "@context";
	private static final String XML_LANG = "xml:lang";
	private static final Qname MUNICIPALITY = new Qname("ML.municipality");
	private static final Qname OLD_MUNICIPALITY = new Qname("ML.oldMunicipality");
	private static final Qname BIOGEOGRAPHICALPROVINCE = new Qname("ML.biogeographicalProvince");
	private static final Qname COUNTRY = new Qname("ML.country");
	private static final TimeZone GMT_TIMEZONE = TimeZone.getTimeZone("GMT");
	private static final String RDF_RESOURCE = "rdf:resource";
	private static final String RDF_DESCRIPTION = "rdf:Description";
	private static final String RDF_ABOUT = "rdf:about";
	private static final String MA_EMAIL_ADDRESS = "MA.emailAddress";
	private static final String MA_FULL_NAME = "MA.fullName";
	private static final String MA_INHERITED_NAME = "MA.inheritedName";
	private static final String MA_PREFERRED_NAME = "MA.preferredName";
	private static final String MA_INSECT_DATABASE_LOGIN_NAME = "MA.insectDatabaseLoginName";
	private static final String MA_LINTUVAARA_LOGIN_NAME = "MA.lintuvaaraLoginName";
	private static final String MA_FIELDJOURNAL_LOGIN_NAME = "MA.fieldjournalLoginName";
	private static final String MA_HATIKKA_LOGIN_NAME = "MA.hatikkaLoginName";
	private static final String MA_INATURALIST_LOGIN_NAME = "MA.inaturalistLoginName";
	private static final String MA_OMARIISTA_LOGIN_NAME = "MA.omariistaLoginName";
	private static final String MA_KASTIKKA_USER_NAME = "MA.kastikkaUserName";
	private static final String PEST_EARLY_WARNING = "pest-early-warning-units";
	private static final String INVASIVE_EARLY_WARNING = "invasive-early-warning-units";

	private static final Object LOCK = new Object();

	private static final Object ELASTIC_LOCK = new Object();
	private static final Object TRIPLESTORE_LOCK = new Object();
	private static final Object TAXON_LOCK = new Object();
	private static final Object LOG_LOCK = new Object();
	private static final Object LOG_FILE_LOCK = new Object();

	protected final Oracle oracle;
	private final ETLDAO etl;
	private final TaxonomyDAOImple taxonomyDAO;
	private final Config config;
	private final ErrorReporter errorReporter;
	private final Class<?> createdBy;
	private final long createdAt;
	private final String verticaSchema;
	private final QueryLogStorer queryLogStorer;
	private HttpClientService triplestoreClient;
	private HttpClientService storingTriplestoreClient;
	private HttpClientService elasticClient;
	private HttpClientService apiClient;
	private HttpClientService lajistoreClient;
	private boolean closed = false;
	private Map<String, Qname> personLookup;
	private Map<String, Qname> taxonLookup;
	private final ThreadStatuses threadStatuses;
	private ExecutorService threadPool;

	public class QueryLogStorer implements Runnable {

		private volatile boolean stop = false;

		public void stop() {
			this.stop = true;
		}

		@Override
		public void run() {
			System.out.println(QueryLogStorer.class.getName() + " is running");
			while (!stop) {
				try {
					tempStoreQueryLog();
					sleep();
				} catch (Throwable e) {}
			}
			System.out.println(QueryLogStorer.class.getName() + " stopped!");
		}

		private void sleep() {
			try { Thread.sleep(1000*10); } catch (InterruptedException e) {}
		}
	}

	public DAOImple(Config config, String verticaSchema, ErrorReporter errorReporter, ThreadStatuses threadStatuses, Class<?> createdBy) {
		this.config = config;
		this.oracle = new Oracle(config);
		this.etl = new ETLDAOImple(config, oracle);
		this.verticaSchema = verticaSchema; // Not read from config so that tests may override the schema
		this.createdBy = createdBy;
		this.createdAt = DateUtils.getCurrentEpoch();
		this.taxonomyDAO = new TaxonomyDAOImple(config, this, this.createdBy);
		this.errorReporter = errorReporter;
		this.threadStatuses = threadStatuses;
		this.queryLogStorer = initQueryLogger();
		this.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created! (Created by " + this.createdBy.getSimpleName() + " at " + this.createdAt + ")" );
	}

	protected QueryLogStorer initQueryLogger() {
		QueryLogStorer logger = new QueryLogStorer();
		Thread t = new Thread(logger);
		t.setName("QueryLogger Thread");
		t.start();
		return logger;
	}

	@Override
	public void close() {
		if (this.closed) return;
		synchronized (LOCK) {
			if (this.closed) return;
			try { logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "DAO closing!"); } catch (Exception e) {}
			if (queryLogStorer != null) {
				try {
					queryLogStorer.stop();
					queryLogStorer.notify();
				} catch (Exception e) {}
			}
			try { tempStoreQueryLog(); } catch (Exception e) {}
			try { if (oracle != null) oracle.close();  } catch (Exception e) {}
			try { if (publicVerticaDAO != null) publicVerticaDAO.close(); } catch (Exception e) {}
			try { if (privateVerticaDAO != null) privateVerticaDAO.close(); } catch (Exception e) {}
			try { if (sharedVerticaInitialization != null) sharedVerticaInitialization.close(); } catch (Exception e) {}
			try { if (triplestoreClient != null) triplestoreClient.close(); } catch (Exception e) {}
			try { if (storingTriplestoreClient != null) storingTriplestoreClient.close(); } catch (Exception e) {}
			try { if (elasticClient != null) elasticClient.close(); } catch (Exception e) {}
			try { if (apiClient != null) apiClient.close(); } catch (Exception e) {}
			try { if (lajistoreClient != null) lajistoreClient.close(); } catch (Exception e) {}
			try { if (threadPool != null) threadPool.shutdown(); } catch (Exception e) {}
			closed = true;
		}
	}

	private final SingleObjectCache<Map<String, CollectionMetadata>> collectionsCache = new SingleObjectCache<>(new CollectionMetadataLoader(), 1, TimeUnit.HOURS);

	private class CollectionMetadataLoader implements SingleObjectCache.CacheLoader<Map<String, CollectionMetadata>> {

		@Override
		public Map<String, CollectionMetadata> load() {
			try {
				return tryToLoad();
			} catch (Exception e) {
				throw exceptionAndReport("Loading collections", e);
			}
		}

		private Map<String, CollectionMetadata> tryToLoad() throws ClientProtocolException, IOException, URISyntaxException {
			Map<String, CollectionMetadata> collections = new HashMap<>();
			JSONObject response = getCollectionApiResponse();
			for (JSONObject collection : response.getArray("results").iterateAsObject()) {
				CollectionMetadata collectionMetadata = parseCollection(collection);
				collections.put(collectionMetadata.getQname().toURI(), collectionMetadata);
			}
			if (collections.isEmpty()) throw new IllegalStateException("No collections in result from api!");
			fillParentsChildren(collections);
			return collections;
		}

		private JSONObject getCollectionApiResponse() throws ClientProtocolException, IOException, URISyntaxException {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/collections")
					.addParameter("access_token", getApiKey())
					.addParameter("pageSize", 10000)
					.addParameter("lang", "multi");
			JSONObject response = client.contentAsJson(new HttpGet(uri.getURI()));
			return response;
		}

		private CollectionMetadata parseCollection(JSONObject json) {
			Qname id = new Qname(json.getString("id"));
			LocalizedText name = new LocalizedText();
			name.set("fi", json.getObject("longName").getString("fi"));
			name.set("sv", json.getObject("longName").getString("sv"));
			name.set("en", json.getObject("longName").getString("en"));
			fillMissingLocales(name);
			String parentId = json.getString("isPartOf");
			Qname parentQname = given(parentId) ? new Qname(parentId) : null;
			CollectionMetadata c = new CollectionMetadata(id, name, parentQname);
			if (json.hasKey("abbreviation")) {
				String abbreviation = json.getString("abbreviation");
				if (given(abbreviation)) c.setAbbreviation(abbreviation);
			}
			if (json.hasKey("owner")) {
				c.setOwnerOrganization(new Qname(json.getString("owner")));
			}
			parseIntellectualRightsAndContact(json, id, c);
			parseCollectionQuality(json, c);

			if (json.hasKey("dataQuarantinePeriod")) {
				c.setDataQuarantinePeriod(json.getDouble("dataQuarantinePeriod"));
			}

			if (json.hasKey("secureLevel")) {
				c.setSecureLevel(new Qname(json.getString("secureLevel")));
			}

			try {
				if (json.hasKey("collectionSize")) {
					c.setSize(Integer.valueOf(json.getString("collectionSize")));
				}
			} catch (Exception e) {}

			try {
				if (json.hasKey("typesSize")) {
					c.setTypeSpecimenSize(Integer.valueOf(json.getString("typesSize")));
				}
			} catch (Exception e) {}

			try {
				if (json.hasKey("allowedForDwStatistics")) {
					if (json.getBoolean("allowedForDwStatistics")) {
						c.setStatisticsAllowed(true);
					}
				}
			} catch (Exception e) {}

			return c;
		}

		private void parseIntellectualRightsAndContact(JSONObject json, Qname id, CollectionMetadata c) {
			if (json.hasKey("intellectualRights")) {
				Qname intellectualRights = new Qname(json.getString("intellectualRights"));
				if (!intellectualRights.isSet()) {
					errorReporter.report("Collection is missing license " + id.toURI());
				} else {
					c.setIntellectualRights(intellectualRights);
					c.setIntellectualRightsDescription(getLabels(intellectualRights));
				}
				String contactEmail = json.getString("contactEmail");
				if (given(contactEmail)) c.setContactEmail(contactEmail);
			}
		}

		private void parseCollectionQuality(JSONObject json, CollectionMetadata c) {
			if (json.hasKey("collectionQuality")) {
				c.setCollectionQuality(new Qname(json.getString("collectionQuality")));
			} else if (json.hasKey("dataQuality")) {
				try {
					// This is for backwards compatibility for collections that do not yet have collectionQuality // TODO remove when all collections have collectionQuality
					int starRating = Integer.valueOf(json.getString("dataQuality").replace("MY.dataQuality", ""));
					if (starRating <= 2) {
						c.setCollectionQuality(new Qname("MY.collectionQualityEnum1"));
					} else if (starRating <= 4) {
						c.setCollectionQuality(new Qname("MY.collectionQualityEnum2"));
					} else {
						c.setCollectionQuality(new Qname("MY.collectionQualityEnum3"));
					}
				} catch (NumberFormatException e) {}
			}

			if (c.getCollectionQuality() == null) {
				c.setCollectionQuality(new Qname("MY.collectionQualityEnum1"));
			}
		}

		private void fillParentsChildren(Map<String, CollectionMetadata> collections) {
			for (CollectionMetadata collection : collections.values()) {
				if (collection.getParentQname() != null) {
					CollectionMetadata parent = collections.get(collection.getParentQname().toURI());
					if (parent == null) {
						String errorMessage = "Parent set but not loaded. Child: " + collection.getQname() + ", parent: " + collection.getParentQname();
						errorReporter.report(errorMessage);
						logError(Const.LAJI_ETL_QNAME, DAOImple.class, collection.getQname().toString(), new IllegalStateException(errorMessage));
					} else {
						collection.setParent(parent);
						parent.getChildren().add(collection);
					}
				}
			}
		}
	}

	@Override
	public Map<String, CollectionMetadata> getCollections() {
		return collectionsCache.get();
	}

	private Set<Qname> statisticsAllowed = null;

	@Override
	public Set<Qname> getStatisticsAllowedCollections() {
		if (statisticsAllowed == null) {
			statisticsAllowed = initStatisticsAllowedCollections();
		}
		return statisticsAllowed;
	}

	private Set<Qname> initStatisticsAllowedCollections() {
		Set<Qname> ids = Utils.set(
				new Qname("HR.61"), new Qname("HR.175"), new Qname("HR.39"), new Qname("HR.2691"), new Qname("HR.157"),
				new Qname("HR.3431"), new Qname("HR.3911"), new Qname("HR.4511")); // TODO remove when all these are checked to be in metadata and the logic bellow works
		ids.addAll(Const.WATERBIRD_COLLECTION);
		try {
			getCollections().values().stream().filter(c->c.isStatisticsAllowed()).map(c->c.getQname()).forEach(ids::add);
		} catch (Exception e) {
			getErrorReporter().report("Init statistics allowed collections", e);
		}
		return Collections.unmodifiableSet(ids);
	}

	private final SingleObjectCache<Map<Qname, Area>> areaCache = new SingleObjectCache<>(new AreaDescLoader(), 12, TimeUnit.HOURS);

	private class AreaDescLoader implements SingleObjectCache.CacheLoader<Map<Qname, Area>> {

		@Override
		public Map<Qname, Area> load() {
			try {
				return tryToLoad();
			} catch (Exception e) {
				throw exceptionAndReport("Loading areas", e);
			}
		}

		private Map<Qname, Area> tryToLoad() throws IOException, ClientProtocolException, URISyntaxException {
			Map<Qname, Area> areas = new HashMap<>();
			Node response = getAreaTriplestoreResponse();
			for (Node n : response.getChildNodes(RDF_DESCRIPTION)) {
				Area area = parseArea(n);
				if (area != null) {
					areas.put(area.getQname(), area);
				}
			}
			return areas;
		}

		private Area parseArea(Node n) {
			try {
				Qname id = Qname.fromURI(n.getAttribute(RDF_ABOUT));
				LocalizedText name = getLocalizedText("ML.name", n);
				Qname type = Qname.fromURI(getValue("ML.areaType", n));
				Qname isPartOf = getIsPartOf(n);
				Area area = new Area(id, name, type);
				area.setPartOf(isPartOf);
				if (type.equals(BIOGEOGRAPHICALPROVINCE)) {
					area.setAbbreviation(getBioprovinceCode(n));
				} else if (type.equals(COUNTRY)) {
					area.setAbbreviation(getCountryCode(n));
				}
				return area;
			} catch (Exception e) {
				errorReporter.report("Invalid data for area " + n.getAttribute(RDF_ABOUT), e);
				return null;
			}
		}

		private Node getAreaTriplestoreResponse() throws IOException, ClientProtocolException, URISyntaxException {
			HttpClientService client = getTriplestoreClient();
			URIBuilder uri = new URIBuilder(getTriplestoreUrl() + "/search");
			uri.addParameter("predicate", "rdf:type");
			uri.addParameter("objectresource", "ML.area");
			uri.addParameter("limit", Integer.MAX_VALUE);
			uri.addParameter("format", "rdfxml");
			Node response = client.contentAsDocument(new HttpGet(uri.getURI())).getRootNode();
			return response;
		}

		private Qname getIsPartOf(Node n) {
			String s = getValue("ML.isPartOf", n);
			if (given(s)) return Qname.fromURI(s);
			return null;
		}

		private String getBioprovinceCode(Node n) {
			for (Node codeNode : n.getChildNodes("ML.provinceCodeAlpha")) {
				if (codeNode.hasAttribute(XML_LANG) && codeNode.getAttribute(XML_LANG).equals("fi")) {
					return codeNode.getContents();
				}
			}
			return null;
		}

		private String getCountryCode(Node n) {
			for (Node codeNode : n.getChildNodes("ML.countryCodeISOalpha2")) {
				return codeNode.getContents();
			}
			return null;
		}
	}

	@Override
	public Map<Qname, Area> getAreas() {
		return areaCache.get();
	}

	@Override
	public Map<Qname, Area> getAreasForceReload() {
		return areaCache.getForceReload();
	}

	public LocalizedText getLocalizedText(String field, Node n) {
		LocalizedText text = new LocalizedText();
		for (Node name : n.getChildNodes(field)) {
			String contents = name.getContents();
			if (!given(contents)) continue;
			if (name.hasAttribute(XML_LANG)) {
				String locale = name.getAttribute(XML_LANG);
				text.set(locale, contents);
			} else {
				text.set("", contents);
			}
		}
		fillMissingLocales(text);
		return text;
	}

	private void fillMissingLocales(LocalizedText text) {
		fillMissingLocales("fi", text, "en", "sv", "");
		fillMissingLocales("sv", text, "fi", "en", "");
		fillMissingLocales("en", text, "fi", "sv", "");
		fillMissingLocales("", text, "fi", "en", "sv");
	}

	private void fillMissingLocales(String locale, LocalizedText text, String ... alternativeLocales) {
		if (text.hasTextForLocale(locale)) return;
		for (String alternative : alternativeLocales) {
			if (text.hasTextForLocale(alternative)) {
				text.set(locale, text.forLocale(alternative));
				return;
			}
		}
	}

	private final SingleObjectCache<Map<String, Source>> sourceCache = new SingleObjectCache<>(new SourceLoader(), 1, TimeUnit.HOURS);

	private class SourceLoader implements SingleObjectCache.CacheLoader<Map<String, Source>> {

		@Override
		public Map<String, Source> load() {
			try {
				return tryToLoad();
			} catch (Exception e) {
				throw exceptionAndReport("Loading information systems", e);
			}
		}

		private Map<String, Source> tryToLoad() throws IOException, ClientProtocolException, URISyntaxException {
			Map<String, Source> sources = createEmptyOrderedByIdMap();
			Node response = getSourceTriplestoreResponse();
			for (Node n : response.getChildNodes(RDF_DESCRIPTION)) {
				Qname id = Qname.fromURI(n.getAttribute(RDF_ABOUT));
				Source source = parseSource(n, id);
				sources.put(id.toURI(), source);
			}
			return sources;
		}

		private Source parseSource(Node n, Qname id) {
			String name = getValue("KE.name", n);
			boolean warehouseSource = getValue("KE.isWarehouseSource", n).equals("true");
			boolean allowedToQueryPrivateWarehouse = getValue("KE.isAllowedToQueryPrivateWarehouse", n).equals("true");
			boolean allowedToUseDatawarehouseEditorOrObserverIdIsNot = getValue("KE.allowedToUseDatawarehouseEditorOrObserverIdIsNot", n).equals("true");
			Source source = new Source(id, name, warehouseSource, allowedToQueryPrivateWarehouse, allowedToUseDatawarehouseEditorOrObserverIdIsNot);
			parseOverridingIdsToSource(n, source);
			return source;
		}

		private Node getSourceTriplestoreResponse() throws IOException, ClientProtocolException, URISyntaxException {
			HttpClientService client = getStoringTriplestoreClient(); // information systems from triplestore staging for dev/staging
			URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl() + "/search");
			uri.addParameter("predicate", "rdf:type");
			uri.addParameter("objectresource", "KE.informationSystem");
			uri.addParameter("limit", 10000);
			uri.addParameter("format", "rdfxml");
			Node response = client.contentAsDocument(new HttpGet(uri.getURI())).getRootNode();
			return response;
		}

		private TreeMap<String, Source> createEmptyOrderedByIdMap() {
			return new TreeMap<>(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return Qname.fromURI(o1).compareTo(Qname.fromURI(o2));
				}
			});
		}

		private void parseOverridingIdsToSource(Node dataNode, Source source) {
			for (Node n : dataNode.getChildNodes("KE.allowedDatawarehouseOverridingSourceId")) {
				if (n.hasAttribute(RDF_RESOURCE)) {
					source.addValidOverridingSource(Qname.fromURI(n.getAttribute(RDF_RESOURCE)));
				}
			}
		}
	}

	private final SingleObjectCache<Map<String, String>> referenceDescriptionsCache = new SingleObjectCache<>(new ReferenceDescLoader(), 1, TimeUnit.HOURS);

	private class ReferenceDescLoader implements SingleObjectCache.CacheLoader<Map<String, String>> {
		@Override
		public Map<String, String> load() {
			try {
				Map<String, String> descriptions = new HashMap<>();
				Node response = getTriplestorePublicationResponse();
				for (Node n : response.getChildNodes(RDF_DESCRIPTION)) {
					String id = Qname.fromURI(n.getAttribute(RDF_ABOUT)).toURI();
					String name = getValue("dc:bibliographicCitation", n);
					descriptions.put(id, name);
				}
				return descriptions;
			} catch (Exception e) {
				throw exceptionAndReport("Loading references", e);
			}
		}

		private Node getTriplestorePublicationResponse() throws IOException, ClientProtocolException, URISyntaxException {
			HttpClientService client = getTriplestoreClient();
			URIBuilder uri = new URIBuilder(getTriplestoreUrl() + "/search");
			uri.addParameter("predicate", "rdf:type");
			uri.addParameter("objectresource", "MP.publication");
			uri.addParameter("limit", 10000);
			uri.addParameter("format", "rdfxml");
			Node response = client.contentAsDocument(new HttpGet(uri.getURI())).getRootNode();
			return response;
		}
	}

	@Override
	public Map<String, String> getReferenceDescriptions() {
		return referenceDescriptionsCache.get();
	}

	@Override
	public Map<String, Source> getSources() {
		return sourceCache.get();
	}

	private HttpClientService getTriplestoreClient() {
		if (triplestoreClient == null) {
			synchronized (TRIPLESTORE_LOCK) {
				if (triplestoreClient == null) {
					String address = getTriplestoreUrl();
					String username = config.get("TriplestoreUsername");
					String password = config.get("TriplestorePassword");
					try {
						triplestoreClient = new HttpClientService(address, username, password);
					} catch (Exception e) {
						throw new ETLException(e);
					}
				}
			}
		}
		return triplestoreClient;
	}

	/**
	 * For production this is the same as normal client. For dev/staging get a client that connects to staging triplestore.
	 * @return
	 * @throws URISyntaxException
	 */
	private HttpClientService getStoringTriplestoreClient() {
		if (config.productionMode()) return getTriplestoreClient();

		if (storingTriplestoreClient == null) {
			synchronized (TRIPLESTORE_LOCK) {
				if (storingTriplestoreClient == null) {
					String address = getStoringTriplestoreUrl();
					String username = config.get("Storing_TriplestoreUsername");
					String password = config.get("Storing_TriplestorePassword");
					try {
						storingTriplestoreClient = new HttpClientService(address, username, password);
					} catch (URISyntaxException e) {
						throw new ETLException(e);
					}
				}
			}
		}
		return storingTriplestoreClient;
	}

	private HttpClientService getElasticClient() throws URISyntaxException {
		if (elasticClient == null) {
			synchronized (ELASTIC_LOCK) {
				if (elasticClient == null) {
					String address = getElasticUrl();
					String username = config.get("MunicipalityElasticCluster_Username");
					String password = config.get("MunicipalityElasticCluster_Password");
					elasticClient = new HttpClientService(address, username, password);
				}
			}
		}
		return elasticClient;
	}

	private HttpClientService getLajistoreClient() {
		if (lajistoreClient == null) {
			synchronized (LOCK) {
				if (lajistoreClient == null) {
					try {
						lajistoreClient = new HttpClientService();
					} catch (Exception e) {
						throw new ETLException(e);
					}
				}
			}
		}
		return lajistoreClient;
	}

	private String getLajistoreUrl() {
		return config.get("LajistoreURL");
	}

	private HttpClientService getApiClient() {
		if (apiClient == null) {
			synchronized (LOCK) {
				if (apiClient == null) {
					apiClient = new HttpClientService();
				}
			}
		}
		return apiClient;
	}

	private String getTriplestoreUrl() {
		return config.get("TriplestoreURL");
	}

	/**
	 * Get a client that modifies triplestore state. For production this is the same as getTriplestoreUrl(), for staging/dev this is test triplestore url.
	 * @return
	 */
	private String getStoringTriplestoreUrl() {
		if (config.productionMode()) return getTriplestoreUrl();
		return config.get("Storing_TriplestoreURL");
	}

	private String getElasticUrl() {
		return config.get("MunicipalityElasticCluster_URL");
	}

	@Override
	public Set<Qname> getFinnishMunicipalities(Coordinates wgs84Coordinates) {
		return getCoordinateMatches("municipality", wgs84Coordinates);
	}

	@Override
	public Set<Qname> getNaturaAreas(Coordinates wgs84Coordinates) {
		return getCoordinateMatches("natura", wgs84Coordinates);
	}

	@Override
	public Set<Qname> getBiogeographicalProvinces(Coordinates wgs84Coordinates) {
		return getCoordinateMatches("biogeographicalProvince", wgs84Coordinates);
	}

	private Set<Qname> getCoordinateMatches(String elasticResourceName, Coordinates wgs84Coordinates) {
		Set<Qname> matches = cachedGetByCoordinatesSearches.get(new GetByCoordinatesRequest(wgs84Coordinates)).get(elasticResourceName);
		if (matches == null) return Collections.emptySet();
		return matches;
	}

	@Override
	public Set<Qname> getFinnishMunicipalities(Geo wgs84geo) {
		return getGeoMatches("municipality", wgs84geo);
	}

	@Override
	public Set<Qname> getBiogeographicalProvinces(Geo wgs84geo) {
		return getGeoMatches("biogeographicalProvince", wgs84geo);
	}

	@Override
	public Set<Qname> getNaturaAreas(Geo wgs84geo) {
		return getGeoMatches("natura", wgs84geo);
	}

	private Set<Qname> getGeoMatches(String elasticResourceName, Geo wgs84geo) {
		Set<Qname> allMatches = new HashSet<>();
		for (Feature f : wgs84geo.getFeatures()) {
			if (f instanceof Polygon) {
				addPolygonMatches(elasticResourceName, allMatches, (Polygon) f);
				continue;
			}
			if (f instanceof Line) {
				addLineMatches(elasticResourceName, allMatches, (Line) f);
				continue;
			}
			addGeoMatches(elasticResourceName, f, allMatches);
		}
		return allMatches;
	}

	private void addLineMatches(String elasticResourceName, Set<Qname> allMatches, Line line) {
		if (tooComplex(line)) {
			addGeoMatches(elasticResourceName, line.convexHullAndSimplify(Type.WGS84), allMatches);
			return;
		}
		addGeoMatches(elasticResourceName, line.simplify(Type.WGS84), allMatches);
	}

	private void addPolygonMatches(String elasticResourceName, Set<Qname> allMatches, Polygon polygon) {
		addGeoMatches(elasticResourceName, polygon.convexHullAndSimplify(Type.WGS84), allMatches);
	}

	private void addGeoMatches(String elasticResourceName, Feature f, Set<Qname> allMatches) {
		Set<Qname> matches = cachedGetByCoordinatesSearches.get(new GetByCoordinatesRequest(f)).get(elasticResourceName);
		if (matches != null) {
			allMatches.addAll(matches);
		}
	}

	private boolean tooComplex(Line line) {
		return line.size() > maxAllowedSize(line);
	}

	private int maxAllowedSize(Line line) {
		Coordinates boundingBox = getBoundingBox(line);
		if (boundingBox.getAccuracyInMeters() <= 1000) return 50;
		if (boundingBox.getAccuracyInMeters() <= 10000) return 25;
		return 8;
	}

	private Coordinates getBoundingBox(Feature f) {
		try {
			Coordinates c = new Coordinates(f.getLatMin(), f.getLatMax(), f.getLonMin(), f.getLonMax(), Type.WGS84);
			c.setAccuracyInMeters(c.calculateBoundingBoxAccuracy());
			return c;
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	private class GetByCoordinatesRequest {
		private final String wgs84Coordinates;
		private final String type;
		private final String toString;
		public GetByCoordinatesRequest(Coordinates wgs84Coordinates) {
			if (wgs84Coordinates.getLatMin().equals(wgs84Coordinates.getLatMax()) && wgs84Coordinates.getLonMin().equals(wgs84Coordinates.getLonMax())) {
				this.type = "point";
				StringBuilder b = new StringBuilder("[").append(wgs84Coordinates.getLonMin()).append(",").append(wgs84Coordinates.getLatMin()).append("]");
				this.wgs84Coordinates = b.toString();
			} else {
				this.type = "envelope";
				StringBuilder b = new StringBuilder("[[");
				b.append(wgs84Coordinates.getLonMin()).append(",").append(wgs84Coordinates.getLatMin()).append("],[");
				b.append(wgs84Coordinates.getLonMax()).append(",").append(wgs84Coordinates.getLatMax()).append("]]");
				this.wgs84Coordinates = b.toString();
			}
			this.toString = this.wgs84Coordinates + this.type;
		}
		public GetByCoordinatesRequest(Feature wgs84Feature) {
			if (wgs84Feature instanceof Polygon) {
				this.type = "polygon";
				Iterator<Point> i = ((Polygon)wgs84Feature).getPoints().iterator();
				StringBuilder b = new StringBuilder("[[");
				while (i.hasNext()) {
					Point p = i.next();
					b.append("[").append(p.getLon()).append(",").append(p.getLat()).append("]");
					if (i.hasNext()) b.append(",");
				}
				b.append("]]");
				this.wgs84Coordinates = b.toString();
			} else if (wgs84Feature instanceof Line) {
				this.type = "linestring";
				Iterator<Point> i = wgs84Feature.iterator();
				StringBuilder b = new StringBuilder("[");
				while (i.hasNext()) {
					Point p = i.next();
					b.append("[").append(p.getLon()).append(",").append(p.getLat()).append("]");
					if (i.hasNext()) b.append(",");
				}
				b.append("]");
				this.wgs84Coordinates = b.toString();
			} else if (wgs84Feature instanceof Point) {
				this.type = "point";
				Point p = (Point)wgs84Feature;
				StringBuilder b = new StringBuilder("[").append(p.getLon()).append(",").append(p.getLat()).append("]");
				this.wgs84Coordinates = b.toString();
			} else {
				throw new UnsupportedOperationException("Not implemented for " + wgs84Feature.getClass().getName());
			}
			this.toString = this.wgs84Coordinates + this.type;
		}
		@Override
		public int hashCode() {
			return toString.hashCode();
		}
		@Override
		public boolean equals(Object o) {
			if (o instanceof GetByCoordinatesRequest) {
				return ((GetByCoordinatesRequest) o).toString.equals(this.toString);
			}
			throw new UnsupportedOperationException("Can only compare to instance of " + GetByCoordinatesRequest.class);
		}
		@Override
		public String toString() {
			return toString;
		}
	}

	private final Cached<GetByCoordinatesRequest, Map<String, Set<Qname>>> cachedGetByCoordinatesSearches = new Cached<>(new CacheLoader<GetByCoordinatesRequest, Map<String, Set<Qname>>>() {

		@Override
		public Map<String, Set<Qname>> load(GetByCoordinatesRequest key) {
			try {
				return tryToLoad(key);
			} catch (Exception e) {
				throw new ETLException(e);
			}
		}

		private Map<String, Set<Qname>> tryToLoad(GetByCoordinatesRequest key) throws Exception {
			Map<String, Set<Qname>> matches = new HashMap<>();
			JSONObject response = getByCoordinatesResponse(key.wgs84Coordinates, key.type);
			List<JSONObject> hits = response.getObject("hits").getArray("hits").iterateAsObject();
			for (JSONObject hit : hits) {
				String type = hit.getString("_type");
				if (!matches.containsKey(type)) {
					matches.put(type, new HashSet<Qname>());
				}
				matches.get(type).add(parseQname(hit));
			}
			return matches;
		}

		private JSONObject getByCoordinatesResponse(String wgs84Coordinates, String type) throws Exception {
			HttpPost request = new HttpPost(getElasticUrl() + "/location/_search");
			StringBuilder b = new StringBuilder();
			b.append("{\"query\": { \"geo_shape\": { \"border\": { \"shape\": { \"type\": \"").append(type).append("\", \"coordinates\": ").append(wgs84Coordinates).append("}}}}");
			b.append(",");
			b.append("\"_source\":  [\"qname\"], \"size\": 2000 }");
			HttpEntity entity = new ByteArrayEntity(b.toString().getBytes("UTF-8"));
			request.setEntity(entity);
			JSONObject response = getElasticClient().contentAsJson(request);
			return response;
		}

	}, 1, TimeUnit.DAYS, 20000);

	private final SingleObjectCache<Map<Qname, Coordinates>> cachedFinnishMunicipalityBoundingBoxes = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<Map<Qname, Coordinates>>() {

		@Override
		public Map<Qname, Coordinates> load() {
			try {
				return tryToLoad();
			} catch (Exception e) {
				throw exceptionAndReport("Loading municipality bounding boxes", e);
			}
		}

		private Map<Qname, Coordinates> tryToLoad() throws ClientProtocolException, IOException, URISyntaxException {
			JSONObject response = getBoundingBoxes();
			List<JSONObject> hits = response.getObject("hits").getArray("hits").iterateAsObject();
			if (hits.isEmpty()) throw new IllegalStateException("Get finnish municipality bounding boxes failed: empty response!");
			return parseResponse(hits);
		}

		private JSONObject getBoundingBoxes() throws ClientProtocolException, IOException, URISyntaxException {
			HttpPost request = new HttpPost(getElasticUrl() + "/location/municipality/_search");
			String data = "{ \"_source\":  [\"boundingBox\",\"qname\"], \"size\": 800 }";
			HttpEntity entity = new ByteArrayEntity(data.toString().getBytes("UTF-8"));
			request.setEntity(entity);
			JSONObject response = getElasticClient().contentAsJson(request);
			return response;
		}

		private Map<Qname, Coordinates> parseResponse(List<JSONObject> hits) {
			Map<Qname, Coordinates> map = new HashMap<>();
			for (JSONObject hit : hits) {
				addHitToMap(map, hit);
			}
			return map;
		}

		private void addHitToMap(Map<Qname, Coordinates> map, JSONObject hit) {
			Qname municipalityQname = parseQname(hit);
			Coordinates coordinates = parseCoordinates(hit);
			map.put(municipalityQname, coordinates);
		}

		private Coordinates parseCoordinates(JSONObject hit) {
			List<JSONArray> coordinateArray = hit.getObject("_source").getObject("boundingBox").getArray("coordinates").iterateAsArray();
			List<Double> min = coordinateArray.get(0).iterateAsDouble();
			List<Double> max = coordinateArray.get(1).iterateAsDouble();
			double latMin = min.get(1);
			double lonMin = min.get(0);
			double latMax = max.get(1);
			double lonMax = max.get(0);
			try {
				return new Coordinates(latMin, latMax, lonMin, lonMax, Type.WGS84);
			} catch (DataValidationException e) {
				throw new ETLException("Coordinates from elastic: " + Utils.debugS(latMin, latMax, lonMin, lonMax));
			}
		}

	}, 1, TimeUnit.DAYS);

	private Qname parseQname(JSONObject hit) {
		return new Qname(hit.getObject("_source").getString("qname"));
	}

	@Override
	public Coordinates getCoordinatesByFinnishMunicipality(Qname municipality) {
		return cachedFinnishMunicipalityBoundingBoxes.get().get(municipality);
	}

	private final Cached<Qname, Map<String, Set<Qname>>> cachedAreaNameSearchMap = new Cached<>(new AreaNameSearchMapLoader(), 12, TimeUnit.HOURS, 1000);

	private class AreaNameSearchMapLoader implements CacheLoader<Qname, Map<String, Set<Qname>>> {
		@Override
		public Map<String, Set<Qname>> load(Qname areaType) {
			try {
				return tryToLoad(areaType);
			} catch (Exception e) {
				throw exceptionAndReport("Loading area type " + areaType, e);
			}
		}

		private Map<String, Set<Qname>> tryToLoad(Qname areaType) throws ClientProtocolException, IOException {
			Document response = getAreaTriplestoreResponse(areaType);
			Map<String, Set<Qname>> searchMap = new HashMap<>();
			for (Node area : response.getRootNode().getChildNodes(RDF_DESCRIPTION)) {
				Qname areaCode = getAreaCode(area);
				for (Node areaDataNode : area.getChildNodes()) {
					if (areaDataNode.hasContents()) {
						String searchWord = areaDataNode.getContents().toLowerCase();
						add(searchMap, searchWord, areaCode);
					}
				}
				add(searchMap, areaCode.toString().toLowerCase(), areaCode);
				add(searchMap, areaCode.toURI().toLowerCase(), areaCode);
			}
			return searchMap;
		}

		private void add(Map<String, Set<Qname>> searchMap, String searchWord, Qname areaCode) {
			if (!searchMap.containsKey(searchWord)) {
				searchMap.put(searchWord, new HashSet<Qname>());
			}
			searchMap.get(searchWord).add(areaCode);
		}

	}

	private Qname getAreaCode(Node area) {
		return Qname.fromURI(area.getAttribute(RDF_ABOUT));
	}

	private Document getAreaTriplestoreResponse(Qname areaType) throws IOException, ClientProtocolException {
		URIBuilder uri = new URIBuilder(getTriplestoreUrl()+"/search")
				.addParameter("predicate", "ML.areaType")
				.addParameter("objectresource", areaType.toString())
				.addParameter("limit", 20000)
				.addParameter("format", "rdfxml");
		HttpGet request = new HttpGet(uri.toString());
		Document response = getTriplestoreClient().contentAsDocument(request);
		return response;
	}

	@Override
	public List<Area> resolveCountiesByName(String reportedCountry) {
		return resolveAreaByName(COUNTRY, reportedCountry);
	}

	@Override
	public List<Area> resolveBiogeographicalProvincesByName(String reportedProvince) {
		return resolveAreaByName(BIOGEOGRAPHICALPROVINCE, reportedProvince);
	}

	@Override
	public List<Area> resolveMunicipalitiesByName(String reportedMunicipality) {
		return resolveAreaByName(MUNICIPALITY, reportedMunicipality);
	}

	@Override
	public List<Area> resolveOldMunicipalitiesByName(String reportedMunicipality) {
		return resolveAreaByName(OLD_MUNICIPALITY, reportedMunicipality);
	}

	private List<Area> resolveAreaByName(Qname areaType, String name) {
		if (!given(name)) return Collections.emptyList();
		Set<Qname> ids = cachedAreaNameSearchMap.get(areaType).get(name.toLowerCase());
		if (!given(ids)) return Collections.emptyList();
		List<Area> list = new ArrayList<>(ids.size());
		for (Qname id : ids) {
			list.add(getArea(id));
		}
		return list;
	}

	private boolean given(Set<Qname> ids) {
		return ids != null && !ids.isEmpty();
	}

	private static class AreaMissingException extends ETLException {
		private static final long serialVersionUID = 1L;
		public AreaMissingException(String message) {
			super(message);
		}
	}

	private Area getArea(Qname id) {
		Area area = getAreas().get(id);
		if (area != null) return area;
		throw new AreaMissingException("Missing area information for " + id);
	}

	private boolean given(String s) {
		return s != null && s.length() > 0 && !s.equalsIgnoreCase("null") && !s.equalsIgnoreCase("null null");
	}

	private VerticaDAOImpleSharedInitialization sharedVerticaInitialization = null;
	private VerticaDAO publicVerticaDAO =  null;
	private VerticaDAO privateVerticaDAO =  null;

	public VerticaDAOImpleSharedInitialization getSharedVerticaInitialization() {
		if (sharedVerticaInitialization == null) {
			synchronized (LOCK) {
				if (sharedVerticaInitialization == null) {
					sharedVerticaInitialization = new VerticaDAOImpleSharedInitialization(config, this, verticaSchema, threadStatuses);
				}
			}
		}
		return sharedVerticaInitialization;
	}

	@Override
	public VerticaDAO getPublicVerticaDAO() {
		if (publicVerticaDAO == null) {
			synchronized (LOCK) {
				if (publicVerticaDAO == null) {
					publicVerticaDAO = new VerticaDAOPublic(getSharedVerticaInitialization());
				}
			}
		}
		return publicVerticaDAO;
	}

	@Override
	public VerticaDAO getPrivateVerticaDAO() {
		if (privateVerticaDAO == null) {
			synchronized (LOCK) {
				if (privateVerticaDAO == null) {
					privateVerticaDAO = new VerticaDAOPrivate(getSharedVerticaInitialization());
				}
			}
		}
		return privateVerticaDAO;
	}

	@Override
	public VerticaDimensionsDAO getVerticaDimensionsDAO() {
		return getSharedVerticaInitialization().getDimensions();
	}

	@Override
	public void logError(Qname source, Class<?> phase, String identifier, Throwable exception) {
		String stackTrace = LogUtils.buildStackTrace(exception);
		log(source, phase, "ERROR", identifier, stackTrace);
	}

	@Override
	public void logError(Qname source, Class<?> phase, String identifier, String message, Throwable exception) {
		String stackTrace = message + ":\n" + LogUtils.buildStackTrace(exception);
		log(source, phase, "ERROR", identifier, stackTrace);
	}

	@Override
	public void logMessage(Qname source, Class<?> phase, String message) {
		log(source, phase, "MESSAGE", null, message);
	}

	@Override
	public void logMessage(Qname source, Class<?> phase, String identifier, String message) {
		log(source, phase, "MESSAGE", identifier, message);
	}

	protected void log(Qname source, Class<?> phase, String type, String identifier, String message) {
		System.out.println(Utils.debugS("Laji-ETL/DW", source, phase.getSimpleName(), type, Utils.trimToLength(identifier, 400), Utils.trimToLength(message, 4000)));
		oracle.log(new LogEntry(source, phase, type, identifier, message));
	}

	@Override
	public List<LogEntry> getLogEntries() {
		return oracle.getLogEntries();
	}

	@Override
	public Taxon getTaxon(Qname qname) throws NoSuchTaxonException {
		return taxonomyDAO.getTaxon(qname);
	}

	@Override
	public boolean hasTaxon(Qname qname) {
		return taxonomyDAO.getTaxonContainer().hasTaxon(qname);
	}

	@Override
	public TaxonomyDAO getTaxonomyDAO() {
		return taxonomyDAO;
	}

	@Override
	/**
	 *  Get all matches for the target name, including conflicting - used to determine secured species
	 */
	public Set<Qname> getAllTaxonMatches(String targetName) {
		if (targetName == null) return Collections.emptySet();
		Set<Qname> qnames = new HashSet<>();
		qnames.addAll(taxonomyDAO.getQnamesThatMatch(targetName));
		qnames.add(getTaxonLinkingService().getTaxonId(targetName));
		qnames.remove(null);
		return qnames;
	}

	private final SingleObjectCache<List<UnlinkedTargetNameData>> cachedUnlinkedTargetNames = new SingleObjectCache<>(
			new SingleObjectCache.CacheLoader<List<UnlinkedTargetNameData>>() {
				@Override
				public List<UnlinkedTargetNameData> load() {
					try {
						return getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getUnlinkedTargetNames();
					} catch (Exception e) {
						throw exceptionAndReport("Unlinked target name cache loader", e);
					}
				}}, 1, TimeUnit.HOURS);

	private final SingleObjectCache<List<UnlinkedUserIdsData>> cachedUnlinkedUserIds = new SingleObjectCache<>(
			new SingleObjectCache.CacheLoader<List<UnlinkedUserIdsData>>() {
				@Override
				public List<UnlinkedUserIdsData> load() {
					try {
						return getPrivateVerticaDAO().getQueryDAO().getCustomQueries().getUnlinkedUserIds();
					} catch (Exception e) {
						throw exceptionAndReport("Unlinked user ids cache loader", e);
					}
				}}, 1, TimeUnit.HOURS);

	@Override
	public List<UnlinkedTargetNameData> getUnlinkedTargetNames() {
		return cachedUnlinkedTargetNames.get();
	}

	@Override
	public List<UnlinkedUserIdsData> getUnlinkedUserIds() {
		return cachedUnlinkedUserIds.get();
	}

	@Override
	public void clearCachesStartReload() {
		this.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Clear caches called");
		taxonomyDAO.reloadTriplets();
		taxonomyDAO.reloadHabitats();
		reloadPersonInfos();
		synchronized (TAXON_LOCK) {
			cachedTaxaWithSecureLevels.invalidate();
			taxonomyDAO.clearCaches();
			personInfoCache.invalidateAll();
			taxonLookup = null;
			personLookup = null;
			areaMunicipalities = null;
			try { getTaxon(new Qname("MX.1")); } catch (Exception e) {}
			try { getTaxonLookupStructure(); } catch (Exception e) {}
			try { getPersonLookupStructure(); } catch (Exception e) {}
		}
	}

	@Override
	public Map<String, Qname> getPersonLookupStructure() {
		if (personLookup == null) {
			synchronized (TAXON_LOCK) {
				if (personLookup == null) {
					try {
						this.logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Initializing person lookup");
						repopulatePersonInfoCache();
						personLookup = new LookupLoader(new PersonLookupNameLoader(this)).load();
					} catch (Exception e) {
						this.logError(Const.LAJI_ETL_QNAME, this.getClass(), "personLookup", e);
					}
				}

			}
		}
		return personLookup;
	}

	@Override
	public TaxonLinkingService getTaxonLinkingService() {
		return new TaxonLinkingService(getTaxonLookupStructure());
	}

	private Map<String, Qname> getTaxonLookupStructure() {
		if (taxonLookup == null) {
			synchronized (TAXON_LOCK) {
				if (taxonLookup == null) {
					try {
						LookupLoader loader = new LookupLoader(new TaxonLookupNameLoader(this, threadStatuses));
						taxonLookup = loader.load();
						try {
							for (String error : loader.getErrors()) {
								log(Const.LAJI_ETL_QNAME, TaxonLookupNameLoader.class, "ERROR", "taxonLookup", error);
							}
							//	for (String error : loader.getWarnings()) {
							// 		log(Const.LAJI_ETL_QNAME, TaxonLookupNameLoader.class, "WARNING", "taxonLookup", error);
							//	}
						} catch (Exception e) {}
						try {
							if (!loader.getErrors().isEmpty()) {
								errorReporter.report("Taxon lookup loading had errors. Check DW log.");
							}
						} catch (Exception e) {}
					} catch (Exception e) {
						this.logError(Const.LAJI_ETL_QNAME, this.getClass(), "taxonLookup", e);
						throw new ETLException(e);
					}
				}
			}
		}
		return taxonLookup;
	}

	private final List<BaseQuery> logCache = new ArrayList<>();

	@Override
	public <T extends BaseQuery> void logQuery(T query) {
		if (config.developmentMode()) {
			System.out.println("LOGGING DW QUERY: " + query);
		}
		if (noPublicLog(query)) {
			return; // do not log public api queries from these sources that do lots of queries
		}
		synchronized (LOG_LOCK) {
			logCache.add(query);
		}
	}

	private <T extends BaseQuery> boolean noPublicLog(T query) {
		if (query.getWarehouse() == Concealment.PRIVATE) return false;
		return noPublicQueryLogSources().contains(query.getApiSourceId());
	}

	private Set<String> noPublicQueryLogSources() {
		return config.productionMode() ? Const.PROD_NO_PUBLIC_QUERY_LOG : Const.STAGING_NO_PUBLIC_QUERY_LOG;
	}

	public void tempStoreQueryLog() {
		List<BaseQuery> copy;
		synchronized (LOG_LOCK) {
			copy = new ArrayList<>(logCache);
			logCache.clear();
		}
		if (copy.isEmpty()) return;
		tempStoreQueryLog(copy);
	}

	private void tempStoreQueryLog(List<BaseQuery> queries) {
		try {
			persistToFile(queries);
		} catch (Exception e) {
			errorReporter.report("Failed to temp store " + queries.size() + " queries", e);
			logError(Const.LAJI_ETL_QNAME, DAOImple.class, "tempStoreQueryLog", e);
		}
	}

	private void persistToFile(List<BaseQuery> queries) throws IOException {
		synchronized (LOG_FILE_LOCK) {
			File tmpFolder = tempFolder();
			File logFile = new File(tmpFolder, "query_log_"+DateUtils.getCurrentDate()+".txt");
			OutputStream out = null;
			OutputStreamWriter writer = null;
			try {
				if (logFile.getParentFile() != null) {
					logFile.getParentFile().mkdirs();
				}
				out = new FileOutputStream(logFile, true);
				writer = new OutputStreamWriter(out, "UTF-8");
				for (BaseQuery q : queries) {
					writer.write(serialize(q)+"\n");
				}
			} finally {
				if (writer != null) writer.close();
				if (out != null) out.close();
			}
		}
	}

	private File tempFolder() {
		return new File(config.baseFolder() + config.get("TempFolder"));
	}

	@Override
	public long persistQueryLogToLogTable() {
		long count = 0;
		synchronized (LOG_FILE_LOCK) {
			File tmpFolder = tempFolder();
			tmpFolder.mkdirs();
			for (File f : tmpFolder.listFiles()) {
				if (f.isFile() && f.getName().startsWith("query_log_")) {
					count = persistQueryLogToLogTable(count, f);
				}
			}
		}
		return count;
	}

	private long persistQueryLogToLogTable(long count, File f) {
		try {
			List<QueryLogEntity> queries = readFromFile(f);
			oracle.storeQueryLog(queries);
			count += queries.size();
			f.delete();
		} catch (Exception e) {
			errorReporter.report("Failed to persist " + f.getName(), e);
			logError(Const.LAJI_ETL_QNAME, DAOImple.class, f.getName(), e);
		}
		return count;
	}

	private List<QueryLogEntity> readFromFile(File f) throws IOException {
		List<String> lines = FileUtils.readLines(f);
		List<QueryLogEntity> queries = new ArrayList<>(lines.size());
		for (String line : lines) {
			if (line.isEmpty()) continue;
			JSONObject json = new JSONObject(line);
			queries.add(deserialize(json));
		}
		return queries;
	}

	private String serialize(BaseQuery q) {
		JSONObject json = new JSONObject();
		json.setString("id", q.getGUID());
		json.setInteger("timestamp", (int) (q.getTimestamp() / 1000));
		json.setString("warehouse", q.getWarehouse().toString());
		json.setString("base", q.getBase().name());
		json.setString("apiUser", q.getApiSourceId());
		json.setString("caller", q.getCaller().getSimpleName());
		json.setString("userAgent", Util.trimToByteLength(q.getUserAgent(), 4000));
		if (q.getPersonId() != null) json.setString("personId", q.getPersonId().toString());
		json.setString("personEmail", q.getPersonEmail());
		json.setString("queryString", q.toString());
		if (q.getPermissionId() != null)  json.setString("permissionId", q.getPermissionId().toString());
		return json.toString().replace("\r", "").replace("\n", "");
	}

	private QueryLogEntity deserialize(JSONObject json) {
		QueryLogEntity e = new QueryLogEntity();
		e.setId(json.getString("id"));
		e.setYear(DateUtils.getCurrentYear());
		e.setTimestamp(json.hasKey("timestamp") ? json.getInteger("timestamp") : DateUtils.getCurrentEpoch());
		e.setWarehouse(json.getString("warehouse"));
		e.setBase(json.getString("base"));
		e.setApiUser(json.getString("apiUser"));
		e.setCaller(json.getString("caller"));
		e.setUserAgent(Util.trimToByteLength(json.getString("userAgent"), 4000));
		e.setPersonId(json.getString("personId"));
		e.setPersonEmail(json.getString("personEmail"));
		e.setQueryString(json.getString("queryString"));
		e.setPermissionId(json.getString("permissionId"));
		return e;
	}

	private final Cached<String, ApiUser> cachedApiUsers = new Cached<>(new CacheLoader<String, ApiUser>() {

		@Override
		public ApiUser load(String accessToken) {
			try {
				return tryToLoad(accessToken);
			} catch (Exception e) {
				throw exceptionAndReport("Api users cache", e);
			}
		}

		private ApiUser tryToLoad(String accessToken) throws Exception {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/api-users")
					.addParameter("access_token", getApiKey()) // our api access token
					.addParameter("accessToken", accessToken); // token to query
			JSONObject response = null;
			try {
				response = client.contentAsJson(new HttpGet(uri.getURI()));
			} catch (NotFoundException nfe) {
				return ApiUser.invalid();
			}
			String email = response.getString("email");
			if (!given(email)) return ApiUser.invalid();
			String systemId = response.getString("systemID");
			if (given(systemId)) {
				return new ApiUser(email, new Qname(systemId));
			}
			return new ApiUser(email);
		}

	}, 1, TimeUnit.HOURS, 5000);

	@Override
	public ApiUser getApiUser(String accessToken) {
		if (!given(accessToken)) throw new IllegalArgumentException("Must give accessToken");
		return cachedApiUsers.get(accessToken);
	}

	private String getApiUrl() {
		return config.get("ApiBaseURL");
	}

	private String getApiKey() {
		return config.get("ApiKey");
	}

	private final Cached<Qname, RdfProperty> cachedProperties = new Cached<>(new CacheLoader<Qname, RdfProperty>() {
		@Override
		public RdfProperty load(Qname propertyQname) {
			synchronized (TRIPLESTORE_LOCK) { // not too many requests at once
				RdfProperty property = new RdfProperty(propertyQname);
				property.setLabels(new LocalizedText());
				try {
					URIBuilder uri  = new URIBuilder(getTriplestoreUrl() + "/" + propertyQname.toString()).addParameter("format", "RDFXML");
					HttpClientService client = getTriplestoreClient();
					Document response = client.contentAsDocument(new HttpGet(uri.getURI()));
					Node root = response.getRootNode().getNode(RDF_DESCRIPTION);
					for (Node label : root.getChildNodes("rdfs:label")) {
						addLabel(property, label);
					}
					for (Node label : root.getChildNodes("MMAN.name")) {
						addLabel(property, label);
					}
					fillMissingLocales(property.getLabel());
					return property;
				} catch (Exception e) {
					errorReporter.report("Property labels " + propertyQname, e);
				}
				return property;
			}
		}

		private void addLabel(RdfProperty property, Node label) {
			String locale = label.hasAttribute(XML_LANG) ? label.getAttribute(XML_LANG) : null;
			String description = label.getContents();
			property.getLabel().set(locale, description);
		}
	}, 12, TimeUnit.HOURS, 5000);

	@Override
	public LocalizedText getLabels(Qname propertyQname) {
		RdfProperty property = getProperty(propertyQname);
		return property.getLabel();
	}

	private RdfProperty getProperty(Qname propertyQname) {
		return cachedProperties.get(propertyQname);
	}

	private final SingleObjectCache<Set<Qname>> cachedAltValues = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<Set<Qname>>() {
		@Override
		public Set<Qname> load() {
			synchronized (TRIPLESTORE_LOCK) { // not too many requests at once
				Set<Qname> alts = new HashSet<>();
				HttpClientService client = getTriplestoreClient();
				try {
					JSONObject response = client.contentAsJson(new HttpGet(getTriplestoreUrl() + "/schema/alt"));
					for (String key : response.getKeys()) {
						for (JSONObject alt : response.getArray(key).iterateAsObject()) {
							alts.add(new Qname(alt.getString("id")));
						}
					}
				} catch (Exception e) {
					throw exceptionAndReport("Loading schema alts", e);
				}
				return alts;
			}
		}
	}, 12, TimeUnit.HOURS);

	@Override
	public boolean isValidEnumeration(Qname enumerationValue) {
		return cachedAltValues.get().contains(enumerationValue);
	}

	@Override
	public void storeDownloadRequest(DownloadRequest request) {
		HttpClientService client = getStoringTriplestoreClient();
		String rdfData = toRdfModel(request);
		HttpPost post = new HttpPost(getStoringTriplestoreUrl()+"/"+request.getId().toString());
		post.setEntity(new StringEntity(rdfData, Charset.forName("UTF-8")));
		CloseableHttpResponse response = null;
		try {
			response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if (status != 200) throw new ETLException("Triplestore returned status " + status + " with message: " + EntityUtils.toString(response.getEntity()));
		} catch (ETLException e) {
			throw e;
		} catch (Exception e ) {
			throw new ETLException(e);
		} finally {
			if (response != null) close(response);
			apiKeyDownloadRequestCache.invalidate(request.getId());
		}
	}

	private String toRdfModel(DownloadRequest request) {
		Model model = new Model(request.getId());
		model.setType("HBF.downloadRequest");

		model.addStatementIfObjectGiven("HBF.downloadType", request.getDownloadType().name(), null);
		if (request.getDownloadFormat() != null) {
			model.addStatementIfObjectGiven("HBF.downloadFormat", request.getDownloadFormat().name(), null);
		}
		for (DownloadInclude include : request.getDownloadIncludes()) {
			model.addStatementIfObjectGiven("HBF.downloadInclude", include.name(), null);
		}
		for (Filter filter : request.getFilters().getSetFiltersIncludingDefaults()) {
			model.addStatementIfObjectGiven("HBF.filter", filter.toString(), null);
		}
		model.addStatementIfObjectGiven("HBF.source", request.getApiSourceId(), null);
		model.addStatementIfObjectGiven("HBF.email", request.getPersonEmail(), null);
		if (request.getPersonId() != null) {
			model.addStatementIfObjectGiven("HBF.person", request.getPersonId().toString());
		}
		model.addStatementIfObjectGiven("HBF.locale", request.getLocale());
		model.addStatementIfObjectGiven("HBF.warehouse", request.getWarehouse().name(), null);
		model.addStatementIfObjectGiven("HBF.approximateResultSize", String.valueOf(request.getApproximateResultSize()), null);
		model.addStatementIfObjectGiven("HBF.dataUsePurpose", request.getDataUsePurpose(), null);
		if (request.getApiKeyExpires() != null) {
			model.addStatementIfObjectGiven("HBF.apiKeyExpires", DateUtils.format(request.getApiKeyExpires(), "yyyy-MM-dd"), null);
		}

		model.addStatementIfObjectGiven("HBF.requested", DateUtils.format(request.getRequested(), "yyyy-MM-dd"), null);
		model.addStatementIfObjectGiven("HBF.completed", request.isCompleted() ? "true" : "false", null);

		if (request.isCompleted()) {
			model.addStatementIfObjectGiven("HBF.createdFile", request.getCreatedFile(), null);
			if (request.getCreatedFileSize() != null) {
				model.addStatementIfObjectGiven("HBF.createdFileSize", String.valueOf(request.getCreatedFileSize()), null);
			}
			model.addStatementIfObjectGiven("HBF.created", DateUtils.format(request.getRequested(), "yyyy-MM-dd"), null);
			for (Qname collectionId : request.getCollectionIds()) {
				model.addStatementIfObjectGiven("HBF.collectionId", collectionId.toURI(), null);
			}
		}

		if (request.isFailed()) {
			model.addStatementIfObjectGiven("HBF.failed", "true", null);
			model.addStatementIfObjectGiven("HBF.failureReason", request.getFailureMessage(), null);
		}

		return model.getRDF();
	}

	@Override
	public DownloadRequest getDownloadRequest(Qname id) {
		try {
			HttpClientService client = getStoringTriplestoreClient();
			URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl() + "/" + id);
			uri.addParameter("format", "rdfxml");
			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			if (doc.getRootNode().getChildNodes().size() < 1) throw new IllegalArgumentException("Download request not found with id " + id);
			Node requestNode = doc.getRootNode().getChildNodes().get(0);
			DownloadRequest downloadRequest = toRequest(requestNode);
			return downloadRequest;
		} catch (Exception e) {
			throw new ETLException("Download request " + id, e);
		}
	}

	@Override
	public List<DownloadRequest> getUncompletedDownloadRequests() {
		return getDownloadRequests("HBF.completed", "false");
	}

	@Override
	public List<DownloadRequest> getFailedDownloadRequests() {
		return getDownloadRequests("HBF.failed", "true");
	}

	@Override
	public List<DownloadRequest> getDownloadRequestsFromToday() {
		return getDownloadRequests("HBF.requested", DateUtils.getCurrentDateTime("yyyy-MM-dd"));
	}

	private List<DownloadRequest> getAuthoritiesDownloadRequestsNonCached() {
		return getDownloadRequests("HBF.downloadType", DownloadType.AUTHORITIES_FULL+","+DownloadType.AUTHORITIES_LIGHTWEIGHT);
	}

	private List<DownloadRequest> getPersonsDownloadRequestsNonCached(Qname personId) {
		return getDownloadRequests("HBF.person", personId.toString());
	}

	private List<DownloadRequest> getDownloadRequests(String predicate, String object) {
		long start = System.currentTimeMillis();
		try {
			List<DownloadRequest> all = new ArrayList<>();
			int page = 1;
			int limit = config.developmentMode() ? 10 : 500; // in dev mode we return only first 10
			List<Node> pageContents = null;
			while (!(pageContents = getDownloadRequests(page++, limit, predicate, object)).isEmpty()) {
				for (Node requestNode : pageContents) {
					all.add(toRequest(requestNode));
				}
				if (config.developmentMode()) break;
			}
			Collections.sort(all);
			return all;
		} catch (Exception e) {
			throw exceptionAndReport("Loading download requests", e);
		} finally {
			double duration = (System.currentTimeMillis() - start)/1000.0;
			if (duration >= 10) {
				logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Loading downloads from Triplestore with parameters " + Utils.debugS(predicate, object) + " took " + duration + " seconds!");
			}
		}
	}

	private List<Node> getDownloadRequests(int page, int limit, String predicate, String object) throws ClientProtocolException, ETLException, IOException {
		URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl()+"/search")
				.addParameter("type", "HBF.downloadRequest")
				.addParameter("predicate", predicate)
				.addParameter("objectliteral", object)
				.addParameter("limit", limit)
				.addParameter("offset", (page-1)*limit)
				.addParameter("format", "rdfxml");
		HttpGet request = new HttpGet(uri.toString());
		Document response = getStoringTriplestoreClient().contentAsDocument(request); // from triplestore staging for dev/staging
		if (!response.getRootNode().hasChildNodes()) return Collections.emptyList();
		return response.getRootNode().getChildNodes();
	}


	private DownloadRequest toRequest(Node requestNode) throws ParseException {
		Concealment warehouse = Concealment.valueOf(requestNode.getNode("HBF.warehouse").getContents());
		String apiSourceId = requestNode.getNode("HBF.source").getContents();
		String email = getValue("HBF.email", requestNode);
		Qname personId = new Qname(getValue("HBF.person", requestNode));
		if (!given(email)) email = null;
		if (!personId.isSet()) personId = null;

		BaseQuery baseQuery = new BaseQueryBuilder(warehouse).setApiSourceId(apiSourceId).setCaller(DAOImple.class).setPersonId(personId).setPersonEmail(email).build();
		Filters filters = baseQuery.getFilters();
		for (Node filterNode : requestNode.getChildNodes("HBF.filter")) {
			try {
				filters.addFilter(filterNode.getContents());
			} catch (Exception e) {
				// Unknown filter is ignored (legacy values etc)
			}
		}
		DownloadType downloadType = getDownloadType(requestNode);
		DownloadFormat downloadFormat = null;
		if (requestNode.hasChildNodes("HBF.downloadFormat")) {
			downloadFormat = DownloadFormat.valueOf(getValue("HBF.downloadFormat", requestNode));
		}
		Set<DownloadInclude> downloadIncludes = new HashSet<>();
		for (Node includeNode : requestNode.getChildNodes("HBF.downloadInclude")) {
			downloadIncludes.add(DownloadInclude.valueOf(includeNode.getContents()));
		}
		boolean failed = getValue("HBF.failed", requestNode).equals("true");
		boolean completed = getValue("HBF.completed", requestNode).equals("true");

		Qname id = Qname.fromURI(requestNode.getAttribute(RDF_ABOUT));
		Date requested = DateUtils.convertToDate(getValue("HBF.requested", requestNode), "yyyy-MM-dd");

		String locale = getValue("HBF.locale", requestNode);
		DownloadRequest downloadRequest = new DownloadRequest(id, requested, baseQuery, downloadType, downloadFormat, downloadIncludes);

		if (given(locale)) downloadRequest.setLocale(locale);
		downloadRequest.setFailed(failed);
		downloadRequest.setCompleted(completed);
		if (requestNode.hasChildNodes("HBF.approximateResultSize")) {
			downloadRequest.setApproximateResultSize(Long.valueOf(getValue("HBF.approximateResultSize", requestNode)));
		}
		if (requestNode.hasChildNodes("HBF.dataUsePurpose")) {
			downloadRequest.setDataUsePurpose(getValue("HBF.dataUsePurpose", requestNode));
		}
		if (requestNode.hasChildNodes("HBF.apiKeyExpires")) {
			downloadRequest.setApiKeyExpires(DateUtils.convertToDate(getValue("HBF.apiKeyExpires", requestNode), "yyyy-MM-dd"));
		}

		if (downloadRequest.isCompleted()) {
			if (requestNode.hasChildNodes("HBF.createdFile")) {
				downloadRequest.setCreatedFile(getValue("HBF.createdFile", requestNode));
			}
			if (requestNode.hasChildNodes("HBF.createdFileSize")) {
				downloadRequest.setCreatedFileSize(Double.valueOf(getValue("HBF.createdFileSize", requestNode)));
			}
			if (requestNode.hasChildNodes("HBF.created")) {
				downloadRequest.setCreated(DateUtils.convertToDate(getValue("HBF.created", requestNode), "yyyy-MM-dd"));
			}
			Set<Qname> collectionIds = new HashSet<>();
			for (Node colNode : requestNode.getChildNodes("HBF.collectionId")) {
				collectionIds.add(Qname.fromURI(colNode.getContents()));
			}
			downloadRequest.setCollectionIds(collectionIds);
		}

		if (failed) {
			downloadRequest.setFailureMessage(getValue("HBF.failureReason", requestNode));
		}

		return downloadRequest;
	}

	private DownloadType getDownloadType(Node requestNode) {
		String s = getValue("HBF.downloadType", requestNode);
		return DownloadType.valueOf(s);
	}

	@Override
	public void sendDownloadRequestForApproval(JSONObject json) {
		HttpClientService client = null;
		CloseableHttpResponse response = null;
		try {
			String address = config.get("ApprovalServiceURL") + "/request/";
			String username = config.get("ApprovalServiceUsername");
			String password = config.get("ApprovalServicePassword");
			client = new HttpClientService(address, username, password);
			HttpPost post = new HttpPost(address);
			post.setEntity(new StringEntity(json.toString(), Charset.forName("UTF-8")));
			response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if (status != 200) throw new ETLException("Approval service returned status " + status + " with message: " + EntityUtils.toString(response.getEntity()));
		} catch (ETLException e) {
			throw e;
		} catch (Exception e) {
			throw new ETLException(e);
		}
		finally {
			if (client != null) client.close();
			if (response != null) close(response);
		}
	}

	@Override
	public void notifyApprovalServiceOfCompletion(Qname id) {
		HttpClientService client = null;
		CloseableHttpResponse response = null;
		try {
			String address = config.get("ApprovalServiceURL") + "/download/" + id.toString();
			String username = config.get("ApprovalServiceUsername");
			String password = config.get("ApprovalServicePassword");
			client = new HttpClientService(address, username, password);
			HttpPost post = new HttpPost(address);
			response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if (status != 200) throw new ETLException("Approval service returned status " + status + " with message: " + EntityUtils.toString(response.getEntity()));
		} catch (ETLException e) {
			throw e;
		}
		catch (Exception e) {
			throw new ETLException("Mark approved request complated " + id, e);
		} finally {
			if (client != null) client.close();
			if (response != null) close(response);
		}
	}

	private void close(CloseableHttpResponse response) {
		try { response.close(); } catch (Exception e) {}
	}

	@Override
	public List<MediaObject> getMediaObjects(Qname documentId) {
		URIBuilder b = new URIBuilder(getStoringTriplestoreUrl()+"/search");
		b.addParameter("predicate", "MM.documentURI");
		b.addParameter("objectliteral", documentId.toURI());

		try {
			HttpClientService client = getStoringTriplestoreClient(); // media from triplestore staging for dev/staging
			Document doc = client.contentAsDocument(new HttpGet(b.getURI()));
			if (!doc.getRootNode().hasChildNodes()) return Collections.emptyList();
			List<MediaObject> media = new ArrayList<>();
			for (Node node : doc.getRootNode().getChildNodes()) {
				MediaObject m = parseMediaObject(node);
				if (m != null) media.add(m);
			}
			return media;
		} catch (Exception e) {
			throw new ETLException("Media objects for document " + documentId, e);
		}
	}

	@Override
	public MediaObject getMediaObject(Qname id) {
		String uri = getStoringTriplestoreUrl()+"/"+id.toString();
		try {
			HttpClientService client = getStoringTriplestoreClient(); // media from triplestore staging for dev/staging
			Document doc = client.contentAsDocument(new HttpGet(uri));
			if (!doc.getRootNode().hasChildNodes()) return null;
			return parseMediaObject(doc.getRootNode().getChildNodes().get(0));
		} catch (NotFoundException notFoundEx) {
			return null;
		} catch (Exception e) {
			throw new ETLException("Media object " + id, e);
		}
	}

	private MediaObject parseMediaObject(Node node) throws DataValidationException {
		String publicityRestrictions = getValue("MZ.publicityRestrictions", node);
		if (notPublic(publicityRestrictions)) return null;

		MediaObject m = new MediaObject(parseMediaType(node));
		m.setId(Qname.fromURI(node.getAttribute(RDF_ABOUT)));

		String fullURL = getValue("MM.fullURL", node);
		String originalURL = getValue("MM.originalURL", node);
		String squareThumbnailURL = getValue("MM.squareThumbnailURL", node);
		String thumbnailURL = getValue("MM.thumbnailURL", node);
		String mp3URL = getValue("MM.mp3URL", node);
		String wavURL = getValue("MM.wavURL", node);
		String videoURL = getValue("MM.videoURL", node);
		String lowDetailModelURL = getValue("MM.lowDetailModelURL", node);
		String highDetailModelURL = getValue("MM.highDetailModelURL", node);
		String capturerVerbatim = Util.catenate(getValues("MM.capturerVerbatim", node));
		String intellectualOwner = getValue("MZ.intellectualOwner", node);
		String intellectualRights = getValue("MZ.intellectualRights", node);
		String caption = getValue("MM.caption", node);
		String type = getValue("MM.type", node);
		String fullResolutionMediaAvailable = getValue("MM.fullResolutionMediaAvailable", node);

		if (given(fullURL)) {
			m.setFullURL(fullURL);
		} else {
			m.setFullURL(originalURL);
		}
		if (given(squareThumbnailURL)) m.setSquareThumbnailURL(squareThumbnailURL);
		if (given(thumbnailURL)) m.setThumbnailURL(thumbnailURL);
		if (given(mp3URL)) m.setMp3URL(mp3URL);
		if (given(wavURL)) m.setWavURL(wavURL);
		if (given(videoURL)) m.setVideoURL(videoURL);
		if (given(lowDetailModelURL)) m.setLowDetailModelURL(lowDetailModelURL);
		if (given(highDetailModelURL)) m.setHighDetailModelURL(highDetailModelURL);
		if (given(capturerVerbatim)) m.setAuthor(capturerVerbatim);
		if (given(intellectualOwner)) m.setCopyrightOwner(intellectualOwner);
		if (given(intellectualRights)) m.setLicenseIdUsingQname(Qname.fromURI(intellectualRights));
		if (given(caption)) m.setCaption(caption);
		if (given(type)) m.setType(Qname.fromURI(type));
		for (String keyword : getValues("MM.keyword", node)) {
			m.addKeyword(keyword);
		}
		if (given(fullResolutionMediaAvailable)) {
			m.setFullResolutionMediaAvailable(fullResolutionMediaAvailable.equals("true"));
		}
		return m;
	}

	private boolean notPublic(String publicityRestrictions) {
		return given(publicityRestrictions) && !publicityRestrictions.equals("http://tun.fi/MZ.publicityRestrictionsPublic");
	}

	private String getValue(String fieldName, Node node) {
		if (!node.hasChildNodes(fieldName)) return "";
		if (node.getChildNodes(fieldName).size() > 1) {
			for (Node langNode : node.getChildNodes(fieldName)) {
				if (langNode.hasAttribute(XML_LANG) && langNode.getAttribute(XML_LANG).equals("en")) {
					return langNode.getContents();
				}
			}
		}
		Node fieldNode = node.getNode(fieldName);
		if (fieldNode.hasAttribute(RDF_RESOURCE)) return fieldNode.getAttribute(RDF_RESOURCE);
		return fieldNode.getContents();
	}

	private List<String> getValues(String fieldName, Node node) {
		if (!node.hasChildNodes(fieldName)) return Collections.emptyList();
		List<String> list = new ArrayList<>();
		for (Node n : node.getChildNodes(fieldName)) {
			list.add(n.getContents());
		}
		return list;
	}

	private MediaType parseMediaType(Node n) {
		if (n.getName().equals("MM.image")) return MediaType.IMAGE;
		if (n.getName().equals("MM.audio")) return MediaType.AUDIO;
		if (n.getName().equals("MM.video")) return MediaType.VIDEO;
		if (n.getName().equals("MM.model")) return MediaType.MODEL;
		throw new UnsupportedOperationException("Not yet implemented for media type " + n.getName());
	}

	@Override
	public void sendNotification(AnnotationNotification notification) {
		CloseableHttpResponse response = null;
		try {
			HttpPost request = new HttpPost(getLajistoreUrl()+"/notification");

			JSONObject data = generateNotificationBody(notification);
			HttpEntity entity = new ByteArrayEntity(data.toString().getBytes("UTF-8"), ContentType.APPLICATION_JSON);
			request.setEntity(entity);
			request.setHeader("Accept", ContentType.APPLICATION_JSON.getMimeType());
			request.addHeader("Authorization", config.get("LajistoreAuthorization"));
			response = getLajistoreClient().execute(request);
			if (response.getStatusLine().getStatusCode() != 201) {
				throwException(response);
			}
		} catch (Exception e) {
			if (e instanceof ETLException) throw (ETLException) e;
			throw new ETLException("Sending notification", e);
		} finally {
			if (response != null) try {response.close(); } catch (Exception e) {}
		}
	}

	private void throwException(CloseableHttpResponse response) {
		String message = "";
		try {
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				message = EntityUtils.toString(responseEntity, "UTF-8");
			}
		} catch (Exception e) {}
		throw new ETLException("Sending notification failed, received status " + response.getStatusLine().getStatusCode() + " with message " + message);
	}

	private JSONObject generateNotificationBody(AnnotationNotification notification) {
		JSONObject data = new JSONObject();
		data.setString("toPerson", notification.getPersonId().toString());
		data.setObject("annotation", etlToLajistoreConversion(ModelToJson.toJson(notification.getAnnotation())));
		if (notification.getNotificationReason() != null) {
			if (notification.getNotificationReason() == NotificationReason.MY_DOCUMENT_ANNOTATED) {
				data.setString("notificationReason", "MHN.notificationReasonOwnDocumentAnnotated");
			} else if (notification.getNotificationReason() == NotificationReason.ANNOTATED_DOCUMENT_ANNOTATED) {
				data.setString("notificationReason", "MHN.notificationReasonAnnotatedDocumentAnnotated");
			}
		}
		data.setBoolean("seen", false);
		Calendar c = Calendar.getInstance();
		c.setTime(new Date(DateUtils.getCurrentEpoch()*1000));
		c.setTimeZone(GMT_TIMEZONE);
		String created = javax.xml.bind.DatatypeConverter.printDateTime(c).replace("Z", "+00:00");
		data.setString("created", created);
		return data;
	}

	private JSONObject etlToLajistoreConversion(JSONObject json) {
		for (String key : json.getKeys()) {
			String value = json.getString(key);
			if (value.startsWith("http://")) {
				json.setString(key, Qname.fromURI(value).toString());
			}
		}
		json.remove("addedTags");
		json.remove("removedTags");
		json.setString("type", "MAN.typeOpinion"); // TODO remove
		return json;
	}

	@Override
	public Qname getSeqNextVal(String qnamePrefix) {
		try {
			HttpClientService client = getStoringTriplestoreClient();
			URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl() + "/uri/" + qnamePrefix);
			JSONObject response = client.contentAsJson(new HttpGet(uri.toString()));
			String qname = response.getObject("response").getString("qname");
			if (!given(qname)) throw new ETLException("Got null qname");
			return new Qname(qname);
		} catch (Exception e) {
			throw new ETLException("Get seq next val + " + qnamePrefix, e);
		}
	}

	private Map<String, Qname> birdAssociationAreas;

	@Override
	public Qname getBirdAssociationArea(String ykjGridString) {
		if (birdAssociationAreas == null) {
			birdAssociationAreas = initBirdAssociationAreas();
		}
		return birdAssociationAreas.get(ykjGridString);
	}

	private Map<String, Qname> initBirdAssociationAreas() {
		try {
			File file = new File(config.templateFolder()+File.separatorChar+"birdAssociationAreas.json");
			String contents = FileUtils.readContents(file);
			JSONObject data = new JSONObject(contents);
			Map<String, Qname> map = new HashMap<>();
			for (String ykjGrid : data.getKeys()) {
				map.put(ykjGrid, new Qname(data.getString(ykjGrid)));
			}
			return map;
		} catch (Exception e) {
			throw new ETLException("Bird association area mapping failed", e);
		}
	}

	@Override
	public ContextDefinitions getContextDefinitions() {
		try {
			return getContextDefinitionFromSchemaWithTimeOut();
		} catch (Exception e) {
			logError(Const.LAJI_ETL_QNAME, DAOImple.class, "ContextDefinitionFile", e);
			try {
				return getLocalContextDefinition();
			} catch (Exception e2) {
				throw new ETLException("Context definition from schema and local definition failed", e2);
			}
		}
	}

	public ContextDefinitions getContextDefinitionFromSchemaWithTimeOut() throws Exception {
		String definition = Utils.getWithTimeOut(new URI(config.get("ContextDefinitionFile")), 20, TimeUnit.SECONDS);
		return getContextDefinition(new JSONObject(definition));
	}

	private static ContextDefinitions getContextDefinition(JSONObject json) {
		Map<String, ContextDefinition> contextDefinitions = new HashMap<>();
		JSONObject context = json.getObject(CONTEXT_CONTEXT);
		for (String properyName : context.getKeys()) {
			if (context.isObject(properyName)) {
				JSONObject propertyData = context.getObject(properyName);
				if (propertyData.hasKey(CONTEXT_ID)) {
					Qname propertQname = Qname.fromURI(propertyData.getString(CONTEXT_ID));
					boolean resource = isResource(propertyData);
					contextDefinitions.put(properyName, new ContextDefinition(propertQname, resource));
				}
			}
		}
		return new ContextDefinitions(contextDefinitions);
	}

	private static boolean isResource(JSONObject propertyData) {
		return CONTEXT_ID.equals(propertyData.getString(CONTEXT_TYPE));
	}

	public static ContextDefinitions getLocalContextDefinition(Config config) throws Exception {
		File file = new File(config.templateFolder()+File.separatorChar+"document.jsonld");
		String contents = FileUtils.readContents(file);
		JSONObject data = new JSONObject(contents);
		return getContextDefinition(data);
	}

	public ContextDefinitions getLocalContextDefinition() throws Exception {
		return getLocalContextDefinition(config);
	}

	@Override
	public Set<Qname> getInvasiveSpeciesEarlyWarningReportedUnits() {
		return parseIds(getPersisted(INVASIVE_EARLY_WARNING));
	}

	@Override
	public void setInvasiveSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds) {
		persist(INVASIVE_EARLY_WARNING, serializeIds(unitIds));
	}

	@Override
	public Set<Qname> getPestSpeciesEarlyWarningReportedUnits() {
		return parseIds(getPersisted(PEST_EARLY_WARNING));
	}

	@Override
	public void setPestSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds) {
		persist(PEST_EARLY_WARNING, serializeIds(unitIds));
	}

	private Set<Qname> parseIds(String serializedUnitIds) {
		if (serializedUnitIds == null || serializedUnitIds.equals("null")) return new HashSet<>();
		return Arrays.asList(serializedUnitIds.split("\n")).stream()
				.map(Qname::new)
				.collect(Collectors.toSet());
	}

	private String serializeIds(Set<Qname> unitIds) {
		if (unitIds.isEmpty()) return "null";
		return unitIds.stream()
				.map(Qname::toString)
				.collect(Collectors.joining("\n"));
	}

	private final SingleObjectCache<Map<Qname, NamedPlaceEntity>> cachedNamedPlaces = new SingleObjectCache<>(new NamedPlacesLoader(), 1, TimeUnit.DAYS);

	private class NamedPlacesLoader implements SingleObjectCache.CacheLoader<Map<Qname, NamedPlaceEntity>> {

		@Override
		public Map<Qname, NamedPlaceEntity> load() {
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = new SimpleTransactionConnection(config.connectionDescription("Lajistore"));
				p = namedPlaceFetch(con);
				rs = p.executeQuery();
				rs.setFetchSize(4001);
				return generatePlaces(rs);
			} catch (Exception e) {
				throw exceptionAndReport("Loading named places", e);
			} finally {
				Utils.close(p, rs, con);
			}
		}

		private PreparedStatement namedPlaceFetch(TransactionConnection con) throws SQLException {
			return con.prepareStatement("" +
					" SELECT	json_value(data, '$.id'), json_value(data, '$.collectionID'), json_value(data, '$.name'), " +
					"           json_value(data, '$.municipality[0]'), json_value(data, '$.birdAssociationArea[0]'), " +
					"           json_value(data, '$.alternativeIDs[0]'), json_query(data, '$.alternativeIDs'), " +
					"           json_query(data, '$.tags'), json_query(data, '$.geometry') " +
					" FROM		laji_document " +
					" WHERE		type IN ('namedPlace') " +
					" AND		json_value(data, '$.collectionID') IS NOT NULL "
					);
		}

		private Map<Qname, NamedPlaceEntity> generatePlaces(ResultSet rs) throws SQLException {
			Map<Qname, NamedPlaceEntity> places = new HashMap<>();
			while (rs.next()) {
				Qname id = new Qname(rs.getString(1));
				NamedPlaceEntity e = generatePlace(rs, id);
				places.put(id, e);
			}
			return places;
		}

		private NamedPlaceEntity generatePlace(ResultSet rs, Qname id) throws SQLException {
			NamedPlaceEntity namedPlace = new NamedPlaceEntity(id.toURI());
			namedPlace.setCollectionId(new Qname(rs.getString(2)).toURI());
			namedPlace.setName(rs.getString(3));

			Qname municipalityId = new Qname(rs.getString(4));
			Qname birdAssociationAreaId = new Qname(rs.getString(5));

			if (municipalityId.isSet()) {
				namedPlace.setMunicipalityId(municipalityId.toURI());
				namedPlace.setMunicipalityDisplayName(getName(municipalityId));
			}
			if (birdAssociationAreaId.isSet()) {
				namedPlace.setBirdAssociationAreaId(birdAssociationAreaId.toURI());
				namedPlace.setBirdAssociationAreaDisplayName(getName(birdAssociationAreaId));
			}

			namedPlace.setAlternativeId(rs.getString(6));
			parseAllAternativeIds(rs.getString(7), namedPlace);
			parseTags(rs.getString(8), namedPlace);
			parseGeometry(rs.getString(9), namedPlace);
			return namedPlace;
		}

		private void parseAllAternativeIds(String alternativeIds, NamedPlaceEntity namedPlace) {
			if (alternativeIds == null) return;
			try {
				String s = new JSONArray(alternativeIds).iterateAsString().stream().collect(Collectors.joining("; "));
				namedPlace.setAlternativeIds(s);
			} catch (Exception e) {
				return;
			}
		}

		private void parseTags(String tags, NamedPlaceEntity namedPlace) {
			if (tags == null) return;
			try {
				JSONArray json = new JSONArray(tags);
				for (String tag : json.iterateAsString()) {
					namedPlace.addTag(new Qname(tag));
				}
			} catch (Exception e) {
				return;
			}
		}

		private void parseGeometry(String geometry, NamedPlaceEntity namedPlace) {
			if (geometry == null) return;
			try {
				JSONObject geoJson = new JSONObject(geometry);
				geoJson.setString(Geo.CRS, Type.WGS84.name());
				String coordinateVerbatim = geoJson.getString("coordinateVerbatim");
				namedPlace.setYkj10km(parse10kmGrid(coordinateVerbatim));
				Geo geo = Geo.fromGeoJSON(geoJson);
				namedPlace.setWgs84WKT(geo.getWKT());
				namedPlace.setWgs84CenterPoint(geo.getBoundingBox().convertCenterPoint());
			} catch (Exception e) {
				return;
			}
		}

	}

	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlacesForceReload() {
		return cachedNamedPlaces.getForceReload();
	}
	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlaces() {
		return cachedNamedPlaces.get();
	}

	private SingleCoordinates parse10kmGrid(String coordinateVerbatim) {
		if (!given(coordinateVerbatim)) return null;
		if (coordinateVerbatim.length() != VALID_YKJ_10KM_LENGTH) return null;
		String[] parts = coordinateVerbatim.split(Pattern.quote(":"));
		if (parts.length != 2) return null;
		try {
			int lat = Integer.valueOf(parts[0]);
			int lon = Integer.valueOf(parts[1]);
			return new SingleCoordinates(lat, lon, Type.YKJ);
		} catch (Exception e) {
			return null;
		}
	}

	private String getName(Qname areaId) {
		try {
			Area area = getArea(areaId);
			String name = area.getName().forLocale("fi");
			if (given(name)) return name;
			return areaId.toString();
		} catch (AreaMissingException e) {
			return null;
		}
	}

	@Override
	public Map<String, String> getFormNames() {
		Map<String, String> names = new HashMap<>();
		try {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/forms")
					.addParameter("access_token", getApiKey())
					.addParameter("pageSize", 10000)
					.addParameter("lang", "fi");
			JSONObject response = client.contentAsJson(new HttpGet(uri.getURI()));
			for (JSONObject form : response.getArray("results").iterateAsObject()) {
				Qname id = new Qname(form.getString("id"));
				String name = form.getString("title");
				names.put(id.toURI(), name);
			}
		} catch (Exception e) {
			throw new ETLException("Loading form names", e);
		}
		return names;
	}

	@Override
	public ErrorReporter getErrorReporter() {
		return errorReporter;
	}

	@Override
	public Collection<Pair<Qname, Collection<URI>>> getGBIFDatasetEndpoints() {
		HttpClientService client = null;
		try {
			client = new HttpClientService();
			JSONObject response = client.contentAsJson(new HttpGet(
					new URIBuilder("http://api.gbif.org/v1/installation/92a00840-efe1-4b82-9a1d-c655b34c8fce/dataset")
					.addParameter("limit", "1000").getURI())); // TODO pagination
			return parseGbifDatasetEndpoints(response);
		} catch (Exception e) {
			throw new ETLException("Loading GBIF datasets", e);
		} finally {
			if (client != null) client.close();
		}
	}

	private Collection<Pair<Qname, Collection<URI>>> parseGbifDatasetEndpoints(JSONObject response) throws URISyntaxException {
		Collection<Pair<Qname, Collection<URI>>> endpoints = new ArrayList<>();
		for (JSONObject dataset : response.getArray("results").iterateAsObject()) {
			Qname collectionId = new Qname("gbif-dataset:"+dataset.getString("key"));
			List<URI> endpointURIs = new ArrayList<>();
			for (JSONObject endpoint : dataset.getArray("endpoints").iterateAsObject()) {
				URI endpointURI = new URI(endpoint.getString("url"));
				endpointURIs.add(endpointURI);
			}
			endpoints.add(new Pair<Qname, Collection<URI>>(collectionId, endpointURIs));
		}
		return endpoints;
	}

	private RuntimeException exceptionAndReport(String message, Exception e) {
		return exceptionAndReport(null, message, e);
	}

	@Override
	public RuntimeException exceptionAndReport(String identifier, String message, Exception e) {
		ETLException generated = new ETLException(identifier, message, e);
		if (generated.equals(e)) {
			errorReporter.report(e);
		} else {
			errorReporter.report(generated);
		}
		if (e instanceof RuntimeException) {
			RuntimeException original = (RuntimeException) e;
			if (original.equals(generated)) return original;
		}
		return generated;
	}

	private final Map<String, Map<Qname, Integer>> downloadCounts = new HashMap<>();
	private final Map<String, Map<Qname, Integer>> polygonSearchCounts = new HashMap<>();

	@Override
	public boolean exceedsDownloadLimit(Qname personId) {
		return exeedsLimit(downloadCounts, personId, MAX_DOWNLOADS_PER_DAY);
	}

	@Override
	public boolean exceedsPolygonSearchLimit(Qname personId) {
		return exeedsLimit(polygonSearchCounts, personId, MAX_POLYGON_SEARCHES_PER_DAY);
	}

	private boolean exeedsLimit(Map<String, Map<Qname, Integer>> counts, Qname personId, int maxPerDay) {
		synchronized (counts) {
			String today = DateUtils.getCurrentDate();
			if (!counts.containsKey(today)) {
				counts.put(today, new HashMap<>());
			}
			if (!counts.get(today).containsKey(personId)) {
				counts.get(today).put(personId, 1);
				return false;
			}
			Integer count = counts.get(today).get(personId);
			counts.get(today).put(personId, count + 1);
			return count >= maxPerDay;
		}
	}

	@Override
	public void clearPersonDailyLimits() {
		String today = DateUtils.getCurrentDate();
		synchronized (downloadCounts) {
			downloadCounts.keySet().removeIf(k->!k.equals(today));
		}
		synchronized (polygonSearchCounts) {
			polygonSearchCounts.keySet().removeIf(k->!k.equals(today));
		}
	}

	@Override
	public PersonInfo getPerson(Qname personId) {
		if (personId == null || !personId.isSet()) throw new IllegalArgumentException("Must give person qname");
		PersonInfo info = personInfoCache.get(personId);
		if (info == null) {
			info = personInfoCache.getForceReload(personId);
		}
		if (info == null) {
			logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, personId.toString(), "Unknown person!");
			return new PersonInfo(personId, null, "Unknown person (" + personId + ")", null, null);
		}
		return info;
	}

	@Override
	public PersonInfo getPerson(String personToken) {
		Qname personId = getPersonId(personToken);
		return getPerson(personId);
	}

	@Override
	public Collection<PersonInfo> getPersons() {
		return personInfoCache.getAll();
	}


	private final Cached<Qname, PersonInfo> personInfoCache = new Cached<>(new Cached.CacheLoader<Qname, PersonInfo>() {

		@Override
		public PersonInfo load(Qname id) {
			try {
				Node n = getPersonInfo(id);
				if (n == null) return null;
				return toPerson(n);
			} catch (Exception e) {
				throw exceptionAndReport(id.toString(), "person info cache", e);
			}
		}

	}, 2, TimeUnit.DAYS, 1000000);


	private File reloadPersonInfos() {
		try {
			synchronized (LOCK) {
				File finalFile = personFile();
				File backupFile = new File(tempFolder(), "persons_BACKUP.txt");
				File tempFile = generatePersonInfoFile();
				if (finalFile.exists()) {
					if (backupFile.exists()) {
						backupFile.delete();
					}
					finalFile.renameTo(backupFile);
				}
				tempFile.renameTo(finalFile);
				logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Person info saved to " + finalFile.getAbsolutePath());
				return finalFile;
			}
		} catch (Exception e) {
			throw new ETLException("Person reload failed", e);
		}
	}

	private File generatePersonInfoFile() throws IOException {
		List<Node> personInfos = loadAllPersonInfos();
		File tempFile = new File(tempFolder(), "person_TEMP_"+DateUtils.getFilenameDatetime()+".txt");
		tempFile.getParentFile().mkdirs();
		try {
			logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Writing person info file...");
			try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"), 1024*1024)) {
				for (Node n : personInfos) {
					write(writer, n);
				}
			}
			return tempFile;
		} catch (Exception e) {
			if (tempFile.exists()) {
				try { tempFile.delete(); } catch (Exception e2) {}
			}
			throw e;
		}
	}

	private void write(BufferedWriter writer, Node n) throws IOException {
		String xml = new XMLWriter(n).generateXML();
		xml = xml.replace("\n", "").replace("\r", "");
		writer.write(xml);
		writer.write("\n");
	}

	private List<Node> getPersistedPersonInfos() {
		try {
			List<Node> all = new ArrayList<>(40000);
			File file = getPersonFile();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"), 1024*1024)) {
				logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reading person info from " + file.getAbsolutePath() + " ... ");
				String line = null;
				while ((line = reader.readLine()) != null) {
					all.add(toNode(line));
				}
			}
			return all;
		} catch (Exception e) {
			logError(Const.LAJI_ETL_QNAME, DAOImple.class, "loading persisted person infos", e);
			return Collections.emptyList();
		}
	}

	private Node toNode(String line) {
		return new XMLReader().parse(line).getRootNode();
	}

	private void repopulatePersonInfoCache() {
		for (Node n : getPersistedPersonInfos()) {
			PersonInfo info = toPerson(n);
			personInfoCache.put(info.getId(), info);
		}
	}

	private File getPersonFile() {
		File personFile = personFile();
		if (personFile.exists()) return personFile;
		synchronized (LOCK) {
			if (personFile.exists()) return personFile;
			personFile = reloadPersonInfos();
			return personFile;
		}
	}

	private File personFile() {
		return new File(tempFolder(), "persons.txt");
	}

	private PersonInfo toPerson(Node personInfo) {
		Qname id = Qname.fromURI(personInfo.getAttribute(RDF_ABOUT));
		String email = getContents(MA_EMAIL_ADDRESS, personInfo);
		String fullName = getContents(MA_FULL_NAME, personInfo);
		String preferredName = getContents(MA_PREFERRED_NAME, personInfo);
		String inheritedName = getContents(MA_INHERITED_NAME, personInfo);
		PersonInfo person = new PersonInfo(id, email, fullName, preferredName, inheritedName);

		addSystemId(person, personInfo, SystemId.VANHAHATIKKA, MA_HATIKKA_LOGIN_NAME);
		addSystemId(person, personInfo, SystemId.HATIKKA, MA_FIELDJOURNAL_LOGIN_NAME);
		addSystemId(person, personInfo, SystemId.LINTUVAARA, MA_LINTUVAARA_LOGIN_NAME);
		addSystemId(person, personInfo, SystemId.INATURALIST, MA_INATURALIST_LOGIN_NAME);
		addSystemId(person, personInfo, SystemId.OMARIISTA, MA_OMARIISTA_LOGIN_NAME);
		addSystemId(person, personInfo, SystemId.KASTIKKA, MA_KASTIKKA_USER_NAME);

		for (Node n : personInfo.getChildNodes(MA_INSECT_DATABASE_LOGIN_NAME)) {
			String insectDbId = toInsectDbId(n.getContents());
			person.addSystemId(SystemId.VIRTALA, insectDbId);
		}

		person.addSystemId(SystemId.LOYDOS, email);
		person.addSystemId(SystemId.LOYDOS, email.toLowerCase());

		return person;
	}

	private void addSystemId(PersonInfo person, Node personInfo, SystemId systemId, String predicateName) {
		for (Node n : personInfo.getChildNodes(predicateName)) {
			person.addSystemId(systemId, n.getContents());
		}
	}

	private String toInsectDbId(String contents) {
		if (!given(contents)) return null;
		try {
			return Integer.valueOf(contents.substring(contents.indexOf("(")+1,contents.indexOf(")"))).toString();
		} catch (Exception e) {
			return null;
		}
	}

	private String getContents(String field, Node personInfo) {
		if (personInfo.hasChildNodes(field)) {
			return personInfo.getNode(field).getContents();
		}
		return "";
	}

	private Node getPersonInfo(Qname id) throws ClientProtocolException, ETLException, IOException {
		try {
			URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl()+"/"+id)
					.addParameter("format", "rdfxml");
			HttpGet request = new HttpGet(uri.toString());
			Document response = getStoringTriplestoreClient().contentAsDocument(request); // persons from triplestore staging for dev/staging
			if (!response.getRootNode().hasChildNodes()) return null;
			return response.getRootNode().getChildNodes().get(0);
		} catch (NotFoundException e) {
			return null;
		}
	}

	private List<Node> loadAllPersonInfos() throws ClientProtocolException, ETLException, IOException {
		logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Loading person infos from Triplestore...");
		List<Node> all = new ArrayList<>(40000);
		int page = 1;
		int limit = config.developmentMode() ? 10 : 1000; // in dev mode we return only first 10
		List<Node> pageContents = null;
		int i = 0;
		while (!(pageContents = getPersonInfoPage(page++, limit)).isEmpty()) {
			all.addAll(pageContents);
			if (config.developmentMode()) break;
			i++;
			if (i % 10 == 0 || i == 1) {
				logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ... person info page " + i);
			}
		}
		logMessage(Const.LAJI_ETL_QNAME, DAOImple.class, "Person infos from Triplestore loaded!");
		return all;
	}

	private List<Node> getPersonInfoPage(int page, int limit) throws ClientProtocolException, ETLException, IOException {
		URIBuilder uri = new URIBuilder(getStoringTriplestoreUrl()+"/search")
				.addParameter("type", "MA.person")
				.addParameter("limit", limit)
				.addParameter("offset", (page-1)*limit)
				.addParameter("format", "rdfxml");
		HttpGet request = new HttpGet(uri.toString());
		Document response = getStoringTriplestoreClient().contentAsDocument(request); // persons from triplestore staging for dev/staging
		if (!response.getRootNode().hasChildNodes()) return Collections.emptyList();
		return response.getRootNode().getChildNodes();
	}

	private static class LoginInfo {
		private final Qname personId;
		private final Qname targetSystemId;
		public LoginInfo(Qname personId, Qname targetSystemId) {
			this.personId = personId;
			this.targetSystemId = targetSystemId;
		}
	}

	private final Cached<String, LoginInfo> cachedPersonTokens = new Cached<>(new CacheLoader<String, LoginInfo>() {

		@Override
		public LoginInfo load(String personToken) {
			LoginInfo info = null;
			try {
				info = tryToLoad(personToken);
			} catch (Exception e) {
				throw exceptionAndReport(personToken, "Person id for token", e);
			}
			if (info == null) {
				// invalid token (response 400) - throw exception but no logging/error reporting
				throw new ETLException(personToken, "Invalid person token");
			}
			return info;
		}

		private LoginInfo tryToLoad(String personToken) throws ClientProtocolException, IOException, URISyntaxException {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/person-token/"+personToken)
					.addParameter("access_token", getApiKey()); // our api access token
			JSONObject response;
			try {
				response = client.contentAsJson(new HttpGet(uri.getURI()));
			} catch (Exception e) {
				try (CloseableHttpResponse res = client.execute(new HttpGet(uri.getURI()))) {
					if (res.getStatusLine().getStatusCode() == 400) {
						return null;
					}
				}
				throw e;
			}
			String personId = response.getString("personId");
			String target = response.getString("target");
			if (!given(personId) || !given(target)) throw new ETLException("No valid login for this user token.");
			return new LoginInfo(new Qname(personId), new Qname(target));
		}

	}, 1, TimeUnit.HOURS, 10000);

	private Qname getPersonId(String personToken) {
		if (!given(personToken)) throw new IllegalArgumentException("Must give personToken");
		return cachedPersonTokens.get(personToken).personId;
	}

	@Override
	public Qname getPersonTargetSystem(String personToken) {
		if (!given(personToken)) throw new IllegalArgumentException("Must give personToken");
		return cachedPersonTokens.get(personToken).targetSystemId;
	}

	@Override
	public void logOccurrenceCounts() {
		long privateCount = getPrivateVerticaDAO().getQueryDAO().getCount(new CountQuery(
				new BaseQuery(new BaseQueryBuilder(Concealment.PRIVATE)
						.setDefaultFilters(false)
						.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
						.setCaller(this.getClass())
						.build()))).getTotal();
		long publicCount = getPublicVerticaDAO().getQueryDAO().getCount(new CountQuery(
				new BaseQuery(new BaseQueryBuilder(Concealment.PUBLIC)
						.setDefaultFilters(false)
						.setApiSourceId(Const.LAJI_ETL_QNAME.toString())
						.setCaller(this.getClass())
						.build()))).getTotal();
		oracle.logOccurrenceCounts(privateCount, publicCount);
	}

	@Override
	public void persist(String key, String data) {
		oracle.persist(key, data);
	}

	@Override
	public String getPersisted(String key) {
		return oracle.getPersisted(key);
	}

	@Override
	public ETLDAO getETLDAO() {
		return etl;
	}

	@Override
	public ExecutorService getSharedThreadPool() {
		if (threadPool == null) {
			synchronized (LOCK) {
				if (threadPool == null) {
					threadPool = Executors.newFixedThreadPool(100);
				}
			}
		}
		return threadPool;
	}

	@Override
	public List<Taxon> getTaxaWithSecureLevels() {
		return cachedTaxaWithSecureLevels.get();
	}

	private static final Qname NONE = new Qname("MX.secureLevelNone");

	private final SingleObjectCache<List<Taxon>> cachedTaxaWithSecureLevels = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<List<Taxon>>() {

		@Override
		public List<Taxon> load() {
			List<Taxon> taxa = new ArrayList<>();
			Taxon biota = getTaxon(Const.MASTER_CHECKLIST_BIOTA_QNAME);
			addAllIncludingChildren(taxa, biota);
			return copy(taxa);
		}

		private List<Taxon> copy(List<Taxon> taxa) {
			List<Taxon> copies = new ArrayList<>();
			for (Taxon taxon : taxa) {
				Taxon copy = new Taxon(taxon.getQname(), null);
				copy.setScientificName(taxon.getScientificName());
				copy.addVernacularName("fi", taxon.getVernacularName().forLocale("fi"));
				copy.setSecureLevel(taxon.getSecureLevel());
				copy.setBreedingSecureLevel(taxon.getBreedingSecureLevel());
				copy.setWinteringSecureLevel(taxon.getWinteringSecureLevel());
				copy.setNestSiteSecureLevel(taxon.getNestSiteSecureLevel());
				copy.setNaturaAreaSecureLevel(taxon.getNaturaAreaSecureLevel());
				copies.add(copy);
			}
			return copies;
		}

		private void addAllIncludingChildren(List<Taxon> taxa, Taxon taxon) {
			if (hasSecureLevels(taxon)) {
				taxa.add(taxon);
			}
			for (Taxon child : taxon.getChildren()) {
				addAllIncludingChildren(taxa, child);
			}
		}

		private boolean hasSecureLevels(Taxon taxon) {
			if (given(taxon.getSecureLevel())) return true;
			if (given(taxon.getBreedingSecureLevel())) return true;
			if (given(taxon.getWinteringSecureLevel())) return true;
			if (given(taxon.getNestSiteSecureLevel())) return true;
			if (given(taxon.getNaturaAreaSecureLevel())) return true;
			return false;
		}

		private boolean given(Qname level) {
			return level != null && !level.equals(NONE);
		}

	}, 1, TimeUnit.HOURS);

	@Override
	public Set<Qname> getMunicipalitiesPartOfELY(Qname elyId) {
		return getAreaMunicipalities(elyId);
	}

	@Override
	public Set<Qname> getMunicipalitiesPartOfProvince(Qname provinceId) {
		return getAreaMunicipalities(provinceId);
	}

	private Set<Qname> getAreaMunicipalities(Qname areaId) {
		Set<Qname> municipalities = getAreaMunicipalities().get(areaId);
		if (municipalities == null) return Collections.emptySet();
		return Collections.unmodifiableSet(municipalities);
	}

	private Map<Qname, Set<Qname>> areaMunicipalities;

	private Map<Qname, Set<Qname>> getAreaMunicipalities() {
		if (areaMunicipalities == null) {
			areaMunicipalities = loadAreaMunicipalities();
		}
		return areaMunicipalities;
	}

	private Map<Qname, Set<Qname>> loadAreaMunicipalities() throws ETLException {
		try {
			Document response = getAreaTriplestoreResponse(MUNICIPALITY);
			Map<Qname, Set<Qname>> searchMap = new HashMap<>();
			for (Node area : response.getRootNode().getChildNodes(RDF_DESCRIPTION)) {
				Qname municipality = getAreaCode(area);
				Qname ely = Qname.fromURI(area.getNode("ML.isPartOfEnvironmentalELY").getAttribute(RDF_RESOURCE));
				Qname province = Qname.fromURI(area.getNode("ML.isPartOfProvince").getAttribute(RDF_RESOURCE));
				addToAreaSearchMap(searchMap, ely, municipality);
				addToAreaSearchMap(searchMap, province, municipality);
			}
			return searchMap;
		} catch (Exception e) {
			throw new ETLException("Failed to load municipality ely/province data", e);
		}
	}

	private void addToAreaSearchMap(Map<Qname, Set<Qname>> searchMap, Qname area, Qname municipality) {
		if (!searchMap.containsKey(area)) {
			searchMap.put(area, new HashSet<>());
		}
		searchMap.get(area).add(municipality);
	}

	@Override
	public String generateAndStoreApiKey(DownloadRequest request) {
		String prefix = apiKeyPrefix(request);
		String apiKey = prefix+Utils.generateToken(64-prefix.length());
		oracle.storeApiKey(apiKey, request.getId(), request.getApiKeyExpires(), request.getPersonId());
		return apiKey;
	}

	private String apiKeyPrefix(DownloadRequest request) {
		if (request.getDownloadType().equals(DownloadType.AUTHORITIES_API_KEY)) return "pa-";
		if (request.getDownloadType().equals(DownloadType.APPROVED_API_KEY_REQUEST)) return "py-";
		if (request.getDownloadType().equals(DownloadType.AUTHORITIES_VIRVA_GEOAPI_KEY)) return "ga-";
		throw new UnsupportedOperationException("Not yet implemented for " + request.getDownloadType());
	}

	private final Cached<String, Qname> apiKeyRequestCache = new Cached<>(new CacheLoader<String, Qname>() {
		@Override
		public Qname load(String apiKey) {
			Qname requestId = oracle.getApiKeyRequest(apiKey);
			if (requestId == null) throw new IllegalArgumentException("Unknown " + Const.PERMISSION_TOKEN);
			return requestId;
		}
	}, 1, TimeUnit.DAYS, 10000);

	private final Cached<Qname, DownloadRequest> apiKeyDownloadRequestCache = new Cached<>(new CacheLoader<Qname, DownloadRequest>() {
		@Override
		public DownloadRequest load(Qname requestId) {
			return getDownloadRequest(requestId);
		}
	}, 1, TimeUnit.DAYS, Long.MAX_VALUE);

	@Override
	public DownloadRequest getRequestForApiKey(String apiKey) {
		Qname requestId = apiKeyRequestCache.get(apiKey);
		return apiKeyDownloadRequestCache.get(requestId);
	}

	@Override
	public List<DownloadRequest> getApiRequests() {
		List<Pair<String, Qname>> keys = oracle.getApiKeyRequests(null);
		List<DownloadRequest> requests = new ArrayList<>();
		for (Pair<String, Qname> key : keys) {
			DownloadRequest req = apiKeyDownloadRequestCache.get(key.getValue());
			if (req == null) {
				errorReporter.report("Request for api key " + key.getValue() + " is missing");
			} else {
				requests.add(req);
			}
		}
		return requests;
	}

	@Override
	public List<Pair<String, DownloadRequest>> getPersonsApiRequests(Qname personId) {
		List<Pair<String, Qname>> keys = oracle.getApiKeyRequests(personId);
		List<Pair<String, DownloadRequest>> requests = new ArrayList<>();
		for (Pair<String, Qname> key : keys) {
			DownloadRequest req = apiKeyDownloadRequestCache.get(key.getValue());
			if (req == null) {
				errorReporter.report("Request for api key " + key.getValue() + " is missing");
			} else {
				requests.add(new Pair<>(key.getKey(), req));
			}
		}
		return requests;
	}

	private final SingleObjectCache<List<DownloadRequest>> authoritiesDownloadRequestsCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<List<DownloadRequest>>() {
		@Override
		public List<DownloadRequest> load() {
			return getAuthoritiesDownloadRequestsNonCached().stream().filter(r->r.isCompleted()).collect(Collectors.toList());
		}
	}, 1, TimeUnit.HOURS);

	private final Cached<Qname, List<DownloadRequest>> personsAuthoritiesDownloadRequestsCache = new Cached<>(new CacheLoader<Qname, List<DownloadRequest>>() {
		@Override
		public List<DownloadRequest> load(Qname personId) {
			return getPersonsDownloadRequestsNonCached(personId).stream().filter(r->r.isAuthoritiesDownload()).filter(r->r.isCompleted()).collect(Collectors.toList());
		}
	}, 1, TimeUnit.MINUTES, 400);

	@Override
	public List<DownloadRequest> getAuthoritiesDownloadRequests() {
		return authoritiesDownloadRequestsCache.get();
	}

	@Override
	public List<DownloadRequest> getPersonsAuthoritiesDownloadRequests(Qname personId) {
		return personsAuthoritiesDownloadRequestsCache.get(personId);
	}

	private final Cached<String, Long> polygonSearchIdCache = new Cached<>(new CacheLoader<String, Long>() {
		@Override
		public Long load(String wkt) {
			return oracle.getPolygonSearchId(wkt);
		}
	}, 12, TimeUnit.HOURS, 1000);

	@Override
	public long getPolygonSearchId(String wkt) {
		return polygonSearchIdCache.get(wkt);
	}

	private final Cached<Long, String> polygonSearchCache = new Cached<>(new CacheLoader<Long, String>() {
		@Override
		public String load(Long id) {
			return oracle.getPolygonSearch(id);
		}
	}, 12, TimeUnit.HOURS, 1000);

	@Override
	public String getPolygonSearch(long id) {
		return polygonSearchCache.get(id);
	}

}
