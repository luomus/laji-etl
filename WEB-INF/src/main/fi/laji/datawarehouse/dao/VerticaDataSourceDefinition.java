package fi.laji.datawarehouse.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class VerticaDataSourceDefinition {

	public static HikariDataSource initDataSource(ConnectionDescription desc, int maxActive, int longestQueryMinutes) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());

		config.setAutoCommit(false); // transaction mode: all changes must be committed

		config.setConnectionTimeout(60000); // 1 minute
		config.setMaximumPoolSize(maxActive);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(longestQueryMinutes*60*1000);

		return new HikariDataSource(config);
	}

}

