package fi.laji.datawarehouse.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.luomus.commons.taxonomy.Taxon;

public interface VerticaDimensionsDAO {

	public interface VerticaDimensionsPersonService {
		void emptyPersonTempTable() throws Exception;
		void insertToPersonTempTable(PersonBaseEntity e) throws Exception;
		void switchPersonTempToActual() throws Exception;
		void close();
	}


	public interface VerticaDimensionsTaxonService {
		void emptyTaxonTempTable() throws Exception;
		void insertToTaxonTempTable(Taxon taxon) throws Exception;
		void switchTaxonTempToActual() throws Exception;
		void generateInformalGroupLinkings() throws Exception;
		void generateTaxonSetLinkings() throws Exception;
		void generateAdminStatusLinkings() throws Exception;
		void generateOccurrenceTypeLinkings() throws Exception;
		void generateHabitatLinkings() throws Exception;
		void close();
	}

	/**
	 * Will try to update taxon and person table contents
	 * and updates linkings from
	 * - targets to taxa
	 * - userids to persons
	 */
	void startTaxonAndPersonReprosessing() ;

	/**
	 * Update names of those dimensions resources that come from triplestore-api to warehouse tables.
	 * @
	 */
	void updateNameableEntityNames() ;

	/**
	 * Get id of dimension resource by it's internal warehouse key (used by aggregate query response)
	 * @param value
	 * @param field
	 * @return
	 * @
	 */
	String getIdForKey(Long value, String field) ;

	/**
	 * Get key from enum
	 * @param enumeration
	 * @return
	 */
	Long getEnumKey(Enum<?> enumeration);

	/**
	 * Remove historic data etc.
	 * @param statusReporter
	 * @
	 */
	void performCleanUp() ;

	VerticaDimensionsPersonService getPersonSevice();

	VerticaDimensionsTaxonService getTaxonService();

	List<NameableEntity> getEntities(Class<? extends NameableEntity> nameableEntityClass);

	void updateEntities(List<BaseEntity> entityList);

	void insertEntities(List<BaseEntity> entityList);

	void storeDocumentIds(Iterator<String> iterator);

	void storeSplittedDocumentIds(Collection<SplittedDocumentIdEntity> values);

}
