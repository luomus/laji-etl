package fi.laji.datawarehouse.dao;

import java.util.Iterator;
import java.util.List;

import fi.luomus.commons.containers.rdf.Qname;

interface LookupNameProvider {

	public boolean hasNext();

	public LookupName getNext();

	public void refresh() ;

	static class LookupName {
		public enum Type { PRIMARY, SECONDARY }
		private final String name;
		private final Qname id;
		private final LookupName.Type type;
		private boolean isOverriding = false;
		public LookupName(String name, Qname id, LookupName.Type type) {
			this.name = clean(name);
			this.id = id;
			this.type = type;
		}
		private String clean(String name) {
			while (name.contains("  ")) {
				name = name.replace("  ", " ");
			}
			return name.trim();
		}
		public void setToOverride() {
			isOverriding = true;
		}
		public boolean isOverriding() {
			return isOverriding;
		}
		public String getName() {
			return name;
		}
		public Qname getId() {
			return id;
		}
		public boolean isPrimary() {
			return type == Type.PRIMARY;
		}
		@Override
		public String toString() {
			return "LookupName [name=" + name + ", id=" + id + ", type=" + type + ", isOverriding=" + isOverriding + "]";
		}
	}

	public static abstract class BaseLookupNameProvider implements LookupNameProvider {

		private Iterator<LookupName> lookupIterator;

		public void setLookUpStructure(List<LookupName> lookupNames) {
			this.lookupIterator = lookupNames.iterator();
		}

		@Override
		public boolean hasNext() {
			return lookupIterator.hasNext();
		}

		@Override
		public LookupName getNext() {
			return lookupIterator.next();
		}

		protected boolean given(String s) {
			return s != null && s.length() > 0;
		}

	}

}