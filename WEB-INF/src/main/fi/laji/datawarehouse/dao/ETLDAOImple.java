package fi.laji.datawarehouse.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.oracle.Oracle;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OriginalIds;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeStats;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class ETLDAOImple implements ETLDAO {

	private static final int QUEUE_EXPIRE_MIN = 2*24*60; // 2 days
	private static final int QUEUE_EXPIRE_MAX = 4*24*60; // 4 days

	private final String individualIdSecretSalt;
	private final Oracle oracle;
	private final Config config;

	public ETLDAOImple(Config config, Oracle oracle) {
		this.oracle = oracle;
		this.config = config;
		this.individualIdSecretSalt = config.get("IndividualIdSecretSalt");
	}

	@Override
	public void clearReprocessDocumentIds() {
		oracle.clearReprocessDocumentIds();
	}

	@Override
	public void storeReprocessDocumentIds(Iterable<String> documentIds) {
		oracle.storeReprocessDocumentIds(documentIds);
	}

	@Override
	public Qname secureIndividualId(Qname individualId) {
		return Util.secureIndividualId(individualId, individualIdSecretSalt);
	}

	@Override
	public void callKastikkaSync() {
		TransactionConnection con = null;
		CallableStatement s = null;
		try {
			con = new SimpleTransactionConnection(config.connectionDescription("KastikkaPull"));
			s = con.prepareCall("{CALL SyncData}");
			s.execute();
		} catch (Exception e) {
			throw new ETLException(e);
		} finally {
			Utils.close(s, con);
		}
	}

	@Override
	public StatelessSession getETLEntityConnection() {
		return oracle.openSession();
	}

	@Override
	public Connection getETLConnection() {
		return oracle.openConnection();
	}

	// IN/OUT PIPES --------------------------------------------------------------------

	@Override
	public void storeToInPipe(Qname source, List<String> data, String contentType) {
		oracle.storeToInPipe(source, data, contentType, 0);
	}


	@Override
	public void storeToInPipeOnHold(Qname source, List<String> data, String contentType) {
		oracle.storeToInPipe(source, data, contentType, -1);
	}

	@Override
	public void releaseInPipeHold(Qname source) {
		oracle.releaseInPipeHold(source);
	}

	@Override
	public void storeToInPipe(Qname source, String data, String contentType) {
		oracle.storeToInPipe(source, data, contentType);
	}

	@Override
	public boolean storeToOutPipe(List<OutPipeData> data) {
		if (data.isEmpty()) return false;
		return oracle.storeToOutPipe(removeDuplicates(data));
	}

	private Map<String, OutPipeData> removeDuplicates(List<OutPipeData> datas) {
		Map<String, OutPipeData> datasByDocument = new HashMap<>();
		for (OutPipeData data : datas) {
			if (!datasByDocument.containsKey(data.getDocumentId().toURI())) {
				datasByDocument.put(data.getDocumentId().toURI(), data);
			} else {
				OutPipeData duplicate = datasByDocument.get(data.getDocumentId().toURI());
				if (duplicate.getInPipeId() < data.getInPipeId()) {
					datasByDocument.put(data.getDocumentId().toURI(), data);
				}
			}
		}
		return datasByDocument;
	}

	@Override
	public Set<Qname> getUnprosessedSources() {
		return oracle.getUnprosessedSources();
	}

	@Override
	public PipeStats getPipeStats(boolean forceReload) {
		return oracle.getPipeStatistics(forceReload);
	}

	@Override
	public List<InPipeData> getUnprocessedNotErroneousInOrderFromInPipe(Qname source) {
		return oracle.getUnprocessedNotErroneousInOrderFromInPipe(source);
	}

	@Override
	public List<OutPipeData> getUnprocessedNotErroneousInOrderFromOutPipe(Qname source) {
		return oracle.getUnprocessedNotErroneousInOrderFromOutPipe(source);
	}

	@Override
	public void reportAttempted(List<? extends PipeData> data, String errorMessage) {
		if (data.isEmpty()) return;
		if (errorMessage == null) throw new IllegalArgumentException("Error message must be provided when reporting a failed attempt.");
		oracle.reportPipeDataAttempted(data, errorMessage);
	}

	@Override
	public void reportPermantentyFailed(List<InPipeData> data, String errorMessage) {
		if (data.isEmpty()) return;
		if (errorMessage == null) throw new IllegalArgumentException("Error message must be provided when reporting a failed attempt.");
		oracle.reportPermantentyFailed(data, errorMessage);
	}

	@Override
	public void reportProcessed(List<? extends PipeData> data) {
		if (data.isEmpty()) return;
		oracle.reportPipeDataProcessed(data);
	}

	@Override
	public InPipeData getFromInPipe(long id) {
		return oracle.getFromInPipe(id);
	}

	@Override
	public OutPipeData getFromOutPipe(long id) {
		OutPipeData data = oracle.getFromOutPipe(id);
		if (data == null) return null;
		addSplittedDocumentIds(Utils.singleEntryList(data));
		return data;
	}

	@Override
	public OutPipeData getFromOutPipe(Qname documentId) {
		return oracle.getFromOutPipe(documentId);
	}

	@Override
	public List<InPipeData> getFromInPipeForSource(Qname source, PipeSearchParams searchParams) {
		return oracle.getFromInPipeForSource(source, searchParams);
	}

	@Override
	public List<OutPipeData> getFromOutPipeForSource(Qname source, PipeSearchParams searchParams) {
		return addSplittedDocumentIds(oracle.getFromOutPipeForSource(source, searchParams));
	}

	private List<OutPipeData> addSplittedDocumentIds(List<OutPipeData> data) {
		SplittedDocumentIds splitted = oracle.getSplittedDocumentIds(data.stream().map(d->d.getDocumentId()).collect(Collectors.toSet()));
		for (OutPipeData d : data) {
			d.setSplittedDocumentIds(splitted.getSplittedDocumentIdsFor(d.getDocumentId()));
		}
		return data;
	}

	@Override
	public void markReprocessInPipe(long id) {
		oracle.markReprocessInPipe(id);
	}

	@Override
	public void markReprocessOutPipe(long id) {
		oracle.markReprocessOutPipe(id);
	}

	@Override
	public void removeInPipe(long id) {
		oracle.removeInPipe(id);
	}

	@Override
	public void removeOutPipe(long id) {
		oracle.removeOutPipe(id);
	}

	@Override
	public List<InPipeData> getFailedAttemptDataFromInPipe(Qname source) {
		return oracle.getFailedAttemptDataFromInPipe(source);
	}

	@Override
	public List<OutPipeData> getFailedAttemptDataFromOutPipe(Qname source) {
		return oracle.getFailedAttemptDataFromOutPipe(source);
	}

	@Override
	public OutPipeData getSingleMultiAttemptedDataFromOutPipe(Qname source) {
		return oracle.getSingleMultiAttemptedDataFromOutPipe(source);
	}

	@Override
	public int markReprocessOutPipe(Qname documentId) {
		return oracle.markReprocessOutPipe(documentId);
	}

	@Override
	public int markReprocessOutPipeByNamedPlaceIds(List<Qname> namedPlaceIds) {
		return oracle.markReprocessOutPipeByNamedPlaceId(namedPlaceIds);
	}

	@Override
	public void markOutPipeErroneousForReattempt() {
		oracle.markOutPipeErroneousForReattempt();
	}

	@Override
	public Date getLatestTimestampFromOutPipe() {
		return oracle.getLatestTimestampFromOutPipeOfNonDeletion();
	}

	@Override
	public int removeUnlinkedInPipeData(Qname source) {
		return oracle.removeUnlinkedInPipeData(source);
	}

	// SPLITTED DOCUMENT QUEUE --------------------------------------------------------------------

	@Override
	public void storeSplittedDocumentToQueue(Qname source, Qname originalDocumentId, String data) {
		oracle.storeSplittedDocumentToQueue(source, originalDocumentId, data, randomExpire());
	}

	private long randomExpire() {
		if (config.productionMode()) {
			return Util.randomExpire(QUEUE_EXPIRE_MIN, QUEUE_EXPIRE_MAX); // 2 - 4 days
		}
		if (config.stagingMode()) {
			return Util.randomExpire(1, 3); // 1-3 minutes
		}
		return DateUtils.getCurrentEpoch(); // immediate expire in dev
	}

	@Override
	public List<QueueData> getExpiredSplittedDocumentsFromQueue(Qname source) {
		return oracle.getExpiredSplittedDocumentsFromQueue(source);
	}

	@Override
	public void removeSplittedDocumentsFromQueue(Set<Qname> originalDocumentIds) {
		oracle.removeSplittedDocumentsFromQueue(originalDocumentIds);
	}

	@Override
	public void removeSplittedQueue(List<Long> ids) {
		oracle.removeSplittedQueue(ids);
	}

	@Override
	public List<QueueData> getTopSplittedDocumentsFromQueue(int limit) {
		return oracle.getTopSplittedDocumentsFromQueue(limit);
	}

	// SPLITTED IDS --------------------------------------------------------------------

	@Override
	public SplittedDocumentIds getSplittedDocumentIds(Set<Qname> originalDocumentIds) {
		return oracle.getSplittedDocumentIds(originalDocumentIds);
	}

	@Override
	public OriginalIds getOriginalIdsOfSplittedDocument(Qname splittedDocumentId) {
		return oracle.getOriginalIdsOfSplittedDocument(splittedDocumentId);
	}

	@Override
	public Map<String, SplittedDocumentIdEntity> getAllSplittedIdToOriginalId() {
		return oracle.getAllSplittedIdToOriginalId();
	}

	@Override
	public SplittedDocumentIds getOrGenerateSplittedDocumentIds(Qname originalDocumentId, List<Qname> originalUnitIds) {
		SplittedDocumentIds existingSplittedIds = getSplittedDocumentIds(Utils.set(originalDocumentId));
		Map<Qname, Qname> generated = new HashMap<>();
		for (Qname originalUnitId : originalUnitIds) {
			Qname existingSplittedDocumentId = existingSplittedIds.getSplittedDocumentIdFor(originalDocumentId, originalUnitId);
			if (existingSplittedDocumentId == null) {
				Qname generatedSplittedDocumentIdId = generateSplittedDocumentId();
				generated.put(originalUnitId, generatedSplittedDocumentIdId);
				existingSplittedIds.add(originalDocumentId, originalUnitId, generatedSplittedDocumentIdId);
			}
		}
		oracle.storeGeneratedSplittedIds(originalDocumentId, generated);
		return existingSplittedIds;
	}

	private Qname generateSplittedDocumentId() {
		String guid = Utils.generateGUID();
		return new Qname(Const.SPLITTED_DOCUMENT_ID_QNAME_NAMESPACE_PREFIX + guid);
	}

	// FIRST LOAD TIMES --------------------------------------------------------------------

	@Override
	public void storeFirstLoadedTimes(Map<Qname, Long> firstLoadTimesOfDocumentIds) {
		oracle.storeFirstLoadedTimes(firstLoadTimesOfDocumentIds);
	}

	@Override
	public Map<Qname, Long> getFirstLoadTimes(List<Document> documents) {
		if (documents.isEmpty()) Collections.emptyMap();
		Set<Qname> ids = documents.stream().map(d->d.getDocumentId()).collect(Collectors.toSet());
		return oracle.getFirstLoadTimes(ids);
	}

	// PULL API SEQUENCES --------------------------------------------------------------------

	@Override
	public Integer getLastReadPullApiEntrySequenceFor(Qname source) {
		String v = oracle.getPersisted(pullKey(source));
		if (v == null) return null;
		return Integer.valueOf(v);
	}

	private String pullKey(Qname source) {
		return "pullsequence_"+source;
	}

	@Override
	public void setLastReadPullApiEntrySequence(Qname source, int sequence) {
		oracle.persist(pullKey(source), Integer.toString(sequence));
	}

	// ANNOTATIONS --------------------------------------------------------------------

	@Override
	public Map<Qname, List<Annotation>> getAnnotations(Set<Qname> documentIds) {
		return oracle.getAnnotations(documentIds);
	}

	@Override
	public Set<Qname> getAnnotatedDocumenIds() {
		return oracle.getAnnotatedDocumentIds();
	}

	@Override
	public void storeAnnotation(Annotation annotation, long inPipeId) {
		oracle.storeAnnotation(annotation, inPipeId);
	}

	@Override
	public Long getExistingAnnotationInPipeId(Qname annotationId) {
		return oracle.getExistingAnnotationInPipeId(annotationId);
	}

	@Override
	public List<Annotation> getTopAnnotations(int limit) {
		return oracle.getTopAnnotations(limit);
	}

	// NOTIFICATIONS --------------------------------------------------------------------

	@Override
	public void storeNotification(AnnotationNotification notification) {
		oracle.storeNotification(notification);
	}

	@Override
	public void markNotificationSent(long id) {
		oracle.markNotificationSent(id);
	}

	@Override
	public List<AnnotationNotification> getUnsentNotifications() {
		return oracle.getUnsentNotifications();
	}

	@Override
	public void removeUnsentNotifications(Qname annotationId) {
		oracle.removeUnsentNotifications(annotationId);
	}

	@Override
	public List<AnnotationNotification> getTopSentNotifications(int limit) {
		return oracle.getTopSentNotifications(limit);
	}

	@Override
	public ResultStream<String> getAllDocumentIds() {
		return oracle.getAllDocumentIds();
	}

}
