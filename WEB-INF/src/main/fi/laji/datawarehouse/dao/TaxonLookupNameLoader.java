package fi.laji.datawarehouse.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.LookupNameProvider.BaseLookupNameProvider;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;

public class TaxonLookupNameLoader extends BaseLookupNameProvider {

	private final TaxonLookupNameGenerator taxonLookupNameGenerator = new TaxonLookupNameGenerator();
	private final DAO dao;
	private final ThreadStatuses threadStatuses;

	public TaxonLookupNameLoader(DAO dao, ThreadStatuses threadStatuses) {
		this.dao = dao;
		this.threadStatuses = threadStatuses;
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created");
	}

	@Override
	public void refresh() {
		long startMoment = DateUtils.getCurrentEpoch();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting taxon lookup refresh... (start moment "+startMoment+")");
		ThreadStatusReporter reporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
		try {
			reporter.setStatus("Get roots");
			Collection<Taxon> roots = getTaxonTreeRoots();

			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "... generating lookup names ...");

			List<LookupName> lookupNames = new ArrayList<>();

			for (Taxon root : roots) {
				reporter.setStatus("Generating lookup for " + root.getChecklist());
				lookupNames.addAll(generateLookupStructure(root));
			}

			setLookUpStructure(lookupNames);
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), ""+startMoment, e);
			throw new ETLException("Refreshing taxon lookup", e);
		} finally {
			threadStatuses.reportThreadDead(this.getClass());
		}
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon lookup refresh completed! (start moment "+startMoment+")");
	}

	private Collection<Taxon> getTaxonTreeRoots() throws Exception {
		return dao.getTaxonomyDAO().getTaxonContainer().getAll().stream().filter(t -> t.getChecklist() != null && !t.hasParent()).collect(Collectors.toList());
	}

	private List<LookupName> generateLookupStructure(Taxon root) throws Exception {
		List<LookupName> lookupNames = new ArrayList<>(root.getCountOfChildren());
		handleTaxonAndChildren(root, lookupNames);
		return lookupNames;
	}

	private void handleTaxonAndChildren(Taxon taxon, List<LookupName> lookupNames) throws Exception {
		taxonLookupNameGenerator.generateLookupNames(taxon, lookupNames);
		for (Taxon child : taxon.getChildren()) {
			handleTaxonAndChildren(child, lookupNames);
		}
	}

}
