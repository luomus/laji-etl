package fi.laji.datawarehouse.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.LookupNameProvider.LookupName;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.containers.rdf.Qname;

class LookupLoader {

	private final LookupNameProvider nameProvider;
	private final List<String> warnings = new ArrayList<>();
	private final List<String> errors = new ArrayList<>();

	public LookupLoader(LookupNameProvider nameProvider) {
		this.nameProvider = nameProvider;
	}

	public Map<String, Qname> load() {
		tryToRefreshNameProvider();
		Map<String, Qname> lookupStructure = generateLookupStructureUsingNameProvider();
		return lookupStructure;
	}

	private Map<String, Qname> generateLookupStructureUsingNameProvider() {
		Map<String, LookupName> primaryLookup = new HashMap<>();
		Map<String, LookupName> secondaryLookup = new HashMap<>();
		Set<String> primaryDuplicates = new HashSet<>();
		Set<String> secondaryDuplicates = new HashSet<>();

		while (nameProvider.hasNext()) {
			LookupName name = nameProvider.getNext();
			if (name.isPrimary()) {
				handle(primaryLookup, primaryDuplicates, name);
			} else {
				handle(secondaryLookup, secondaryDuplicates, name);
			}
		}

		Set<String> primaryDuplicateNames = new HashSet<>();
		for (String name : primaryDuplicates) {
			primaryLookup.remove(name);
			secondaryLookup.remove(name);
			primaryDuplicateNames.add(lookupToName(name));
		}
		if (!primaryDuplicateNames.isEmpty()) {
			warnings.add("Primary duplicate names: " + toString(primaryDuplicateNames));
		}

		Set<String> secondaryDuplicateNames = new HashSet<>();
		for (String name : secondaryDuplicates) {
			secondaryLookup.remove(name);
			secondaryDuplicateNames.add(lookupToName(name));
		}
		if (!secondaryDuplicateNames.isEmpty()) {
			warnings.add("Secondary duplicate names: " + toString(secondaryDuplicateNames));
		}

		for (String name : secondaryLookup.keySet()) {
			if (!primaryLookup.containsKey(name)) {
				primaryLookup.put(name, secondaryLookup.get(name));
			}
		}

		Map<String, Qname> nameToKeyMap = new HashMap<>();
		for (Map.Entry<String, LookupName> e : primaryLookup.entrySet()) {
			String name = e.getKey();
			LookupName lookupName = e.getValue();
			nameToKeyMap.put(name, lookupName.getId());
		}

		return nameToKeyMap;
	}

	private String toString(Set<String> names) {
		return new ArrayList<>(names).stream().sorted().collect(Collectors.joining("; "));
	}

	private String lookupToName(String name) {
		return name.replace(":null", "").replace(":"+TaxonLinkingService.COUNTRY_FI, "");
	}

	private void handle(Map<String, LookupName> lookup, Set<String> duplicates, LookupName name) {
		LookupName existing = lookup.get(name.getName());
		if (existing == null) {
			lookup.put(name.getName(), name);
			return;
		}
		if (name.isOverriding() && !existing.isOverriding()) {
			lookup.put(name.getName(), name);
			return;
		}
		if (existing.isOverriding() && !name.isOverriding()) {
			return;
		}
		if (nameIsNotForSameIdAsExisting(existing, name)) {
			duplicates.add(name.getName());
			if (existing.isOverriding() && name.isOverriding()) {
				errors.add("Two overriding names [name: " + name.getName() + " ids: " + existing.getId() + ", " + name.getId()+"]");
			}

		}
	}

	private void tryToRefreshNameProvider() {
		try {
			nameProvider.refresh();
		} catch (Exception e) {
			throw new ETLException("Refreshing names of name provider", e);
		}
	}

	private boolean nameIsNotForSameIdAsExisting(LookupName existing, LookupName name) {
		return !existing.getId().equals(name.getId());
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public List<String> getErrors() {
		return errors;
	}

}