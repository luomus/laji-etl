package fi.laji.datawarehouse.dao;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;

public interface DAO extends AutoCloseable, MediaDAO {

	/**
	 * Get sources
	 * @return
	 * @
	 */
	Map<String, Source> getSources();

	/**
	 * Get finnish municipalities by name
	 * @param reportedMunicipality
	 * @return
	 * @
	 */
	List<Area> resolveMunicipalitiesByName(String reportedMunicipality);

	/**
	 * Get old finnish municipalities by name
	 * @param reportedMunicipality
	 * @return
	 * @
	 */
	List<Area> resolveOldMunicipalitiesByName(String reportedMunicipality);

	/**
	 * Get biogeographical provinces by name
	 * @param reportedProvince
	 * @return null of not found, id if single match found
	 * @
	 */
	List<Area> resolveBiogeographicalProvincesByName(String reportedProvince);

	/**
	 * Get countries by name
	 * @param reportedCountry
	 * @return null of not found, id if single match found
	 * @
	 */
	List<Area> resolveCountiesByName(String reportedCountry);

	/**
	 * Get all finnish municipality ids that touch the given bounding box
	 * @param wgs84Coordinates
	 * @return ids or empty list
	 * @
	 */
	Set<Qname> getFinnishMunicipalities(Coordinates wgs84Coordinates);

	/**
	 * Get all finnish municipality ids that touch the given geometry
	 * @param wgs84Geo
	 * @return
	 */
	Set<Qname> getFinnishMunicipalities(Geo wgs84Geo);

	/**
	 * Get all natura 2000 areas that touch the given bounding box
	 * @param wgs84Coordinates
	 * @return
	 * @
	 */
	Set<Qname> getNaturaAreas(Coordinates wgs84Coordinates);

	/**
	 * Get all biogeographical province ids that touch the given bounding box
	 * @param wgs84Coordinates
	 * @return ids or empty list
	 * @
	 */
	Set<Qname> getBiogeographicalProvinces(Coordinates wgs84Coordinates);

	/**
	 * Get all biogeographical province ids that touch the given geometry
	 * @param wgs84Geo
	 * @return
	 */
	Set<Qname> getBiogeographicalProvinces(Geo wgs84Geo);

	/**
	 * Get all natura area ids that touch the given geometry
	 * @param wgs84geo
	 * @return
	 */
	Set<Qname> getNaturaAreas(Geo wgs84geo);

	/**
	 * Get municipality bounding box (wgs84) by municipality id
	 * @param finnishMunicipality
	 * @return wgs84 coordinates
	 * @
	 */
	Coordinates getCoordinatesByFinnishMunicipality(Qname finnishMunicipality);

	/**
	 * Get warehouse dao that connects to the private warehouse
	 * @return
	 * @
	 */
	VerticaDAO getPrivateVerticaDAO();

	/**
	 * Get warehouse dao that connects to the public warehouse
	 * @return
	 * @
	 */
	VerticaDAO getPublicVerticaDAO();

	/**
	 * Get dimensions dao shared by public and private warehouse
	 * @return
	 * @
	 */
	VerticaDimensionsDAO getVerticaDimensionsDAO();

	/**
	 * Get ETL dao
	 * @return
	 */
	ETLDAO getETLDAO();

	/**
	 * Log error to log table
	 * @param source source information system id
	 * @param phase process class
	 * @param identifier document id or other identifier
	 * @param exception the exception
	 */
	void logError(Qname source, Class<?> phase, String identifier, Throwable exception);

	/**
	 * Log error to log table
	 * @param source source information system id
	 * @param phase process class
	 * @param identifier document id or other identifier
	 * @param message additional information
	 * @param exception the exception
	 */
	void logError(Qname source, Class<?> phase, String identifier, String message, Throwable exception);

	/**
	 * Log message to log table
	 * @param source
	 * @param phase
	 * @param message
	 */
	void logMessage(Qname source, Class<?> phase, String message);

	/**
	 * Log message to log table
	 * @param source
	 * @param phase
	 * @param identifier
	 * @param message
	 */
	void logMessage(Qname source, Class<?> phase, String identifier, String message);

	/**
	 * Get top entries from log table
	 * @return
	 * @
	 */
	List<LogEntry> getLogEntries();

	/**
	 * Get taxon by id
	 * @param qname
	 * @return
	 * @throws NoSuchTaxonException
	 */
	Taxon getTaxon(Qname qname);

	/**
	 * Has taxon by id
	 * @param qname
	 * @return
	 */
	boolean hasTaxon(Qname qname);

	/**
	 * Get target names from warehouse that are not linked to any taxon. Ordered by count of uses in the warehouse (most used first). Returns only some of the first results.
	 * @return
	 * @
	 */
	List<UnlinkedTargetNameData> getUnlinkedTargetNames();

	/**
	 * Get usernames from warehouse that are not linked to any person. Ordered by count of uses in the warehouse (most used first). Returns only some of the first results.
	 * @return
	 */
	List<UnlinkedUserIdsData> getUnlinkedUserIds();

	/**
	 * Start taxon loading again etc
	 */
	void clearCachesStartReload();

	/**
	 * Get collections
	 * @return
	 * @
	 */
	Map<String, CollectionMetadata> getCollections();

	/**
	 * Get ids of collections that are allowed for statistics queries
	 * @return
	 */
	Set<Qname> getStatisticsAllowedCollections();

	/**
	 * Get areas
	 * @return
	 * @
	 */
	Map<Qname, Area> getAreas();

	/**
	 * Get areas, reload cache
	 * @return
	 */
	Map<Qname, Area> getAreasForceReload();

	/**
	 * Get identifiers of municipalities part of ELY centre
	 * @param elyId
	 * @return
	 */
	Set<Qname> getMunicipalitiesPartOfELY(Qname elyId);

	/**
	 * Get identifiers of municipalities part of province
	 * @param elyId
	 * @return
	 */
	Set<Qname> getMunicipalitiesPartOfProvince(Qname provinceId);

	/**
	 * Log warehouse search query
	 * @param query
	 */
	<T extends BaseQuery> void logQuery(T query);

	/**
	 * Persist logged queries to ETL database log table
	 * @return count of stored queries
	 */
	long persistQueryLogToLogTable();

	/**
	 * Get references
	 * @return
	 * @
	 */
	Map<String, String> getReferenceDescriptions();

	/**
	 * Get information about api access token
	 * @param accessToken
	 * @return
	 * @
	 */
	ApiUser getApiUser(String accessToken);

	TaxonomyDAO getTaxonomyDAO();

	/**
	 * Get labels for schema.laji.fi property
	 * @param propertyQname
	 * @return
	 * @
	 */
	LocalizedText getLabels(Qname propertyQname);


	/**
	 * Checks if given resource identifier is a enumeration value of some alt range
	 * @param enumerationValue
	 * @return
	 */
	boolean isValidEnumeration(Qname enumerationValue);

	/**
	 * Store a new download request to be handled later.
	 * @param request
	 * @
	 */
	void storeDownloadRequest(DownloadRequest request);

	/**
	 * Get uncompleted download requests
	 * @return
	 */
	List<DownloadRequest> getUncompletedDownloadRequests();

	/**
	 * Get single download request
	 * @param id
	 * @return
	 */
	DownloadRequest getDownloadRequest(Qname id);

	/**
	 * Notify approval service of new private file download request
	 * @
	 */
	void sendDownloadRequestForApproval(JSONObject json);

	/**
	 * Generate and store API key that has been created for this request
	 * @param request
	 * @return
	 */
	String generateAndStoreApiKey(DownloadRequest request);

	/**
	 * Get original request information for apiKey
	 * @param apiKey
	 * @return
	 */
	DownloadRequest getRequestForApiKey(String apiKey);

	/**
	 * Get all granted api keys
	 * @return
	 */
	List<DownloadRequest> getApiRequests();

	/**
	 * Get persons all granted api keys
	 * @param personId
	 * @return api keys and request for that api key
	 */
	List<Pair<String, DownloadRequest>> getPersonsApiRequests(Qname personId);

	/**
	 * Get all created authority portal downloads
	 * @return
	 */
	List<DownloadRequest> getAuthoritiesDownloadRequests();

	/**
	 * Get persons created authority portal downloads
	 * @param personId
	 * @return
	 */
	List<DownloadRequest> getPersonsAuthoritiesDownloadRequests(Qname personId);

	/**
	 * Notify approval service that a prive file download has been completed
	 * @param id of the request
	 * @
	 */
	void notifyApprovalServiceOfCompletion(Qname id);

	List<DownloadRequest> getDownloadRequestsFromToday();

	List<DownloadRequest> getFailedDownloadRequests();

	/**
	 * Send notification
	 * @param notification
	 */
	void sendNotification(AnnotationNotification notification);
	/**
	 * Get next triplestore sequence value
	 * @param qnamePrefix
	 * @return
	 */
	Qname getSeqNextVal(String qnamePrefix);

	/**
	 * Map from usernames to person Ids. Person ids are URIs or in form lintuvaara:123 etc.
	 * @return
	 */
	Map<String, Qname> getPersonLookupStructure(); // TODO do the same for personlookp as taxon -> personlinkinservice

	/**
	 * Get schema.laji.fi context definitions
	 * @return
	 */
	ContextDefinitions getContextDefinitions();

	/**
	 * Get ids of units of which an early warning has been submitted
	 * @return
	 */
	Set<Qname> getInvasiveSpeciesEarlyWarningReportedUnits();

	/**
	 * Set ids of units of which an early warning has been submitted
	 * @param unitIds
	 */
	void setInvasiveSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds);

	/**
	 * Get ids of units of which an early warning has been submitted
	 * @return
	 */
	Set<Qname> getPestSpeciesEarlyWarningReportedUnits();

	/**
	 * Set ids of units of which an early warning has been submitted
	 * @param unitIds
	 */
	void setPestSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds);

	Map<Qname, NamedPlaceEntity> getNamedPlaces();

	Map<Qname, NamedPlaceEntity> getNamedPlacesForceReload();

	Map<String, String> getFormNames();

	ErrorReporter getErrorReporter();

	/**
	 * Get GBIF dataset endpoints that have been defined to be synced to FinBIF
	 * @return identifier of the dataset and endpoint URIs
	 */
	Collection<Pair<Qname, Collection<URI>>> getGBIFDatasetEndpoints();

	/**
	 * Report exception, wrap to runnable and return wrapped
	 * @param identifier
	 * @param message
	 * @param e
	 * @return
	 */
	RuntimeException exceptionAndReport(String identifier, String message, Exception e);

	/**
	 * Has person exceeded his/her download limit?
	 * @param personId
	 * @return
	 */
	boolean exceedsDownloadLimit(Qname personId);

	/**
	 * Has person exceeded his/her polygon search limit?
	 * @param personId
	 * @return
	 */
	boolean exceedsPolygonSearchLimit(Qname personId);

	/**
	 * Clean up person download limits
	 */
	void clearPersonDailyLimits();

	PersonInfo getPerson(Qname personId);

	PersonInfo getPerson(String personToken);

	Collection<PersonInfo> getPersons();

	Qname getPersonTargetSystem(String personToken);

	/**
	 * Stores current public and private dw unit counts to log table
	 */
	void logOccurrenceCounts();

	/**
	 * Store data to key value storage
	 * @param key
	 * @param data
	 */
	void persist(String key, String data);

	/**
	 * Get previously persisted data from key value storage
	 * @param key
	 * @return or null if empty
	 */
	String getPersisted(String key);

	@Override void close();

	TaxonLinkingService getTaxonLinkingService();

	/**
	 * Get all taxon concept ids that match the targetName, including conflicting etc. Used to find out if any meaning of the name could be about a taxon concept that needs to be secured.
	 * @param targetName
	 * @return
	 */
	Set<Qname> getAllTaxonMatches(String targetName);

	/**
	 * Gets all taxa that have a set secure level
	 * @return
	 */
	List<Taxon> getTaxaWithSecureLevels();

	ExecutorService getSharedThreadPool();

	/**
	 * Returns id of the given polygon. Polygon search filter takes these ids as parameter.
	 * @param wkt - the polygon as WKT
	 * @return
	 */
	long getPolygonSearchId(String wkt);

	/**
	 * Returns polygon as WKT by id. Polygon search filter takes these ids as parameter.
	 * @param id
	 * @return
	 */
	String getPolygonSearch(long id);

	/**
	 * Return bird association area id by ykj 10km grid, given in format 666:333
	 * @param ykjGridString
	 * @return
	 */
	Qname getBirdAssociationArea(String ykjGridString);

}
