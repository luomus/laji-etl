package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.utils.Util;

@Entity
@Table(name="team")
@AttributeOverride(name="id", column=@Column(name="teamlookup"))
class TeamEntity extends BaseEntity {

	private List<Long> agentKeys = null;
	private List<String> agentNames = null;

	public TeamEntity() {}

	public TeamEntity(List<Long> agentKeys, List<String> agentNames) {
		this.agentKeys = agentKeys;
		this.agentNames = agentNames;
		setId(agentKeys.toString());
	}

	public List<Long> revealAgentKeys() {
		return agentKeys;
	}

	public Integer getAgentCount() {
		return agentKeys.size();
	}

	public void setAgentCount(@SuppressWarnings("unused") Integer count) {
		// for hibernate
	}

	@Column(name="team_text")
	public String getTeamText() {
		return Util.trimToByteLength(Util.catenate(agentNames), 1000);
	}

	public void setTeamText(@SuppressWarnings("unused") String teamText) {
		// for hibernate
	}

	@OneToMany(mappedBy="team", fetch=FetchType.LAZY)
	public Set<TeamAgent> getTeamAgents() {
		return null; // for queries only
	}

	public void setTeamAgents(@SuppressWarnings("unused") Set<TeamAgent> teamAgents) {
		// for queries only
	}

}