package fi.laji.datawarehouse.dao.vertica;

import java.util.Collection;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO.VerticaDimensionsTaxonService;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;

public class TaxonReload {

	private final DAO dao;
	private final ThreadStatuses threadStatuses;

	public TaxonReload(DAO dao, ThreadStatuses threadStatuses) {
		this.dao = dao;
		this.threadStatuses = threadStatuses;
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created");
	}

	public void reload() {
		long startMoment = DateUtils.getCurrentEpoch();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting taxon reload to Vertica... (start moment "+startMoment+")");
		ThreadStatusReporter reporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
		VerticaDimensionsTaxonService service = null;
		try {
			reporter.setStatus("Get taxon service");
			service = dao.getVerticaDimensionsDAO().getTaxonService();

			reporter.setStatus("Get roots");
			Collection<Taxon> roots = getTaxonTreeRoots();

			reporter.setStatus("Emptying temp taxon table");
			service.emptyTaxonTempTable();

			logAndThreadStatus("Loading taxa to temp table", reporter);
			for (Taxon root : roots) {
				reporter.setStatus("Start inserting taxa of checklist " + root.getChecklist() + " to temp");
				loadTaxaToTempTable(root, service, reporter);
			}

			logAndThreadStatus("Switching temp and actual", reporter);
			service.switchTaxonTempToActual();

			logAndThreadStatus("Informal group linkings", reporter);
			service.generateInformalGroupLinkings();

			logAndThreadStatus("Taxon set linkings", reporter);
			service.generateTaxonSetLinkings();

			logAndThreadStatus("Admin status linkings", reporter);
			service.generateAdminStatusLinkings();

			logAndThreadStatus("Occurrence type linkings", reporter);
			service.generateOccurrenceTypeLinkings();

			logAndThreadStatus("Habitat linkings...", reporter);
			service.generateHabitatLinkings();
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), ""+startMoment, e);
			dao.getErrorReporter().report("Taxon reload to Vertica failed", e);
			throw new ETLException("Taxon reload to Vertica failed", e);
		} finally {
			if (service != null) service.close();
			threadStatuses.reportThreadDead(this.getClass());
		}
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon reload to Vertica completed! (start moment "+startMoment+")");
	}

	private void logAndThreadStatus(String message, ThreadStatusReporter reporter) {
		reporter.setStatus(message);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), message);
	}

	private Collection<Taxon> getTaxonTreeRoots() throws Exception {
		return dao.getTaxonomyDAO().getTaxonContainer().getAll().stream().filter(t -> t.getChecklist() != null && !t.hasParent()).collect(Collectors.toList());
	}

	private static class Counter {
		private final int total;
		private int i = 0;
		private String toString;
		public Counter(int total) {
			this.total = total;
			toString = " (0/"+total+")";
		}
		public void increase(int amount) {
			i += amount;
			toString = " ("+i+"/"+total+")";
		}
		@Override
		public String toString() {
			return toString;
		}
	}

	private void loadTaxaToTempTable(Taxon root, VerticaDimensionsTaxonService service, ThreadStatusReporter reporter) throws Exception {
		Counter counter = new Counter(root.getCountOfChildren());
		handleTaxonAndChildren(root.getChecklist(), root, service, reporter, counter);
	}

	private void handleTaxonAndChildren(Qname rootChecklist, Taxon taxon, VerticaDimensionsTaxonService service, ThreadStatusReporter reporter, Counter counter) throws Exception {
		reporter.setStatus("Iterating " + taxon.getQname().toString() + counter);
		if (!rootChecklist.equals(taxon.getChecklist())) {
			// No need to load synonyms or taxa from different checklist added to the checklist (Pinkka taxa TODO remove this after Pinkka taxa no longer in master checklist)
			counter.increase(taxon.getCountOfChildren() + 1);
			return;
		}
		counter.increase(1);

		reporter.setStatus("Inserting " + taxon.getQname().toString() + counter);
		service.insertToTaxonTempTable(taxon);
		reporter.setStatus(taxon.getQname().toString() + " inserted" + counter);

		for (Taxon child : taxon.getChildren()) {
			handleTaxonAndChildren(rootChecklist, child, service, reporter, counter);
		}
	}

}
