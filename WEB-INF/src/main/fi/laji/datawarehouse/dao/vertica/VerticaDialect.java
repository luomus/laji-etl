package fi.laji.datawarehouse.dao.vertica;

import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.PostgreSQL9Dialect;
import org.hibernate.dialect.identity.GetGeneratedKeysDelegate;
import org.hibernate.dialect.identity.IdentityColumnSupport;
import org.hibernate.id.PostInsertIdentityPersister;

public class VerticaDialect extends PostgreSQL9Dialect {

	private static final String SELECT_LAST_INSERT_ID = "SELECT LAST_INSERT_ID()";
	
	@Override
	public IdentityColumnSupport getIdentityColumnSupport() {
		final IdentityColumnSupport parentDialectIdentityColumnSupport = super.getIdentityColumnSupport();
		return new IdentityColumnSupport() {
			@Override
			public boolean supportsInsertSelectIdentity() {
				return false;
			}
			
			@Override
			public boolean supportsIdentityColumns() {
				return false;
			}
			
			@Override
			public boolean hasDataTypeInIdentityColumn() {
				return parentDialectIdentityColumnSupport.hasDataTypeInIdentityColumn();
			}
			
			@Override
			public String getIdentitySelectString(String arg0, String arg1, int arg2) throws MappingException {
				return SELECT_LAST_INSERT_ID;
			}
			
			@Override
			public String getIdentityInsertString() {
				return parentDialectIdentityColumnSupport.getIdentityInsertString();
			}
			
			@Override
			public String getIdentityColumnString(int arg0) throws MappingException {
				return parentDialectIdentityColumnSupport.getIdentityColumnString(arg0);
			}
			
			@Override
			public GetGeneratedKeysDelegate buildGetGeneratedKeysDelegate(PostInsertIdentityPersister arg0, Dialect arg1) {
				return parentDialectIdentityColumnSupport.buildGetGeneratedKeysDelegate(arg0, arg1);
			}
			
			@Override
			public String appendIdentitySelectToInsert(String arg0) {
				return parentDialectIdentityColumnSupport.appendIdentitySelectToInsert(arg0);
			}
		};
	}

}