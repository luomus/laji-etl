package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gathering_userid")
class GatheringUserId implements Serializable {

	private static final long serialVersionUID = -5158881277780827081L;
	private Long gatheringKey;
	private Long userIdKey;

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return gatheringKey;
	}
	public void setGatheringKey(Long gatheringKey) {
		this.gatheringKey = gatheringKey;
	}

	@Id @Column(name="userid_key")
	public Long getUserIdKey() {
		return userIdKey;
	}
	public void setUserIdKey(Long userIdKey) {
		this.userIdKey = userIdKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="userid_key", referencedColumnName="key", insertable=false, updatable=false)
	public UserIdEntity getUserId() { // for queries only
		return null;
	}

	public void setUserId(@SuppressWarnings("unused") UserIdEntity userId) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

}
