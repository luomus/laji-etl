package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="sample_fact")
class SampleFact extends FactLink {

	private static final long serialVersionUID = 8402840089125246985L;

	public SampleFact(Fact fact) {
		super(fact);
	}

	@Deprecated
	public SampleFact() {}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", insertable=false, updatable=false)
	public SampleEntity getSample() { // for queries only
		return null;
	}

	public void setSample(@SuppressWarnings("unused") SampleEntity sample) {
		// for queries only
	}

}
