package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;

import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.Unit;

@Entity
@Table(name="unit_media")
public class UnitMediaEntity {

	private MediaObject mediaObject;
	private Unit parentUnit;
	private Gathering parentGathering;
	private Document parentDocument;
	private DocumentForeignKeys documentForeignKeys;
	private GatheringForeignKeys gatheringForeignKeys;
	private UnitForeignKeys foreignKeys;
	private UnitMediaForeignKeys mediaForeignKeys = new UnitMediaForeignKeys();
	private Long aggregateRowCount;
	private Long securedCount;

	public UnitMediaEntity() {}

	public UnitMediaEntity(UnitEntity unitEntity, MediaObject media) {
		this.setMediaObject(media);
		this.parentUnit = unitEntity.getUnit();
		this.parentGathering = unitEntity.getParentGathering();
		this.parentDocument = unitEntity.getParentDocument();
		this.documentForeignKeys = unitEntity.getDocumentForeignKeys();
		this.gatheringForeignKeys = unitEntity.getGatheringForeignKeys();
		this.foreignKeys = unitEntity.getForeignKeys();
		mediaForeignKeys.setUnitKey(unitEntity.getKey());
		mediaForeignKeys.setUnitId(unitEntity.getId());
	}

	@Formula(value="count(*) over()")
	public Long getAggregateRowCount() {
		return aggregateRowCount;
	}

	public void setAggregateRowCount(Long aggregateRowCount) {
		this.aggregateRowCount = aggregateRowCount;
	}

	@Formula(value="count(CASE WHEN secured = true THEN 1 ELSE NULL END)")
	public Long getSecuredCount() {
		return securedCount;
	}

	public void setSecuredCount(Long securedCount) {
		this.securedCount = securedCount;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="notes", column=@Column(insertable=false, updatable=false))
	})
	public Gathering getParentGathering() {
		return parentGathering;
	}

	public void setParentGathering(Gathering gathering) {
		this.parentGathering = gathering;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="notes", column=@Column(insertable=false, updatable=false)),
		@AttributeOverride(name="partial", column=@Column(insertable=false, updatable=false))
	})
	public Document getParentDocument() {
		return parentDocument;
	}

	public void setParentDocument(Document parentDocument) {
		this.parentDocument = parentDocument;
	}

	@Embedded
	public UnitMediaForeignKeys getMediaForeignKeys() {
		return mediaForeignKeys;
	}

	public void setMediaForeignKeys(UnitMediaForeignKeys foreignKeys) {
		this.mediaForeignKeys = foreignKeys;
	}

	@Embedded
	public DocumentForeignKeys getDocumentForeignKeys() {
		return documentForeignKeys;
	}

	public void setDocumentForeignKeys(DocumentForeignKeys documentForeignKeys) {
		this.documentForeignKeys = documentForeignKeys;
	}

	@Embedded
	public GatheringForeignKeys getGatheringForeignKeys() {
		return gatheringForeignKeys;
	}

	public void setGatheringForeignKeys(GatheringForeignKeys gatheringForeignKeys) {
		this.gatheringForeignKeys = gatheringForeignKeys;
	}

	@Embedded
	public UnitForeignKeys getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(UnitForeignKeys unitForeignKeys) {
		this.foreignKeys = unitForeignKeys;
	}

	@Embedded
	public Unit getParentUnit() {
		return parentUnit;
	}

	public void setParentUnit(Unit unit) {
		this.parentUnit = unit;
	}

	@EmbeddedId
	@Embedded
	public MediaObject getMediaObject() {
		return mediaObject;
	}

	public void setMediaObject(MediaObject mediaObject) {
		this.mediaObject = mediaObject;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="target_key", referencedColumnName="key", insertable=false, updatable=false)
	public TargetEntity getTarget() {  // for queries only
		return null;
	}

	public void setTarget(@SuppressWarnings("unused") TargetEntity target) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="original_target_key", referencedColumnName="key", insertable=false, updatable=false)
	public TargetEntity getOriginalTarget() {  // for queries only
		return null;
	}

	public void setOriginalTarget(@SuppressWarnings("unused") TargetEntity target) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="namedplace_id", referencedColumnName="id", insertable=false, updatable=false)
	public NamedPlaceEntity getNamedPlace() {
		return null; // for queries only
	}

	public void setNamedPlace(@SuppressWarnings("unused") NamedPlaceEntity e) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="collection_key", referencedColumnName="key", insertable=false, updatable=false)
	public CollectionEntity getCollection() {
		return null; // for queries only
	}

	public void setCollection(@SuppressWarnings("unused") CollectionEntity e) {
		// for hibernate query only
	}

}
