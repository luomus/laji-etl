package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit_document_securereason")
class UnitDocumentSecureReason implements Serializable {

	private static final long serialVersionUID = -5788558488980970371L;

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return null;
	}
	public void setUnitKey(@SuppressWarnings("unused") Long key) {

	}

	@Id @Column(name="securereason_key")
	public Long getSecureReasonKey() {
		return null;
	}
	public void setSecureReasonKey(@SuppressWarnings("unused") Long secureReasonKey) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
