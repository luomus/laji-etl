package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gathering_document_securereason")
class GatheringDocumentSecureReason implements Serializable {

	private static final long serialVersionUID = -3167277991280398464L;

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return null;
	}
	public void setGatheringKey(@SuppressWarnings("unused") Long key) {

	}

	@Id @Column(name="securereason_key")
	public Long getSecureReasonKey() {
		return null;
	}
	public void setSecureReasonKey(@SuppressWarnings("unused") Long secureReasonKey) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
