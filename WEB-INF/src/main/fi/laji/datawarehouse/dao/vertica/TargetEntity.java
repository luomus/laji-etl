package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;

@Entity
@Table(name="target")
@AttributeOverride(name="id", column=@Column(name="ORIGINAL_REFERENCE"))
public class TargetEntity extends BaseEntity {

	private Long taxonKey;
	private Long referenceKey;
	private String targetlookup;
	private String targetLowercase;
	private Boolean notExactTaxonMatch;

	public TargetEntity() {}

	@Transient
	public boolean resolvedSameAs(TargetEntity other) {
		if (other == null) throw new IllegalArgumentException("Null given");
		if (taxonKey == null) {
			if (other.taxonKey != null)
				return false;
		} else if (!taxonKey.equals(other.taxonKey))
			return false;
		if (notExactTaxonMatch == null) {
			if (other.notExactTaxonMatch != null)
				return false;
		} else if (!notExactTaxonMatch.equals(other.notExactTaxonMatch))
			return false;
		return true;
	}

	@Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Column(name="reference_key")
	public Long getReferenceKey() {
		return referenceKey;
	}

	public void setReferenceKey(Long referenceKey) {
		this.referenceKey = referenceKey;
	}

	@Column(name="targetlookup")
	public String getTargetlookup() {
		return targetlookup;
	}

	public void setTargetlookup(String targetlookup) {
		this.targetlookup = targetlookup;
	}

	@Column(name="target_lowercase")
	public String getTargetLowercase() {
		return targetLowercase;
	}

	public void setTargetLowercase(String targetLowercase) {
		this.targetLowercase = targetLowercase;
	}

	@Column(name="not_exact_taxon_match")
	public Boolean getNotExactTaxonMatch() {
		return notExactTaxonMatch;
	}

	public void setNotExactTaxonMatch(Boolean notExactTaxonMatch) {
		this.notExactTaxonMatch = notExactTaxonMatch;
	}

	@Override
	public String toString() {
		return "TargetEntity [getKey()=" + getKey() + ", getId()=" + getId() + ", targetlookup=" + targetlookup + ", targetLowercase=" + targetLowercase +
				", referenceKey=" + referenceKey + ", taxonKey=" + taxonKey + ", notExactTaxonMatch=" + notExactTaxonMatch + "]";
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="taxon_key", referencedColumnName="key", insertable=false, updatable=false)
	public TaxonEntity getTaxon() { // for queries only
		return null;
	}

	public void setTaxon(@SuppressWarnings("unused") TaxonEntity taxon) {
		// for hibernate query only
	}



}
