package fi.laji.datawarehouse.dao.vertica;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.utils.Util;

@Entity
@Table(name="annotation")
class AnnotationEntity extends NonAutogeneratingBaseEntity {

	private Long documentKey;
	private Long unitKey;
	private String documentId;
	private String unitId;
	private Long annotationByPersonKey;
	private Long annotationBySystemKey;
	private String annotationByPersonName;
	private String annotationBySystemName;
	private String resolvedType;
	private Date created;

	public AnnotationEntity() {}

	public AnnotationEntity(Annotation annotation) {
		setKey(generateKey());
		setId(annotation.getId().toURI());
		setCreated(annotation.createdAsDate());
		setResolvedType(annotation.resolveType().name());
		setAnnotationByPersonName(annotation.getAnnotationByPersonName());
		setAnnotationBySystemName(annotation.getAnnotationBySystemName());
	}

	@Column(name="document_key")
	public Long getDocumentKey() {
		return documentKey;
	}

	public void setDocumentKey(Long documentKey) {
		this.documentKey = documentKey;
	}

	@Column(name="unit_key")
	public Long getUnitKey() {
		return unitKey;
	}

	public void setUnitKey(Long unitKey) {
		this.unitKey = unitKey;
	}

	@Column(name="document_id")
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	@Column(name="unit_id")
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	@Column(name="person_key")
	public Long getAnnotationByPersonKey() {
		return annotationByPersonKey;
	}

	public void setAnnotationByPersonKey(Long annotationByPersonKey) {
		this.annotationByPersonKey = annotationByPersonKey;
	}

	@Column(name="system_key")
	public Long getAnnotationBySystemKey() {
		return annotationBySystemKey;
	}

	public void setAnnotationBySystemKey(Long annotationBySystemKey) {
		this.annotationBySystemKey = annotationBySystemKey;
	}

	@Column(name="created")
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name="person_name")
	public String getAnnotationByPersonName() {
		return annotationByPersonName;
	}

	public void setAnnotationByPersonName(String annotationByPersonName) {
		this.annotationByPersonName = Util.trimToByteLength(annotationByPersonName, 1000);
	}

	@Column(name="system_name")
	public String getAnnotationBySystemName() {
		return annotationBySystemName;
	}

	public void setAnnotationBySystemName(String annotationBySystemName) {
		this.annotationBySystemName = Util.trimToByteLength(annotationBySystemName, 1000);
	}

	@Column(name="resolved_type")
	public String getResolvedType() {
		return resolvedType;
	}

	public void setResolvedType(String resolvedType) {
		this.resolvedType = resolvedType;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

}
