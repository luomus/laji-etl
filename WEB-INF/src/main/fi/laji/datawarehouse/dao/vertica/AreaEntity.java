package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="area")
class AreaEntity extends NameableEntity {

	public AreaEntity() {}

	public AreaEntity(String uri) {
		setId(uri);
	}

}
