package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;

@MappedSuperclass
class MediaBaseEntity {

	private Long parentKey;
	private MediaObject mediaObject;
	private Long mediaTypeKey;

	public MediaBaseEntity() {}

	public MediaBaseEntity(MediaObject mediaObject) {
		this.mediaObject = mediaObject;
	}

	@Column(name="parent_key")
	public Long getParentKey() {
		return parentKey;
	}

	public void setParentKey(Long parentKey) {
		this.parentKey = parentKey;
	}

	@EmbeddedId
	@Embedded
	public MediaObject getMediaObject() {
		return mediaObject;
	}

	public void setMediaObject(MediaObject mediaObject) {
		this.mediaObject = mediaObject;
	}

	@Column(name="mediatype_key")
	public Long getMediaTypeKey() {
		return mediaTypeKey;
	}

	public void setMediaTypeKey(Long mediaTypeKey) {
		this.mediaTypeKey = mediaTypeKey;
	}

}
