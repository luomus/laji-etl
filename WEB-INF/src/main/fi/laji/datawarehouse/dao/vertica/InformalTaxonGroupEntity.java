package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="taxongroup")
class InformalTaxonGroupEntity extends NameableEntity {

	public InformalTaxonGroupEntity() {}

	public InformalTaxonGroupEntity(String groupId) {
		setId(groupId);
	}	

}
