package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="unit_fact")
class UnitFact extends FactLink {

	private static final long serialVersionUID = -2339649038788399088L;

	public UnitFact(Fact fact) {
		super(fact);
	}

	@Deprecated
	public UnitFact() {}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

}
