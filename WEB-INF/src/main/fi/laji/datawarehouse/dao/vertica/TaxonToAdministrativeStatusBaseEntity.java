package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class TaxonToAdministrativeStatusBaseEntity implements Serializable {

	private static final long serialVersionUID = 3555106029574035323L;

	private Long taxonKey;
	private Long statusKey;

	@Id @Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Id @Column(name="status_key")
	public Long getStatusKey() {
		return statusKey;
	}

	public void setStatusKey(Long statusKey) {
		this.statusKey = statusKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
