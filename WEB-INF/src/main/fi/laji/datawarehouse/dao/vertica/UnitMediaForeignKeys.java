package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class UnitMediaForeignKeys {

	private String unitId;
	private Long unitKey;
	private Long mediaTypeKey;

	public UnitMediaForeignKeys() {}

	@Column(name="unit_id")
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	@Column(name="unit_key")
	public Long getUnitKey() {
		return unitKey;
	}

	public void setUnitKey(Long unitKey) {
		this.unitKey = unitKey;
	}

	@Column(name="mediatype_key")
	public Long getMediaTypeKey() {
		return mediaTypeKey;
	}

	public void setMediaTypeKey(Long mediaTypeKey) {
		this.mediaTypeKey = mediaTypeKey;
	}

}
