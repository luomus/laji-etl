package fi.laji.datawarehouse.dao.vertica;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.InterpreterForQualityControl;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationDwLinkings;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Person;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.Filter;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.Filters.Operator;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.OrderBy.OrderCriteria;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.Cached;

public class VerticaDAOImpleQueries implements VerticaQueryDAO {

	private static final Criterion NOP_CRITERION = VerticaDAOImpleQueryFilters.NOP_CRITERION;
	private static final Criterion NO_RESULTS_CRITERION = VerticaDAOImpleQueryFilters.NO_RESULTS_CRITERION;
	private static final Map<String, DateColumn> DATE_COLUMNS = initDateColumnsMap();
	private final VerticaDAOImpleQueryFilters filterBuilder;
	private final VerticaDAOImpleSharedMappings mappings = new VerticaDAOImpleSharedMappings();
	private final VerticaDAOImple verticaDAO;
	private final DAO dao;
	private final int listCacheTimeMinutes;
	private final int aggregateCacheTimeMinutes;
	private final int countCacheTimeMinutes;
	private final int countCacheSize;
	private final int listCacheSize;
	private final int aggregateCacheSize;

	private final Cached<ListQuery, ListResponse> listCache;
	private final Cached<CountQuery, CountResponse> countQueryCache;
	private final Cached<AggregatedQuery, AggregateResponse> aggregateCache;

	public VerticaDAOImpleQueries(VerticaDAOImple verticaDAO, boolean production) {
		this.filterBuilder = new VerticaDAOImpleQueryFilters(verticaDAO);
		this.verticaDAO = verticaDAO;
		this.dao = verticaDAO.dao;

		if (production) {
			listCacheTimeMinutes = 30;
			aggregateCacheTimeMinutes = 1200;
			countCacheTimeMinutes = 10;
			listCacheSize = 100;
			aggregateCacheSize = 50000;
			countCacheSize = listCacheSize + aggregateCacheSize + 10000;
		} else {
			listCacheTimeMinutes = 5;
			aggregateCacheTimeMinutes = 100;
			countCacheTimeMinutes = 1;
			countCacheSize = 50;
			listCacheSize = 10;
			aggregateCacheSize = 50;
		}

		listCache = new Cached<>(new Cached.CacheLoader<ListQuery, ListResponse>() {
			@Override
			public ListResponse load(ListQuery query) {
				return getListNonCached(query).setCacheTimestamp();
			}
		}, listCacheTimeMinutes, TimeUnit.MINUTES, listCacheSize);

		countQueryCache = new Cached<>(new Cached.CacheLoader<CountQuery, CountResponse>() {
			@Override
			public CountResponse load(CountQuery query) {
				return getCountNonCached(query).setCacheTimestamp();
			}
		}, countCacheTimeMinutes, TimeUnit.MINUTES, countCacheSize);

		aggregateCache = new Cached<>(new Cached.CacheLoader<AggregatedQuery, AggregateResponse>() {
			@Override
			public AggregateResponse load(AggregatedQuery query) {
				return getAggregateNonCached(query).setCacheTimestamp();
			}
		}, aggregateCacheTimeMinutes, TimeUnit.MINUTES, aggregateCacheSize);
	}

	private static class DateColumn {
		private final String asName;
		private final String dbName;
		public DateColumn(String asName, String dbName) {
			this.asName = asName;
			this.dbName = dbName;
		}
	}

	private static Map<String, DateColumn> initDateColumnsMap() {
		Map<String, DateColumn> map = new HashMap<>();
		map.put("document.loadDate", new DateColumn("loadDate", "load_date"));
		map.put("document.firstLoadDate", new DateColumn("firstLoadDate", "first_load_date"));
		map.put("document.createdDate", new DateColumn("createdDate", "created_date"));
		map.put("document.modifiedDate", new DateColumn("modifiedDate", "modified_date"));
		map.put("document.createdDateMonth", new DateColumn("createdDateMonth", "extract(month from this_.created_date)"));
		return map;
	}

	@Override
	public Document get(Qname documentId) {
		return get(documentId, false);
	}

	@Override
	public Document get(Qname documentId, boolean approvedDataRequest) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getDocument(session, documentId.toURI(), approvedDataRequest);
		}
	}

	private Document getDocument(StatelessSession session, String documentId, boolean approvedDataRequest) {
		String json = getJson(documentId, session);
		if (json == null) return null;
		Document document;
		try {
			document = JsonToModel.documentFromJson(new JSONObject(json));
		} catch (CriticalParseFailure e) {
			throw new ETLException(e);
		}
		if (approvedDataRequest) {
			cleanUp(document);
		}
		setLinkings(document);
		setLicenseFromCollectionMetadataIfMissing(document); // TODO remove when all data reloaded and contains license

		return document;
	}

	private void cleanUp(Document document) {
		if (document == null) return;
		if (document.getEditorUserIds() != null) document.getEditorUserIds().clear();
		Const.PERSON_INFO_FACTS.forEach(document::removeFact);
		if (document.getGatherings() != null) document.getGatherings().forEach(gathering->cleanUp(gathering));
		if (document.getMedia() != null) document.getMedia().forEach(media->cleanUp(media));
	}

	private void cleanUp(Gathering gathering) {
		if (gathering == null) return;
		if (gathering.getTeam() != null) gathering.getTeam().clear();
		if (gathering.getObserverUserIds() != null) gathering.getObserverUserIds().clear();
		Const.PERSON_INFO_FACTS.forEach(gathering::removeFact);
		if (gathering.getUnits() != null) gathering.getUnits().forEach(unit->cleanUp(unit));
		if (gathering.getMedia() != null) gathering.getMedia().forEach(media->cleanUp(media));
	}

	private void cleanUp(Unit unit) {
		if (unit == null) return;
		if (unit.getMedia() != null) unit.getMedia().forEach(media->cleanUp(media));
	}

	private void cleanUp(MediaObject media) {
		if (media == null) return;
		media.setAuthor("Hidden");
		media.setCopyrightOwner("Hidden");
	}

	private void setLicenseFromCollectionMetadataIfMissing(Document document) {
		if (document == null) return;
		if (document.getLicenseId() == null) {
			CollectionMetadata collectionMetadata = dao.getCollections().get(document.getCollectionId().toURI());
			if (collectionMetadata != null) {
				document.setLicenseIdUsingQname(collectionMetadata.getIntellectualRights());
			}
		}
	}

	private void setLinkings(Document document) {
		Map<String, Person> persons = getDistinctPersons(getDistinctUserIds(document));
		setDocumentEditorLinkings(persons, document);
		setCollectionLinking(document);
		setNamedPlaceLinking(document);
		for (Gathering gathering : document.getGatherings()) {
			setGatheringObserverLinkings(persons, gathering);
			for (Unit unit : gathering.getUnits()) {
				setUnitLinkings(unit, gathering);
			}
		}
	}

	private Taxon getTaxon(Unit unit, Gathering gathering, boolean useAnnotations) {
		Qname taxonId = dao.getTaxonLinkingService().getTaxonId(unit, gathering, useAnnotations);
		if (!dao.hasTaxon(taxonId)) return null;
		return dao.getTaxon(taxonId);
	}

	private Map<String, Person> getDistinctPersons(Set<String> distinctUserIds) {
		if (distinctUserIds.isEmpty()) return Collections.emptyMap();
		Map<String, Person> persons = new HashMap<>();
		Map<String, Qname> personLookup = dao.getPersonLookupStructure();
		for (String userId : distinctUserIds) {
			Qname personId = personLookup.get(userId);
			if (personId == null) {
				persons.put(userId, new Person(userId));
			} else {
				String fullName = dao.getPerson(personId).getFullName();
				persons.put(userId, new Person(personId, userId, fullName));
			}
		}
		return persons;
	}

	private Set<String> getDistinctUserIds(Document document) {
		Set<String> userIds = new HashSet<>();
		for (String userId : document.getEditorUserIds()) {
			userIds.add(userId);
		}
		for (Gathering gathering : document.getGatherings()) {
			addDistinctUserIds(userIds, gathering);
		}
		return userIds;
	}

	private void addDistinctUserIds(Set<String> userIds, Gathering gathering) {
		if (gathering == null) return;
		for (String userId : gathering.getObserverUserIds()) {
			userIds.add(userId);
		}
	}

	private Set<String> getDistinctUserIds(JoinedRow row) {
		Set<String> userIds = getDistinctUserIds(row.getDocument());
		addDistinctUserIds(userIds, row.getGathering());
		return userIds;
	}

	private void setDocumentEditorLinkings(Map<String, Person> persons, Document document) {
		DocumentDWLinkings linkings = document.createLinkings();
		for (String userId : document.getEditorUserIds()) {
			linkings.getEditors().add(persons.get(userId));
		}

	}

	private void setGatheringObserverLinkings(Map<String, Person> persons, Gathering gathering) {
		GatheringDWLinkings gatheringLinkings = gathering.createLinkings();
		for (String userId : gathering.getObserverUserIds()) {
			gatheringLinkings.getObservers().add(persons.get(userId));
		}
	}

	private String getJson(String documentId, StatelessSession session) {
		Object o = session.createCriteria(DocumentEntity.class)
				.add(Restrictions.eq("id", documentId))
				.setProjection(Projections.property("json"))
				.uniqueResult();
		if (o == null) return null;
		if (!(o instanceof String)) throw new IllegalStateException();
		String json = (String) o;
		return json;
	}

	@Override
	public ListResponse getList(ListQuery query) {
		if (query.useCache()) {
			return listCache.get(query);
		}
		return getListNonCached(query);
	}

	@Override
	public List<JoinedRow> getRawList(ListQuery query) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getRawList(query, session);
		}
	}

	private ListResponse getListNonCached(ListQuery query) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getList(query, session);
		}
	}

	private class ListResults implements Iterable<ListResult> {
		List<ListResult> results = new ArrayList<>();
		public ListResult add(Long documentKey) {
			ListResult r = new ListResult(documentKey);
			results.add(r);
			return r;
		}
		public boolean isEmpty() {
			return results.isEmpty();
		}
		public int size() {
			return results.size();
		}
		public Collection<Long> documentKeys() {
			return results.stream().map(r->r.documentKey).collect(Collectors.toSet());
		}
		@Override
		public Iterator<ListResult> iterator() {
			return results.iterator();
		}
		public void addJson(Long documentKey, JSONObject documentJson) {
			for (ListResult r : results) {
				if (r.documentKey.equals(documentKey)) r.documentJson=documentJson;
			}
		}
	}

	private class ListResult {
		private final Long documentKey;
		private String unitId;
		private String id;
		private JSONObject documentJson;
		public ListResult(Long documentKey) {
			this.documentKey = documentKey;
		}
	}

	private ListResponse getList(ListQuery query, StatelessSession session) {
		List<JoinedRow> rows = getRawList(query, session);
		if (rows.isEmpty()) return new ListResponse(0, query.getCurrentPage(), query.getPageSize(), Collections.emptyList());
		long totalResults = getCount(new CountQuery(query)).getTotal();
		return new ListResponse(totalResults, query.getCurrentPage(), query.getPageSize(), rows);
	}

	private List<JoinedRow> getRawList(ListQuery query, StatelessSession session) {
		ListResults results = getListQueryKeys(query, session);
		if (results.isEmpty()) return Collections.emptyList();
		return getJoinedRows(results, query, session);
	}

	private ListResults getListQueryKeys(ListQuery query, StatelessSession session) {
		CriteriaBuilder builder = buildBaseCriteria(query, session);
		if (builder.noResults) return new ListResults();
		int limit = query.getPageSize();
		int offset = (query.getCurrentPage() - 1) * limit;
		builder.criteria.setMaxResults(limit).setFirstResult(offset);
		defineListOrderBy(query, builder.criteria);
		if (query.getBase() == Base.DOCUMENT) {
			return getDocumentKeys(builder);
		}
		if (query.getBase() == Base.UNIT_MEDIA) {
			return getUnitMediaListQueryKeysAndIds(query, builder);
		}
		return getUnitListQueryKeysAndIds(query, builder);
	}

	private ListResults getDocumentKeys(CriteriaBuilder builder) {
		builder.criteria.setProjection(Projections.property("key"));
		List<?> queryResult = builder.criteria.list();
		ListResults results = new ListResults();
		for (Object o : queryResult) {
			Long key = (Long) o;
			results.add(key);
		}
		return results;
	}

	private ListResults getUnitListQueryKeysAndIds(ListQuery query, CriteriaBuilder builder) {
		builder.criteria.setProjection(Projections.projectionList()
				.add(Projections.property("gatheringForeignKeys.documentKey"))
				.add(Projections.property("id"))
				.add(Projections.property(getIdField(query))));
		List<?> queryResult = builder.criteria.list();
		ListResults results = new ListResults();
		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			Long documentKey = (Long) row[0];
			ListResult r = results.add(documentKey);
			r.unitId = (String) row[1];
			r.id = (String) row[2];
		}
		return results;
	}

	private ListResults getUnitMediaListQueryKeysAndIds(ListQuery query, CriteriaBuilder builder) {
		builder.criteria.setProjection(Projections.projectionList()
				.add(Projections.property("gatheringForeignKeys.documentKey"))
				.add(Projections.property("mediaForeignKeys.unitId"))
				.add(Projections.property(getIdField(query))));
		List<?> queryResult = builder.criteria.list();
		ListResults results = new ListResults();
		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			Long documentKey = (Long) row[0];
			ListResult r = results.add(documentKey);
			r.unitId = (String) row[1];
			r.id = (String) row[2];
		}
		return results;
	}

	private List<JoinedRow> getJoinedRows(ListResults results, ListQuery query, StatelessSession session) {
		results = addDocumentJsons(results, session);
		ArrayList<JoinedRow> rows = new ArrayList<>(results.size());
		for (ListResult result : results) {
			if (result.documentJson == null) continue; // in theory it is possible for unit to get removed between key fetch an json fetch
			try {
				JoinedRow row = JsonToModel.joinedRowFromJson(result.documentJson, result.unitId, query.getBase(), result.id);
				if (query.isApprovedDataRequest()) {
					cleanUp(row);
				}
				setJoinedRowLinkings(query, row);
				rows.add(row);
			} catch (CriticalParseFailure e) {
				throw new ETLException(e);
			}
		}
		return rows;
	}

	private void cleanUp(JoinedRow row) {
		cleanUp(row.getDocument());
		cleanUp(row.getGathering());
		cleanUp(row.getUnit());
		cleanUp(row.getMedia());
	}

	private ListResults addDocumentJsons(ListResults results, StatelessSession session) {
		Criteria criteria = session.createCriteria(DocumentEntity.class);
		criteria.add(Restrictions.in("key", results.documentKeys()));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.property("key"), "key")
				.add(Projections.property("json"), "json"));
		List<?> queryResult = criteria.list();
		for (Object o : queryResult) {
			Object[] row = (Object[]) o;
			Long key = (Long) row[0];
			String json = (String) row[1];
			results.addJson(key, new JSONObject(json));
		}
		return results;
	}

	@Override
	public void setJoinedRowLinkings(JoinedRow row) {
		setJoinedRowLinkings(null, row);
	}

	private void setJoinedRowLinkings(ListQuery query, JoinedRow row) {
		setUnitLinkings(row.getUnit(), row.getGathering());
		setLinkings(query, row);
		if (licenseSelected(query)) {
			setLicenseFromCollectionMetadataIfMissing(row.getDocument()); // TODO remove when all data reloaded
		}
	}

	private boolean licenseSelected(ListQuery query) {
		if (query == null || query.getSelected() == null) return false;
		return query.getSelected().getSelectedFields().contains("document.licenseId");
	}

	private void setLinkings(ListQuery query, JoinedRow row) {
		if (linkingsSelected(query, "document.linkings.editors", "gathering.linkings.observers")) {
			Map<String, Person> persons = getDistinctPersons(getDistinctUserIds(row));
			setDocumentEditorLinkings(persons, row.getDocument());
			if (row.getGathering() != null) {
				setGatheringObserverLinkings(persons, row.getGathering());
			}
		}
		if (linkingsSelected(query, "document.linkings.collectionQuality")) {
			setCollectionLinking(row.getDocument());
		}
		if (linkingsSelected(query, "document.namedPlace")) {
			setNamedPlaceLinking(row.getDocument());
		}
	}

	private void setNamedPlaceLinking(Document document) {
		NamedPlaceEntity namedPlace = dao.getNamedPlaces().get(Qname.fromURI(document.getNamedPlaceId()));
		document.setNamedPlace(namedPlace);
	}

	private void setCollectionLinking(Document document) {
		DocumentDWLinkings linkings = document.createLinkings();
		linkings.setCollectionQuality(InterpreterForQualityControl.collectionQuality(document, dao));
	}

	private boolean linkingsSelected(ListQuery query, String ... fields) {
		if (query == null) return true; // Add all linkings when calling without a query
		if (query.getSelected() == null) return false;
		Set<String> selected = query.getSelected().getSelectedFieldParts();
		for (String s : fields) {
			if (selected.contains(s)) return true;
		}
		return false;
	}

	private static final Double NEUTRAL_QUALITY_NUMERIC = InterpreterForQualityControl.recordQualityNumeric(RecordQuality.NEUTRAL);

	private void setUnitLinkings(Unit unit, Gathering gathering) {
		if (unit == null) return;
		UnitDWLinkings linkings = unit.createLinkings();
		Taxon taxon = getTaxon(unit, gathering, true);
		Taxon originalTaxon = getTaxon(unit, gathering, false);
		linkings.setTaxon(taxon);
		linkings.setOriginalTaxon(originalTaxon);
		unit.setLinkings(linkings);

		for (Annotation a : unit.getAnnotations()) {
			Identification i = a.getIdentification();
			OccurrenceAtTimeOfAnnotation o = a.getOccurrenceAtTimeOfAnnotation();
			if (i != null) {
				i.setLinkings(new IdentificationDwLinkings());
				i.getLinkings().setTaxon(getTaxon(i.getTaxonID()));
			}
			if (o != null) {
				o.setLinkings(new IdentificationDwLinkings());
				o.getLinkings().setTaxon(getTaxon(o.getTaxonId()));
			}
		}

		for (IdentificationEvent e : unit.getIdentifications()) {
			setTaxonLinking(e);
		}

		for (TypeSpecimen t : unit.getTypes()) {
			setTaxonLinking(t);
		}

		// TODO remove when all data reloaded
		UnitInterpretations interpretations = unit.createInterpretations();
		if (interpretations.getNeedsCheck() == null) interpretations.setNeedsCheck(false);
		if (interpretations.getNeedsIdentification()== null) interpretations.setNeedsIdentification(false);
		if (interpretations.getRecordQuality() == null) {
			interpretations.setRecordQuality(RecordQuality.NEUTRAL);
			interpretations.setRecordQualityNumeric(NEUTRAL_QUALITY_NUMERIC);
			interpretations.setReliability(Reliability.UNDEFINED);
		}
	}

	private void setTaxonLinking(Identification i) {
		Taxon identificationTaxon = getTaxon(i);
		if (identificationTaxon != null) {
			i.setLinkings(new IdentificationDwLinkings());
			i.getLinkings().setTaxon(identificationTaxon);
		}
	}

	private Taxon getTaxon(Identification i) {
		Taxon taxon = getTaxon(i.getTaxonID());
		if (taxon != null) return taxon;
		Qname taxonId = dao.getTaxonLinkingService().getTaxonId(i.getTaxon());
		return getTaxon(taxonId);
	}

	private Taxon getTaxon(Qname taxonId) {
		if (taxonId == null) return null;
		if (!dao.hasTaxon(taxonId)) return null;
		return dao.getTaxon(taxonId);
	}

	private static class CriteriaBuilder {
		final boolean noResults;
		final Criteria criteria;
		CriteriaBuilder(Criteria criteria) {
			this.criteria = criteria;
			this.noResults = criteria == null;
		}
		static CriteriaBuilder noResults() {
			return new CriteriaBuilder(null);
		}
	}

	private <Q extends BaseQuery> CriteriaBuilder buildBaseCriteria(Q query, StatelessSession session) {
		Criteria criteria = session.createCriteria(getBaseEntityClass(query));
		defineJoinsIfNeeded(query, criteria);
		if (!defineFilters(query, criteria)) return CriteriaBuilder.noResults();
		return new CriteriaBuilder(criteria);
	}

	private void defineListOrderBy(ListQuery query, Criteria criteria) {
		if (query.getOrderBy() != null) {
			addOrderByToCriteria(query.getOrderBy(), criteria, true);
		}
		criteria.addOrder(Order.asc(getIdField(query)));
	}

	private void addOrderByToCriteria(OrderBy orderBy, Criteria criteria, boolean listQuery) {
		for (OrderCriteria orderCriteria : orderBy.getOrderCriterias()) {
			if (Const.RANDOM.equals(orderCriteria.getField())) {
				criteria.addOrder(random());
				continue;
			}
			if (orderCriteria.getField().startsWith(Const.RANDOM)) {
				int seed = orderCriteria.getField().hashCode();
				criteria.addOrder(random(seed));
				continue;
			}
			String entityReference = getOrderByUnitEntityReference(orderBy.getBase(), orderCriteria.getField(), listQuery);
			if (orderCriteria.getOrder() == OrderBy.Order.ASC) {
				criteria.addOrder(Order.asc(entityReference));
			} else {
				criteria.addOrder(Order.desc(entityReference));
			}
		}
	}

	private static final OrderRandom ORDER_RANDOM = new OrderRandom();

	private static class OrderRandom extends Order {
		private static final String RANDOM = "RANDOM()";
		private static final long serialVersionUID = -5363350231077580585L;
		private final String seed;
		public OrderRandom() {
			super("", false);
			this.seed = null;
		}
		public OrderRandom(int seed) {
			super("", false);
			this.seed = "SEEDED_RANDOM("+seed+")";
		}
		@Override
		public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) {
			if (seed == null) {
				return RANDOM;
			}
			return seed;
		}
	}

	private Order random() {
		return ORDER_RANDOM;
	}

	private Order random(int seed) {
		return new OrderRandom(seed);
	}

	private String getOrderByUnitEntityReference(Base base, String field, boolean listQuery) {
		if (!listQuery) {
			Set<String> aggregateFunctionNames = Const.aggregateFunctions(base);
			if (aggregateFunctionNames.contains(field)) return field;
			if (DATE_COLUMNS.containsKey(field)) {
				return DATE_COLUMNS.get(field).asName; // date columns are grouped differently
			}
		}
		return mappings.getEntityReference(base, field);
	}

	private <Q extends BaseQuery> boolean defineFilters(Q query, Criteria criteria) {
		if (!defineNormalFilters(query, criteria)) return false;
		if (query.getPermissionFilters() != null) {
			if (!definePermissionsFilters(query, criteria)) return false;
		}
		return true;
	}

	private <Q extends BaseQuery> boolean defineNormalFilters(Q query, Criteria criteria) {
		List<Criterion> andFilters = new ArrayList<>();
		List<Criterion> taxonStatusFilters = new ArrayList<>();
		if (!defineFilters(query.getFilters(), andFilters, taxonStatusFilters)) return false;
		if (!andFilters.isEmpty()) {
			criteria.add(Restrictions.and(toArray(andFilters)));
		}
		if (!taxonStatusFilters.isEmpty()) {
			criteria.add(Restrictions.or(toArray(taxonStatusFilters)));
		}
		return true;
	}

	private boolean defineFilters(Filters filters, List<Criterion> andFilters, List<Criterion> taxonStatusFilters) {
		boolean taxonStatusOr = Operator.OR.equals(filters.getTaxonAdminFiltersOperator());
		for (Filter filter : filters.getSetFiltersIncludingDefaults()) {
			Criterion criterion = filterBuilder.getFilter(filter);
			if (criterion == NOP_CRITERION) continue;
			if (taxonStatusOr && isTaxonStatusFilter(filter)) {
				taxonStatusFilters.add(criterion);
			} else {
				if (criterion == NO_RESULTS_CRITERION) return false;
				andFilters.add(criterion);
			}
		}
		if (!taxonStatusFilters.isEmpty()) {
			for (Criterion c : taxonStatusFilters) {
				if (c != NO_RESULTS_CRITERION) return true;
			}
			return false;
		}
		return true;
	}

	private boolean isTaxonStatusFilter(Filter filter) {
		return filter.getDefinition().taxonStatusFilter();
	}

	private <Q extends BaseQuery> boolean definePermissionsFilters(Q query, Criteria criteria) {
		DetachedCriteria permissionsSubQuery = permissionsSubQuery(query);
		if (permissionsSubQuery == null) return false;
		if (query.getBase() == Base.DOCUMENT) {
			permissionsSubQuery.setProjection(Projections.distinct(Property.forName("gatheringForeignKeys.documentKey")));
		} else if (query.getBase() == Base.GATHERING) {
			permissionsSubQuery.setProjection(Projections.distinct(Property.forName("foreignKeys.gatheringKey")));
		} else {
			permissionsSubQuery.setProjection(Property.forName("key"));
		}
		criteria.add(Subqueries.propertyIn("key", permissionsSubQuery));
		return true;
	}

	private <Q extends BaseQuery> DetachedCriteria permissionsSubQuery(Q query) {
		DetachedCriteria subQuery = DetachedCriteria.forClass(UnitEntity.class);

		Filters filters = query.getPermissionFilters();
		Map<String, String> innerJoins = defineFilterInnerJoins(filters);
		Map<String, String> leftJoins = defineFilterLeftJoins(filters);

		for (String innerJoin : innerJoins.keySet()) {
			leftJoins.remove(innerJoin);
		}

		for (Map.Entry<String, String> innerJoin : innerJoins.entrySet()) {
			subQuery.createAlias(innerJoin.getKey(), innerJoin.getValue(), JoinType.INNER_JOIN);
		}

		for (Map.Entry<String, String> leftJoin : leftJoins.entrySet()) {
			subQuery.createAlias(leftJoin.getKey(), leftJoin.getValue(), JoinType.LEFT_OUTER_JOIN);
		}

		List<Criterion> andFilters = new ArrayList<>();
		List<Criterion> taxonStatusFilters = new ArrayList<>();
		if (!defineFilters(filters, andFilters, taxonStatusFilters)) return null;
		if (!andFilters.isEmpty()) {
			subQuery.add(Restrictions.and(toArray(andFilters)));
		}
		if (!taxonStatusFilters.isEmpty()) {
			subQuery.add(Restrictions.or(toArray(taxonStatusFilters)));
		}
		return subQuery;
	}

	@Override
	public CountResponse getCount(CountQuery query) {
		if (query.useCache()) {
			return countQueryCache.get(query);
		}
		return getCountNonCached(query);
	}

	private CountResponse getCountNonCached(CountQuery query) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getCount(query, session);
		}
	}

	private CountResponse getCount(CountQuery query, StatelessSession session) {
		CriteriaBuilder builder = buildBaseCriteria(query, session);
		if (builder.noResults) return new CountResponse(0);
		Long count = (Long) builder.criteria.setProjection(Projections.rowCount()).uniqueResult();
		return new CountResponse(count);
	}

	@Override
	public AggregateResponse getAggregate(AggregatedQuery query) {
		if (query.useCache()) {
			return aggregateCache.get(query);
		}
		return getAggregateNonCached(query);
	}

	@Override
	public List<AggregateRow> getRawAggregate(AggregatedQuery query) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getRawAggregate(query, session);
		}
	}

	private AggregateResponse getAggregateNonCached(AggregatedQuery query) {
		try (StatelessSession session = verticaDAO.getQuerySession()) {
			return getAggregated(query, session);
		}
	}

	private AggregateResponse getAggregated(AggregatedQuery query, StatelessSession session) {
		List<AggregateRow> results = getRawAggregate(query, session);
		if (results.isEmpty()) return new AggregateResponse(0, query.getCurrentPage(), query.getPageSize(), results);
		long count = getAggregatedCount(query, results.size(), session);
		return new AggregateResponse(count, query.getCurrentPage(), query.getPageSize(), results);
	}

	private List<AggregateRow> getRawAggregate(AggregatedQuery query, StatelessSession session) {
		CriteriaBuilder builder = buildBaseCriteria(query, session);
		if (builder.noResults) return Collections.emptyList();
		excludeNulls(query, builder.criteria);
		ProjectionList projectionList = buildGroupBy(query);
		addAggregateFunctions(query, projectionList);
		builder.criteria.setProjection(projectionList);
		if (query.hasSetOrder()) {
			definedAggregateOrderBy(query, builder.criteria);
		} else {
			defaultAggregateOrderBy(query, builder.criteria);
		}

		builder.criteria.setResultTransformer(new AggregatedResultTransformer(query.getAggregateBy()));

		int limit = query.getPageSize();
		int offset = (query.getCurrentPage() - 1) * limit;
		builder.criteria.setMaxResults(limit).setFirstResult(offset);

		@SuppressWarnings("unchecked")
		List<AggregateRow> results = builder.criteria.list();
		return results;
	}

	private long getAggregatedCount(AggregatedQuery query, int resultCount, StatelessSession session) {
		if (query.getAggregateBy().getFields().isEmpty()) return 1;
		if (query.getCurrentPage() == 1 && resultCount < query.getPageSize()) {
			return resultCount;
		}
		CriteriaBuilder builder = buildBaseCriteria(query, session);
		excludeNulls(query, builder.criteria);
		ProjectionList projectionList = buildGroupBy(query);
		projectionList.add(Projections.property("aggregateRowCount"));
		builder.criteria.setProjection(projectionList);
		builder.criteria.setMaxResults(1);
		builder.criteria.setResultTransformer(AggregatedCountResultTransformer.instance());
		List<?> res = builder.criteria.list();
		if (res.isEmpty()) return 0L;
		return (Long) res.get(0);
	}

	private void excludeNulls(AggregatedQuery query, Criteria criteria) {
		if (!query.isExludeNulls()) return;
		if (!query.getAggregateBy().hasValues()) return;

		String firstField = query.getAggregateBy().getFields().get(0);
		if (DATE_COLUMNS.containsKey(firstField)) {
			if (DATE_COLUMNS.get(firstField).asName.equals("createdDateMonth")) {
				criteria.add(Restrictions.isNotNull(mappings.getEntityReference(query.getBase(), "document.createdDate")));
				return;
			}
		}
		criteria.add(Restrictions.isNotNull(mappings.getEntityReference(query.getBase(), firstField)));
	}

	private void addAggregateFunctions(AggregatedQuery query, ProjectionList projectionList) {
		projectionList.add(Projections.alias(Projections.rowCount(), Const.COUNT));
		if (query.getBase() == Base.UNIT) {
			if (!query.isOnlyCountRequest()) {
				projectionList.add(Projections.alias(Projections.sum(mappings.getEntityReference(Base.UNIT, "unit.interpretations.individualCount")), Const.INDIVIDUAL_COUNT_SUM));
				projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(Base.UNIT, "unit.interpretations.individualCount")), Const.INDIVIDUAL_COUNT_MAX));
				addOldestNewestRecord(query, projectionList);
				projectionList.add(Projections.alias(Projections.min(mappings.getEntityReference(Base.UNIT, "unit.interpretations.recordQuality")), Const.RECORD_QUALITY_MAX));
			}
			if (query.isTaxonAggregateRequest()) {
				String taxonTable = query.getFilters().useIdentificationAnnotations() ? "taxon" : "originalTaxon";
				projectionList.add(Projections.alias(Projections.countDistinct(taxonTable+".id"), Const.TAXON_COUNT));
				projectionList.add(Projections.alias(Projections.countDistinct(taxonTable+".speciesId"), Const.SPECIES_COUNT));
				projectionList.add(Projections.alias(Projections.max(taxonTable+".redListStatusGroup"), Const.RED_LIST_STATUS_MAX));
			}
			if (query.isAtlasCountRequest()) {
				projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(Base.UNIT, "unit.atlasCode")), Const.ATLAS_CODE_MAX));
				projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(Base.UNIT, "unit.atlasClass")), Const.ATLAS_CLASS_MAX));
			}
			if (query.isPairCountRequest()) {
				projectionList.add(Projections.alias(Projections.sum(mappings.getEntityReference(Base.UNIT, "unit.interpretations.pairCount")), Const.PAIR_COUNT_SUM));
				projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(Base.UNIT, "unit.interpretations.pairCount")), Const.PAIR_COUNT_MAX));
			}
			if (query.isGatheringCountRequest()) {
				projectionList.add(Projections.alias(Projections.countDistinct(mappings.getEntityReference(Base.UNIT, "gathering.gatheringId")), Const.GATHERING_COUNT));
			}
		} else if (query.getBase() == Base.GATHERING) {
			if (!query.isOnlyCountRequest()) {
				addOldestNewestRecord(query, projectionList);
				projectionList.add(Projections.alias(Projections.sum(mappings.getEntityReference(Base.GATHERING, "gathering.conversions.linelengthInMeters")), Const.LINELENGTH_SUM));
			}
		}
		if (!query.isOnlyCountRequest()) {
			projectionList.add(Projections.alias(Projections.min(mappings.getEntityReference(query.getBase(), "document.firstLoadDate")), Const.FIRST_LOAD_DATE_MIN));
			projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(query.getBase(), "document.firstLoadDate")), Const.FIRST_LOAD_DATE_MAX));
			projectionList.add(Projections.alias(Projections.property("securedCount"), Const.SECURED_COUNT));
		}
	}

	private void addOldestNewestRecord(AggregatedQuery query, ProjectionList projectionList) {
		if (query.isPessimisticDateRangeHandling()) {
			projectionList.add(Projections.alias(Projections.min(mappings.getEntityReference(query.getBase(), "gathering.eventDate.end")), Const.OLDEST_RECORD));
			projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(query.getBase(), "gathering.eventDate.begin")), Const.NEWEST_RECORD));
		} else {
			projectionList.add(Projections.alias(Projections.min(mappings.getEntityReference(query.getBase(), "gathering.eventDate.begin")), Const.OLDEST_RECORD));
			projectionList.add(Projections.alias(Projections.max(mappings.getEntityReference(query.getBase(), "gathering.eventDate.end")), Const.NEWEST_RECORD));
		}
	}

	private void defaultAggregateOrderBy(AggregatedQuery query, Criteria criteria) {
		criteria.addOrder(Order.desc(Const.COUNT));
		for (String fieldName : getFieldNamesOfAggregateBy(query)) {
			criteria.addOrder(Order.asc(fieldName));
		}
	}

	private void definedAggregateOrderBy(AggregatedQuery query, Criteria criteria) {
		addOrderByToCriteria(query.getOrderBy(), criteria, false);
		Collection<String> alreadyAdded = query.getOrderBy().getFieldNames();
		for (String field : query.getAggregateBy().getFields()) {
			if (alreadyAdded.contains(field)) continue;
			String entityReference = getOrderByUnitEntityReference(query.getBase(), field, false);
			criteria.addOrder(Order.asc(entityReference));
		}
		criteria.addOrder(Order.desc(Const.COUNT));
	}

	private List<String> getFieldNamesOfAggregateBy(AggregatedQuery query) {
		List<String> fieldNamesForOrderBy = new ArrayList<>();
		for (String field : query.getAggregateBy().getFields()) {
			if (DATE_COLUMNS.containsKey(field)) {
				fieldNamesForOrderBy.add(DATE_COLUMNS.get(field).asName); // date columns are grouped differently
			} else {
				fieldNamesForOrderBy.add(mappings.getEntityReference(query.getBase(), field));
			}
		}
		return fieldNamesForOrderBy;
	}

	private ProjectionList buildGroupBy(AggregatedQuery query) {
		ProjectionList projectionList = Projections.projectionList();
		for (String field : query.getAggregateBy().getFields()) {
			if (DATE_COLUMNS.containsKey(field)) {
				// date columns are grouped as dates without time
				DateColumn dateColumn = DATE_COLUMNS.get(field);
				projectionList.add(dateColumnProjection(dateColumn));
			} else {
				String fieldName = mappings.getEntityReference(query.getBase(), field);
				projectionList.add(Projections.groupProperty(fieldName));
			}
		}
		return projectionList;
	}

	private Projection dateColumnProjection(DateColumn dateColumn) {
		// Isn't hibernate API syntax absolutely fantastic?!

		if (dateColumn.asName.equals("createdDateMonth")) {
			return Projections.alias(
					Projections.sqlGroupProjection(
							dateColumn.dbName + " as " + dateColumn.asName,
							dateColumn.asName,
							new String[] {dateColumn.asName},
							new org.hibernate.type.Type[] {StandardBasicTypes.INTEGER}),
					dateColumn.asName);
		}

		return Projections.alias(
				Projections.sqlGroupProjection(
						"date(this_."+dateColumn.dbName+") as " + dateColumn.asName,
						dateColumn.asName,
						new String[] {dateColumn.asName},
						new org.hibernate.type.Type[] {StandardBasicTypes.DATE}),
				dateColumn.asName);
	}

	private <Q extends BaseQuery> void defineJoinsIfNeeded(Q query, Criteria criteria) {
		Map<String, String> innerJoins = defineInnerJoins(query.getFilters());
		Map<String, String> leftJoins = defineLeftJoinFields(query, innerJoins);

		for (String innerJoin : innerJoins.keySet()) {
			leftJoins.remove(innerJoin);
		}

		for (Map.Entry<String, String> innerJoin : innerJoins.entrySet()) {
			criteria.createAlias(innerJoin.getKey(), innerJoin.getValue(), JoinType.INNER_JOIN);
		}

		for (Map.Entry<String, String> leftJoin : leftJoins.entrySet()) {
			criteria.createAlias(leftJoin.getKey(), leftJoin.getValue(), JoinType.LEFT_OUTER_JOIN);
		}
	}

	private Map<String, String> defineInnerJoins(Filters filters) {
		Map<String, String> innerJoins = defineFilterInnerJoins(filters);

		if (filters.base().includes(Base.ANNOTATION)) {
			innerJoins.put("annotations", "unitAnnotation");
		}

		if (filters.base().includes(Base.SAMPLE)) {
			innerJoins.put("samples", "sample");
		}

		return innerJoins;
	}

	private Map<String, String> defineFilterInnerJoins(Filters filters) {
		Map<String, String> innerJoins = new HashMap<>();
		if (filters.base().includes(Base.UNIT)) {
			if (filters.hasTaxonFilter()) {
				joinTargetAndTaxon(filters, innerJoins);
			}
			if (filters.hasTargetFilter()) {
				joinTarget(filters, innerJoins);
			}
		}
		if (filters.hasNamedPlaceFilter()) {
			innerJoins.put("namedPlace", "namedPlace");
		}
		if (filters.hasCollectionFilter()) {
			innerJoins.put("collection", "collection");
		}

		return innerJoins;
	}

	private boolean hasInformalTaxonGroupIncludingReportedFilter(Filters filters) {
		return filters.getInformalTaxonGroupIdIncludingReported() != null;
	}

	private <Q extends BaseQuery> Map<String, String> defineLeftJoinFields(Q query, Map<String, String> innerJoins) {
		Map<String, String> leftJoins = defineFilterLeftJoins(query.getFilters());

		Set<String> joinFields = new HashSet<>();
		if (query instanceof AggregatedQuery) {
			AggregatedQuery aggregatedQuery = (AggregatedQuery) query;
			joinFields.addAll(aggregatedQuery.getAggregateBy().getFields());
			if (aggregatedQuery.isTaxonAggregateRequest() && aggregatedQuery.getBase().includes(Base.UNIT)) {
				joinTargetAndTaxon(query.getFilters(), leftJoins);
			}
		}

		if (query instanceof ListQuery) {
			OrderBy orderBy = ((ListQuery) query).getOrderBy();
			if (orderBy != null) {
				joinFields.addAll(orderBy.getFieldNames());
			}
		}

		if (joinFields.isEmpty()) return leftJoins;

		// Left side is the name that is found from the entity. Right side should match with the alias defined in VerticaDAOImpleSharedMappings
		if (joinFields.contains("document.keywords")) {
			leftJoins.put("documentKeywords", "documentKeyword");
		}

		if (query.getBase().includes(Base.UNIT)) {
			if (joinFields.contains("unit.keywords")) {
				leftJoins.put("unitKeywords", "unitKeyword");
			}
			if (joinFields.contains("unit.samples.keywords")) {
				leftJoins.put("samples", "sample");
				leftJoins.put("sample.sampleKeywords", "sampleKeyword");
			}
			if (joinFields.contains("unit.interpretations.effectiveTags")) {
				leftJoins.put("unitEffectiveTags", "unitEffectiveTag");
			}
		}

		if (joinFields.contains("document.secureReasons")) {
			leftJoins.put("documentSecureReasons", "documentSecureReason");
		}

		if (joinFields.contains("document.editorUserIds")) {
			leftJoins.put("documentUserIds", "documentUserId");
		}

		if (joinFields.contains("document.linkings.editors")) {
			leftJoins.put("documentUserIds", "documentUserId");
			leftJoins.put("documentUserIds.userId", "documentUserIdUserId");
		}

		if (joinFields.contains("document.linkings.collectionQuality")) {
			innerJoins.put("collection", "collection");
		}

		if (query.getBase().includes(Base.GATHERING)) {
			if (joinFields.contains("gathering.observerUserIds")) {
				leftJoins.put("gatheringUserIds", "gatheringUserId");
			}
			if (joinFields.contains("gathering.linkings.observers")) {
				leftJoins.put("gatheringUserIds", "gatheringUserId");
				leftJoins.put("gatheringUserIds.userId", "gatheringUserIdUserId");
			}
			if (joinFields.contains("gathering.team")) {
				leftJoins.put("team", "team");
			}
			if (joinFields.contains("gathering.team.memberId")) {
				leftJoins.put("team", "team");
				leftJoins.put("team.teamAgents", "teamAgent");
			}
			if (joinFields.contains("gathering.team.memberName")) {
				leftJoins.put("team", "team");
				leftJoins.put("team.teamAgents", "teamAgent");
				leftJoins.put("team.teamAgents.agent", "teamAgentAgent");
			}
		}

		for (String joinField : joinFields) {
			if (query.getBase().includes(Base.UNIT)) {
				if (joinField.startsWith("unit.linkings.taxon.")) {
					leftJoins.put("target", "target");
					leftJoins.put("target.taxon", "taxon");
					if (joinField.contains("informalTaxonGroups")) {
						leftJoins.put("target.taxon.taxonToGroup", "taxonToGroup");
					}
					if (joinField.contains("taxonSets")) {
						leftJoins.put("target.taxon.taxonToSet", "taxonToSet");
					}
					if (joinField.contains("habitats")) {
						leftJoins.put("target.taxon.taxonToHabitat", "taxonToHabitat");
					}
					if (joinField.contains("administrativeStatuses")) {
						leftJoins.put("target.taxon.taxonToStatus", "taxonToStatus");
					}
					if (joinField.contains("typesOfOccurrenceInFinland")) {
						leftJoins.put("target.taxon.taxonToTypeOfOccurrence", "taxonToTypeOfOccurrence");
					}
				}
				if (joinField.startsWith("unit.linkings.originalTaxon.")) {
					leftJoins.put("originalTarget", "originalTarget");
					leftJoins.put("originalTarget.taxon", "originalTaxon");
					if (joinField.contains("informalTaxonGroups")) {
						leftJoins.put("originalTarget.taxon.taxonToGroup", "originalTaxonToGroup");
					}
					if (joinField.contains("taxonSets")) {
						leftJoins.put("originalTarget.taxon.taxonToSet", "originalTaxonToSet");
					}
					if (joinField.contains("habitats")) {
						leftJoins.put("originalTarget.taxon.taxonToHabitat", "originalTaxonToHabitat");
					}
					if (joinField.contains("administrativeStatuses")) {
						leftJoins.put("originalTarget.taxon.taxonToStatus", "originalTaxonToStatus");
					}
					if (joinField.contains("typesOfOccurrenceInFinland")) {
						leftJoins.put("originalTarget.taxon.taxonToTypeOfOccurrence", "originalTaxonToTypeOfOccurrence");
					}
				}
				if (joinField.startsWith("unit.annotations.")) {
					leftJoins.put("annotations", "unitAnnotation");
				}
				if (query.getBase() != Base.UNIT_MEDIA) {
					if (joinField.startsWith("unit.media.")) {
						leftJoins.put("unitMedia", "unitMedia");
					}
				}
				if (joinField.startsWith("unit.facts.")) {
					leftJoins.put("unitFacts", "unitFact");
				}
				if (joinField.startsWith("unit.samples.")) {
					leftJoins.put("samples", "sample");
				}
				if (joinField.startsWith("unit.samples.facts.")) {
					leftJoins.put("samples.sampleFacts", "sampleFact");
				}
			}
			if (query.getBase().includes(Base.GATHERING)) {
				if (joinField.startsWith("gathering.media.")) {
					leftJoins.put("gatheringMedia", "gatheringMedia");
				}
				if (joinField.startsWith("gathering.taxonCensus.")) {
					leftJoins.put("taxonCensus", "taxonCensus");
				}
				if (joinField.startsWith("gathering.facts.")) {
					leftJoins.put("gatheringFacts", "gatheringFact");
				}
			}
			if (joinField.startsWith("document.media.")) {
				leftJoins.put("documentMedia", "documentMedia");
			}
			if (joinField.startsWith("document.facts.")) {
				leftJoins.put("documentFacts", "documentFact");
			}
			if (joinField.startsWith("document.namedPlace.")) {
				leftJoins.put("namedPlace", "namedPlace");
			}
		}
		return leftJoins;
	}

	private Map<String, String> defineFilterLeftJoins(Filters filters) {
		Map<String, String> leftJoins = new HashMap<>();
		if (filters.base().includes(Base.UNIT)) {
			// target search may be a taxon search if the search term matches with some taxon id so taxon table must be (left) joined
			if (filters.hasTargetFilter()) {
				joinTaxon(filters, leftJoins);
			}

			// informalTaxonGroupIdIncludingReported is not part of query.hasTaxonQuery() (which would cause a inner join to taxon) because this filter is an OR query between unit.reportedInformalTaxonGroupId and taxon informalTaxonGroupId -> taxon must be left joined
			if (hasInformalTaxonGroupIncludingReportedFilter(filters)) {
				joinTargetAndTaxon(filters, leftJoins);
			}
		}
		return leftJoins;
	}

	private void joinTarget(Filters filters, Map<String, String> joins) {
		if (filters.useIdentificationAnnotations()) {
			joins.put("target", "target");
		} else {
			joins.put("originalTarget", "originalTarget");
		}
	}

	private void joinTaxon(Filters filters, Map<String, String> joins) {
		if (filters.useIdentificationAnnotations()) {
			joins.put("target.taxon", "taxon");
		} else {
			joins.put("originalTarget.taxon", "originalTaxon");
		}
	}

	private void joinTargetAndTaxon(Filters filters, Map<String, String> joins) {
		joinTarget(filters, joins);
		joinTaxon(filters, joins);
	}

	private static Criterion[] toArray(List<Criterion> criterion) {
		return criterion.toArray(new Criterion[criterion.size()]);
	}

	@Override
	public VerticaCustomQueriesDAO getCustomQueries() {
		return new VerticaDAOImpleCustomQueries(verticaDAO);
	}

	@Override
	public void clearCaches() {
		listCache.invalidateAll();
		countQueryCache.invalidateAll();
		aggregateCache.invalidateAll();
	}

	private Class<?> getBaseEntityClass(BaseQuery query) {
		if (query.getBase() == Base.UNIT_MEDIA) return UnitMediaEntity.class;
		if (query.getBase().includes(Base.UNIT)) return UnitEntity.class;
		if (query.getBase() == Base.GATHERING) return GatheringEntity.class;
		if (query.getBase() == Base.DOCUMENT) return DocumentEntity.class;
		throw new UnsupportedOperationException("Not yet implemented for base " + query.getBase());
	}

	private String getIdField(ListQuery query) {
		if (query.getBase() == Base.UNIT) return "id";
		if (query.getBase() == Base.ANNOTATION) return "unitAnnotation.id";
		if (query.getBase() == Base.UNIT_MEDIA) return "mediaObject.fullURL";
		if (query.getBase() == Base.SAMPLE) return "sample.id";
		if (query.getBase() == Base.DOCUMENT) return "id";
		throw new UnsupportedOperationException("Not yet implemented for base " + query.getBase());
	}

}
