package fi.laji.datawarehouse.dao.vertica;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;

interface MediaContainer {

	public Long getKey();
	
	public List<MediaObject> revealMedia();
	
}
