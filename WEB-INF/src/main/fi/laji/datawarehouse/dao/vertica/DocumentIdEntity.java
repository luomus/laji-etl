package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="all_documentids")
class DocumentIdEntity  {

	@Id
	private String documentId;

	public DocumentIdEntity() {}

	public DocumentIdEntity(String uri) {
		setDocumentId(uri);
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

}
