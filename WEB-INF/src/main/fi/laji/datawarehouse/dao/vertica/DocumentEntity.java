package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.ModelToJson;

@Entity
@Table(name="document")
@AttributeOverride(name="id", column=@Column(name="document_id"))
class DocumentEntity extends NonAutogeneratingBaseEntity implements MediaContainer, FactContainer, AnnotationContainer {

	private Document document;
	private DocumentForeignKeys foreignKeys = new DocumentForeignKeys();
	private String json;
	private Long aggregateRowCount;
	private Long securedCount;

	public DocumentEntity() {}

	public DocumentEntity(Document document) {
		this.document = document;
		setId(document.getDocumentId().toURI());
		setKey(generateKey());
		try {
			setJson(ModelToJson.toJson(document).toString());
		} catch (Exception e) {
			throw new ETLException("Document to JSON", e);
		}
	}

	@Formula(value="count(*) over()")
	public Long getAggregateRowCount() {
		return aggregateRowCount;
	}

	public void setAggregateRowCount(Long aggregateRowCount) {
		this.aggregateRowCount = aggregateRowCount;
	}

	@Formula(value="count(CASE WHEN secured = true THEN 1 ELSE NULL END)")
	public Long getSecuredCount() {
		return securedCount;
	}

	public void setSecuredCount(Long securedCount) {
		this.securedCount = securedCount;
	}

	@OneToMany(mappedBy="document", fetch=FetchType.LAZY)
	public Set<DocumentUserId> getDocumentUserIds() {
		return null; // for queries only
	}

	public void setDocumentUserIds(@SuppressWarnings("unused") Set<DocumentUserId> documentUserIds) {
		// for queries only
	}

	@OneToMany(mappedBy="document", fetch=FetchType.LAZY)
	public Set<DocumentSecureReason> getDocumentSecureReasons() {
		return null; // for queries only
	}

	public void setDocumentSecureReasons(@SuppressWarnings("unused") Set<DocumentSecureReason> documentSecureReasons) {
		// for queries only
	}

	@OneToMany(mappedBy="document", fetch=FetchType.LAZY)
	public Set<DocumentKeyword> getDocumentKeywords() {
		return null; // for queries only
	}

	public void setDocumentKeywords(@SuppressWarnings("unused") Set<DocumentKeyword> documentKeywords) {
		// for queries only
	}

	@OneToMany(mappedBy="document", fetch=FetchType.LAZY)
	public Set<DocumentFact> getDocumentFacts() {
		return null; // for queries only
	}

	public void setDocumentFacts(@SuppressWarnings("unused") Set<DocumentFact> documentFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="document", fetch=FetchType.LAZY)
	public Set<DocumentMedia> getDocumentMedia() {
		return null; // for queries only
	}

	public void setDocumentMedia(@SuppressWarnings("unused") Set<DocumentMedia> media) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="namedplace_id", referencedColumnName="id", insertable=false, updatable=false)
	public NamedPlaceEntity getNamedPlace() {
		return null; // for queries only
	}

	public void setNamedPlace(@SuppressWarnings("unused") NamedPlaceEntity e) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="collection_key", referencedColumnName="key", insertable=false, updatable=false)
	public CollectionEntity getCollection() {
		return null; // for queries only
	}

	public void setCollection(@SuppressWarnings("unused") CollectionEntity e) {
		// for hibernate query only
	}

	@Embedded
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	@Embedded
	public DocumentForeignKeys getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(DocumentForeignKeys foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	@Column(name="original_document")
	public String getJson() {
		return json;
	}

	public void setJson(String documentJson) {
		this.json = documentJson;
	}

	@Override
	@Transient
	public List<MediaObject> revealMedia() {
		return getDocument().getMedia();
	}

	@Override
	@Transient
	public List<Fact> revealFacts() {
		return getDocument().getFacts();
	}

	@Override
	@Transient
	public List<Annotation> revealAnnotations() {
		return document.getAnnotations();
	}

	@Override
	@Transient
	public AnnotationContainerInformation revealAnnotationContainerInformation() {
		return new AnnotationContainerInformation(document, getKey());
	}

}
