package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class GatheringForeignKeys {

	private String documentId;
	private Long documentKey;
	private Long teamKey;
	private Long birdaAsociationAreaKey;
	private Long countryKey;
	private Long sourceOfCountryKey;
	private Long biogeographicalProvinceKey;
	private Long sourceOfBiogeographicalProvinceKey;
	private Long finnishMunicipalityKey;
	private Long sourceOfFinnishMunicipalityKey;
	private Long sourceOfCoordinatesKey;
	private Long eventDateBeginKey;
	private Long eventDateEndKey;
	private Long issueKey;
	private Long issueSourceKey;
	private Long timeIssueKey;
	private Long timeIssueSourceKey;
	private Long locationIssueKey;
	private Long locationIssueSourceKey;

	public GatheringForeignKeys() {}


	@Column(name="team_key")
	public Long getTeamKey() {
		return teamKey;
	}

	public void setTeamKey(Long dwTeamKey) {
		this.teamKey = dwTeamKey;
	}

	@Column(name="birdarea_key")
	public Long getBirdAssociationAreaKey() {
		return birdaAsociationAreaKey;
	}

	public void setBirdAssociationAreaKey(Long dwAreaKey) {
		this.birdaAsociationAreaKey = dwAreaKey;
	}

	@Column(name="country_key")
	public Long getCountryKey() {
		return countryKey;
	}

	public void setCountryKey(Long dwCountryKey) {
		this.countryKey = dwCountryKey;
	}

	@Column(name="bioprovince_key")
	public Long getBiogeographicalProvinceKey() {
		return biogeographicalProvinceKey;
	}

	public void setBiogeographicalProvinceKey(Long dwBiogeographicalProvinceKey) {
		this.biogeographicalProvinceKey = dwBiogeographicalProvinceKey;
	}

	@Column(name="bioprovincesource_key")
	public Long getSourceOfBiogeographicalProvinceKey() {
		return sourceOfBiogeographicalProvinceKey;
	}

	public void setSourceOfBiogeographicalProvinceKey(Long dwSourceOfBiogeographicalProvinceKey) {
		this.sourceOfBiogeographicalProvinceKey = dwSourceOfBiogeographicalProvinceKey;
	}

	@Column(name="municipalitysource_key")
	public Long getSourceOfFinnishMunicipalityKey() {
		return sourceOfFinnishMunicipalityKey;
	}

	public void setSourceOfFinnishMunicipalityKey(Long dwSourceOfFinnishMunicipalityKey) {
		this.sourceOfFinnishMunicipalityKey = dwSourceOfFinnishMunicipalityKey;
	}

	@Column(name="countrysource_key")
	public Long getSourceOfCountryKey() {
		return sourceOfCountryKey;
	}

	public void setSourceOfCountryKey(Long dwSourceOfCountryKey) {
		this.sourceOfCountryKey = dwSourceOfCountryKey;
	}

	@Column(name="municipality_key")
	public Long getFinnishMunicipalityKey() {
		return finnishMunicipalityKey;
	}

	public void setFinnishMunicipalityKey(Long dwFinnishMunicipalityKey) {
		this.finnishMunicipalityKey = dwFinnishMunicipalityKey;
	}

	@Column(name="coordinatesource_key")
	public Long getSourceOfCoordinatesKey() {
		return sourceOfCoordinatesKey;
	}

	public void setSourceOfCoordinatesKey(Long dwSourceOfCoordinatesKey) {
		this.sourceOfCoordinatesKey = dwSourceOfCoordinatesKey;
	}

	@Column(name="document_key")
	public Long getDocumentKey() {
		return documentKey;
	}

	public void setDocumentKey(Long documentKey) {
		this.documentKey = documentKey;
	}

	@Column(name="document_id")
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	@Column(name="datebegin_key")
	public Long getEventDateBeginKey() {
		return eventDateBeginKey;
	}

	public void setEventDateBeginKey(Long eventDateBeginKey) {
		this.eventDateBeginKey = eventDateBeginKey;
	}

	@Column(name="dateend_key")
	public Long getEventDateEndKey() {
		return eventDateEndKey;
	}

	public void setEventDateEndKey(Long eventDateEndKey) {
		this.eventDateEndKey = eventDateEndKey;
	}

	@Column(name="gathering_issue_key")
	public Long getIssueKey() {
		return issueKey;
	}

	public void setIssueKey(Long issueKey) {
		this.issueKey = issueKey;
	}

	@Column(name="gathering_issuesource_key")
	public Long getIssueSourceKey() {
		return issueSourceKey;
	}

	public void setIssueSourceKey(Long issueSourceKey) {
		this.issueSourceKey = issueSourceKey;
	}

	@Column(name="time_issue_key")
	public Long getTimeIssueKey() {
		return timeIssueKey;
	}

	public void setTimeIssueKey(Long timeIssueKey) {
		this.timeIssueKey = timeIssueKey;
	}

	@Column(name="time_issuesource_key")
	public Long getTimeIssueSourceKey() {
		return timeIssueSourceKey;
	}

	public void setTimeIssueSourceKey(Long timeIssueSourceKey) {
		this.timeIssueSourceKey = timeIssueSourceKey;
	}

	@Column(name="location_issue_key")
	public Long getLocationIssueKey() {
		return locationIssueKey;
	}

	public void setLocationIssueKey(Long locationIssueKey) {
		this.locationIssueKey = locationIssueKey;
	}

	@Column(name="location_issuesource_key")
	public Long getLocationIssueSourceKey() {
		return locationIssueSourceKey;
	}

	public void setLocationIssueSourceKey(Long locationIssueSourceKey) {
		this.locationIssueSourceKey = locationIssueSourceKey;
	}

}
