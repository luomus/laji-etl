package fi.laji.datawarehouse.dao.vertica;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class VerticaDimensionsResourceBaseService {

	private final String dimensionSchema;

	public VerticaDimensionsResourceBaseService(String dimensionSchema) {
		this.dimensionSchema = dimensionSchema;
	}

	protected void truncateTable(StatelessSession session, String realDbTableName) {
		if (session == null) throw new ETLException("Null session given");
		String sql = "TRUNCATE TABLE " + dimensionSchema + "." + realDbTableName;
		try {
			session.createSQLQuery(sql).executeUpdate();
		} catch (Exception e) {
			throw new ETLException(sql, e);
		}
	}

	protected void emptyTable(StatelessSession session, Class<?> entity) {
		if (session == null) throw new ETLException("Null session given");
		if (entity == null) throw new ETLException("Null entity given");
		String sql = "DELETE FROM " + entity.getSimpleName();
		try {
			session.createQuery(sql).executeUpdate();
		} catch (Exception e) {
			throw new ETLException(sql, e);
		}
	}

	protected void switchTableContents(StatelessSession session, Class<?> from, Class<?> to) {
		if (session == null) throw new ETLException("Null session given");
		if (from == null) throw new ETLException("Null 'from' entity given");
		if (to == null) throw new ETLException("Null 'to' entity given");

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(to.getSimpleName());
		sql.append(" ( ");
		fields(to, sql);
		sql.append(" ) ");
		sql.append(" SELECT ");
		fields(to, sql);
		sql.append(" FROM ").append(from.getSimpleName());


		try {
			session.getTransaction().begin();
			emptyTable(session, to);
			session.createQuery(sql.toString()).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			try {
				session.getTransaction().rollback();
			} catch (Exception e2) {}
			throw new ETLException(sql.toString(), e);
		}
	}

	private void fields(Class<?> to, StringBuilder sql) {
		List<String> fields = getFields(to);
		for (String field : fields) {
			sql.append(field).append(",");
		}
		Utils.removeLastChar(sql);
	}

	private static List<String> getFields(Class<?> type) {
		List<String> fields = new ArrayList<>();
		Collection<Method> getters = ReflectionUtil.getGetters(type);
		for (Method method : getters) {
			if (method.getAnnotation(OneToMany.class) != null) continue;
			if (method.getAnnotation(ManyToOne.class) != null) continue;
			if (method.getAnnotation(ManyToMany.class) != null) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(method);
			fields.add(fieldName);
		}
		return fields;
	}

	protected void close(StatelessSession session) {
		if (session != null) {
			try {
				session.close();
			} catch (Exception e) {}
		}
	}

}
