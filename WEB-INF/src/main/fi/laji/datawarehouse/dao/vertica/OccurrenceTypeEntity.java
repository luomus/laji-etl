package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="occurrencetype")
class OccurrenceTypeEntity extends NameableEntity {

	public OccurrenceTypeEntity() {}

	public OccurrenceTypeEntity(String typeId) {
		setId(typeId);
	}	

}
