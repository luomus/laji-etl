package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;

@Entity
@Table(name="taxoncensus")
public class TaxonCensusEntity implements Serializable {

	private static final long serialVersionUID = 5319421259052102022L;

	private Long gatheringKey;
	private Long taxonKey;
	private String type;

	public TaxonCensusEntity() {}

	public TaxonCensusEntity(TaxonCensus taxonCensus) throws CriticalParseFailure {
		try {
			this.taxonKey = TaxonBaseEntity.parseTaxonKey(taxonCensus.getTaxonId());
		} catch (Exception e) {
			throw new CriticalParseFailure("Invalid taxon key: " + taxonCensus.getTaxonId());
		}
		this.type = taxonCensus.getType().toURI();
	}

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return gatheringKey;
	}

	public void setGatheringKey(Long gatheringKey) {
		this.gatheringKey = gatheringKey;
	}

	@Id @Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Id @Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

}
