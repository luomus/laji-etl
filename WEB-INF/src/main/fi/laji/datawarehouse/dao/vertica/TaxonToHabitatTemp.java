package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_habitat_temp")
class TaxonToHabitatTemp extends TaxonToHabitatBaseEntity {

	private static final long serialVersionUID = -9168927468056558344L;

}
