package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="unit_gathering_fact")
class UnitGatheringFact extends Fact implements Serializable {

	private static final long serialVersionUID = 7718803582119126279L;

	@SuppressWarnings("deprecation")
	public UnitGatheringFact() {}

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return null;
	}

	public void setUnitKey(@SuppressWarnings("unused") Long unitKey) {

	}

	@Id @Column(name="property_key")
	public Long getPropertyKey() {
		return null;
	}

	public void setPropertyKey(@SuppressWarnings("unused") Long propertyKey) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
