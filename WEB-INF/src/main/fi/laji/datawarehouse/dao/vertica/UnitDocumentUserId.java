package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit_document_userid")
class UnitDocumentUserId implements Serializable {

	private static final long serialVersionUID = 7449357131871847673L;

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return null;
	}
	public void setUnitKey(@SuppressWarnings("unused") Long unitKey) {

	}

	@Id @Column(name="userid_key")
	public Long getUserIdKey() {
		return null;
	}
	public void setUserIdKey(@SuppressWarnings("unused") Long userIdKey) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="userid_key", referencedColumnName="key", insertable=false, updatable=false)
	public UserIdEntity getUserId() { // for queries only
		return null;
	}

	public void setUserId(@SuppressWarnings("unused") UserIdEntity userId) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
