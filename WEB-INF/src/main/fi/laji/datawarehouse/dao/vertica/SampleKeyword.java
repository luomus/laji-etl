package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sample_keyword")
class SampleKeyword implements Serializable {

	private static final long serialVersionUID = -3340456304829441113L;
	private Long sampleKey;
	private String keyword;

	@Id @Column(name="sample_key")
	public Long getSampleKey() {
		return sampleKey;
	}
	public void setSampleKey(Long sampleKey) {
		this.sampleKey = sampleKey;
	}

	@Id @Column(name="keyword")
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sample_key", insertable=false, updatable=false)
	public SampleEntity getSample() { // for queries only
		return null;
	}

	public void setSample(@SuppressWarnings("unused") SampleEntity document) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
