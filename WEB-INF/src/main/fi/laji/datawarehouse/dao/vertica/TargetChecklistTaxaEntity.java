package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="target_checklist_taxa")
public class TargetChecklistTaxaEntity implements Serializable {

	private static final long serialVersionUID = 7573209594283226652L;

	private Long targetKey;
	private String checklist;
	private Long taxonKey;
	private Boolean notExactTaxonMatch;

	public TargetChecklistTaxaEntity() {}

	@Transient
	public boolean resolvedSameAs(TargetChecklistTaxaEntity other) {
		if (other == null) throw new IllegalArgumentException("Null given");
		if (taxonKey == null) {
			if (other.taxonKey != null)
				return false;
		} else if (!taxonKey.equals(other.taxonKey))
			return false;
		if (notExactTaxonMatch == null) {
			if (other.notExactTaxonMatch != null)
				return false;
		} else if (!notExactTaxonMatch.equals(other.notExactTaxonMatch))
			return false;
		return true;
	}

	@Id @Column(name="target_key")
	public Long getTargetKey() {
		return targetKey;
	}

	public void setTargetKey(Long targetKey) {
		this.targetKey = targetKey;
	}

	@Id @Column(name="checklist")
	public String getChecklist() {
		return checklist;
	}

	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}

	@Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Column(name="not_exact_taxon_match")
	public Boolean getNotExactTaxonMatch() {
		return notExactTaxonMatch;
	}

	public void setNotExactTaxonMatch(Boolean notExactTaxonMatch) {
		this.notExactTaxonMatch = notExactTaxonMatch;
	}

	@Override
	public String toString() {
		return "TargetChecklistTaxaEntity [targetKey=" + targetKey + ", checklist=" + checklist + ", taxonKey=" + taxonKey + ", notExactTaxonMatch=" + notExactTaxonMatch + "]";
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}
}
