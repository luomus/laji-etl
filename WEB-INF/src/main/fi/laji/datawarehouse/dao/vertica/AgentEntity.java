package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;

@Entity
@Table(name="agent")
@AttributeOverride(name="id", column=@Column(name="name"))
class AgentEntity extends BaseEntity {

	public AgentEntity() {}

	public AgentEntity(String agentName) {
		setId(agentName);
	}

}
