package fi.laji.datawarehouse.dao.vertica;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

class NameableEntityNameLoader {

	private final VerticaDAOImpleDimensions dimensions;
	private final Class<? extends NameableEntity> entityClass;
	private final Map<String, String> allDescriptionsById;

	public NameableEntityNameLoader(VerticaDAOImpleDimensions dimensions, Map<String, String> allDescriptionsById, Class<? extends NameableEntity> entityClass) {
		this.dimensions = dimensions;
		this.entityClass = entityClass;
		this.allDescriptionsById = allDescriptionsById;
	}

	public void update()   {
		List<BaseEntity> entitiesToUpdate =  new ArrayList<>();
		List<NameableEntity> existingEntities = dimensions.getEntities(entityClass);

		for (Map.Entry<String, String> e : allDescriptionsById.entrySet()) {
			String id = e.getKey();
			String name = e.getValue();
			if (name == null) name = "";

			NameableEntity existing = getExisting(existingEntities, id);
			if (existing == null) continue;
			String existingName = existing.getName();
			if (existingName == null) existingName = "";

			if (name.equals(existing.getName())) continue;

			existing.setName(name);
			entitiesToUpdate.add(existing);
		}

		if (!entitiesToUpdate.isEmpty()) {
			dimensions.updateEntities(entitiesToUpdate);
		}
	}

	private NameableEntity getExisting(List<NameableEntity> existingEntities, String id) {
		for (NameableEntity e : existingEntities) {
			if (e.getId().equals(id)) return e;
		}
		return null;
	}

}
