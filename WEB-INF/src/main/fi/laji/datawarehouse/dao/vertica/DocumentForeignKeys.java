package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class DocumentForeignKeys {

	private Long sourceKey;
	private Long collectionKey;
	private Long formKey;
	private Long secureLevelKey;
	private Long documentIssueKey;
	private Long documentIssueSourceKey;

	public DocumentForeignKeys() {}

	@Column(name="source_key")
	public Long getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(Long dwSourceKey) {
		this.sourceKey = dwSourceKey;
	}

	@Column(name="collection_key")
	public Long getCollectionKey() {
		return collectionKey;
	}

	public void setCollectionKey(Long dwCollectionKey) {
		this.collectionKey = dwCollectionKey;
	}

	@Column(name="form_key")
	public Long getFormKey() {
		return formKey;
	}

	public void setFormKey(Long dwFormKey) {
		this.formKey = dwFormKey;
	}

	@Column(name="securelevel_key")
	public Long getSecureLevelKey() {
		return secureLevelKey;
	}

	public void setSecureLevelKey(Long dwsecureLevelKey) {
		this.secureLevelKey = dwsecureLevelKey;
	}

	@Column(name="document_issue_key")
	public Long getDocumentIssueKey() {
		return documentIssueKey;
	}

	public void setDocumentIssueKey(Long documentIssueKey) {
		this.documentIssueKey = documentIssueKey;
	}

	@Column(name="document_issuesource_key")
	public Long getDocumentIssueSourceKey() {
		return documentIssueSourceKey;
	}

	public void setDocumentIssueSourceKey(Long documentIssueSourceKey) {
		this.documentIssueSourceKey = documentIssueSourceKey;
	}

}
