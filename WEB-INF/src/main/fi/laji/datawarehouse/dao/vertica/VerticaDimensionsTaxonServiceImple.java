package fi.laji.datawarehouse.dao.vertica;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO.VerticaDimensionsTaxonService;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.OccurrenceType;
import fi.luomus.commons.containers.TaxonSet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;

public class VerticaDimensionsTaxonServiceImple extends VerticaDimensionsResourceBaseService implements VerticaDimensionsTaxonService {

	private static final Qname SPECIES_TAXON_RANK = new Qname("MX.species");

	private final VerticaDAOImpleDimensions dimensions;
	private final DAO dao;
	private final StatelessSession taxonTempInsertSession;
	private final Set<Qname> reportedMissingTaxonRanks = new HashSet<>();
	private final ThreadStatuses threadStatuses;

	public VerticaDimensionsTaxonServiceImple(VerticaDAOImpleDimensions dimensions, ThreadStatuses threadStatuses) {
		super(dimensions.getDimensionsSchema());
		this.threadStatuses = threadStatuses;
		this.dimensions = dimensions;
		this.dao = dimensions.getDao();
		this.taxonTempInsertSession = dimensions.getSession();
		this.taxonTempInsertSession.getTransaction().begin();
	}

	@Override
	public void emptyTaxonTempTable() {
		StatelessSession session = null;
		try {
			session = dimensions.getSession();
			super.truncateTable(session, "taxon_temp");
		} finally {
			super.close(session);
		}
	}

	@Override
	public void insertToTaxonTempTable(Taxon taxon) throws Exception {
		TaxonTempEntity taxonEntity = buildEntity(taxon);
		taxonTempInsertSession.insert(taxonEntity);
	}

	private TaxonTempEntity buildEntity(Taxon taxon) throws IllegalAccessException, InvocationTargetException {
		TaxonTempEntity taxonEntity = new TaxonTempEntity(taxon.getQname());
		Long key = taxonEntity.getKey();

		taxonEntity.setNameAccordingTo(taxon.getChecklist());
		taxonEntity.setTaxonomicOrder(taxon.getTaxonomicOrder());
		taxonEntity.setTaxonRankUsingQname(taxon.getTaxonRank());
		taxonEntity.setScientificName(taxon.getScientificName());
		taxonEntity.setScientificNameDisplayName(taxon.getScientificNameDisplayName());
		taxonEntity.setAuthor(taxon.getScientificNameAuthorship());
		taxonEntity.setNameFinnish(taxon.getVernacularName().forLocale("fi"));
		taxonEntity.setNameSwedish(taxon.getVernacularName().forLocale("sv"));
		taxonEntity.setNameEnglish(taxon.getVernacularName().forLocale("en"));
		if (taxon.getLatestRedListStatusFinland() != null) {
			taxonEntity.setRedListStatusUsingQname(taxon.getLatestRedListStatusFinland().getStatus());
			taxonEntity.setRedListStatusGroupUsingQname(taxon.getRedListStatusGroup());
		}
		taxonEntity.setFinnish(taxon.isFinnish());
		taxonEntity.setInvasive(taxon.isInvasiveObservationSpecies());
		taxonEntity.setSpecies(taxon.isSpecies());
		taxonEntity.setCursiveName(taxon.isCursiveName());
		taxonEntity.setSensitive(taxon.isSensitive());
		taxonEntity.setVirva(taxon.isVirva());

		if (taxon.getTaxonRank() != null) {
			Method setter = getTaxonRankSetter(taxon.getTaxonRank());
			if (setter != null) {
				setter.invoke(taxonEntity, key);
			}
		}
		if (taxon.hasParent()) {
			Long parentKey = TaxonBaseEntity.parseTaxonKey(taxon.getParent().getQname());
			taxonEntity.setParentId(parentKey);
			setParentTaxonRanks(taxonEntity, taxon);
		}

		if (taxonRankIsSpecies(taxon)) { // exactly species
			setSpeciesInfo(taxonEntity, taxon);
		} else {
			Taxon parentSpecies = taxon.getParentOfRank(SPECIES_TAXON_RANK);
			if (parentSpecies != null) {
				setSpeciesInfo(taxonEntity, parentSpecies);
			}
		}

		taxonEntity.setEuringCode(taxon.getEuringCode());
		taxonEntity.setEuringNumber(taxon.getEuringNumber());
		taxonEntity.setBirdlifeCode(taxon.getBirdlifeCode());

		if (taxon.getPrimaryHabitat() != null) {
			taxonEntity.setPrimaryHabitat(taxon.getPrimaryHabitat().toHabitatSearchString());
		}
		taxonEntity.setOccurrenceCount(taxon.getObservationCount());
		taxonEntity.setOccurrenceCountFinland(taxon.getObservationCountFinland());
		return taxonEntity;
	}

	private boolean taxonRankIsSpecies(Taxon taxon) {
		return SPECIES_TAXON_RANK.equals(taxon.getTaxonRank());
	}

	private void setSpeciesInfo(TaxonTempEntity taxonEntity, Taxon species) {
		taxonEntity.setSpeciesScientificName(species.getScientificName());
		taxonEntity.setSpeciesNameFinnish(species.getVernacularName().forLocale("fi"));
		taxonEntity.setSpeciesNameSwedish(species.getVernacularName().forLocale("sv"));
		taxonEntity.setSpeciesNameEnglish(species.getVernacularName().forLocale("en"));
		taxonEntity.setSpeciesTaxonomicOrder(species.getTaxonomicOrder());
	}

	private void setParentTaxonRanks(TaxonTempEntity taxonEntity, Taxon taxon) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (!taxon.hasParent()) return;
		Taxon parent = taxon.getParent();
		Qname parentRank = parent.getTaxonRank();
		if (parentRank != null) {
			Method setter = getTaxonRankSetter(parentRank);
			if (setter != null) {
				Long parentKey = TaxonBaseEntity.parseTaxonKey(parent.getQname());
				setter.invoke(taxonEntity, parentKey);
			} else {
				if (!reportedMissingTaxonRanks.contains(parentRank)) {
					dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), parentRank.toString(), new UnsupportedOperationException("Unknown taxon rank: " + parentRank));
					reportedMissingTaxonRanks.add(parentRank);
				}
			}
		}
		setParentTaxonRanks(taxonEntity, parent);
	}

	private Method getTaxonRankSetter(Qname taxonRankId) {
		try {
			String taxonRankSetterName = "set" + capitalizeFirst(taxonRankId.toString().replace("MX.", "") + "Id");
			return TaxonEntity.class.getMethod(taxonRankSetterName, Long.class);
		} catch (NoSuchMethodException e) {
			return null;
		}
	}

	private static String capitalizeFirst(String s) {
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}

	@Override
	public void switchTaxonTempToActual() {
		StatelessSession session = null;
		try {
			taxonTempInsertSession.getTransaction().commit();
			session = dimensions.getSession();
			super.switchTableContents(session, TaxonTempEntity.class, TaxonEntity.class);
		} finally {
			super.close(session);
		}
	}

	@Override
	public void generateInformalGroupLinkings() throws Exception {
		StatelessSession session = null;
		try {
			ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
			session = dimensions.getSession();
			tryToGenerateInformalGroupLinkings(statusReporter, session);
		} finally {
			super.close(session);
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private void tryToGenerateInformalGroupLinkings(ThreadStatusReporter statusReporter, StatelessSession session) throws Exception {
		statusReporter.setStatus("Emptying taxon informal group linkings temp table...");

		super.truncateTable(session, "TAXON_TAXONGROUP_TEMP");

		TaxonomyDAO taxonomyDAO = dao.getTaxonomyDAO();
		statusReporter.setStatus("Reading informal taxon group metadata...");
		Collection<InformalTaxonGroup> groups = taxonomyDAO.getInformalTaxonGroups().values();

		statusReporter.setStatus("Inserting informal taxon group linkings to temp table...");

		for (InformalTaxonGroup group : groups) {
			dimensions.getKey(new InformalTaxonGroupEntity(group.getQname().toURI()));
		}

		session.getTransaction().begin();
		int i = 0;
		for (InformalTaxonGroup group : groups) {
			i++;
			statusReporter.setStatus("Getting key for group " + group.getQname());
			Long groupKey = dimensions.getKey(new InformalTaxonGroupEntity(group.getQname().toURI()));
			TaxonToInformalTaxonGroupTemp taxonToGroup = new TaxonToInformalTaxonGroupTemp();
			taxonToGroup.setGroupKey(groupKey);
			Set<Qname> taxaOfGroup = taxonomyDAO.getTaxonContainer().getInformalGroupFilter().getFilteredTaxa(group.getQname());
			statusReporter.setStatus("Inserting taxons for group " + group.getQname() +" ("+i+"/"+groups.size()+"): " + taxaOfGroup.size() + " taxons");
			for (Qname taxonQname : taxaOfGroup) {
				taxonToGroup.setTaxonKey(TaxonEntity.parseTaxonKey(taxonQname));
				session.insert(taxonToGroup);
			}
		}
		session.getTransaction().commit();

		statusReporter.setStatus("Switching informal taxon group temp to actual table...");
		super.switchTableContents(session, TaxonToInformalTaxonGroupTemp.class, TaxonToInformalTaxonGroup.class);
		statusReporter.setStatus("Informal taxon group temp to actual table switched");
	}

	@Override
	public void generateTaxonSetLinkings() throws Exception {
		StatelessSession session = null;
		try {
			ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
			session = dimensions.getSession();
			tryToGenerateTaxonSetLinkings(statusReporter, session);
		} finally {
			super.close(session);
			threadStatuses.reportThreadDead(this.getClass());
		}
	}

	private void tryToGenerateTaxonSetLinkings(ThreadStatusReporter statusReporter, StatelessSession session) throws Exception {
		statusReporter.setStatus("Emptying taxon to set linkings temp table...");

		super.truncateTable(session, "TAXON_TAXONSET_TEMP");

		TaxonomyDAO taxonomyDAO = dao.getTaxonomyDAO();
		statusReporter.setStatus("Reading taxon set group metadata...");
		Collection<TaxonSet> sets = taxonomyDAO.getTaxonSets().values();

		statusReporter.setStatus("Inserting taxonset linkings to temp table...");

		for (TaxonSet set : sets) {
			dimensions.getKey(new TaxonSetEntity(set.getQname().toURI()));
		}

		session.getTransaction().begin();
		int i = 0;
		for (Taxon taxon : taxonomyDAO.getTaxonContainer().getAll()) {
			i++;
			for (Qname taxonSet : taxon.getTaxonSets()) {
				statusReporter.setStatus("Getting key for set " + taxonSet);
				Long setKey = dimensions.getKey(new TaxonSetEntity(taxonSet.toURI()));
				TaxonToTaxonSetTemp taxonToSet = new TaxonToTaxonSetTemp();
				taxonToSet.setSetKey(setKey);
				taxonToSet.setTaxonKey(TaxonEntity.parseTaxonKey(taxon.getQname()));
				statusReporter.setStatus("Inserting taxons for sets " + i);
				session.insert(taxonToSet);
			}
		}
		session.getTransaction().commit();

		statusReporter.setStatus("Switching taxon to set temp to actual table...");
		super.switchTableContents(session, TaxonToTaxonSetTemp.class, TaxonToTaxonSet.class);
		statusReporter.setStatus("Taxon to set temp to actual table switched");
	}

	@Override
	public void generateAdminStatusLinkings() throws Exception {
		StatelessSession session = null;
		try {
			ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
			session = dimensions.getSession();
			statusReporter.setStatus("Emptying taxon administrative status linkings temp table...");

			super.truncateTable(session, "TAXON_ADMINSTATUS_TEMP");

			TaxonomyDAO taxonomyDAO = dao.getTaxonomyDAO();
			statusReporter.setStatus("Inserting administrative status linkings to temp table...");

			Collection<AdministrativeStatus> statuses = taxonomyDAO.getAdministrativeStatuses().values();

			for (AdministrativeStatus status : statuses) {
				dimensions.getKey(new AdministrativeStatusEntity(status.getQname().toURI()));
			}

			session.getTransaction().begin();
			for (AdministrativeStatus status : statuses) {
				Long statusKey = dimensions.getKey(new AdministrativeStatusEntity(status.getQname().toURI()));
				TaxonToAdministrativeStatusTemp taxonToStatus = new TaxonToAdministrativeStatusTemp();
				taxonToStatus.setStatusKey(statusKey);
				for (Qname taxonQname : taxonomyDAO.getTaxonContainer().getAdminStatusFilter().getFilteredTaxa(status.getQname())) {
					taxonToStatus.setTaxonKey(TaxonEntity.parseTaxonKey(taxonQname));
					session.insert(taxonToStatus);
				}
			}
			session.getTransaction().commit();
			statusReporter.setStatus("Switching administrative status temp to actual table...");
			switchTableContents(session, TaxonToAdministrativeStatusTemp.class, TaxonToAdministrativeStatus.class);
			statusReporter.setStatus("Administrative status temp to actual table switched");
		} finally {
			threadStatuses.reportThreadDead(this.getClass());
			super.close(session);
		}
	}

	@Override
	public void generateOccurrenceTypeLinkings() throws Exception {
		StatelessSession session = null;
		try {
			ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
			session = dimensions.getSession();
			statusReporter.setStatus("Emptying taxon occurrence type linkings temp table...");

			super.truncateTable(session, "taxon_occurrencetype_temp");

			TaxonomyDAO taxonomyDAO = dao.getTaxonomyDAO();
			statusReporter.setStatus("Inserting occurrence type linkings to temp table...");

			Collection<OccurrenceType> types = taxonomyDAO.getOccurrenceTypes().values();

			for (OccurrenceType type : types) {
				dimensions.getKey(new OccurrenceTypeEntity(type.getQname().toURI()));
			}

			session.getTransaction().begin();
			for (OccurrenceType type : types) {
				Long typeKey = dimensions.getKey(new OccurrenceTypeEntity(type.getQname().toURI()));
				TaxonToTypeOfOccurrenceTemp taxonToType = new TaxonToTypeOfOccurrenceTemp();
				taxonToType.setTypeKey(typeKey);
				for (Qname taxonQname : taxonomyDAO.getTaxonContainer().getTypesOfOccurrenceFilter().getFilteredTaxa(type.getQname())) {
					taxonToType.setTaxonKey(TaxonEntity.parseTaxonKey(taxonQname));
					session.insert(taxonToType);
				}
			}
			session.getTransaction().commit();
			statusReporter.setStatus("Switching occurrence type temp to actual table...");
			switchTableContents(session, TaxonToTypeOfOccurrenceTemp.class, TaxonToTypeOfOccurrence.class);
			statusReporter.setStatus("Occurrence type temp to actual table switched");
		} finally {
			threadStatuses.reportThreadDead(this.getClass());
			super.close(session);
		}
	}

	@Override
	public void generateHabitatLinkings() throws Exception {
		StatelessSession session = null;
		try {
			ThreadStatusReporter statusReporter = threadStatuses.getThreadStatusReporterFor(this.getClass());
			session = dimensions.getSession();
			statusReporter.setStatus("Emptying taxon habitat linkings temp table...");

			super.truncateTable(session, "taxon_habitat_temp");

			TaxonomyDAO taxonomyDAO = dao.getTaxonomyDAO();
			statusReporter.setStatus("Inserting habitat linkings to temp table...");
			session.getTransaction().begin();
			for (Taxon taxon : taxonomyDAO.getTaxonContainer().getAll()) {
				addHabitatLinking(taxon.getQname(), taxon.getPrimaryHabitat(), true, session);
				for (HabitatObject h : taxon.getSecondaryHabitats()) {
					addHabitatLinking(taxon.getQname(), h, false, session);
				}
			}
			session.getTransaction().commit();
			statusReporter.setStatus("Switching habitat temp to actual table...");
			switchTableContents(session, TaxonToHabitatTemp.class, TaxonToHabitat.class);
			statusReporter.setStatus("Habitat temp to actual table switched");
		} finally {
			threadStatuses.reportThreadDead(this.getClass());
			super.close(session);
		}
	}

	private void addHabitatLinking(Qname taxonQname, HabitatObject habitatObject, boolean primary, StatelessSession session) {
		if (habitatObject == null) return;
		TaxonToHabitatTemp taxonToHabitat = new TaxonToHabitatTemp();
		taxonToHabitat.setTaxonKey(TaxonEntity.parseTaxonKey(taxonQname));
		taxonToHabitat.setPrimary(primary);
		taxonToHabitat.setHabitat(habitatObject.toHabitatSearchString());
		session.insert(taxonToHabitat);
	}

	@Override
	public void close() {
		super.close(taxonTempInsertSession);
	}

}
