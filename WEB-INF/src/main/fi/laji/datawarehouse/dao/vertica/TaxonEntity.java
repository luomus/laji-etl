package fi.laji.datawarehouse.dao.vertica;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="taxon")
class TaxonEntity extends TaxonBaseEntity {

	public TaxonEntity() {}

	public TaxonEntity(Qname id) {
		super(id);
	}

	@OneToMany(mappedBy="taxon", fetch=FetchType.LAZY)
	public Set<TaxonToInformalTaxonGroup> getTaxonToGroup() {
		return null; // for queries only
	}

	public void setTaxonToGroup(@SuppressWarnings("unused") Set<TaxonToInformalTaxonGroup> taxonToInformalTaxonGroup) {
		// for queries only
	}

	@OneToMany(mappedBy="taxon", fetch=FetchType.LAZY)
	public Set<TaxonToTaxonSet> getTaxonToSet() {
		return null; // for queries only
	}

	public void setTaxonToSet(@SuppressWarnings("unused") Set<TaxonToTaxonSet> taxonToTaxonSet) {
		// for queries only
	}

	@OneToMany(mappedBy="taxon", fetch=FetchType.LAZY)
	public Set<TaxonToHabitat> getTaxonToHabitat() {
		return null; // for queries only
	}

	public void setTaxonToHabitat(@SuppressWarnings("unused") Set<TaxonToHabitat> taxonToHabitat) {
		// for queries only
	}

	@OneToMany(mappedBy="taxon", fetch=FetchType.LAZY)
	public Set<TaxonToAdministrativeStatus> getTaxonToStatus() {
		return null; // for queries only
	}

	public void setTaxonToStatus(@SuppressWarnings("unused") Set<TaxonToAdministrativeStatus> taxonToAdministrativeStatus) {
		// for queries only
	}

	@OneToMany(mappedBy="taxon", fetch=FetchType.LAZY)
	public Set<TaxonToTypeOfOccurrence> getTaxonToTypeOfOccurrence() {
		return null; // for queries only
	}

	public void setTaxonToTypeOfOccurrence(@SuppressWarnings("unused") Set<TaxonToTypeOfOccurrence> taxonToOccurreneType) {
		// for queries only
	}
}
