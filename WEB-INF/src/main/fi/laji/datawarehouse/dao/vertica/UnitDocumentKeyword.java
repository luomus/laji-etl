package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit_document_keyword")
class UnitDocumentKeyword implements Serializable {

	private static final long serialVersionUID = -479023396825300734L;

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return null;
	}
	public void setUnitKey(@SuppressWarnings("unused") Long unitKey) {

	}

	@Id @Column(name="keyword")
	public String getKeyword() {
		return null;
	}
	public void setKeyword(@SuppressWarnings("unused") String keyword) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
