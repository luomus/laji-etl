package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class GatheringArea implements Serializable {

	private static final long serialVersionUID = -8959951604684172323L;
	private Long gatheringKey;
	private Long areaKey;

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return gatheringKey;
	}
	public void setGatheringKey(Long gatheringKey) {
		this.gatheringKey = gatheringKey;
	}

	@Id @Column(name="area_key")
	public Long getAreaKey() {
		return areaKey;
	}
	public void setAreaKey(Long areaKey) {
		this.areaKey = areaKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
