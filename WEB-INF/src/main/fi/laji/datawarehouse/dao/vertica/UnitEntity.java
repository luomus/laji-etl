package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Unit;

@Entity
@Table(name="unit")
@AttributeOverride(name="id", column=@Column(name="unit_id"))
class UnitEntity extends NonAutogeneratingBaseEntity implements MediaContainer, FactContainer, AnnotationContainer {

	private Unit unit;
	private Gathering parentGathering;
	private Document parentDocument;
	private DocumentForeignKeys documentForeignKeys;
	private GatheringForeignKeys gatheringForeignKeys;
	private UnitForeignKeys foreignKeys = new UnitForeignKeys();
	private TaxonBaseEntity taxon;
	private Long aggregateRowCount;
	private Long securedCount;

	public UnitEntity() {}

	public UnitEntity(GatheringEntity gatheringEntity, Unit unit) {
		this.setUnit(unit);
		this.parentGathering = gatheringEntity.getGathering();
		this.parentDocument = gatheringEntity.getParentDocument();
		this.documentForeignKeys = gatheringEntity.getDocumentForeignKeys();
		this.gatheringForeignKeys = gatheringEntity.getForeignKeys();
		foreignKeys.setGatheringKey(gatheringEntity.getKey());
		foreignKeys.setGatheringId(gatheringEntity.getId());
		setId(unit.getUnitId().toURI());
		setKey(generateKey());
	}

	@Formula(value="count(*) over()")
	public Long getAggregateRowCount() {
		return aggregateRowCount;
	}

	public void setAggregateRowCount(Long aggregateRowCount) {
		this.aggregateRowCount = aggregateRowCount;
	}

	@Formula(value="count(CASE WHEN secured = true THEN 1 ELSE NULL END)")
	public Long getSecuredCount() {
		return securedCount;
	}

	public void setSecuredCount(Long securedCount) {
		this.securedCount = securedCount;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="target_key", referencedColumnName="key", insertable=false, updatable=false)
	public TargetEntity getTarget() {  // for queries only
		return null;
	}

	public void setTarget(@SuppressWarnings("unused") TargetEntity target) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="original_target_key", referencedColumnName="key", insertable=false, updatable=false)
	public TargetEntity getOriginalTarget() {  // for queries only
		return null;
	}

	public void setOriginalTarget(@SuppressWarnings("unused") TargetEntity target) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="team_key", referencedColumnName="key", insertable=false, updatable=false)
	public TeamEntity getTeam() {  // for queries only
		return null;
	}

	public void setTeam(@SuppressWarnings("unused") TeamEntity team) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="namedplace_id", referencedColumnName="id", insertable=false, updatable=false)
	public NamedPlaceEntity getNamedPlace() {
		return null; // for queries only
	}

	public void setNamedPlace(@SuppressWarnings("unused") NamedPlaceEntity e) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="collection_key", referencedColumnName="key", insertable=false, updatable=false)
	public CollectionEntity getCollection() {
		return null; // for queries only
	}

	public void setCollection(@SuppressWarnings("unused") CollectionEntity e) {
		// for hibernate query only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitFact> getUnitFacts() {
		return null; // for queries only
	}

	public void setUnitFacts(@SuppressWarnings("unused") Set<UnitFact> unitFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitMediaEntity> getUnitMedia() {
		return null; // for queries only
	}

	public void setUnitMedia(@SuppressWarnings("unused") Set<UnitMediaEntity> media) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<AnnotationEntity> getAnnotations() { // for queries only
		return null;
	}

	public void setAnnotations(@SuppressWarnings("unused") Set<AnnotationEntity> annotations) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<SampleEntity> getSamples() { // for queries only
		return null;
	}

	public void setSamples(@SuppressWarnings("unused") Set<SampleEntity> samples) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitKeyword> getUnitKeywords() {
		return null; // for queries only
	}

	public void setUnitKeywords(@SuppressWarnings("unused") Set<UnitKeyword> unitKeywords) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitEffectiveTag> getUnitEffectiveTags() {
		return null; // for queries only
	}

	public void setUnitEffectiveTags(@SuppressWarnings("unused") Set<UnitEffectiveTag> unitEffectiveTags) {
		// for queries only
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="notes", column=@Column(insertable=false, updatable=false))
	})
	public Gathering getParentGathering() {
		return parentGathering;
	}

	public void setParentGathering(Gathering gathering) {
		this.parentGathering = gathering;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="notes", column=@Column(insertable=false, updatable=false)),
		@AttributeOverride(name="partial", column=@Column(insertable=false, updatable=false))
	})
	public Document getParentDocument() {
		return parentDocument;
	}

	public void setParentDocument(Document parentDocument) {
		this.parentDocument = parentDocument;
	}

	@Embedded
	public UnitForeignKeys getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(UnitForeignKeys foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	@Embedded
	public DocumentForeignKeys getDocumentForeignKeys() {
		return documentForeignKeys;
	}

	public void setDocumentForeignKeys(DocumentForeignKeys documentForeignKeys) {
		this.documentForeignKeys = documentForeignKeys;
	}

	@Embedded
	public GatheringForeignKeys getGatheringForeignKeys() {
		return gatheringForeignKeys;
	}

	public void setGatheringForeignKeys(GatheringForeignKeys gatheringForeignKeys) {
		this.gatheringForeignKeys = gatheringForeignKeys;
	}

	@Embedded
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Transient
	public TaxonBaseEntity getTaxon() { // Here go taxon aggregate by fields
		return taxon;
	}

	public void setTaxon(TaxonBaseEntity taxon) {
		this.taxon = taxon;
	}

	@Override
	@Transient
	public List<MediaObject> revealMedia() {
		return getUnit().getMedia();
	}

	@Override
	@Transient
	public List<Fact> revealFacts() {
		return getUnit().getFacts();
	}

	@Override
	@Transient
	public List<Annotation> revealAnnotations() {
		return unit.getAnnotations();
	}

	@Override
	@Transient
	public AnnotationContainerInformation revealAnnotationContainerInformation() {
		return new AnnotationContainerInformation(parentDocument, gatheringForeignKeys.getDocumentKey(), parentGathering, unit, getKey());
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitDocumentUserId> getDocumentUserIds() {
		return null; // for queries only
	}

	public void setDocumentUserIds(@SuppressWarnings("unused") Set<UnitDocumentUserId> documentUserIds) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitDocumentSecureReason> getDocumentSecureReasons() {
		return null; // for queries only
	}

	public void setDocumentSecureReasons(@SuppressWarnings("unused") Set<UnitDocumentSecureReason> documentSecureReasons) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitDocumentKeyword> getDocumentKeywords() {
		return null; // for queries only
	}

	public void setDocumentKeywords(@SuppressWarnings("unused") Set<UnitDocumentKeyword> documentKeywords) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitDocumentFact> getDocumentFacts() {
		return null; // for queries only
	}

	public void setDocumentFacts(@SuppressWarnings("unused") Set<UnitDocumentFact> documentFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitDocumentMedia> getDocumentMedia() {
		return null; // for queries only
	}

	public void setDocumentMedia(@SuppressWarnings("unused") Set<UnitDocumentMedia> media) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitGatheringFact> getGatheringFacts() {
		return null; // for queries only
	}

	public void setGatheringFacts(@SuppressWarnings("unused") Set<UnitGatheringFact> gatheringFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitGatheringMedia> getGatheringMedia() {
		return null; // for queries only
	}

	public void setGatheringMedia(@SuppressWarnings("unused") Set<UnitGatheringMedia> media) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitGatheringUserId> getGatheringUserIds() {
		return null; // for queries only
	}

	public void setGatheringUserIds(@SuppressWarnings("unused") Set<UnitGatheringUserId> gatheringUserIds) {
		// for queries only
	}

	@OneToMany(mappedBy="unit", fetch=FetchType.LAZY)
	public Set<UnitGatheringTaxonCensus> getTaxonCensus() { // for queries only
		return null;
	}

	public void setTaxonCensus(@SuppressWarnings("unused") Set<UnitGatheringTaxonCensus> taxonCensus) {
		// for queries only
	}

}
