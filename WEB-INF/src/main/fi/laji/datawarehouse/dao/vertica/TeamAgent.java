package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="team_agent")
class TeamAgent implements Serializable {

	private static final long serialVersionUID = 3350528239040057342L;
	private Long teamKey;
	private Long agentKey;
	private Integer sequence;

	@Id @Column(name="team_key")
	public Long getTeamKey() {
		return teamKey;
	}

	public void setTeamKey(Long teamKey) {
		this.teamKey = teamKey;
	}

	@Id @Column(name="agent_key")
	public Long getAgentKey() {
		return agentKey;
	}

	public void setAgentKey(Long agentKey) {
		this.agentKey = agentKey;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="team_key", insertable=false, updatable=false)
	public TeamEntity getTeam() { // for queries only
		return null;
	}

	public void setTeam(@SuppressWarnings("unused") TeamEntity team) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="agent_key", referencedColumnName="key", insertable=false, updatable=false)
	public AgentEntity getAgent() { // for queries only
		return null;
	}

	public void setAgent(@SuppressWarnings("unused") AgentEntity agent) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
