package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit_effective_tag")
class UnitEffectiveTag implements Serializable {

	private static final long serialVersionUID = 5509195249820489814L;

	private Long unitKey;
	private Long tagKey;

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return unitKey;
	}

	public void setUnitKey(Long unitKey) {
		this.unitKey = unitKey;
	}

	@Id @Column(name="tag_key")
	public Long getTagKey() {
		return tagKey;
	}

	public void setTagKey(Long tagKey) {
		this.tagKey = tagKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

}
