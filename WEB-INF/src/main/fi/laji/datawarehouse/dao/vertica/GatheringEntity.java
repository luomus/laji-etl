package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;

@Entity
@Table(name="gathering")
@AttributeOverride(name="id", column=@Column(name="gathering_id"))
class GatheringEntity extends NonAutogeneratingBaseEntity implements MediaContainer, FactContainer {

	private Gathering gathering;
	private Document parentDocument;
	private DocumentForeignKeys documentForeignKeys;
	private GatheringForeignKeys foreignKeys = new GatheringForeignKeys();
	private Long aggregateRowCount;
	private Long securedCount;

	public GatheringEntity() {}

	public GatheringEntity(DocumentEntity documentEntity, Gathering gathering) {
		this.gathering = gathering;
		this.parentDocument = documentEntity.getDocument();
		this.documentForeignKeys = documentEntity.getForeignKeys();
		foreignKeys.setDocumentKey(documentEntity.getKey());
		foreignKeys.setDocumentId(documentEntity.getId());
		setId(gathering.getGatheringId().toURI());
		setKey(generateKey());
	}

	@Formula(value="count(*) over()")
	public Long getAggregateRowCount() {
		return aggregateRowCount;
	}

	public void setAggregateRowCount(Long aggregateRowCount) {
		this.aggregateRowCount = aggregateRowCount;
	}

	@Formula(value="count(CASE WHEN secured = true THEN 1 ELSE NULL END)")
	public Long getSecuredCount() {
		return securedCount;
	}

	public void setSecuredCount(Long securedCount) {
		this.securedCount = securedCount;
	}

	@Embedded
	public Gathering getGathering() {
		return gathering;
	}

	public void setGathering(Gathering gathering) {
		this.gathering = gathering;
	}

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="notes", column=@Column(insertable=false, updatable=false)),
		@AttributeOverride(name="partial", column=@Column(insertable=false, updatable=false))
	})
	public Document getParentDocument() {
		return parentDocument;
	}

	public void setParentDocument(Document parentDocument) {
		this.parentDocument = parentDocument;
	}

	@Embedded
	public DocumentForeignKeys getDocumentForeignKeys() {
		return documentForeignKeys;
	}

	public void setDocumentForeignKeys(DocumentForeignKeys documentForeignKeys) {
		this.documentForeignKeys = documentForeignKeys;
	}

	@Embedded
	public GatheringForeignKeys getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(GatheringForeignKeys gatheringForeignKeys) {
		this.foreignKeys = gatheringForeignKeys;
	}

	@Override
	@Transient
	public List<MediaObject> revealMedia() {
		return getGathering().getMedia();
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringFact> getGatheringFacts() {
		return null; // for queries only
	}

	public void setGatheringFacts(@SuppressWarnings("unused") Set<GatheringFact> gatheringFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringMedia> getGatheringMedia() {
		return null; // for queries only
	}

	public void setGatheringMedia(@SuppressWarnings("unused") Set<GatheringMedia> media) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringUserId> getGatheringUserIds() {
		return null; // for queries only
	}

	public void setGatheringUserIds(@SuppressWarnings("unused") Set<GatheringUserId> gatheringUserIds) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<TaxonCensusEntity> getTaxonCensus() { // for queries only
		return null;
	}

	public void setTaxonCensus(@SuppressWarnings("unused") Set<TaxonCensusEntity> taxonCensus) {
		// for queries only
	}

	@Override
	@Transient
	public List<Fact> revealFacts() {
		return getGathering().getFacts();
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="team_key", referencedColumnName="key", insertable=false, updatable=false)
	public TeamEntity getTeam() {  // for queries only
		return null;
	}

	public void setTeam(@SuppressWarnings("unused") TeamEntity team) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="namedplace_id", referencedColumnName="id", insertable=false, updatable=false)
	public NamedPlaceEntity getNamedPlace() {
		return null; // for queries only
	}

	public void setNamedPlace(@SuppressWarnings("unused") NamedPlaceEntity e) {
		// for hibernate query only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="collection_key", referencedColumnName="key", insertable=false, updatable=false)
	public CollectionEntity getCollection() {
		return null; // for queries only
	}

	public void setCollection(@SuppressWarnings("unused") CollectionEntity e) {
		// for hibernate query only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringDocumentUserId> getDocumentUserIds() {
		return null; // for queries only
	}

	public void setDocumentUserIds(@SuppressWarnings("unused") Set<GatheringDocumentUserId> documentUserIds) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringDocumentSecureReason> getDocumentSecureReasons() {
		return null; // for queries only
	}

	public void setDocumentSecureReasons(@SuppressWarnings("unused") Set<GatheringDocumentSecureReason> documentSecureReasons) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringDocumentKeyword> getDocumentKeywords() {
		return null; // for queries only
	}

	public void setDocumentKeywords(@SuppressWarnings("unused") Set<GatheringDocumentKeyword> documentKeywords) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringDocumentFact> getDocumentFacts() {
		return null; // for queries only
	}

	public void setDocumentFacts(@SuppressWarnings("unused") Set<GatheringDocumentFact> documentFacts) {
		// for queries only
	}

	@OneToMany(mappedBy="gathering", fetch=FetchType.LAZY)
	public Set<GatheringDocumentMedia> getDocumentMedia() {
		return null; // for queries only
	}

	public void setDocumentMedia(@SuppressWarnings("unused") Set<GatheringDocumentMedia> media) {
		// for queries only
	}

}
