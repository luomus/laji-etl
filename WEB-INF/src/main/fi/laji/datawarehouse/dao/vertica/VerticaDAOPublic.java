package fi.laji.datawarehouse.dao.vertica;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

public class VerticaDAOPublic extends VerticaDAOImple {

	public VerticaDAOPublic(VerticaDAOImpleSharedInitialization shared) {
		super(shared, shared.getSchema() + "_PUBLIC");
	}

	@Override
	protected Concealment concealment() {
		return Concealment.PUBLIC;
	}

}
