package fi.laji.datawarehouse.dao.vertica;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Fact;

interface FactContainer {

	public Long getKey();
	
	public List<Fact> revealFacts();
	
}
