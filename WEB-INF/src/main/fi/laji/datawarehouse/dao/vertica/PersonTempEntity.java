package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="person_temp")
class PersonTempEntity extends PersonBaseEntity {

	public PersonTempEntity() {}

	public PersonTempEntity(Qname id) {
		super(id);
	}

	public PersonTempEntity(PersonBaseEntity e) {
		this(Qname.fromURI(e.getId()));
		setName(e.getName());
		setLintuvaaraId(e.getLintuvaaraId());
		setOmaRiistaId(e.getOmaRiistaId());
		setEmail(e.getEmail());
	}

}
