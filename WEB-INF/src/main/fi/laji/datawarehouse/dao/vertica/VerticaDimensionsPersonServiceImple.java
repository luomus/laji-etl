package fi.laji.datawarehouse.dao.vertica;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.VerticaDimensionsDAO.VerticaDimensionsPersonService;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;

public class VerticaDimensionsPersonServiceImple extends VerticaDimensionsResourceBaseService implements VerticaDimensionsPersonService  {

	private final StatelessSession session;

	public VerticaDimensionsPersonServiceImple(VerticaDAOImpleDimensions dimensions) {
		super(dimensions.getDimensionsSchema());
		this.session = dimensions.getSession();
	}

	@Override
	public void emptyPersonTempTable() {
		super.truncateTable(session, "person_temp");
	}

	@Override
	public void insertToPersonTempTable(PersonBaseEntity e) {
		PersonTempEntity tempEntity = new PersonTempEntity(e);
		session.insert(tempEntity);
	}

	@Override
	public void switchPersonTempToActual() {
		super.switchTableContents(session, PersonTempEntity.class, PersonEntity.class);
	}

	@Override
	public void close() {
		super.close(session);
	}

}
