package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_taxongroup_temp")
class TaxonToInformalTaxonGroupTemp extends TaxonToInformalTaxonGroupBaseEntity {

	private static final long serialVersionUID = -246523435904032537L;
	
}
