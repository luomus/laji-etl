package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gathering_document_media")
public class GatheringDocumentMedia extends MediaBaseEntity {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

}
