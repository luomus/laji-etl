package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class TaxonToTaxonSetBaseEntity implements Serializable {

	private static final long serialVersionUID = -7476434804069280334L;

	private Long taxonKey;
	private Long setKey;

	@Id @Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Id @Column(name="set_key")
	public Long getSetKey() {
		return setKey;
	}

	public void setSetKey(Long groupKey) {
		this.setKey = groupKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
