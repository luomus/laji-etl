package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;

@Entity
@Table(name="sex")
class SexEntity extends BaseEntity {

	public SexEntity() {}
	
}
