package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="person")
class PersonEntity extends PersonBaseEntity {

	public PersonEntity() {}

	public PersonEntity(Qname id) {
		super(id);
	}

}
