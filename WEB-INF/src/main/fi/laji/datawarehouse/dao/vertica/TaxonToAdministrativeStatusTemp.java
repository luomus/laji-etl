package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_adminstatus_temp")
class TaxonToAdministrativeStatusTemp extends TaxonToAdministrativeStatusBaseEntity {

	private static final long serialVersionUID = -1086856532803368073L;

}
