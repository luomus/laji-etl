package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import fi.laji.datawarehouse.etl.utils.Util;

@MappedSuperclass
class NonAutogeneratingBaseEntity {

	private Long key;
	private String id;

	public NonAutogeneratingBaseEntity() {}

	public static Long generateKey() {
		return Util.generateKey();
	}

	@Id
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getId() {
		return id;
	}

	public NonAutogeneratingBaseEntity setId(String id) {
		this.id = id;
		return this;
	}

}