package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="gathering_fact")
class GatheringFact extends FactLink {

	private static final long serialVersionUID = -5651092178208867882L;

	public GatheringFact(Fact fact) {
		super(fact);
	}

	@Deprecated
	public GatheringFact() {}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

}
