package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@MappedSuperclass
class FactLink extends Fact implements Serializable {

	private static final long serialVersionUID = -7930485489487619236L;
	private Long parentKey;
	private Long propertyKey;

	public FactLink(Fact fact) {
		super(fact.getFact(), fact.getValue());
	}

	@Deprecated
	public FactLink() {}

	@Id @Column(name="parent_key")
	public Long getParentKey() {
		return parentKey;
	}

	public void setParentKey(Long parentKey) {
		this.parentKey = parentKey;
	}

	@Id @Column(name="property_key")
	public Long getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(Long propertyKey) {
		this.propertyKey = propertyKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
