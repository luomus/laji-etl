package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="document_userid")
class DocumentUserId implements Serializable {

	private static final long serialVersionUID = 2164300937483677757L;
	private Long documentKey;
	private Long userIdKey;

	@Id @Column(name="document_key")
	public Long getDocumentKey() {
		return documentKey;
	}
	public void setDocumentKey(Long documentKey) {
		this.documentKey = documentKey;
	}

	@Id @Column(name="userid_key")
	public Long getUserIdKey() {
		return userIdKey;
	}
	public void setUserIdKey(Long userIdKey) {
		this.userIdKey = userIdKey;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="userid_key", referencedColumnName="key", insertable=false, updatable=false)
	public UserIdEntity getUserId() { // for queries only
		return null;
	}

	public void setUserId(@SuppressWarnings("unused") UserIdEntity userId) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="document_key", referencedColumnName="key", insertable=false, updatable=false)
	public DocumentEntity getDocument() { // for queries only
		return null;
	}

	public void setDocument(@SuppressWarnings("unused") DocumentEntity document) {
		// for queries only
	}

}
