package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gathering_document_keyword")
class GatheringDocumentKeyword implements Serializable {

	private static final long serialVersionUID = -5666569526815358920L;

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return null;
	}
	public void setGatheringKey(@SuppressWarnings("unused") Long documentKey) {

	}

	@Id @Column(name="keyword")
	public String getKeyword() {
		return null;
	}
	public void setKeyword(@SuppressWarnings("unused") String keyword) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
