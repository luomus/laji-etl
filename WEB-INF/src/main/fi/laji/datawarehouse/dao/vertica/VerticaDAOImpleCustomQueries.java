package fi.laji.datawarehouse.dao.vertica;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.Streams.ScrollableResultsStream;
import fi.laji.datawarehouse.dao.VerticaQueryDAO.VerticaCustomQueriesDAO;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.containers.SourceDocumentLoadStatisticsData;
import fi.laji.datawarehouse.etl.models.containers.SourceDocumentLoadStatisticsData.Stat;
import fi.laji.datawarehouse.etl.models.containers.TeamMemberData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.luomus.commons.containers.rdf.Qname;

class VerticaDAOImpleCustomQueries implements VerticaCustomQueriesDAO {

	public static final String DIMENSION_SCHEMA = "<DIMENSION_SCHEMA>";
	public static final String SCHEMA = "<SCHEMA>";
	protected final VerticaDAOImple verticaDAO;

	public VerticaDAOImpleCustomQueries(VerticaDAOImple verticaDAO) {
		this.verticaDAO = verticaDAO;
	}

	@Override
	public Collection<ObsCount> getTaxonObservationCounts() {
		try {
			verticaDAO.dao.logMessage(Const.LAJI_ETL_QNAME, VerticaDAOImpleCustomQueries.class, "Loading taxon observation counts...");
			Map<Qname, ObsCount> counts = new HashMap<>();
			addObservationTotalCounts(counts);
			addObservationBiogeographicalProvinceCounts(counts);
			addObservationFinlandCounts(counts);
			addUnitHabitatCounts(counts);
			addGatheringHabitatCounts(counts);
			verticaDAO.dao.logMessage(Const.LAJI_ETL_QNAME, VerticaDAOImpleCustomQueries.class, "Taxon observation counts loaded!");
			return new ArrayList<>(counts.values());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void addObservationTotalCounts(Map<Qname, ObsCount> counts) {
		String sql = observationCountQuery("", "", "");
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname taxonId = new Qname("MX."+integer(row[0]));
				int count = integer(row[1]);
				ObsCount obsCount = getObsCount(counts, taxonId);
				obsCount.count = count;
			}
		}
	}

	private void addObservationFinlandCounts(Map<Qname, ObsCount> counts) {
		String sql = observationCountQuery("", "AND unit.country_key = (SELECT key FROM <DIMENSION_SCHEMA>.area WHERE id = '"+Const.FINLAND.toURI()+"')", "");
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname taxonId = new Qname("MX."+integer(row[0]));
				int count = integer(row[1]);
				ObsCount obsCount = getObsCount(counts, taxonId);
				obsCount.countFinland = count;
			}
		}
	}

	private void addObservationBiogeographicalProvinceCounts(Map<Qname, ObsCount> counts) {
		String sql = observationCountQuery("area.id", "AND bioprovince_key IS NOT NULL", "JOIN <DIMENSION_SCHEMA>.area ON area.key = unit.bioprovince_key");
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname taxonId = new Qname("MX."+integer(row[0]));
				Qname area = Qname.fromURI(string(row[1]));
				int count = integer(row[2]);
				ObsCount obsCount = getObsCount(counts, taxonId);
				if (obsCount.biogeographicalProvinceCounts == null) {
					obsCount.biogeographicalProvinceCounts = new HashMap<>();
				}
				obsCount.biogeographicalProvinceCounts.put(area, count);
			}
		}
	}

	private void addGatheringHabitatCounts(Map<Qname, ObsCount> counts) {
		String sql = observationCountQuery("gf.value", "", "" +
				"JOIN <SCHEMA>.gf ON gf.gathering_key = unit.gathering_key AND gf.property in (" +
				"'http://tun.fi/MY.batHabitat', 'http://tun.fi/MY.habitat', 'http://tun.fi/MY.habitatDescription' ) " +
				"AND LENGTH(gf.value) < 50");
		addHabitatCount(counts, sql);
	}

	private void addUnitHabitatCounts(Map<Qname, ObsCount> counts) {
		String sql = observationCountQuery("uf.value", "", "JOIN <SCHEMA>.uf ON uf.unit_key = unit.key AND uf.property = 'http://tun.fi/MY.habitatIUCN' AND LENGTH(uf.value) < 50");
		addHabitatCount(counts, sql);
	}

	private void addHabitatCount(Map<Qname, ObsCount> counts, String sql) {
		try (ResultStream<Object[]> results = getCustomQuery(sql)) {
			Iterator<Object[]> i = results.iterator();
			while (i.hasNext()) {
				Object[] row = i.next();
				Qname taxonId = new Qname("MX."+integer(row[0]));
				String habitat = string(row[1]);
				int count = integer(row[2]);
				ObsCount obsCount = getObsCount(counts, taxonId);
				addHabitatCount(obsCount, habitat, count);
			}
		}
	}

	private void addHabitatCount(ObsCount obsCount, String habitat, int count) {
		if (obsCount.habitatOccurrenceCounts == null) {
			obsCount.habitatOccurrenceCounts = new HashMap<>();
		}
		if (obsCount.habitatOccurrenceCounts.size() > 30) return; // take only top 30 habitat per taxa
		if (!habitat.startsWith("http://")) {
			habitat = habitat.toLowerCase();
			if (count == 1) return; // ignore all habitats that have been used only once (most likely not useful values)
		}
		Integer existing = obsCount.habitatOccurrenceCounts.get(habitat);
		if (existing == null) existing = 0;
		obsCount.habitatOccurrenceCounts.put(habitat, count + existing);
	}

	private ObsCount getObsCount(Map<Qname, ObsCount> counts, Qname taxonId) {
		if (!counts.containsKey(taxonId)) {
			counts.put(taxonId, new ObsCount(taxonId));
		}
		return counts.get(taxonId);
	}

	private String string(Object object) {
		if (object == null) return null;
		return object.toString();
	}

	private Integer integer(Object object) {
		if (object == null) return null;
		return ((BigInteger) object).intValue();
	}

	private String observationCountQuery(String aggregateBy, String whereCondition, String join) {
		StringBuilder b = new StringBuilder();
		b.append("SELECT taxon.key");
		if (!aggregateBy.isEmpty()) {
			b.append(", ").append(aggregateBy);
		}
		b.append(", COUNT(*) as total FROM <SCHEMA>.unit JOIN <DIMENSION_SCHEMA>.target ON target.key = unit.target_key JOIN <DIMENSION_SCHEMA>.taxon ON taxon.key = target.taxon_key ");
		if (!join.isEmpty()) {
			b.append(join).append(" ");
		}
		b.append("WHERE    unit.individualcount > 0 ");
		b.append("AND      unit.issues = false ");
		b.append("AND      (unit.reliability_key IS NULL OR unit.reliability_key IN (SELECT key FROM <DIMENSION_SCHEMA>.reliability WHERE id IN ('RELIABLE','UNDEFINED'))) ");
		b.append("AND      (unit.needs_check IS NULL OR unit.needs_check = false) ");
		if (!whereCondition.isEmpty()) {
			b.append(whereCondition).append(" ");
		}
		b.append("GROUP BY taxon.key");
		if (!aggregateBy.isEmpty()) {
			b.append(", ").append(aggregateBy);
		}
		b.append(" ");
		b.append("ORDER BY taxon.key, total DESC");
		return b.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResultStream<Object[]> getCustomQuery(String sql) {
		sql = sql.replace(SCHEMA, verticaDAO.dataSchema).replaceAll(DIMENSION_SCHEMA, verticaDAO.dimensionsSchema);
		StatelessSession session = verticaDAO.getQuerySession();
		ScrollableResults results = session.createSQLQuery(sql).setFetchSize(4000).setReadOnly(true).scroll(ScrollMode.FORWARD_ONLY);
		return new ScrollableResultsStream<>(results, session);
	}

	@Override
	public SourceDocumentLoadStatisticsData getLoadStats() {
		StatelessSession session = null;
		SourceDocumentLoadStatisticsData data = new SourceDocumentLoadStatisticsData();
		try {
			session = verticaDAO.getQuerySession();
			lastLoadedSince(Stat.MONTH, "1 months", session, data);
			lastLoadedSince(Stat.WEEK, "7 days", session, data);
			lastLoadedSince(Stat.DAY, "1 days", session, data);
		} finally {
			if (session != null) session.close();
		}
		return data;
	}

	private void lastLoadedSince(Stat stat, String timeInterval, StatelessSession session, SourceDocumentLoadStatisticsData data) {
		List<?> queryResult = session.createSQLQuery("" +
				" SELECT	source.id, count(1) " +
				" FROM		"+verticaDAO.dataSchema+".document " +
				" JOIN		"+verticaDAO.dimensionsSchema+".source on (document.source_key = source.key) " +
				" WHERE 	document.load_date > current_date - interval '"+timeInterval+"' " +
				" GROUP BY 	source.id " +
				" ORDER BY 	source.id "
				).list();
		for (Object result : queryResult) {
			Object[] row = (Object[]) result;
			Qname source = Qname.fromURI(((String) row[0]));
			Integer count = ((BigInteger) row[1]).intValue();
			data.addStat(stat, source, count);
		}
	}

	@Override
	public List<UnlinkedUserIdsData> getUnlinkedUserIds() {
		List<UnlinkedUserIdsData> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT   userid.id as user_id, source.id as source_id, COUNT(1) c " +
					" FROM     "+verticaDAO.dataSchema+".document " +
					" JOIN     "+verticaDAO.dataSchema+".document_userid du ON (document.key = du.document_key) " +
					" JOIN     "+verticaDAO.dimensionsSchema+".userid ON (du.userid_key = userid.key) " +
					" JOIN     "+verticaDAO.dimensionsSchema+".source ON (document.source_key = source.key) " +
					" WHERE    userid.person_key IS NULL " +
					" GROUP BY userid.id, source.id " +
					" ORDER BY c desc " +
					" LIMIT 1500 ")
					.list();
			for (Object result : queryResult) {
				Object[] row = (Object[]) result;
				String userId = (String) row[0];
				String sourceURI = (String) row[1];
				int count = ((BigInteger) row[2]).intValue();
				Qname sourceId = Qname.fromURI(sourceURI);
				results.add(new UnlinkedUserIdsData(userId, sourceId, count));
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public List<UnlinkedTargetNameData> getUnlinkedTargetNames() {
		List<UnlinkedTargetNameData> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT          u.taxon_verbatim, r.id, t.not_exact_taxon_match, t.taxon_key, count(*) c " +
					" FROM            " + verticaDAO.dataSchema + ".unit u " +
					" JOIN            " + verticaDAO.dimensionsSchema + ".target t ON (u.target_key = t.key) " +
					" LEFT JOIN       " + verticaDAO.dimensionsSchema + ".reference r ON (u.reference_key = r.key) " +
					" WHERE           (t.taxon_key IS NULL OR t.not_exact_taxon_match = true)" +
					" GROUP BY        u.taxon_verbatim, r.id, t.not_exact_taxon_match, t.taxon_key " +
					" ORDER BY        c DESC " +
					" LIMIT 1500 ")
					.list();
			for (Object result : queryResult) {
				Object[] row = (Object[]) result;
				String target = (String) row[0];
				if (target == null || target.length() < 1) continue;
				String referenceUri = (String) row[1];
				Boolean notExactMatch = (Boolean) row[2];
				BigInteger taxonkey = ((BigInteger) row[3]);
				Qname taxonId = taxonkey == null ? null : new Qname("MX." + taxonkey.intValue());
				int count = ((BigInteger) row[4]).intValue();
				results.add(new UnlinkedTargetNameData(target, referenceUri == null ? null : Qname.fromURI(referenceUri), notExactMatch, taxonId, count));
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public List<Annotation> getInvasiveControlled() {
		List<Annotation> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT  controlEvents.control, unit.document_id, unit.unit_id, controlEvents.document_id AS relatesTo " +
					" FROM ( " +
					"         SELECT   unit.document_id AS document_id, gf.value AS control, target.taxon_key AS taxon_key, unit.dateend_key AS date_key, " +
					"                  unit.EUREFNMIN AS n_min, unit.EUREFNMAX AS n_max, unit.EUREFEMIN AS e_min, unit.EUREFEMAX AS e_max " +
					"         FROM     " + verticaDAO.dataSchema + ".unit " +
					"         JOIN     " + verticaDAO.dimensionsSchema + ".target ON (unit.target_key = target.key) " +
					"         JOIN     " + verticaDAO.dataSchema + ".gf ON (gf.gathering_key = unit.gathering_key AND gf.property = 'http://tun.fi/MY.invasiveControlEffectiveness') " +
					"         JOIN     " + verticaDAO.dataSchema + ".gathering_geometry geo ON (unit.gathering_key = geo.gathering_key) " +
					"         WHERE    collection_key = (SELECT key FROM " + verticaDAO.dimensionsSchema + ".collection WHERE id = '" + Const.INVASIVE_ALIEN_SPECIES_CONTROL_COLLECTION_ID.toURI() + "') " +
					"         AND      actual_load_date > current_date - interval '7 days' " +
					"         AND      target.taxon_key IS NOT NULL " +
					"         AND      issues = false " +
					" ) controlEvents " +
					" JOIN " + verticaDAO.dimensionsSchema + ".target on target.taxon_key = controlEvents.taxon_key " +
					" JOIN " + verticaDAO.dataSchema + ".unit on ( " +
					"    unit.target_key = target.key AND " +
					"    unit.dateend_key <= controlEvents.date_key AND " +
					"    unit.invasivecontrolled = false AND " +
					"    unit.issues = false AND " +
					"    unit.coordinateaccuracy <= 150 AND " +
					"    unit.eurefnmin <= controlEvents.n_max AND " +
					"    unit.eurefnmax >= controlEvents.n_min AND " +
					"    unit.eurefemin <= controlEvents.e_max AND " +
					"    unit.eurefemax >= controlEvents.e_min " +
					" ) " +
					" WHERE unit.reliability_key NOT IN (SELECT key FROM " + verticaDAO.dimensionsSchema + ".reliability WHERE id = 'UNRELIABLE') "
					)
					.list();
			for (Object result : queryResult) {
				Object[] row = (Object[]) result;
				Qname invasiveControlEffectiveness = Qname.fromURI((String) row[0]);
				InvasiveControl c = (InvasiveControl) EnumToProperty.getInstance().get(invasiveControlEffectiveness);
				Qname rootId = Qname.fromURI((String) row[1]);
				Qname unitId = Qname.fromURI((String) row[2]);
				//Qname relatesTo = Qname.fromURI((String) row[3]); // see bellow
				Annotation annotation = Annotation.emptyAnnotation();
				try {
					annotation.setRootID(rootId);
					annotation.setTargetID(unitId);
					annotation.addTag(Annotation.toTag(c));
					//annotation.setRelatesTo(new ArrayList<>(Utils.list(relatesTo))); TODO do we want to show from what control event the invasive control comes from?
					annotation.setAnnotationBySystem(Const.LAJI_ETL_QNAME);
					annotation.setCreatedTimestamp(Util.now());
				} catch (CriticalParseFailure e) {
					throw new RuntimeException(e);
				}
				results.add(annotation);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public ResultStream<String> getAllDocumentIds() {
		StatelessSession session = verticaDAO.getETLSession();
		ScrollableResults results = session.createSQLQuery("" +
				" SELECT    DISTINCT document_id " +
				" FROM      " + verticaDAO.dataSchema + ".document ")
				.setFetchSize(10000)
				.scroll(ScrollMode.FORWARD_ONLY);
		return new ScrollableResultsStream<>(results, session, String.class);
	}

	@Override
	public Collection<String> getdDocumentIdsWithSecureLevel() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".document " +
					" WHERE     securelevel_key != (SELECT key FROM " + verticaDAO.dimensionsSchema + ".securelevel WHERE id = 'NONE') ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithLocationIssues() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" WHERE     location_issue_key is not null ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithQualityIssues() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" WHERE     issues = true ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsFromMunicipality(Qname municipalityId) {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" JOIN      " + verticaDAO.dimensionsSchema + " .area ON (gathering.municipality_key = area.key) " +
					" WHERE     area.id = ? ")
					.setString(0, municipalityId.toURI())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsFromCountry(Qname countryId) {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" JOIN      " + verticaDAO.dimensionsSchema + " .area ON (gathering.country_key = area.key) " +
					" WHERE     area.id = ? ")
					.setString(0, countryId.toURI())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsNotFromCountry(Qname countryId) {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" WHERE     gathering.country_key != (SELECT key FROM  " + verticaDAO.dimensionsSchema + ".area WHERE id = ?) ")
					.setString(0, countryId.toURI())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithMultipleMunicipalities() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT 	DISTINCT g.document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering_municipality gm " +
					" JOIN      " + verticaDAO.dataSchema + ".gathering g on (gm.gathering_key = g.key) ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithMultipleBioprovinces() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT 	DISTINCT g.document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering_bioprovince gp " +
					" JOIN      " + verticaDAO.dataSchema + ".gathering g on (gp.gathering_key = g.key) ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Set<String> getDocumentIdsFromSource(Qname sourceId) {
		Set<String> results = new HashSet<>(5000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".document " +
					" JOIN      " + verticaDAO.dimensionsSchema + " .source ON (document.source_key = source.key) " +
					" WHERE     source.id = ? ")
					.setString(0, sourceId.toURI())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithLargeAreas() {
		Collection<String> results = new ArrayList<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT 	DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" WHERE		bbox_area > 100000000 ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsFromCollection(Qname collectionId) {
		Set<String> results = new HashSet<>(10000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".document " +
					" JOIN      " + verticaDAO.dimensionsSchema + " .collection ON (document.collection_key = collection.key) " +
					" WHERE     collection.id = ? ")
					.setString(0, collectionId.toURI())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Date getLatestLoadTimestamp() {
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			return (Date) session.createSQLQuery(" select max(actual_load_date) from " + verticaDAO.dataSchema + ".document ").uniqueResult();
		} finally {
			if (session != null) session.close();
		}
	}

	@Override
	public boolean isTaxonIdUsed(Qname taxonId) {
		StatelessSession session = null;
		try {
			// TODO taxon use queries: used in annotation target; used as autocompleteSelectedTaxonId; ...
			session = verticaDAO.getQuerySession();
			if (usedAsTarget(taxonId, session)) return true;
			return usedInTaxonCensus(taxonId, session);
		} finally {
			if (session != null) session.close();
		}
	}

	private boolean usedInTaxonCensus(Qname taxonId, StatelessSession session) {
		Long taxonkey = TaxonEntity.parseTaxonKey(taxonId);
		String sql = "" +
				" select count(1) " +
				" from "+verticaDAO.dataSchema+".taxoncensus " +
				" where taxon_key = ? ";
		SQLQuery query = session.createSQLQuery(sql);
		query.setLong(0, taxonkey);
		BigInteger count = (BigInteger) query.uniqueResult();
		return count.intValue() > 0;
	}

	private boolean usedAsTarget(Qname taxonId, StatelessSession session) {
		if (taxonId == null || !taxonId.isSet()) throw new IllegalArgumentException("taxonId not given");
		String sql = "" +
				" select count(1) " +
				" from "+verticaDAO.dataSchema+".target " +
				" join "+verticaDAO.dataSchema+".unit on target_key = target.key " +
				" where target_lowercase in (?, ?)";
		SQLQuery query = session.createSQLQuery(sql);
		query.setString(0, TaxonLinkingService.cleanTargetName(taxonId.toString()));
		query.setString(1, TaxonLinkingService.cleanTargetName(taxonId.toURI()));
		BigInteger count = (BigInteger) query.uniqueResult();
		return count.intValue() > 0;
	}

	@Override
	public Collection<String> getDocumentIdsWithUnitImages() {
		Set<String> results = new HashSet<>(1000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" select distinct document_id " +
					" from " + verticaDAO.dataSchema + ".unit " +
					" where  UNIT_MEDIACOUNT > 0 ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithMultipleImages() {
		Set<String> results = new HashSet<>(1000);
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" select distinct document_id " +
					" from " + verticaDAO.dataSchema + ".unit " +
					" where  DOCUMENT_MEDIACOUNT > 1 or GATHERING_MEDIACOUNT > 1 or UNIT_MEDIACOUNT > 1 ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithoutCoordinates() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" WHERE 	latitudemin is null ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public List<TeamMemberData> getTeamMembers(String searchWord) {
		List<TeamMemberData> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT	agent.key, agent.name, COUNT(1) " +
					" FROM		" + verticaDAO.dataSchema + ".unit " +
					" JOIN		" + verticaDAO.dimensionsSchema + ".team_agent ON unit.team_key = team_agent.team_key " +
					" JOIN		" + verticaDAO.dimensionsSchema + ".agent ON team_agent.agent_key = agent.key " +
					" WHERE		agent.name ILIKE ? " +
					" GROUP BY	agent.key, agent.name " +
					" ORDER BY	count(1) desc ")
					.setString(0, searchWord.replace("*", "%"))
					.list();
			for (Object result : queryResult) {
				Object[] row = (Object[]) result;
				int agentKey = ((BigInteger) row[0]).intValue();
				String name = (String) row[1];
				int count = ((BigInteger) row[2]).intValue();
				results.add(new TeamMemberData(agentKey, name, count));
			}
		} finally {
			if (session != null) session.close();
		}
		return results;

	}

	@Override
	public Collection<String> getDocumentIdsWithUnitsWithoutTaxonLinking() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" JOIN      " + verticaDAO.dimensionsSchema + ".target ON unit.target_key = target.key " +
					" WHERE 	target.taxon_key is null ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithOldStartDate() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".gathering " +
					" WHERE 	datebegin_key <= 17001231 ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithUnknownPersons() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT DISTINCT annotation.document_id " +
					" FROM  " + verticaDAO.dataSchema + ".annotation " +
					" JOIN  " + verticaDAO.dataSchema + ".document ON annotation.document_key = document.key " +
					" WHERE annotation.person_name LIKE '%Unknown person%' " +
					" UNION" +
					" SELECT DISTINCT document_id " +
					" FROM  " + verticaDAO.dimensionsSchema + ".team " +
					" JOIN  " + verticaDAO.dataSchema + ".gathering ON gathering.team_key = team.key " +
					" WHERE team_text LIKE '%Unknown person%' ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithUnitAnnotations() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" WHERE 	unit_annotationcount > 0 ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithUnitsMarkedNeedDet() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" WHERE 	needs_det = true ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDocumentIdsWithoutTaxonLinking() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" JOIN      " + verticaDAO.dimensionsSchema + ".target ON unit.target_key = target.key " +
					" WHERE     target.taxon_key IS NULL OR target.NOT_EXACT_TAXON_MATCH = true ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getNoTargetUnitDocumentIds() {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT    DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" LEFT JOIN " + verticaDAO.dimensionsSchema + ".target ON unit.target_key = target.key " +
					" WHERE     unit.target_key IS NOT NULL " +
					" AND       target.key IS NULL ")
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

	@Override
	public Collection<String> getDodocumentIdsWithQuality(RecordQuality quality) {
		Collection<String> results = new ArrayList<>();
		StatelessSession session = null;
		try {
			session = verticaDAO.getQuerySession();
			List<?> queryResult = session.createSQLQuery("" +
					" SELECT	DISTINCT document_id " +
					" FROM      " + verticaDAO.dataSchema + ".unit " +
					" WHERE     unit.recordquality_key = (SELECT key FROM " + verticaDAO.dimensionsSchema + ".recordquality WHERE id = ?) ")
					.setString(0, quality.name())
					.list();
			for (Object result : queryResult) {
				String documentId = (String) result;
				results.add(documentId);
			}
		} finally {
			if (session != null) session.close();
		}
		return results;
	}

}