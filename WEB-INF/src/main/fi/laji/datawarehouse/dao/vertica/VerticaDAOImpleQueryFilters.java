package fi.laji.datawarehouse.dao.vertica;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation.AnnotationType;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Feature;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.CoordinatesWithOverlapRatio;
import fi.laji.datawarehouse.query.model.Filter;
import fi.laji.datawarehouse.query.model.Filters;
import fi.laji.datawarehouse.query.model.Filters.QualityIssues;
import fi.laji.datawarehouse.query.model.Filters.Wild;
import fi.laji.datawarehouse.query.model.Partition;
import fi.laji.datawarehouse.query.model.PolygonIdSearch;
import fi.laji.datawarehouse.query.model.PolygonSearch;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class VerticaDAOImpleQueryFilters {

	public static final Criterion NOP_CRITERION = Restrictions.sqlRestriction(" 1 = 1 ");
	public static final Criterion NO_RESULTS_CRITERION = Restrictions.sqlRestriction(" 1 = 2 ");
	private static final int DATE_KEY_LENGTH = "yyyymmdd".length();

	private final VerticaDAOImpleSharedMappings mappings = new VerticaDAOImpleSharedMappings();
	private final VerticaDAOImpleDimensions dimensions;
	private final DAO dao;

	public VerticaDAOImpleQueryFilters(VerticaDAOImple verticaDAO) {
		this.dimensions = verticaDAO.dimensions;
		this.dao = verticaDAO.dao;
	}

	public Criterion getFilter(Filter filter) throws IllegalArgumentException {
		FilterConstructor filterConstuctor = FILTER_CONSTUCTORS.get(filter.getFieldName());
		if (filterConstuctor != null) {
			return filterConstuctor.createCriteria(filter);
		}
		throw new IllegalArgumentException(filter.getFieldName());
	}

	private interface FilterConstructor {
		Criterion createCriteria(Filter filter);
	}

	private class NOPFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			return NOP_CRITERION;
		}
	}

	private class EnumFilterConstructor implements FilterConstructor {
		private final String fieldName;

		public EnumFilterConstructor(String fieldName) {
			this.fieldName = fieldName;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> enumKeys = new ArrayList<>();
			for (Enum<?> e : filter.getEnums()) {
				Long enumKey = dimensions.getEnumKey(e);
				enumKeys.add(enumKey);
			}
			String reference = mappings.getEntityReference(filter.getBase(), fieldName);
			return Restrictions.in(reference, enumKeys);
		}
	}

	private class YKJGridFilter implements FilterConstructor {
		private final String fieldName;
		public YKJGridFilter(String fieldName) {
			this.fieldName = fieldName;
		}

		@Override
		public Criterion createCriteria(Filter filter) {
			List<Criterion> orFilters = new ArrayList<>();
			for (SingleCoordinates coordinates : filter.getSingleCoordinates()) {
				orFilters.add(
						Restrictions.and(
								Restrictions.eq(mappings.getEntityReference(filter.getBase(), fieldName+".lat"), coordinates.getLat()),
								Restrictions.eq(mappings.getEntityReference(filter.getBase(), fieldName+".lon"), coordinates.getLon())
								));

			}
			return orFilters(orFilters);
		}

	}
	private class UriIdentifierListFilterConstructor implements FilterConstructor {
		private final String fieldName;
		public UriIdentifierListFilterConstructor(String fieldName) {
			this.fieldName = fieldName;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<String> uris = new ArrayList<>();
			for (Qname identifier : filter.getQnames()) {
				uris.add(identifier.toURI());
			}
			return Restrictions.in(mappings.getEntityReference(filter.getBase(), fieldName), uris);
		}
	}

	private Criterion orFilters(List<Criterion> orFilters) {
		if (orFilters.size() == 1) {
			return orFilters.get(0);
		}
		return Restrictions.or(orFilters.toArray(new Criterion[orFilters.size()]));
	}

	private Criterion andFilters(List<Criterion> andFilters) {
		if (andFilters.size() == 1) {
			return andFilters.get(0);
		}
		return Restrictions.and(andFilters.toArray(new Criterion[andFilters.size()]));
	}

	private class AreaFilterConstructor implements FilterConstructor {
		private final Class<? extends GatheringArea> linkEntityClass;
		private final String field;
		private final Set<Qname> filterValues;
		public AreaFilterConstructor(String field, Class<? extends GatheringArea> linkEntityClass) {
			this(field, linkEntityClass, null);
		}
		public AreaFilterConstructor(String field, Class<? extends GatheringArea> linkEntityClass, Set<Qname> filterValues) {
			this.linkEntityClass = linkEntityClass;
			this.field = field;
			this.filterValues = filterValues;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			// Gathering can have 0-n areas of the same type. If there is only one area, it is stored to the gathering itself (but not to link table).
			// If there are >1 areas, they are stored to link table (and the main interpreted one to gathering).
			// We do not store only 1 link areas to the link table to reduce the size of the link table. (Most records only have one area of each type.)
			// Because of this, we need to do an OR search: the filtered area ids must be either in gathering itself or in the link table.
			Set<Qname> filterValues = this.filterValues == null ? filter.getQnames() : this.filterValues;
			List<Long> keys = getKeysOfQnames(filterValues, AreaEntity.class);
			if (keys.isEmpty()) return NO_RESULTS_CRITERION;
			DetachedCriteria subquery = DetachedCriteria.forClass(linkEntityClass)
					.add(Restrictions.in("areaKey", keys))
					.setProjection(Property.forName("gatheringKey"));
			return Restrictions.or(
					Restrictions.in(mappings.getEntityReference(filter.getBase(), field), keys),
					Subqueries.propertyIn(gatheringKey(filter.getBase()), subquery)
					);
		}
	}

	private class CollectionFilterConstructor implements FilterConstructor {
		private final String fieldName;
		private final boolean includeSubCollections;
		public CollectionFilterConstructor(String fieldName, boolean includeSubCollections) {
			this.fieldName = fieldName;
			this.includeSubCollections = includeSubCollections;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> keys = getCollectionKeys(filter, includeSubCollections);
			if (keys.isEmpty()) return NO_RESULTS_CRITERION;
			return Restrictions.in(mappings.getEntityReference(filter.getBase(), fieldName), keys);
		}
	}

	private class CollectionNotFilterConstructor implements FilterConstructor {
		private final String fieldName;
		private final boolean includeSubCollections;
		public CollectionNotFilterConstructor(String fieldName, boolean includeSubCollections) {
			this.fieldName = fieldName;
			this.includeSubCollections = includeSubCollections;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> keys = getCollectionKeys(filter, includeSubCollections);
			if (keys.isEmpty()) return NOP_CRITERION;
			return Restrictions.not(Restrictions.in(mappings.getEntityReference(filter.getBase(), fieldName), keys));
		}

	}

	private List<Long> getCollectionKeys(Filter filter, boolean includeSubCollections) {
		Collection<Qname> ids = null;
		if (includeSubCollections && includeSubCollections(filter)) {
			ids = getChildCollectionIds(filter);
		} else {
			ids = filter.getQnames();
		}
		if (ids.isEmpty()) return Collections.emptyList();
		return getKeysOfQnames(ids, CollectionEntity.class);
	}

	private Boolean includeSubCollections(Filter filter) {
		Boolean include = filter.getParent().getIncludeSubCollections();
		return include == null || Boolean.TRUE.equals(include);
	}

	private Collection<Qname> getChildCollectionIds(Filter filter) {
		Collection<Qname> ids;
		Map<String, CollectionMetadata> allCollections = dao.getCollections();
		ids = new HashSet<>();
		for (Qname collectionId : filter.getQnames()) {
			CollectionMetadata collection = allCollections.get(collectionId.toURI());
			if (collection != null) {
				ids.addAll(collection.getChildIdsIncludeSelf());
			}
		}
		return ids;
	}

	private class EntityQnamesListFilterConstructor<E extends BaseEntity> implements FilterConstructor {
		private final String fieldName;
		private final Class<E> entityClass;
		public EntityQnamesListFilterConstructor(String fieldName, Class<E> entityClass) {
			this.fieldName = fieldName;
			this.entityClass = entityClass;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> keys = getKeysOfQnames(filter, entityClass);
			if (keys.isEmpty()) return NO_RESULTS_CRITERION;
			return Restrictions.in(mappings.getEntityReference(filter.getBase(), fieldName), keys);
		}
	}

	private <E extends BaseEntity> List<Long> getKeysOfEnums(Filter filter, Class<E> entityClass) {
		Set<String> identifiers = new LinkedHashSet<>();
		for (Enum<?> enumeration : filter.getEnums()) {
			identifiers.add(enumeration.name());
		}
		return getKeysOfStrings(identifiers, entityClass);
	}

	private <E extends BaseEntity> List<Long> getKeysOfQnames(Collection<Qname> qnames, Class<E> entityClass) {
		Set<String> identifiers = new LinkedHashSet<>();
		for (Qname qname : qnames) {
			identifiers.add(qname.toURI());
		}
		return getKeysOfStrings(identifiers, entityClass);
	}

	private <E extends BaseEntity> List<Long> getKeysOfQnames(Filter filter, Class<E> entityClass) {
		return getKeysOfQnames(filter.getQnames(), entityClass);
	}

	private <E extends BaseEntity> List<Long> getKeysOfStrings(Set<String> strings, Class<E> entityClass) {
		try {
			List<Long> keys = new ArrayList<>();
			for (String id : strings) {
				if (id == null || id.isEmpty()) continue;
				E entity = entityClass.getConstructor().newInstance();
				entity.setId(id);
				Long entityKey = dimensions.getKeyDoNoCreate(entity);
				if (entityKey != null) {
					keys.add(entityKey);
				}
			}
			return keys;
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	private class TaxonPropertyFilterConstructor<E extends BaseEntity> implements FilterConstructor {
		private final Class<E> entityClass;
		private final Class<?> linkEntityClass;
		private final String linkFieldName;
		public TaxonPropertyFilterConstructor(Class<E> entityClass, Class<?> linkEntityClass, String linkFieldName) {
			this.entityClass = entityClass;
			this.linkEntityClass = linkEntityClass;
			this.linkFieldName = linkFieldName;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> keys = getKeysOfQnames(filter, entityClass);
			if (keys.isEmpty()) return NO_RESULTS_CRITERION;
			DetachedCriteria subquery = DetachedCriteria.forClass(linkEntityClass)
					.add(Restrictions.in(linkFieldName, keys))
					.setProjection(Property.forName("taxonKey"));
			String taxonTable = getTaxonTable(filter);
			return Subqueries.propertyIn(taxonTable + ".key", subquery);
		}
	}

	private class TaxonPropertyNotFilterConstructor<E extends BaseEntity> implements FilterConstructor {
		private final Class<E> entityClass;
		private final Class<?> linkEntityClass;
		private final String linkFieldName;
		public TaxonPropertyNotFilterConstructor(Class<E> entityClass, Class<?> linkEntityClass, String linkFieldName) {
			this.entityClass = entityClass;
			this.linkEntityClass = linkEntityClass;
			this.linkFieldName = linkFieldName;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> keys = getKeysOfQnames(filter, entityClass);
			if (keys.isEmpty()) return NO_RESULTS_CRITERION;
			DetachedCriteria subquery = DetachedCriteria.forClass(linkEntityClass)
					.add(Restrictions.in(linkFieldName, keys))
					.setProjection(Property.forName("taxonKey"));
			return Subqueries.propertyNotIn(getTaxonTable(filter)+".key", subquery);
		}
	}

	private class HasMediaFilterConstructor implements FilterConstructor {
		private final String fieldName;
		public HasMediaFilterConstructor(String fieldName) {
			this.fieldName = fieldName;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			String reference = mappings.getEntityReference(filter.getBase(), fieldName);
			if (filter.getBoolean()) {
				return Restrictions.ge(reference, 1);
			}
			return Restrictions.eq(reference, 0);
		}
	}

	private class ObserverIdFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> userIdKeys = getUserIdKeys(filter);
			if (userIdKeys.isEmpty()) return NO_RESULTS_CRITERION;

			DetachedCriteria subquery = DetachedCriteria.forClass(GatheringUserId.class)
					.add(Restrictions.in("userIdKey", userIdKeys))
					.setProjection(Property.forName("gatheringKey"));
			return Subqueries.propertyIn(gatheringKey(filter.getBase()), subquery);
		}

	}

	private class EditorIdFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Long> userIdKeys = getUserIdKeys(filter);
			if (userIdKeys.isEmpty()) return NO_RESULTS_CRITERION;

			DetachedCriteria subquery = DetachedCriteria.forClass(DocumentUserId.class)
					.add(Restrictions.in("userIdKey", userIdKeys))
					.setProjection(Property.forName("documentKey"));
			return Subqueries.propertyIn(documentKey(filter.getBase()), subquery);
		}
	}

	private List<Long> getUserIdKeys(Filter filter) {
		List<Long> personKeys = new ArrayList<>();
		for (Qname personId : filter.getQnames()) {
			Long personKey = PersonBaseEntity.parsePersonKey(personId);
			if (personKey != null) personKeys.add(personKey);
		}
		return dimensions.getUserIdKeys(personKeys);
	}

	private class TargetFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Criterion> orFilters = new ArrayList<>();
			for (String targetName : filter.getStrings()) {
				addTargetFilter(filter, orFilters, targetName);
			}
			return orFilters(orFilters);
		}

		private void addTargetFilter(Filter filter, List<Criterion> orFilters, String targetName) {
			boolean wildcardVerbatim = targetName.contains("*");
			Qname taxonId = wildcardVerbatim ? null : dao.getTaxonLinkingService().getTaxonId(targetName);
			if (taxonId == null) {
				addTargetFilter(filter, orFilters, wildcardVerbatim, targetName);
			} else {
				addTaxonFilter(orFilters, taxonId, filter);
			}
		}

		private void addTargetFilter(Filter filter, List<Criterion> orFilters, boolean wildcardVerbatim, String targetName) {
			// TODO Note that if unit has annotation identification, the target is actually taxon id and it will not match to targetName
			// Could change this to use unit.taxonVerbatim instead? Then, however, it would not match weirdly typed names that target cleanup has cleaned
			// -- that would be ok, since by definition, this is a verbatim search?
			String targetField = Boolean.FALSE.equals(filter.getParent().getUseIdentificationAnnotations()) ? "originalTarget.targetLowercase" : "target.targetLowercase";
			if (wildcardVerbatim) {
				targetName = TaxonLinkingService.cleanTargetNameAllowWildcard(targetName);
				orFilters.add(Restrictions.like(targetField, targetName.replace("*", "%")));
			} else {
				targetName = TaxonLinkingService.cleanTargetName(targetName);
				orFilters.add(Restrictions.eq(targetField, targetName));
			}
		}
	}

	private class TaxonFilterConsturctor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Criterion> orFilters = new ArrayList<>();
			for (Qname taxonId : filter.getQnames()) {
				addTaxonFilter(orFilters, taxonId, filter);
			}
			return orFilters(orFilters);
		}
	}

	private class HabitatFilterConsturctor implements FilterConstructor {
		private final boolean primary;
		public HabitatFilterConsturctor(boolean primary) {
			this.primary = primary;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			List<Criterion> criterion = new ArrayList<>();
			for (String param : filter.getStrings()) {
				criterion.add(createCriteria(filter, param));
			}
			return orFilters(criterion);
		}
		private Criterion createCriteria(Filter filter, String param) {
			HabitatObject habitatObject = HabitatObject.fromSearchString(param);
			DetachedCriteria subquery = createCriterion(habitatObject);
			return Subqueries.propertyIn(getTaxonTable(filter)+".key", subquery);
		}
		private DetachedCriteria createCriterion(HabitatObject habitatObject) {
			DetachedCriteria subquery = DetachedCriteria.forClass(TaxonToHabitat.class)
					.add(Restrictions.like("habitat", habitatObject.getHabitat().toString()+"%"))
					.setProjection(Property.forName("taxonKey"));

			for (Qname specificType : habitatObject.getHabitatSpecificTypes()) {
				subquery.add(Restrictions.like("habitat", "%:"+specificType.toString()+":%"));
			}
			if (primary) {
				subquery.add(Restrictions.eq("primary", true));
			}
			return subquery;
		}
	}

	private class InformalGroupFilterConstructor implements FilterConstructor {
		private final boolean includeReported;
		public InformalGroupFilterConstructor(boolean includeReported) {
			this.includeReported = includeReported;
		}
		@Override
		public Criterion createCriteria(Filter filter) {
			Criterion taxonPartOfGroup = NO_RESULTS_CRITERION;

			List<Long> taxonInformalGroupKeys = getKeysOfQnames(filter, InformalTaxonGroupEntity.class);
			if (!taxonInformalGroupKeys.isEmpty()) {
				DetachedCriteria subquery = DetachedCriteria.forClass(TaxonToInformalTaxonGroup.class)
						.add(Restrictions.in("groupKey", taxonInformalGroupKeys))
						.setProjection(Property.forName("taxonKey"));
				taxonPartOfGroup = Subqueries.propertyIn(getTaxonTable(filter)+".key", subquery);
			}

			if (!includeReported) return taxonPartOfGroup;

			Criterion reportedInformalGroup = NO_RESULTS_CRITERION;

			Set<Qname> reportedFilterQnames = new HashSet<>();
			for (Qname groupId : filter.getQnames()) {
				addGroupAndSubGroups(groupId, reportedFilterQnames);
			}
			List<Long> reportedInformalGroupKeys = getKeysOfQnames(reportedFilterQnames, InformalTaxonGroupEntity.class);
			if (!reportedInformalGroupKeys.isEmpty()) {
				reportedInformalGroup = Restrictions.in(mappings.getEntityReference(filter.getBase(), "unit.reportedInformalTaxonGroup"), reportedInformalGroupKeys);
			}

			if (taxonPartOfGroup == NO_RESULTS_CRITERION && reportedInformalGroup == NO_RESULTS_CRITERION) return NO_RESULTS_CRITERION;
			if (taxonPartOfGroup == NO_RESULTS_CRITERION) return reportedInformalGroup;
			if (reportedInformalGroup == NO_RESULTS_CRITERION) return taxonPartOfGroup;
			return Restrictions.or(taxonPartOfGroup, reportedInformalGroup);
		}

		private void addGroupAndSubGroups(Qname groupId, Set<Qname> reportedFilterQnames) {
			reportedFilterQnames.add(groupId);
			try {
				InformalTaxonGroup group = dao.getTaxonomyDAO().getInformalTaxonGroups().get(groupId.toString());
				if (group == null) return;
				for (Qname subGroupId : group.getSubGroups()) {
					addGroupAndSubGroups(subGroupId, reportedFilterQnames);
				}
			} catch (Exception e) {
				throw new ETLException(e);
			}
		}
	}

	private class InformalGroupNotFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			Criterion informalGroupFilter = new InformalGroupFilterConstructor(false).createCriteria(filter);
			return Restrictions.not(informalGroupFilter);
		}
	}

	private class FactFilterConstructor implements FilterConstructor {
		private final Class<? extends FactLink> factLinkClass;
		public FactFilterConstructor(Class<? extends FactLink> factLinkClass) {
			this.factLinkClass = factLinkClass;
		}

		@Override
		public Criterion createCriteria(Filter filter) {
			List<Criterion> keyIn = new ArrayList<>();
			for (List<Criterion> citerionOfSingleProperty : criterionByProperty(filter)) {
				keyIn.add(keyIn(subQuery(citerionOfSingleProperty), filter));
			}
			return andFilters(keyIn);
		}

		private DetachedCriteria subQuery(List<Criterion> citerionOfSingleProperty) {
			DetachedCriteria subquery = DetachedCriteria.forClass(factLinkClass)
					.setProjection(Property.forName("parentKey"));
			subquery.add(andFilters(citerionOfSingleProperty));
			return subquery;
		}

		private Criterion keyIn(DetachedCriteria subquery, Filter filter) {
			if (factLinkClass == UnitFact.class) {
				return Subqueries.propertyIn(unitKey(filter.getBase()), subquery);
			}
			if (factLinkClass == GatheringFact.class) {
				return Subqueries.propertyIn(gatheringKey(filter.getBase()), subquery);
			}
			if (factLinkClass == DocumentFact.class) {
				return Subqueries.propertyIn(documentKey(filter.getBase()), subquery);
			}
			if (factLinkClass == SampleFact.class) {
				return Subqueries.propertyIn("sample.key", subquery);
			}
			throw new UnsupportedOperationException("Unsupported fact type " + factLinkClass.getName());
		}

		private Collection<List<Criterion>> criterionByProperty(Filter filter) {
			Map<Set<Long>, List<Criterion>> criterionGroupedByPropertyKeys = new HashMap<>();
			for (Pair<Set<Long>, String> propertyKeysAndValue :  parsePropertyKeysAndValues(filter)) {
				Set<Long> propertyKeys = propertyKeysAndValue.getKey();
				String value = propertyKeysAndValue.getValue();
				List<Criterion> criterionOfProperty = getOrCreateCriterionOfProperties(criterionGroupedByPropertyKeys, propertyKeys);
				addValueCriterion(criterionOfProperty, value);

			}
			return criterionGroupedByPropertyKeys.values();
		}

		private List<Criterion> getOrCreateCriterionOfProperties(Map<Set<Long>, List<Criterion>> criterionByPropertyKeys, Set<Long> propertyKeys) {
			if (!criterionByPropertyKeys.containsKey(propertyKeys)) {
				List<Criterion> criterionOfProperty = Utils.list(Restrictions.in("propertyKey", propertyKeys));
				criterionByPropertyKeys.put(propertyKeys, criterionOfProperty);
				return criterionOfProperty;
			}
			return criterionByPropertyKeys.get(propertyKeys);
		}

		private void addValueCriterion(List<Criterion> criterionOfProperty, String value) {
			if (value == null) return;
			double[] numericRange = Util.parseNumericRange(value);
			if (numericRange != null) {
				criterionOfProperty.add(Restrictions.ge("decimalValue", numericRange[0]));
				criterionOfProperty.add(Restrictions.le("decimalValue", numericRange[1]));
			} else {
				List<String> values = new ArrayList<>();
				values.add(value);
				if (!value.startsWith("http")) {
					values.add(new Qname(value).toURI());
				} else {
					Qname qname = toQname(value);
					if (qname != null) values.add(qname.toString());
				}
				criterionOfProperty.add(Restrictions.in("value", values));
			}
		}

		private List<Pair<Set<Long>, String>> parsePropertyKeysAndValues(Filter filter) {
			List<Pair<Set<Long>, String>> properyKeysAndValues = new ArrayList<>();
			for (String factAndValue : filter.getStrings()) {
				Set<Long> keys = new HashSet<>();
				String[] parts = factAndValue.split(Pattern.quote("="));
				String fact = parts[0];
				String value = parts.length > 1 ? parts[1] : null;
				keys.add(getPropertyKey(fact));
				if (!fact.startsWith("http")) {
					String uri = new Qname(fact).toURI();
					keys.add(getPropertyKey(uri));
				} else {
					Qname qname = toQname(fact);
					if (qname != null) keys.add(getPropertyKey(qname.toString()));
				}
				properyKeysAndValues.add(new Pair<>(keys, value));
			}
			return properyKeysAndValues;
		}

		private Long getPropertyKey(String fact) {
			Long propertyKey = dimensions.getKeyDoNoCreate(new PropertyEntity().setId(fact));
			if (propertyKey == null) propertyKey = -1L;
			return propertyKey;
		}
	}

	private class CoordinateFilterConstructor implements FilterConstructor {

		@Override
		public Criterion createCriteria(Filter filter) {
			return createCriteria(filter, filter.getCoordinates());
		}

		public Criterion createCriteria(Filter filter, Set<CoordinatesWithOverlapRatio> values) {
			List<Criterion> orFilters = new ArrayList<>();
			for (CoordinatesWithOverlapRatio coordinates : values) {
				String coordinateFieldPrefix = getCoordinateFieldPrefix(coordinates.getType());
				double latMin = coordinates.getLatMin();
				double latMax = coordinates.getLatMax();
				double lonMin = coordinates.getLonMin();
				double lonMax = coordinates.getLonMax();
				if (coordinates.getType() == Type.YKJ) {
					latMin = trimToYkjLength(latMin);
					latMax = trimToYkjLength(latMax);
					lonMin = trimToYkjLength(lonMin);
					lonMax = trimToYkjLength(lonMax);
				}
				if (mustBeWithin(coordinates)) {
					coordinatesWithin(filter.getBase(), orFilters, coordinateFieldPrefix, latMin, latMax, lonMin, lonMax);
				} else if (shouldTouch(coordinates)) {
					coordinatesTouches(filter.getBase(), orFilters, coordinateFieldPrefix, latMin, latMax, lonMin, lonMax);
				} else {
					coordinateOverlapRatio(filter.getBase(), orFilters, coordinates.getType(), latMin, latMax, lonMin, lonMax, coordinates.getOverlapRatio());
				}
			}
			return orFilters(orFilters);
		}

		private boolean mustBeWithin(CoordinatesWithOverlapRatio coordinates) {
			return coordinates.getOverlapRatio() == null || coordinates.getOverlapRatio() == 1.0;
		}

		private boolean shouldTouch(CoordinatesWithOverlapRatio coordinates) {
			return coordinates.getOverlapRatio() != null && coordinates.getOverlapRatio() == 0.0;
		}

		private void coordinatesTouches(Base base, List<Criterion> orFilters, String coordinateFieldPrefix, double latMin, double latMax, double lonMin, double lonMax) {
			orFilters.add(
					Restrictions.and(
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"latMin"), latMax),
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"latMax"), latMin),
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMin"), lonMax),
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMax"), lonMin)
							));
		}

		private void coordinatesWithin(Base base, List<Criterion> orFilters, String coordinateFieldPrefix, double latMin, double latMax, double lonMin, double lonMax) {
			orFilters.add(
					Restrictions.and(
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"latMin"), latMin),
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"latMax"), latMax),
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMin"), lonMin),
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMax"), lonMax)
							));
		}

		private void coordinateOverlapRatio(Base base, List<Criterion> orFilters, Type type, double latMin, double latMax, double lonMin, double lonMax, Double overlapRatio) {
			if (overlapRatio == null) throw new IllegalArgumentException("Coordinate overlap ratio must be given");
			if (overlapRatio > 1.0 || overlapRatio < 0.0) throw new IllegalArgumentException("Coordinate overlap ratio must be between 0.0 and 1.0");
			String fLatMin = "{alias}.ykjnmin";
			String fLatMax = "{alias}.ykjnmax";
			String fLonMin = "{alias}.ykjemin";
			String fLonMax = "{alias}.ykjemax";
			if (type == Type.EUREF) {
				fLatMin = "{alias}.eurefnmin";
				fLatMax = "{alias}.eurefnmax";
				fLonMin = "{alias}.eurefemin";
				fLonMax = "{alias}.eurefemax";
			}
			if (type == Type.WGS84) {
				fLatMin = "{alias}.latitudemin";
				fLatMax = "{alias}.latitudemax";
				fLonMin = "{alias}.longitudemin";
				fLonMax = "{alias}.longitudemax";
			}
			String coordinateFieldPrefix = getCoordinateFieldPrefix(type);
			orFilters.add(
					Restrictions.and(
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"latMin"), latMax),
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"latMax"), latMin),
							Restrictions.le(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMin"), lonMax),
							Restrictions.ge(mappings.getEntityReference(base, coordinateFieldPrefix+"lonMax"), lonMin),
							Restrictions.sqlRestriction("" +
									"( " +
									" GREATEST(((LEAST("+fLatMax+",?) - GREATEST("+fLatMin+",?)) * (LEAST("+fLonMax+",?) - GREATEST("+fLonMin+",?))), 0.0000001) / " +
									" GREATEST((("+fLatMax+" - "+fLatMin+") * ("+fLonMax+" - "+fLonMin+")), 0.0000001) " +
									") " +
									" >= ? ",
									new Object[] {latMax, latMin, lonMax, lonMin, overlapRatio},
									new org.hibernate.type.Type[] {DoubleType.INSTANCE, DoubleType.INSTANCE, DoubleType.INSTANCE, DoubleType.INSTANCE, DoubleType.INSTANCE}
									)
							));
		}
	}

	private class PolygonFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			PolygonSearch polygon = filter.getPolygon();
			return polygonFilter(filter, polygon);
		}

		public Criterion createCriteria(Filter filter, PolygonSearch polygon) {
			return polygonFilter(filter, polygon);
		}

		private Criterion polygonFilter(Filter filter, PolygonSearch polygon) {
			DetachedCriteria subquery = DetachedCriteria.forClass(GatheringGeometry.class)
					.setProjection(Property.forName("gatheringKey"))
					.add(Restrictions.sqlRestriction("public.STV_DWithin({alias}.euref_geo, public.ST_GeomFromText(?), 10)", polygon.getWKT(), StringType.INSTANCE))
					.add(Restrictions.in("grid", grids(polygon)));
			return Subqueries.propertyIn(gatheringKey(filter.getBase()), subquery);
		}

		private Collection<String> grids(PolygonSearch polygon) {
			Set<String> grids = new HashSet<>();
			try {
				for (Feature f : Geo.fromWKT(polygon.getWKT(), Type.EUREF)) {
					if (!(f instanceof Polygon)) throw new IllegalStateException("Non-polygon polygon search");
					Polygon poly = (Polygon) f;
					for (Point point : poly) {
						grids.add(grid(point));
					}
				}
			} catch (DataValidationException e) {
				throw new ETLException(e);
			}
			return grids;
		}

		private String grid(Point point) {
			int lat = (int) Math.floor(point.getLat() / 100000);
			int lon = (int) Math.floor(point.getLon() / 100000);
			return lat + ":" + lon;
		}
	}

	private class PolygonIdFilterConstructor implements FilterConstructor {
		@Override
		public Criterion createCriteria(Filter filter) {
			PolygonIdSearch polygonId = filter.getPolygonId();
			String wkt = dao.getPolygonSearch(polygonId.getId());
			if (wkt == null) {
				throw new IllegalArgumentException("Nothing found with polygonId " + polygonId.getId());
			}
			PolygonSearch searchParam = new PolygonSearch(wkt);
			return new PolygonFilterConstructor().createCriteria(filter, searchParam);
		}
	}

	private final Map<String, FilterConstructor> FILTER_CONSTUCTORS;
	{
		FILTER_CONSTUCTORS = new HashMap<>();

		FILTER_CONSTUCTORS.put("includeSubCollections", new NOPFilterConstructor());
		FILTER_CONSTUCTORS.put("useIdentificationAnnotations", new NOPFilterConstructor());
		FILTER_CONSTUCTORS.put("includeSubTaxa", new NOPFilterConstructor());
		FILTER_CONSTUCTORS.put("taxonAdminFiltersOperator", new NOPFilterConstructor());

		FILTER_CONSTUCTORS.put("unitFact", new FactFilterConstructor(UnitFact.class));
		FILTER_CONSTUCTORS.put("gatheringFact", new FactFilterConstructor(GatheringFact.class));
		FILTER_CONSTUCTORS.put("documentFact", new FactFilterConstructor(DocumentFact.class));
		FILTER_CONSTUCTORS.put("sampleFact", new FactFilterConstructor(SampleFact.class));

		FILTER_CONSTUCTORS.put("invasiveControl", new EnumFilterConstructor("unit.interpretations.invasiveControlEffectiveness"));
		FILTER_CONSTUCTORS.put("recordBasis", new EnumFilterConstructor("unit.recordBasis"));
		FILTER_CONSTUCTORS.put("superRecordBasis", new EnumFilterConstructor("unit.superRecordBasis"));
		FILTER_CONSTUCTORS.put("lifeStage", new EnumFilterConstructor("unit.lifeStage"));
		FILTER_CONSTUCTORS.put("secureLevel", new EnumFilterConstructor("document.secureLevel"));
		FILTER_CONSTUCTORS.put("collectionQuality", new EnumFilterConstructor("document.linkings.collectionQuality"));
		FILTER_CONSTUCTORS.put("recordQuality", new EnumFilterConstructor("unit.interpretations.recordQuality"));
		FILTER_CONSTUCTORS.put("reliability", new EnumFilterConstructor("unit.interpretations.reliability"));
		FILTER_CONSTUCTORS.put("sourceOfCoordinates", new EnumFilterConstructor("gathering.interpretations.sourceOfCoordinates"));

		FILTER_CONSTUCTORS.put("documentId", new UriIdentifierListFilterConstructor("document.documentId"));
		FILTER_CONSTUCTORS.put("gatheringId", new UriIdentifierListFilterConstructor("gathering.gatheringId"));
		FILTER_CONSTUCTORS.put("unitId", new UriIdentifierListFilterConstructor("unit.unitId"));
		FILTER_CONSTUCTORS.put("sampleId", new UriIdentifierListFilterConstructor("unit.samples.sampleId"));
		FILTER_CONSTUCTORS.put("individualId", new UriIdentifierListFilterConstructor("unit.individualId"));
		FILTER_CONSTUCTORS.put("namedPlaceId", new UriIdentifierListFilterConstructor("document.namedPlaceId"));
		FILTER_CONSTUCTORS.put("formId", new UriIdentifierListFilterConstructor("document.formId"));
		FILTER_CONSTUCTORS.put("sampleType", new UriIdentifierListFilterConstructor("unit.samples.type"));
		FILTER_CONSTUCTORS.put("sampleQuality", new UriIdentifierListFilterConstructor("unit.samples.quality"));
		FILTER_CONSTUCTORS.put("sampleStatus", new UriIdentifierListFilterConstructor("unit.samples.status"));
		FILTER_CONSTUCTORS.put("sampleMaterial", new UriIdentifierListFilterConstructor("unit.samples.material"));
		FILTER_CONSTUCTORS.put("plantStatusCode", new UriIdentifierListFilterConstructor("unit.plantStatusCode"));
		FILTER_CONSTUCTORS.put("atlasCode", new UriIdentifierListFilterConstructor("unit.atlasCode"));
		FILTER_CONSTUCTORS.put("atlasClass", new UriIdentifierListFilterConstructor("unit.atlasClass"));
		FILTER_CONSTUCTORS.put("samplingMethod", new UriIdentifierListFilterConstructor("unit.samplingMethod"));

		FILTER_CONSTUCTORS.put(Const.COLLECTION_ID, new CollectionFilterConstructor("document.collectionId", true));
		FILTER_CONSTUCTORS.put("collectionIdExplicit", new CollectionFilterConstructor("document.collectionId", false));
		FILTER_CONSTUCTORS.put("sampleCollectionId", new CollectionFilterConstructor("unit.samples.collectionId", true));
		FILTER_CONSTUCTORS.put("collectionIdNot", new CollectionNotFilterConstructor("document.collectionId", true));
		FILTER_CONSTUCTORS.put("collectionIdExplicitNot", new CollectionNotFilterConstructor("document.collectionId", false));
		FILTER_CONSTUCTORS.put("sourceId", new EntityQnamesListFilterConstructor<>("document.sourceId", SourceEntity.class));

		FILTER_CONSTUCTORS.put("namedPlaceTag", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> filters = new ArrayList<>();
				for (Qname tag : filter.getQnames()) {
					filters.add(Restrictions.like("namedPlace.tagsString", "%|" + tag + "|%"));
				}
				return orFilters(filters);
			}
		});

		FILTER_CONSTUCTORS.put("identificationBasis", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> filters = new ArrayList<>();
				String ref = filter.getBase() == Base.UNIT_MEDIA ? "parentUnit.identificationBasisString" : "unit.identificationBasisString";
				for (Qname value : filter.getQnames()) {
					filters.add(Restrictions.like(ref, "%|" + value + "|%"));
				}
				return orFilters(filters);
			}
		});

		FILTER_CONSTUCTORS.put("taxonRankId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return new UriIdentifierListFilterConstructor("unit.linkings."+getTaxonTable(filter)+".taxonRank").createCriteria(filter);
			}
		});

		FILTER_CONSTUCTORS.put("higherTaxon", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(getTaxonTable(filter)+".species", !filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("finnishMunicipalityId", new AreaFilterConstructor("gathering.interpretations.finnishMunicipality", GatheringMunicipality.class));
		FILTER_CONSTUCTORS.put("biogeographicalProvinceId", new AreaFilterConstructor("gathering.interpretations.biogeographicalProvince", GatheringBioprovince.class));
		FILTER_CONSTUCTORS.put("countryId", new EntityQnamesListFilterConstructor<>("gathering.interpretations.country", AreaEntity.class));
		FILTER_CONSTUCTORS.put("birdAssociationAreaId", new EntityQnamesListFilterConstructor<>("gathering.conversions.birdAssociationArea", AreaEntity.class));

		FILTER_CONSTUCTORS.put("elyCentreId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<Qname> municipalityIds = new HashSet<>();
				for (Qname elyId : filter.getQnames()) {
					municipalityIds.addAll(dao.getMunicipalitiesPartOfELY(elyId));
				}
				return new AreaFilterConstructor("gathering.interpretations.finnishMunicipality", GatheringMunicipality.class, municipalityIds).createCriteria(filter);
			}
		});

		FILTER_CONSTUCTORS.put("provinceId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<Qname> municipalityIds = new HashSet<>();
				for (Qname provinceId : filter.getQnames()) {
					municipalityIds.addAll(dao.getMunicipalitiesPartOfProvince(provinceId));
				}
				return new AreaFilterConstructor("gathering.interpretations.finnishMunicipality", GatheringMunicipality.class, municipalityIds).createCriteria(filter);
			}
		});

		FILTER_CONSTUCTORS.put("informalTaxonGroupId", new InformalGroupFilterConstructor(false));
		FILTER_CONSTUCTORS.put("informalTaxonGroupIdIncludingReported", new InformalGroupFilterConstructor(true));
		FILTER_CONSTUCTORS.put("informalTaxonGroupIdNot", new InformalGroupNotFilterConstructor());
		FILTER_CONSTUCTORS.put("primaryHabitat", new HabitatFilterConsturctor(true));
		FILTER_CONSTUCTORS.put("anyHabitat", new HabitatFilterConsturctor(false));
		FILTER_CONSTUCTORS.put("typeOfOccurrenceId", new TaxonPropertyFilterConstructor<>(OccurrenceTypeEntity.class, TaxonToTypeOfOccurrence.class, "typeKey"));
		FILTER_CONSTUCTORS.put("typeOfOccurrenceIdNot", new TaxonPropertyNotFilterConstructor<>(OccurrenceTypeEntity.class, TaxonToTypeOfOccurrence.class, "typeKey"));

		FILTER_CONSTUCTORS.put("hasUnitMedia", new HasMediaFilterConstructor("unit.mediaCount"));
		FILTER_CONSTUCTORS.put("hasUnitImages", new HasMediaFilterConstructor("unit.imageCount"));
		FILTER_CONSTUCTORS.put("hasUnitAudio", new HasMediaFilterConstructor("unit.audioCount"));
		FILTER_CONSTUCTORS.put("hasUnitVideo", new HasMediaFilterConstructor("unit.videoCount"));
		FILTER_CONSTUCTORS.put("hasUnitModel", new HasMediaFilterConstructor("unit.modelCount"));
		FILTER_CONSTUCTORS.put("hasGatheringMedia", new HasMediaFilterConstructor("gathering.mediaCount"));
		FILTER_CONSTUCTORS.put("hasDocumentMedia", new HasMediaFilterConstructor("document.mediaCount"));

		FILTER_CONSTUCTORS.put("administrativeStatusId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (filter.getParent().hasVirvaTaxonFilters()) {
					return Restrictions.eq(getTaxonTable(filter)+".virva", true);
				}
				return new TaxonPropertyFilterConstructor<>(AdministrativeStatusEntity.class, TaxonToAdministrativeStatus.class, "statusKey").createCriteria(filter);
			}
		});

		FILTER_CONSTUCTORS.put("taxonSetId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return new TaxonPropertyFilterConstructor<>(TaxonSetEntity.class, TaxonToTaxonSet.class, "setKey").createCriteria(filter);
			}
		});

		FILTER_CONSTUCTORS.put("sex", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> criterion = new ArrayList<>();
				criterion.add(new EnumFilterConstructor("unit.sex").createCriteria(filter));
				for (Enum<?> e : filter.getEnums()) {
					Unit.Sex sex = (Unit.Sex) e;
					if (sex == Unit.Sex.FEMALE) {
						criterion.add(Restrictions.ge(mappings.getEntityReference(filter.getBase(), "unit.individualCountFemale"), 1));
					}
					if (sex == Unit.Sex.MALE) {
						criterion.add(Restrictions.ge(mappings.getEntityReference(filter.getBase(), "unit.individualCountMale"), 1));
					}
				}
				return orFilters(criterion);
			}
		});

		FILTER_CONSTUCTORS.put("includeNonValidTaxa", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (Boolean.FALSE.equals(filter.getBoolean())) {
					return Restrictions.isNotNull(getTaxonTable(filter)+".id");
				}
				return NOP_CRITERION;
			}
		});

		FILTER_CONSTUCTORS.put("onlyNonValidTaxa", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (Boolean.TRUE.equals(filter.getBoolean())) {
					String targetTable = getTargetTable(filter);
					return Restrictions.and(
							Restrictions.isNull(targetTable+".taxonKey"),
							Restrictions.isNull(targetTable+".notExactTaxonMatch"));
				}
				return NOP_CRITERION;
			}
		});

		FILTER_CONSTUCTORS.put("hasMedia", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (filter.getBase() == Base.DOCUMENT) {
					return new HasMediaFilterConstructor("document.mediaCount").createCriteria(filter);
				}
				if (filter.getBase() == Base.GATHERING) {
					if (filter.getBoolean()) {
						// Some have media
						return Restrictions.or(
								new HasMediaFilterConstructor("gathering.mediaCount").createCriteria(filter),
								new HasMediaFilterConstructor("document.mediaCount").createCriteria(filter));
					}
					// None have media
					return Restrictions.and(
							new HasMediaFilterConstructor("gathering.mediaCount").createCriteria(filter),
							new HasMediaFilterConstructor("document.mediaCount").createCriteria(filter));
				}
				if (filter.getBoolean()) {
					// Some have media
					return Restrictions.or(
							new HasMediaFilterConstructor("unit.mediaCount").createCriteria(filter),
							new HasMediaFilterConstructor("gathering.mediaCount").createCriteria(filter),
							new HasMediaFilterConstructor("document.mediaCount").createCriteria(filter));
				}
				// None have media
				return Restrictions.and(
						new HasMediaFilterConstructor("unit.mediaCount").createCriteria(filter),
						new HasMediaFilterConstructor("gathering.mediaCount").createCriteria(filter),
						new HasMediaFilterConstructor("document.mediaCount").createCriteria(filter));
			}
		});

		FILTER_CONSTUCTORS.put("keyword", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<String> keywords = withAndWithoutWhitespace(filter.getStrings());
				if (keywords.isEmpty()) return NO_RESULTS_CRITERION;
				if (filter.getBase().includes(Base.SAMPLE)) {
					DetachedCriteria subquery = DetachedCriteria.forClass(SampleKeyword.class)
							.add(Restrictions.in("keyword", keywords))
							.setProjection(Property.forName("sampleKey"));
					return Subqueries.propertyIn("sample.key", subquery);
				}

				if (filter.getBase().includes(Base.UNIT)) {
					DetachedCriteria subquery = DetachedCriteria.forClass(UnitKeyword.class)
							.add(Restrictions.in("keyword", keywords))
							.setProjection(Property.forName("unitKey"));
					return Subqueries.propertyIn(unitKey(filter.getBase()), subquery);
				}

				DetachedCriteria subquery = DetachedCriteria.forClass(DocumentKeyword.class)
						.add(Restrictions.in("keyword", keywords))
						.setProjection(Property.forName("documentKey"));
				return Subqueries.propertyIn(documentKey(filter.getBase()), subquery);
			}

			private Set<String> withAndWithoutWhitespace(Set<String> strings) {
				Set<String> all = new HashSet<>(strings);
				for (String s : strings) {
					all.add(Utils.removeWhitespace(s));
					if (!s.startsWith("http")) {
						all.add(new Qname(s).toURI());
					} else {
						Qname qname = toQname(s);
						if (qname != null) all.add(qname.toString());
					}
				}
				return all;
			}
		});

		FILTER_CONSTUCTORS.put("effectiveTag", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Long> keys = getKeysOfEnums(filter, TagEntity.class);
				if (keys.isEmpty()) return NO_RESULTS_CRITERION;
				DetachedCriteria subquery = DetachedCriteria.forClass(UnitEffectiveTag.class)
						.add(Restrictions.in("tagKey", keys))
						.setProjection(Property.forName("unitKey"));
				return Subqueries.propertyIn(unitKey(filter.getBase()), subquery);
			}
		});

		FILTER_CONSTUCTORS.put("secureReason", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Long> keys = getKeysOfEnums(filter, SecureReasonEntity.class);
				if (keys.isEmpty()) return NO_RESULTS_CRITERION;
				DetachedCriteria subquery = DetachedCriteria.forClass(DocumentSecureReason.class)
						.add(Restrictions.in("secureReasonKey", keys))
						.setProjection(Property.forName("documentKey"));
				return Subqueries.propertyIn(documentKey(filter.getBase()), subquery);
			}
		});

		FILTER_CONSTUCTORS.put(Const.EDITOR_ID_FILTER, new EditorIdFilterConstructor());

		FILTER_CONSTUCTORS.put(Const.OBSERVER_ID_FILTER, new ObserverIdFilterConstructor());

		FILTER_CONSTUCTORS.put(Const.EDITOR_OR_OBSERVER_ID_FILTER, new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.or(
						new EditorIdFilterConstructor().createCriteria(filter),
						new ObserverIdFilterConstructor().createCriteria(filter));
			}
		});

		FILTER_CONSTUCTORS.put(Const.EDITOR_OR_OBSERVER_ID_IS_NOT_FILTER, new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Criterion isEditor = new EditorIdFilterConstructor().createCriteria(filter);
				Criterion isObserver = new ObserverIdFilterConstructor().createCriteria(filter);
				return Restrictions.and(Restrictions.not(isEditor), Restrictions.not(isObserver));
			}
		});

		FILTER_CONSTUCTORS.put("invasiveControlled", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.interpretations.invasiveControlled"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("typeSpecimen", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.typeSpecimen"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("documentIdPrefix", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.in(mappings.getEntityReference(filter.getBase(), "document.prefix"), filter.getStrings());
			}
		});

		FILTER_CONSTUCTORS.put("wild", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (filter.getEnums().size() == 3) return NOP_CRITERION;
				String fieldname = mappings.getEntityReference(filter.getBase(), "unit.wild");
				List<Criterion> orFilters = new ArrayList<>();
				for (Enum<?> e : filter.getEnums()) {
					Wild wild = (Wild) e;
					if (wild == Wild.WILD_UNKNOWN) {
						orFilters.add(Restrictions.isNull(fieldname));
						continue;
					}
					if (wild == Wild.WILD) {
						orFilters.add(Restrictions.eq(fieldname, true));
						continue;
					}
					if (wild == Wild.NON_WILD) {
						orFilters.add(Restrictions.eq(fieldname, false));
						continue;
					}
				}
				return orFilters(orFilters);
			}
		});

		FILTER_CONSTUCTORS.put("breedingSite", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.breedingSite"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("local", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.local"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("alive", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String reference = mappings.getEntityReference(filter.getBase(), "unit.alive");
				if (Boolean.FALSE.equals(filter.getBoolean())) {
					return Restrictions.eq(reference, false);
				}
				return Restrictions.or(
						Restrictions.isNull(reference),
						Restrictions.eq(reference, true));
			}
		});

		FILTER_CONSTUCTORS.put("finnish", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(getTaxonTable(filter)+".finnish", filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("invasive", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(getTaxonTable(filter)+".invasive", filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("sensitive", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(getTaxonTable(filter)+".sensitive", filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("qualityIssues", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				QualityIssues qualityIssues = (QualityIssues) filter.getEnum();

				if (qualityIssues == QualityIssues.BOTH) return NOP_CRITERION;

				if (filter.getBase() == Base.DOCUMENT) {
					if (qualityIssues == QualityIssues.NO_ISSUES) {
						return Restrictions.isNull("foreignKeys.documentIssueKey");
					}
					if (qualityIssues == QualityIssues.ONLY_ISSUES) {
						return Restrictions.isNotNull("foreignKeys.documentIssueKey");
					}
				}

				if (filter.getBase() == Base.GATHERING) {
					if (qualityIssues == QualityIssues.NO_ISSUES) {
						return Restrictions.and(
								Restrictions.isNull("documentForeignKeys.documentIssueKey"),
								Restrictions.isNull("foreignKeys.issueKey"),
								Restrictions.isNull("foreignKeys.timeIssueKey"),
								Restrictions.isNull("foreignKeys.locationIssueKey"));
					}
					if (qualityIssues == QualityIssues.ONLY_ISSUES) {
						return Restrictions.or(
								Restrictions.isNotNull("documentForeignKeys.documentIssueKey"),
								Restrictions.isNotNull("foreignKeys.issueKey"),
								Restrictions.isNotNull("foreignKeys.timeIssueKey"),
								Restrictions.isNotNull("foreignKeys.locationIssueKey"));
					}
				}

				if (qualityIssues == QualityIssues.NO_ISSUES) {
					return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.quality.documentGatheringUnitQualityIssues"), false);
				}
				if (qualityIssues == QualityIssues.ONLY_ISSUES) {
					return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.quality.documentGatheringUnitQualityIssues"), true);
				}

				throw new UnsupportedOperationException("Invalid quality issue filter value " + qualityIssues);
			}
		});

		FILTER_CONSTUCTORS.put("unidentified", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String reference = mappings.getEntityReference(filter.getBase(), "unit.interpretations.needsIdentification");
				return Restrictions.eq(reference, filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("needsCheck", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String reference = mappings.getEntityReference(filter.getBase(), "unit.interpretations.needsCheck");
				return Restrictions.eq(reference, filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("redListStatusId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<String> statusURIs = new ArrayList<>();
				for (Qname status : filter.getQnames()) {
					statusURIs.add(status.toURI());
				}
				return Restrictions.in(getTaxonTable(filter)+".redListStatus", statusURIs);
			}
		});

		FILTER_CONSTUCTORS.put("target", new TargetFilterConstructor());
		FILTER_CONSTUCTORS.put("taxonId", new TaxonFilterConsturctor());

		FILTER_CONSTUCTORS.put("dayOfYear", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> orFilters = new ArrayList<>();
				for (String days : filter.getStrings()) {
					String beginReference =  mappings.getEntityReference(filter.getBase(), "gathering.conversions.dayOfYearBegin");
					String endReference =  mappings.getEntityReference(filter.getBase(), "gathering.conversions.dayOfYearEnd");
					orFilters.add(createTimeSpanFilter(days, beginReference, endReference, 1, 366, filter));
				}
				return orFilters(orFilters);
			}
		});

		FILTER_CONSTUCTORS.put("season", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> orFilters = new ArrayList<>();
				for (String season : filter.getStrings()) {
					String beginReference =  mappings.getEntityReference(filter.getBase(), "gathering.conversions.seasonBegin");
					String endReference =  mappings.getEntityReference(filter.getBase(), "gathering.conversions.seasonEnd");
					orFilters.add(createTimeSpanFilter(season, beginReference, endReference, 101, 1231, filter));
				}
				return orFilters(orFilters);
			}

		});

		FILTER_CONSTUCTORS.put("time", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<String> values = filter.getStrings();
				return createTimeFilter(filter, values);
			}
		});

		FILTER_CONSTUCTORS.put("timeAccuracy", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.sqlRestriction("({alias}.dateend_key - {alias}.datebegin_key + 1) <= " + filter.getInt());
			}
		});

		FILTER_CONSTUCTORS.put("yearMonth", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<String> values = filter.getStrings();
				Set<String> securedValues = new LinkedHashSet<>(values.size());
				for (String value : values) {
					securedValues.add(secure(value));
				}
				return createTimeFilter(filter, securedValues);
			}

			private String secure(String value) {
				if (value.length() < 4) throw invalidFormat();
				value = value.replace("-", "");

				String begin = value;
				String end = value;
				if (value.contains("/")) {
					String[] parts = value.split(Pattern.quote("/"));
					begin = parts[0];
					if (parts.length == 2) {
						end = parts[1];
					} else {
						end = null;
					}
				}
				if (end != null) {
					return securePart(begin) + "/" + securePart(end);
				}
				return securePart(begin);
			}

			private IllegalArgumentException invalidFormat() {
				return new IllegalArgumentException("Invalid value. Valid formats are yyyy, yyyy-mm, yyyy/yyyy, yyyy-mm/yyyy-mm, yyyy/yyyy-mm and yyyy-mm/yyyy.");
			}

			private String securePart(String value) {
				if (value.length() > 6) {
					throw invalidFormat();
				}
				return value;
			}
		});

		FILTER_CONSTUCTORS.put("completeListTaxonId", new UriIdentifierListFilterConstructor("document.completeListTaxonId"));
		FILTER_CONSTUCTORS.put("completeListType", new UriIdentifierListFilterConstructor("document.completeListType"));

		FILTER_CONSTUCTORS.put("taxonCensus", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Set<Long> taxonKeys = new HashSet<>();
				for (Qname taxonId : filter.getQnames()) {
					if (dao.hasTaxon(taxonId)) {
						Taxon taxon = dao.getTaxon(taxonId);
						taxonKeys.add(TaxonBaseEntity.parseTaxonKey(taxonId));
						while (taxon.hasParent()) {
							taxon = taxon.getParent();
							taxonKeys.add(TaxonBaseEntity.parseTaxonKey(taxon.getQname()));
						}
					} else {
						taxonKeys.add(-1L);
					}
				}
				if (taxonKeys.isEmpty()) return NO_RESULTS_CRITERION;
				DetachedCriteria subquery = DetachedCriteria.forClass(TaxonCensusEntity.class)
						.add(Restrictions.in("taxonKey", taxonKeys))
						.setProjection(Property.forName("gatheringKey"));
				return Subqueries.propertyIn(gatheringKey(filter.getBase()), subquery);
			}
		});

		FILTER_CONSTUCTORS.put("coordinateAccuracyMax", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String propertyName = mappings.getEntityReference(filter.getBase(), "gathering.interpretations.coordinateAccuracy");
				int accuracyMax = filter.getInt();
				return Restrictions.le(propertyName, accuracyMax);
			}
		});

		FILTER_CONSTUCTORS.put("occurrenceCountMax", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.lt(getTaxonTable(filter)+".occurrenceCount", filter.getInt());
			}
		});

		FILTER_CONSTUCTORS.put("occurrenceCountFinlandMax", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.lt(getTaxonTable(filter)+".occurrenceCountFinland", filter.getInt());
			}
		});

		FILTER_CONSTUCTORS.put("polygon", new PolygonFilterConstructor());
		FILTER_CONSTUCTORS.put("polygonId", new PolygonIdFilterConstructor());

		FILTER_CONSTUCTORS.put("coordinates", new CoordinateFilterConstructor());

		FILTER_CONSTUCTORS.put("wgs84CenterPoint", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> orFilters = new ArrayList<>();
				for (CoordinatesWithOverlapRatio coordinates : filter.getCoordinates()) {
					if (coordinates.getType() != Type.WGS84) throw new UnsupportedOperationException("Not yet implemented for " + coordinates.getType());
					double latMin = coordinates.getLatMin();
					double latMax = coordinates.getLatMax();
					double lonMin = coordinates.getLonMin();
					double lonMax = coordinates.getLonMax();
					orFilters.add(
							Restrictions.and(
									Restrictions.between(mappings.getEntityReference(filter.getBase(), "gathering.conversions.wgs84CenterPoint.lat"), latMin, latMax),
									Restrictions.between(mappings.getEntityReference(filter.getBase(), "gathering.conversions.wgs84CenterPoint.lon"), lonMin, lonMax)));
				}
				return orFilters(orFilters);
			}
		});

		FILTER_CONSTUCTORS.put("ykj1km", new YKJGridFilter("gathering.conversions.ykj1km"));
		FILTER_CONSTUCTORS.put("ykj10km", new YKJGridFilter("gathering.conversions.ykj10km"));
		FILTER_CONSTUCTORS.put("ykj50km", new YKJGridFilter("gathering.conversions.ykj50km"));
		FILTER_CONSTUCTORS.put("ykj100km", new YKJGridFilter("gathering.conversions.ykj100km"));
		FILTER_CONSTUCTORS.put("ykj1kmCenter", new YKJGridFilter("gathering.conversions.ykj1kmCenter"));
		FILTER_CONSTUCTORS.put("ykj10kmCenter", new YKJGridFilter("gathering.conversions.ykj10kmCenter"));
		FILTER_CONSTUCTORS.put("ykj50kmCenter", new YKJGridFilter("gathering.conversions.ykj50kmCenter"));
		FILTER_CONSTUCTORS.put("ykj100kmCenter", new YKJGridFilter("gathering.conversions.ykj100kmCenter"));

		FILTER_CONSTUCTORS.put("area", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> orFilters = new ArrayList<>();
				for (String area : filter.getStrings()) {
					Area country = single(dao.resolveCountiesByName(area));
					if (country != null) {
						orFilters.add(FILTER_CONSTUCTORS.get("countryId").createCriteria(new Filter(null, Utils.set(country.getQname()), filter.getParent())));
						continue;
					}
					Area municipality = single(dao.resolveMunicipalitiesByName(area));
					if (municipality != null) {
						orFilters.add(FILTER_CONSTUCTORS.get("finnishMunicipalityId").createCriteria(new Filter(null, Utils.set(municipality.getQname()), filter.getParent())));
						continue;
					}
					Area bioprovince = single(dao.resolveBiogeographicalProvincesByName(area));
					if (bioprovince != null) {
						orFilters.add(FILTER_CONSTUCTORS.get("biogeographicalProvinceId").createCriteria(new Filter(null, Utils.set(bioprovince.getQname()), filter.getParent())));
						continue;
					}
					//area = "%"+area+"%";
					if (area.contains("*")) {
						area = area.replace("*", "%");
						orFilters.add(Restrictions.ilike(mappings.getEntityReference(filter.getBase(), "gathering.locality"), area));
						orFilters.add(Restrictions.ilike(mappings.getEntityReference(filter.getBase(), "gathering.municipality"), area));
					} else {
						orFilters.add(Restrictions.eq(mappings.getEntityReference(filter.getBase(), "gathering.locality"), area).ignoreCase());
						orFilters.add(Restrictions.eq(mappings.getEntityReference(filter.getBase(), "gathering.municipality"), area).ignoreCase());
					}
				}
				return orFilters(orFilters);
			}

			private Area single(List<Area> resolveByName) {
				if (resolveByName.size() == 1) return resolveByName.get(0);
				return null;
			}
		});

		FILTER_CONSTUCTORS.put("loadedSameOrAfter", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.ge(mappings.getEntityReference(filter.getBase(), "document.loadDate"), filter.getDate());
			}
		});

		FILTER_CONSTUCTORS.put("loadedSameOrBefore", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.le(mappings.getEntityReference(filter.getBase(), "document.loadDate"), endOfDay(filter.getDate()));
			}

		});

		FILTER_CONSTUCTORS.put("firstLoadedSameOrAfter", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.ge(mappings.getEntityReference(filter.getBase(), "document.firstLoadDate"), filter.getDate());
			}
		});

		FILTER_CONSTUCTORS.put("firstLoadedSameOrBefore", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.le(mappings.getEntityReference(filter.getBase(), "document.firstLoadDate"), endOfDay(filter.getDate()));
			}
		});

		FILTER_CONSTUCTORS.put("actualLoadBefore", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.sqlRestriction("{alias}.actual_load_date < ?", filter.getDate(), DateType.INSTANCE);
			}
		});

		FILTER_CONSTUCTORS.put("createdDateYear", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.sqlRestriction("extract(year from {alias}.created_date) = " + filter.getInt());
			}
		});

		FILTER_CONSTUCTORS.put("individualCountMin", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String propertyName = mappings.getEntityReference(filter.getBase(), "unit.interpretations.individualCount");
				int individualCountMin = filter.getInt();
				return Restrictions.ge(propertyName, individualCountMin);
			}
		});

		FILTER_CONSTUCTORS.put("individualCountMax", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String propertyName = mappings.getEntityReference(filter.getBase(), "unit.interpretations.individualCount");
				int individualCountMax = filter.getInt();
				return Restrictions.le(propertyName, individualCountMax);
			}
		});

		FILTER_CONSTUCTORS.put("secured", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "document.secured"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("hasSample", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (Boolean.FALSE.equals(filter.getBoolean())) {
					return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.sampleCount"), 0);
				}
				return Restrictions.gt(mappings.getEntityReference(filter.getBase(), "unit.sampleCount"), 0);
			}
		});

		FILTER_CONSTUCTORS.put("annotated", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (Boolean.FALSE.equals(filter.getBoolean())) {
					return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.annotationCount"), 0);
				}
				return Restrictions.gt(mappings.getEntityReference(filter.getBase(), "unit.annotationCount"), 0);
			}
		});

		FILTER_CONSTUCTORS.put("annotationType", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (filter.getEnums().contains(AnnotationType.ALL)) return NOP_CRITERION;
				List<String> values = new ArrayList<>();
				for (Enum<?> e : filter.getEnums()) {
					values.add(e.name());
				}
				return Restrictions.in("unitAnnotation.resolvedType", values);
			}
		});

		FILTER_CONSTUCTORS.put("includeSystemAnnotations", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				if (Boolean.FALSE.equals(filter.getBoolean())) {
					return Restrictions.isNotNull(mappings.getEntityReference(filter.getBase(), "unit.annotations.annotationByPerson"));
				}
				return NOP_CRITERION;
			}
		});

		FILTER_CONSTUCTORS.put("annotatedSameOrAfter", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.ge(mappings.getEntityReference(filter.getBase(), "unit.annotations.created"), filter.getDate());
			}
		});

		FILTER_CONSTUCTORS.put("annotatedSameOrBefore", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.le(mappings.getEntityReference(filter.getBase(), "unit.annotations.created"), filter.getDate());
			}
		});

		FILTER_CONSTUCTORS.put("teamMember", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> orFilters = new ArrayList<>();
				for (String searchTerm : filter.getStrings()) {
					orFilters.add(Restrictions.ilike("a.id", searchTerm.replace("*", "%"))); // a.id == agent.name
				}
				Criterion c = orFilters(orFilters);
				DetachedCriteria subquery = DetachedCriteria.forClass(TeamAgent.class)
						.createAlias("agent", "a", JoinType.INNER_JOIN)
						.add(c)
						.setProjection(Property.forName("teamKey"));
				return Subqueries.propertyIn(teamKey(filter.getBase()), subquery);
			}
		});

		FILTER_CONSTUCTORS.put("teamMemberId", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				DetachedCriteria subquery = DetachedCriteria.forClass(TeamAgent.class)
						.add(Restrictions.in("agentKey", filter.getLongs()))
						.setProjection(Property.forName("teamKey"));
				return Subqueries.propertyIn(teamKey(filter.getBase()), subquery);
			}

		});

		FILTER_CONSTUCTORS.put("sampleMultiple", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				return Restrictions.eq(mappings.getEntityReference(filter.getBase(), "unit.samples.multiple"), filter.getBoolean());
			}
		});

		FILTER_CONSTUCTORS.put("collectionAndRecordQuality", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<String> acceptedValues = new ArrayList<>();
				for (CollectionAndRecordQuality q : filter.getCollectionAndRecordQualities()) {
					if (q.getCollectionQuality() == null) throw new IllegalArgumentException("Collection quality must be given for collectionAndRecordQuality");
					if (q.getRecordQuality().isEmpty()) throw new IllegalArgumentException("At least one record quality must be given for collectionAndRecordQuality");
					for (RecordQuality rq : q.getRecordQuality()) {
						acceptedValues.add(q.getCollectionQuality().name()+":"+rq.name());
					}
				}
				return Restrictions.in(mappings.getEntityReference(filter.getBase(), "unit.interpretations.collectionAndRecordQuality"), acceptedValues);
			}
		});

		FILTER_CONSTUCTORS.put("partition", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				Partition partition = filter.getPartition();
				return Restrictions.sqlRestriction("mod(hash({alias}.document_id), "+partition.getSize()+") = " + (partition.getIndex()-1));
			}
		});

		FILTER_CONSTUCTORS.put("hasValue", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				List<Criterion> filters = new ArrayList<>();
				for (String fieldName : filter.getStrings()) {
					filters.add(Restrictions.isNotNull(mappings.getEntityReference(filter.getBase(), fieldName)));
				}
				return andFilters(filters);
			}
		});

		FILTER_CONSTUCTORS.put("onlyNonStateLands", new FilterConstructor() {
			@Override
			public Criterion createCriteria(Filter filter) {
				String reference = mappings.getEntityReference(filter.getBase(), "gathering.stateLand");
				if (filter.getBoolean()) {
					return Restrictions.or(
							Restrictions.isNull(reference),
							Restrictions.eq(reference, false));
				}
				return Restrictions.eq(reference, true);
			}
		});

		validateAllFiltersAreMapped();
	}

	private Date endOfDay(Date date) {
		return org.apache.commons.lang3.time.DateUtils.addMilliseconds(org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.DATE), -1);
	}

	private void addTaxonFilter(List<Criterion> orFilters, Qname taxonId, Filter filter) {
		if (Boolean.FALSE.equals(filter.getParent().getIncludeSubTaxa())) {
			addExactTaxonSearch(orFilters, taxonId, filter);
			return;
		}
		if (!dao.hasTaxon(taxonId)) {
			addExactTaxonSearch(orFilters, taxonId, filter); // will not cause matches
			return;
		}
		Taxon searchedTaxon = dao.getTaxon(taxonId);
		if (hasSameRankMultipleTimes(searchedTaxon)) {
			addExactTaxonSearch(orFilters, getAllIdsIncludingSelf(searchedTaxon), filter);
			return;
		}
		if (searchedTaxon.getTaxonRank() == null) {
			addExactTaxonSearch(orFilters, taxonId, filter);
			for (Taxon rankedChild : getRankedChildren(searchedTaxon)) {
				addIncludeSubtaxaSearch(orFilters, rankedChild.getQname(), rankedChild.getTaxonRank(), filter);
			}
			return;
		}
		addIncludeSubtaxaSearch(orFilters, taxonId, searchedTaxon.getTaxonRank(), filter);
	}

	private Collection<Qname> getAllIdsIncludingSelf(Taxon searchedTaxon) {
		return getAllIdsIncludingSelf(searchedTaxon, new ArrayList<>());
	}

	private Collection<Qname> getAllIdsIncludingSelf(Taxon searchedTaxon, Collection<Qname> ids) {
		ids.add(searchedTaxon.getId());
		for (Taxon child : searchedTaxon.getChildren()) {
			getAllIdsIncludingSelf(child, ids);
		}
		return ids;
	}

	private boolean hasSameRankMultipleTimes(Taxon searchedTaxon) {
		if (!searchedTaxon.hasParent()) return false;
		Taxon parent = searchedTaxon.getParent();
		return parent.getParentOfRank(searchedTaxon.getTaxonRank()) != null; // getParentOfRank will return taxon itself if it is of the requested rank
	}

	private List<Taxon> getRankedChildren(Taxon taxon) {
		List<Taxon> rankedChildren = new ArrayList<>();
		for (Taxon child : taxon.getChildren()) {
			if (child.getTaxonRank() != null) {
				rankedChildren.add(child);
			} else {
				rankedChildren.addAll(getRankedChildren(child));
			}
		}
		return rankedChildren;
	}

	private void addIncludeSubtaxaSearch(List<Criterion> orFilters, Qname taxonId, Qname rank, Filter filter) {
		String taxonRankSetterName = rank.toString().replace("MX.", "") + "Id";
		String taxonTable = getTaxonTable(filter);
		orFilters.add(Restrictions.eq(taxonTable+"." + taxonRankSetterName, TaxonEntity.parseTaxonKey(taxonId)));
	}

	private void addExactTaxonSearch(List<Criterion> orFilters, Qname taxonId, Filter filter) {
		String taxonTable = getTaxonTable(filter);
		orFilters.add(Restrictions.eq(taxonTable+".key", TaxonEntity.parseTaxonKey(taxonId)));
	}

	private void addExactTaxonSearch(List<Criterion> orFilters, Collection<Qname> ids, Filter filter) {
		List<Long> keys = ids.stream().map(id->TaxonEntity.parseTaxonKey(id)).collect(Collectors.toList());
		String taxonTable = getTaxonTable(filter);
		orFilters.add(Restrictions.in(taxonTable+".key", keys));
	}

	private String getTaxonTable(Filter filter) {
		return Boolean.FALSE.equals(filter.getParent().getUseIdentificationAnnotations()) ? "originalTaxon" : "taxon";
	}

	private String getTargetTable(Filter filter) {
		return Boolean.FALSE.equals(filter.getParent().getUseIdentificationAnnotations()) ? "originalTarget" : "target";
	}

	protected int intvalOr(String s, int defaultValue) {
		if (s == null || s.isEmpty()) return defaultValue;
		return Integer.valueOf(s);
	}

	private void validateAllFiltersAreMapped() {
		for (Field f : Filters.class.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers()) || Modifier.isFinal(f.getModifiers())) continue;
			if (!FILTER_CONSTUCTORS.containsKey(f.getName())) throw new IllegalStateException("Unmapped filter: " + f.getName());
		}
	}

	private double trimToYkjLength(double coord) {
		String val = Integer.toString(Double.valueOf(coord).intValue());
		while (val.length() < 7) {
			val += "0";
		}
		return Double.valueOf(val);
	}

	private String getCoordinateFieldPrefix(Type type) {
		if (type == Type.WGS84) return "gathering.conversions.wgs84.";
		if (type == Type.YKJ) return "gathering.conversions.ykj.";
		if (type == Type.EUREF) return "gathering.conversions.euref.";
		throw new UnsupportedOperationException("Search using coordinate type " + type + " is not supported");
	}

	private Criterion createTimeSpanFilter(String timeSpan, String beginReference, String endReference, int beginDefault, int endDefault, Filter filter) {
		timeSpan = Utils.removeWhitespace(timeSpan);
		String begin = timeSpan;
		String end = null;
		if (timeSpan.equals("/")) {
			begin = null;
		} else if (timeSpan.contains("/")) {
			String[] parts = timeSpan.split(Pattern.quote("/"), -1);
			begin = parts[0];
			end = parts[1];
		}
		int beginKey = intvalOr(begin, beginDefault);
		int endKey = intvalOr(end, endDefault);
		String yearReference =  mappings.getEntityReference(filter.getBase(), "gathering.conversions.year");
		if (beginKey == endKey) {
			return Restrictions.and(Restrictions.eq(beginReference, beginKey), Restrictions.eq(endReference, endKey), Restrictions.isNotNull(yearReference));
		} else if (beginKey < endKey) {
			return Restrictions.and(Restrictions.ge(beginReference, beginKey), Restrictions.le(endReference, endKey), Restrictions.isNotNull(yearReference));
		}
		return Restrictions.or(
				Restrictions.and(Restrictions.isNotNull(yearReference), Restrictions.or(Restrictions.ge(beginReference, beginKey), Restrictions.le(endReference, endKey))),
				Restrictions.and(Restrictions.isNull(yearReference), Restrictions.ge(beginReference, beginKey), Restrictions.le(endReference, endKey))
				);
	}

	private Criterion createTimeFilter(Filter filter, Set<String> values) {
		List<Criterion> orFilters = new ArrayList<>();
		for (String time : values) {
			orFilters.add(createTimeFilter(filter, time));
		}
		return orFilters(orFilters);
	}

	private Criterion createTimeFilter(Filter filter, String time) {
		String begin = time;
		String end = time;
		if (time.contains("/")) {
			String[] parts = time.split(Pattern.quote("/"));
			begin = parts[0];
			if (parts.length == 2) {
				end = parts[1];
			} else {
				end = null;
			}
		}
		Long beginKey = toDateKey(begin, "0");
		Long endKey = toDateKey(end, "9");
		if (beginKey != null && endKey != null) {
			return Restrictions.and(
					Restrictions.ge(mappings.getEntityReference(filter.getBase(), "gathering.eventDate.begin"), beginKey),
					Restrictions.le(mappings.getEntityReference(filter.getBase(), "gathering.eventDate.end"), endKey));
		}
		if (endKey != null) {
			return Restrictions.le(mappings.getEntityReference(filter.getBase(), "gathering.eventDate.end"), endKey);
		}
		if (beginKey != null) {
			return Restrictions.ge(mappings.getEntityReference(filter.getBase(), "gathering.eventDate.begin"), beginKey);
		}
		throw new IllegalStateException();
	}

	private Long toDateKey(String date, String pad) {
		if (date == null || date.isEmpty()) return null;
		try {
			if (date.equals("0") || date.startsWith("-")) {
				int offset = Integer.valueOf(date);
				Calendar cal = GregorianCalendar.getInstance();
				cal.add(Calendar.DAY_OF_YEAR, offset);
				Date offsetted = cal.getTime();
				return Long.valueOf(DateUtils.format(offsetted, "yyyyMMdd"));
			}
			date = date.replace("-", "");
			while (date.length() < DATE_KEY_LENGTH) {
				date += pad;
			}
			return Long.valueOf(date);
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid time filter value " + date);
		}
	}

	private String teamKey(Base base) {
		if (base == Base.GATHERING) return "foreignKeys.teamKey";
		return "gatheringForeignKeys.teamKey";
	}

	private static String gatheringKey(Base base) {
		if (base == Base.GATHERING) return "key";
		return "foreignKeys.gatheringKey";
	}

	private static String documentKey(Base base) {
		if (base == Base.DOCUMENT) return "key";
		if (base == Base.GATHERING) return "foreignKeys.documentKey";
		return "gatheringForeignKeys.documentKey";
	}

	private static String unitKey(Base base) {
		if (base == Base.UNIT_MEDIA) return "mediaForeignKeys.unitKey";
		return "key";
	}

	private Qname toQname(String s) {
		Qname qname = null;
		try {
			qname = Qname.fromURI(s);
		} catch (Exception e) {}
		return qname;
	}
}
