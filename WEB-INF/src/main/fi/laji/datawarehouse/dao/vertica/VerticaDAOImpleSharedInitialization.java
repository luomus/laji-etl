package fi.laji.datawarehouse.dao.vertica;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import com.zaxxer.hikari.HikariDataSource;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDataSourceDefinition;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DocumentQuality;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;

public class VerticaDAOImpleSharedInitialization {

	private final Config config;
	private final String schema;
	private final String dimensionsSchema;
	private final DAO dao;
	private final HikariDataSource verticaETLDataSource;
	private final HikariDataSource verticaQueryDataSource;
	private final SessionFactory dimensionsSchemaSessionFactory;
	private final VerticaDAOImpleDimensions dimensions;

	public VerticaDAOImpleSharedInitialization(Config config, DAO dao, String schema, ThreadStatuses threadStatuses) {
		this.config = config;
		this.schema = schema;
		this.dimensionsSchema = schema + "_DIMENSIONS";
		this.dao = dao;
		int queryConnectionPoolMaxSize = config.productionMode() ? 100 : 7;
		int etlConnectionPoolMaxSize = config.productionMode() ? 20 : 3;
		this.verticaETLDataSource = VerticaDataSourceDefinition.initDataSource(config.connectionDescription("Vertica"), etlConnectionPoolMaxSize, 120);
		this.verticaQueryDataSource = VerticaDataSourceDefinition.initDataSource(config.connectionDescription("Vertica_Query"), queryConnectionPoolMaxSize, 10);
		this.dimensionsSchemaSessionFactory = buildSessionFactoryForSchema(dimensionsSchema, true);

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Creating dimensions for schema " + dimensionsSchema +"...");
		this.dimensions = new VerticaDAOImpleDimensions(dimensionsSchemaSessionFactory, dimensionsSchema, dao, threadStatuses);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Dimensions for schema " + dimensionsSchema +" created!");
	}

	public void close() {
		try { dimensionsSchemaSessionFactory.close(); } catch (Exception e) {}
		try { verticaETLDataSource.close(); } catch (Exception e) {}
		try { verticaQueryDataSource.close(); } catch (Exception e) {}
	}

	public SessionFactory buildSessionFactoryForSchema(String dataSchema, boolean etl) {
		Configuration dataSchemaConfiguration = initConfigurationForSchema(dataSchema, etl);
		return dataSchemaConfiguration
				.buildSessionFactory(
						new StandardServiceRegistryBuilder()
						.applySettings(dataSchemaConfiguration.getProperties())
						.applySetting(Environment.DATASOURCE, etl ? verticaETLDataSource : verticaQueryDataSource)
						.build());
	}

	private Configuration initConfigurationForSchema(String dataSchema, boolean etl) {
		Configuration configuration = new Configuration()
				.setProperty("hibernate.dialect", "fi.laji.datawarehouse.dao.vertica.VerticaDialect")
				.setProperty("hibernate.jdbc.batch_size", "1000000")
				.setProperty("hibernate.cache.use_second_level_cache", "false");
		if (config.developmentMode()) {
			configuration.setProperty("hibernate.show_sql", "true"); // XXX
			configuration.setProperty("hibernate.format_sql", etl ? "false" : "true");
		} else {
			configuration.setProperty("hibernate.show_sql", "false");
		}
		addAnnotatedClasses(configuration);
		configuration.setProperty("hibernate.default_schema", dataSchema);
		return configuration;
	}

	private void addAnnotatedClasses(Configuration configuration) {
		configuration.addAnnotatedClass(BaseModel.class)
		.addAnnotatedClass(Document.class)
		.addAnnotatedClass(DocumentEntity.class)
		.addAnnotatedClass(BaseEntity.class)
		.addAnnotatedClass(NameableEntity.class)
		.addAnnotatedClass(CollectionEntity.class)
		.addAnnotatedClass(SourceEntity.class)
		.addAnnotatedClass(FormEntity.class)
		.addAnnotatedClass(SecureLevelEntity.class)
		.addAnnotatedClass(SecureReasonEntity.class)
		.addAnnotatedClass(LifeStageEntity.class)
		.addAnnotatedClass(RecordBasisEntity.class)
		.addAnnotatedClass(SexEntity.class)
		.addAnnotatedClass(AbundanceUnitEntity.class)
		.addAnnotatedClass(PropertyEntity.class)
		.addAnnotatedClass(Fact.class)
		.addAnnotatedClass(FactLink.class)
		.addAnnotatedClass(DocumentFact.class)
		.addAnnotatedClass(DocumentKeyword.class)
		.addAnnotatedClass(UnitKeyword.class)
		.addAnnotatedClass(GeoSourceEntity.class)
		.addAnnotatedClass(AgentEntity.class)
		.addAnnotatedClass(TeamEntity.class)
		.addAnnotatedClass(TeamAgent.class)
		.addAnnotatedClass(AreaEntity.class)
		.addAnnotatedClass(Gathering.class)
		.addAnnotatedClass(GatheringEntity.class)
		.addAnnotatedClass(DateEntity.class)
		.addAnnotatedClass(GatheringFact.class)
		.addAnnotatedClass(DocumentUserId.class)
		.addAnnotatedClass(GatheringUserId.class)
		.addAnnotatedClass(UserIdEntity.class)
		.addAnnotatedClass(ReferenceEntity.class)
		.addAnnotatedClass(TaxonEntity.class)
		.addAnnotatedClass(TargetEntity.class)
		.addAnnotatedClass(UnitFact.class)
		.addAnnotatedClass(Unit.class)
		.addAnnotatedClass(UnitEntity.class)
		.addAnnotatedClass(TaxonBaseEntity.class)
		.addAnnotatedClass(TaxonTempEntity.class)
		.addAnnotatedClass(PersonBaseEntity.class)
		.addAnnotatedClass(PersonEntity.class)
		.addAnnotatedClass(PersonTempEntity.class)
		.addAnnotatedClass(InformalTaxonGroupEntity.class)
		.addAnnotatedClass(TaxonSetEntity.class)
		.addAnnotatedClass(AdministrativeStatusEntity.class)
		.addAnnotatedClass(TaxonToAdministrativeStatusBaseEntity.class)
		.addAnnotatedClass(TaxonToAdministrativeStatus.class)
		.addAnnotatedClass(TaxonToAdministrativeStatusTemp.class)
		.addAnnotatedClass(TaxonToInformalTaxonGroupBaseEntity.class)
		.addAnnotatedClass(TaxonToInformalTaxonGroup.class)
		.addAnnotatedClass(TaxonToInformalTaxonGroupTemp.class)
		.addAnnotatedClass(TaxonToTaxonSetBaseEntity.class)
		.addAnnotatedClass(TaxonToTaxonSet.class)
		.addAnnotatedClass(TaxonToTaxonSetTemp.class)
		.addAnnotatedClass(TaxonToHabitat.class)
		.addAnnotatedClass(TaxonToHabitatTemp.class)
		.addAnnotatedClass(TaxonToHabitatBaseEntity.class)
		.addAnnotatedClass(OccurrenceTypeEntity.class)
		.addAnnotatedClass(TaxonToTypeOfOccurrenceBaseEntity.class)
		.addAnnotatedClass(TaxonToTypeOfOccurrence.class)
		.addAnnotatedClass(TaxonToTypeOfOccurrenceTemp.class)
		.addAnnotatedClass(MediaObject.class)
		.addAnnotatedClass(MediaBaseEntity.class)
		.addAnnotatedClass(DocumentMedia.class)
		.addAnnotatedClass(GatheringMedia.class)
		.addAnnotatedClass(UnitMediaEntity.class)
		.addAnnotatedClass(MediaTypeEntity.class)
		.addAnnotatedClass(DocumentSecureReason.class)
		.addAnnotatedClass(GatheringGeometry.class)
		.addAnnotatedClass(InvasiveControlEntity.class)
		.addAnnotatedClass(TaxonConfidenceEntity.class)
		.addAnnotatedClass(DocumentQuality.class)
		.addAnnotatedClass(GatheringQuality.class)
		.addAnnotatedClass(UnitQuality.class)
		.addAnnotatedClass(QualityIssueEntity.class)
		.addAnnotatedClass(QualitySourceEntity.class)
		.addAnnotatedClass(RecordQualityEntity.class)
		.addAnnotatedClass(AnnotationEntity.class)
		.addAnnotatedClass(Quality.class)
		.addAnnotatedClass(GatheringArea.class)
		.addAnnotatedClass(GatheringMunicipality.class)
		.addAnnotatedClass(GatheringBioprovince.class)
		.addAnnotatedClass(TaxonCensusEntity.class)
		.addAnnotatedClass(NamedPlaceEntity.class)
		.addAnnotatedClass(TargetChecklistTaxaEntity.class)
		.addAnnotatedClass(Sample.class)
		.addAnnotatedClass(SampleEntity.class)
		.addAnnotatedClass(SampleFact.class)
		.addAnnotatedClass(SampleKeyword.class)
		.addAnnotatedClass(CollectionQualityEntity.class)
		.addAnnotatedClass(ReliabilityEntity.class)
		.addAnnotatedClass(TagEntity.class)
		.addAnnotatedClass(UnitEffectiveTag.class)
		.addAnnotatedClass(GatheringDocumentUserId.class)
		.addAnnotatedClass(GatheringDocumentSecureReason.class)
		.addAnnotatedClass(GatheringDocumentKeyword.class)
		.addAnnotatedClass(GatheringDocumentFact.class)
		.addAnnotatedClass(GatheringDocumentMedia.class)
		.addAnnotatedClass(UnitDocumentUserId.class)
		.addAnnotatedClass(UnitDocumentSecureReason.class)
		.addAnnotatedClass(UnitDocumentKeyword.class)
		.addAnnotatedClass(UnitDocumentFact.class)
		.addAnnotatedClass(UnitDocumentMedia.class)
		.addAnnotatedClass(UnitGatheringUserId.class)
		.addAnnotatedClass(UnitGatheringFact.class)
		.addAnnotatedClass(UnitGatheringMedia.class)
		.addAnnotatedClass(UnitGatheringTaxonCensus.class)
		.addAnnotatedClass(SplittedDocumentIdEntity.class)
		.addAnnotatedClass(DocumentIdEntity.class);
	}

	public DAO getDao() {
		return dao;
	}

	public VerticaDAOImpleDimensions getDimensions() {
		return dimensions;
	}

	public String getDimensionsSchema() {
		return dimensionsSchema;
	}

	public String getSchema() {
		return schema;
	}

	public boolean isProductionMode() {
		return config.productionMode();
	}

}
