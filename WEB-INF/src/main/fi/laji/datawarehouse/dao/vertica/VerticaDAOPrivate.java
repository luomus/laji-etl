package fi.laji.datawarehouse.dao.vertica;

import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;

public class VerticaDAOPrivate extends VerticaDAOImple {

	public VerticaDAOPrivate(VerticaDAOImpleSharedInitialization shared) {
		super(shared, shared.getSchema() + "_PRIVATE");
	}

	// This is for tests
	public VerticaDAOPrivate(VerticaDAOImpleSharedInitialization shared, String dataSchema) {
		super(shared, dataSchema);
	}

	@Override
	protected Concealment concealment() {
		return Concealment.PRIVATE;
	}

}
