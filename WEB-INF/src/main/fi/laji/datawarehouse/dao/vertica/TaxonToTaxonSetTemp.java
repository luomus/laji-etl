package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_taxonset_temp")
class TaxonToTaxonSetTemp extends TaxonToTaxonSetBaseEntity {

	private static final long serialVersionUID = 5911460447543287720L;

}
