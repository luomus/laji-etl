package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="source")
class SourceEntity extends NameableEntity {

	public SourceEntity() {}

	public SourceEntity(String uri) {
		setId(uri);
	}

}
