package fi.laji.datawarehouse.dao.vertica;

import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.luomus.commons.containers.rdf.Qname;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_temp")
class TaxonTempEntity extends TaxonBaseEntity {
	
	public TaxonTempEntity() {}

	public TaxonTempEntity(Qname id) {
		super(id);
	}
	
}
