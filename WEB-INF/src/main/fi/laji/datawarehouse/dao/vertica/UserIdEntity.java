package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.BaseEntity;

@Entity
@Table(name="userid")
class UserIdEntity extends BaseEntity {

	private Long personKey;

	public UserIdEntity() {}

	public UserIdEntity(String userId) {
		setId(userId);
	}

	@Column(name="person_key")
	public Long getPersonKey() {
		return personKey;
	}

	public void setPersonKey(Long personKey) {
		this.personKey = personKey;
	}

}
