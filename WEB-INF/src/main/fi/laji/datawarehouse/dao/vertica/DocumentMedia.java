package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;

@Entity
@Table(name="document_media")
public class DocumentMedia extends MediaBaseEntity {

	@Deprecated // for hibernate
	public DocumentMedia() {}

	public DocumentMedia(MediaObject media) {
		super(media);
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", referencedColumnName="key", insertable=false, updatable=false)
	public DocumentEntity getDocument() { // for queries only
		return null;
	}

	public void setDocument(@SuppressWarnings("unused") DocumentEntity document) {
		// for queries only
	}

}
