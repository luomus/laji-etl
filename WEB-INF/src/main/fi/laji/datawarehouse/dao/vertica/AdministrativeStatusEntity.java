package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="administrativestatus")
class AdministrativeStatusEntity extends NameableEntity {

	public AdministrativeStatusEntity() {}

	public AdministrativeStatusEntity(String statusId) {
		setId(statusId);
	}	

}
