package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class TaxonToInformalTaxonGroupBaseEntity implements Serializable {

	private static final long serialVersionUID = -4346021770337663504L;

	private Long taxonKey;
	private Long groupKey;

	@Id @Column(name="taxon_key")
	public Long getTaxonKey() {
		return taxonKey;
	}

	public void setTaxonKey(Long taxonKey) {
		this.taxonKey = taxonKey;
	}

	@Id @Column(name="group_key")
	public Long getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(Long groupKey) {
		this.groupKey = groupKey;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
