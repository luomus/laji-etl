package fi.laji.datawarehouse.dao.vertica;

import java.util.List;

import org.hibernate.transform.ResultTransformer;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;

class AggregatedCountResultTransformer implements ResultTransformer {

	private static final long serialVersionUID = 4557770277138767362L;
	private static final AggregatedCountResultTransformer instance = new AggregatedCountResultTransformer();

	public static ResultTransformer instance() {
		return instance;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List transformList(List allResults) {
		return allResults;
	}

	@Override
	public Object transformTuple(Object[] values, String[] fields) {
		try {
			if (values.length == 0) throw new IllegalStateException("Values is empty");
			return values[values.length-1];
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

}
