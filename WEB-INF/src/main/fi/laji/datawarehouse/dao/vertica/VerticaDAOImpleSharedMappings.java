package fi.laji.datawarehouse.dao.vertica;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.FieldDefinition;
import fi.laji.datawarehouse.query.model.Base;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.luomus.commons.utils.ReflectionUtil;

public class VerticaDAOImpleSharedMappings {

	/**
	 * By what name dw-model fields are known "looking from unit entity"
	 */
	private final static Map<String, String> FIELD_TO_UNITENTITY_REFERENCE = unitBaseRef(Base.UNIT);

	private final static Map<String, String> FIELD_TO_SAMPLE_UNITENTITY_REFERENCE = unitBaseRef(Base.SAMPLE);

	private final static Map<String, String> FIELD_TO_ANNOTATION_UNITENTITY_REFERENCE = unitBaseRef(Base.ANNOTATION);

	private final static Map<String, String> FIELD_TO_UNIT_MEDIA_UNITENTITY_REFERENCE = unitBaseRef(Base.UNIT_MEDIA);

	private static Map<String, String> unitBaseRef(Base base) {
		Map<String, String> unitRef = new TreeMap<>();

		Set<String> fields = new HashSet<>();
		fields.addAll(Fields.sortable(base).getAllFields());
		fields.addAll(Fields.aggregatable(base).getAllFields());

		if (base == Base.UNIT_MEDIA) {
			unitRef.put("unit.unitId", "mediaForeignKeys.unitId");
		} else {
			unitRef.put("unit.unitId", "id");
		}

		// ------- FOREIGN KEYS -------------

		// Id to Key
		unitRef.put("document.collectionId", "documentForeignKeys.collectionKey");
		unitRef.put("document.sourceId", "documentForeignKeys.sourceKey");
		unitRef.put("document.documentId", "gatheringForeignKeys.documentId");
		unitRef.put("gathering.gatheringId", "foreignKeys.gatheringId");
		unitRef.put("unit.referencePublication", "foreignKeys.referenceKey");
		unitRef.put("unit.interpretations.annotatedTaxonId", "foreignKeys.annotatedTaxonKey");
		unitRef.put("unit.abundanceUnit", "foreignKeys.abundanceUnitKey");

		// Date to Key
		unitRef.put("gathering.eventDate.begin", "gatheringForeignKeys.eventDateBeginKey");
		unitRef.put("gathering.eventDate.end", "gatheringForeignKeys.eventDateEndKey");

		// Enum to Key
		unitRef.put("document.secureLevel", "documentForeignKeys.secureLevelKey");
		unitRef.put("document.quality.issue.issue", "documentForeignKeys.documentIssueKey");
		unitRef.put("document.quality.issue.source", "documentForeignKeys.documentIssueSourceKey");
		unitRef.put("gathering.interpretations.country", "gatheringForeignKeys.countryKey");
		unitRef.put("gathering.interpretations.biogeographicalProvince", "gatheringForeignKeys.biogeographicalProvinceKey");
		unitRef.put("gathering.interpretations.finnishMunicipality", "gatheringForeignKeys.finnishMunicipalityKey");
		unitRef.put("gathering.interpretations.sourceOfCountry", "gatheringForeignKeys.sourceOfCountryKey");
		unitRef.put("gathering.interpretations.sourceOfBiogeographicalProvince", "gatheringForeignKeys.sourceOfBiogeographicalProvinceKey");
		unitRef.put("gathering.interpretations.sourceOfFinnishMunicipality", "gatheringForeignKeys.sourceOfFinnishMunicipalityKey");
		unitRef.put("gathering.interpretations.sourceOfCoordinates", "gatheringForeignKeys.sourceOfCoordinatesKey");
		unitRef.put("gathering.conversions.birdAssociationArea", "gatheringForeignKeys.birdAssociationAreaKey");
		unitRef.put("gathering.quality.issue.issue", "gatheringForeignKeys.issueKey");
		unitRef.put("gathering.quality.issue.source", "gatheringForeignKeys.issueSourceKey");
		unitRef.put("gathering.quality.timeIssue.issue", "gatheringForeignKeys.timeIssueKey");
		unitRef.put("gathering.quality.timeIssue.source", "gatheringForeignKeys.timeIssueSourceKey");
		unitRef.put("gathering.quality.locationIssue.issue", "gatheringForeignKeys.locationIssueKey");
		unitRef.put("gathering.quality.locationIssue.source", "gatheringForeignKeys.locationIssueSourceKey");
		unitRef.put("unit.quality.issue.issue", "foreignKeys.issueKey");
		unitRef.put("unit.quality.issue.source", "foreignKeys.issueSourceKey");
		unitRef.put("unit.interpretations.reliability", "foreignKeys.reliabilityKey");
		unitRef.put("unit.interpretations.recordQuality", "foreignKeys.recordQualityKey");
		unitRef.put("unit.lifeStage", "foreignKeys.lifeStageKey");
		unitRef.put("unit.recordBasis", "foreignKeys.recordBasisKey");
		unitRef.put("unit.superRecordBasis", "foreignKeys.superRecordBasisKey");
		unitRef.put("unit.sex", "foreignKeys.sexKey");
		unitRef.put("unit.reportedTaxonConfidence", "foreignKeys.taxonConfidenceKey");
		unitRef.put("unit.interpretations.invasiveControlEffectiveness", "foreignKeys.invasiveControlEffectivenessKey");
		unitRef.put("unit.reportedInformalTaxonGroup", "foreignKeys.reportedInformalTaxonGroupKey");

		// --------JOINS------------- Names that are defined here should match with aliases defined in VerticaDAOImpleQueries
		unitRef.put("document.editorUserIds", "documentUserId.userIdKey"); // unit -> document_userid AS documentUserId
		unitRef.put("document.linkings.editors", "documentUserIdUserId.personKey"); // unit -> document_userid -> userid AS documentUserdIdUserId
		unitRef.put("document.keywords", "documentKeyword.keyword"); // unit -> document_keyword AS documentKeyword
		unitRef.put("document.secureReasons", "documentSecureReason.secureReasonKey"); // unit -> document_securereason as secureReason
		unitRef.put("document.linkings.collectionQuality", "collection.collectionQualityKey"); // unit -> collection as collection

		unitRef.put("gathering.team", "team.teamText"); // unit -> team AS team
		unitRef.put("gathering.team.memberId", "teamAgent.agentKey"); // unit -> team -> team_agent AS teamAgent
		unitRef.put("gathering.team.memberName", "teamAgentAgent.id"); // unit -> team -> team_agent -> agent AS teamAgentAgent
		unitRef.put("gathering.observerUserIds", "gatheringUserId.userIdKey"); // unit -> gathering_userid AS gatheringUserId
		unitRef.put("gathering.linkings.observers", "gatheringUserIdUserId.personKey"); // unit -> gathering_userid -> userid AS gatheringUserdIdUserId
		unitRef.put("gathering.taxonCensus.taxonId", "taxonCensus.taxonKey"); // unit -> taxonCensus AS taxonCensus
		unitRef.put("gathering.taxonCensus.type", "taxonCensus.type"); // unit -> taxonCensus AS taxonCensus

		if (base == Base.ANNOTATION) {
			unitRef.put("unit.annotations.id", "unitAnnotation.id");
		}
		unitRef.put("unit.annotations.annotationByPerson", "unitAnnotation.annotationByPersonKey");
		unitRef.put("unit.annotations.annotationBySystem", "unitAnnotation.annotationBySystemKey");
		unitRef.put("unit.annotations.annotationByPersonName", "unitAnnotation.annotationByPersonName");
		unitRef.put("unit.annotations.annotationBySystemName", "unitAnnotation.annotationBySystemName");
		unitRef.put("unit.annotations.created", "unitAnnotation.created");

		unitRef.put("unit.keywords", "unitKeyword.keyword"); // unit -> unit_keyword as unitKeyword
		unitRef.put("unit.interpretations.effectiveTags", "unitEffectiveTag.tagKey"); // unit -> unit_effective_tag as unitEffectiveTag

		// NamedPlace joins
		for (Method m : ReflectionUtil.getGetters(NamedPlaceEntity.class)) {
			if (ignore(m)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (fieldName.equals("tags")) continue;
			if (fieldName.equals("ykj10km")) {
				unitRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				unitRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			if (fieldName.equals("wgs84CenterPoint")) {
				unitRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				unitRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			unitRef.put("document.namedPlace."+fieldName, "namedPlace."+fieldName);
		}

		// Fact joins
		for (Method m : ReflectionUtil.getGetters(Fact.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			String mappedName = fieldName;
			if (fieldName.equals("fact")) mappedName = "propertyKey";
			unitRef.put("document.facts." + fieldName, "documentFact." + mappedName); // unit -> document_fact AS documentFact
			unitRef.put("gathering.facts." + fieldName, "gatheringFact." + mappedName); // unit -> gathering_fact AS gatheringFact
			unitRef.put("unit.facts." + fieldName, "unitFact." + mappedName); // unit -> unit_fact AS unitFact
			unitRef.put("unit.samples.facts." + fieldName, "sampleFact." + mappedName); // unit -> sample -> sample_fact AS sampleFact
		}

		// Media joins
		for (Method m : ReflectionUtil.getGetters(MediaObject.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (fields.contains("unit.media."+fieldName)) {
				if (base == Base.UNIT_MEDIA) {
					unitRef.put("unit.media." + fieldName, "mediaObject." + fieldName);
				} else {
					unitRef.put("unit.media." + fieldName, "unitMedia.mediaObject." + fieldName); // unit -> unit_media AS unitMedia
				}
			}
			if (fields.contains("gathering.media."+fieldName))
				unitRef.put("gathering.media." + fieldName, "gatheringMedia.mediaObject." + fieldName); // unit -> gathering_media AS gatheringMedia
			if (fields.contains("document.media."+fieldName))
				unitRef.put("document.media." + fieldName, "documentMedia.mediaObject." + fieldName); // unit -> document_media AS documentMedia
		}
		unitRef.put("document.media.mediaType", "documentMedia.mediaTypeKey");
		unitRef.put("gathering.media.mediaType", "gatheringMedia.mediaTypeKey");
		if (base == Base.UNIT_MEDIA) {
			unitRef.put("unit.media.mediaType", "mediaForeignKeys.mediaTypeKey");
		} else {
			unitRef.put("unit.media.mediaType", "unitMedia.mediaForeignKeys.mediaTypeKey");
		}

		// Sample join
		for (Method m : ReflectionUtil.getGetters(Sample.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (!fields.contains("unit.samples."+fieldName)) continue;
			unitRef.put("unit.samples." + fieldName, "sample.sample."+fieldName); // unit -> sample as sample
		}
		unitRef.put("unit.samples.sampleId", "sample.id");
		unitRef.put("unit.samples.collectionId", "sample.collectionKey");
		unitRef.put("unit.samples.keywords", "sampleKeyword.keyword"); // unit -> sample -> sample_keyword as sampleKeyword

		// Taxon joins
		for (String field : fields) {
			if (field.startsWith("unit.linkings.taxon.")) {
				unitRef.put(field, field.replace("unit.linkings.taxon.", "taxon."));  // unit -> target -> taxon AS taxon
			}
		}
		for (String field : fields) {
			if (field.startsWith("unit.linkings.originalTaxon.")) {
				unitRef.put(field, field.replace("unit.linkings.originalTaxon.", "originalTaxon."));  // unit -> originalTarget -> taxon AS originalTaxon
			}
		}
		unitRef.put("unit.linkings.taxon.informalTaxonGroups", "taxonToGroup.groupKey"); // unit -> target -> taxon -> taxon_taxongroup as taxonToGroup
		unitRef.put("unit.linkings.taxon.taxonSets", "taxonToSet.setKey"); // unit -> target -> taxon -> taxon_taxonset as taxonToSet
		unitRef.put("unit.linkings.taxon.administrativeStatuses", "taxonToStatus.statusKey"); // unit -> target -> taxon -> taxon_adminstatus AS taxonToStatus
		unitRef.put("unit.linkings.taxon.typesOfOccurrenceInFinland", "taxonToTypeOfOccurrence.typeKey"); // unit -> target -> taxon -> taxon_occurrencetype AS taxonToTypeOfOccurrence
		unitRef.put("unit.linkings.taxon.habitats", "taxonToHabitat.habitat"); // unit -> target -> taxon -> taxon_habitat as taxonToHabitat
		unitRef.put("unit.linkings.originalTaxon.informalTaxonGroups", "originalTaxonToGroup.groupKey"); // unit -> target -> taxon -> taxon_taxongroup as originalTaxonToGroup
		unitRef.put("unit.linkings.originalTaxon.taxonSets", "originalTaxonToSet.setKey"); // unit -> target -> taxon -> taxon_taxonset as originalTaxonToSet
		unitRef.put("unit.linkings.originalTaxon.administrativeStatuses", "originalTaxonToStatus.statusKey"); // unit -> target -> taxon -> taxon_adminstatus AS taxonToStatus
		unitRef.put("unit.linkings.originalTaxon.typesOfOccurrenceInFinland", "originalTaxonToTypeOfOccurrence.typeKey"); // unit -> target -> taxon -> taxon_occurrencetype AS taxonToTypeOfOccurrence
		unitRef.put("unit.linkings.originalTaxon.habitats", "originalTaxonToHabitat.habitat"); // unit -> target -> taxon -> taxon_habitat as taxonToHabitat

		// Base mappings
		for (String field : fields) {
			if (unitRef.containsKey(field)) continue;
			if (field.startsWith("document.")) {
				unitRef.put(field, field.replace("document.", "parentDocument."));
				continue;
			}
			if (field.startsWith("gathering.")) {
				unitRef.put(field, field.replace("gathering.", "parentGathering."));
				continue;
			}
			if (field.startsWith("unit.")) {
				if (base == Base.UNIT_MEDIA) {
					unitRef.put(field, field.replace("unit.", "parentUnit."));
				} else {
					unitRef.put(field, field);
				}
			}
		}

		validateAllFieldsAreMapped(base, unitRef);

		return Collections.unmodifiableMap(unitRef);
	}

	/**
	 * By what name dw-model fields are known "looking from gathering entity"
	 */
	private final static Map<String, String> FIELD_TO_GATHERINGENTITY_REFERENCE;
	static {
		Map<String, String> gatheringRef = new TreeMap<>();

		Set<String> fields = new HashSet<>();
		fields.addAll(Fields.sortable(Base.GATHERING).getAllFields());
		fields.addAll(Fields.aggregatable(Base.GATHERING).getAllFields());

		gatheringRef.put("gathering.gatheringId", "id");

		// ------- FOREIGN KEYS -------------

		// Id to Key
		gatheringRef.put("document.collectionId", "documentForeignKeys.collectionKey");
		gatheringRef.put("document.sourceId", "documentForeignKeys.sourceKey");
		gatheringRef.put("document.documentId", "foreignKeys.documentId");

		// Date to Key
		gatheringRef.put("gathering.eventDate.begin", "foreignKeys.eventDateBeginKey");
		gatheringRef.put("gathering.eventDate.end", "foreignKeys.eventDateEndKey");

		// Enum to Key
		gatheringRef.put("document.secureLevel", "documentForeignKeys.secureLevelKey");
		gatheringRef.put("document.quality.issue.issue", "documentForeignKeys.documentIssueKey");
		gatheringRef.put("document.quality.issue.source", "documentForeignKeys.documentIssueSourceKey");
		gatheringRef.put("gathering.interpretations.country", "foreignKeys.countryKey");
		gatheringRef.put("gathering.interpretations.biogeographicalProvince", "foreignKeys.biogeographicalProvinceKey");
		gatheringRef.put("gathering.interpretations.finnishMunicipality", "foreignKeys.finnishMunicipalityKey");
		gatheringRef.put("gathering.interpretations.sourceOfCountry", "foreignKeys.sourceOfCountryKey");
		gatheringRef.put("gathering.interpretations.sourceOfBiogeographicalProvince", "foreignKeys.sourceOfBiogeographicalProvinceKey");
		gatheringRef.put("gathering.interpretations.sourceOfFinnishMunicipality", "foreignKeys.sourceOfFinnishMunicipalityKey");
		gatheringRef.put("gathering.interpretations.sourceOfCoordinates", "foreignKeys.sourceOfCoordinatesKey");
		gatheringRef.put("gathering.conversions.birdAssociationArea", "foreignKeys.birdAssociationAreaKey");
		gatheringRef.put("gathering.quality.issue.issue", "foreignKeys.issueKey");
		gatheringRef.put("gathering.quality.issue.source", "foreignKeys.issueSourceKey");
		gatheringRef.put("gathering.quality.timeIssue.issue", "foreignKeys.timeIssueKey");
		gatheringRef.put("gathering.quality.timeIssue.source", "foreignKeys.timeIssueSourceKey");
		gatheringRef.put("gathering.quality.locationIssue.issue", "foreignKeys.locationIssueKey");
		gatheringRef.put("gathering.quality.locationIssue.source", "foreignKeys.locationIssueSourceKey");

		// --------JOINS------------- Names that are defined here should match with aliases defined in VerticaDAOImpleQueries
		gatheringRef.put("document.editorUserIds", "documentUserId.userIdKey"); // gatherging -> document_userid AS documentUserId
		gatheringRef.put("document.linkings.editors", "documentUserIdUserId.personKey"); // gatherging -> document_userid -> userid AS documentUserdIdUserId
		gatheringRef.put("document.keywords", "documentKeyword.keyword"); // gatherging -> document_keyword AS documentKeyword
		gatheringRef.put("document.secureReasons", "documentSecureReason.secureReasonKey"); // gatherging -> document_securereason as secureReason
		gatheringRef.put("document.linkings.collectionQuality", "collection.collectionQualityKey"); //gathering -> collection as collection

		gatheringRef.put("gathering.team", "team.teamText"); // gathering -> team AS team
		gatheringRef.put("gathering.team.memberId", "teamAgent.agentKey"); // gathering -> team -> team_agent AS teamAgent
		gatheringRef.put("gathering.team.memberName", "teamAgentAgent.id"); // gathering-> team -> team_agent -> agent AS teamAgentAgent
		gatheringRef.put("gathering.observerUserIds", "gatheringUserId.userIdKey"); // gathering -> gathering_userid AS gatheringUserId
		gatheringRef.put("gathering.linkings.observers", "gatheringUserIdUserId.personKey"); // gathering -> gathering_userid -> userid AS gatheringUserdIdUserId
		gatheringRef.put("gathering.taxonCensus.taxonId", "taxonCensus.taxonKey"); // gathering -> taxonCensus AS taxonCensus
		gatheringRef.put("gathering.taxonCensus.type", "taxonCensus.type"); // gathering -> taxonCensus AS taxonCensus

		// NamedPlace joins
		for (Method m : ReflectionUtil.getGetters(NamedPlaceEntity.class)) {
			if (ignore(m)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (fieldName.equals("tags")) continue;
			if (fieldName.equals("ykj10km")) {
				gatheringRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				gatheringRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			if (fieldName.equals("wgs84CenterPoint")) {
				gatheringRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				gatheringRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			gatheringRef.put("document.namedPlace."+fieldName, "namedPlace."+fieldName);
		}

		// Fact joins
		for (Method m : ReflectionUtil.getGetters(Fact.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			String mappedName = fieldName;
			if (fieldName.equals("fact")) mappedName = "propertyKey";
			gatheringRef.put("document.facts." + fieldName, "documentFact." + mappedName); // gathering -> document_fact AS documentFact
			gatheringRef.put("gathering.facts." + fieldName, "gatheringFact." + mappedName); // gathering -> gathering_fact AS gatheringFact
		}

		// Media joins
		for (Method m : ReflectionUtil.getGetters(MediaObject.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (!fields.contains("gathering.media."+fieldName)) continue;
			gatheringRef.put("document.media." + fieldName, "documentMedia.mediaObject." + fieldName); // gathering -> document_media AS documentMedia
			gatheringRef.put("gathering.media." + fieldName, "gatheringMedia.mediaObject." + fieldName); // gathering -> gathering_media AS gatheringMedia
		}
		gatheringRef.put("document.media.mediaType", "documentMedia.mediaTypeKey");
		gatheringRef.put("gathering.media.mediaType", "gatheringMedia.mediaTypeKey");

		// Base mappings
		for (String field : fields) {
			if (gatheringRef.containsKey(field)) continue;
			if (field.startsWith("document.")) {
				gatheringRef.put(field, field.replace("document.", "parentDocument."));
				continue;
			}
			if (field.startsWith("gathering.")) {
				gatheringRef.put(field, field);
				continue;
			}
		}

		validateAllFieldsAreMapped(Base.GATHERING, gatheringRef);

		FIELD_TO_GATHERINGENTITY_REFERENCE = Collections.unmodifiableMap(gatheringRef);
	}

	/**
	 * By what name dw-model fields are known "looking from document entity"
	 */
	private final static Map<String, String> FIELD_TO_DOCUMENTENTITY_REFERENCE;
	static {
		Map<String, String> documentRef = new TreeMap<>();

		Set<String> fields = new HashSet<>();
		fields.addAll(Fields.sortable(Base.DOCUMENT).getAllFields());
		fields.addAll(Fields.aggregatable(Base.DOCUMENT).getAllFields());

		documentRef.put("document.documentId", "id");
		documentRef.put("document.randomKey", "key");

		// ------- FOREIGN KEYS -------------

		// Id to Key
		documentRef.put("document.collectionId", "foreignKeys.collectionKey");
		documentRef.put("document.sourceId", "foreignKeys.sourceKey");

		// Enum to Key
		documentRef.put("document.secureLevel", "foreignKeys.secureLevelKey");
		documentRef.put("document.quality.issue.issue", "foreignKeys.documentIssueKey");
		documentRef.put("document.quality.issue.source", "foreignKeys.documentIssueSourceKey");

		// --------JOINS------------- Names that are defined here should match with aliases defined in VerticaDAOImpleQueries
		documentRef.put("document.editorUserIds", "documentUserId.userIdKey"); // document -> document_userid AS documentUserId
		documentRef.put("document.linkings.editors", "documentUserIdUserId.personKey"); // document -> document_userid -> userid AS documentUserdIdUserId
		documentRef.put("document.keywords", "documentKeyword.keyword"); // document -> document_keyword AS documentKeyword
		documentRef.put("document.secureReasons", "documentSecureReason.secureReasonKey"); // document -> document_securereason as secureReason
		documentRef.put("document.linkings.collectionQuality", "collection.collectionQualityKey"); // document -> collection as collection

		// NamedPlace joins
		for (Method m : ReflectionUtil.getGetters(NamedPlaceEntity.class)) {
			if (ignore(m)) continue;
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (fieldName.equals("tags")) continue;
			if (fieldName.equals("ykj10km")) {
				documentRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				documentRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			if (fieldName.equals("wgs84CenterPoint")) {
				documentRef.put("document.namedPlace."+fieldName+".lat", "namedPlace."+fieldName+".lat");
				documentRef.put("document.namedPlace."+fieldName+".lon", "namedPlace."+fieldName+".lon");
				continue;
			}
			documentRef.put("document.namedPlace."+fieldName, "namedPlace."+fieldName);
		}

		// Fact joins
		for (Method m : ReflectionUtil.getGetters(Fact.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			String mappedName = fieldName;
			if (fieldName.equals("fact")) mappedName = "propertyKey";
			documentRef.put("document.facts." + fieldName, "documentFact." + mappedName); // document -> document_fact AS documentFact
		}

		for (Method m : ReflectionUtil.getGetters(MediaObject.class)) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(m);
			if (!fields.contains("document.media." + fieldName)) continue;
			documentRef.put("document.media." + fieldName, "documentMedia.mediaObject." + fieldName); // document -> document_media AS documentMedia
		}
		documentRef.put("document.media.mediaType", "documentMedia.mediaTypeKey");

		// Base joins
		for (String field : fields) {
			if (documentRef.containsKey(field)) continue;
			if (field.startsWith("document.")) {
				documentRef.put(field, field);
				continue;
			}
		}

		validateAllFieldsAreMapped(Base.DOCUMENT, documentRef);

		FIELD_TO_DOCUMENTENTITY_REFERENCE = Collections.unmodifiableMap(documentRef);
	}

	private static void validateAllFieldsAreMapped(Base base, Map<String, String> mapping) {
		Set<String> combinedFields = new HashSet<>();
		combinedFields.addAll(Fields.aggregatable(base).getAllFields());
		combinedFields.addAll(Fields.sortable(base).getAllFields());
		for (String field : combinedFields) {
			if (field.startsWith(Const.RANDOM)) continue;
			if (!mapping.containsKey(field)) throw new IllegalStateException("Unmapped field " + field + " for base " + base);
		}
		for (String mappedField : mapping.keySet()) {
			if (!combinedFields.contains(mappedField)) throw new IllegalStateException("Unnecessary mapping defined for field " + mappedField + " for base " + base);
		}
	}

	private static boolean ignore(Method m) {
		FieldDefinition d = m.getAnnotation(FieldDefinition.class);
		if (d == null) return false;
		return d.ignoreForQuery();
	}

	public String getEntityReference(Base base, String fieldName) {
		Map<String, String> mapping = getMapping(base);
		String reference = mapping.get(fieldName);
		if (reference != null) return reference;
		throw new IllegalStateException("Unmapped field " + fieldName + " for base " + base);
	}

	private Map<String, String> getMapping(Base base) {
		if (base == Base.UNIT) return FIELD_TO_UNITENTITY_REFERENCE;
		if (base == Base.UNIT_MEDIA) return FIELD_TO_UNIT_MEDIA_UNITENTITY_REFERENCE;
		if (base == Base.DOCUMENT) return FIELD_TO_DOCUMENTENTITY_REFERENCE;
		if (base == Base.GATHERING) return FIELD_TO_GATHERINGENTITY_REFERENCE;
		if (base == Base.ANNOTATION) return FIELD_TO_ANNOTATION_UNITENTITY_REFERENCE;
		if (base == Base.SAMPLE) return FIELD_TO_SAMPLE_UNITENTITY_REFERENCE;
		throw new UnsupportedOperationException("Not yet implemented for " + base);
	}

	private static final Map<String, Class<? extends BaseEntity>> DIMENSION_FIELD_TO_CLASS;
	static {
		DIMENSION_FIELD_TO_CLASS = new TreeMap<>();
		DIMENSION_FIELD_TO_CLASS.put("document.secureLevel", SecureLevelEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.secureReasons", SecureReasonEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.sourceId", SourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.collectionId", CollectionEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.media.mediaType", MediaTypeEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.editorUserIds", UserIdEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.facts.fact", PropertyEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.quality.issue.issue", QualityIssueEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.quality.issue.source", QualitySourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("document.linkings.collectionQuality", CollectionQualityEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.biogeographicalProvince", AreaEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.country", AreaEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.finnishMunicipality", AreaEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.sourceOfBiogeographicalProvince", GeoSourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.sourceOfCoordinates", GeoSourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.sourceOfCountry", GeoSourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.interpretations.sourceOfFinnishMunicipality", GeoSourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.conversions.birdAssociationArea", AreaEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.team.memberName", AgentEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.observerUserIds", UserIdEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.media.mediaType", MediaTypeEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.facts.fact", PropertyEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.issue.issue", QualityIssueEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.issue.source", QualitySourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.timeIssue.issue", QualityIssueEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.timeIssue.source", QualitySourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.locationIssue.issue", QualityIssueEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("gathering.quality.locationIssue.source", QualitySourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.lifeStage", LifeStageEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.recordBasis", RecordBasisEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.superRecordBasis", RecordBasisEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.referencePublication", ReferenceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.sex", SexEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.abundanceUnit", AbundanceUnitEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.reportedTaxonConfidence", TaxonConfidenceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.linkings.taxon.administrativeStatuses", AdministrativeStatusEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.linkings.taxon.typesOfOccurrenceInFinland", OccurrenceTypeEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.linkings.taxon.informalTaxonGroups", InformalTaxonGroupEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.linkings.taxon.taxonSets", TaxonSetEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.interpretations.invasiveControlEffectiveness", InvasiveControlEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.media.mediaType", MediaTypeEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.facts.fact", PropertyEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.quality.issue.issue", QualityIssueEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.quality.issue.source", QualitySourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.annotations.annotationBySystem", SourceEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.reportedInformalTaxonGroup", InformalTaxonGroupEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("sample.facts.fact", PropertyEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.interpretations.recordQuality", RecordQualityEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.interpretations.reliability", ReliabilityEntity.class);
		DIMENSION_FIELD_TO_CLASS.put("unit.interpretations.effectiveTags", TagEntity.class);
	}

	/**
	 * Return mapping from field to entity
	 * @param field
	 * @return null if not mapped
	 */
	public Class<? extends BaseEntity> getDimensionClass(String field) {
		return DIMENSION_FIELD_TO_CLASS.get(field);
	}

}
