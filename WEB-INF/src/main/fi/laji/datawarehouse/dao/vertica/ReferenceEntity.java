package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="reference")
class ReferenceEntity extends NameableEntity {

	public ReferenceEntity() {}

	public ReferenceEntity(Qname id) {
		setId(id.toURI());
	}

}
