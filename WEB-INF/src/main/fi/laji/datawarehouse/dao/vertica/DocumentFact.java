package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="document_fact")
class DocumentFact extends FactLink {

	private static final long serialVersionUID = -5358429740394686097L;

	public DocumentFact(Fact fact) {
		super(fact);
	}

	@Deprecated
	public DocumentFact() {}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", referencedColumnName="key", insertable=false, updatable=false)
	public DocumentEntity getDocument() { // for queries only
		return null;
	}

	public void setDocument(@SuppressWarnings("unused") DocumentEntity document) {
		// for queries only
	}

}
