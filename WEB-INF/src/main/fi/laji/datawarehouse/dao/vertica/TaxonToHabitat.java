package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="taxon_habitat")
class TaxonToHabitat extends TaxonToHabitatBaseEntity {

	private static final long serialVersionUID = 4275387425209479773L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="taxon_key", insertable=false, updatable=false)
	public TaxonEntity getTaxon() { // for queries only
		return null;
	}

	public void setTaxon(@SuppressWarnings("unused") TaxonEntity taxon) {
		// for queries only
	}

}
