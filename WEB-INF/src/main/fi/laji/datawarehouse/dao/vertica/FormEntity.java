package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="form")
class FormEntity extends NameableEntity {

	public FormEntity() {}

	public FormEntity(String uri) {
		setId(uri);
	}

}
