package fi.laji.datawarehouse.dao.vertica;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.dao.vertica.VerticaDAOImpleDimensions.EntitiesKeyCache.EntityKeyCache;
import fi.laji.datawarehouse.etl.models.Converter;
import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.Reliability;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.OccurrenceType;
import fi.luomus.commons.containers.TaxonSet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.DateUtils;

class VerticaDAOImpleDimensions implements VerticaDimensionsDAO {

	public static class EntitiesKeyCache {

		private static final Object LOCK = new Object();
		private final Map<Class<? extends BaseEntity>, EntityKeyCache> classToCacheMap = new HashMap<>();

		public EntityKeyCache getCacheFor(Class<? extends BaseEntity> entityClass) {
			if (!classToCacheMap.containsKey(entityClass)) {
				synchronized (LOCK) {
					if (!classToCacheMap.containsKey(entityClass)) {
						boolean caching = !isEnumerationEntity(entityClass); // No caching needed for enum values, there aren't many of them and all can be kept in memory
						classToCacheMap.put(entityClass, new EntityKeyCache(entityClass, caching));
					}
				}
			}
			return classToCacheMap.get(entityClass);
		}

		public EntityKeyCache getCacheFor(BaseEntity entity) {
			Class<? extends BaseEntity> entityClass = entity.getClass();
			return getCacheFor(entityClass);
		}

		public static class EntityKeyCache {

			private final Class<? extends BaseEntity> cachingKeysOfClass;
			private final Cache<String, Optional<Long>> idToKeyCache;
			private final Cache<Long, String> keyToIdCache;

			public EntityKeyCache(Class<? extends BaseEntity> cachingKeysOfClass, boolean caching) {
				this.cachingKeysOfClass = cachingKeysOfClass;
				if (caching) {
					idToKeyCache = Caffeine.newBuilder().expireAfterAccess(12, TimeUnit.HOURS).maximumSize(100000).build();
					keyToIdCache = Caffeine.newBuilder().expireAfterAccess(12, TimeUnit.HOURS).maximumSize(100000).build();
				} else {
					idToKeyCache = Caffeine.newBuilder().build(); // This cache never evicts
					keyToIdCache = Caffeine.newBuilder().build();
				}
			}

			public Optional<Long> getKey(String id) {
				return idToKeyCache.getIfPresent(id);
			}

			public void setKey(String id, Long key) {
				if (id == null) throw new ETLException(cachingKeysOfClass.getName() + " sets null id; id=" + id + " key=" + key);
				idToKeyCache.put(id, Optional.fromNullable(key));
				if (key != null) {
					keyToIdCache.put(key, id);
				}
			}

			public String getId(Long key) {
				return keyToIdCache.getIfPresent(key);
			}

		}

	}

	private static final int DATE_INITIALIZATION_RANGE_START = 1700;
	private static final int DATE_INITIALIZATION_RANGE_END = 2200;
	private static final Map<Class<? extends Enum<?>>, Class<? extends BaseEntity>> ENUM_TO_ENTITY_MAP = initEnumToEntityMap();
	private final VerticaDAOImpleSharedMappings mappings = new VerticaDAOImpleSharedMappings();

	private static Map<Class<? extends Enum<?>>, Class<? extends BaseEntity>> initEnumToEntityMap() {
		Map<Class<? extends Enum<?>>, Class<? extends BaseEntity>> enumToEntityMap = new HashMap<>();
		enumToEntityMap.put(SecureLevel.class, SecureLevelEntity.class);
		enumToEntityMap.put(SecureReason.class, SecureReasonEntity.class);
		enumToEntityMap.put(RecordBasis.class, RecordBasisEntity.class);
		enumToEntityMap.put(Sex.class, SexEntity.class);
		enumToEntityMap.put(AbundanceUnit.class, AbundanceUnitEntity.class);
		enumToEntityMap.put(LifeStage.class, LifeStageEntity.class);
		enumToEntityMap.put(GeoSource.class, GeoSourceEntity.class);
		enumToEntityMap.put(MediaType.class, MediaTypeEntity.class);
		enumToEntityMap.put(InvasiveControl.class, InvasiveControlEntity.class);
		enumToEntityMap.put(TaxonConfidence.class, TaxonConfidenceEntity.class);
		enumToEntityMap.put(Quality.Issue.class, QualityIssueEntity.class);
		enumToEntityMap.put(Quality.Source.class, QualitySourceEntity.class);
		enumToEntityMap.put(RecordQuality.class, RecordQualityEntity.class);
		enumToEntityMap.put(Reliability.class, ReliabilityEntity.class);
		enumToEntityMap.put(CollectionQuality.class, CollectionQualityEntity.class);
		enumToEntityMap.put(Tag.class, TagEntity.class);
		return enumToEntityMap;
	}

	public static boolean isEnumerationEntity(Class<? extends BaseEntity> entityClass) {
		for (Class<? extends BaseEntity> enumEntityClass : ENUM_TO_ENTITY_MAP.values()) {
			if (enumEntityClass.equals(entityClass)) return true;
		}
		return false;
	}

	private final SessionFactory sessionFactory;
	private final DAO dao;
	private final Set<Long> existingDates = new HashSet<>();
	private final EntitiesKeyCache entitiesKeyCache = new EntitiesKeyCache();
	private final String dimensionsSchema;
	private final ThreadStatuses threadStatuses;
	private static final Object ENTITY_LOCK = new Object();
	private static final Object DATE_LOCK = new Object();

	public VerticaDAOImpleDimensions(SessionFactory sessionFactory, String dimensionsSchema, DAO dao, ThreadStatuses threadStatuses) {
		this.sessionFactory = sessionFactory;
		this.dimensionsSchema = dimensionsSchema;
		this.dao = dao;
		this.threadStatuses = threadStatuses;
		initEnumIdToKeyMapAndFetchExistingDates();
	}

	public DAO getDao() {
		return dao;
	}


	private void initEnumIdToKeyMapAndFetchExistingDates() {
		StatelessSession session = null;
		try {
			session = getSession();
			session.getTransaction().begin();
			initEnumIdToKeyMapAndFetchExistingDatesUsing(session);
			session.getTransaction().commit();
		} catch (Exception e) {
			if (session != null) {
				try { session.getTransaction().rollback(); } catch (Exception e2) {}
			}
			throw new ETLException(e);
		} finally {
			close(session);
		}
	}

	private void initEnumIdToKeyMapAndFetchExistingDatesUsing(StatelessSession session) {
		for (Map.Entry<Class<? extends Enum<?>>, Class<? extends BaseEntity>> e : ENUM_TO_ENTITY_MAP.entrySet()) {
			initEnumIdToKeyMap(e.getKey(), e.getValue(), session);
		}
		interimCommit(session);
		fetchExistingDatesAndCreateIfDoNotExist(session);
	}

	private void interimCommit(StatelessSession session) {
		session.getTransaction().commit();
		session.getTransaction().begin();
	}

	private void fetchExistingDatesAndCreateIfDoNotExist(StatelessSession session) {
		fetchExistingDates(session);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), existingDates.size() + " dates exist");
		if (datesNotInitialized(existingDates.size())) {
			initializeDatesToDw(session);
		}
	}

	private boolean datesNotInitialized(int count) {
		return count < (365*(DATE_INITIALIZATION_RANGE_END-DATE_INITIALIZATION_RANGE_START)) - 300;
	}

	private void initializeDatesToDw(StatelessSession session) {
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Adding dates between " + DATE_INITIALIZATION_RANGE_START + "-" + DATE_INITIALIZATION_RANGE_END);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(DateUtils.convertToDate("01.01."+DATE_INITIALIZATION_RANGE_START));
		} catch (ParseException e) {
			throw new ETLException(e);
		}
		int previousYear = -1;
		int year = -1;
		while ((year = cal.get(Calendar.YEAR)) <= DATE_INITIALIZATION_RANGE_END) {
			if (year != previousYear) {
				interimCommit(session);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Adding ... " + year);
				previousYear = year;
			}
			initializeDateToDw(session, cal.getTime());
			cal.add(Calendar.DATE, 1);
		}
	}

	private void initializeDateToDw(StatelessSession session, Date date) {
		Long key = formulateDateKey(date);
		if (!existingDates.contains(key)) {
			initializeDateToDw(key, date, session);
			existingDates.add(key);
		}
	}

	private Long formulateDateKey(Date date) {
		return Long.valueOf(DateUtils.format(date, "yyyyMMdd"));
	}

	private void initEnumIdToKeyMap(Class<? extends Enum<?>> enumeration, Class<? extends BaseEntity> entityClass, StatelessSession session) {
		@SuppressWarnings("unchecked")
		List<BaseEntity> existing = session.createCriteria(entityClass).list();
		Map<String, Long> existingKeys = existing.stream().collect(Collectors.toMap(BaseEntity::getId, BaseEntity::getKey));
		for (Enum<?> enumerationValue : enumeration.getEnumConstants()) {
			Long key = existingKeys.get(enumerationValue.name());
			if (key == null) {
				BaseEntity concreteEntity = getEntityForEnum(enumerationValue);
				concreteEntity.setId(enumerationValue.name());
				key = insert(concreteEntity, session);
			}
			entitiesKeyCache.getCacheFor(entityClass).setKey(enumerationValue.name(), key);
		}
	}

	private BaseEntity getEntityForEnum(Enum<?> enumerationValue) {
		try {
			return ENUM_TO_ENTITY_MAP.get(enumerationValue.getDeclaringClass()).getConstructor().newInstance();
		} catch (Exception e) {
			throw new IllegalStateException(enumerationValue.toString(), e);
		}
	}

	private void fetchExistingDates(StatelessSession session) {
		List<?> results = session.createCriteria(DateEntity.class)
				.setProjection(Projections.property("key"))
				.list();
		for (Object o : results) {
			existingDates.add((Long) o);
		}
	}

	@Override
	public Long getEnumKey(Enum<?> enumeration) {
		if (enumeration == null) return null;
		BaseEntity entity = getEntityForEnum(enumeration);
		Optional<Long> optional = entitiesKeyCache.getCacheFor(entity).getKey(enumeration.name());
		if (optional == null || !optional.isPresent()) throw new IllegalStateException("Enumeration " + enumeration.getDeclaringClass() + " has not been initialized properly. Missing value: " + enumeration);
		return optional.get();
	}

	public <E extends BaseEntity> Long getKeyDoNoCreate(E entity) {
		return getEntityKey(entity, false);
	}

	public Long getKey(Object object) {
		if (object == null) throw new IllegalArgumentException("Null given");

		if (object.getClass().isEnum()) return getEnumKey((Enum<?>) object);
		if (object instanceof BaseEntity) return getEntityKey((BaseEntity) object, true);
		if (object instanceof Date) return getDateKey((Date) object);

		throw new UnsupportedOperationException("Can not get key for " + object.getClass());
	}

	private Long getDateKey(Date date) {
		Long key = formulateDateKey(date);
		if (existingDates.contains(key)) return key;

		synchronized (DATE_LOCK) {
			if (existingDates.contains(key)) return key;
			try (StatelessSession session = getSession()) {
				session.getTransaction().begin();
				addDateToDwIfDoesNotExist(key, date, session);
				existingDates.add(key);
				session.getTransaction().commit();
				return key;
			}
		}
	}

	private void addDateToDwIfDoesNotExist(Long key, Date date, StatelessSession session) {
		List<?> results = session.createCriteria(DateEntity.class).add(Restrictions.eq("key", key)).list();
		if (results.size() > 1) throw new IllegalStateException("Multiple result rows for " + DateEntity.class + " with value " + date);
		if (results.size() == 1) {
			return;
		}
		initializeDateToDw(key, date, session);
	}

	private void initializeDateToDw(Long key, Date date, StatelessSession session) {
		DateValue dateValue = DateUtils.convertToDateValue(date);
		DateEntity entity = new DateEntity();
		entity.setKey(key);
		entity.setCentury(Converter.century(dateValue));
		entity.setDecade(Converter.decade(dateValue));
		entity.setYear(dateValue.getYearAsInt());
		entity.setMonth(dateValue.getMonthAsInt());
		entity.setDay(dateValue.getDayAsInt());
		entity.setWeekday(DateUtils.getDayOfWeek(date));
		session.insert(entity);
	}

	private Long getEntityKey(BaseEntity entity, boolean create) {
		if (entity.getId() == null || entity.getId().isEmpty()) throw new IllegalArgumentException("Null id for " + entity.getClass());

		EntityKeyCache keyCache = entitiesKeyCache.getCacheFor(entity);

		Optional<Long> optional = keyCache.getKey(entity.getId());
		if (optional != null) {
			if (optional.isPresent()) return optional.get();
			if (!create) return null; // cached absent
		}

		synchronized (ENTITY_LOCK) {
			optional = keyCache.getKey(entity.getId());
			if (optional != null) {
				if (optional.isPresent()) return optional.get();
				if (!create) return null; // cached absent
			}

			try (StatelessSession session = getSession()) {
				session.getTransaction().begin();
				Long dwKey = getKeyFromDw(entity, session);
				if (dwKey == null) {
					if (create) {
						dwKey = insert(entity, session);
					}
				}
				keyCache.setKey(entity.getId(), dwKey);
				session.getTransaction().commit();
				return dwKey;
			}
		}
	}

	private Long getKeyFromDw(BaseEntity entity, StatelessSession session) {
		Long key = (Long) session.createCriteria(entity.getClass())
				.add(Restrictions.eq("id", entity.getId()))
				.setProjection(Projections.property("key"))
				.uniqueResult();
		return key;
	}

	private Long insert(BaseEntity entity, StatelessSession session) {
		if (entity instanceof UserIdEntity) {
			UserIdEntity userId = (UserIdEntity) entity;
			String lookupName = userId.getId();
			Qname personId = dao.getPersonLookupStructure().get(lookupName);
			if (personId == null) {
				Qname newPersonId = null;
				if (lookupName.startsWith("http://tun.fi/MA.")) {
					newPersonId = Qname.fromURI(lookupName);
				} else if (lookupName.startsWith("MA.")) {
					newPersonId = new Qname(lookupName);

				}
				if (newPersonId != null) {
					PersonInfo newPerson = dao.getPerson(newPersonId);
					if (newPerson != null && newPerson.getEmail() != null) {
						personId = newPersonId;
					}
				}

			}
			userId.setPersonKey(PersonBaseEntity.parsePersonKey(personId));
		}

		Long key = (Long) session.insert(entity);

		if (entity instanceof TeamEntity) {
			TeamEntity team = (TeamEntity) entity;
			int sequence = 0;
			for (Long agentKey : team.revealAgentKeys()) {
				TeamAgent teamAgent = new TeamAgent();
				teamAgent.setAgentKey(agentKey);
				teamAgent.setTeamKey(key);
				teamAgent.setSequence(sequence++);
				session.insert(teamAgent);
			}
		}

		return key;
	}

	public StatelessSession getSession() {
		return sessionFactory.openStatelessSession();
	}

	private String getEntityId(Class<? extends BaseEntity> entityClass, Long key) {
		if (key == null) return null;

		EntityKeyCache keyCache = entitiesKeyCache.getCacheFor(entityClass);

		String id = keyCache.getId(key);
		if (id != null) return id;

		synchronized (ENTITY_LOCK) {
			id = keyCache.getId(key);
			if (id != null) return id;
			id = getIdFromDatabase(entityClass, key);
			if (id == null) return null;
			keyCache.setKey(id, key);
			return id;
		}
	}

	private String getIdFromDatabase(Class<?> entityClass, Long key) {
		try (StatelessSession session = getSession()) {
			String id = (String) session.createCriteria(entityClass)
					.add(Restrictions.eq("key", key))
					.setProjection(Projections.property("id"))
					.uniqueResult();
			return id;
		}
	}

	public List<TeamAgent> getTeamAgents(Long teamKey) {
		try (StatelessSession session = getSession()) {
			@SuppressWarnings("unchecked")
			List<TeamAgent> list = session.createCriteria(TeamAgent.class)
			.add(Restrictions.eq("teamKey", teamKey))
			.addOrder(Order.asc("sequence"))
			.list();
			return list;
		}
	}

	public TaxonEntity getTaxonEntity(Long taxonKey) {  // This has it's own method because TaxonEntity could not extend BaseEntity
		try (StatelessSession session = getSession()) {
			TaxonEntity entity = (TaxonEntity) session.createCriteria(TaxonEntity.class)
					.add(Restrictions.eq("key", taxonKey))
					.uniqueResult();
			return entity;
		}
	}

	public PersonEntity getPersonEntity(Long personKey) {  // This has it's own method because PersonEntity could not extend BaseEntity
		try (StatelessSession session = getSession()) {
			PersonEntity entity = (PersonEntity) session.createCriteria(PersonEntity.class)
					.add(Restrictions.eq("key", personKey))
					.uniqueResult();
			return entity;
		}
	}

	@SuppressWarnings("unchecked")
	public <E extends BaseEntity> E getEntity(Long key, Class<E> entityClass) {
		try (StatelessSession session = getSession()) {
			E entity = (E) session.createCriteria(entityClass)
					.add(Restrictions.eq("key", key))
					.uniqueResult();
			return entity;
		}
	}

	@SuppressWarnings("unchecked")
	public <E extends BaseEntity> E getEntity(String id, Class<E> entityClass) {
		try (StatelessSession session = getSession()) {
			E entity = (E) session.createCriteria(entityClass)
					.add(Restrictions.eq("id", id))
					.uniqueResult();
			return entity;
		}
	}

	@Override
	public void startTaxonAndPersonReprosessing() {
		synchronized (ENTITY_LOCK) {
			dao.clearCachesStartReload();
			try {
				new TaxonReload(dao, threadStatuses).reload();
				new TargetReprocessor(this).reprocess();
			} catch (Exception e) {
				// exceptions logged in implementations
			}
			try {
				new PersonReload(dao).reload();
				new UserIdReprocessor(this).reprocess();
			} catch (Exception e) {
				// exceptions logged in implementations
			}
		}
	}

	@Override
	public void updateNameableEntityNames()   {
		synchronized (ENTITY_LOCK) {
			new NameableEntityNameLoader(this, getSourceNames(), SourceEntity.class).update();
			new NameableEntityNameLoader(this, getAreaDescriptions(), AreaEntity.class).update();
			new NameableEntityNameLoader(this, dao.getReferenceDescriptions(), ReferenceEntity.class).update();
			new NameableEntityNameLoader(this, getInformalGroupNames(), InformalTaxonGroupEntity.class).update();
			new NameableEntityNameLoader(this, getTaxonSetNames(), TaxonSetEntity.class).update();
			new NameableEntityNameLoader(this, getAdminStatusNames(), AdministrativeStatusEntity.class).update();
			new NameableEntityNameLoader(this, getOccurrenceTypeNames(), OccurrenceTypeEntity.class).update();
			new NameableEntityNameLoader(this, getFormNames(), FormEntity.class).update();
		}
	}

	private Map<String, String> getAreaDescriptions() {
		return Util.getAreaDescriptions(dao.getAreas().values(), true);
	}

	private Map<String, String> getAdminStatusNames() {
		try {
			Map<String, String> adminStatusNames = new HashMap<>();
			for (Map.Entry<String, AdministrativeStatus> e : dao.getTaxonomyDAO().getAdministrativeStatuses().entrySet()) {
				adminStatusNames.put(new Qname(e.getKey()).toURI(), e.getValue().getName("fi"));
			}
			return adminStatusNames;
		} catch (Exception e) {
			throw new ETLException("Admin status names", e);
		}
	}

	private Map<String, String> getOccurrenceTypeNames() {
		try {
			Map<String, String> occurrenceTypeNames = new HashMap<>();
			for (Map.Entry<String, OccurrenceType> e : dao.getTaxonomyDAO().getOccurrenceTypes().entrySet()) {
				occurrenceTypeNames.put(new Qname(e.getKey()).toURI(), e.getValue().getName("fi"));
			}
			return occurrenceTypeNames;
		} catch (Exception e) {
			throw new ETLException("Occurrence type names", e);
		}
	}

	private Map<String, String> getInformalGroupNames() {
		try {
			Map<String, String> informalGroupNames = new HashMap<>();
			for (Map.Entry<String, InformalTaxonGroup> e : dao.getTaxonomyDAO().getInformalTaxonGroups().entrySet()) {
				informalGroupNames.put(new Qname(e.getKey()).toURI(), e.getValue().getName("fi"));
			}
			return informalGroupNames;
		} catch (Exception e) {
			throw new ETLException("Informal group names", e);
		}
	}

	private Map<String, String> getTaxonSetNames() {
		try {
			Map<String, String> taxonSetNames = new HashMap<>();
			for (Map.Entry<String, TaxonSet> e : dao.getTaxonomyDAO().getTaxonSets().entrySet()) {
				taxonSetNames.put(new Qname(e.getKey()).toURI(), e.getValue().getName("fi"));
			}
			return taxonSetNames;
		} catch (Exception e) {
			throw new ETLException("Taxon set names", e);
		}
	}

	private Map<String, String> getFormNames() {
		return dao.getFormNames();
	}

	private Map<String, String> getSourceNames() {
		Map<String, String> sourcenames = new HashMap<>();
		for (Map.Entry<String, Source> e : dao.getSources().entrySet()) {
			sourcenames.put(e.getKey(), e.getValue().getName());
		}
		return sourcenames;
	}

	private final Cached<Long, List<Long>> userIdCache = new Cached<>(new Cached.CacheLoader<Long, List<Long>>() {

		@Override
		public List<Long> load(Long personKey) {
			if (personKey == null) return Collections.emptyList();
			try (StatelessSession session = getSession()) {
				@SuppressWarnings("unchecked")
				List<UserIdEntity> userIds = session.createCriteria(UserIdEntity.class)
				.add(Restrictions.eq("personKey", personKey)).list();
				return userIds.stream().map(uid->uid.getKey()).collect(Collectors.toList());
			}
		}

	}, 1, TimeUnit.DAYS, 10000);

	public List<Long> getUserIdKeys(List<Long> personKeys) {
		if (personKeys == null || personKeys.isEmpty()) return Collections.emptyList();
		List<Long> userIdKeys = new ArrayList<>();
		for (Long personKey : personKeys) {
			userIdKeys.addAll(userIdCache.get(personKey));
		}
		return userIdKeys;
	}

	public List<UserIdEntity> getUserIds() {
		try (StatelessSession session = getSession()) {
			@SuppressWarnings("unchecked")
			List<UserIdEntity> userIds = session.createCriteria(UserIdEntity.class).addOrder(Order.asc("id")).list();
			return userIds;
		}
	}

	public List<TargetEntity> getTargets() {
		try (StatelessSession session = getSession()) {
			@SuppressWarnings("unchecked")
			List<TargetEntity> targets = session.createCriteria(TargetEntity.class).addOrder(Order.asc("id")).list();
			return targets;
		}
	}

	public List<TargetChecklistTaxaEntity> getTargetChecklistTaxa(Qname checklist) {
		try (StatelessSession session = getSession()) {
			@SuppressWarnings("unchecked")
			List<TargetChecklistTaxaEntity> checklistLinkings = session.createCriteria(TargetChecklistTaxaEntity.class).add(Restrictions.eq("checklist", checklist.toURI())).addOrder(Order.asc("id")).list();
			return checklistLinkings;
		}
	}

	@Override
	public void updateEntities(List<BaseEntity> entityList) {
		if (entityList.isEmpty()) return;
		if (entityList.size() > 2000) {
			for (List<BaseEntity> part : Lists.partition(entityList, 1000)) {
				updateEntities(part);
			}
			return;
		}
		StatelessSession session = null;
		try {
			session = getSession();
			session.getTransaction().begin();

			for (BaseEntity e : entityList) {
				session.update(e);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			if (session != null) {
				try { session.getTransaction().rollback(); } catch (Exception e2) {}
			}
			throw e;
		} finally {
			close(session);
		}
	}

	@Override
	public void insertEntities(List<BaseEntity> entityList) {
		if (entityList.isEmpty()) return;
		if (entityList.size() > 2000) {
			for (List<BaseEntity> part : Lists.partition(entityList, 1000)) {
				insertEntities(part);
			}
			return;
		}
		StatelessSession session = null;
		try {
			session = getSession();
			session.getTransaction().begin();

			for (BaseEntity e : entityList) {
				session.insert(e);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			if (session != null) {
				try { session.getTransaction().rollback(); } catch (Exception e2) {}
			}
			throw e;
		} finally {
			close(session);
		}
	}

	private void close(StatelessSession session) {
		if (session != null) {
			try {
				session.close();
			} catch (Exception e) {}
		}
	}

	@Override
	public List<NameableEntity> getEntities(Class<? extends NameableEntity> nameableEntityClass) {
		try (StatelessSession session = getSession()) {
			@SuppressWarnings("unchecked")
			List<NameableEntity> entities = session.createCriteria(nameableEntityClass).addOrder(Order.asc("id")).list();
			return entities;
		}
	}

	@Override
	public String getIdForKey(Long key, String field) {
		Class<? extends BaseEntity> fieldClass = mappings.getDimensionClass(field);
		if (fieldClass !=  null) {
			return getEntityId(fieldClass, key);
		}

		KeyToStringifier stringifier = STRINGIFIERS.get(field);
		if (stringifier != null) {
			return stringifier.toString(key);
		}

		if (field.startsWith("unit.linkings.") && field.endsWith(".id")) {
			return TAXON_STRINGIFIER.toString(key);
		}
		if (field.startsWith("unit.linkings.") && field.endsWith("Id")) {
			return TAXON_STRINGIFIER.toString(key);
		}
		if (field.startsWith("gathering.eventDate.")) {
			return DATE_STRINGIFIER.toString(key);
		}

		return key.toString();
	}

	private interface KeyToStringifier {
		String toString(Long key);
	}

	private static final KeyToStringifier DATE_STRINGIFIER = new KeyToStringifier() {
		@Override
		public String toString(Long key) {
			try {
				return DateUtils.format(DateUtils.convertToDate(key.toString(), "yyyyMMdd"), "yyyy-MM-dd");
			} catch (Exception e) {
				throw new IllegalArgumentException(e);
			}
		}
	};

	private static final KeyToStringifier TAXON_STRINGIFIER = new KeyToStringifier() {
		@Override
		public String toString(Long key) {
			return new Qname("MX."+key).toURI();
		}
	};

	private static final KeyToStringifier PERSON_STRINGIFIER = new KeyToStringifier() {
		@Override
		public String toString(Long key) {
			return new Qname("MA."+key).toURI();
		}
	};

	private static final KeyToStringifier LONG_STRINGIFIER = new KeyToStringifier() {
		@Override
		public String toString(Long key) {
			return key.toString();
		}
	};

	private static final Map<String, KeyToStringifier> STRINGIFIERS;
	static {
		STRINGIFIERS = new HashMap<>();
		STRINGIFIERS.put("unit.interpretations.annotatedTaxonId", TAXON_STRINGIFIER);
		STRINGIFIERS.put("gathering.taxonCensus.taxonId", TAXON_STRINGIFIER);
		STRINGIFIERS.put("document.linkings.editors", PERSON_STRINGIFIER);
		STRINGIFIERS.put("gathering.linkings.observers", PERSON_STRINGIFIER);
		STRINGIFIERS.put("unit.annotations.annotationByPerson", PERSON_STRINGIFIER);
		STRINGIFIERS.put("unit.annotations.created", LONG_STRINGIFIER);
		STRINGIFIERS.put("gathering.conversions.boundingBoxAreaInSquareMeters", LONG_STRINGIFIER);
	}

	@Override
	public void performCleanUp() {
		synchronized (ENTITY_LOCK) {
			try (StatelessSession session = getSession()) {
				List<?> tables = session
						.createSQLQuery(" SELECT table_schema || '.' || table_name FROM tables WHERE table_schema = '"+dimensionsSchema+"' ")
						.list();
				for (Object table : tables) {
					try {
						performCleanUp(session, table.toString());
					} catch (Exception e) {
						dao.getErrorReporter().report(table.toString(), e);
					}
				}
			}
		}
	}

	private void performCleanUp(StatelessSession session, String tableName) {
		session.createSQLQuery(" SELECT PURGE_TABLE('"+tableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT DO_TM_TASK('moveout', '"+tableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT DO_TM_TASK('mergeout', '"+tableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT ANALYZE_STATISTICS('"+tableName+"') ").uniqueResult();
	}

	@Override
	public VerticaDimensionsPersonService getPersonSevice() {
		return new VerticaDimensionsPersonServiceImple(this);
	}

	@Override
	public VerticaDimensionsTaxonService getTaxonService() {
		return new VerticaDimensionsTaxonServiceImple(this, threadStatuses);
	}

	public String getDimensionsSchema() {
		return dimensionsSchema;
	}

	@Override
	public void storeDocumentIds(Iterator<String> iterator) {
		try (StatelessSession session = getSession()) {
			session.beginTransaction();
			truncateTable(session, "ALL_DOCUMENTIDS");
			while (iterator.hasNext()) {
				session.insert(new DocumentIdEntity(iterator.next()));
			}
			session.getTransaction().commit();
		}
	}

	@Override
	public void storeSplittedDocumentIds(Collection<SplittedDocumentIdEntity> values) {
		try (StatelessSession session = getSession()) {
			session.beginTransaction();
			truncateTable(session, "SPLITTED_DOCUMENTID");
			for (SplittedDocumentIdEntity e : values) {
				session.insert(e);
			}
			session.getTransaction().commit();
		}
	}

	private void truncateTable(StatelessSession session, String realDbTableName) {
		if (session == null) throw new ETLException("Null session given");
		String sql = "TRUNCATE TABLE " + dimensionsSchema + "." + realDbTableName;
		try {
			session.createSQLQuery(sql).executeUpdate();
		} catch (Exception e) {
			throw new ETLException(sql, e);
		}
	}

}
