package fi.laji.datawarehouse.dao.vertica;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.luomus.commons.utils.ReflectionUtil;

class AggregatedResultTransformer implements ResultTransformer {

	private static final long serialVersionUID = -4776842968376348829L;

	private final AggregateBy aggregateBy;

	public AggregatedResultTransformer(AggregateBy aggregateBy) {
		this.aggregateBy = aggregateBy;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List transformList(List allResults) {
		return allResults;
	}

	@Override
	public Object transformTuple(Object[] values, String[] fields) {
		try {
			return tryToTransform(values, fields);
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	private Object tryToTransform(Object[] values, String[] fields) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (values.length != fields.length) throw new IllegalStateException("Values length: " + values.length + " fields length: " + fields.length);
		AggregateRow row = new AggregateRow();
		int i = 0;
		for (String field : fields) {
			boolean aggregateField = aggregateBy.getFields().size() > i;
			Object value = values[i++];
			transformField(field, value, row, aggregateField);
		}
		return row;
	}

	private void transformField(String field, Object value, AggregateRow row, boolean aggregateField) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (aggregateField) {
			row.addAggregateValue(value);
		} else {
			if (value != null) {
				Method setter = ReflectionUtil.getSetter(field, AggregateRow.class);
				setter.invoke(row, value);
			}
		}
	}

}
