package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gathering_geometry")
class GatheringGeometry {

	private Long gatheringKey;

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return gatheringKey;
	}

	public void setGatheringKey(Long gatheringKey) {
		this.gatheringKey = gatheringKey;
	}

	private String grid;

	@Column(name="grid")
	public String getGrid() {
		return grid;
	}

	public void setGrid(String grid) {
		this.grid = grid;
	}

}
