package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.Fact;

@Entity
@Table(name="gathering_document_fact")
class GatheringDocumentFact extends Fact implements Serializable {

	private static final long serialVersionUID = 1677211852907829639L;

	@SuppressWarnings("deprecation")
	public GatheringDocumentFact() {}

	@Id @Column(name="gathering_key")
	public Long getGatheringKey() {
		return null;
	}

	public void setGatheringKey(@SuppressWarnings("unused") Long gatheringKey) {

	}

	@Id @Column(name="property_key")
	public Long getPropertyKey() {
		return null;
	}

	public void setPropertyKey(@SuppressWarnings("unused") Long propertyKey) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gathering_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity document) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
