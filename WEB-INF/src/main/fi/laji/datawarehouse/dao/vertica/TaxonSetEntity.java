package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

@Entity
@Table(name="taxonset")
class TaxonSetEntity extends NameableEntity {

	public TaxonSetEntity() {}

	public TaxonSetEntity(String setId) {
		setId(setId);
	}

}
