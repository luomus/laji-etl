package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;

public class TargetReprocessor {

	private final VerticaDAOImpleDimensions dimensions;

	public TargetReprocessor(VerticaDAOImpleDimensions dimensions) {
		this.dimensions = dimensions;
	}

	public void reprocess() {
		StatelessSession session = null;
		try {
			session = dimensions.getSession();
			tryToReprocess(session);
		} catch (Exception e) {
			dimensions.getDao().logError(Const.LAJI_ETL_QNAME, this.getClass(), null, e);
			dimensions.getDao().getErrorReporter().report("Target reprocesing failed", e);
			throw new ETLException("Target reprocessing failed", e);
		} finally {
			if (session != null) try { session.close(); } catch (Exception e) {}
		}

		dimensions.getDao().logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reprocessing contents of Target table completed!");
	}

	private void tryToReprocess(StatelessSession session) {
		DAO dao = dimensions.getDao();

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to reprocess contents of Target table ...");
		List<TargetEntity> targets = dimensions.getTargets();

		int affected = reprocessTargetMasterChecklistLinkings(session, targets, dao);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reprocessing contents of Target table completed (" +affected+" rows affected)");

		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reprocessing contents of Target Checklist Taxa Table for MR.84 ...");
		affected = reprocessChecklistLinkings(new Qname("MR.84"), session, targets, dao);
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reprocessing contents of Target Checklist Taxa Table for MR.84 completed (" +affected+" rows affected)");
	}

	private int reprocessTargetMasterChecklistLinkings(StatelessSession session, List<TargetEntity> targets, DAO dao) {
		session.getTransaction().begin();
		int total = 0;
		int i = 0;
		for (TargetEntity current : targets) {
			TargetEntity resolved = dao.getTaxonLinkingService().getTargetEntity(current);
			if (!resolved.resolvedSameAs(current)) {
				session.update(resolved);
				i = maybeIntermCommit(session, i);
				total++;
			}
		}
		session.getTransaction().commit();
		return total;
	}

	private int reprocessChecklistLinkings(Qname checklist, StatelessSession session, List<TargetEntity> targets, DAO dao) {
		session.getTransaction().begin();
		int total = 0;
		int i = 0;
		List<TargetChecklistTaxaEntity> checklistLinkings = dimensions.getTargetChecklistTaxa(checklist);
		Map<Long, TargetChecklistTaxaEntity> currentTargetChecklistLinkings = checklistLinkings.stream().collect(Collectors.toMap(TargetChecklistTaxaEntity::getTargetKey, Function.identity()));
		for (TargetEntity target : targets) {
			TargetChecklistTaxaEntity resolved = dao.getTaxonLinkingService().getTargetChecklistEntity(checklist, target);
			TargetChecklistTaxaEntity current = currentTargetChecklistLinkings.get(target.getKey());
			if (current == null) {
				if (resolved.getTaxonKey() != null) {
					session.insert(resolved);
					i = maybeIntermCommit(session, i);
					total++;
				}
				continue;
			}
			if (!resolved.resolvedSameAs(current)) {
				session.update(resolved);
				i = maybeIntermCommit(session, i);
				total++;
			}
		}
		session.getTransaction().commit();
		return total;
	}

	private int maybeIntermCommit(StatelessSession session, int i) {
		if (i++ >= 20) {
			session.getTransaction().commit();
			session.getTransaction().begin();
			i = 0;
		}
		return i;
	}

}
