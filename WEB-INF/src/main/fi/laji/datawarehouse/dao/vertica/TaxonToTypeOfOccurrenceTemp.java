package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="taxon_occurrencetype_temp")
class TaxonToTypeOfOccurrenceTemp extends TaxonToTypeOfOccurrenceBaseEntity {

	private static final long serialVersionUID = 1432125768448556184L;

}
