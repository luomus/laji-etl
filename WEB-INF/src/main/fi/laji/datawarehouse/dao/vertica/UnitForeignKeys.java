package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
class UnitForeignKeys {

	private String gatheringId;
	private Long gatheringKey;
	private Long targetKey;
	private Long originalTargetKey;
	private Long reportedInformalTaxonGroupKey;
	private Long referenceKey;
	private Long recordBasisKey;
	private Long superRecordBasisKey;
	private Long abundanceUnitKey;
	private Long sexKey;
	private Long lifeStageKey;
	private Long invasiveControlEffectivenessKey;
	private Long taxonConfidenceKey;
	private Long recordQualityKey;
	private Long reliabilityKey;
	private Long issueKey;
	private Long issueSourceKey;
	private Long annotatedTaxonKey;

	public UnitForeignKeys() {}

	@Column(name="gathering_id")
	public String getGatheringId() {
		return gatheringId;
	}

	public void setGatheringId(String gatheringId) {
		this.gatheringId = gatheringId;
	}

	@Column(name="gathering_key")
	public Long getGatheringKey() {
		return gatheringKey;
	}

	public void setGatheringKey(Long gatheringKey) {
		this.gatheringKey = gatheringKey;
	}

	@Column(name="target_key")
	public Long getTargetKey() {
		return targetKey;
	}

	public void setTargetKey(Long targetKey) {
		this.targetKey = targetKey;
	}

	@Column(name="original_target_key")
	public Long getOriginalTargetKey() {
		return originalTargetKey;
	}

	public void setOriginalTargetKey(Long targetKey) {
		this.originalTargetKey = targetKey;
	}

	@Column(name="reference_key")
	public Long getReferenceKey() {
		return referenceKey;
	}

	public void setReferenceKey(Long referenceKey) {
		this.referenceKey = referenceKey;
	}

	@Column(name="recordbasis_key")
	public Long getRecordBasisKey() {
		return recordBasisKey;
	}

	public void setRecordBasisKey(Long recordBasisKey) {
		this.recordBasisKey = recordBasisKey;
	}

	@Column(name="super_recordbasis_key")
	public Long getSuperRecordBasisKey() {
		return superRecordBasisKey;
	}

	public void setSuperRecordBasisKey(Long superRecordBasisKey) {
		this.superRecordBasisKey = superRecordBasisKey;
	}

	@Column(name="sex_key")
	public Long getSexKey() {
		return sexKey;
	}

	public void setSexKey(Long sexKey) {
		this.sexKey = sexKey;
	}

	@Column(name="abundance_unit_key")
	public Long getAbundanceUnitKey() {
		return abundanceUnitKey;
	}

	public void setAbundanceUnitKey(Long abundanceUnitKey) {
		this.abundanceUnitKey = abundanceUnitKey;
	}

	@Column(name="taxonconfidence_key")
	public Long getTaxonConfidenceKey() {
		return taxonConfidenceKey;
	}

	public void setTaxonConfidenceKey(Long taxonConfidenceKey) {
		this.taxonConfidenceKey = taxonConfidenceKey;
	}

	@Column(name="lifestage_key")
	public Long getLifeStageKey() {
		return lifeStageKey;
	}

	public void setLifeStageKey(Long lifeStageKey) {
		this.lifeStageKey = lifeStageKey;
	}

	@Column(name="invasivecontrol_key")
	public Long getInvasiveControlEffectivenessKey() {
		return invasiveControlEffectivenessKey;
	}

	public void setInvasiveControlEffectivenessKey(Long invasiveControlEffectivenessKey) {
		this.invasiveControlEffectivenessKey = invasiveControlEffectivenessKey;
	}

	@Column(name="recordquality_key")
	public Long getRecordQualityKey() {
		return recordQualityKey;
	}

	public void setRecordQualityKey(Long recordQualityKey) {
		this.recordQualityKey = recordQualityKey;
	}

	@Column(name="reliability_key")
	public Long getReliabilityKey() {
		return reliabilityKey;
	}

	public void setReliabilityKey(Long reliabilityKey) {
		this.reliabilityKey = reliabilityKey;
	}

	@Column(name="unit_issue_key")
	public Long getIssueKey() {
		return issueKey;
	}

	public void setIssueKey(Long issueKey) {
		this.issueKey = issueKey;
	}

	@Column(name="unit_issuesource_key")
	public Long getIssueSourceKey() {
		return issueSourceKey;
	}

	public void setIssueSourceKey(Long issueSourceKey) {
		this.issueSourceKey = issueSourceKey;
	}

	@Column(name="annotated_taxon_key")
	public Long getAnnotatedTaxonKey() {
		return annotatedTaxonKey;
	}

	public void setAnnotatedTaxonKey(Long annotatedTaxonKey) {
		this.annotatedTaxonKey = annotatedTaxonKey;
	}

	@Column(name="taxongroup_key")
	public Long getReportedInformalTaxonGroupKey() {
		return reportedInformalTaxonGroupKey;
	}

	public void setReportedInformalTaxonGroupKey(Long reportedInformalTaxonGroupKey) {
		this.reportedInformalTaxonGroupKey = reportedInformalTaxonGroupKey;
	}

}
