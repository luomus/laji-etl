package fi.laji.datawarehouse.dao.vertica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.dao.vertica.AnnotationContainer.AnnotationContainerInformation;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.CollectionEntity;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TaxonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.dw.UnitQuality;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public abstract class VerticaDAOImple implements VerticaDAO {

	private final Object VERTICA_LOCK = new Object();
	protected final DAO dao;
	protected final String dataSchema;
	protected final String dimensionsSchema;
	protected final VerticaDAOImpleDimensions dimensions;
	private final SessionFactory etlSessionFactory;
	private final SessionFactory querySessionFactory;
	private final VerticaDAOImpleQueries queryDAO;
	private final String GEO_UPDATE_STATEMENT;

	protected abstract Concealment concealment();

	public VerticaDAOImple(VerticaDAOImpleSharedInitialization shared, String dataSchema) {
		this.dao = shared.getDao();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Creating DAO for schema " + dataSchema +"...");
		this.dataSchema = dataSchema;
		this.dimensionsSchema = shared.getDimensionsSchema();
		this.dimensions = shared.getDimensions();
		this.etlSessionFactory = shared.buildSessionFactoryForSchema(dataSchema, true);
		this.querySessionFactory = shared.buildSessionFactoryForSchema(dataSchema, false);
		this.queryDAO = new VerticaDAOImpleQueries(this, shared.isProductionMode());
		GEO_UPDATE_STATEMENT = generateGeoUpdateSql();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "DAO for schema " + dataSchema +" created!");
	}

	private String generateGeoUpdateSql() {
		String eCenterPoint = "((eurefEmin+eurefEMax)/2)";
		String nCenterPoint = "((eurefNmin+eurefNMax)/2)";
		return "" +
		" INSERT INTO	" + dataSchema +   ".gathering_geometry (gathering_key, grid, euref_geo) " +
		" SELECT 		key,  " +
		"				floor(" + nCenterPoint + " / 100000)||':'||floor( " + eCenterPoint + " / 100000 ), " +
		"				public.STV_GeometryPoint(" + eCenterPoint + ", " + nCenterPoint + ") " +
		" FROM			" + dataSchema + ".gathering " +
		" WHERE			key NOT IN (SELECT gathering_key FROM " + dataSchema + ".gathering_geometry ) " +
		" AND			eurefNmin IS NOT NULL AND eurefNmax IS NOT NULL AND eurefEmin IS NOT NULL AND eurefEmax IS NOT NULL ";
	}

	protected StatelessSession getETLSession() {
		return etlSessionFactory.openStatelessSession();
	}

	protected StatelessSession getQuerySession() {
		return querySessionFactory.openStatelessSession();
	}

	@Override
	public VerticaQueryDAO getQueryDAO() {
		return queryDAO;
	}

	@Override
	public void close() {
		try { if (etlSessionFactory != null) this.etlSessionFactory.close(); } catch (Exception e) {}
		try { if (querySessionFactory != null) this.querySessionFactory.close(); } catch (Exception e) {}
	}

	@Override
	public int save(List<Document> documents, ThreadStatusReporter statusReporter) {
		if (documents.isEmpty()) return 0;
		statusReporter.setStatus("Queuing to save to "+concealment()+" DW - taxon loading...");
		dimensions.getDao().getTaxonLinkingService(); // To prevent a deadlock TODO fix this some other way??
		statusReporter.setStatus("Queuing to save to "+concealment()+" DW - Vertica lock");
		synchronized (VERTICA_LOCK) {
			StatelessSession session = null;
			try {
				session = getETLSession();
				session.getTransaction().begin();
				delete(session, documents, statusReporter);
				int count = save(session, documents, statusReporter);
				session.getTransaction().commit();
				return count;
			} catch (Exception e) {
				if (session != null) {
					try { session.getTransaction().rollback(); } catch (Exception e2) {}
				}
				throw e;
			} finally {
				if (session != null) {
					try { session.close(); } catch (Exception e) {}
				}
			}
		}
	}

	private int save(StatelessSession session, List<Document> documents, ThreadStatusReporter statusReporter) {
		statusReporter.setStatus("Preparing to  save " +documents.size()+ " documents to " + dataSchema);
		List<DocumentEntity> documentEntities = new ArrayList<>();
		Set<Qname> documentIds = new HashSet<>();
		for (Document document : documents) {
			if (documentIds.contains(document.getDocumentId())) {
				throw new ETLException("Document multiple times in same batch: " + document.getDocumentId().toURI());
			}
			documentIds.add(document.getDocumentId());
			if (notDeleted(document)) {
				DocumentEntity documentEntity = new DocumentEntity(document);
				documentEntities.add(documentEntity);
			}
		}
		if (documentEntities.isEmpty()) {
			return 0;
		}
		statusReporter.setStatus("Saving " +documentEntities.size()+ " documents to " + dataSchema);
		for (DocumentEntity documentEntity : documentEntities) {
			Document document = documentEntity.getDocument();
			documentEntity.getForeignKeys().setCollectionKey(	dimensions.getKey(new CollectionEntity(document.getCollectionId().toURI()))	);
			documentEntity.getForeignKeys().setSourceKey(		dimensions.getKey(new SourceEntity(document.getSourceId().toURI()))	);
			documentEntity.getForeignKeys().setSecureLevelKey(	dimensions.getEnumKey(document.getSecureLevel()) );

			if (document.getFormId() != null) {
				documentEntity.getForeignKeys().setFormKey(dimensions.getKey(new FormEntity(document.getFormId())));
			}

			if (document.getQuality() != null && document.getQuality().getIssue() != null) {
				documentEntity.getForeignKeys().setDocumentIssueKey(getEnumKey(document.getQuality().getIssue().getIssue()));
				documentEntity.getForeignKeys().setDocumentIssueSourceKey(getEnumKey(document.getQuality().getIssue().getSource()));
			}
		}

		for (DocumentEntity documentEntity : documentEntities) {
			session.insert(documentEntity);
		}

		saveFacts(session, documentEntities, DocumentFact.class);

		saveDocumentKeywords(session, documentEntities);

		saveEditors(session, documentEntities);

		for (DocumentEntity documentEntity : documentEntities) {
			for (SecureReason secureReason : documentEntity.getDocument().getSecureReasons()) {
				session.insert(createDocumentSecureReason(documentEntity.getKey(), secureReason));
			}
		}

		saveMedia(session, documentEntities, DocumentMedia.class);

		saveAnnotations(session, documentEntities);

		saveGatherings(documentEntities, session);

		return documentEntities.size();
	}

	private void saveEditors(StatelessSession session, List<DocumentEntity> documentEntities) {
		List<DocumentUserId> documentUserIds = new ArrayList<>();
		for (DocumentEntity documentEntity : documentEntities) {
			for (String userId : documentEntity.getDocument().getEditorUserIds()) {
				documentUserIds.add(createDocumentEditor(documentEntity.getKey(), userId));
			}
		}
		for (DocumentUserId documentUserId : documentUserIds) {
			session.insert(documentUserId);
		}
	}

	private void saveDocumentKeywords(StatelessSession session, List<DocumentEntity> documentEntities) {
		List<DocumentKeyword> keywords = new ArrayList<>();
		for (DocumentEntity documentEntity : documentEntities) {
			for (String keyword : keywords(documentEntity.getDocument().getKeywords())) {
				keywords.add(createDocumentKeyword(documentEntity.getKey(), keyword));
			}
		}
		for (DocumentKeyword keyword : keywords) {
			session.insert(keyword);
		}
	}

	private void saveFacts(StatelessSession session, List<? extends FactContainer> factContainers, Class<? extends FactLink> linkClass) {
		// Preload all property keys
		Map<String, Long> propertyKeys = new HashMap<>();
		for (FactContainer container : factContainers) {
			for (Fact fact : container.revealFacts()) {
				propertyKeys.put(fact.getFact(), dimensions.getKey(new PropertyEntity().setId(fact.getFact())));
			}
		}
		// Save facts
		for (FactContainer container : factContainers) {
			for (Fact fact : container.revealFacts()) {
				FactLink link;
				try {
					link = linkClass.getConstructor(Fact.class).newInstance(fact);
				} catch (Exception e) {
					throw new ETLException("Saving fact " + fact, e);
				}
				link.setParentKey(container.getKey());
				link.setPropertyKey(propertyKeys.get(fact.getFact()));
				session.insert(link);
			}
		}
	}

	private void saveMedia(StatelessSession session, List<? extends MediaContainer> mediaContainers, Class<? extends MediaBaseEntity> entityClass) {
		for (MediaContainer container : mediaContainers) {
			Long parentKey = container.getKey();
			for (MediaObject mediaObject : container.revealMedia()) {
				MediaBaseEntity mediaEntity;
				try {
					mediaEntity = entityClass.getConstructor(MediaObject.class).newInstance(mediaObject);
				} catch (Exception e) {
					throw new ETLException(e);
				}
				mediaEntity.setParentKey(parentKey);
				mediaEntity.setMediaTypeKey(getEnumKey(mediaObject.getMediaType()));
				session.insert(mediaEntity);
			}
		}
	}

	private boolean notDeleted(Document document) {
		if (document.isDeleted() == null) return true;
		return !document.isDeleted();
	}

	private void saveGatherings(List<DocumentEntity> documentEntities, StatelessSession session) {
		List<GatheringEntity> gatheringEntities = new ArrayList<>();
		for (DocumentEntity documentEntity : documentEntities) {
			for (Gathering gathering : documentEntity.getDocument().getGatherings()) {
				GatheringEntity gatheringEntity = new GatheringEntity(documentEntity, gathering);
				GatheringInterpretations interpretations = gathering.getInterpretations();
				if (interpretations != null) {
					gatheringEntity.getForeignKeys().setBiogeographicalProvinceKey(getAreaKey(interpretations.getBiogeographicalProvince()));
					gatheringEntity.getForeignKeys().setSourceOfBiogeographicalProvinceKey(getEnumKey(interpretations.getSourceOfBiogeographicalProvince()));
					gatheringEntity.getForeignKeys().setCountryKey(getAreaKey(interpretations.getCountry()));
					gatheringEntity.getForeignKeys().setSourceOfCountryKey(getEnumKey(interpretations.getSourceOfCountry()));
					gatheringEntity.getForeignKeys().setFinnishMunicipalityKey(getAreaKey(interpretations.getFinnishMunicipality()));
					gatheringEntity.getForeignKeys().setSourceOfFinnishMunicipalityKey(getEnumKey(interpretations.getSourceOfFinnishMunicipality()));
					gatheringEntity.getForeignKeys().setSourceOfCoordinatesKey(getEnumKey(interpretations.getSourceOfCoordinates()));
				}
				GatheringConversions conversions = gathering.getConversions();
				if (conversions != null) {
					gatheringEntity.getForeignKeys().setBirdAssociationAreaKey(getAreaKey(conversions.getBirdAssociationArea()));
				}
				if (gathering.getQuality() != null) {
					if (gathering.getQuality().getIssue() != null) {
						gatheringEntity.getForeignKeys().setIssueKey(getEnumKey(gathering.getQuality().getIssue().getIssue()));
						gatheringEntity.getForeignKeys().setIssueSourceKey(getEnumKey(gathering.getQuality().getIssue().getSource()));

					}
					if (gathering.getQuality().getTimeIssue() != null) {
						gatheringEntity.getForeignKeys().setTimeIssueKey(getEnumKey(gathering.getQuality().getTimeIssue().getIssue()));
						gatheringEntity.getForeignKeys().setTimeIssueSourceKey(getEnumKey(gathering.getQuality().getTimeIssue().getSource()));

					}
					if (gathering.getQuality().getLocationIssue() != null) {
						gatheringEntity.getForeignKeys().setLocationIssueKey(getEnumKey(gathering.getQuality().getLocationIssue().getIssue()));
						gatheringEntity.getForeignKeys().setLocationIssueSourceKey(getEnumKey(gathering.getQuality().getLocationIssue().getSource()));
					}
				}
				if (gathering.getEventDate() != null) {
					DateRange range = gathering.getEventDate();
					if (range.getBegin() != null) {
						gatheringEntity.getForeignKeys().setEventDateBeginKey(dimensions.getKey(range.getBegin()));
					}
					if (range.getEnd() != null) {
						gatheringEntity.getForeignKeys().setEventDateEndKey(dimensions.getKey(range.getEnd()));
					}
				}

				List<Long> agentKeys = new ArrayList<>();
				for (String agentName : gathering.getTeam()) {
					Long agentKey = dimensions.getKey(new AgentEntity(agentName));
					agentKeys.add(agentKey);
				}
				if (!agentKeys.isEmpty()) {
					Long teamKey = dimensions.getKey(new TeamEntity(agentKeys, gathering.getTeam()));
					gatheringEntity.getForeignKeys().setTeamKey(teamKey);
				}
				gatheringEntities.add(gatheringEntity);
			}
		}

		for (GatheringEntity gatheringEntity : gatheringEntities) {
			session.insert(gatheringEntity);
		}

		saveFacts(session, gatheringEntities, GatheringFact.class);

		saveObservers(session, gatheringEntities);

		saveGatheringAreas(session, gatheringEntities);

		saveMedia(session, gatheringEntities, GatheringMedia.class);

		saveTaxonCensus(session, gatheringEntities);

		saveUnits(gatheringEntities, session);
	}

	private void saveTaxonCensus(StatelessSession session, List<GatheringEntity> gatheringEntities) {
		for (GatheringEntity gatheringEntity : gatheringEntities) {
			for (TaxonCensus taxonCensus : gatheringEntity.getGathering().getTaxonCensus()) {
				try {
					TaxonCensusEntity entity = createTaxonCensusEntity(gatheringEntity, taxonCensus);
					session.insert(entity);
				} catch (CriticalParseFailure e) {
					// invalid entity, skip
				}
			}
		}
	}

	private TaxonCensusEntity createTaxonCensusEntity(GatheringEntity gatheringEntity, TaxonCensus taxonCensus) throws CriticalParseFailure {
		TaxonCensusEntity entity = new TaxonCensusEntity(taxonCensus);
		entity.setGatheringKey(gatheringEntity.getKey());
		return entity;
	}

	private void saveGatheringAreas(StatelessSession session, List<GatheringEntity> gatheringEntities) {
		List<GatheringArea> municipalityGatheringAreas = new ArrayList<>();
		List<GatheringArea> bioprovinceGatheringAreas = new ArrayList<>();
		for (GatheringEntity gatheringEntity : gatheringEntities) {
			GatheringInterpretations interpretations = gatheringEntity.getGathering().getInterpretations();
			if (interpretations == null) continue;
			if (interpretations.getFinnishMunicipalities() != null) {
				if (interpretations.getFinnishMunicipalities().size() > 1) { // for performance we only add link table rows for those that have more than one; query is done based on municipality_key and the link table
					for (Qname municipality : interpretations.getFinnishMunicipalities()) {
						municipalityGatheringAreas.add(createGatheringArea(new GatheringMunicipality(), municipality, gatheringEntity.getKey()));
					}
				}
			}
			if (interpretations.getBiogeographicalProvinces() != null) {
				if (interpretations.getBiogeographicalProvinces().size() > 1) { // for performance we only add link table rows for those that have more than one; query is done based on bioprovince_key and the link table
					for (Qname bioProvince : interpretations.getBiogeographicalProvinces()) {
						bioprovinceGatheringAreas.add(createGatheringArea(new GatheringBioprovince(), bioProvince, gatheringEntity.getKey()));
					}
				}
			}
		}

		for (GatheringArea area : municipalityGatheringAreas) {
			session.insert(area);
		}
		for (GatheringArea area : bioprovinceGatheringAreas) {
			session.insert(area);
		}
	}

	private GatheringArea createGatheringArea(GatheringArea entity, Qname areaId, Long gatheringKey) {
		entity.setGatheringKey(gatheringKey);
		entity.setAreaKey(getAreaKey(areaId));
		return entity;
	}

	private void saveObservers(StatelessSession session, List<GatheringEntity> gatheringEntities) {
		List<GatheringUserId> gatheringUserIds = new ArrayList<>();
		for (GatheringEntity gatheringEntity : gatheringEntities) {
			for (String userId : gatheringEntity.getGathering().getObserverUserIds()) {
				gatheringUserIds.add(createGatheringObserver(gatheringEntity.getKey(), userId));
			}
		}
		for (GatheringUserId gatheringUserId : gatheringUserIds) {
			session.insert(gatheringUserId);
		}
	}

	private GatheringUserId createGatheringObserver(Long gatheringKey, String userId) {
		Long userIdKey = dimensions.getKey(new UserIdEntity(userId));
		GatheringUserId gatheringUserId = new GatheringUserId();
		gatheringUserId.setUserIdKey(userIdKey);
		gatheringUserId.setGatheringKey(gatheringKey);
		return gatheringUserId;
	}

	@Override
	public void callGeoUpdate() {
		StatelessSession session = null;
		try {
			session = getETLSession();
			session.beginTransaction();
			session.createSQLQuery(GEO_UPDATE_STATEMENT).executeUpdate();
			session.getTransaction().commit();
		} finally {
			if (session != null) session.close();
		}
	}

	private void saveUnits(List<GatheringEntity> gatheringEntities, StatelessSession session) {
		List<UnitEntity> unitEntities = new ArrayList<>();
		for (GatheringEntity gatheringEntity : gatheringEntities) {
			for (Unit unit : gatheringEntity.getGathering().getUnits()) {
				UnitEntity unitEntity = new UnitEntity(gatheringEntity, unit);

				unitEntity.getForeignKeys().setLifeStageKey(getEnumKey(unit.getLifeStage()));
				unitEntity.getForeignKeys().setRecordBasisKey(getEnumKey(unit.getRecordBasis()));
				unitEntity.getForeignKeys().setSuperRecordBasisKey(getEnumKey(unit.getSuperRecordBasis()));
				unitEntity.getForeignKeys().setAbundanceUnitKey(getEnumKey(unit.getAbundanceUnit()));
				unitEntity.getForeignKeys().setSexKey(getEnumKey(unit.getSex()));
				unitEntity.getForeignKeys().setTaxonConfidenceKey(getEnumKey(unit.getReportedTaxonConfidence()));

				if (unit.getInterpretations() != null) {
					UnitInterpretations i = unit.getInterpretations();
					unitEntity.getForeignKeys().setAnnotatedTaxonKey(TaxonBaseEntity.parseTaxonKey(i.getAnnotatedTaxonId()));
					unitEntity.getForeignKeys().setInvasiveControlEffectivenessKey(getEnumKey(i.getInvasiveControlEffectiveness()));
					unitEntity.getForeignKeys().setRecordQualityKey(getEnumKey(i.getRecordQuality()));
					unitEntity.getForeignKeys().setReliabilityKey(getEnumKey(i.getReliability()));
				}
				if (unit.getQuality() == null) {
					throw new ETLException("unit.quality is null");
				}
				UnitQuality quality = unit.getQuality();
				if (quality.isDocumentGatheringUnitQualityIssues() == null) throw new ETLException("unit.quality.documentGatheringUnitQualityIssues is null");

				if (quality.getIssue() != null) {
					unitEntity.getForeignKeys().setIssueKey(getEnumKey(quality.getIssue().getIssue()));
					unitEntity.getForeignKeys().setIssueSourceKey(getEnumKey(quality.getIssue().getSource()));
				}

				Long referenceKey = null;
				if (given(unit.getReferencePublication())) {
					referenceKey = dimensions.getKey(new ReferenceEntity(unit.getReferencePublication()));
					unitEntity.getForeignKeys().setReferenceKey(referenceKey);
				}

				unitEntity.getForeignKeys().setTargetKey(getTargetKey(unit, gatheringEntity, true, referenceKey));
				unitEntity.getForeignKeys().setOriginalTargetKey(getTargetKey(unit, gatheringEntity, false, referenceKey));

				if (given(unit.getReportedInformalTaxonGroup())) {
					unitEntity.getForeignKeys().setReportedInformalTaxonGroupKey(dimensions.getKey(new InformalTaxonGroupEntity(unit.getReportedInformalTaxonGroup().toURI())));
				}

				unitEntities.add(unitEntity);
			}
		}

		for (UnitEntity unitEntity : unitEntities) {
			session.insert(unitEntity);
		}

		saveFacts(session, unitEntities, UnitFact.class);

		saveUnitMedia(session, unitEntities);

		saveAnnotations(session, unitEntities);

		saveUnitKeywords(session, unitEntities);

		saveUnitEffectiveTags(session, unitEntities);

		saveSamples(session, unitEntities);
	}

	private void saveUnitMedia(StatelessSession session, List<UnitEntity> unitEntities) {
		List<UnitMediaEntity> unitMediaEntities = new ArrayList<>();
		for (UnitEntity unitEntity : unitEntities) {
			for (MediaObject media : unitEntity.getUnit().getMedia()) {
				UnitMediaEntity unitMediaEntity = new UnitMediaEntity(unitEntity, media);
				unitMediaEntity.getMediaForeignKeys().setMediaTypeKey(getEnumKey(media.getMediaType()));
				unitMediaEntities.add(unitMediaEntity);
			}
		}
		for (UnitMediaEntity unitMediaEntity : unitMediaEntities) {
			session.insert(unitMediaEntity);
		}
	}

	private Long getTargetKey(Unit unit, GatheringEntity gatheringEntity, boolean useAnnotations, Long referencekey) {
		TargetEntity e = dao.getTaxonLinkingService().getTargetEntity(unit, gatheringEntity.getGathering(), useAnnotations, referencekey);
		if (e == null) return null;
		return dimensions.getKey(e);
	}


	private void saveSamples(StatelessSession session, List<UnitEntity> unitEntities) {
		List<SampleEntity> sampleEntities = new ArrayList<>();
		for (UnitEntity unitEntity : unitEntities) {
			for (Sample sample : unitEntity.getUnit().getSamples()) {
				SampleEntity sampleEntity = new SampleEntity(sample, unitEntity);
				if (given(sample.getCollectionId())) {
					sampleEntity.setCollectionKey(dimensions.getKey(new CollectionEntity(sample.getCollectionId().toURI())));
				}
				sampleEntities.add(sampleEntity);
			}
		}
		for (SampleEntity sampleEntity : sampleEntities) {
			session.insert(sampleEntity);
		}
		saveFacts(session, sampleEntities, SampleFact.class);
		saveSampleKeywords(session, sampleEntities);
	}

	private void saveSampleKeywords(StatelessSession session, List<SampleEntity> sampleEntities) {
		List<SampleKeyword> keywordEntities = new ArrayList<>();
		for (SampleEntity sampleEntity : sampleEntities) {
			Set<String> keywords = new HashSet<>();
			keywords.addAll(keywords(sampleEntity.getParentUnitEntity().getUnit().getKeywords()));
			keywords.addAll(keywords(sampleEntity.getParentUnitEntity().getParentDocument().getKeywords()));
			addSampleKeywords(keywords, sampleEntity.getSample());
			for (String keyword : keywords) {
				keywordEntities.add(createSampleKeyword(sampleEntity.getKey(), keyword));
			}
		}
		for (SampleKeyword keywordEntity : keywordEntities) {
			session.insert(keywordEntity);
		}
	}

	private void addSampleKeywords(Set<String> keywords, Sample sample) {
		keywords.addAll(keywords(sample.getKeywords()));
		keywords.add(sample.getSampleId().toString());
		keywords.add(sample.getSampleId().toURI());
	}

	private Set<String> keywords(List<String> keywords) {
		Set<String> toBeStored = new HashSet<>();
		for (String keyword : keywords) {
			keyword = Utils.removeWhitespace(keyword);
			if (keyword.isEmpty()) continue;
			toBeStored.add(keyword);
			splitByColon(toBeStored, keyword);
		}
		return toBeStored;
	}

	private void splitByColon(Set<String> toBeStored, String keyword) {
		if (!keyword.contains(":")) return;
		if (keyword.startsWith("http")) return;
		String[] parts = keyword.split(Pattern.quote(":"));
		if (parts.length != 2) return;
		keyword = parts[1];
		if (!keyword.isEmpty()) {
			toBeStored.add(keyword);
		}
	}

	private SampleKeyword createSampleKeyword(Long sampleKey, String keyword) {
		SampleKeyword sampleKeyword = new SampleKeyword();
		sampleKeyword.setSampleKey(sampleKey);
		sampleKeyword.setKeyword(keyword);
		return sampleKeyword;
	}

	private void saveUnitKeywords(StatelessSession session, List<UnitEntity> unitEntities) {
		List<UnitKeyword> keywordEntities = new ArrayList<>();
		for (UnitEntity unitEntity : unitEntities) {
			Set<String> keywords = new HashSet<>();
			keywords.addAll(keywords(unitEntity.getUnit().getKeywords()));
			keywords.addAll(keywords(unitEntity.getParentDocument().getKeywords()));
			for (Sample sample : unitEntity.getUnit().getSamples()) {
				addSampleKeywords(keywords, sample);
			}
			for (String keyword : keywords) {
				keywordEntities.add(createUnitKeyword(unitEntity.getKey(), keyword));
			}
		}
		for (UnitKeyword keywordEntity : keywordEntities) {
			session.insert(keywordEntity);
		}
	}

	private void saveUnitEffectiveTags(StatelessSession session, List<UnitEntity> unitEntities) {
		List<UnitEffectiveTag> entities = new ArrayList<>();
		for (UnitEntity unitEntity : unitEntities) {
			List<Tag> effectiveTags = unitEntity.getUnit().getInterpretations().getEffectiveTags();
			if (effectiveTags == null) continue;
			for (Tag t : effectiveTags) {
				UnitEffectiveTag unitEffectiveTag = new UnitEffectiveTag();
				unitEffectiveTag.setUnitKey(unitEntity.getKey());
				unitEffectiveTag.setTagKey(dimensions.getEnumKey(t));
				entities.add(unitEffectiveTag);
			}
		}
		for (UnitEffectiveTag e : entities) {
			session.insert(e);
		}
	}

	private UnitKeyword createUnitKeyword(Long unitKey, String keyword) {
		UnitKeyword unitKeyword = new UnitKeyword();
		unitKeyword.setUnitKey(unitKey);
		unitKeyword.setKeyword(keyword);
		return unitKeyword;
	}


	private void saveAnnotations(StatelessSession session, List<? extends AnnotationContainer> entities) {
		List<AnnotationEntity> annotationEntities = new ArrayList<>();
		for (AnnotationContainer entity : entities) {
			AnnotationContainerInformation info = entity.revealAnnotationContainerInformation();
			for (Annotation annotation : entity.revealAnnotations()) {
				if (annotation.isDeleted()) continue;
				AnnotationEntity annotationEntity = new AnnotationEntity(annotation);
				annotationEntity.setDocumentId(info.getDocument().getDocumentId().toURI());
				annotationEntity.setDocumentKey(info.getDocumentKey());
				if (info.getUnit() != null) {
					annotationEntity.setUnitId(info.getUnit().getUnitId().toURI());
					annotationEntity.setUnitKey(info.getUnitKey());
				}
				if (annotation.getAnnotationByPerson() != null) {
					Long personKey = PersonBaseEntity.parsePersonKey(annotation.getAnnotationByPerson());
					annotationEntity.setAnnotationByPersonKey(personKey);
				}
				if (annotation.getAnnotationBySystem() != null) {
					annotationEntity.setAnnotationBySystemKey(dimensions.getKey(new SourceEntity(annotation.getAnnotationBySystem().toURI())));
				}
				annotationEntities.add(annotationEntity);
			}
		}
		for (AnnotationEntity annotationEntity : annotationEntities) {
			session.insert(annotationEntity);
		}
	}

	private DocumentSecureReason createDocumentSecureReason(Long documentKey, SecureReason secureReason) {
		Long secureReasonKey = dimensions.getKey(secureReason);
		DocumentSecureReason documentSecureReason = new DocumentSecureReason();
		documentSecureReason.setSecureReasonKey(secureReasonKey);
		documentSecureReason.setDocumentKey(documentKey);
		return documentSecureReason;
	}

	private DocumentUserId createDocumentEditor(Long documentKey, String userId) {
		Long userIdKey = dimensions.getKey(new UserIdEntity(userId));
		DocumentUserId documentUserId = new DocumentUserId();
		documentUserId.setUserIdKey(userIdKey);
		documentUserId.setDocumentKey(documentKey);
		return documentUserId;
	}

	private DocumentKeyword createDocumentKeyword(Long documentKey, String keyword) {
		DocumentKeyword documentKeyword = new DocumentKeyword();
		documentKeyword.setDocumentKey(documentKey);
		documentKeyword.setKeyword(keyword);
		return documentKeyword;
	}

	private Long getEnumKey(Enum<?> enumValue) {
		return dimensions.getEnumKey(enumValue);
	}

	private Long getAreaKey(Qname area) {
		if (area == null || !area.isSet()) return null;
		return dimensions.getKey(new AreaEntity(area.toURI()));
	}

	@Override
	public void delete(Document document, ThreadStatusReporter statusReporter) {
		synchronized (VERTICA_LOCK) {
			StatelessSession session = null;
			try {
				session = getETLSession();
				session.getTransaction().begin();
				delete(session, Utils.singleEntryList(document), statusReporter);
				session.getTransaction().commit();
			} catch (Exception e) {
				if (session != null) {
					try { session.getTransaction().rollback(); } catch (Exception e2) {}
				}
				throw e;
			} finally {
				if (session != null) {
					try { session.close(); } catch (Exception e) {}
				}
			}
		}
	}

	private void delete(StatelessSession session, List<Document> documents, ThreadStatusReporter statusReporter) {
		if (documents.isEmpty()) return;

		statusReporter.setStatus("Searching for document keys to delete from " + dataSchema);
		List<Long> documentKeysToDelete = new ArrayList<>();
		for (List<Document> part : split(documents)) {
			documentKeysToDelete.addAll(getDocumentKeys(session, part));
		}
		if (documentKeysToDelete.isEmpty()) return;

		statusReporter.setStatus("Deleting " + documentKeysToDelete.size() + " documents from " + dataSchema);
		for (List<Long> part : split(documentKeysToDelete)) {
			delete(part, session);
		}
	}

	private List<Long> getDocumentKeys(StatelessSession session, List<Document> documents) {
		List<String> documentIds = new ArrayList<>();
		for (Document document : documents) {
			documentIds.add(document.getDocumentId().toURI());
		}
		@SuppressWarnings("unchecked")
		List<Long> keys = session.createCriteria(DocumentEntity.class)
		.add(Restrictions.in("id", documentIds))
		.setProjection(Projections.property("key"))
		.list();

		return keys;
	}

	private void delete(List<Long> documentKeysToDelete, StatelessSession session) {
		deleteUnitMedia(documentKeysToDelete, session);
		deleteUnits(documentKeysToDelete, session);
		deleteGatherings(documentKeysToDelete, session);
		deleteDocuments(documentKeysToDelete, session);
	}

	private void deleteDocuments(List<Long> documentKeys, StatelessSession session) {
		session.createQuery("DELETE FROM " + DocumentEntity.class.getSimpleName() + " WHERE key IN (:documentKeys) ")
		.setParameterList("documentKeys", documentKeys)
		.executeUpdate();
	}

	private void deleteGatherings(List<Long> documentKeys, StatelessSession session) {
		session.createQuery("DELETE FROM " + GatheringEntity.class.getSimpleName() + " WHERE document_key IN (:documentKeys) ")
		.setParameterList("documentKeys", documentKeys)
		.executeUpdate();
	}

	private void deleteUnits(List<Long> documentKeys, StatelessSession session) {
		session.createQuery("DELETE FROM " + UnitEntity.class.getSimpleName() + " WHERE document_key IN (:documentKeys) ")
		.setParameterList("documentKeys", documentKeys)
		.executeUpdate();
	}

	private void deleteUnitMedia(List<Long> documentKeys, StatelessSession session) {
		session.createQuery("DELETE FROM " + UnitMediaEntity.class.getSimpleName() + " WHERE document_key IN (:documentKeys) ")
		.setParameterList("documentKeys", documentKeys)
		.executeUpdate();
	}

	private <T> List<List<T>> split(List<T> keys) {
		List<List<T>> splitted = Lists.partition(keys, 5000);
		return splitted;
	}

	private boolean given(Object o) {
		return o != null && o.toString().trim().length() > 0;
	}

	private static final List<String> BIG_CLEANUP_TABLES = Utils.list("unit", "gathering", "document", "unit_media");
	private static final List<String> CLEANUP_TABLES = Utils.list(
			"annotation", "sample", "unit_fact", "gathering_fact", "document_fact", "sample_fact",
			"gathering_media", "document_media",
			"gathering_geometry", "gathering_municipality", "gathering_bioprovince",
			"document_keyword", "document_securereason", "document_userid", "gathering_userid",
			"taxoncensus", "unit_keyword", "sample_keyword", "unit_effective_tag");

	@Override
	public void performCleanUp(ThreadStatusReporter statusReporter) {
		performCleanUpAndMaintenance(getRotateBigTable(), statusReporter);

		CLEANUP_TABLES.stream()
		.forEach(table -> performCleanUpAndMaintenance(table, statusReporter));
	}

	private String getRotateBigTable() {
		String table = BIG_CLEANUP_TABLES.get(0);
		Collections.rotate(BIG_CLEANUP_TABLES, 1);
		return table;
	}

	private static final Map<String, String> CLEAN_UP_QUERIES;
	static {
		CLEAN_UP_QUERIES = new HashMap<>();
		CLEAN_UP_QUERIES.put("document_fact", "" +
				"delete from <SCHEMA>.document_fact where parent_key in ( " +
				"   select parent_key from <SCHEMA>.document_fact " +
				"   left join <SCHEMA>.document on document.key = document_fact.parent_key " +
				"    where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_fact", "" +
				"delete from <SCHEMA>.gathering_fact where parent_key in ( " +
				"		select parent_key from <SCHEMA>.gathering_fact " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_fact.parent_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("unit_fact", "" +
				"delete from <SCHEMA>.unit_fact where parent_key in ( " +
				"		select parent_key from <SCHEMA>.unit_fact " +
				"		left join <SCHEMA>.unit on unit.key = unit_fact.parent_key " +
				"		where unit.key is null " +
				")");
		CLEAN_UP_QUERIES.put("sample_fact", "" +
				"delete from <SCHEMA>.sample_fact where parent_key in ( " +
				"		select parent_key from <SCHEMA>.sample_fact " +
				"		left join <SCHEMA>.sample on sample.key = sample_fact.parent_key " +
				"		where sample.key is null " +
				")");
		CLEAN_UP_QUERIES.put("document_media", "" +
				"delete from <SCHEMA>.document_media where parent_key in ( " +
				"		select parent_key from <SCHEMA>.document_media " +
				"		left join <SCHEMA>.document on document.key = document_media.parent_key " +
				"		where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_media", "" +
				"delete from <SCHEMA>.gathering_media where parent_key in ( " +
				"		select parent_key from <SCHEMA>.gathering_media " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_media.parent_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("document_userid", "" +
				"delete from <SCHEMA>.document_userid where document_key in ( " +
				"		select document_key from <SCHEMA>.document_userid " +
				"		left join <SCHEMA>.document on document.key = document_userid.document_key " +
				"		where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_userid", "" +
				"delete from <SCHEMA>.gathering_userid where gathering_key in ( " +
				"		select gathering_key from <SCHEMA>.gathering_userid " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_userid.gathering_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("document_keyword", "" +
				"delete from <SCHEMA>.document_keyword where document_key in ( " +
				"		select document_key from <SCHEMA>.document_keyword " +
				"		left join <SCHEMA>.document on document.key = document_keyword.document_key " +
				"		where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("unit_keyword", "" +
				"delete from <SCHEMA>.unit_keyword where unit_key in ( " +
				"		select unit_key from <SCHEMA>.unit_keyword " +
				"		left join <SCHEMA>.unit on unit.key = unit_keyword.unit_key " +
				"		where unit.key is null " +
				")");
		CLEAN_UP_QUERIES.put("unit_effective_tag", "" +
				"delete from <SCHEMA>.unit_effective_tag where unit_key in ( " +
				"		select unit_key from <SCHEMA>.unit_effective_tag " +
				"		left join <SCHEMA>.unit on unit.key = unit_effective_tag.unit_key " +
				"		where unit.key is null " +
				")");
		CLEAN_UP_QUERIES.put("sample_keyword", "" +
				"delete from <SCHEMA>.sample_keyword where sample_key in ( " +
				"		select sample_key from <SCHEMA>.sample_keyword " +
				"		left join <SCHEMA>.sample on sample.key = sample_keyword.sample_key " +
				"		where sample.key is null " +
				")");
		CLEAN_UP_QUERIES.put("document_securereason", "" +
				"delete from <SCHEMA>.document_securereason where document_key in ( " +
				"		select document_key from <SCHEMA>.document_securereason " +
				"		left join <SCHEMA>.document on document.key = document_securereason.document_key " +
				"		where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_geometry", "" +
				"delete from <SCHEMA>.gathering_geometry where gathering_key in ( " +
				"		select gathering_key from <SCHEMA>.gathering_geometry " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_geometry.gathering_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_municipality", "" +
				"delete from <SCHEMA>.gathering_municipality where gathering_key in ( " +
				"		select gathering_key from <SCHEMA>.gathering_municipality " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_municipality.gathering_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("gathering_bioprovince", "" +
				"delete from <SCHEMA>.gathering_bioprovince where gathering_key in ( " +
				"		select gathering_key from <SCHEMA>.gathering_bioprovince " +
				"		left join <SCHEMA>.gathering on gathering.key = gathering_bioprovince.gathering_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("taxoncensus", "" +
				"delete from <SCHEMA>.taxoncensus where gathering_key in ( " +
				"		select gathering_key from <SCHEMA>.taxoncensus " +
				"		left join <SCHEMA>.gathering on gathering.key = taxoncensus.gathering_key " +
				"		where gathering.key is null " +
				")");
		CLEAN_UP_QUERIES.put("annotation", "" +
				"delete from <SCHEMA>.annotation where key in ( " +
				"		select annotation.key from <SCHEMA>.annotation " +
				"		left join <SCHEMA>.document on document.key = annotation.document_key " +
				"		where document.key is null " +
				")");
		CLEAN_UP_QUERIES.put("sample", "" +
				"delete from <SCHEMA>.sample where key in ( " +
				"		select sample.key from <SCHEMA>.sample " +
				"		left join <SCHEMA>.unit on unit.key = sample.unit_key " +
				"		where unit.key is null " +
				")");
	}

	private void performCleanUpAndMaintenance(String tableName, ThreadStatusReporter statusReporter) {
		String schemaTableName = dataSchema + "." + tableName;
		statusReporter.setStatus(schemaTableName + ": clean up and maintenance");
		try {
			performCleanUp(tableName, statusReporter);
			performMaintenance(tableName);
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, VerticaDAOImple.class, schemaTableName, e);
			dao.getErrorReporter().report(schemaTableName, e);
		}
	}

	private void performMaintenance(String tableName) {
		StatelessSession session = null;
		try {
			session = getETLSession();
			performMaintenance(tableName, session);
		} finally {
			if (session != null) {
				try { session.close(); } catch (Exception e) {}
			}
		}
	}

	private void performCleanUp(String tableName, ThreadStatusReporter statusReporter) {
		String cleanUpSql = CLEAN_UP_QUERIES.get(tableName);
		if (!given(cleanUpSql)) return;

		tableName = dataSchema + "." + tableName;
		synchronized (VERTICA_LOCK) {
			StatelessSession session = null;
			try {
				session = getETLSession();
				performCleanUp(statusReporter, tableName, session, cleanUpSql);
			} finally {
				if (session != null) {
					try { session.close(); } catch (Exception e) {}
				}
			}
		}
	}

	private void performCleanUp(ThreadStatusReporter statusReporter, String tableName, StatelessSession session, String sql) {
		sql = sql.replace("<SCHEMA>", dataSchema);
		session.getTransaction().begin();
		int count = session.createSQLQuery(sql).executeUpdate();
		session.getTransaction().commit();
		if (count > 0) {
			statusReporter.setStatus("Cleanup form " + tableName + " deleted " + count + " rows");
		}
	}

	private void performMaintenance(String tableName, StatelessSession session) {
		String schemaTableName = dataSchema + "." + tableName;
		session.createSQLQuery(" SELECT PURGE_TABLE('"+schemaTableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT DO_TM_TASK('moveout', '"+schemaTableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT DO_TM_TASK('mergeout', '"+schemaTableName+"') ").uniqueResult();
		session.createSQLQuery(" SELECT ANALYZE_STATISTICS('"+schemaTableName+"') ").uniqueResult();
	}

}
