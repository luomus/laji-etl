package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="document_keyword")
class DocumentKeyword implements Serializable {

	private static final long serialVersionUID = 8108680480820101004L;
	private Long documentKey;
	private String keyword;

	@Id @Column(name="document_key")
	public Long getDocumentKey() {
		return documentKey;
	}
	public void setDocumentKey(Long documentKey) {
		this.documentKey = documentKey;
	}

	@Id @Column(name="keyword")
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="document_key", referencedColumnName="key", insertable=false, updatable=false)
	public DocumentEntity getDocument() { // for queries only
		return null;
	}

	public void setDocument(@SuppressWarnings("unused") DocumentEntity document) {
		// for queries only
	}

}
