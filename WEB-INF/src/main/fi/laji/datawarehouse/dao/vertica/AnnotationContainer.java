package fi.laji.datawarehouse.dao.vertica;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;

interface AnnotationContainer {

	static class AnnotationContainerInformation {
		private final Document document;
		private final Gathering gathering;
		private final Unit unit;
		private final Long documentKey;
		private final Long unitKey;
		public AnnotationContainerInformation(Document document, Long documentKey) {
			this(document, documentKey, null, null, null);
		}
		public AnnotationContainerInformation(Document document, Long documentKey, Gathering gathering, Unit unit, Long unitKey) {
			this.document = document;
			this.gathering = gathering;
			this.unit = unit;
			this.documentKey = documentKey;
			this.unitKey = unitKey;
		}
		public Long getDocumentKey() {
			return documentKey;
		}
		public Long getUnitKey() {
			return unitKey;
		}
		public Unit getUnit() {
			return unit;
		}
		public Gathering getGathering() {
			return gathering;
		}
		public Document getDocument() {
			return document;
		}
	}

	public List<Annotation> revealAnnotations();

	public AnnotationContainerInformation revealAnnotationContainerInformation();

}