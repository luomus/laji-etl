package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Map;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;

class UserIdReprocessor {

	private final VerticaDAOImpleDimensions dimensions;

	public UserIdReprocessor(VerticaDAOImpleDimensions dimensions) {
		this.dimensions = dimensions;
	}

	public void reprocess() {
		dimensions.getDao().logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to reprocess contents of UserId table...");

		StatelessSession session = null;
		try {
			session = dimensions.getSession();
			tryToReprocess(session);
		} catch (Exception e) {
			dimensions.getDao().logError(Const.LAJI_ETL_QNAME, this.getClass(), null, e);
			dimensions.getDao().getErrorReporter().report("UserId reprocessing failed", e);
			throw new ETLException("UserId reprocessing failed", e);
		} finally {
			if (session != null) try { session.close(); } catch (Exception e) {}
		}
		dimensions.getDao().logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reprocessing contents of UserId table completed!");
	}

	private void tryToReprocess(StatelessSession session) {
		List<UserIdEntity> userIds = dimensions.getUserIds();
		Map<String, Qname> lookupStructure = dimensions.getDao().getPersonLookupStructure();
		int i = 0;
		session.getTransaction().begin();
		for (UserIdEntity userId : userIds) {
			Qname personId = lookupStructure.get(userId.getId());
			Long personKey = PersonBaseEntity.parsePersonKey(personId);
			if (reasonToUpdate(userId, personKey)) {
				userId.setPersonKey(personKey);
				session.update(userId);
				if (i++ >= 20) {
					session.getTransaction().commit();
					session.getTransaction().begin();
					i = 0;
				}
			}
		}
		session.getTransaction().commit();
	}

	private boolean reasonToUpdate(UserIdEntity userId, Long personKey) {
		if (userId.getPersonKey() == null && personKey == null) return false;
		if (personKey != null && personKey.equals(userId.getPersonKey())) return false;
		return true;
	}

}
