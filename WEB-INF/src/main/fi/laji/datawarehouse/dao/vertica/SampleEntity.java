package fi.laji.datawarehouse.dao.vertica;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Sample;

@Entity
@Table(name="sample")
@AttributeOverride(name="id", column=@Column(name="sample_id"))
class SampleEntity extends NonAutogeneratingBaseEntity implements FactContainer {

	private Sample sample;
	private Long unitKey;
	private UnitEntity unitEntity;
	private Long collectionKey;

	public SampleEntity() {}

	public SampleEntity(Sample sample, UnitEntity unitEntity) {
		this.setSample(sample);
		this.setUnitKey(unitEntity.getKey());
		this.unitEntity = unitEntity;
		setKey(generateKey());
		setId(sample.getSampleId().toURI());
	}

	@Embedded
	public Sample getSample() {
		return sample;
	}

	public void setSample(Sample sample) {
		this.sample = sample;
	}

	@Column(name="unit_key")
	public Long getUnitKey() {
		return unitKey;
	}

	public void setUnitKey(Long unitKey) {
		this.unitKey = unitKey;
	}

	@Column(name="collection_key")
	public Long getCollectionKey() {
		return collectionKey;
	}

	public void setCollectionKey(Long dwCollectionKey) {
		this.collectionKey = dwCollectionKey;
	}

	@Override
	public List<Fact> revealFacts() {
		return sample.getFacts();
	}

	@OneToMany(mappedBy="sample", fetch=FetchType.LAZY)
	public Set<SampleKeyword> getSampleKeywords() {
		return null; // for queries only
	}

	public void setSampleKeywords(@SuppressWarnings("unused") Set<SampleKeyword> sampleKeywords) {
		// for queries only
	}

	@OneToMany(mappedBy="sample", fetch=FetchType.LAZY)
	public Set<SampleFact> getSampleFacts() {
		return null; // for queries only
	}

	public void setSampleFacts(@SuppressWarnings("unused") Set<SampleFact> sampleFacts) {
		// for queries only
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Transient
	public UnitEntity getParentUnitEntity() {
		return unitEntity;
	}

}
