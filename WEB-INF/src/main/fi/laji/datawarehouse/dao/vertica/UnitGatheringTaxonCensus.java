package fi.laji.datawarehouse.dao.vertica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="unit_gathering_taxoncensus")
public class UnitGatheringTaxonCensus implements Serializable {

	private static final long serialVersionUID = -4727077010441467008L;

	@Id @Column(name="unit_key")
	public Long getUnitKey() {
		return null;
	}

	public void setUnitKey(@SuppressWarnings("unused") Long unitKey) {

	}

	@Id @Column(name="taxon_key")
	public Long getTaxonKey() {
		return null;
	}

	public void setTaxonKey(@SuppressWarnings("unused") Long taxonKey) {

	}

	@Id @Column(name="type")
	public String getType() {
		return null;
	}

	public void setType(@SuppressWarnings("unused") String type) {

	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="unit_key", referencedColumnName="key", insertable=false, updatable=false)
	public UnitEntity getUnit() { // for queries only
		return null;
	}

	public void setUnit(@SuppressWarnings("unused") UnitEntity unit) {
		// for queries only
	}

	@Override
	public boolean equals(Object o) {
		throw new UnsupportedOperationException("Equals not implemented for " + this.getClass());
	}

	@Override
	public int hashCode() {
		throw new UnsupportedOperationException("HashCode not implemented for " + this.getClass());
	}

}
