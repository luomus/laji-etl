package fi.laji.datawarehouse.dao.vertica;

import java.util.Collection;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO.VerticaDimensionsPersonService;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo.SystemId;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.utils.DateUtils;

public class PersonReload {

	private final DAO dao;

	public PersonReload(DAO dao) {
		this.dao = dao;
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created");
	}

	public void reload() {
		long startMoment = DateUtils.getCurrentEpoch();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting person reload to Vertica... (start moment "+startMoment+")");
		VerticaDimensionsPersonService service = null;
		try {
			service = dao.getVerticaDimensionsDAO().getPersonSevice();
			service.emptyPersonTempTable();
			loadPersonToTempTable(service);
			service.switchPersonTempToActual();
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), ""+startMoment, e);
			dao.getErrorReporter().report("Person reload to Vertica failed", e);
			throw new ETLException("Person reload to Vertica failed", e);
		} finally {
			if (service != null) service.close();
		}
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Person reload to Vertica completed! (start moment "+startMoment+")");
	}

	private void loadPersonToTempTable(VerticaDimensionsPersonService service) throws Exception {
		Collection<PersonInfo> persons = dao.getPersons();
		for (PersonInfo person : persons) {
			PersonBaseEntity e = new PersonBaseEntity(person.getId());
			e.setName(getFullNameLogError(person));
			e.setLintuvaaraId(getLintuvaaraIdLogError(person));
			e.setOmaRiistaId(getOmaRiistaIdLogError(person));
			try {
				e.setEmail(person.getEmail());
			} catch (Exception ex) {
				// no warning for missing email
			}
			service.insertToPersonTempTable(e);
		}
	}

	private Integer getOmaRiistaIdLogError(PersonInfo person) {
		Integer omaRiistaId = null;
		try {
			if (person.getSystemIds().containsKey(SystemId.OMARIISTA) ) {
				omaRiistaId = Integer.valueOf(person.getSystemIds().get(SystemId.OMARIISTA).iterator().next());
			}
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), person.getId().toString(), new IllegalStateException("Person " + person.getId() + " has invalid oma riista id!"));
		}
		return omaRiistaId;
	}

	private Integer getLintuvaaraIdLogError(PersonInfo person) {
		Integer lintuvaaraId = null;
		try {
			if (person.getSystemIds().containsKey(SystemId.LINTUVAARA)) {
				lintuvaaraId = Integer.valueOf(person.getSystemIds().get(SystemId.LINTUVAARA).iterator().next());
			}
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), person.getId().toString(), new IllegalStateException("Person " + person.getId() + " has invalid lintuvaara id!"));
		}
		return lintuvaaraId;
	}

	private String getFullNameLogError(PersonInfo person) {
		String fullName = person.getFullName();
		if (!given(person.getFullName())) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), person.getId().toString(), new IllegalStateException("Person " + person.getId() + " is missing full name!"));
			fullName = person.getId().toURI();
		}
		return fullName;
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}
}
