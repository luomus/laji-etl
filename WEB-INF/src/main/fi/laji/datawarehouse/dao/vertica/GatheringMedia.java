package fi.laji.datawarehouse.dao.vertica;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;

@Entity
@Table(name="gathering_media")
public class GatheringMedia extends MediaBaseEntity {

	@Deprecated // for hibernate
	public GatheringMedia() {}

	public GatheringMedia(MediaObject media) {
		super(media);
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_key", referencedColumnName="key", insertable=false, updatable=false)
	public GatheringEntity getGathering() { // for queries only
		return null;
	}

	public void setGathering(@SuppressWarnings("unused") GatheringEntity gathering) {
		// for queries only
	}

}
