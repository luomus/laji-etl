package fi.laji.datawarehouse.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.dao.LookupNameProvider.BaseLookupNameProvider;
import fi.laji.datawarehouse.dao.LookupNameProvider.LookupName.Type;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo.SystemId;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class PersonLookupNameLoader extends BaseLookupNameProvider {

	private final DAO dao;

	public PersonLookupNameLoader(DAO dao) {
		this.dao = dao;
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created");
	}

	@Override
	public void refresh() {
		long startMoment = DateUtils.getCurrentEpoch();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting person lookup refresh... (start moment "+startMoment+")");
		try {
			List<LookupName> lookupNames = loadPersonToTempTableAndGenerateLookupStructure();
			setLookUpStructure(lookupNames);
		} catch (Exception e) {
			dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), ""+startMoment, e);
			throw new ETLException("Refreshing person lookup", e);
		}
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Person lookup refresh completed! (start moment "+startMoment+")");
	}

	private List<LookupName> loadPersonToTempTableAndGenerateLookupStructure() {
		Collection<PersonInfo> persons = dao.getPersons();
		List<LookupName> lookupNames = new ArrayList<>(persons.size()*2);
		for (PersonInfo person : persons) {
			try {
				addToLookUpNames(lookupNames, person, person.getId());
			}	catch (Exception e) {
				dao.logError(Const.LAJI_ETL_QNAME, this.getClass(), ""+person.getId(), e);
			}
		}
		return lookupNames;
	}

	private void addToLookUpNames(List<LookupName> lookupNames, PersonInfo personInfo, Qname personId) {
		add(lookupNames, personId.toURI(), personId, LookupName.Type.PRIMARY);
		add(lookupNames, personId.toString(), personId, LookupName.Type.PRIMARY);

		for (Map.Entry<SystemId, Collection<String>> e : personInfo.getSystemIds().entrySet()) {
			for (String userId : e.getValue()) {
				add(lookupNames, e.getKey().getUserIdPrefix()+userId, personId, LookupName.Type.PRIMARY);
			}
		}
		if (personInfo.getSystemIds().containsKey(SystemId.HATIKKA)) {
			for (String hatikkaId : personInfo.getSystemIds().get(SystemId.HATIKKA)) {
				if (hatikkaId.toLowerCase().contains("@hatikka.fi")) {
					add(lookupNames, hatikkaId, personId, LookupName.Type.PRIMARY); // hatikka ids also exist without prefix, ie instead of "hatikka:user@hatikka.fi" the document simply has "user@hatikka.fi"
				}
			}
		}

		if (personInfo.getSystemIds().containsKey(SystemId.INATURALIST)) {
			for (String inatId : personInfo.getSystemIds().get(SystemId.INATURALIST)) {
				add(lookupNames, "KE.901:"+inatId, personId, LookupName.Type.PRIMARY); // there may be userids in forms "inaturalist:1245" and "KE.901:12345" - link both forms
			}
		}

		try {
			String email = personInfo.getEmail();
			// Havikset.net linking is done via having same email address in both systems. TODO: implement another solution that allows user to use different emails and have the actual Havikset.net userid/email in MA.xxx profile
			add(lookupNames, "Havikset.net:"+email, personId, Type.PRIMARY);
		} catch (Exception e) {
			// missing email
		}
	}

	private void add(List<LookupName> lookupNames, String value, Qname id, LookupName.Type type) {
		if (given(value)) {
			lookupNames.add(new LookupName(value, id, type));
		}
	}

}