package fi.laji.datawarehouse.dao;

import java.io.Closeable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;

public class Streams {

	public interface ResultStream<T> extends Iterable<T>, Closeable {
		@Override void close();
	}

	public static <T> ResultStream<T> abort() {
		return new AbortedStream<>();
	}

	public static class AbortedStream<T> implements ResultStream<T> {
		@Override
		public Iterator<T> iterator() {
			throw new UnsupportedOperationException("This stream has been aborted");
		}

		@Override
		public void close() {}
	}

	public static class ResultSetStream<T> implements ResultStream<T> {

		public interface ToInstance<T> {
			T get(ResultSet rs) throws SQLException;
		}

		private final ResultSet rs;
		private final PreparedStatement p;
		private final TransactionConnection con;
		private final ToInstance<T> instantor;

		public ResultSetStream(ResultSet rs, PreparedStatement p, TransactionConnection con, ToInstance<T> instantor) throws Exception {
			rs.setFetchSize(4001);
			this.rs = rs;
			this.p = p;
			this.con = con;
			this.instantor = instantor;
		}

		@Override
		public Iterator<T> iterator() {
			return new Iterator<T>() {

				@Override
				public boolean hasNext() {
					try {
						return rs.next();
					} catch (SQLException e) {
						throw new ETLException(e);
					}
				}

				@Override
				public T next() {
					try {
						return instantor.get(rs);
					} catch (SQLException e) {
						throw new ETLException(e);
					}
				}};
		}

		@Override
		public void close() {
			Utils.close(p, rs, con);
		}

	}

	public static class ScrollableResultsStream<T> implements ResultStream<T> {

		private final ScrollableResults results;
		private final StatelessSession session;
		private final Class<T> typeOf;

		public ScrollableResultsStream(ScrollableResults results, StatelessSession session) {
			this.results = results;
			this.session = session;
			this.typeOf = null;
		}

		public ScrollableResultsStream(ScrollableResults results, StatelessSession session, Class<T> typeOf) {
			this.results = results;
			this.session = session;
			this.typeOf = typeOf;
		}

		@Override
		public Iterator<T> iterator() {
			return new Iterator<T>() {

				private boolean last = false;
				@Override
				public boolean hasNext() {
					if (results.next()) return true;
					last = true;
					return false;
				}

				@SuppressWarnings("unchecked")
				@Override
				public T next() {
					if (last) throw new IllegalArgumentException("Last result was already reached");
					if (typeOf == null) return (T) results.get();
					return (T) results.get(0);
				}};
		}

		@Override
		public void close() {
			try { results.close(); } catch (Exception e) {}
			try { session.close(); } catch (Exception e) {}
		}

	}

}
