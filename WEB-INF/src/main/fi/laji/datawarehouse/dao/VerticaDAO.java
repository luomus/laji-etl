package fi.laji.datawarehouse.dao;

import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;

public interface VerticaDAO {

	/**
	 * Save documents to warehouse. Deletes all documents and saves new versions. If document is marked deleted, it will not be saved.
	 * @param docs
	 * @param statusReporter
	 * @return
	 * @
	 */
	public int save(List<Document> docs, ThreadStatusReporter statusReporter) ;

	/**
	 * Delete document from warehouse
	 * @param document
	 * @param statusReporter
	 * @
	 */
	public void delete(Document document, ThreadStatusReporter statusReporter) ;

	/**
	 * Get dao for queries
	 * @return
	 */
	public VerticaQueryDAO getQueryDAO();
	
	/**
	 * Remove orphan linkings, historic data etc.
	 * @param statusReporter
	 * @
	 */
	public void performCleanUp(ThreadStatusReporter statusReporter) ;

	/**
	 * Call when closing application 
	 */
	public void close();

	/**
	 * Update geometry and geography tables for new/edited documents
	 */
	public void callGeoUpdate();

}
