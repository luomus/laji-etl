package fi.laji.datawarehouse.dao.oracle;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.utils.Util;

@Entity
@Table(name="out_pipe")
public class OutPipeMetaEntity {

	private long id;
	private String source;
	private String documentId;
	private String collectionId;
	private String namedPlaceId;
	private long inPipeId;
	private int deletion = 0;
	private int processed = 0;
	private int inError = 0;
	private int attemptCount = 0;
	private String errorMessage;
	private Date timestamp;

	public OutPipeMetaEntity() {}

	@Id
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@Column
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	@Column
	public String getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}
	@Column
	public String getNamedPlaceId() {
		return namedPlaceId;
	}
	public void setNamedPlaceId(String namedPlaceId) {
		this.namedPlaceId = namedPlaceId;
	}
	@Column
	public long getInPipeId() {
		return inPipeId;
	}
	public void setInPipeId(long inPipeId) {
		this.inPipeId = inPipeId;
	}
	@Column
	public int getDeletion() {
		return deletion;
	}
	public void setDeletion(int delete) {
		this.deletion = delete;
	}
	@Column
	public int getProcessed() {
		return processed;
	}
	public void setProcessed(int processed) {
		this.processed = processed;
	}
	@Column
	public int getInError() {
		return inError;
	}
	public void setInError(int inError) {
		this.inError = inError;
	}
	@Column
	public int getAttemptCount() {
		return attemptCount;
	}
	public void setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
	}
	@Column
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = Util.trimToByteLength(errorMessage, 4000);
	}
	@Column
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
