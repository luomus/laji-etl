package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.utils.ModelToJson;

@Entity
@Table(name="notification")
public class NotificationEntity {

	private long id;
	private String annotationId;
	private String personId;
	private String data;
	private String reason;
	private int sent = 0;

	public NotificationEntity() {}

	public NotificationEntity(AnnotationNotification notification) {
		setAnnotationId(notification.getAnnotation().getId().toString());
		setPersonId(notification.getPersonId().toString());
		setData(ModelToJson.toJson(notification.getAnnotation()).toString());
		setReason(notification.getNotificationReason() == null ? null : notification.getNotificationReason().name());
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getAnnotationId() {
		return annotationId;
	}
	public void setAnnotationId(String annotationId) {
		this.annotationId = annotationId;
	}
	@Column
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	@Column(columnDefinition="CLOB")
	@Lob
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Column
	public int getSent() {
		return sent;
	}
	public void setSent(int sent) {
		this.sent = sent;
	}
	@Column
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

}
