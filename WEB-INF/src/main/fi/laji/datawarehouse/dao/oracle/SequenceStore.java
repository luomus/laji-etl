package fi.laji.datawarehouse.dao.oracle;

import java.math.BigInteger;

import org.hibernate.Query;
import org.hibernate.StatelessSession;
import org.hibernate.type.StandardBasicTypes;

public class SequenceStore {

	private final int incrementBy;
	private final String sql;
	private long nextVal = -1;
	private long requests = 0;

	public SequenceStore(String sequenceName, int incrementBy) {
		this.incrementBy = incrementBy;
		this.sql = "SELECT " +sequenceName+".nextval AS value FROM dual";
	}

	public synchronized long getVal(StatelessSession ses) {
		requests++;
		if (requests > incrementBy) {
			nextVal = -1;
			requests = 1;
		}
		if (nextVal == -1) {
			long next = getValActual(ses);
			nextVal = next + 1;
			return next;
		}
		return nextVal++;
	}

	private long getValActual(StatelessSession ses) {
		Query query = ses.createSQLQuery(sql).addScalar("value", StandardBasicTypes.BIG_INTEGER);
		return ((BigInteger) query.uniqueResult()).longValue();
	}

}
