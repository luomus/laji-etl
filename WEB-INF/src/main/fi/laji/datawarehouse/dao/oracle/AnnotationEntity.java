package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="annotation")
public class AnnotationEntity {
	private String annotationId;
	private String documentId;
	private String data;
	private Long created;
	private Long inPipeEntryId;
	@Id
	@Column
	public String getAnnotationId() {
		return annotationId;
	}
	public void setAnnotationId(String annotationId) {
		this.annotationId = annotationId;
	}
	@Column
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	@Column
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Column
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	@Column
	public Long getInPipeEntryId() {
		return inPipeEntryId;
	}
	public void setInPipeEntryId(Long inPipeEntryId) {
		this.inPipeEntryId = inPipeEntryId;
	}
}