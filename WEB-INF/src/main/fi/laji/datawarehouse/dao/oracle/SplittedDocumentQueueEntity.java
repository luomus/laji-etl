package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="splitted_document_queue")
public class SplittedDocumentQueueEntity {

	private long id;
	private String originalDocumentId;
	private String source;
	private long expireTime;
	private String data;

	public SplittedDocumentQueueEntity() {}

	public SplittedDocumentQueueEntity(Qname source, Qname originalDocumentId, String data, long expireTime) {
		this.originalDocumentId = originalDocumentId.toURI();
		this.source = source.toString();
		this.expireTime = expireTime;
		this.data = data;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getOriginalDocumentId() {
		return originalDocumentId;
	}
	public void setOriginalDocumentId(String originalDocumentId) {
		this.originalDocumentId = originalDocumentId;
	}
	@Column
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@Column
	public long getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}
	@Column(columnDefinition="CLOB")
	@Lob
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

}
