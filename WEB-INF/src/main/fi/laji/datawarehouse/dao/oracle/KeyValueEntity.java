package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="storage")
public class KeyValueEntity {
	private String key;
	private String data;
	@Id
	@Column
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@Column(columnDefinition="CLOB")
	@Lob
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
