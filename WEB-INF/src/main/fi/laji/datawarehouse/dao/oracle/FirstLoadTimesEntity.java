package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="firstloaddates")
public class FirstLoadTimesEntity {

	private String documentId;
	private long time;

	public FirstLoadTimesEntity() {}

	public FirstLoadTimesEntity(Qname documentId, Long time) {
		setDocumentId(documentId.toURI());
		setTime(time);
	}

	@Id
	@Column
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	@Column
	public long getTime() {
		return time;
	}
	public void setTime(long firstLoadTime) {
		this.time = firstLoadTime;
	}

}
