package fi.laji.datawarehouse.dao.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariDataSource;

import fi.laji.datawarehouse.dao.ETLDAO.PipeSearchParams;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.Streams.ScrollableResultsStream;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.laji.datawarehouse.etl.models.containers.OriginalIds;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeStats;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.Utils;

public class Oracle {

	private static final int MAX_TIMESTAMP_PERSIST_INTERVAL = 1000 * 60 * 10; // 10 minutes
	private static final String MAX_TIMESTAMP = "maxTimestamp";
	private static final Object OUTPIPE_LOCK = new Object();

	private final SingleObjectCache<PipeStats> statsCache;
	private final Config config;
	private final HikariDataSource dataSource;
	private final SessionFactory sessionFactory;
	private final SequenceStore inPipeSeq = new SequenceStore("seq_in", 10000);
	private final SequenceStore outPipeSeq = new SequenceStore("seq_out", 10000);
	private final SequenceStore polygonSeq = new SequenceStore("seq_polygon", 100);

	public Oracle(Config config) {
		this.config = config;
		this.dataSource = OracleDataSourceDefinition.initDataSource(config.connectionDescription("Oracle"));
		this.sessionFactory = sessionFactory();
		this.statsCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<PipeStats>() {
			@Override
			public PipeStats load() {
				return updatePipeStatistics();
			}
		}, 1, TimeUnit.MINUTES);
	}

	public void close() {
		try { if (sessionFactory != null) sessionFactory.close(); } catch (Exception e) { }
		try { if (dataSource != null) dataSource.close(); } catch (Exception e) { }
	}

	private SessionFactory sessionFactory() {
		Configuration dataSchemaConfiguration = initConfiguration();
		return dataSchemaConfiguration
				.buildSessionFactory(
						new StandardServiceRegistryBuilder()
						.applySettings(dataSchemaConfiguration.getProperties())
						.applySetting(Environment.DATASOURCE, dataSource)
						.build());
	}

	private Configuration initConfiguration() {
		Configuration configuration = new Configuration()
				.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle12cDialect")
				.setProperty("hibernate.jdbc.batch_size", "50")
				.setProperty("hibernate.jdbc.fetch_size", "4001")
				.setProperty("hibernate.cache.use_second_level_cache", "false");
		if (config.developmentMode()) {
			configuration.setProperty("hibernate.show_sql", "true");
			configuration.setProperty("hibernate.format_sql", "true");
		} else {
			configuration.setProperty("hibernate.show_sql", "false");
		}
		addAnnotatedClasses(configuration);
		return configuration;
	}

	private void addAnnotatedClasses(Configuration configuration) {
		configuration
		.addAnnotatedClass(AnnotationEntity.class)
		.addAnnotatedClass(QueryLogEntity.class)
		.addAnnotatedClass(KeyValueEntity.class)
		.addAnnotatedClass(LajiGISPullReader.Occurrence.class)
		.addAnnotatedClass(LajiGISPullReader.OccurrenceGeo.class)
		.addAnnotatedClass(LajiGISPullReader.EventGeo.class)
		.addAnnotatedClass(LajiGISPullReader.EventProject.class)
		.addAnnotatedClass(LogEntry.class)
		.addAnnotatedClass(ReprocessTempEntity.class)
		.addAnnotatedClass(FirstLoadTimesEntity.class)
		.addAnnotatedClass(NotificationEntity.class)
		.addAnnotatedClass(SplittedDocumentIdEntity.class)
		.addAnnotatedClass(SplittedDocumentQueueEntity.class)
		.addAnnotatedClass(InPipeMetaEntity.class)
		.addAnnotatedClass(InPipeDataEntity.class)
		.addAnnotatedClass(OutPipeMetaEntity.class)
		.addAnnotatedClass(OutPipeDataEntity.class)
		.addAnnotatedClass(ApiKeyEntity.class)
		.addAnnotatedClass(PolygonEntity.class);
	}

	public StatelessSession openSession() {
		return sessionFactory.openStatelessSession();
	}

	public Connection openConnection() {
		try {
			return dataSource.getConnection();
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	// ANNOTATIONS --------------------------------------------------------------------

	public void storeAnnotation(Annotation annotation, long inPipeId) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			deleteAnnotation(annotation, session);
			insertAnnotation(annotation, inPipeId, session);
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Storing annotation " + annotation.getId(), e);
		}
	}

	private void insertAnnotation(Annotation annotation, long inPipeId, StatelessSession session) {
		AnnotationEntity e = new AnnotationEntity();
		e.setDocumentId(annotation.getRootID().toURI());
		e.setAnnotationId(annotation.getId().toURI());
		e.setData(ModelToJson.toJson(annotation).toString());
		e.setCreated(annotation.getCreatedTimestamp());
		e.setInPipeEntryId(inPipeId);
		session.insert(e);
	}

	private static final String DELETE_ANNOTATION_SQL = "DELETE FROM "+ AnnotationEntity.class.getName() +" a WHERE a.annotationId= :id";

	private void deleteAnnotation(Annotation annotation, StatelessSession session) {
		session.createQuery(DELETE_ANNOTATION_SQL).setString("id", annotation.getId().toURI()).executeUpdate();
	}

	public Long getExistingAnnotationInPipeId(Qname annotationId) {
		try (StatelessSession session = openSession()) {
			return (Long)
					session.createCriteria(AnnotationEntity.class)
					.add(Restrictions.eq("annotationId", annotationId.toURI()))
					.setProjection(Projections.property("inPipeEntryId"))
					.uniqueResult();
		} catch (Exception e) {
			throw new ETLException("Getting existing annotation inpipeid " + annotationId, e);
		}
	}

	public Set<Qname> getAnnotatedDocumentIds() {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<String> results =
			session.createCriteria(AnnotationEntity.class)
			.setProjection(Projections.distinct(Projections.property("documentId")))
			.list();
			return results.stream().map(id-> Qname.fromURI(id)).collect(Collectors.toSet());
		}
	}

	public Map<Qname, List<Annotation>> getAnnotations(Set<Qname> documentIds) {
		Map<Qname, List<Annotation>> annotations = new HashMap<>();
		if (documentIds.isEmpty()) return annotations;
		List<String> uris = documentIds.stream().map(d->d.toURI()).collect(Collectors.toList());
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			for (List<String> uriBatch : Lists.partition(uris, 800)) {
				getAnnotationsBatch(annotations, uriBatch, session);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Getting annotations", e);
		}
		return annotations;
	}

	private void getAnnotationsBatch(Map<Qname, List<Annotation>> annotations, List<String> uriBatch, StatelessSession session) throws CriticalParseFailure {
		@SuppressWarnings("unchecked")
		List<String> results =
		session.createCriteria(AnnotationEntity.class)
		.add(Restrictions.in("documentId", uriBatch))
		.addOrder(Order.asc("created"))
		.setProjection(Projections.property("data"))
		.list();
		for (String data : results) {
			Annotation annotation = JsonToModel.annotationFromJson(new JSONObject(data));
			if (!annotations.containsKey(annotation.getRootID())) {
				annotations.put(annotation.getRootID(), new ArrayList<>());
			}
			annotations.get(annotation.getRootID()).add(annotation);
		}
	}

	public List<Annotation> getTopAnnotations(int limit) {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<AnnotationEntity> results =
			session.createCriteria(AnnotationEntity.class)
			.addOrder(Order.desc("created"))
			.setMaxResults(limit)
			.list();
			List<Annotation> annotations = new ArrayList<>(results.size());
			for (AnnotationEntity e : results) {
				Annotation annotation = JsonToModel.annotationFromJson(new JSONObject(e.getData()));
				annotations.add(annotation);
			}
			return annotations;
		} catch (Exception e) {
			throw new ETLException("Getting top annotations", e);
		}
	}

	// QUERY LOG --------------------------------------------------------------------

	public void storeQueryLog(List<QueryLogEntity> queries) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			for (QueryLogEntity e : queries) {
				session.insert(e);
			}
			session.getTransaction().commit();
		}
	}

	// OCCURENCE COUNTS LOG --------------------------------------------------------------------

	public void logOccurrenceCounts(long privateCount, long publicCount) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createSQLQuery("INSERT INTO occurrence_count_log (private_count, public_count) VALUES (?, ?) ")
			.setLong(0, privateCount).setLong(1, publicCount).executeUpdate();
			session.getTransaction().commit();
		}
	}

	// KEY-VALUE-STORAGE --------------------------------------------------------------------

	public void persist(String key, String data) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			deleteKeyValue(key, session);
			insertKeyValue(key, data, session);
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Persist " + key, e);
		}
	}

	private void insertKeyValue(String key, String data, StatelessSession session) {
		KeyValueEntity e = new KeyValueEntity();
		e.setKey(key);
		e.setData(data);
		session.insert(e);
	}

	private static final String DELETE_KEYVALUE_SQL = "DELETE FROM "+ KeyValueEntity.class.getName() +" a WHERE a.key= :key";

	private void deleteKeyValue(String key, StatelessSession session) {
		session.createQuery(DELETE_KEYVALUE_SQL).setString("key", key).executeUpdate();
	}

	public String getPersisted(String key) {
		try (StatelessSession session = openSession()) {
			return (String) session.createCriteria(KeyValueEntity.class)
					.add(Restrictions.eq("key", key))
					.setProjection(Projections.property("data"))
					.uniqueResult();
		} catch (Exception e) {
			throw new ETLException("Get peristed " + key, e);
		}
	}

	// SYSTEM LOG --------------------------------------------------------------------

	private int logCount = 0;

	public void log(LogEntry entry) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.insert(entry);
			session.getTransaction().commit();
			if (logCount++ > 100) {
				purgeLog(session);
				logCount = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static long LOG_AGE_MILLIS = 1000L * 60 * 60 * 24 * 7;

	private void purgeLog(StatelessSession session) {
		session.beginTransaction();
		long timestamp = System.currentTimeMillis() - LOG_AGE_MILLIS;
		session.createSQLQuery("DELETE FROM log WHERE timestamp < " + timestamp).executeUpdate();
		session.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<LogEntry> getLogEntries() {
		try (StatelessSession session = openSession()) {
			return session.createCriteria(LogEntry.class)
					.setMaxResults(1000)
					.addOrder(Order.desc("id")).list();
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	// REPROCESS TEMP --------------------------------------------------------------------

	public void clearReprocessDocumentIds() {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createSQLQuery("TRUNCATE TABLE reprocess_temp").executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	public void storeReprocessDocumentIds(Iterable<String> documentIds) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			int i = 1;
			for (String documentId : documentIds) {
				session.insert(new ReprocessTempEntity(i++, documentId));
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	// NOTIFICATIONS --------------------------------------------------------------------

	public void storeNotification(AnnotationNotification notification) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			if (exists(notification, session)) return;
			session.insert(new NotificationEntity(notification));
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Storing notification for annotation " + notification.getAnnotation().getId() + " for person " + notification.getPersonId(), e);
		}
	}

	private boolean exists(AnnotationNotification notification, StatelessSession session) {
		return (long) session.createCriteria(NotificationEntity.class)
				.add(Restrictions.eq("annotationId", notification.getAnnotation().getId().toString()))
				.add(Restrictions.eq("personId", notification.getPersonId().toString()))
				.setProjection(Projections.rowCount()).uniqueResult() > 0;
	}

	private static final String UPDATE_NOTIFICATION_SQL = "UPDATE "+ NotificationEntity.class.getName() +" a SET a.sent = 1 WHERE a.id= :id";

	public void markNotificationSent(long id) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(UPDATE_NOTIFICATION_SQL).setLong("id", id).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Marking notification sent " + id, e);
		}
	}

	public List<AnnotationNotification> getUnsentNotifications() {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<NotificationEntity> results = session.createCriteria(NotificationEntity.class)
			.add(Restrictions.eq("sent", 0)).list();
			return toNotifications(results, false);
		} catch (Exception e) {
			throw new ETLException("Get unsent notifications", e);
		}
	}

	private static final String DELETE_NOTIFICATION_SQL = "DELETE FROM "+ NotificationEntity.class.getName() +" a WHERE a.sent = 0 AND a.annotationId= :id";

	public void removeUnsentNotifications(Qname annotationId) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(DELETE_NOTIFICATION_SQL).setString("id", annotationId.toString()).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Removing unsent notifications for " + annotationId.toString(), e);
		}
	}

	public List<AnnotationNotification> getTopSentNotifications(int limit) {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<NotificationEntity> results = session.createCriteria(NotificationEntity.class)
			.add(Restrictions.eq("sent", 1))
			.addOrder(Order.desc("id"))
			.setMaxResults(limit)
			.list();
			return toNotifications(results, true);
		} catch (Exception e) {
			throw new ETLException("Get unsent notifications", e);
		}
	}

	private List<AnnotationNotification> toNotifications(List<NotificationEntity> results, boolean allowInvalid) throws CriticalParseFailure {
		List<AnnotationNotification> notifications = new ArrayList<>(results.size());
		for (NotificationEntity e : results) {
			AnnotationNotification notification = new AnnotationNotification(
					e.getId(),
					new Qname(e.getPersonId()),
					getAnnotation(e, allowInvalid),
					e.getReason() == null ? null : NotificationReason.valueOf(e.getReason()));
			notifications.add(notification);
		}
		return notifications;
	}

	private Annotation getAnnotation(NotificationEntity e, boolean allowInvalid) throws CriticalParseFailure {
		if (!allowInvalid) {
			return getAnnotation(e);
		}
		try {
			return getAnnotation(e);
		} catch (Exception ex) {
			try {
				return new Annotation(new Qname(e.getAnnotationId()), new Qname("ERROR-LEGACY DATA"), new Qname("ERROR-LEGACY DATA"), 0L);
			} catch (CriticalParseFailure ex2) {
				throw new IllegalStateException("Impossible state");
			}
		}
	}

	private Annotation getAnnotation(NotificationEntity e) throws CriticalParseFailure {
		return JsonToModel.annotationFromJson(new JSONObject(e.getData()));
	}

	// SPLITTED IDS --------------------------------------------------------------------

	public void storeGeneratedSplittedIds(Qname originalDocumentId, Map<Qname, Qname> originalUnitIdTogeneratedSplittedDocumentIdId) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			for (Map.Entry<Qname, Qname> e : originalUnitIdTogeneratedSplittedDocumentIdId.entrySet()) {
				Qname originalUnitId = e.getKey();
				Qname splittedDocumentId = e.getValue();
				session.insert(new SplittedDocumentIdEntity(originalDocumentId, originalUnitId, splittedDocumentId));
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Storing splitted ids " + originalDocumentId, e);
		}
	}

	public SplittedDocumentIds getSplittedDocumentIds(Set<Qname> originalDocumentIds) {
		SplittedDocumentIds splitted = new SplittedDocumentIds();
		if (originalDocumentIds.isEmpty()) return splitted;
		try (StatelessSession session = openSession()) {
			List<String> ids = originalDocumentIds.stream().map(id->id.toURI()).collect(Collectors.toList());
			@SuppressWarnings("unchecked")
			List<SplittedDocumentIdEntity> results = session.createCriteria(SplittedDocumentIdEntity.class)
			.add(Restrictions.in("originalDocumentId", ids)).list();
			for (SplittedDocumentIdEntity e : results) {
				Qname originalDocumentId = Qname.fromURI(e.getOriginalDocumentId());
				Qname originalUnitId = Qname.fromURI(e.getOriginalUnitId());
				Qname splittedDocumentId = Qname.fromURI(e.getSplittedDocumentId());
				splitted.add(originalDocumentId, originalUnitId, splittedDocumentId);
			}
			return splitted;
		} catch (Exception e) {
			throw new ETLException("Getting splitted document ids for " + originalDocumentIds, e);
		}
	}

	public OriginalIds getOriginalIdsOfSplittedDocument(Qname splittedDocumentId) {
		try (StatelessSession session = openSession()) {
			SplittedDocumentIdEntity e = (SplittedDocumentIdEntity) session.createCriteria(SplittedDocumentIdEntity.class)
					.add(Restrictions.eq("splittedDocumentId", splittedDocumentId.toURI())).uniqueResult();
			if (e == null) return null;
			Qname documentId = Qname.fromURI(e.getOriginalDocumentId());
			Qname unitId = Qname.fromURI(e.getOriginalUnitId());
			return new OriginalIds(documentId, unitId);
		} catch (Exception e) {
			throw new ETLException("Getting original ids for " + splittedDocumentId, e);
		}
	}


	public Map<String, SplittedDocumentIdEntity> getAllSplittedIdToOriginalId() {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<SplittedDocumentIdEntity> results = session.createCriteria(SplittedDocumentIdEntity.class).list();
			Map<String, SplittedDocumentIdEntity> map = new HashMap<>();
			for (SplittedDocumentIdEntity e : results) {
				map.put(e.getSplittedDocumentId(), e);
			}
			return map;
		} catch (Exception e) {
			throw new ETLException("Getting all splitted ids", e);
		}
	}

	// SPLITTED DOCUMENT QUEUE --------------------------------------------------------------------

	public void storeSplittedDocumentToQueue(Qname source, Qname originalDocumentId, String data, long expireEpochSeconds) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.insert(new SplittedDocumentQueueEntity(source, originalDocumentId, data, expireEpochSeconds));
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Storing splitted documents to queue: " + Utils.debugS(source, originalDocumentId, expireEpochSeconds), e);
		}
	}

	public List<QueueData> getExpiredSplittedDocumentsFromQueue(Qname source) {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<SplittedDocumentQueueEntity> results = session.createCriteria(SplittedDocumentQueueEntity.class)
			.add(Restrictions.lt("expireTime", DateUtils.getCurrentEpoch()))
			.add(Restrictions.eq("source", source.toString()))
			.addOrder(Order.asc("expireTime"))
			.setMaxResults(100).list();
			return toQueueData(results);
		} catch (Exception e) {
			throw new ETLException("Getting expired splitted documents for " + source, e);
		}
	}

	private List<QueueData> toQueueData(List<SplittedDocumentQueueEntity> results) {
		List<QueueData> data = new ArrayList<>();
		for (SplittedDocumentQueueEntity e : results) {
			data.add(new QueueData(e.getId(), new Qname(e.getSource()), e.getExpireTime(), e.getData()));
		}
		return data;
	}

	private static final String DELETE_SPLITTED_Q_BY_DOCID_SQL = "DELETE FROM "+ SplittedDocumentQueueEntity.class.getName() +" a WHERE a.originalDocumentId IN (:ids)";

	public void removeSplittedDocumentsFromQueue(Set<Qname> originalDocumentIds) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			List<String> ids = originalDocumentIds.stream().map(id->id.toURI()).collect(Collectors.toList());
			session.createQuery(DELETE_SPLITTED_Q_BY_DOCID_SQL).setParameterList("ids", ids).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Removing splitted documents from queue for " + originalDocumentIds, e);
		}
	}

	private static final String DELETE_SPLITTED_Q_BY_ID_SQL = "DELETE FROM "+ SplittedDocumentQueueEntity.class.getName() +" a WHERE a.id IN (:ids)";

	public void removeSplittedQueue(List<Long> ids) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(DELETE_SPLITTED_Q_BY_ID_SQL).setParameterList("ids", ids).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Removing splitted documents from queue by ids " + ids, e);
		}
	}

	public List<QueueData> getTopSplittedDocumentsFromQueue(int limit) {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<SplittedDocumentQueueEntity> results = session.createCriteria(SplittedDocumentQueueEntity.class)
			.addOrder(Order.desc("id"))
			.setMaxResults(limit)
			.list();
			return toQueueData(results);
		} catch (Exception e) {
			throw new ETLException("Getting top splitted documents", e);
		}
	}

	// FIRST LOADED TIMES --------------------------------------------------------------------

	public void storeFirstLoadedTimes(Map<Qname, Long> firstLoadTimesOfDocumentIds) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			for (Map.Entry<Qname, Long> e : firstLoadTimesOfDocumentIds.entrySet()) {
				session.insert(new FirstLoadTimesEntity(e.getKey(), e.getValue()));
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	public Map<Qname, Long> getFirstLoadTimes(Set<Qname> documentIds) {
		Map<Qname, Long> times = new HashMap<>();
		List<String> uris = documentIds.stream().map(d->d.toURI()).collect(Collectors.toList());
		try (StatelessSession session = openSession()) {
			for (List<String> uriBatch : Lists.partition(uris, 800)) {
				getFirstLoadTimesBatch(times, uriBatch, session);
			}
		} catch (Exception e) {
			throw new ETLException("Getting first load times for " + documentIds, e);
		}
		return times;
	}

	private void getFirstLoadTimesBatch(Map<Qname, Long> times, List<String> uriBatch, StatelessSession session) {
		@SuppressWarnings("unchecked")
		List<FirstLoadTimesEntity> results =
		session.createCriteria(FirstLoadTimesEntity.class)
		.add(Restrictions.in("documentId", uriBatch))
		.list();
		for (FirstLoadTimesEntity e : results) {
			times.put(Qname.fromURI(e.getDocumentId()), e.getTime());
		}
	}

	// IN/OUT PIPES --------------------------------------------------------------------

	public void storeToInPipe(Qname source, String content, String contentType) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			InPipeMetaEntity meta = createMeta(source, session, 0);
			session.insert(meta);
			session.insert(createData(content, contentType, meta));
			session.getTransaction().commit();
		}
	}

	private InPipeDataEntity createData(String content, String contentType, InPipeMetaEntity meta) {
		InPipeDataEntity data = new InPipeDataEntity();
		data.setId(meta.getId());
		data.setContentType(contentType);
		data.setData(content);
		data.setSource(meta.getSource());
		return data;
	}

	private InPipeMetaEntity createMeta(Qname source, StatelessSession session, int processedStatus) {
		InPipeMetaEntity meta = new InPipeMetaEntity();
		meta.setId(inPipeSeq.getVal(session));
		meta.setSource(source.toString());
		meta.setProcessed(processedStatus);
		meta.setTimestamp(now());
		return meta;
	}

	private Date now() {
		return new Date();
	}

	public void storeToInPipe(Qname source, List<String> contents, String contentType) {
		storeToInPipe(source, contents, contentType, 0);
	}

	public void storeToInPipe(Qname source, List<String> contents, String contentType, int processedStatus) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			List<InPipeMetaEntity> metas = new ArrayList<>(contents.size());
			List<InPipeDataEntity> datas = new ArrayList<>(contents.size());
			for (String content : contents) {
				InPipeMetaEntity meta = createMeta(source, session, processedStatus);
				metas.add(meta);
				datas.add(createData(content, contentType, meta));
			}
			for (InPipeMetaEntity meta : metas) {
				session.insert(meta);
			}
			for (InPipeDataEntity data : datas) {
				session.insert(data);
			}
			session.getTransaction().commit();
		}
	}

	private static final String INPIPE_RELEASE_HOLD_SQL = "UPDATE "+ InPipeMetaEntity.class.getName() +" a SET a.processed = 0 WHERE a.processed=-1 AND a.source=:source ";

	public void releaseInPipeHold(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(INPIPE_RELEASE_HOLD_SQL)
			.setParameter("source", source.toString())
			.executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Releasing hold "  + InPipeMetaEntity.class.getName() + " failed", e);
		}
	}

	/**
	 * Stores to out pipe if newer version of document does not exist
	 * @param data map document uri -> data
	 * @return true if caused changes
	 */
	public boolean storeToOutPipe(Map<String, OutPipeData> data) {
		if (data.size() > 100) throw new IllegalArgumentException("Max size of out pipe batch is 100");
		synchronized (OUTPIPE_LOCK) {
			try (StatelessSession session = openSession()) {
				return storeToOutPipe(data, session);
			}
		}
	}

	private boolean storeToOutPipe(Map<String, OutPipeData> data, StatelessSession session) {
		session.beginTransaction();

		List<Long> deletes = new ArrayList<>(data.size());
		List<OutPipeData> inserts = new ArrayList<>(data.size());
		resolveInsertsDeletes(data, session, deletes, inserts);

		if (inserts.isEmpty()) return false;

		boolean insertedNonDeletion = storeToOutPipe(deletes, inserts, session);
		session.getTransaction().commit();

		if (insertedNonDeletion) {
			persistMaxTimestamp();
		}

		return true;
	}

	private void resolveInsertsDeletes(Map<String, OutPipeData> data, StatelessSession session, List<Long> deletes, List<OutPipeData> inserts) {
		Map<String, OutPipeMetaEntity> existingByDocumentId = getExistingIds(data.keySet(), session);
		for (OutPipeData d : data.values()) {
			OutPipeMetaEntity existing = existingByDocumentId.get(d.getDocumentId().toURI());
			if (existing == null) {
				inserts.add(d);
			} else {
				if (existing.getInPipeId() <= d.getInPipeId()) {
					deletes.add(existing.getId());
					inserts.add(d);
				}
			}
		}
	}

	private Map<String, OutPipeMetaEntity> getExistingIds(Set<String> documentUris, StatelessSession session) {
		@SuppressWarnings("unchecked")
		List<Object[]> results =
		session.createCriteria(OutPipeMetaEntity.class)
		.setProjection(Projections.projectionList()
				.add(Projections.property("id"))
				.add(Projections.property("documentId"))
				.add(Projections.property("inPipeId")))
		.add(Restrictions.in("documentId", documentUris))
		.list();
		return results.stream()
				.map(r->toOutPipeMetaEntity(r))
				.collect(Collectors.toMap(OutPipeMetaEntity::getDocumentId, Function.identity()));
	}

	private OutPipeMetaEntity toOutPipeMetaEntity(Object[] row) {
		OutPipeMetaEntity meta = new OutPipeMetaEntity();
		meta.setId((Long) row[0]);
		meta.setDocumentId((String) row[1]);
		if (row.length == 3) {
			meta.setInPipeId((Long) row[2]);
		}
		return meta;
	}

	private boolean storeToOutPipe(List<Long> deletes, List<OutPipeData> inserts, StatelessSession session) {
		removeOutPipe(deletes, session);
		List<OutPipeMetaEntity> metas = new ArrayList<>(inserts.size());
		List<OutPipeDataEntity> datas = new ArrayList<>(inserts.size());
		boolean insertedNonDeletion = false;
		for (OutPipeData d : inserts) {
			OutPipeMetaEntity meta = createMeta(d, session);
			metas.add(meta);
			datas.add(createData(d.getData(), meta));
			if (!d.isDeletion()) insertedNonDeletion = true;
		}
		for (OutPipeMetaEntity meta : metas) {
			session.insert(meta);
		}
		for (OutPipeDataEntity data : datas) {
			session.insert(data);
		}
		return insertedNonDeletion;
	}

	private long previouspPersistMaxTimestamp = 0;

	private void persistMaxTimestamp() {
		long now = System.currentTimeMillis();
		if (shouldPersistMaxTimestamp(now)) {
			persist(MAX_TIMESTAMP, Long.toString(now));
			previouspPersistMaxTimestamp = now;
		}
	}

	private boolean shouldPersistMaxTimestamp(long now) {
		long diff = now - previouspPersistMaxTimestamp;
		return diff > MAX_TIMESTAMP_PERSIST_INTERVAL;
	}

	private OutPipeMetaEntity createMeta(OutPipeData data, StatelessSession session) {
		OutPipeMetaEntity meta = new OutPipeMetaEntity();
		meta.setId(outPipeSeq.getVal(session));
		meta.setSource(data.getSource().toString());
		meta.setDocumentId(data.getDocumentId().toURI());
		if (data.getCollectionId() != null)
			meta.setCollectionId(data.getCollectionId().toURI());
		if (data.getNamedPlaceId() != null)
			meta.setNamedPlaceId(data.getNamedPlaceId().toURI());
		meta.setDeletion(data.isDeletion() ? 1 : 0);
		meta.setInPipeId(data.getInPipeId());
		meta.setAttemptCount(data.getAttemptCount());
		meta.setTimestamp(now());
		return meta;
	}

	private OutPipeDataEntity createData(String content, OutPipeMetaEntity meta) {
		OutPipeDataEntity data = new OutPipeDataEntity();
		data.setId(meta.getId());
		data.setSource(meta.getSource());
		data.setData(content);
		return data;
	}

	public Set<Qname> getUnprosessedSources() {
		Set<Qname> sources = getUnprosessedSources(InPipeMetaEntity.class);
		sources.addAll(getUnprosessedSources(OutPipeMetaEntity.class));
		sources.addAll(getUnprosessedSourcesFromSplittedQueue());
		return sources;
	}

	private Set<Qname> getUnprosessedSources(Class<?> pipeClass) {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<String> sources =
			session.createCriteria(pipeClass)
			.setProjection(Projections.distinct(Projections.property("source")))
			.add(Restrictions.eq("processed", 0)).list();
			return sources.stream().map(s->new Qname(s)).collect(Collectors.toSet());
		}
	}

	private Set<Qname> getUnprosessedSourcesFromSplittedQueue() {
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<String> sources =
			session.createCriteria(SplittedDocumentQueueEntity.class)
			.setProjection(Projections.distinct(Projections.property("source")))
			.list();
			return sources.stream().map(s->new Qname(s)).collect(Collectors.toSet());
		}
	}

	public List<InPipeData> getUnprocessedNotErroneousInOrderFromInPipe(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Object[]> results =
			session.createCriteria(InPipeMetaEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.property("id"))
					.add(Projections.property("attemptCount")))
			.add(Restrictions.eq("source", source.toString()))
			.add(Restrictions.eq("processed", 0))
			.add(Restrictions.eq("inError", 0))
			.add(Restrictions.eq("unrecoverableError", 0))
			.addOrder(Order.asc("attemptCount"))
			.addOrder(Order.desc("id"))
			.setMaxResults(10).list();
			List<InPipeData> data = getFromInPipe(session, toInPipeMetaEntity(results), source);
			session.getTransaction().commit();
			return data;
		}
	}

	private List<InPipeMetaEntity> toInPipeMetaEntity(List<Object[]> results) {
		return results.stream().map(o->toInPipeMetaEntity(o)).collect(Collectors.toList());
	}

	private InPipeMetaEntity toInPipeMetaEntity(Object[] row) {
		InPipeMetaEntity meta = new InPipeMetaEntity();
		meta.setId((Long) row[0]);
		meta.setAttemptCount((Integer) row[1]);
		return meta;
	}

	private List<InPipeData> getFromInPipe(StatelessSession session, List<InPipeMetaEntity> metas, Qname source) {
		if (metas.isEmpty()) return Collections.emptyList();
		List<Long> ids = metas.stream().map(m->m.getId()).collect(Collectors.toList());
		@SuppressWarnings("unchecked")
		List<InPipeDataEntity> datas =
		session.createCriteria(InPipeDataEntity.class)
		.add(Restrictions.in("id", ids)).list();
		Map<Long, InPipeDataEntity> dataById = datas.stream().collect(Collectors.toMap(InPipeDataEntity::getId, Function.identity()));
		List<InPipeData> finalPipeDatas = new ArrayList<>(metas.size());
		for (InPipeMetaEntity meta : metas) {
			InPipeDataEntity data = dataById.get(meta.getId());
			if (data == null) continue; // Very rarely we get reads where the in_pipe is stored but in_pipe_data is not yet stored; skip these rows
			InPipeData finalPipeData = new InPipeData(meta.getId(), source, data.getData(), data.getContentType(), meta.getTimestamp(), meta.getErrorMessage(), false);
			finalPipeData.setAttemptCount(meta.getAttemptCount());
			finalPipeDatas.add(finalPipeData);
		}
		return finalPipeDatas;
	}

	public List<OutPipeData> getUnprocessedNotErroneousInOrderFromOutPipe(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Object[]> results =
			session.createCriteria(OutPipeMetaEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.property("id"))
					.add(Projections.property("documentId")))
			.add(Restrictions.eq("source", source.toString()))
			.add(Restrictions.eq("processed", 0))
			.add(Restrictions.eq("inError", 0))
			.addOrder(Order.asc("attemptCount"))
			.addOrder(Order.desc("id"))
			.setMaxResults(100).list();
			List<OutPipeData> data = getFromOutPipe(session, toOutPipeMetaEntity(results));
			session.getTransaction().commit();
			return data;
		}
	}

	private List<OutPipeMetaEntity> toOutPipeMetaEntity(List<Object[]> results) {
		return results.stream().map(o->toOutPipeMetaEntity(o)).collect(Collectors.toList());
	}

	private List<OutPipeData> getFromOutPipe(StatelessSession session, List<OutPipeMetaEntity> metas) {
		if (metas.isEmpty()) return Collections.emptyList();
		List<Long> ids = metas.stream().map(m->m.getId()).collect(Collectors.toList());
		@SuppressWarnings("unchecked")
		List<OutPipeDataEntity> datas =
		session.createCriteria(OutPipeDataEntity.class)
		.add(Restrictions.in("id", ids)).list();
		Map<Long, OutPipeDataEntity> dataById = datas.stream().collect(Collectors.toMap(OutPipeDataEntity::getId, Function.identity()));
		List<OutPipeData> finalPipeDatas = new ArrayList<>(metas.size());
		for (OutPipeMetaEntity meta : metas) {
			OutPipeDataEntity data = dataById.get(meta.getId());
			if (data == null) continue; // Very rarely we get reads where the out_pipe is stored but out_pipe_data is not yet stored; skip these rows
			OutPipeData finalPipeData = new OutPipeData(
					meta.getId(),
					meta.getInPipeId(),
					new Qname(meta.getSource()),
					Qname.fromURI(meta.getDocumentId()),
					getCollectionId(meta),
					getNamedPlaceId(meta),
					data.getData(),
					meta.getDeletion() == 1,
					meta.getTimestamp(),
					meta.getErrorMessage());
			finalPipeDatas.add(finalPipeData);
		}
		return finalPipeDatas;
	}

	private Qname getNamedPlaceId(OutPipeMetaEntity meta) {
		return meta.getNamedPlaceId() == null ? null : Qname.fromURI(meta.getNamedPlaceId());
	}

	private Qname getCollectionId(OutPipeMetaEntity meta) {
		return meta.getCollectionId() == null ? null : Qname.fromURI(meta.getCollectionId());
	}

	public List<InPipeData> getFailedAttemptDataFromInPipe(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Object[]> results =
			session.createCriteria(InPipeMetaEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.property("id"))
					.add(Projections.property("attemptCount")))
			.add(Restrictions.eq("source", source.toString()))
			.add(Restrictions.eq("processed", 0))
			.add(Restrictions.eq("inError", 1))
			.add(Restrictions.eq("unrecoverableError", 0))
			.addOrder(Order.asc("attemptCount"))
			.addOrder(Order.desc("id"))
			.setMaxResults(1).list();
			List<InPipeData> data = getFromInPipe(session, toInPipeMetaEntity(results), source);
			session.getTransaction().commit();
			return data;
		}
	}

	public List<OutPipeData> getFailedAttemptDataFromOutPipe(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Object[]> results =
			session.createCriteria(OutPipeMetaEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.property("id"))
					.add(Projections.property("documentId")))
			.add(Restrictions.eq("source", source.toString()))
			.add(Restrictions.eq("processed", 0))
			.add(Restrictions.eq("inError", 1))
			.add(Restrictions.lt("attemptCount", 2))
			.addOrder(Order.asc("attemptCount"))
			.addOrder(Order.desc("id"))
			.setMaxResults(40).list();
			List<OutPipeData> data = getFromOutPipe(session, toOutPipeMetaEntity(results));
			session.getTransaction().commit();
			return data;
		}
	}

	public OutPipeData getSingleMultiAttemptedDataFromOutPipe(Qname source) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Object[]> results =
			session.createCriteria(OutPipeMetaEntity.class)
			.setProjection(Projections.projectionList()
					.add(Projections.property("id"))
					.add(Projections.property("documentId")))
			.add(Restrictions.eq("source", source.toString()))
			.add(Restrictions.eq("processed", 0))
			.add(Restrictions.eq("inError", 1))
			.addOrder(Order.asc("attemptCount"))
			.addOrder(Order.desc("id"))
			.setMaxResults(1).list();
			OutPipeData data = getSingleFromOutPipe(session, toOutPipeMetaEntity(results));
			session.getTransaction().commit();
			return data;
		}
	}

	public InPipeData getFromInPipe(long id) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<InPipeMetaEntity> meta =
			session.createCriteria(InPipeMetaEntity.class)
			.add(Restrictions.eq("id", id)).list();
			InPipeData data = getSingleFromInPipe(session, meta);
			session.getTransaction().commit();
			return data;
		}
	}

	private InPipeData getSingleFromInPipe(StatelessSession session, List<InPipeMetaEntity> meta) {
		if (meta.isEmpty()) return null;
		if (meta.size() != 1) throw new IllegalStateException("Should ask for single");
		List<InPipeData> list = getFromInPipe(session, meta, new Qname(meta.get(0).getSource()));
		if (list.isEmpty()) return null;
		return list.get(0);
	}

	public OutPipeData getFromOutPipe(long id) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<OutPipeMetaEntity> meta =
			session.createCriteria(OutPipeMetaEntity.class)
			.add(Restrictions.eq("id", id)).list();
			OutPipeData data = getSingleFromOutPipe(session, meta);
			session.getTransaction().commit();
			return data;
		}
	}

	public OutPipeData getFromOutPipe(Qname documentId) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<OutPipeMetaEntity> meta =
			session.createCriteria(OutPipeMetaEntity.class)
			.add(Restrictions.eq("documentId", documentId.toURI())).list();
			OutPipeData data = getSingleFromOutPipe(session, meta);
			session.getTransaction().commit();
			return data;
		}
	}

	private OutPipeData getSingleFromOutPipe(StatelessSession session, List<OutPipeMetaEntity> meta) {
		List<OutPipeData> list = getFromOutPipe(session, meta);
		if (list.isEmpty()) return null;
		return list.get(0);
	}

	public List<InPipeData> getFromInPipeForSource(Qname source, PipeSearchParams searchParams) {
		try (StatelessSession session = openSession()) {
			Criteria c = createPipeSearchQuery(source, searchParams, session, InPipeMetaEntity.class);
			@SuppressWarnings("unchecked")
			List<InPipeMetaEntity> metas = c.list();
			if (metas.isEmpty()) return Collections.emptyList();
			List<InPipeData> finalPipeDatas = new ArrayList<>(metas.size());
			for (InPipeMetaEntity meta : metas) {
				InPipeData finalPipeData = new InPipeData(meta.getId(), new Qname(meta.getSource()), null, null, meta.getTimestamp(), meta.getErrorMessage(), isUnrecoverableError(meta));
				finalPipeDatas.add(finalPipeData);
			}
			return finalPipeDatas;
		}
	}

	private Criteria createPipeSearchQuery(Qname source, PipeSearchParams searchParams, StatelessSession session, Class<?> pipeClass) {
		Criteria c = session.createCriteria(pipeClass)
				.add(Restrictions.eq("source", source.toString()));
		addPipeRestrictions(searchParams, c);
		c.addOrder(Order.desc("id"))
		.setMaxResults(100);
		return c;
	}

	private void addPipeRestrictions(PipeSearchParams searchParams, Criteria c) {
		if (searchParams == PipeSearchParams.WAITING) {
			c.add(Restrictions.le("processed", 0));
			c.add(Restrictions.eq("inError", 0));
		} else if (searchParams == PipeSearchParams.IN_ERROR) {
			c.add(Restrictions.eq("processed", 0));
			c.add(Restrictions.eq("inError", 1));
		} else if (searchParams == PipeSearchParams.PROCESSED) {
			c.add(Restrictions.eq("processed", 1));
		} else if (searchParams == PipeSearchParams.DELETED) {
			c.add(Restrictions.eq("processed", 1));
			c.add(Restrictions.eq("inError", 0));
			c.add(Restrictions.eq("attemptCount", 0));
			c.add(Restrictions.eq("deletion", 1));
		}
	}

	private boolean isUnrecoverableError(InPipeMetaEntity meta) {
		return meta.getUnrecoverableError() > 0;
	}

	public List<OutPipeData> getFromOutPipeForSource(Qname source, PipeSearchParams searchParams) {
		try (StatelessSession session = openSession()) {
			Criteria c = createPipeSearchQuery(source, searchParams, session, OutPipeMetaEntity.class);
			@SuppressWarnings("unchecked")
			List<OutPipeMetaEntity> metas = c.list();
			if (metas.isEmpty()) return Collections.emptyList();
			List<OutPipeData> finalPipeDatas = new ArrayList<>(metas.size());
			for (OutPipeMetaEntity meta : metas) {
				OutPipeData finalPipeData = new OutPipeData(
						meta.getId(),
						meta.getInPipeId(),
						source,
						Qname.fromURI(meta.getDocumentId()),
						null,
						null,
						null,
						meta.getDeletion() == 1,
						meta.getTimestamp(),
						meta.getErrorMessage());
				finalPipeDatas.add(finalPipeData);
			}
			return finalPipeDatas;
		}
	}

	public void reportPipeDataProcessed(List<? extends PipeData> data) {
		if (data.isEmpty()) return;
		List<Long> ids = getIds(data);
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(getPipeProcessedSql(data))
			.setParameterList("ids", ids).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Marking "  + getPipeClass(data) + " processed " + ids, e);
		}
	}

	private List<Long> getIds(List<? extends PipeData> data) {
		return data.stream().map(d->d.getId()).collect(Collectors.toList());
	}

	private static final String OUTPIPE_PROCESSED_SQL = "UPDATE " + OutPipeMetaEntity.class.getName() + " a SET a.attemptCount = 0, processed = 1, inError = 0, errorMessage = null, timestamp = SYSDATE WHERE a.id IN (:ids)";
	private static final String INPIPE_PROCESSED_SQL  = "UPDATE " + InPipeMetaEntity.class.getName()  + " a SET a.attemptCount = 0, processed = 1, inError = 0, errorMessage = null, timestamp = SYSDATE WHERE a.id IN (:ids)";

	private String getPipeProcessedSql(List<? extends PipeData> data) {
		Object o = data.get(0);
		if (o instanceof InPipeData) {
			return INPIPE_PROCESSED_SQL;
		}
		return OUTPIPE_PROCESSED_SQL;
	}

	private String getPipeClass(List<? extends PipeData> data) {
		Object o = data.get(0);
		if (o instanceof InPipeData) return InPipeMetaEntity.class.getName();
		return OutPipeMetaEntity.class.getName();
	}

	public void reportPipeDataAttempted(List<? extends PipeData> data, String errorMessage) {
		if (data.isEmpty()) return;
		List<Long> ids = getIds(data);
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(getPipeAttemptedSql(data))
			.setParameter("message", Util.trimToByteLength(errorMessage, 4000))
			.setParameterList("ids", ids).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Marking "  + getPipeClass(data) + " attempted " + ids, e);
		}
	}

	private static final String OUTPIPE_ATTEMPTED_SQL = "UPDATE "+ OutPipeMetaEntity.class.getName() + " a SET a.attemptCount = attemptCount+1, processed = 0, inError = 1, errorMessage = :message, timestamp = SYSDATE WHERE a.id IN (:ids)";
	private static final String INPIPE_ATTEMPTED_SQL  = "UPDATE "+ InPipeMetaEntity.class.getName()  + " a SET a.attemptCount = attemptCount+1, processed = 0, inError = 1, errorMessage = :message, timestamp = SYSDATE WHERE a.id IN (:ids)";

	private String getPipeAttemptedSql(List<? extends PipeData> data) {
		Object o = data.get(0);
		if (o instanceof InPipeData) {
			return INPIPE_ATTEMPTED_SQL;
		}
		return OUTPIPE_ATTEMPTED_SQL;
	}

	private static final String INPIPE_PERM_FAILED_SQL = "UPDATE "+ InPipeMetaEntity.class.getName() +" a SET a.attemptCount = attemptCount+1, processed = 0, inError = 1, a.unrecoverableError = 1, errorMessage = :message, timestamp = SYSDATE WHERE a.id IN (:ids)";

	public void reportPermantentyFailed(List<InPipeData> data, String errorMessage) {
		if (data.isEmpty()) return;
		List<Long> ids = getIds(data);
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(INPIPE_PERM_FAILED_SQL)
			.setParameter("message", Util.trimToByteLength(errorMessage, 4000))
			.setParameterList("ids", ids).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Marking "  + InPipeMetaEntity.class.getName() + " permanently failed " + ids, e);
		}
	}

	private static final String MARK_REPROCESS_INPIPE_SQL = "UPDATE " + InPipeMetaEntity.class.getName() + " a SET a.processed = 0, a.inError = 0, a.attemptCount = -1, a.unrecoverableError = 0, errorMessage = NULL, timestamp = SYSDATE WHERE a.id= :id";

	public void markReprocessInPipe(long id) {
		changeState(id, MARK_REPROCESS_INPIPE_SQL);
	}

	private int changeState(Object id, String sql) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			Query q = session.createQuery(sql);
			if (id instanceof Long) {
				q.setLong("id", (Long)id);
			} else if (List.class.isAssignableFrom(id.getClass())) {
				q.setParameterList("ids", (List<?>)id);
			} else {
				q.setString("id", (String)id);
			}
			int count = q.executeUpdate();
			session.getTransaction().commit();
			return count;
		} catch (Exception e) {
			throw new ETLException("Marking for reprocessing " + id, e);
		}
	}

	private static final String MARK_REPROCESS_OUTPIPE_SQL = "UPDATE " + OutPipeMetaEntity.class.getName() + " a SET a.processed = 0, a.inError = 0, a.attemptCount = -1, errorMessage = NULL, timestamp = SYSDATE WHERE a.id= :id";

	public void markReprocessOutPipe(long id) {
		changeState(id, MARK_REPROCESS_OUTPIPE_SQL);
	}

	private static final String MARK_REPROCESS_OUTPIPE_BY_DOCID_SQL = "UPDATE " + OutPipeMetaEntity.class.getName() + " a SET a.processed = 0, a.inError = 0, a.attemptCount = -1, errorMessage = NULL, timestamp = SYSDATE WHERE a.documentId= :id";

	public int markReprocessOutPipe(Qname documentId) {
		return changeState(documentId.toURI(), MARK_REPROCESS_OUTPIPE_BY_DOCID_SQL);
	}

	public int markReprocessOutPipeByNamedPlaceId(List<Qname> namedPlaceIds) {
		if (namedPlaceIds.isEmpty()) return 0;
		int count = 0;
		for (List<Qname> batch : Lists.partition(namedPlaceIds, 900)) {
			count += markReprocessOutPipeByNamedPlaceIdBatch(batch);
		}
		return count;
	}

	private static final String MARK_REPROCESS_OUTPIPE_BY_NAMEDPLACEID_SQL = "UPDATE " + OutPipeMetaEntity.class.getName() + " a SET a.processed = 0, a.inError = 0, a.attemptCount = -1, errorMessage = NULL, timestamp = SYSDATE WHERE a.namedPlaceId IN(:ids)";

	private int markReprocessOutPipeByNamedPlaceIdBatch(List<Qname> namedPlaceIdBatch) {
		return changeState(namedPlaceIdBatch.stream().map(id->id.toURI()).collect(Collectors.toList()), MARK_REPROCESS_OUTPIPE_BY_NAMEDPLACEID_SQL);
	}

	private static final String CLEAR_OUTPIPE_ERRORS_SQL = "UPDATE " + OutPipeMetaEntity.class.getName() + " a SET a.inError = 0, a.attemptCount = 0, errorMessage = NULL, timestamp = SYSDATE WHERE a.processed = 0 AND a.inError = 1";

	public void markOutPipeErroneousForReattempt() {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			session.createQuery(CLEAR_OUTPIPE_ERRORS_SQL).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Marking outpipe erroneus for reattempt ", e);
		}
	}

	private static final String DELETE_IN_PIPE_SQL = "DELETE FROM " + InPipeMetaEntity.class.getName() + " WHERE id= :id";

	public void removeInPipe(long id) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			removePipe(id, session, DELETE_IN_PIPE_SQL);
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Deleting in pipe entry " + id, e);
		}
	}

	private void removePipe(long id, StatelessSession session, String sql) {
		session.createQuery(sql)
		.setParameter("id", id).executeUpdate();
	}

	private static final String DELETE_OUT_PIPE_SQL = "DELETE FROM " + OutPipeMetaEntity.class.getName() + " WHERE id= :id";

	public void removeOutPipe(long id) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			removePipe(id, session, DELETE_OUT_PIPE_SQL);
			session.getTransaction().commit();
		} catch (Exception e) {
			throw new ETLException("Deleting out pipe entry " + id, e);
		}
	}

	private static final String DELETE_OUT_PIPE_BATCH_SQL = "DELETE FROM " + OutPipeMetaEntity.class.getName() + " WHERE id IN (:ids)";
	private static final String DELETE_IN_PIPE_BATCH_SQL = "DELETE FROM " + InPipeMetaEntity.class.getName() + " WHERE id IN (:ids)";

	private void removeOutPipe(List<Long> ids, StatelessSession session) {
		if (ids.isEmpty()) return;
		removePipe(ids, session, DELETE_OUT_PIPE_BATCH_SQL);
	}

	private void removeInPipe(List<Long> ids, StatelessSession session) {
		if (ids.isEmpty()) return;
		removePipe(ids, session, DELETE_IN_PIPE_BATCH_SQL);
	}

	private void removePipe(List<Long> ids, StatelessSession session, String sql) {
		session.createQuery(sql)
		.setParameterList("ids", ids).executeUpdate();
	}

	public Date getLatestTimestampFromOutPipeOfNonDeletion() {
		String timestamp = getPersisted(MAX_TIMESTAMP);
		if (timestamp == null) return null;
		return new Date(Long.valueOf(timestamp));
	}

	public PipeStats getPipeStatistics(boolean forceReload) {
		if (forceReload) {
			return statsCache.getForceReload();
		}
		return statsCache.get();
	}

	private PipeStats updatePipeStatistics() {
		long start = System.currentTimeMillis();
		try (StatelessSession session = openSession()) {
			PipeStats stats = new PipeStats();
			getPipeStats(InPipeMetaEntity.class, PipeSearchParams.PROCESSED, session).forEach(r->{
				stats.updateInPipeProcessed(r.getKey(), r.getValue());
			});

			getPipeStats(InPipeMetaEntity.class, PipeSearchParams.WAITING, session).forEach(r->{
				stats.updateInPipeWaiting(r.getKey(), r.getValue());
			});

			getPipeStats(InPipeMetaEntity.class, PipeSearchParams.IN_ERROR, session).forEach(r->{
				stats.updateInPipeInError(r.getKey(), r.getValue());
			});

			getPipeStats(OutPipeMetaEntity.class, PipeSearchParams.PROCESSED, session).forEach(r->{
				stats.updateOutPipeProcessed(r.getKey(), r.getValue());
			});

			getPipeStats(OutPipeMetaEntity.class, PipeSearchParams.WAITING, session).forEach(r->{
				stats.updateOutPipeWaiting(r.getKey(), r.getValue());
			});

			getPipeStats(OutPipeMetaEntity.class, PipeSearchParams.IN_ERROR, session).forEach(r->{
				stats.updateOutPipeInError(r.getKey(), r.getValue());
			});

			getPipeStats(OutPipeMetaEntity.class, PipeSearchParams.DELETED, session).forEach(r->{
				stats.updateOutPipeDeleted(r.getKey(), r.getValue());
			});
			return stats;
		} finally {
			double duration = (System.currentTimeMillis() - start)/1000.0;
			if (duration >= 2) {
				System.out.println("Laji-ETL Oracle: Update pipe stats took " + duration + " seconds");
			}
		}
	}

	private Stream<Pair<Qname, Long>> getPipeStats(Class<?> pipeClass, PipeSearchParams pipeSearchParams, StatelessSession session) {
		Criteria c = session.createCriteria(pipeClass)
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("source"))
						.add(Projections.rowCount()));
		addPipeRestrictions(pipeSearchParams, c);
		@SuppressWarnings("unchecked")
		List<Object[]> results = c.list();
		return results.stream().map(r->{
			return new Pair<>(new Qname((String) r[0]), (long) r[1]);
		});
	}

	public int removeUnlinkedInPipeData(Qname source) {
		List<Long> ids = getUnlinkedPipeIds(source);
		if (ids.isEmpty()) return 0;
		try (StatelessSession session = openSession()) {
			for (List<Long> batch : Lists.partition(ids, 900)) {
				session.beginTransaction();
				removeInPipe(batch, session);
				session.getTransaction().commit();
			}
		}
		return ids.size();
	}

	private static final String GET_UNLINKED_SQL = "" +
			" SELECT i.id " +
			" FROM in_pipe i " +
			" LEFT JOIN out_pipe o ON o.source = i.source AND o.inpipeid = i.id " +
			" WHERE i.source = ? " +
			" AND o.id is null " +
			" AND i.processed = 1 " +
			" FETCH FIRST 10000 ROWS ONLY ";

	private List<Long> getUnlinkedPipeIds(Qname source) {
		List<Long> ids = new ArrayList<>();
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = new SimpleTransactionConnection(openConnection());
			p = con.prepareStatement(GET_UNLINKED_SQL);
			p.setFetchSize(5001);
			p.setString(1, source.toString());
			rs = p.executeQuery();
			while (rs.next()) {
				ids.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new ETLException("Remove unlinked in pipe data for " + source, e);
		} finally {
			Utils.close(p, rs, con);
		}
		return ids;
	}

	public void storeApiKey(String apiKey, Qname requestId, Date expires, Qname personId) {
		try (StatelessSession session = openSession()) {
			session.beginTransaction();
			ApiKeyEntity e = new ApiKeyEntity(apiKey, requestId.toString(), expires.getTime(), personId.toString());
			session.insert(e);
			session.getTransaction().commit();
		}
	}

	public Qname getApiKeyRequest(String apiKey) {
		try (StatelessSession session = openSession()) {
			String requestId = (String) session.createCriteria(ApiKeyEntity.class)
					.add(Restrictions.eq("apiKey", apiKey))
					.setProjection(Projections.property("requestId"))
					.uniqueResult();
			if (requestId == null) return null;
			return new Qname(requestId);
		}
	}

	public List<Pair<String, Qname>> getApiKeyRequests(Qname personId) {
		try (StatelessSession session = openSession()) {
			Criteria c = session.createCriteria(ApiKeyEntity.class)
					.addOrder(Order.desc("expires"));
			if (personId != null) {
				c.add(Restrictions.eq("personId", personId.toString()));
			}
			@SuppressWarnings("unchecked")
			List<ApiKeyEntity> keys = c.list();
			if (keys.isEmpty()) return Collections.emptyList();

			return keys.stream().map(e->new Pair<>(e.getApiKey(), new Qname(e.getRequestId()))).collect(Collectors.toList());
		}
	}

	public long getPolygonSearchId(String wkt) {
		String hash = Util.getHash(wkt);
		try (StatelessSession session = openSession()) {
			@SuppressWarnings("unchecked")
			List<PolygonEntity> existing = session.createCriteria(PolygonEntity.class)
			.add(Restrictions.eq("hash", hash)).list();
			for (PolygonEntity e : existing) {
				if (e.getWkt().equals(wkt)) return e.getId();
			}
			session.beginTransaction();
			long id = polygonSeq.getVal(session);
			session.insert(new PolygonEntity(id, hash, wkt));
			session.getTransaction().commit();
			return id;
		}
	}

	public String getPolygonSearch(Long id) {
		try (StatelessSession session = openSession()) {
			return (String) session.createCriteria(PolygonEntity.class)
					.add(Restrictions.eq("id", id))
					.setProjection(Projections.property("wkt"))
					.uniqueResult();
		}
	}

	public ResultStream<String> getAllDocumentIds() {
		StatelessSession session = openSession();
		ScrollableResults res =
				session.createCriteria(OutPipeMetaEntity.class)
				.add(Restrictions.eq("processed", 1))
				.add(Restrictions.eq("deletion", 0))
				.setProjection(Projections.distinct(Projections.property("documentId")))
				.setFetchSize(5001)
				.scroll(ScrollMode.FORWARD_ONLY);
		return new ScrollableResultsStream<>(res, session, String.class);
	}

}
