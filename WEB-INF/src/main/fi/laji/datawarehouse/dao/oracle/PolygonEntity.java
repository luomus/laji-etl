package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="polygon_search")
public class PolygonEntity {

	private long id;
	private String hash;
	private String wkt;

	public PolygonEntity() {}

	public PolygonEntity(long id, String hash, String wkt) {
		setId(id);
		setHash(hash);
		setWkt(wkt);
	}

	@Id
	@Column
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Column(columnDefinition="CLOB")
	@Lob
	public String getWkt() {
		return wkt;
	}

	public void setWkt(String wkt) {
		this.wkt = wkt;
	}

}
