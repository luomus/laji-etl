package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="apikey")
public class ApiKeyEntity {

	private String apiKey;
	private String requestId;
	private long expires;
	private String personId;

	public ApiKeyEntity() {}

	public ApiKeyEntity(String apiKey, String requestId, long expires, String personId) {
		setApiKey(apiKey);
		setRequestId(requestId);
		setExpires(expires);
		setPersonId(personId);
	}

	@Id
	@Column
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Column
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	@Column
	public long getExpires() {
		return expires;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	@Column
	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

}
