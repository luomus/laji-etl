package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reprocess_temp")
public class ReprocessTempEntity {

	private long id;
	private String documentId;

	public ReprocessTempEntity() {}

	public ReprocessTempEntity(int id, String documentId) {
		setId(id);
		setDocumentId(documentId);
	}

	@Id
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


}
