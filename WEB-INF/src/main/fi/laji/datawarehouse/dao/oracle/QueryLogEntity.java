package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="query_log")
public class QueryLogEntity {

	private String id;
	private long timestamp;
	private int year;
	private String warehouse;
	private String apiUser;
	private String caller;
	private String userAgent;
	private String personId;
	private String personEmail;
	private String base;
	private String queryString;
	private String permissionId;

	@Id
	@Column
	public String getId() {
		return id;
	}
	@Column
	public long getTimestamp() {
		return timestamp;
	}
	@Column
	public int getYear() {
		return year;
	}
	@Column
	public String getWarehouse() {
		return warehouse;
	}
	@Column
	public String getApiUser() {
		return apiUser;
	}
	@Column
	public String getCaller() {
		return caller;
	}
	@Column
	public String getUserAgent() {
		return userAgent;
	}
	@Column
	public String getPersonId() {
		return personId;
	}
	@Column
	public String getPersonEmail() {
		return personEmail;
	}
	@Column
	public String getBase() {
		return base;
	}
	@Column
	public String getQueryString() {
		return queryString;
	}
	@Column
	public String getPermissionId() {
		return permissionId;
	}

	public void setId(String id) {
		if (!given(id)) throw new IllegalArgumentException();
		this.id = id;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setWarehouse(String warehouse) {
		if (!given(warehouse)) throw new IllegalArgumentException();
		this.warehouse = warehouse;
	}
	public void setApiUser(String apiUser) {
		if (given(apiUser)) {
			this.apiUser = apiUser;
		} else {
			this.apiUser = null;
		}
	}
	public void setCaller(String caller) {
		if (given(caller)) {
			this.caller = caller;
		} else {
			this.caller = null;
		}
	}
	public void setUserAgent(String userAgent) {
		if (given(userAgent)) {
			this.userAgent = userAgent;
		} else {
			this.userAgent = null;
		}
	}
	public void setPersonId(String personId) {
		if (given(personId)) {
			this.personId = personId;
		} else {
			this.personId = null;
		}
	}
	public void setPersonEmail(String personEmail) {
		if (given(personEmail)) {
			this.personEmail = personEmail;
		} else {
			this.personEmail = null;
		}
	}
	public void setQueryString(String queryString) {
		if (given(queryString)) {
			this.queryString = queryString;
		} else {
			this.queryString = null;
		}
	}
	public void setBase(String base) {
		if (!given(base)) throw new IllegalArgumentException();
		this.base = base;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setPermissionId(String permissionId) {
		if (given(permissionId)) {
			this.permissionId = permissionId;
		} else {
			this.permissionId = null;
		}
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

}