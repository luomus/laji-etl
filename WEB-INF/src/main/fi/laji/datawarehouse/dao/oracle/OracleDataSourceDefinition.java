package fi.laji.datawarehouse.dao.oracle;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class OracleDataSourceDefinition {

	public static HikariDataSource initDataSource(ConnectionDescription desc) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());

		config.setAutoCommit(false); // transaction mode: all changes must be committed

		config.setConnectionTimeout(30000); // 30 seconds
		config.setMaximumPoolSize(60);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(14400000); // 4 hours

		return new HikariDataSource(config);
	}

}

