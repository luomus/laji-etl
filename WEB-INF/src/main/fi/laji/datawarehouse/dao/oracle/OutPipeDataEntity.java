package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="out_pipe_data")
public class OutPipeDataEntity {

	private long id;
	private String data;
	private String source;

	public OutPipeDataEntity() {}

	@Id
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(columnDefinition="CLOB")
	@Lob
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Column
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

}
