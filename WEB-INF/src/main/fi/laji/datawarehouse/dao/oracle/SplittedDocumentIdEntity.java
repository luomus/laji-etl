package fi.laji.datawarehouse.dao.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import fi.luomus.commons.containers.rdf.Qname;

@Entity
@Table(name="splitted_documentid")
public class SplittedDocumentIdEntity {

	private String splittedDocumentId;
	private String originalDocumentId;
	private String originalUnitId;

	public SplittedDocumentIdEntity() {}

	public SplittedDocumentIdEntity(Qname originalDocumentId, Qname originalUnitId, Qname splittedDocumentId) {
		this.originalDocumentId = originalDocumentId.toURI();
		this.originalUnitId = originalUnitId.toURI();
		this.splittedDocumentId = splittedDocumentId.toURI();
	}

	@Id
	@Column
	public String getSplittedDocumentId() {
		return splittedDocumentId;
	}
	public void setSplittedDocumentId(String splittedDocumentId) {
		this.splittedDocumentId = splittedDocumentId;
	}
	@Column
	public String getOriginalDocumentId() {
		return originalDocumentId;
	}
	public void setOriginalDocumentId(String originalDocumentId) {
		this.originalDocumentId = originalDocumentId;
	}
	@Column
	public String getOriginalUnitId() {
		return originalUnitId;
	}
	public void setOriginalUnitId(String originalUnitId) {
		this.originalUnitId = originalUnitId;
	}

}
