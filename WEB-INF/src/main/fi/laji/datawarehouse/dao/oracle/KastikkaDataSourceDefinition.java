package fi.laji.datawarehouse.dao.oracle;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class KastikkaDataSourceDefinition {

	public static HikariDataSource initDataSource(ConnectionDescription desc) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());
		config.setAutoCommit(false); // transaction mode: all changes must be committed
		config.setMaximumPoolSize(2);
		return new HikariDataSource(config);
	}

}

