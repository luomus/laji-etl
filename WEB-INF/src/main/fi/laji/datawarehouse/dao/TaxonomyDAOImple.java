package fi.laji.datawarehouse.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import fi.laji.datawarehouse.etl.models.TaxonLinkingService;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.LocalizedTexts;
import fi.luomus.commons.containers.TaxonSet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.AdministrativeStatusContainer;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple.InfiniteTaxonLoopException;
import fi.luomus.commons.taxonomy.InformalTaxonGroupContainer;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonomyDAOBaseImple;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;

public class TaxonomyDAOImple extends TaxonomyDAOBaseImple {

	private static final String SEPARATOR = "\u001F";

	private static final Set<String> IGNORED_PREDICATES = initIgnoredPredicates();

	private static Set<String> initIgnoredPredicates() {
		Set<String> ignored = new LinkedHashSet<>();
		ignored.add("MX.externalLinkURL");
		ignored.add("MX.stopOccurrenceInFinlandPublicationInheritance");
		ignored.add("MX.stopOriginalPublicationInheritance");
		ignored.add("MX.typeOfOccurrenceInFinlandNotes");
		ignored.add("MX.distributionMapFinland");
		ignored.add("MX.taxonExpert");
		ignored.add("MX.taxonEditor");
		ignored.add("MX.originalPublication");
		ignored.add("MX.occurrenceInFinlandPublication");
		ignored.add("MX.nameDecidedBy");
		ignored.add("MX.nameDecidedDate");
		ignored.add("MX.notes");
		ignored.add("MX.privateNotes");
		ignored.add("MZ.createdAtTimestamp");
		ignored.add("HBE.invasiveSpeciesMainGroup");
		ignored.add("MX.speciesCardAuthors");
		ignored.add("MX.ingressText");
		ignored.add("MX.descriptionText");
		ignored.add("MX.identificationText");
		ignored.add("MX.miscText");
		ignored.add("MX.distributionFinland");
		ignored.add("MX.originAndDistributionText");
		ignored.add("MX.descriptionOrganismSize");
		ignored.add("MX.descriptionStem");
		ignored.add("MX.descriptionLeaf");
		ignored.add("MX.descriptionFlower");
		ignored.add("MX.descriptionFruitAndSeed");
		ignored.add("MX.descriptionCone");
		ignored.add("MX.descriptionRoot");
		ignored.add("MX.descriptionThallus");
		ignored.add("MX.descriptionFruitbody");
		ignored.add("MX.descriptionSpore");
		ignored.add("MX.algalPartnerOfLichen");
		ignored.add("MX.reproduction");
		ignored.add("MX.lifeCycle");
		ignored.add("MX.reproductionFloweringTime");
		ignored.add("MX.reproductionPollination");
		ignored.add("MX.descriptionSporangiumAndAsexualReproduction");
		ignored.add("MX.habitat");
		ignored.add("MX.habitatSubstrate");
		ignored.add("MX.ecology");
		ignored.add("MX.behaviour");
		ignored.add("MX.descriptionMicroscopicIdentification");
		ignored.add("MX.growthFormAndGrowthHabit");
		ignored.add("MX.conservationStatusDescriptionFinland");
		ignored.add("MX.conservationStatusDescriptionWorld");
		ignored.add("MX.management");
		ignored.add("MX.invasiveEffectText");
		ignored.add("MX.invasivePreventionMethodsText");
		ignored.add("MX.invasiveCitizenActionsText");
		ignored.add("MX.invasiveSpeciesClassificationDescription");
		ignored.add("MX.taxonomyText");
		ignored.add("MX.etymologyText");
		ignored.add("MX.cultivationText");
		ignored.add("MX.productionText");
		ignored.add("MX.economicUseText");
		ignored.add("MX.customReportFormLink");
		ignored.add("MX.circumscription");
		ignored.add("MX.frequencyScoringPoints");
		ignored.add("MX.typeSpecimenURI");
		ignored.add("MX.descriptionToxicity");
		ignored.add("MX.descriptionHostParasite");
		ignored.add("MX.descriptionReferences");
		ignored.add("MX.originalDescription");
		return ignored;
	}

	private static final Map<String, InformalTaxonGroup> TEST_INFORMALGROUP_MAP;
	static {
		TEST_INFORMALGROUP_MAP = new HashMap<>();
		TEST_INFORMALGROUP_MAP.put("MVL.200", new InformalTaxonGroup(new Qname("MVL.200"), new LocalizedText().set("fi", "Turskat"), 1));
		TEST_INFORMALGROUP_MAP.put("MVL.1", new InformalTaxonGroup(new Qname("MVL.1"), new LocalizedText().set("fi", "Linnut"), 1));
	}

	private static final Map<String, TaxonSet> TEST_TAXON_SET_MAP;
	static {
		TEST_TAXON_SET_MAP = new HashMap<>();
		TEST_TAXON_SET_MAP.put("MX.testSet", new TaxonSet(new Qname("MX.testSet"), new LocalizedText().set("fi", "Testisetti")));
	}

	private final Map<String, Set<Qname>> taxonSearchMap = new HashMap<>(TaxonContainer.ESTIMATED_NUMBER_OF_TAXA);
	private final Config config;
	private final Class<?> createdBy;
	private final long createdAt;
	private final DAO dao;
	private TaxonContainer taxonContainer = null;
	private static final Object LOCK = new Object();

	public TaxonomyDAOImple(Config config, DAO dao, Class<?> createdBy) {
		super(config);
		this.config = config;
		this.dao = dao;
		this.createdBy = createdBy;
		this.createdAt = DateUtils.getCurrentEpoch();
		dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created! (Created by " + this.createdBy.getSimpleName() + " at " + this.createdAt + ")" );
	}

	@Override
	public Taxon getTaxon(Qname qname) throws NoSuchTaxonException {
		return getTaxonContainer().getTaxon(qname);
	}

	/**
	 * Get all matches for the target name, including conflicting - used to determine secured species
	 * @param targetName
	 * @return
	 */
	public Set<Qname> getQnamesThatMatch(String targetName) {
		getTaxonContainer(); // To make sure search map is initialized
		String cleanedName = TaxonLinkingService.cleanTargetName(targetName);
		if (!taxonSearchMap.containsKey(cleanedName)) return Collections.emptySet();
		return Collections.unmodifiableSet(taxonSearchMap.get(cleanedName));
	}

	@Override
	public void clearCaches() {
		synchronized (LOCK) {
			taxonSearchMap.clear();
			taxonContainer = null;
			super.clearCaches();
		}
	}

	@Override
	public TaxonContainer getTaxonContainer() {
		if (taxonContainer == null) {
			synchronized (LOCK) {
				if (taxonContainer == null) {
					File tripletFile = getTripletFile();
					File habitatFile = getHabitatFile();
					taxonContainer = new TaxonContainerLoader(config, dao, tripletFile, habitatFile).load();
				}
			}
		}
		return taxonContainer;
	}

	public File reloadTriplets() {
		synchronized (LOCK) {
			File finalFile = tripletFile();
			File backupFile = new File(tempFolder(), "taxon_triplets_BACKUP.txt");
			File tempFile = new TaxonTripletLoader(config, dao).load();
			if (finalFile.exists()) {
				if (backupFile.exists()) {
					backupFile.delete();
				}
				finalFile.renameTo(backupFile);
			}
			tempFile.renameTo(finalFile);
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon triplets saved to " + finalFile.getAbsolutePath());
			return finalFile;
		}
	}

	public File reloadHabitats() {
		synchronized (LOCK) {
			File finalFile = habitatFile();
			File backupFile = new File(tempFolder(), "taxon_habitats_BACKUP.txt");
			File tempFile = new HabitatLoader(config, dao).load();
			if (finalFile.exists()) {
				if (backupFile.exists()) {
					backupFile.delete();
				}
				finalFile.renameTo(backupFile);
			}
			tempFile.renameTo(finalFile);
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Habitats saved to " + finalFile.getAbsolutePath());
			return finalFile;
		}
	}

	@Override
	public TaxonSearchResponse search(TaxonSearch taxonSearch) {
		throw new UnsupportedOperationException("Taxon search not implemented");
	}

	@Override
	public Map<String, InformalTaxonGroup> getInformalTaxonGroups() {
		if (config.developmentMode()) { // TODO should move all test related stuff out from here and override
			return TEST_INFORMALGROUP_MAP;
		}
		return super.getInformalTaxonGroups();
	}

	@Override
	public Map<String, TaxonSet> getTaxonSets() {
		if (config.developmentMode()) { // TODO should move all test related stuff out from here and override
			return TEST_TAXON_SET_MAP;
		}
		return super.getTaxonSets();
	}

	private File getTripletFile() {
		File tripletFile = tripletFile();
		if (tripletFile.exists()) return tripletFile;
		synchronized (LOCK) {
			if (tripletFile.exists()) return tripletFile;
			tripletFile = reloadTriplets();
			return tripletFile;
		}
	}

	private File getHabitatFile() {
		File habitatFile = habitatFile();
		if (habitatFile.exists()) return habitatFile;
		synchronized (LOCK) {
			if (habitatFile.exists()) return habitatFile;
			habitatFile = reloadHabitats();
			return habitatFile;
		}
	}

	private File tripletFile() {
		return new File(tempFolder(), "taxon_triplets.txt");
	}

	private File habitatFile() {
		return new File(tempFolder(), "taxon_habitats.txt");
	}

	private File tempFolder() {
		return new File(config.baseFolder() + config.get("TempFolder"));
	}

	private class TaxonTripletLoader {

		private final ConnectionDescription connectionDescription;
		private final DAO dao;
		private final Config config;
		private final Set<String> usedTaxonPredicates = new TreeSet<>();

		public TaxonTripletLoader(Config config, DAO dao) {
			this.config = config;
			this.connectionDescription = config.connectionDescription("Taxonomy");
			this.dao = dao;
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created!");
		}

		public File load() {
			TransactionConnection con = null;
			try {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to load taxon triplets...");
				con = new SimpleTransactionConnection(connectionDescription);
				File f = loadUsing(con);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon triplets loading done");
				return f;
			} catch (Exception e) {
				throw new ETLException("Taxon triplet load failed", e);
			}
			finally {
				if (con != null) con.release();
			}
		}

		private File loadUsing(TransactionConnection con) throws Exception {
			PreparedStatement p = null;
			ResultSet rs = null;
			File tempFile = new File(tempFolder(), "taxon-triplets-"+DateUtils.getFilenameDatetime()+".txt");
			tempFile.getParentFile().mkdirs();
			try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"), 1024*1024)) {
				usedTaxonPredicates.clear();
				String sql = getTaxonLoadSQL();
				p = con.prepareStatement(sql);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Executing taxon triplet query...");
				rs = p.executeQuery();
				rs.setFetchSize(4001);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Received first taxon triplet query response, going through result set...");
				int i = 0;
				while (rs.next()) {
					write(rs, writer);
					i++;
					if (i % 100000 == 0 || i == 1) {
						dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ... taxon triplet row " + i);
					}
				}
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Used taxon predicates (remember to add unneccesary to ignored predicates): " + usedTaxonPredicates);
				return tempFile;
			} catch (Exception e) {
				if (tempFile.exists()) {
					try { tempFile.delete(); } catch (Exception e2) {}
				}
				throw e;
			} finally {
				Utils.close(p, rs);
			}
		}

		private String getTaxonLoadSQL() throws ClientProtocolException, IOException, URISyntaxException {
			StringBuilder sql = new StringBuilder()
					.append(" SELECT     s.subjectname, s.predicatename, s.objectname, s.resourceliteral, s.langcodefk, s.contextname ")
					.append(" FROM       rdf_statementview s ")
					.append(" LEFT JOIN  rdf_statementview c ON (c.subjectname = s.subjectname AND c.predicatename = 'MX.nameAccordingTo' ) ")
					.append(" WHERE      s.subjectname IN (     ")
					.append(" 				SELECT distinct subjectname     ")
					.append(" 				FROM   rdf_statementview     ")
					.append(" 				WHERE  predicatename = 'rdf:type' ")
					.append(" 				AND    objectname = 'MX.taxon' ");
			if (config.developmentMode()) { // Limit taxon load
				Set<String> limitToIds = getLimitedIds();
				sql.append(" 				AND subjectname IN ( ");
				Utils.toCommaSeperatedStatement(sql, limitToIds, true);
				sql.append(" 				) ");
			}
			sql.append(" ) ");
			sql.append(" AND ( c.objectname IS NULL OR c.objectname IN ('MR.1', 'MR.84') ) "); // Limit by checklist, include orphan
			sql.append(" AND s.predicatename NOT IN ( "); // Limit by predicates
			Utils.toCommaSeperatedStatement(sql, IGNORED_PREDICATES, true);
			sql.append(" ) ORDER BY s.subjectname ");
			return sql.toString();
		}

		private void write(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
			String taxonId = s(rs, 1);
			String predicate = s(rs, 2);
			String object = s(rs, 3);
			String resourceliteral = s(rs, 4);
			String locale = s(rs, 5);
			String context = s(rs, 6);
			w(writer, taxonId);
			w(writer, predicate);
			w(writer, object);
			w(writer, resourceliteral);
			w(writer, locale);
			w(writer, context);
			writer.write('\n');
			usedTaxonPredicates.add(predicate.toString());
		}
	}

	private class HabitatLoader {

		private final ConnectionDescription connectionDescription;
		private final DAO dao;
		private final Config config;

		public HabitatLoader(Config config, DAO dao) {
			this.config = config;
			this.connectionDescription = config.connectionDescription("Taxonomy");
			this.dao = dao;
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created!");
		}

		public File load() {
			TransactionConnection con = null;
			try {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to load taxon habitats...");
				con = new SimpleTransactionConnection(connectionDescription);
				File f = loadUsing(con);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon habitat loading done");
				return f;
			} catch (Exception e) {
				throw new ETLException("Taxon habitat load failed", e);
			}
			finally {
				if (con != null) con.release();
			}
		}

		private File loadUsing(TransactionConnection con) throws Exception {
			PreparedStatement p = null;
			ResultSet rs = null;
			File tempFile = new File(tempFolder(), "taxon-habitats-"+DateUtils.getFilenameDatetime()+".txt");
			tempFile.getParentFile().mkdirs();
			try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"), 1024*1024)) {
				String sql = getHabitatLoadSQL();
				p = con.prepareStatement(sql);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Executing habitat query...");
				rs = p.executeQuery();
				rs.setFetchSize(4001);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Received first habitat query response, going through result set...");
				int i = 0;
				while (rs.next()) {
					write(rs, writer);
					i++;
					if (i % 10000 == 0 || i == 1) {
						dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ... habitat row " + i);
					}
				}
				return tempFile;
			} catch (Exception e) {
				if (tempFile.exists()) {
					try { tempFile.delete(); } catch (Exception e2) {}
				}
				throw e;
			} finally {
				Utils.close(p, rs);
			}
		}

		private String getHabitatLoadSQL() throws ClientProtocolException, IOException, URISyntaxException {
			StringBuilder sql = new StringBuilder("" +
					" SELECT 	taxon.subjectname AS taxonId, " +
					" 			habitatId.objectname AS habitatId,  " +
					" 			habitatId.predicatename AS type,  " +
					" 			habitat.objectname AS habitat,  " +
					" 			sortOrder.resourceliteral AS sortOrder, " +
					" 			habitatSpecificType.objectname AS habitatSpecificType " +
					" FROM 		rdf_statementview taxon " +
					" JOIN 		rdf_statementview habitatId ON taxon.subjectname = habitatId.subjectname AND habitatId.predicatename IN ('MKV.primaryHabitat', 'MKV.secondaryHabitat') " +
					" JOIN 		rdf_statementview habitat ON habitatId.objectname = habitat.subjectname AND habitat.predicatename = 'MKV.habitat' " +
					" LEFT JOIN rdf_statementview sortOrder ON habitatId.objectname = sortOrder.subjectname AND sortOrder.predicatename = 'sortOrder' " +
					" LEFT JOIN	rdf_statementview habitatSpecificType ON habitatId.objectname = habitatSpecificType.subjectname AND habitatSpecificType.predicatename = 'MKV.habitatSpecificType' " +
					" WHERE 	taxon.predicatename = 'rdf:type' " +
					" AND		taxon.objectname = 'MX.taxon' ");

			if (config.developmentMode()) { // Limit taxon load
				Set<String> limitToIds = getLimitedIds();
				sql.append(" 				AND taxon.subjectname IN ( ");
				Utils.toCommaSeperatedStatement(sql, limitToIds, true);
				sql.append(" 				) ");
			}
			sql.append(" ORDER BY	taxonId, habitatId, type ");
			return sql.toString();

		}

		private void write(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
			//					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeP
			//					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeH
			//					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypePAK
			//					// MX.59808	MKV.383408	MKV.secondaryHabitat	MKV.habitatIu	0
			String taxonId = s(rs, 1);
			String habitatId = s(rs, 2);
			String type = s(rs, 3);
			String habitat = s(rs, 4);
			String order = s(rs, 5);
			String habitatSpecificType = s(rs, 6);
			w(writer, taxonId);
			w(writer, habitatId);
			w(writer, type);
			w(writer, habitat);
			w(writer, order);
			w(writer, habitatSpecificType);
			writer.write('\n');
		}
	}

	private static void w(BufferedWriter writer, String s) throws IOException {
		writer.write(s);
		writer.write(SEPARATOR);
	}

	private static String s(ResultSet rs, int i) throws SQLException {
		String s = rs.getString(i);
		if (s == null) return "";
		return s.replace(SEPARATOR, "").replace("\n", "").replace("\r", "");
	}

	private class TaxonContainerLoader {

		private final DAO dao;
		private final Config config;
		private final File tripletFile;
		private final File habitatFile;

		public TaxonContainerLoader(Config config, DAO dao, File tripletFile, File habitatFile) {
			this.config = config;
			this.dao = dao;
			this.tripletFile = tripletFile;
			this.habitatFile = habitatFile;
			dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), this.getClass().getSimpleName() + " created!");
		}

		public TaxonContainer load() {
			try {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Starting to create taxon container...");
				InMemoryTaxonContainerImple taxonContainer = load(tripletFile);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loading habitats...");
				addHabitats(taxonContainer, habitatFile);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Loading occurrence counts...");
				addOccurrenceCounts(taxonContainer);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Generating search map...");
				Taxon root = getRoot(taxonContainer);
				toSearchMap(root);
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Taxon container creation done");
				return taxonContainer;
			} catch (Exception e) {
				throw new ETLException("Taxon container creation failed", e);
			}
		}

		private Taxon getRoot(TaxonContainer taxonContainer) {
			return taxonContainer.getTaxon(Const.MASTER_CHECKLIST_BIOTA_QNAME);
		}

		private void toSearchMap(Taxon taxon) {
			Qname taxonId = taxon.getQname();
			toSearchMap(taxonId, taxonId.toString());
			toSearchMap(taxonId, taxonId.toURI());
			toSearchMap(taxonId, taxon.getScientificName());
			toSearchMap(taxonId, taxon.getVernacularName());
			toSearchMap(taxonId, taxon.getAlternativeVernacularNames());
			toSearchMap(taxonId, taxon.getObsoleteVernacularNames());
			toSearchMap(taxonId, taxon.getBirdlifeCode());
			toSearchMap(taxonId, taxon.getEuringCode());
			toSearchMap(taxonId, taxon.getAlsoKnownAsNames());
			toSearchMap(taxonId, taxon.getTradeNames());
			for (Taxon synonym : taxon.getAllSynonyms()) {
				toSearchMap(taxonId, synonym.getQname().toString());
				toSearchMap(taxonId, synonym.getQname().toURI());
				toSearchMap(taxonId, synonym.getScientificName());
			}
			if (taxon.hasChildren()) {
				for (Taxon child : taxon.getChildren()) {
					toSearchMap(child);
				}
			}
		}

		private void toSearchMap(Qname taxonId, Set<String> names) {
			if (names == null) return;
			for (String name : names) {
				toSearchMap(taxonId, name);
			}
		}

		private void toSearchMap(Qname taxonId, LocalizedTexts localizedTexts) {
			if (localizedTexts == null) return;
			for (String name : localizedTexts.getAllValues()) {
				toSearchMap(taxonId, name);
			}
		}

		private void toSearchMap(Qname taxonId, LocalizedText localizedText) {
			if (localizedText == null) return;
			for (String name : localizedText.getAllTexts().values()) {
				toSearchMap(taxonId, name);
			}
		}

		private void toSearchMap(Qname taxonQname, String name) {
			if (name == null) return;
			name = TaxonLinkingService.cleanTargetName(name);
			if (name.isEmpty()) return;
			if (!taxonSearchMap.containsKey(name)) {
				taxonSearchMap.put(name, new HashSet<Qname>(1));
			}
			taxonSearchMap.get(name).add(taxonQname);
		}


		private void addOccurrenceCounts(InMemoryTaxonContainerImple taxonContainer) {
			String data = dao.getPersisted("obscounts");
			if (data == null)  {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Occurrence counts not loaded - skipping!");
				return;
			}
			JSONObject json = new JSONObject(data);
			for (JSONObject counts : json.getArray("results").iterateAsObject()) {
				Qname taxonId = Qname.fromURI(counts.getString("taxonId"));
				if (!taxonContainer.hasTaxon(taxonId)) continue;
				Taxon taxon = taxonContainer.getTaxon(taxonId);
				taxon.setExplicitObservationCount(counts.getInteger("count"));
				taxon.setExplicitObservationCountFinland(counts.getInteger("countFinland"));
			}
			taxonContainer.generateInheritedOccurrencesAndHabitats();
		}

		private void addHabitats(TaxonContainer taxonContainer, File habitatFile) throws Exception {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(habitatFile), "UTF-8"), 1024*1024)) {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reading habitats from " + habitatFile.getAbsolutePath() + " ... ");
				String line = null;
				HabitatObject habitatObject = new HabitatObject(new Qname(""), null, -1);
				while ((line = reader.readLine()) != null) {
					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeP
					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeH
					// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypePAK
					// MX.59808	MKV.383408	MKV.secondaryHabitat	MKV.habitatIu	0
					String[] parts = line.split(SEPARATOR, -1);
					Qname taxonId = new Qname(parts[0]);
					Qname habitatId = new Qname(parts[1]);
					String type = parts[2];
					Qname habitat = new Qname(parts[3]);
					int order = intV(parts[4]);
					Qname habitatSpecificType = new Qname(parts[5]);

					if (habitatId.equals(habitatObject.getId())) {
						habitatObject.addHabitatSpecificType(habitatSpecificType);
					} else {
						habitatObject = new HabitatObject(habitatId, habitat, order);
						if (habitatSpecificType.isSet()) {
							habitatObject.addHabitatSpecificType(habitatSpecificType);
						}
						if (!taxonContainer.hasTaxon(taxonId)) continue;
						Taxon taxon = taxonContainer.getTaxon(taxonId);
						if (type.equals("MKV.primaryHabitat")) {
							taxon.setPrimaryHabitat(habitatObject);
						} else {
							taxon.addSecondaryHabitat(habitatObject);
						}
					}
				}
			}
		}

		private int intV(String string) {
			if (string == null || string.isEmpty()) return 0;
			try {
				return Integer.valueOf(string);
			} catch (NumberFormatException e) {
				return 0;
			}
		}

		private InMemoryTaxonContainerImple load(File tripletFile) throws Exception {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(tripletFile), "UTF-8"), 1024*1024)) {
				dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), "Reading triplets from " + tripletFile.getAbsolutePath() + " ... ");
				InMemoryTaxonContainerImple taxonContainer = new InMemoryTaxonContainerImple(
						new InformalTaxonGroupContainer(dao.getTaxonomyDAO().getInformalTaxonGroups()),
						new InformalTaxonGroupContainer(dao.getTaxonomyDAO().getRedListEvaluationGroups()),
						new AdministrativeStatusContainer(dao.getTaxonomyDAO().getAdministrativeStatuses()));
				int i = 0;
				String line = null;
				while ((line = reader.readLine()) != null) {
					addTaxonInformation(line, taxonContainer);
					i++;
					if (i % 100000 == 0 || i == 1) {
						dao.logMessage(Const.LAJI_ETL_QNAME, this.getClass(), " ... taxon triplet row " + i);
					}
				}
				List<InfiniteTaxonLoopException> ex = taxonContainer.generateTaxonomicOrders();
				for (InfiniteTaxonLoopException e : ex) {
					dao.getErrorReporter().report(e);
					e.printStackTrace();
				}
				if (config.developmentMode()) {
					taxonContainer.getTaxon(new Qname("MX.26018")).addTaxonSet(new Qname("MX.testSet")); // TODO should move all test related stuff out from here and override
				}
				return taxonContainer;
			}
		}

		private void addTaxonInformation(String line, InMemoryTaxonContainerImple taxonContainer) {
			String[] parts = line.split(SEPARATOR, -1);
			Qname taxonId = new Qname(parts[0]);
			Qname predicate = new Qname(parts[1]);
			Qname object = new Qname(parts[2]);

			String resourceliteral = parts[3];
			if (resourceliteral.isEmpty()) resourceliteral = null;

			String locale = parts[4];
			if (locale.isEmpty()) locale = null;

			Qname context = new Qname(parts[5]);
			if (!context.isSet()) context = null;

			taxonContainer.handle(taxonId, predicate, object, resourceliteral, locale, context);
		}
	}

	private Set<String> getLimitedIds() throws ClientProtocolException, IOException, URISyntaxException {
		HttpClientService client = null;
		try {
			client = new HttpClientService();
			Set<String> ids = new HashSet<>();
			ids.addAll(getLimitedIds(client, "MX.26825")); // tunturihaukka
			ids.addAll(getLimitedIds(client, "MX.34567")); // talitiainen
			ids.addAll(getLimitedIds(client, "MX.26018")); // ruusupelikaani
			ids.addAll(getLimitedIds(client, "MX.28715")); // käki
			ids.addAll(getLimitedIds(client, "MX.46549")); // susi
			ids.addAll(getLimitedIds(client, "MX.26813")); // idännuolihaukka
			ids.addAll(getLimitedIds(client, "MX.27791")); // mustatiira (has a synonym)
			ids.add("MX.200102"); // mustatiiran synonyymi
			ids.addAll(getLimitedIds(client, "MX.1")); // sudenkorennot
			ids.addAll(getLimitedIds(client, "MX.26944")); // vuoriviiriäinen
			ids.add("MX.316762"); // synonym of vuoriviiriäinen
			ids.addAll(getLimitedIds(client, "MX.272980")); // niinijalokirsikäs (multiple secondary habitats)
			ids.addAll(getLimitedIds(client, "MX.59808")); //  kangashietakoi (primary habitat has several habitatSpecificTypes)
			ids.addAll(getLimitedIds(client, "MX.52882")); //  Clavibacter michiganensis subsp. michiganensis (subspecies)
			ids.addAll(getLimitedIds(client, "MX.39158")); // jättipalsami (for invasive control tests)
			ids.addAll(getLimitedIds(client, "MX.53780")); // koivunlehti 1 cm (MR.84 - spring monitoring)
			ids.addAll(getLimitedIds(client, "MX.50594")); // Muridae with aka name "hiiri, myyrä"
			ids.addAll(getLimitedIds(client, "MX.48533")); // Myopus schisticolor
			ids.addAll(getLimitedIds(client, "MX.51085")); // Myotis
			ids.addAll(getLimitedIds(client, "MX.37637")); // kangaskäärme (KM5 secure level)
			ids.addAll(getLimitedIds(client, "MX.30438")); // valkoselkätikka (highest on natura area, 5KM for breeding site and season; custom breeding season)
			ids.addAll(getLimitedIds(client, "MX.5083976")); // kanahaukan alalaji (no secure levels but should get them from parent; custom breeding season)
			ids.addAll(getLimitedIds(client, "MX.26727")); // maakotka (custom breeding season + baseline secure level)
			ids.addAll(getLimitedIds(client, "MX.37095")); // kuukkeli (custom secure rule: KM100 in south)
			ids.addAll(getLimitedIds(client, "MX.27699")); // suosirri (custom secure rule: only secure in the south)
			ids.addAll(getLimitedIds(client, "MX.26530")); // merikotka (custom secure rule: different secure level south and north; custom breeding season)
			ids.addAll(getLimitedIds(client, "MX.291296")); // koira (auto non wild = true)
			ids.addAll(getLimitedIds(client, "MX.4973478")); // Raunucuus -group species - parent chain has two taxa with aggregate rank: MX.37908 and MX.4972569 -- above those is genus MX.37894
			ids.addAll(getLimitedIds(client, "MX.4973479")); // same as above, different species
			ids.addAll(getLimitedIds(client, "MX.61819")); // Xanthorhoe decoloraria - has hard coded validation rules
			return ids;
		} finally {
			if (client != null) client.close();
		}
	}

	private Collection<String> getLimitedIds(HttpClientService client, String childId) throws ClientProtocolException, IOException, URISyntaxException {
		Collection<String> ids = new ArrayList<>();
		ids.add(childId);
		URIBuilder uri = new URIBuilder(config.get("ApiBaseURL") + "/taxa/"+childId+"/parents").addParameter("access_token", config.get("ApiKey")).addParameter("selectedFields", "id");
		JSONArray response = client.contentAsJsonArray(new HttpGet(uri.getURI()));
		for (JSONObject o : response.iterateAsObject()) {
			ids.add(o.getString("id"));
		}
		return ids;
	}

}
