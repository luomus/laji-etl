package fi.laji.datawarehouse.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.etl.models.containers.SourceDocumentLoadStatisticsData;
import fi.laji.datawarehouse.etl.models.containers.TeamMemberData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.luomus.commons.containers.rdf.Qname;

public interface VerticaQueryDAO {

	/**
	 * Get single document by document id from the warehouse
	 * @param documentId
	 * @return
	 * @throws CriticalParseFailure
	 */
	Document get(Qname documentId);

	/**
	 * Get single document by document id from the warehouse
	 * @param documentId
	 * @param approvedDataRequest
	 * @return
	 * @throws CriticalParseFailure
	 */
	Document get(Qname documentId, boolean approvedDataRequest);

	/**
	 * List query
	 * @param query
	 * @return
	 */
	ListResponse getList(ListQuery query);

	/**
	 * List query - raw response without total count. CAN NOT BE CACHED.
	 * @param query
	 * @return
	 */
	List<JoinedRow> getRawList(ListQuery query);

	/**
	 * Aggregate query
	 * @param query
	 * @return
	 */
	AggregateResponse getAggregate(AggregatedQuery query);

	/**
	 * Aggregate query - raw response without tota count. CAN NOT BE CACHED.
	 * @param query
	 * @return
	 */
	List<AggregateRow> getRawAggregate(AggregatedQuery query);

	/**
	 * Count query
	 * @param query
	 * @return
	 */
	CountResponse getCount(CountQuery query) ;

	void clearCaches();

	/**
	 * Sets document and unit linkings to the row
	 * @param row
	 */
	void setJoinedRowLinkings(JoinedRow row);

	VerticaCustomQueriesDAO getCustomQueries();

	public interface VerticaCustomQueriesDAO {

		/**
		 * Get invasive species observations that have been controlled
		 * @return
		 */
		List<Annotation> getInvasiveControlled();

		/**
		 * Get target names that are not linked to any taxon
		 * @return
		 */
		List<UnlinkedTargetNameData> getUnlinkedTargetNames();

		/**
		 * Get user ids thata re not linked to any person
		 * @return
		 */
		List<UnlinkedUserIdsData> getUnlinkedUserIds();

		/**
		 * Get load statistics about all sources
		 * @return
		 */
		SourceDocumentLoadStatisticsData getLoadStats();

		/**
		 * Highest actual load time
		 * @return
		 */
		Date getLatestLoadTimestamp();

		Collection<String> getdDocumentIdsWithSecureLevel();
		Collection<String> getDocumentIdsWithQualityIssues();
		Collection<String> getDocumentIdsWithLocationIssues();
		Collection<String> getDocumentIdsFromCountry(Qname countryId);
		Collection<String> getDocumentIdsFromMunicipality(Qname municipalityId);
		Collection<String> getDocumentIdsNotFromCountry(Qname finland);
		Collection<String> getDocumentIdsWithMultipleMunicipalities();
		Collection<String> getDocumentIdsWithMultipleBioprovinces();
		Collection<String> getDocumentIdsFromSource(Qname sourceId);
		Collection<String> getDocumentIdsWithLargeAreas();
		Collection<String> getDocumentIdsFromCollection(Qname collectionId);
		Collection<String> getDocumentIdsWithMultipleImages();
		Collection<String> getDocumentIdsWithoutCoordinates();
		Collection<String> getDocumentIdsWithUnitImages();
		Collection<String> getDocumentIdsWithUnitsWithoutTaxonLinking();
		Collection<String> getDocumentIdsWithOldStartDate();
		Collection<String> getDocumentIdsWithUnknownPersons();
		Collection<String> getDocumentIdsWithUnitAnnotations();
		Collection<String> getDocumentIdsWithUnitsMarkedNeedDet();
		Collection<String> getDocumentIdsWithoutTaxonLinking();
		Collection<String> getNoTargetUnitDocumentIds();
		Collection<String> getDodocumentIdsWithQuality(RecordQuality communityVerified);

		/**
		 * Is the taxon id used explicitly as a target name, taxon cenus target etc
		 * @param taxonId
		 * @return
		 */
		boolean isTaxonIdUsed(Qname taxonId);

		/**
		 * Return team members that match the search word - logic should follow the logic on teamMemberName filter (* is wild card etc)
		 * @param searchWord
		 * @return
		 */
		List<TeamMemberData> getTeamMembers(String searchWord);

		/**
		 * Execute a custom query. Strings &lt;SCHEMA&gt; and &lt;DIMENSION_SCHEMA&gt; in the sql are replaced with their respective actual schema names.
		 * @param sql
		 * @return {@link CustomQueryIterator} that you must close after using
		 */
		<T> ResultStream<T> getCustomQuery(String sql);

		/**
		 * Return observation count information of all master checklist taxa
		 * @return
		 */
		Collection<ObsCount> getTaxonObservationCounts();

		/**
		 * Get all document ids
		 * @return
		 */
		ResultStream<String> getAllDocumentIds();

	}



}
