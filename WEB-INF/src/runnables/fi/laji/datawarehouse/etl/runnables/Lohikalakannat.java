package fi.laji.datawarehouse.etl.runnables;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

/**
 * Virtavesien lohikalakannat https://ckan.ymparisto.fi/dataset/%7BE22D5675-EBA1-4441-8B7A-A222B5D464E5%7D
 * http://tun.fi/HR.4011
 *
 * Converts the data to Laji-ETL model and sends the JSON to ETL push API
 *
 * @author eopiirai
 *
 */
public class Lohikalakannat {

	private static final String ACCESS_TOKEN = "xxx";
	private static final String WAREHOUSE_PUSH_URL = "xxx/push";
	private static final String APPLICATION_JSON = "application/json";
	private static final Qname COLLECTION_ID = new Qname("HR.4011");

	public static void main(String[] args) {
		try {
			List<DwRoot> data = parseData();
			validate(data);
			//save(data);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void validate(List<DwRoot> data) throws CriticalParseFailure {
		for (DwRoot root : data) {
			root.getPublicDocument().validateIncomingIds();
		}
	}

	@SuppressWarnings("unused")
	private static void save(List<DwRoot> data) {
		try (HttpClientService client = new HttpClientService()) {
			saveWith(client, data);
		}
	}

	private static void saveWith(HttpClientService client, Collection<DwRoot> roots) {
		int i = 0;
		HttpPost post = new HttpPost(WAREHOUSE_PUSH_URL);
		post.addHeader("Authorization", ACCESS_TOKEN);
		for (DwRoot root : roots) {
			System.out.println("Storing " + (++i) + " / " + roots.size());
			String data = root.toJSON().toString();
			post.setEntity(new StringEntity(data, ContentType.create(APPLICATION_JSON, "utf-8")));
			try (CloseableHttpResponse res = client.execute(post)) {
				int status = res.getStatusLine().getStatusCode();
				if (status != 200) {
					throw new RuntimeException("Returned " + status + " with message " + getContents(res));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private static String getContents(CloseableHttpResponse response) {
		String message = "";
		try {
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				message = EntityUtils.toString(responseEntity, "UTF-8");
			}
		} catch (Exception e) {}
		return message;
	}

	private static List<DwRoot> parseData() throws Exception {
		List<DwRoot> roots = new ArrayList<>();
		for (Row row : parseRows()) {
			DwRoot root = new DwRoot(documentId(row), Const.LAJI_ETL_QNAME);
			root.setCollectionId(COLLECTION_ID);
			Document publicDocument = root.createPublicDocument();
			Document privateDocument = root.createPrivateDocument();
			parseDocument(row, root, publicDocument);
			parseDocument(row, root, privateDocument);
			roots.add(root);
		}
		return roots;
	}

	private static void parseDocument(Row r, DwRoot root, Document d) throws CriticalParseFailure {
		Gathering g = parseGathering(r, root.getDocumentId());
		Unit u = parseUnit(r, root.getDocumentId(), d.isPublic());
		g.addUnit(u);
		d.addGathering(g);
		if (given(r.modifiedDate))
			try {
				d.setModifiedDate(r.modifiedDate);
			} catch (DateValidationException e) {
				throw new ETLException(e);
			}
	}

	private static Unit parseUnit(Row r, Qname documentId, boolean isPublic) throws CriticalParseFailure {
		Unit u = new Unit(new Qname(documentId.toString()+"_U"));
		u.setReportedTaxonId(new Qname("MX.53115"));
		u.setTaxonVerbatim("Salmoniformes");

		u.setAbundanceUnit(AbundanceUnit.OCCURS_DOES_NOT_OCCUR);
		if (r.lohikalaPublic == 0) {
			u.setAbundanceString("0");
		} else {
			u.setAbundanceString("1");
			u.setBreedingSite(true);
			u.setWild(true);
		}

		if (isPublic) {
			u.addFact("Lohikalakanta", ""+r.lohikalaPublic);
		} else {
			u.addFact("Lohikalakanta", ""+r.lohikalaPrivate);
			if (given(r.tutkittu)) u.addFact("Tutkittu", r.tutkittu ? "Kyllä - sähkökalastusta tai muita tutkimuksia" : "Ei");
			if (given(r.erityiskohde)) u.addFact("Erityiskohde", r.erityiskohde ? "Kyllä - Kohteen läheisyydessä tapahtuvista toimeinpiteistä nuodatettava erityistä varovaisuutta. Otettava yhteyttä ELY-keskukseen." : "Ei");
		}

		u.setNotes(notes(r, isPublic));

		u.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		return u;
	}

	private static String notes(Row r, boolean isPublic) {
		String notes = "";
		if (r.lohikalaPublic == 0) {
			notes = "Kohteella ei ole lohikalakantaa tai kanta ei ole tiedossa";
		}
		if (isPublic && r.lohikalaPublic == 1) {
			notes = "Kohteella on lohikalakanta, jossa joko säännöllistä luontaista lisääntymistä (sekä istutus että alkuperäiset kannat) tai lohikaloja on havaittu, mutta luontainen lisääntyminen on epävarmaa";
		}
		if (!isPublic) {
			if (r.lohikalaPrivate == 1) notes = "Kohteella on havaittu lohikaloja ajoittain, mutta ei havaittua säännöllistä luontaista lisääntymistä tai istutuksia, jotka eivät ole havaitusti johtaneet säännölliseen luontaiseen lisääntymiseen";
			if (r.lohikalaPrivate == 2) notes = "Kohteella on säännöllistä luontaista lisääntymistä";
		}
		if (!isPublic && given(r.notesPrivate)) {
			notes = notes + "; " + r.notesPrivate;
		}
		return notes;
	}

	private static Gathering parseGathering(Row r, Qname documentId) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname(documentId.toString()+"_G"));
		g.setCountry("Suomi");
		g.setLocality(r.notesPublic);
		if (given(r.ely)) g.addFact("ELY", r.ely);
		if (given(r.paaJakoNro)) g.addFact("Päävesistöalue", r.paaJakoNro.toString());
		if (given(r.uomaNro)) g.addFact("Ranta10 uoma", r.uomaNro.toString());
		if (given(r.uomaId)) g.addFact("Uoma", r.uomaId.toString());
		try {
			g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2020"), DateUtils.convertToDate("31.12.2020")));
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		try {
			Geo geo = Geo.fromWKT(r.wkt, Type.EUREF);
			geo.validate();
			g.setGeo(geo);
		} catch (DataValidationException e) {
			throw new ETLException(r.wkt, e);
		}
		return g;
	}

	private static boolean given(Object o) {
		return o != null && !o.toString().isEmpty();
	}

	private static Qname documentId(Row r) {
		return Qname.fromURI(COLLECTION_ID.toURI() + "/" + r.objectId);
	}

	private static List<Row> parseRows() throws Exception {
		List<String> lines = FileUtils.readLines(new File("E:/esko-local/temp/lohikalat/VirtavesienLohikalakannatViranomaiset.csv"));
		System.out.println("Line count " + lines.size());
		List<Row> rows = new ArrayList<>();
		boolean header = true;
		for (String line : lines) {
			if (header) {
				header = false;
				continue;
			}
			try {
				rows.add(parseRow(line));
			} catch (Exception e) {
				throw new ETLException(line, e);
			}
		}
		return rows;
	}

	private static Row parseRow(String line) {
		List<String> parts = parseLine(line);
		Row r = new Row();
		r.wkt = parts.get(0);
		r.objectId = Long.valueOf(parts.get(1));
		r.uomaNro = longMayBeNull(parts.get(2));
		r.uomaId = longMayBeNull(parts.get(3));
		r.paaJakoNro = longMayBeNull(parts.get(4));
		r.ely = parts.get(5);
		r.tutkittu = bool(parts.get(6));
		r.erityiskohde = bool(parts.get(7));
		r.lohikalaPublic = Integer.valueOf(parts.get(8));
		r.lohikalaPrivate = Integer.valueOf(parts.get(9));
		r.notesPublic = parts.get(10);
		r.notesPrivate = parts.get(11);
		r.modifiedDate = date(parts.get(12));
		return r;
	}

	private static Date date(String string) {
		if (string == null) return null;
		try {
			return DateUtils.convertToDate(string.replace(" 00:00:00", ""), "yyyy/MM/dd");
		} catch (ParseException e) {
			throw new ETLException(string, e);
		}
	}

	private static Boolean bool(String string) {
		if (string == null) return null;
		if (string.equals("1")) return true;
		if (string.equals("0")) return false;
		return null;
	}

	private static List<String> parseLine(String line) {
		String[] parts = line.split(";");
		List<String> cleanedParts = new ArrayList<>();
		for (String s : parts) {
			cleanedParts.add(s.replace("\"", ""));
		}
		return cleanedParts;
	}

	private static Long longMayBeNull(String string) {
		if (string == null) return null;
		if (string.isEmpty()) return null;
		return Long.valueOf(string);
	}

	private static class Row {
		private String wkt;
		private long objectId;
		private Long uomaNro;
		private Long uomaId;
		private Long paaJakoNro;
		private String ely;
		private Boolean tutkittu;
		private Boolean erityiskohde;
		private int lohikalaPublic;
		private int lohikalaPrivate;
		private String notesPublic;
		private String notesPrivate;
		private Date modifiedDate;
	}

}
