package fi.laji.datawarehouse.etl.runnables;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import com.google.common.collect.Lists;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

/**
 * YÖPETI database dump; Yöperhosseuranta
 * http://tun.fi/HR.4511
 *
 * Converts the data to Laji-ETL model and sends the JSON to ETL push API
 *
 * @author eopiirai
 *
 */
public class Yopeti {

	private static final String FILENAME = "xxx/Koko_aineisto_Luomus.txt";
	private static final String ACCESS_TOKEN = "xxx";
	private static final String WAREHOUSE_PUSH_URL = "https://dw.laji.fi/push";
	private static final String APPLICATION_JSON = "application/json";
	private static final Qname COLLECTION_ID = new Qname("HR.4511");
	private static final Qname SYSTEM_ID = new Qname("KE.1501");

	public static void main(String[] args) {
		try {
			List<DwRoot> data = parseData();
			validate(data);
			System.out.println(data.get(0).toJSON().beautify());
			save(data);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void validate(List<DwRoot> data) throws Exception {
		List<String> errors = new ArrayList<>();
		Set<Qname> ids = new HashSet<>();
		for (DwRoot root : data) {
			try {
				root.getPublicDocument().validateIncomingIds();
				validateId(ids, root.getDocumentId());
				validateId(ids, root.getPublicDocument().getGatherings().get(0).getGatheringId());
				for (Unit u : root.getPublicDocument().getGatherings().get(0).getUnits()) {
					validateId(ids, u.getUnitId());
				}
			} catch (Exception e) {
				errors.add(e.getMessage());
			}
		}
		if (!errors.isEmpty()) {
			errors.forEach(e->System.out.println(e));
			throw new Exception("Had " + errors.size() + " validation errors");
		}
	}

	private static void validateId(Set<Qname> ids, Qname id) {
		if (ids.contains(id)) throw new IllegalStateException("Duplicate id " + id);
		ids.add(id);
	}

	@SuppressWarnings("unused")
	private static void save(List<DwRoot> data) {
		try (HttpClientService client = new HttpClientService()) {
			saveWith(client, data);
		}
	}

	private static void saveWith(HttpClientService client, List<DwRoot> roots) {
		int i = 0;
		HttpPost post = new HttpPost(WAREHOUSE_PUSH_URL);
		post.addHeader("Authorization", ACCESS_TOKEN);
		roots = roots.subList(12940, roots.size());
		for (List<DwRoot> batch : Lists.partition(roots, 100)) {
			JSONObject batchJson = new JSONObject();
			batchJson.setString("schema", "laji-etl");
			JSONArray batchRoots = batchJson.getArray("roots");
			for (DwRoot root : batch) {
				i++;
				batchRoots.appendObject(root.toJSON());
			}
			String data = batchJson.toString();
			System.out.println("Storing " + i + " / " + roots.size());
			post.setEntity(new StringEntity(data, ContentType.create(APPLICATION_JSON, "utf-8")));
			try (CloseableHttpResponse res = client.execute(post)) {
				int status = res.getStatusLine().getStatusCode();
				if (status != 200) {
					throw new RuntimeException("Returned " + status + " with message " + getContents(res));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private static String getContents(CloseableHttpResponse response) {
		String message = "";
		try {
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				message = EntityUtils.toString(responseEntity, "UTF-8");
			}
		} catch (Exception e) {}
		return message;
	}

	private static List<DwRoot> parseData() throws Exception {
		List<Observation> observations = parseObservations();
		sort(observations);
		return combineObservationsToDocument(observations);
	}

	private static List<DwRoot> combineObservationsToDocument(List<Observation> observations) throws CriticalParseFailure {
		List<DwRoot> roots = new ArrayList<>();
		int prevEventId = -1;
		DwRoot root = null;
		for (Observation o : observations) {
			if (root == null || prevEventId != o.eventId) {
				root = new DwRoot(documentId(o.eventId), SYSTEM_ID);
				root.setCollectionId(COLLECTION_ID);
				roots.add(root);
				Document d = root.createPublicDocument();
				d.addGathering(parseGathering(o, root.getDocumentId()));
				d.setNamedPlaceIdUsingQname(new Qname(COLLECTION_ID + "/P."+o.placeId));
				d.addKeyword(""+o.placeId);
				d.addKeyword(""+o.eventId);
				prevEventId = o.eventId;
			}
			if (o.occId != null) {
				root.getPublicDocument().getGatherings().get(0).addUnit(parseUnit(o, root.getDocumentId()));
			}
		}
		System.out.println("Observation count " + observations.size());
		System.out.println("Root/document count " + roots.size());
		if (countUnits(roots) != nonNullCount(observations)) throw new IllegalStateException(countUnits(roots) + "!=" + observations.size());
		return roots;
	}

	private static int nonNullCount(List<Observation> observations) {
		return (int) observations.stream().filter(o->o.occId != null).count();
	}

	private static int countUnits(List<DwRoot> roots) {
		int i = 0;
		for (DwRoot root : roots) {
			i += root.getPublicDocument().getGatherings().get(0).getUnits().size();
		}
		return i;
	}

	private static Unit parseUnit(Observation o, Qname documentId) throws CriticalParseFailure {
		Unit u = new Unit(new Qname(documentId.toString()+"/"+o.occId));
		u.setTaxonVerbatim(o.scientificName);
		if (o.count != null) u.setAbundanceString(""+o.count);
		u.addKeyword(""+o.occId);
		u.setIndividualCountFemale(o.countFemale);
		u.setIndividualCountMale(o.countMale);
		u.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		return u;
	}

	private static Gathering parseGathering(Observation o, Qname documentId) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname(documentId.toString()+"/G"));
		g.setCountry("Suomi");
		g.setMunicipality(municipality(o.placeName));
		g.setLocality(locality(o.placeName));
		try {
			g.setCoordinates(new Coordinates(o.etrN, o.etrE, Type.EUREF));
		} catch (DataValidationException e) {
			throw new IllegalStateException(e);
		}
		try {
			g.setEventDate(new DateRange(date(o.dateBegin), date(o.dateEnd)));
		} catch (DateValidationException | ParseException e) {
			throw new IllegalStateException(e);
		}
		return g;
	}

	private static Date date(String date) throws ParseException {
		return DateUtils.convertToDate(date, "yyyy-MM-dd");
	}

	private static String municipality(String placeName) {
		if (!placeName.contains(" ")) return null;
		return placeName.split(Pattern.quote(" "))[0];
	}

	private static String locality(String placeName) {
		if (!placeName.contains(" ")) return null;
		return placeName.split(Pattern.quote(" "))[1];
	}

	private static Qname documentId(int eventId) {
		return new Qname(COLLECTION_ID.toString() + "/" + eventId);
	}

	private static void sort(List<Observation> observations) {
		observations.sort(new Comparator<Observation>() {
			@Override
			public int compare(Observation o1, Observation o2) {
				int c = Integer.compare(o1.eventId, o2.eventId);
				if (c != 0) return c;
				Integer id1 = o1.occId == null ? -1 : o1.occId;
				Integer id2 = o2.occId == null ? -1 : o2.occId;
				return id1.compareTo(id2);
			}
		});
	}

	private static List<Observation> parseObservations() throws Exception {
		List<String> lines = FileUtils.readLines(new File(FILENAME));
		System.out.println("Line count " + lines.size());
		List<Observation> observations = new ArrayList<>();
		boolean header = true;
		for (String line : lines) {
			if (header) {
				header = false;
				continue;
			}
			try {
				observations.add(parseObservation(line));
			} catch (Exception e) {
				throw new RuntimeException(line, e);
			}
		}
		return observations;
	}

	private static Observation parseObservation(String line) {
		Observation o = new Observation();
		String[] parts = line.split(";");
		int i = 0;
		o.placeId = Integer.valueOf(parts[i++]);
		o.placeName = parts[i++];
		o.etrN = Integer.valueOf(parts[i++]);
		o.etrE = Integer.valueOf(parts[i++]);
		o.eventId = Integer.valueOf(parts[i++]);
		o.dateBegin = parts[i++];
		o.dateEnd = parts[i++];
		o.occId = intV(parts[i++]);
		o.scientificName = parts[i++];
		o.count = intV(parts[i++]);
		o.countFemale = intV(parts[i++]);
		o.countMale = intV(parts[i++]);
		return o;
	}

	private static Integer intV(String value) {
		if (value.equals("NULL")) return null;
		return Integer.valueOf(value);
	}

	private static class Observation {
		private int placeId;
		private String placeName;
		private int etrN;
		private int etrE;
		private int eventId;
		private String dateBegin;
		private String dateEnd;
		private Integer occId;
		private String scientificName;
		private Integer count;
		private Integer countFemale;
		private Integer countMale;
	}

}
