package fi.laji.datawarehouse.etl.runnables;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

/**
 * GPS-pannoilla merkittyjen metsäpeurojen paikkatietoainestot https://opendata.luke.fi/fi/dataset/metsapeurojen-paikkatieto
 * Top collection http://tun.fi/HR.4852
 * Summer: http://tun.fi/HR.5012
 * Winter: http://tun.fi/HR.5013
 * Spring/autumn http://tun.fi/HR.5014
 *
 * Converts the data to Laji-ETL model and sends the JSON to ETL push API
 *
 * @author eopiirai
 *
 */
public class ReindeerDensity {

	private static final String ACCESS_TOKEN = "xxx";
	private static final String WAREHOUSE_PUSH_URL = "xxx/push";
	private static final String SECRET_SALT = "xxx"; // stored to wiki: Luke Wild Forest Reindeer to FinBIF
	private static final String APPLICATION_JSON = "application/json";
	private static final File FOLDER = new File("E:\\esko-local\\git\\eskon-dokkarit\\data\\luke");

	public static void main(String[] args) {
		try {
			List<DwRoot> data = parseData();
			validate(data);
			//save(data);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void validate(List<DwRoot> data) throws CriticalParseFailure {
		Set<Qname> ids = new HashSet<>();
		for (DwRoot root : data) {
			root.getPublicDocument().validateIncomingIds();
			root.getPrivateDocument().validateIncomingIds();
			if (ids.contains(root.getDocumentId())) {
				throw new IllegalStateException("Same id twice: " + root.getDocumentId().toURI());
			}
			ids.add(root.getDocumentId());
		}
	}

	@SuppressWarnings("unused")
	private static void save(List<DwRoot> data) {
		try (HttpClientService client = new HttpClientService()) {
			saveWith(client, data);
		}
	}

	private static void saveWith(HttpClientService client, Collection<DwRoot> roots) {
		int i = 0;
		HttpPost post = new HttpPost(WAREHOUSE_PUSH_URL);
		post.addHeader("Authorization", ACCESS_TOKEN);
		for (DwRoot root : roots) {
			System.out.println("Storing " + (++i) + " / " + roots.size());
			String data = root.toJSON().toString();
			post.setEntity(new StringEntity(data, ContentType.create(APPLICATION_JSON, "utf-8")));
			try (CloseableHttpResponse res = client.execute(post)) {
				int status = res.getStatusLine().getStatusCode();
				if (status != 200) {
					throw new RuntimeException("Returned " + status + " with message " + getContents(res));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			sleep();
		}
	}

	private static void sleep() {
		try {Thread.sleep(50);} catch (InterruptedException e1) {}
	}

	private static String getContents(CloseableHttpResponse response) {
		String message = "";
		try {
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				message = EntityUtils.toString(responseEntity, "UTF-8");
			}
		} catch (Exception e) {}
		return message;
	}

	private static List<DwRoot> parseData() throws Exception {
		List<DwRoot> roots = new ArrayList<>();
		roots.addAll(parseData("HR.5012", "metsapeura_kesa_2021_2022.txt"));
		roots.addAll(parseData("HR.5013", "metsapeura_talvi_2021_2022.txt"));
		roots.addAll(parseData("HR.5014", "metsapeura_siirtyma_2021_2022.txt"));
		return roots;
	}

	private static Collection<? extends DwRoot> parseData(String collectionQname, String filename) throws Exception {
		List<DwRoot> roots = new ArrayList<>();
		Qname collectionId = new Qname(collectionQname);
		for (Row row : parseRows(filename)) {
			DwRoot root = new DwRoot(documentId(collectionId, row.wkt, SECRET_SALT), Const.LAJI_ETL_QNAME);
			root.setCollectionId(collectionId);
			Document privateDocument = root.createPrivateDocument();
			parseDocument(row, root, privateDocument);
			Document publicDocument = privateDocument.copy(Concealment.PUBLIC);
			publicDocument.setSecureLevel(SecureLevel.KM5);
			publicDocument.addSecureReason(SecureReason.CUSTOM);
			root.setPublicDocument(publicDocument);
			roots.add(root);
		}
		return roots;
	}

	private static void parseDocument(Row r, DwRoot root, Document d) throws CriticalParseFailure {
		Gathering g = parseGathering(r, root.getDocumentId());
		Unit u = parseUnit(r, root.getDocumentId());
		g.addUnit(u);
		d.addGathering(g);
	}

	private static Unit parseUnit(Row r, Qname documentId) throws CriticalParseFailure {
		Unit u = new Unit(new Qname(documentId.toString()+"_U"));
		u.setReportedTaxonId(new Qname("MX.200556"));
		u.setTaxonVerbatim("Rangifer tarandus fennicus");

		u.setAbundanceUnit(AbundanceUnit.RELATIVE_DENSITY);
		u.setAbundanceString(""+r.density);
		u.setWild(true);
		u.addSourceTag(Tag.EXPERT_TAG_VERIFIED);

		u.setRecordBasis(RecordBasis.MACHINE_OBSERVATION_SATELLITE_TRANSMITTER);
		return u;
	}

	private static Gathering parseGathering(Row r, Qname documentId) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname(documentId.toString()+"_G"));
		try {
			g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2008"), DateUtils.convertToDate("31.12.2021")));
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		try {
			Geo geo = Geo.fromWKT(r.wkt, Type.EUREF);
			geo.validate();
			g.setGeo(geo);
		} catch (DataValidationException e) {
			throw new ETLException(r.wkt, e);
		}
		return g;
	}

	public static Qname documentId(Qname collectionId, String wkt, String salt) {
		try {
			String id = wkt + salt;
			byte[] digest = MessageDigest.getInstance("MD5").digest(id.getBytes(StandardCharsets.UTF_8));
			String hex = (new HexBinaryAdapter()).marshal(digest);
			return Qname.fromURI(collectionId.toURI() + "/A." + hex);
		} catch (Exception e) {
			throw new ETLException(e);
		}
	}

	@Test
	public void docid() {
		Qname id = documentId(new Qname("HR.123"), "POLYGON(123,2323232)", "secretsalt");
		assertEquals("http://tun.fi/HR.123/A.84150E66140A6E04A6257A03F7FC4805", id.toURI());
		Qname again = documentId(new Qname("HR.123"), "POLYGON(123,2323232)", "secretsalt");
		assertEquals(id.toString(), again.toString());
		Qname other = documentId(new Qname("HR.123"), "POLYGON(9999)", "secretsalt");
		assertEquals("http://tun.fi/HR.123/A.15F65A2C3C4DC3C34A187F7F3681973C", other.toURI());
	}

	private static List<Row> parseRows(String filename) throws Exception {
		List<String> lines = FileUtils.readLines(new File(FOLDER, filename));
		System.out.println("Line count " + filename + ": " + lines.size());
		List<Row> rows = new ArrayList<>();
		boolean header = true;
		for (String line : lines) {
			if (header) {
				header = false;
				continue;
			}
			try {
				rows.add(parseRow(line));
			} catch (Exception e) {
				throw new ETLException(line, e);
			}
		}
		return rows;
	}

	private static Row parseRow(String line) {
		Row r = new Row();
		String[] parts = line.split(Pattern.quote("\t"));
		r.density = Double.valueOf(parts[0].replace(",", "."));
		r.wkt = parts[1];
		return r;
	}

	private static class Row {
		private Double density;
		private String wkt;
	}

}
