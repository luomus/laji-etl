package fi.laji.datawarehouse.etl.runnables;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

/**
 * Hangon lintuaseman pitkäaikaisaineisto https://www.tringa.fi/hangon-lintuasema/haliasdata/
 * http://tun.fi/HR.2931
 *
 * Converts the data to Laji-ETL model and sends the JSON to ETL push API
 *
 * Note: Make sure source file is first converted to UTF-8 encoding
 * Tips and other help: https://wiki.helsinki.fi/pages/viewpage.action?pageId=283222846
 *
 * @author eopiirai
 *
 */
public class Halias {

	private static final String FILENAME = "X:\\xx/xx.txt";
	private static final String ACCESS_TOKEN = "..."; // KE.398
	private static final String WAREHOUSE_PUSH_URL = ".../push";
	private static final String APPLICATION_JSON = "application/json";
	private static final Qname COLLECTION_ID = new Qname("HR.2931");

	public static void main(String[] args) {
		try {
			List<DwRoot> data = parseData();
			validate(data);
			save(data);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void validate(List<DwRoot> data) throws Exception {
		List<String> errors = new ArrayList<>();
		for (DwRoot root : data) {
			try {
				root.getPublicDocument().validateIncomingIds();
			} catch (Exception e) {
				errors.add(e.getMessage());
			}
		}
		if (!errors.isEmpty()) {
			errors.forEach(e->System.out.println(e));
			throw new Exception("Had " + errors.size() + " validation errors");
		}
	}

	@SuppressWarnings("unused")
	private static void save(List<DwRoot> data) {
		try (HttpClientService client = new HttpClientService()) {
			saveWith(client, data);
		}
	}

	private static void saveWith(HttpClientService client, Collection<DwRoot> roots) {
		int i = 0;
		int howManyToSkip = 0;
		HttpPost post = new HttpPost(WAREHOUSE_PUSH_URL);
		post.addHeader("Authorization", ACCESS_TOKEN);
		for (DwRoot root : roots) {
			++i;
			if (i < howManyToSkip) {
				System.out.println("Skipping " + i + " / " + roots.size());
				continue;
			}
			System.out.println("Storing " + i + " / " + roots.size());
			String data = root.toJSON().toString();
			post.setEntity(new StringEntity(data, ContentType.create(APPLICATION_JSON, "utf-8")));
			try (CloseableHttpResponse res = client.execute(post)) {
				int status = res.getStatusLine().getStatusCode();
				if (status != 200) {
					throw new RuntimeException("Returned " + status + " with message " + getContents(res));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private static String getContents(CloseableHttpResponse response) {
		String message = "";
		try {
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				message = EntityUtils.toString(responseEntity, "UTF-8");
			}
		} catch (Exception e) {}
		return message;
	}

	private static List<DwRoot> parseData() throws Exception {
		List<Observation> observations = parseObservations();
		sort(observations);
		return combineObservationsToDocument(observations);
	}

	private static List<DwRoot> combineObservationsToDocument(List<Observation> observations) throws CriticalParseFailure {
		List<DwRoot> roots = new ArrayList<>();
		Date prevDate = null;
		DwRoot root = null;
		for (Observation o : observations) {
			if (root == null || prevDate == null || !prevDate.equals(o.date)) {
				root = new DwRoot(documentId(o.date), Const.LAJI_ETL_QNAME);
				root.setCollectionId(COLLECTION_ID);
				roots.add(root);
				Document d = root.createPublicDocument();
				d.addGathering(parseGathering(o.date, root.getDocumentId()));
				prevDate = o.date;
			}
			root.getPublicDocument().getGatherings().get(0).addUnit(parseUnit(o, root.getDocumentId()));
		}
		System.out.println("Observation count " + observations.size());
		System.out.println("Root/document count " + roots.size());
		if (countUnits(roots) != observations.size()) throw new IllegalStateException();
		return roots;
	}

	private static int countUnits(List<DwRoot> roots) {
		int i = 0;
		for (DwRoot root : roots) {
			i += root.getPublicDocument().getGatherings().get(0).getUnits().size();
		}
		return i;
	}

	private static Unit parseUnit(Observation o, Qname documentId) throws CriticalParseFailure {
		Unit u = new Unit(new Qname(documentId.toString()+"#"+o.speciesCode));
		u.setTaxonVerbatim(o.species);
		u.addFact("Local", String.valueOf(o.local));
		u.addFact("Migr", String.valueOf(o.migr));
		u.addFact("Stand", String.valueOf(o.stand));
		u.addFact("Additional", String.valueOf(o.additional));
		u.addFact("Observed", String.valueOf(o.observed));
		u.addFact("Night_migr", String.valueOf(o.nightMigr));
		u.setAbundanceString(String.valueOf(o.local + o.migr + +intv(o.additional) + intv(o.nightMigr)));
		u.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		return u;
	}

	private static int intv(String nightMigr) {
		try {
			return Integer.valueOf(nightMigr);
		} catch (Exception e) {
			return 0;
		}
	}

	private static Gathering parseGathering(Date date, Qname documentId) throws CriticalParseFailure {
		Gathering g = new Gathering(new Qname(documentId.toString()+"#G"));
		g.setCountry("Suomi");
		g.setMunicipality("Hanko");
		try {
			g.setCoordinates(new Coordinates(6638601, 6641987, 3268232, 3271623, Type.YKJ));
		} catch (DataValidationException e) {
			throw new IllegalStateException(e);
		}
		try {
			g.setEventDate(new DateRange(date));
		} catch (DateValidationException e) {
			throw new IllegalStateException(e);
		}
		return g;
	}

	private static Qname documentId(Date date) {
		return new Qname("MKH." + DateUtils.format(date, "yyyyMMdd"));
	}

	private static void sort(List<Observation> observations) {
		observations.sort(new Comparator<Observation>() {
			@Override
			public int compare(Observation o1, Observation o2) {
				int c = o1.date.compareTo(o2.date);
				if (c != 0) return c;
				return Integer.compare(o1.speciesCode, o2.speciesCode);
			}
		});
	}

	private static List<Observation> parseObservations() throws Exception {
		List<String> lines = FileUtils.readLines(new File(FILENAME));
		System.out.println("Line count " + lines.size());
		List<Observation> observations = new ArrayList<>();
		boolean header = true;
		for (String line : lines) {
			if (header) {
				header = false;
				continue;
			}
			if (line.isEmpty()) continue;
			try {
				observations.add(parseObservation(line));
			} catch (Exception e) {
				throw new RuntimeException(line, e);
			}
		}
		return observations;
	}

	private static Observation parseObservation(String line) throws ParseException {
		Observation o = new Observation();
		String[] parts = line.split("\t");
		o.species = parts[4];
		o.speciesCode = Integer.valueOf(parts[5]);
		o.date = parseDate(parts[6], parts[8]);
		o.local = Integer.valueOf(parts[9]);
		o.migr = Integer.valueOf(parts[10]);
		o.stand = Integer.valueOf(parts[11]);
		o.additional = parts[12];
		o.observed = Boolean.valueOf(parts[13]);
		o.nightMigr = parts[14];
		return o;
	}

	private static Date parseDate(String date, String year) throws ParseException {
		return DateUtils.convertToDate(date + "." + year, "dd.MM.yyyy");
	}

	private static class Observation {
		private String species; //		(5) [Species_Abb] Lajin tai lajiryhmän lyhenne
		private int speciesCode; //		(6) [Species_code] Lajin tai lajiryhmän numero
		private Date date; //		(7) [Date] Päivämäärä muodossa PÄIVÄ.KUUKAUSI.VUOSI (9) [Year] Havaintopäivän vuosiluku.
		private int local; //		(10) [Local] Havaittujen paikallisten lintujen lukumäärä kyseiseltä päivämäärältä. Kyyhkyillä, käillä, pöllöillä, kehrääjillä, kirskujilla, säihkylinnuilla, tikoilla ja varpuslinnuilla määrät tarkoittavat lukumäärää, joka on havaittu niemen kärjessä olevalta ydinalueelta asemarakennuksen lähettyviltä. Muilla lajeilla (vesi- ja rantalinnut, petolinnut) sarakkeen luku kuvaa koko seuranta-alueella havaittujen yksilöiden määrää mukaan lukien Gåsörsuddenilta havaitut linnut.
		private int migr; //		(11) [Migr] Koko päivän aikana muutolla havaittujen lintujen lukumäärä poislukien yömuuttajat.
		private int stand; //		(12) [Stand] Vakioidulla muutonhavainnointijaksolla havaitut muuttajat. Vakiohavainnointi alkaa auringonnoususta ja kestää 1.4.–1.11. välisenä aikana neljä tuntia, muina aikoina kaksi tuntia. Huomaa, että vakiota ei ole suoritettu kaikkina havaintopäivinä, joten nolla-arvo ei välttämättä tarkoita, että vakio on suoritettu ja yhtään lintua ei ole havaittu.
		private String additional;//		(13) [Additional] Aseman ydinalueen ulkopuolella (ns. lisäalue, joka sisältää mm. Gåsörsuddenin) havaittujen yksilöiden lukumääriä kyyhkyjen, käkien, pöllöjen, kehrääjien, kirskujien, säihkylintujen, tikkojen ja varpuslintujen osalta. Mikäli lajia ei ole havaittu ydinalueella (ks. kohta 9 [Local]), mutta laji on havaittu lisäalueella, lukumäärä on kirjattu. Lukumäärä on voitu kirjata lisäalueelta myös tilanteessa, jossa laji on havaittu ydinalueella, mutta lisäalueen havaintomäärä on huomattavan suuri (esimerkiksi muuton huippu tai yleisesti harvalukuinen laji). Tämän sarakkeen tiedot ovat luonteeltaan satunnaishavaintoja, joiden käyttöä ei suositella analyyseissä ilman tarkkaa harkintaa.
		private boolean observed; //		(14) [Observed] Etenkin aseman alkuaikoina joidenkin lajien paikallislukumäärää ei välttämättä ole kirjattu, vaikka laji on havaittu kyseisenä päivänä (ns. + merkintä). Mikäli laji on havaittu paikallisena, mutta lukumäärää ei ole laskettu, saa tämä sarake arvon TRUE. Mikäli lajin paikallisten lukumäärä on puolestaan kirjattu (vaikka se olisi 0) saa sarake arvon FALSE.
		private String nightMigr; //		(15) [Night_migr] Satunnaisia yömuuttavien lajien havaintomääriä. Olennainen etenkin kaulushaikaralle, jolla valtaosa havainnoista on öiseen aikaan muuttavia yksilöitä.
	}

}
