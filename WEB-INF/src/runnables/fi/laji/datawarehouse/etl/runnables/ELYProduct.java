package fi.laji.datawarehouse.etl.runnables;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Converts full download files to grouped/refined data product to be loaded to GIS
 *
 * @author eopiirai
 *
 */
public class ELYProduct {

	private static final String DOWNLOADFOLDER = "E:\\esko-local\\temp\\HBF.9151";
	private static final Set<Field> FIELDS = Utils.set(
			new Field("Taxon.ID", Method.SPECIAL_GROUPBY),
			new Field("Taxon.ScientificName", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.FinnishName", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.SwedishName", Method.FOLLOWS_DIRECTLY),
			new Field("Unit.TaxonVerbatim", Method.SPECIAL_GROUPBY),
			new Field("Taxon.LatestRedListStatus", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.Lajiturva", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.StatusesFI", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.InformalGroupFI", Method.FOLLOWS_DIRECTLY),
			new Field("Taxon.TaxonomicOrder", Method.FOLLOWS_DIRECTLY),
			// unit.linkings.taxon.primaryHabitat
			// unit.linkins.taxon.sensitive
			new Field("Unit.Interpretations.RecordQuality", Method.CONCATENATE_UNIQUE),
			new Field("Unit.Det", Method.CONCATENATE_UNIQUE),
			new Field("Unit.UnitID", Method.FIRST),
			new Field("Document.Keywords", Method.CONCATENATE_UNIQUE), // should be unit.keywords
			new Field("Unit.Abundance", Method.FIRST),
			new Field("Unit.AbundanceUnit", Method.GROUPBY),
			new Field("Unit.Interpretations.IndividualCount", Method.SUM),
			new Field("Unit.RecordBasis", Method.CONCATENATE_UNIQUE),
			new Field("Unit.Sex", Method.CONCATENATE_UNIQUE),
			new Field("Unit.LifeStage", Method.CONCATENATE_UNIQUE),
			new Field("Unit.BreedingSite", Method.GROUPBY),
			new Field("Unit.AtlasCode", Method.MAX),
			new Field("Unit.AtlasClass", Method.MAX),
			new Field("Unit.Notes", Method.CONCATENATE_UNIQUE),
			new Field("Gathering.TeamMembers", Method.GROUPBY),
			new Field("Gathering.Notes", Method.CONCATENATE_UNIQUE),
			new Field("Gathering.Interpretations.Municipality", Method.GROUPBY),
			new Field("Gathering.LocalityVerbatim", Method.GROUPBY),
			new Field("Gathering.Interpretations.Bioprovince", Method.FOLLOWS_DIRECTLY),
			new Field("Gathering.Date.Begin", Method.GROUPBY),
			new Field("Gathering.Date.End", Method.GROUPBY),
			new Field("Gathering.Conversions.WGS84_WKT", Method.GROUPBY),
			new Field("Gathering.Conversions.ETRS-TM35FINCenterPoint.Lat(N)", Method.FOLLOWS_DIRECTLY),
			new Field("Gathering.Conversions.ETRS-TM35FINCenterPoint.Lon(E)", Method.FOLLOWS_DIRECTLY),
			new Field("Gathering.Interpretations.CoordinateAccuracy", Method.FOLLOWS_DIRECTLY),
			new Field("Gathering.StateLand", Method.FOLLOWS_DIRECTLY),
			new Field("Document.CollectionID", Method.GROUPBY),
			// collectionName
			new Field("Document.Linkings.CollectionQuality", Method.FOLLOWS_DIRECTLY),
			new Field("Document.DataSecureReasons", Method.CONCATENATE_UNIQUE),
			new Field("Document.OriginalDataSource", Method.GROUPBY),
			new Field("Document.MonitoringSiteType", Method.GROUPBY),
			new Field("Document.MonitoringSiteStatus", Method.GROUPBY),
			new Field("Document.Notes", Method.CONCATENATE_UNIQUE),
			new Field("Document.DocumentID", Method.FIRST),
			new Field("Document.GatheringID", Method.FIRST)
			);
	private static final Set<Field> FACTS = Utils.set(
			new Field("Seurattava laji", Method.GROUPBY),
			new Field("Sijainnin tarkkuusluokka", Method.GROUPBY),
			new Field("Havainnon laatu", Method.GROUPBY),
			new Field("Peittävyysprosentti", Method.GROUPBY),
			new Field("Havainnon määrän yksikkö", Method.GROUPBY),
			new Field("Vesistöalue", Method.FIRST),
			new Field("Merialueen tunniste", Method.FIRST)
			);

	public static void main(String[] args) {
		try {
			System.out.println("Parse and group rows");
			Collection<List<Row>> grouped = parseRows();
			System.out.println("Write rows");
			write(grouped);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void write(Collection<List<Row>> grouped) throws Exception {
		OutputStream out = null;
		OutputStreamWriter writer = null;
		try {
			out = new FileOutputStream(new File(DOWNLOADFOLDER, "joined_"+DateUtils.getFilenameDatetime()+".tsv"), false);
			writer = new OutputStreamWriter(out, "UTF-8");
			writer.write(header());
			int i = 0;
			for (List<Row> r : grouped) {
				if (i % 1000 == 0) System.out.println("Writing results " + i + "/" + grouped.size());
				writer.write(generate(r));
				i++;
			}
		} finally {
			if (writer != null) writer.close();
			if (out != null) out.close();
		}

	}

	private static String header() {
		StringBuilder b = new StringBuilder();
		for (Field f : FIELDS) {
			b.append(f.name).append("\t");
		}
		for (Field f : FACTS) {
			b.append(f.name).append("\t");
		}
		b.append("ZeroOcc\t");
		b.append("GroupCount");
		b.append("\n");
		return b.toString();
	}

	private static String generate(List<Row> rows) {
		// TODO	ELY-keskukset
		StringBuilder b = new StringBuilder();
		for (Field f : FIELDS) {
			b.append(generate(f, rows)).append("\t");
		}
		for (Field f : FACTS) {
			// currently there are only groupby/first fields
			b.append(rows.get(0).f(f.name)).append("\t");
		}
		String count = rows.get(0).v("Unit.Interpretations.IndividualCount");
		b.append("0".equals(count)).append("\t");
		b.append(rows.size());
		b.append("\n");
		return b.toString();
	}

	private static String generate(Field f, List<Row> rows) {
		if (f.isFirstValue() || rows.size() == 1) {
			return rows.get(0).v(f.name);
		}
		if (f.method == Method.SUM) {
			int i = 0;
			for (Row r : rows) {
				try {
					i += Integer.valueOf(r.v(f.name));
				} catch (Exception e) {}
			}
			return String.valueOf(i);
		}
		if (f.method == Method.MAX) {
			String max = "";
			for (Row r : rows) {
				String v = r.v(f.name);
				if (v.compareTo(max) > 0) max = v;
			}
			return max;
		}
		if (f.method == Method.CONCATENATE_UNIQUE) {
			Set<String> values = new TreeSet<>();
			for (Row r : rows) {
				String v = r.v(f.name);
				if (!given(v)) continue;
				values.add(v);
			}
			return values.stream().collect(Collectors.joining("; "));
		}
		throw new IllegalStateException("Unknown method " + f.method);
	}

	private static Collection<List<Row>> parseRows() throws Exception {
		Map<String, List<Fact>> documentFacts = parseFacts(dfFile());
		Map<String, List<Fact>> gatheringFacts = parseFacts(gfFile());
		Map<String, List<Fact>> unitFacts = parseFacts(ufFile());

		System.out.println("Reading and parsing rows file");

		Map<String, List<Row>> grouped = new HashMap<>();

		List<String> headers = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(rowsFile()), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (headers == null) {
					headers = headers(line);
				} else {
					Row row = parse(headers, line);
					row.addFacts(getFacts(documentFacts, row.getDocumentId()));
					row.addFacts(getFacts(gatheringFacts, row.getGatheringId()));
					row.addFacts(getFacts(unitFacts, row.getUnitId()));
					group(grouped, row);
				}
			}
		} finally {
			if (reader != null) reader.close();
		}
		return grouped.values();
	}

	private static void group(Map<String, List<Row>> grouped, Row row) {
		if (!grouped.containsKey(row.hash())) {
			grouped.put(row.hash(), new ArrayList<>());
		}
		grouped.get(row.hash()).add(row);
	}

	private static Row parse(List<String> headers, String line) {
		Iterator<String> header = headers.iterator();
		Map<String, String> data = new HashMap<>();
		for (String s : line.split(Pattern.quote("\t"))) {
			String field = header.next();
			if (!NEEDED_FIELDS.contains(field)) continue;
			data.put(field, s.trim());
		}
		Row row = new Row(data);
		return row;
	}

	private static List<Fact> getFacts(Map<String, List<Fact>> facts, String id) {
		if (!facts.containsKey(id)) return null;
		return facts.get(id);
	}

	private static Map<String, List<Fact>> parseFacts(File file) throws Exception {
		System.out.println("Reading and parsing " + file.getName());
		Map<String, List<Fact>> facts = new HashMap<>();
		boolean header = true;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(rowsFile()), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (header) {
					header = false;
					continue;
				}
				String[] parts = line.split(Pattern.quote("\t"));
				String id = parts[0];
				String fact = parts[1];
				String value = parts[2];
				if (!NEEDED_FACTS.contains(fact)) continue;
				if (!facts.containsKey(id)) {
					facts.put(id, new ArrayList<>());
				}
				facts.get(id).add(new Fact(fact, value));
			}
		} finally {
			if (reader != null) reader.close();
		}
		return facts;
	}

	private static List<String> headers(String line) {
		List<String> headers = new ArrayList<>();
		for (String s : line.split(Pattern.quote("\t"))) {
			headers.add(s.trim());
		}
		return headers;
	}

	private static File rowsFile() {
		return file("rows_");
	}

	private static File file(String name) {
		for (File f : new File(DOWNLOADFOLDER).listFiles()) {
			if (f.getName().startsWith(name)) return f;
		}
		throw new IllegalStateException();
	}

	private static File ufFile() {
		return file("unit_facts_");
	}

	private static File gfFile() {
		return file("gathering_facts_");
	}

	private static File dfFile() {
		return file("document_facts_");
	}

	public static boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	enum Method { GROUPBY, SPECIAL_GROUPBY, FOLLOWS_DIRECTLY, FIRST, SUM, MAX, CONCATENATE_UNIQUE }

	private static class Field {
		String name;
		Method method;
		private Field(String name, Method method) {
			this.name = name;
			this.method = method;
		}
		public boolean isFirstValue() {
			if (method == Method.GROUPBY) return true;
			if (method == Method.SPECIAL_GROUPBY) return true;
			if (method == Method.FOLLOWS_DIRECTLY) return true;
			if (method == Method.FIRST) return true;
			return false;
		}
	}

	private static final Set<String> NEEDED_FIELDS = FIELDS.stream().map(f->f.name).collect(Collectors.toSet());
	private static final Set<String> NEEDED_FACTS = FACTS.stream().map(f->f.name).collect(Collectors.toSet());
	private static final Set<String> GROUP_BY_FIELDS = FIELDS.stream().filter(f->f.method == Method.GROUPBY).map(f->f.name).collect(Collectors.toSet());
	private static final Set<String> GROUP_BY_FACTS = FACTS.stream().filter(f->f.method == Method.GROUPBY).map(f->f.name).collect(Collectors.toSet());

	private static class Row {
		private final Map<String, String> fields;
		private List<Fact> facts = null;

		public Row(Map<String, String> data) {
			this.fields = data;
		}
		private String hash = null;
		public String hash() {
			if (hash != null) return hash;
			StringBuilder b = new StringBuilder();

			// Special handling for taxon id and if no taxon linking for taxon verbatim
			b.append(given(v("Taxon.ID")) ? v("Taxon.ID") : v("Unit.TaxonVerbatim")).append("\t");

			// Special handling for zero / non-zero occurrences
			String count = v("Unit.Interpretations.IndividualCount");
			b.append("0".equals(count)).append("\t");

			for (String f : GROUP_BY_FIELDS) {
				b.append(v(f)).append("\t");
			}
			for (String f : GROUP_BY_FACTS) {
				b.append(f(f)).append("\t");
			}
			hash = b.toString();
			return hash;
		}
		private String f(String field) {
			if (facts == null) return null;
			for (Fact f : facts) {
				if (f.getFact().equals(field)) return f.getValue();
			}
			return null;
		}
		private String v(String field) {
			return fields.get(field);
		}
		public String getDocumentId() {
			return v("Document.DocumentID");
		}
		public String getGatheringId() {
			return v("Gathering.GatheringID");
		}
		public String getUnitId() {
			return v("Unit.UnitID");
		}
		public void addFacts(List<Fact> facts) {
			if (facts == null || facts.isEmpty()) return;
			if (this.facts == null) this.facts = new ArrayList<>();
			this.facts.addAll(facts);
		}
	}


}
