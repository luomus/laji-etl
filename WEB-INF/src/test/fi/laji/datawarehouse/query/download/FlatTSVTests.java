package fi.laji.datawarehouse.query.download;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.DAOStub;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.query.download.FakeFileCreator.FakeFile;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.rowhandlers.FlatTSVRowHandlerImple;
import fi.laji.datawarehouse.query.download.util.File;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.java.tests.commons.taxonomy.TaxonContainerStub;

public class FlatTSVTests {

	public static class FakeDAOLabels extends DAOStub {
		@Override
		public LocalizedText getLabels(Qname propertyQname) {
			return new LocalizedText().set("fi", "stubbed-label-fi-for-"+propertyQname);
		}
	}

	public static class FakeTaxonContainer extends TaxonContainerStub {
		@Override
		public LocalizedText getInformalTaxonGroupNames(Set<Qname> informalGroups) {
			return new LocalizedText().set("fi", "group-fi-stubbed").set("en", "groups-en-stubbed");
		}

		@Override
		public LocalizedText getAdministrativeStatusNames(Set<Qname> informalGroups) {
			return new LocalizedText().set("fi", "statuses-fi-stubbed");
		}

	}

	protected FakeFileCreator fileCreator;

	@Before
	public void init() {
		fileCreator = new FakeFileCreator();
	}

	@Test
	public void testTsv() throws Exception {
		Set<DownloadInclude> downloadIncludes = Utils.set(
				DownloadInclude.UNIT_FACTS, // asked and exists so file created
				DownloadInclude.UNIT_MEDIA, // asked and exists so file created
				DownloadInclude.GATHERING_MEDIA // asked but none exist so file not created
				); // document facts exist but not asked so file not created

		FlatTSVRowHandlerImple handler = new FlatTSVRowHandlerImple(fileCreator, new FakeDAOLabels());

		JoinedRow row = createTestData();
		handler.handle(row, downloadIncludes);
		handler.close();

		// DownloadInclude.UNIT_FACTS,  asked and exists so file created
		// DownloadInclude.UNIT_MEDIA,  asked and exists so file created
		// DownloadInclude.GATHERING_MEDIA  asked but none exist so file not created
		// document facts exist but not asked so file not created
		assertEquals(4, fileCreator.getWrittenFiles().size());
		assertTrue(created("rows"));
		assertTrue(created("unit_media"));
		assertTrue(created("unit_facts"));
		assertTrue(created("samples"));

		FakeFile rows = (FakeFile) fileCreator.getFile("rows");
		assertEquals(2, rows.getLines().size());

		String headerLine = rows.getLines().get(0);
		String dataLine = rows.getLines().get(1);

		assertEquals("" +
				"Unit.UnitID	Unit.UnitOrder	Unit.TaxonVerbatim	Unit.Quality.Issue.Issue	Unit.Quality.Issue.Source	Unit.Quality.Issue.Message	Unit.ReportedTaxonConfidence	Taxon.ID	Taxon.TaxonomicOrder	Taxon.ScientificName	Taxon.FinnishName	Taxon.EnglishName	Taxon.SwedishName	Taxon.LatestRedListStatus	Taxon.Lajiturva	Taxon.StatusesFI	Taxon.InformalGroupFI	Taxon.InformalGroupEN	Unit.NameAccordingTo	Unit.Abundance	Unit.AbundanceUnit	Unit.Interpretations.AnnotatedTaxonId	Unit.Interpretations.IndividualCount	Unit.Interpretations.PairCount	Unit.Interpretations.InvasiveControl	Unit.Interpretations.RecordQuality	Unit.Interpretations.Tags	Unit.RecordBasis	Unit.DetailedRecordBasis	Unit.TypeSpecimen	Unit.Det	Unit.Sex	Unit.LifeStage	Unit.Wild	Unit.Alive	Unit.Local	Unit.Keywords	Unit.BreedingSite	Unit.PlantStatusCode	Unit.AtlasCode	Unit.AtlasClass	Unit.IndividualID	Unit.Notes	Unit.AnnotationCount	Unit.MediaCount	Unit.SampleCount	Gathering.GatheringID	Gathering.GatheringOrder	Gathering.GatheringSection	Gathering.Quality.Issue.Issue	Gathering.Quality.Issue.Source	Gathering.Quality.Issue.Message	Gathering.Quality.TimeIssue.Issue	Gathering.Quality.TimeIssue.Source	Gathering.Quality.TimeIssue.Message	Gathering.Quality.LocationIssue.Issue	Gathering.Quality.LocationIssue.Source	Gathering.Quality.LocationIssue.Message	Gathering.Date.Begin	Gathering.Date.End	Gathering.HourBegin	Gathering.HourEnd	Gathering.DisplayDateTime	Gathering.TeamMembers	Gathering.Conversions.WGS84.LatMin(N)	Gathering.Conversions.WGS84.LatMax(N)	Gathering.Conversions.WGS84.LonMin(E)	Gathering.Conversions.WGS84.LonMax(E)	Gathering.Conversions.WGS84CenterPoint.Lat(N)	Gathering.Conversions.WGS84CenterPoint.Lon(E)	Gathering.Conversions.ETRS-TM35FINCenterPoint.Lat(N)	Gathering.Conversions.ETRS-TM35FINCenterPoint.Lon(E)	Gathering.Conversions.ETRS-TM35FIN.LatMin(N)	Gathering.Conversions.ETRS-TM35FIN.LatMax(N)	Gathering.Conversions.ETRS-TM35FIN.LonMin(E)	Gathering.Conversions.ETRS-TM35FIN.LonMax(E)	Gathering.Conversions.YKJ.LatMin(N)	Gathering.Conversions.YKJ.LatMax(N)	Gathering.Conversions.YKJ.LonMin(E)	Gathering.Conversions.YKJ.LonMax(E)	Gathering.Conversions.YKJ_10KM.Lat(N)	Gathering.Conversions.YKJ_10KM.Lon(E)	Gathering.Conversions.YKJ_1KM.Lat(N)	Gathering.Conversions.YKJ_1KM.Lon(E)	Gathering.Conversions.Century	Gathering.Conversions.Decade	Gathering.Conversions.Year	Gathering.Conversions.Month	Gathering.Conversions.Day	Gathering.Conversions.DayOfYearBegin	Gathering.Conversions.DayOfYearEnd	Gathering.Conversions.WGS84_WKT	Gathering.Interpretations.CoordinateAccuracy	Gathering.Interpretations.CoordinateSource	Gathering.Interpretations.Country	Gathering.Interpretations.Bioprovince	Gathering.Interpretations.Municipality	Gathering.Interpretations.CountrySource	Gathering.Interpretations.BioProvinceSource	Gathering.Interpretations.MunicipalitySource	Gathering.StateLand	Gathering.AccurateArea	Gathering.HigherGeographyVerbatim	Gathering.CountryVerbatim	Gathering.MunicipalityVerbatim	Gathering.BioProvinceVerbatim	Gathering.ProvinceVerbatim	Gathering.LocalityVerbatim	Gathering.Notes	Gathering.MediaCount	Document.DocumentID	Document.DataSecureLevel	Document.DataSecureReasons	Document.PartialDocument	Document.CollectionID	Document.License	Document.OriginalDataSource	Document.Linkings.CollectionQuality	Document.Quality.Issue.Issue	Document.Quality.Issue.Source	Document.Quality.Issue.Message	Document.SourceID	Document.NamedPlaceID	Document.MonitoringSiteType	Document.MonitoringSiteStatus	Document.Keywords	Document.FirstLoadDate	Document.LoadDate	Document.Created	Document.Modified	Document.Notes	Document.MediaCount",
				headerLine);

		assertEquals("" +
				"http://tun.fi/KS.123#U1	0	sinitintti					http://tun.fi/MX.1	0	Tsiritis piritis	sinitiainen	bluetit	blåtit	LC 2019	stubbed-label-fi-for-MX.threatenedStatusStatutoryProtected	statuses-fi-stubbed	group-fi-stubbed	groups-en-stubbed					1				CHECK; INVASIVE_FULL			false														0	1	1	http://tun.fi/KS.123#G1	0											2016-02-05	2016-02-07			2016-02-05 - 2016-02-07	member1; member2	60.0	60.0	30.0	30.0	60.0	30.0																																true						lokaalipaikka	g notes	0	http://tun.fi/KS.123	NONE		false	http://tun.fi/HR.1							http://tun.fi/KE.1				1234; hyn:AX23.21	2022-10-26	2022-10-26			noottia \"jup\"	0",
				dataLine);

		assertEquals("http://tun.fi/KS.123", getValue("Document.DocumentID", headerLine, dataLine));
		assertEquals("1234; hyn:AX23.21", getValue("Document.Keywords", headerLine, dataLine));
		assertEquals("member1; member2", getValue("Gathering.TeamMembers", headerLine, dataLine));
		assertEquals("noottia \"jup\"", getValue("Document.Notes", headerLine, dataLine));
		assertEquals("2016-02-05", getValue("Gathering.Date.Begin", headerLine, dataLine));

		FakeFile unitMedia = (FakeFile) fileCreator.getFile("unit_media");
		FakeFile unitFacts = (FakeFile) fileCreator.getFile("unit_facts");

		assertEquals(2, unitMedia.getLines().size());
		assertEquals(3, unitFacts.getLines().size());

		assertEquals("Parent,Type,URL,ThumbnailURL,Author,Caption,License,CopyrightOwner", unitMedia.getLines().get(0).replace("\t", ","));
		assertEquals("http://tun.fi/KS.123#U1,IMAGE,https://image.com/1.jpg,,,,,", unitMedia.getLines().get(1).replace("\t", ","));
		assertEquals("Parent,Fact,Value,IntValue,DecimalValue", unitFacts.getLines().get(0).replace("\t", ","));
		assertEquals("http://tun.fi/KS.123#U1,f1,v1,,", unitFacts.getLines().get(1).replace("\t", ","));
		assertEquals("http://tun.fi/KS.123#U1,f2,2,5,,2.5", unitFacts.getLines().get(2).replace("\t", ","));

		FakeFile sample = (FakeFile) fileCreator.getFile("samples");
		assertEquals(2, sample.getLines().size());
		assertEquals("Parent	SampleID	CollectionID	Keywords	Type	Quality	Status	Material	Multiple	SampleOrder	Notes", sample.getLines().get(0));
		assertEquals("http://tun.fi/KS.123#U1	http://tun.fi/S1	http://tun.fi/HR.123	keyw; keyw2	http://tun.fi/sometype			http://tun.fi/mat	false	0	", sample.getLines().get(1));

		for (File f : fileCreator.getWrittenFiles()) {
			assertTrue(f.getName(), ((FakeFile)f).closed);
		}
	}

	protected JoinedRow createTestData() throws Exception {
		Unit unit = new Unit(new Qname("KS.123#U1"));
		Gathering gathering = new Gathering(new Qname("KS.123#G1"));
		Document document = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("KS.123"), new Qname("HR.1"));
		JoinedRow row = new JoinedRow(unit, gathering, document);

		unit.setTaxonVerbatim("sinitintti");
		unit.addFact("f1", "v1");
		unit.addFact("f2", "2,5");
		unit.addMedia(new MediaObject(MediaType.IMAGE, "http://image.com/1.jpg"));
		unit.createInterpretations().setEffectiveTags((ArrayList<Tag>) Utils.list(Tag.CHECK, Tag.INVASIVE_FULL));
		Taxon taxon = new Taxon(new Qname("MX.1"), new FakeTaxonContainer());
		unit.setLinkings(new UnitDWLinkings());
		unit.getLinkings().setTaxon(taxon);
		unit.getLinkings().setOriginalTaxon(taxon);
		taxon.setScientificName("Tsiritis piritis");
		taxon.addVernacularName("fi", "sinitiainen");
		taxon.addVernacularName("sv", "blåtit");
		taxon.addVernacularName("en", "bluetit");
		taxon.addAdministrativeStatus(new Qname("MX.finlex160_1997_appendix4_specialInterest_2021"));
		taxon.setRedListStatus2019Finland(new Qname("MX.iucnLC"));

		document.setNotes("noottia\n\r \"jup\"");
		document.addFact("f1", "v1");
		document.addKeyword("1234");
		document.addKeyword("hyn:AX23.21");
		document.setLoadTimeNow();
		document.setFirstLoadTime(1666788700L);
		document.setLoadTime(1666788700L);
		gathering.setConversions(new GatheringConversions());
		gathering.getConversions().setWgs84(new Coordinates(60, 30, Type.WGS84).setAccuracyInMeters(100000));
		gathering.addTeamMember("member1");
		gathering.addTeamMember("member2");
		gathering.setNotes("g notes");
		gathering.setLocality("lokaalipaikka");
		gathering.setEventDate(new DateRange(DateUtils.convertToDate("5.2.2016"), DateUtils.convertToDate("7.2.2016")));
		gathering.setAccurateArea(true);

		Sample sample = new Sample(new Qname("S1"));
		sample.setCollectionId(new Qname("HR.123"));
		sample.setTypeUsingQname(new Qname("sometype"));
		sample.addKeyword("keyw");
		sample.addKeyword("keyw2");
		sample.setMaterialUsingQname(new Qname("mat"));
		unit.addSample(sample);

		return row;
	}

	protected boolean created(String fileName) {
		return fileCreator.getFile(fileName) != null;
	}

	private String getValue(String field, String headerLine, String dataLine) {
		return valueOf(columnOrderOf(field, headerLine), dataLine);
	}

	private String valueOf(int targetColumn, String line) {
		return tsvToList(line).get(targetColumn-1);
	}

	private List<String> tsvToList(String tsvLine) {
		String[] parts = tsvLine.split(Pattern.quote("\t"));
		return Utils.list(parts);
	}

	private int columnOrderOf(String field, String headerLine) {
		int i = 1;
		for (String s : headerLine.split(Pattern.quote("\t"))) {
			if (s.equals(field)) return i;
			i++;
		}
		throw new IllegalStateException();
	}

}
