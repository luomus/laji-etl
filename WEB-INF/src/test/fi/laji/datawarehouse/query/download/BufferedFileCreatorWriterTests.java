package fi.laji.datawarehouse.query.download;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.query.download.util.BufferedFileCreatorImple;

public class BufferedFileCreatorWriterTests {

	File out = new File(TestConfig.getConfig().baseFolder() + TestConfig.getConfig().get("TempFolder"));

	@After
	public void cleanup() {
		for (File f : out.listFiles()) {
			f.delete();
		}
	}

	@Test
	public void writeAbortReWrite() throws IOException {
		BufferedFileCreatorImple creator = new BufferedFileCreatorImple(out, this.getClass().getSimpleName(), "txt");
		fi.laji.datawarehouse.query.download.util.File f1 = creator.create("file1");

		assertEquals(false, f1.getPhysicalFile().exists());
		assertEquals("[]", creator.getWrittenFiles().toString());

		f1.write("test");
		assertEquals("[file1_BufferedFileCreatorWriterTests.txt]", creator.getWrittenFiles().toString());

		assertEquals(false, f1.getPhysicalFile().exists());

		for (int i = 0; i<100; i++) {
			f1.write("test");
		}

		assertEquals(true, f1.getPhysicalFile().exists());
		f1.close();

		assertEquals(true, f1.getPhysicalFile().exists());

		f1 = creator.create("file1");
		assertEquals(false, f1.getPhysicalFile().exists());
		assertEquals("[file1_BufferedFileCreatorWriterTests.txt]", creator.getWrittenFiles().toString());

		fi.laji.datawarehouse.query.download.util.File f2 = creator.create("file2");
		fi.laji.datawarehouse.query.download.util.File f3 = creator.create("file3");
		f1.write("test");
		f2.write("test");

		f1.close();
		f2.close();
		f3.close();

		assertEquals(true, f1.getPhysicalFile().exists());
		assertEquals(true, f2.getPhysicalFile().exists());
		assertEquals(false, f3.getPhysicalFile().exists());
		assertEquals("[file1_BufferedFileCreatorWriterTests.txt, file2_BufferedFileCreatorWriterTests.txt]", creator.getWrittenFiles().toString());
	}

}
