package fi.laji.datawarehouse.query.download;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.query.download.util.File;
import fi.laji.datawarehouse.query.download.util.FileCreator;

public class FakeFileCreator implements FileCreator {

	public static class FakeFile implements File {
		String name;
		public FakeFile(String name) {
			this.name = name;
		}
		List<String> lines = new ArrayList<>();
		boolean closed = false;
		@Override
		public void close() throws IOException {
			closed = true;
		}
		@Override
		public boolean isEmpty() {
			return lines.isEmpty();
		}
		@Override
		public void write(String line) {
			lines.add(line);
		}
		public List<String> getLines() {
			return lines;
		}
		@Override
		public String getName() {
			return name;
		}
		@Override
		public String toString() {
			return getName();
		}
		@Override
		public java.io.File getPhysicalFile() {
			throw new UnsupportedOperationException("Fake file");
		}
		public boolean isClosed() {
			return closed;
		}
	}

	private final Map<String, FakeFileCreator.FakeFile> files = new HashMap<>();
	@Override
	public File create(String name) {
		files.put(name, new FakeFile(name));
		return files.get(name);
	}
	public File getFile(String name) {
		return files.get(name);
	}

	@Override
	public List<File> getWrittenFiles() {
		List<File> list = new ArrayList<>();
		for (File f : files.values()) {
			if (!f.isEmpty()) list.add(f);
		}
		return list;
	}
}