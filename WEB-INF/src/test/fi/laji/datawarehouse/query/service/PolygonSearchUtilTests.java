package fi.laji.datawarehouse.query.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonSearchUtil;
import fi.laji.datawarehouse.query.service.PolygonSubscriptionAPI.PolygonValidationException;
import fi.luomus.commons.json.JSONObject;

public class PolygonSearchUtilTests {

	@Test
	public void noParams() {
		try {
			PolygonSearchUtil util = new PolygonSearchUtil(null, null, null);
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Invalid polygon: Must give either geoJSON or wkt parameter", e.getMessage());
		}
	}

	@Test
	public void testInvalidEuref() {
		try {
			PolygonSearchUtil util = new PolygonSearchUtil(null, null, "POLYGON ((70 55,70 55.1,70.1 55.1,70.1 55,70 55))");
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Invalid polygon: Invalid Euref coordinates: 55 : 55 : 70 : 70 : EUREF", e.getMessage());
		}
	}

	@Test
	public void testValidWKTButOutsideFinland() {
		try {
			PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), null, "POLYGON ((70 55,70 55.1,70.1 55.1,70.1 55,70 55))");
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Polygon search is implemented only for Finland (based on ETRS-TM35FIN coordinate system)", e.getMessage());
			assertEquals("Polygonihaku toimii ainoastaan Suomessa (perustuu ETRS-TM35FIN koordinaattijärjestelmään)", e.getLocalizedText().forLocale("fi"));
		}
	}

	@Test
	public void testValidWKT() throws Exception {
		PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), null, "POLYGON ((26.602898 63.901204,26.622263 63.897639,26.62574 63.908497,26.602898 63.901204))");
		String wkt = util.getAndValidateEurefWKT();
		assertEquals("POLYGON ((480508 7086066, 481634 7086872, 481457 7085663, 480508 7086066))", wkt);
	}

	@Test
	public void testTooLargePolygon() {
		PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), null, "POLYGON ((25.41204 62.831202,31.102483 62.831202,31.102483 65.409653,25.41204 65.409653,25.41204 62.831202))");
		try {
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Polygon area is too large: Max height or width is 100000 meters", e.getMessage());
		}
	}

	@Test
	public void testInvalidCrs() {
		try {
			PolygonSearchUtil util = new PolygonSearchUtil("foobar", wgs84GeoJson, null);
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Invalid API parameter: Invalid crs: foobar", e.getMessage());
		}
	}

	@Test
	public void testValidGeoJson() throws Exception {
		PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), wgs84GeoJson, null);
		String wkt = util.getAndValidateEurefWKT();
		assertEquals("POLYGON ((480508 7086066, 481634 7086872, 481457 7085663, 480508 7086066))", wkt);
	}

	@Test
	public void tooFarAawayEuref() {
		try {
			PolygonSearchUtil util = new PolygonSearchUtil(null, null, "POLYGON ((1506556 5798297,1610780 5691667,1640173 5873514,1506556 5798297))");
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Invalid polygon: Invalid Euref coordinates: 5691667 : 5873514 : 1506556 : 1640173 : EUREF", e.getMessage());
		}
	}

	@Test
	public void testValidWKTEuref() throws Exception {
		PolygonSearchUtil util = new PolygonSearchUtil(Type.EUREF.name(), null, "POLYGON ((336378 6682652,341727 6677209,341273 6683922,336378 6682652))");
		String wkt = util.getAndValidateEurefWKT();
		assertEquals("POLYGON ((336378 6682652, 341273 6683922, 341727 6677209, 336378 6682652))", wkt);
	}

	@Test
	public void testValidWKTYKJ() throws Exception {
		PolygonSearchUtil util = new PolygonSearchUtil(Type.YKJ.name(), null, "POLYGON ((3336482 6685458,3341833 6680013,3341379 6686729,3336482 6685458))");
		String wkt = util.getAndValidateEurefWKT();
		assertEquals("POLYGON ((336378 6682652, 341273 6683922, 341727 6677209, 336378 6682652))", wkt);
	}

	@Test
	public void twoPolygons() {
		String wkt = "" +
				"GEOMETRYCOLLECTION(" +
				"POLYGON ((336378 6682652,341727 6677209,341273 6683922,336378 6682652))," +
				"POLYGON ((342100 6684339,344206 6684762,344105 6685944,342100 6684339)) )";
		try {
			PolygonSearchUtil util = new PolygonSearchUtil(null, null, wkt);
			util.getAndValidateEurefWKT();
			fail();
		} catch (PolygonValidationException e) {
			assertEquals("Multiple polygons were given, must provide only one, there were 2", e.getMessage());
			assertEquals("Useita polygoneja annettu, tätyy antaa vain yksi, oli 2", e.getLocalizedText().forLocale("fi"));
		}
	}

	@Test
	public void tooComplex() throws DataValidationException, PolygonValidationException {
		String data = "{ \"type\": \"Polygon\", \"coordinates\": [ [ [ 25.448275, 61.352191 ], [ 25.451879, 61.352439 ], [ 25.453798, 61.351883 ], [ 25.455886, 61.35137 ], [ 25.455141, 61.350784 ], [ 25.45365, 61.349652 ], [ 25.455636, 61.349468 ], [ 25.456784, 61.348862 ], [ 25.458988, 61.347731 ], [ 25.45783, 61.34681 ], [ 25.459427, 61.345796 ], [ 25.462245, 61.344424 ], [ 25.461764, 61.343717 ], [ 25.461306, 61.342556 ], [ 25.460695, 61.34098 ], [ 25.459717, 61.339895 ], [ 25.460613, 61.339162 ], [ 25.460502, 61.337923 ], [ 25.458125, 61.337318 ], [ 25.457466, 61.336732 ], [ 25.457086, 61.335737 ], [ 25.456378, 61.334408 ], [ 25.454445, 61.333519 ], [ 25.452748, 61.333087 ], [ 25.452551, 61.331846 ], [ 25.452424, 61.330937 ], [ 25.45077, 61.329638 ], [ 25.45106, 61.328981 ], [ 25.452375, 61.328459 ], [ 25.454074, 61.32885 ], [ 25.455961, 61.328913 ], [ 25.459042, 61.329154 ], [ 25.459449, 61.329613 ], [ 25.460018, 61.33028 ], [ 25.461753, 61.329928 ], [ 25.463802, 61.330199 ], [ 25.463947, 61.330737 ], [ 25.465475, 61.331126 ], [ 25.468874, 61.331907 ], [ 25.468833, 61.332733 ], [ 25.465326, 61.332404 ], [ 25.461861, 61.332984 ], [ 25.459655, 61.334156 ], [ 25.459922, 61.335728 ], [ 25.461421, 61.336695 ], [ 25.465428, 61.337359 ], [ 25.46805, 61.338214 ], [ 25.468954, 61.33905 ], [ 25.471589, 61.339658 ], [ 25.473982, 61.339933 ], [ 25.473255, 61.34075 ], [ 25.472254, 61.341895 ], [ 25.471101, 61.342625 ], [ 25.471651, 61.345438 ], [ 25.470596, 61.345922 ], [ 25.46968, 61.347067 ], [ 25.468163, 61.348206 ], [ 25.466906, 61.349307 ], [ 25.464368, 61.350228 ], [ 25.463446, 61.351497 ], [ 25.46259, 61.35318 ], [ 25.461518, 61.353994 ], [ 25.460346, 61.355095 ], [ 25.460365, 61.356458 ], [ 25.460884, 61.358156 ], [ 25.459965, 61.359343 ], [ 25.458977, 61.360199 ], [ 25.45916, 61.361728 ], [ 25.458435, 61.363065 ], [ 25.456561, 61.36514 ], [ 25.455462, 61.366135 ], [ 25.456191, 61.36732 ], [ 25.455265, 61.36836 ], [ 25.456413, 61.369927 ], [ 25.457399, 61.37124 ], [ 25.457243, 61.372625 ], [ 25.455658, 61.372817 ], [ 25.454278, 61.372381 ], [ 25.452351, 61.372401 ], [ 25.453637, 61.372962 ], [ 25.454999, 61.373734 ], [ 25.456841, 61.37367 ], [ 25.457695, 61.3741 ], [ 25.458069, 61.375407 ], [ 25.459598, 61.376348 ], [ 25.460518, 61.377241 ], [ 25.459793, 61.377737 ], [ 25.458785, 61.378649 ], [ 25.459352, 61.37958 ], [ 25.45931, 61.38042 ], [ 25.458819, 61.381506 ], [ 25.459809, 61.382736 ], [ 25.458856, 61.384321 ], [ 25.457912, 61.385697 ], [ 25.455839, 61.386892 ], [ 25.454395, 61.387757 ], [ 25.452413, 61.388869 ], [ 25.451125, 61.390115 ], [ 25.449433, 61.390683 ], [ 25.44809, 61.391256 ], [ 25.446012, 61.392535 ], [ 25.445852, 61.394003 ], [ 25.446763, 61.395064 ], [ 25.448764, 61.395339 ], [ 25.450179, 61.395061 ], [ 25.452748, 61.394502 ], [ 25.454559, 61.395069 ], [ 25.453909, 61.395817 ], [ 25.451649, 61.395456 ], [ 25.450567, 61.396073 ], [ 25.44897, 61.396475 ], [ 25.44507, 61.395633 ], [ 25.443706, 61.394861 ], [ 25.444192, 61.3939 ], [ 25.444238, 61.392977 ], [ 25.445166, 61.391937 ], [ 25.443594, 61.391835 ], [ 25.442356, 61.392073 ], [ 25.439698, 61.392673 ], [ 25.437076, 61.392558 ], [ 25.434804, 61.392448 ], [ 25.4347, 61.391019 ], [ 25.435561, 61.389558 ], [ 25.435364, 61.388254 ], [ 25.436579, 61.386714 ], [ 25.438307, 61.387196 ], [ 25.438733, 61.385688 ], [ 25.441496, 61.384712 ], [ 25.442774, 61.38544 ], [ 25.443788, 61.386166 ], [ 25.447388, 61.385997 ], [ 25.449855, 61.385689 ], [ 25.452789, 61.384799 ], [ 25.45471, 61.38314 ], [ 25.45541, 61.381384 ], [ 25.455979, 61.380508 ], [ 25.45522, 61.379911 ], [ 25.453993, 61.379939 ], [ 25.454993, 61.379195 ], [ 25.453584, 61.379347 ], [ 25.452576, 61.378495 ], [ 25.450519, 61.377589 ], [ 25.450758, 61.37629 ], [ 25.452415, 61.376435 ], [ 25.453698, 61.377037 ], [ 25.452786, 61.377783 ], [ 25.454417, 61.378432 ], [ 25.455847, 61.37786 ], [ 25.45545, 61.377015 ], [ 25.454444, 61.376122 ], [ 25.453723, 61.374769 ], [ 25.451941, 61.373615 ], [ 25.450495, 61.372758 ], [ 25.450208, 61.371495 ], [ 25.448764, 61.370596 ], [ 25.448824, 61.369378 ], [ 25.449847, 61.368172 ], [ 25.450526, 61.366835 ], [ 25.449543, 61.36548 ], [ 25.450092, 61.364982 ], [ 25.450048, 61.362335 ], [ 25.451044, 61.363439 ], [ 25.451427, 61.364535 ], [ 25.453035, 61.363881 ], [ 25.454132, 61.362928 ], [ 25.452402, 61.36253 ], [ 25.449986, 61.36183 ], [ 25.449869, 61.360653 ], [ 25.450466, 61.359189 ], [ 25.451655, 61.358153 ], [ 25.452161, 61.356772 ], [ 25.449762, 61.355737 ], [ 25.448259, 61.354291 ], [ 25.447967, 61.353112 ], [ 25.448275, 61.352191 ] ] ] } }";
		String wkt = Polygon.fromGeoJSON(new JSONObject(data)).getWKT();
		PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), null, wkt);
		wkt = util.getAndValidateEurefWKT();
		assertEquals("" +
				"POLYGON ((417167 6800355, 417098 6800415, 417084 6800488, 416416 6807036, 416388 6807345, 416397 6807504, 416954 6807845, 417164 6807934, 417426 6807855, 417459 6807771, 417707 6806390, 418353 6801606, 418059 6800718, 417783 6800534, 417525 6800424, 417167 6800355))",
				wkt);
	}

	@Test
	public void tooComplex2() throws PolygonValidationException, DataValidationException {
		String data = "POLYGON((25.453619 61.394668,25.454201 61.395147,25.45333 61.395935,25.452068 61.395449,25.450298 61.396045,25.449443 61.396507,25.448222 61.396711,25.446731 61.396259,25.444713 61.395764,25.443528 61.395243,25.44371 61.394628,25.444493 61.394093,25.44429 61.393619,25.444488 61.392678,25.445062 61.391777,25.44386 61.391618,25.442924 61.392188,25.441849 61.392502,25.440486 61.392523,25.439109 61.392834,25.438047 61.392894,25.436534 61.392877,25.435269 61.392463,25.434988 61.392025,25.434801 61.391224,25.434994 61.390392,25.435248 61.389851,25.435367 61.388981,25.435614 61.388585,25.434509 61.387992,25.435136 61.387564,25.435768 61.387027,25.43699 61.386787,25.438352 61.386766,25.43998 61.385987,25.439627 61.385475,25.440723 61.384726,25.441924 61.384921,25.442968 61.385223,25.442787 61.385801,25.445185 61.386264,25.447086 61.386068,25.448306 61.385864,25.449215 61.385839,25.450291 61.385488,25.452058 61.384928,25.453588 61.384582,25.45433 61.384881,25.454985 61.385396,25.456013 61.386025,25.457053 61.386399,25.456501 61.386865,25.455272 61.38725,25.454334 61.387856,25.453629 61.388356,25.452547 61.388815,25.452135 61.389048,25.452009 61.389064,25.451785 61.389249,25.451677 61.38933,25.45153 61.389452,25.451423 61.389521,25.451315 61.38961,25.451201 61.389674,25.451111 61.38977,25.450935 61.38987,25.450835 61.390019,25.450713 61.390149,25.450641 61.39024,25.450559 61.390297,25.450457 61.390375,25.450264 61.390444,25.450102 61.390498,25.449908 61.390575,25.449819 61.390644,25.449525 61.390773,25.449371 61.390806,25.449103 61.390897,25.448979 61.390937,25.448737 61.390993,25.448595 61.391035,25.448488 61.391092,25.448277 61.391152,25.448029 61.391216,25.447805 61.391278,25.447625 61.391338,25.447518 61.391395,25.447263 61.391483,25.447166 61.391585,25.447057 61.391681,25.446585 61.392044,25.446007 61.392525,25.445517 61.393062,25.444887 61.393654,25.445276 61.39419,25.446134 61.39461,25.446789 61.394928,25.447173 61.39511,25.447513 61.395247,25.448382 61.395434,25.449234 61.39551,25.449382 61.395324,25.450941 61.39512,25.451435 61.394937,25.452575 61.394783,25.452914 61.394954,25.453209 61.395057,25.453675 61.394995,25.453619 61.394668))";
		Geo g = Geo.fromWKT(data, Type.WGS84);

		assertEquals(109, g.getFeatures().get(0).size());
		assertTrue(g.getFeatures().get(0).size() > PolygonSubscriptionAPI.MAX_POLYGON_COMPLEXITY);

		PolygonSearchUtil util = new PolygonSearchUtil(Type.WGS84.name(), null, data);
		String wkt = util.getAndValidateEurefWKT();
		assertEquals("" +
				"POLYGON ((417408 6807727, 417412 6807764, 417387 6807771, 417352 6807741, 417183 6807806, 417176 6807826, 417130 6807819, 417083 6807799, 416961 6807685, 416939 6807625, 417060 6807381, 417228 6807253, 417307 6807105, 417392 6807024, 417477 6806899, 417541 6806854, 417569 6806802, 417513 6806762, 417420 6806636, 417380 6806604, 417149 6806749, 416935 6806802, 416806 6806753, 416814 6806689, 416693 6806636, 416636 6806721, 416656 6806778, 416571 6806867, 416499 6806871, 416434 6806899, 416369 6807008, 416430 6807073, 416418 6807117, 416394 6807368, 416406 6807457, 416422 6807505, 416491 6807549, 416628 6807541, 416701 6807505, 416774 6807501, 416830 6807465, 416879 6807400, 416943 6807416, 416915 6807517, 416907 6807622, 416919 6807675, 416879 6807735, 416870 6807804, 416935 6807861, 417125 6807962, 417190 6807937, 417234 6807885, 417327 6807816, 417396 6807869, 417440 6807780, 417408 6807727))",
				wkt);
		g = Geo.fromWKT(wkt, Type.EUREF);
		assertEquals(56, g.getFeatures().get(0).size());
	}

	private String wgs84GeoJson = "" +
			"{" +
			"  \"type\": \"FeatureCollection\"," +
			"  \"features\": [" +
			"    {" +
			"      \"type\": \"Feature\"," +
			"      \"properties\": {}," +
			"      \"geometry\": {" +
			"        \"type\": \"Polygon\"," +
			"        \"coordinates\": [" +
			"          [" +
			"            [" +
			"              26.602898," +
			"              63.901204" +
			"            ]," +
			"            [" +
			"              26.622263," +
			"              63.897639" +
			"            ]," +
			"            [" +
			"              26.62574," +
			"              63.908497" +
			"            ]," +
			"            [" +
			"              26.602898," +
			"              63.901204" +
			"            ]" +
			"          ]" +
			"        ]" +
			"      }" +
			"    }" +
			"  ]" +
			"}";
}
