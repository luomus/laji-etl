package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.TestDAO;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.query.model.EnumToProperty;
import fi.laji.datawarehouse.query.model.EnumerationLabels;
import fi.laji.datawarehouse.query.service.FilterDescriptionsAPI.FilterDescriptions;
import fi.luomus.commons.json.JSONObject;

public class EnumAndFilterLabelsTests {

	private static TestDAO dao;

	@BeforeClass
	public static void init() {
		dao = new TestDAO(EnumAndFilterLabelsTests.class);
	}

	@AfterClass
	public static void close() {
		if (dao != null) dao.close();
	}

	@Test
	public void enumToProperty() {
		EnumToProperty.getInstance();
	}

	@Test
	public void enumLabels() {
		EnumerationLabels enumerationLabels = new EnumerationLabels(dao);
		JSONObject labels = enumerationLabels.getAllLabels();

		String expected = TestFieldsBuilding.getTestData("expected-enum-labels.json");
		assertEquals(expected, labels.beautify());
	}

	@Test
	public void enumLabels_single() {
		EnumerationLabels enumerationLabels = new EnumerationLabels(dao);
		JSONObject labels = enumerationLabels.getLabels(Sex.FEMALE.name());
		assertEquals("FEMALE", labels.getString("enumeration"));
		assertEquals("MY.sexF", labels.getString("property"));
		assertEquals("F - Female", labels.getObject("label").getString("en"));
	}

	@Test
	public void filters() {
		FilterDescriptions filterDescriptions = new FilterDescriptions(dao);
		JSONObject descriptions = filterDescriptions.getAllDescriptions();

		JSONObject recordBasis = descriptions.getObject("recordBasis");
		assertEquals("Havaintotyyppi", recordBasis.getObject("label").getString("fi"));
		assertEquals("ENUMERATION", recordBasis.getString("type"));
		assertEquals("PRESERVED_SPECIMEN", recordBasis.getArray("enumerations").iterateAsObject().get(0).getString("name"));
		assertEquals("Näyte", recordBasis.getArray("enumerations").iterateAsObject().get(0).getObject("label").getString("fi"));

		String expected = "" +
				"{\"label\":{\"fi\":\"Lajin uhanalaisuusluokitus\",\"en\":\"Red list status of species\",\"sv\":\"Artens rödlistekategori\"},\"type\":\"RESOURCE\",\"resource\":\"metadata/ranges/MX.iucnStatuses\"}";
		assertEquals(expected, descriptions.getObject("redListStatusId").toString());
	}

}
