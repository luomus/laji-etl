package fi.laji.datawarehouse.query.model;

import java.util.List;

import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.CountQuery;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.CountResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.luomus.commons.containers.rdf.Qname;

public class VerticaQueryDAOStub implements VerticaQueryDAO {

	@Override
	public Document get(Qname documentId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Document get(Qname documentId, boolean approvedDataRequest) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ListResponse getList(ListQuery query) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public AggregateResponse getAggregate(AggregatedQuery query) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public CountResponse getCount(CountQuery query) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public VerticaCustomQueriesDAO getCustomQueries() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void clearCaches() {
		// Auto-generated method stub

	}

	@Override
	public void setJoinedRowLinkings(JoinedRow row) {
		// Auto-generated method stub

	}

	@Override
	public List<JoinedRow> getRawList(ListQuery query) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<AggregateRow> getRawAggregate(AggregatedQuery query) {
		// Auto-generated method stub
		return null;
	}

}
