package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.definedfields.Fields;
import fi.laji.datawarehouse.query.model.definedfields.Fields.DefinedFields;
import fi.laji.datawarehouse.query.model.definedfields.UnitSelectableFields;
import fi.laji.datawarehouse.query.model.queries.OrderBy;
import fi.laji.datawarehouse.query.model.queries.OrderBy.Order;
import fi.laji.datawarehouse.query.model.queries.OrderBy.OrderCriteria;
import fi.luomus.commons.utils.FileUtils;

public class TestFieldsBuilding {

	@Test
	public void orderCriteriaBuilding() throws NoSuchFieldException {
		OrderCriteria c1 = OrderBy.toOrderCriteria("field");
		OrderCriteria c2 = OrderBy.toOrderCriteria("field asc");
		OrderCriteria c3 = OrderBy.toOrderCriteria("field blaa");
		OrderCriteria c4 = OrderBy.toOrderCriteria("field descENDING");
		OrderCriteria c5 = OrderBy.toOrderCriteria("field DEsc");
		OrderCriteria c6 = OrderBy.toOrderCriteria("field DEsc ASC");
		OrderCriteria c7 = OrderBy.toOrderCriteria("FieLD DEsc ");
		assertEquals(Order.ASC, c1.getOrder());
		assertEquals(Order.ASC, c2.getOrder());
		assertEquals(Order.ASC, c3.getOrder());
		assertEquals(Order.DESC, c4.getOrder());
		assertEquals(Order.DESC, c5.getOrder());
		assertEquals(Order.DESC, c6.getOrder());
		assertEquals(Order.DESC, c7.getOrder());
		assertEquals("field", c1.getField());
		assertEquals("field", c2.getField());
		assertEquals("field", c3.getField());
		assertEquals("field", c4.getField());
		assertEquals("field", c5.getField());
		assertEquals("field", c6.getField());
		assertEquals("FieLD", c7.getField());

		new OrderBy(Const.RANDOM);
		new OrderBy(Const.RANDOM_WITH_SEED);
		new OrderBy(Const.RANDOM+":123");
	}

	@Test
	public void selectableFieldValidation() {
		try {
			Fields.selectable(Base.UNIT).validateField("document.blaa");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document.blaa", e.getMessage());
		}

		try {
			Fields.selectable(Base.UNIT).validateField("document.");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document.", e.getMessage());
		}

		try {
			Fields.selectable(Base.UNIT).validateField("document");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document", e.getMessage());
		}
	}

	@Test
	public void annotationSelectableFieldValidation() throws NoSuchFieldException {
		try {
			Fields.selectable(Base.UNIT).validateField("unit.annotations.id");
			Fields.selectable(Base.UNIT).validateField("annotation.id");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: annotation.id", e.getMessage());
		}

		Fields.selectable(Base.ANNOTATION).validateField("annotation.id");
		Fields.selectable(Base.ANNOTATION).validateField("unit.annotations.id");
	}

	@Test
	public void sortableFieldValidation() throws NoSuchFieldException {
		try {
			Fields.sortable(Base.UNIT).validateField("document");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document", e.getMessage());
		}

		Fields.sortable(Base.UNIT).validateField("document.documentId");
	}

	@Test
	public void aggregateFieldValidation() throws NoSuchFieldException {
		try {
			Fields.aggregatable(Base.UNIT).validateField("document");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document", e.getMessage());
		}

		Fields.aggregatable(Base.UNIT).validateField("document.documentId");

	}

	@Test
	public void enumerations() {
		Collection<Enum<?>> enums = Fields.getEnumerations();
		assertTrue(enums.size() > 70);
		assertTrue(enums.contains(RecordBasis.HUMAN_OBSERVATION_HEARD));
	}

	@Test
	public void statisticFields() throws NoSuchFieldException {
		DefinedFields fields = Fields.statisticsAggregatable(Base.GATHERING);
		System.out.println(fields.getAllFields());
		fields.validateField("gathering.conversions.year");
		fields.validateField("gathering.conversions.ykj10km.lat");
		fields.validateField("gathering.conversions.ykj10km.lon");
	}

	@Test
	public void unitStatisticFields() throws NoSuchFieldException {
		DefinedFields fields = Fields.statisticsAggregatable(Base.UNIT);
		System.out.println(fields.getAllFields());
		fields.validateField("gathering.conversions.year");
		fields.validateField("gathering.conversions.ykj10km.lat");
		fields.validateField("gathering.conversions.ykj10km.lon");
	}

	@Test
	public void all() {
		for (Base base : Base.values()) {
			Fields.selectable(base);
			assertEquals("selectable " + base,
					expected("selectable", base),
					actual(Fields.selectable(base)));
		}

		for (Base base : Base.values()) {
			if (base.includes(Base.UNIT) && base != Base.UNIT) continue;
			try {
				Fields.aggregatable(base);
				assertEquals("aggregatable " + base,
						expected("aggregatable", base),
						actual(Fields.aggregatable(base)));
			} catch (NullPointerException e) {
				fail("Missing expected file: aggregatable_" + base.name().toLowerCase() + " .txt");
			} catch (UnsupportedOperationException e) {
				assertTrue(
						e.getMessage().equals("Not yet implemented for " + Base.DOCUMENT)
						);
			}
		}

		for (Base base : Base.values()) {
			try {
				Fields.statisticsAggregatable(base);
				assertEquals("statistics aggregatable " + base,
						expected("statistics_aggregatable", base),
						actual(Fields.statisticsAggregatable(base)));
			} catch (UnsupportedOperationException e) {
				assertTrue(
						e.getMessage().equals("Not yet implemented for " + Base.UNIT_MEDIA) ||
						e.getMessage().equals("Not yet implemented for " + Base.DOCUMENT) ||
						e.getMessage().equals("Not yet implemented for " + Base.ANNOTATION) ||
						e.getMessage().equals("Not yet implemented for " + Base.SAMPLE)
						);
			}
		}

		for (Base base : Base.values()) {
			Fields.sortable(base);
			assertEquals("sortable " + base,
					expected("sortable", base),
					actual(Fields.sortable(base)));
		}

		assertEquals("non-array " + Base.UNIT,
				expected("nonarray", Base.UNIT),
				actual(UnitSelectableFields.getNonArrayFields()));
	}

	private String actual(Set<String> fields) {
		return fields.stream().collect(Collectors.joining("\n"));
	}

	private String actual(DefinedFields fields) {
		return actual(fields.getAllFields());
	}

	private Object expected(String type, Base base) {
		return getTestData(type + "_" + base.name().toLowerCase() + ".txt");
	}

	public static String getTestData(String filename) {
		URL url = TestFieldsBuilding.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
