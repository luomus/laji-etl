package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringConversions;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class RdfXmlResponseTests {

	private Map<String, CollectionMetadata> collectionMetadatas;
	private Map<Qname, Area> areas;

	@Before
	public void init() {
		collectionMetadatas = new HashMap<>();
		CollectionMetadata m1 = new CollectionMetadata(new Qname("HR.1"), new LocalizedText().set("en", "Coll name from metadata"), null);
		m1.setIntellectualRightsDescription(new LocalizedText().set("en", "Coll license desc"));
		m1.setAbbreviation("COLABBR");
		collectionMetadatas.put(m1.getQname().toURI(), m1);
		areas = new HashMap<>();
		areas.put(new Qname("ML.206"), new Area(new Qname("ML.206"), new LocalizedText().set("en", "Finland").set("fi", "not used"), new Qname("ML.country")).setAbbreviation("FI"));
	}

	@Test
	public void test_minimal() throws Exception {
		List<JoinedRow> rows = generateMinimalTestData();
		String xml = CetafRdfXmlUtil.generate(rows, collectionMetadatas, areas);
		String expected = getExpected("expected-rdf-xml-minimal.xml");
		assertEquals(expected.trim(), xml.trim());
	}

	private List<JoinedRow> generateMinimalTestData() throws CriticalParseFailure {
		Document document = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("docid"), new Qname("HR.1"));
		Gathering gathering = new Gathering(new Qname("gid"));
		Unit specimen = new Unit(new Qname("uid1"));
		Unit observation = new Unit(new Qname("uid2"));
		JoinedRow row1 = new JoinedRow(specimen, gathering, document);
		JoinedRow row2 = new JoinedRow(observation, gathering, document);
		return Utils.list(row1, row2);
	}

	@Test
	public void test() throws Exception {
		List<JoinedRow> rows = generateTestData();
		String xml = CetafRdfXmlUtil.generate(rows, collectionMetadatas, areas);
		String expected = getExpected("expected-rdf-xml.xml");
		assertEquals(expected.trim(), xml.trim());
	}

	private List<JoinedRow> generateTestData() throws Exception {
		List<JoinedRow> rows = generateMinimalTestData();
		JoinedRow row1 = rows.get(0);
		JoinedRow row2 = rows.get(1);

		Document document = row1.getDocument();
		Gathering gathering = row1.getGathering();
		Unit specimen = row1.getUnit();
		Unit observation = row2.getUnit();

		document.setCreatedDate(DateUtils.convertToDate("1.5.2019"));
		document.setModifiedDate(DateUtils.convertToDate("25.12.2019"));
		document.addFact(CetafUnitDescription.ADDITIONAL_IDS_FACT, "additionalid1");
		document.addFact(CetafUnitDescription.ADDITIONAL_IDS_FACT, "additionalid2");
		document.setNotes("doc notes");
		document.addMedia(new MediaObject(MediaType.IMAGE, "http://image.com/docimage1.jpg"));
		document.addMedia(new MediaObject(MediaType.IMAGE, "http://image.com/docimage2.jpg"));
		document.addFact(CetafUnitDescription.LEG_ID_FACT, "12345");

		gathering.addMedia(new MediaObject(MediaType.IMAGE, "http://image.com/gimage.jpg"));
		gathering.addTeamMember("leg 1");
		gathering.addTeamMember("Leg Meikäläinen 2");
		gathering.setConversions(new GatheringConversions());
		gathering.getConversions().setWgs84CenterPoint(new SingleCoordinates(60.123456789012345678, 31.111212112121, Type.WGS84));
		gathering.setInterpretations(new GatheringInterpretations());
		gathering.getInterpretations().setCountry(new Qname("ML.206"));
		gathering.getInterpretations().setSourceOfCoordinates(GeoSource.COORDINATE_CENTERPOINT);
		gathering.getInterpretations().setMunicipalityDisplayname("Akaa");
		gathering.getInterpretations().setBiogeographicalProvinceDisplayname("Luusuomi (LU)");
		gathering.getInterpretations().setCoordinates(new Coordinates(6.666, 6.666, Type.WGS84).setAccuracyInMeters(100));
		gathering.setEventDate(new DateRange(DateUtils.convertToDate("1.2.2005"), DateUtils.convertToDate("28.2.2005")));
		gathering.setHigherGeography("Higher geog");
		gathering.setLocality("Localstring");
		gathering.setProvince("Provin");
		gathering.setBiogeographicalProvince("not used");
		gathering.setMunicipality("not used");

		specimen.setRecordBasis(RecordBasis.PRESERVED_SPECIMEN);
		observation.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		specimen.setTaxonVerbatim("Verbatim taxname");
		specimen.setAuthor("Verb author");
		observation.setTaxonVerbatim("not used");
		observation.setAuthor("not used");

		specimen.setAbundanceString("1");
		specimen.setInterpretations(new UnitInterpretations());
		specimen.getInterpretations().setIndividualCount(1);

		observation.setAbundanceString("lots");
		observation.setInterpretations(new UnitInterpretations());
		observation.getInterpretations().setIndividualCount(1);

		specimen.setTypeSpecimen(true);
		specimen.addFact(CetafUnitDescription.TYPE_STATUS_FACT, new Qname("MY.typeStatusSomeType").toURI());
		specimen.addMedia(new MediaObject(MediaType.IMAGE, "http://image.com/unitimage.jpg"));
		specimen.setNotes("U notes");

		observation.setLinkings(new UnitDWLinkings());
		observation.getLinkings().setTaxon(createTaxon());

		return rows;
	}

	private Taxon createTaxon() {
		Taxon taxon = new Taxon(new Qname("MX.1"), null) {
			@Override
			public String getScientificName() {
				return "Genusname speciespart epi var morfor";
			}
			@Override
			public String getScientificNameAuthorship() {
				return "Tax name auth";
			}
			@Override
			public String getScientificNameOfRank(Qname rank) {
				if (rank.toString().equals("MX.species")) return "Genusname speciespart";
				return "Sciname rank " + rank;
			}
		};
		return taxon;
	}

	private static String getExpected(String filename) {
		URL url = RdfXmlResponseTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
