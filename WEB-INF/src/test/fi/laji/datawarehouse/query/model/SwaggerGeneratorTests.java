package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.Set;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.utils.SwaggerAPIDescriptionGenerator;
import fi.laji.datawarehouse.etl.utils.SwaggerV3APIDescriptionGenerator;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class SwaggerGeneratorTests {

	@Test
	public void testSwagger() throws Exception {
		JSONObject json = SwaggerAPIDescriptionGenerator.generateSwaggerDescription(TestConfig.getConfig());

		validateTypes(json);

		String expected = getExpected("expected-swagger.json");
		assertEquals(expected, json.beautify());
	}

	@Test
	public void testSwaggerV3() throws Exception {
		JSONObject json = SwaggerV3APIDescriptionGenerator.generateSwaggerDescription(TestConfig.getConfig());

		validateTypes(json);

		String expected = getExpected("expected-swagger-v3.json");
		assertEquals(expected, json.beautify());
	}

	private void validateTypes(JSONObject json) {
		if (json.hasKey("type")) {
			if (json.isObject("type")) {
				validateTypes(json.getObject("type"));
			} else {
				validateType(json.getString("type"));
			}
		}
		for (String key : json.getKeys()) {
			if (json.isArray(key)) {
				validateTypes(json.getArray(key));
			} else if (json.isObject(key)) {
				validateTypes(json.getObject(key));
			}
		}
	}

	private void validateTypes(JSONArray array) {
		for (Object o : array.iterateAsRaw()) {
			if (o instanceof String) continue;
			if (o instanceof JSONObject) {
				validateTypes((JSONObject)o);
				continue;
			}
			throw new IllegalStateException("Not yet implemented " + o.getClass().getCanonicalName());
		}
	}

	private static final Set<String> ACCEPTED_TYPES = Utils.set("string", "boolean", "integer", "number", "array", "object");

	private void validateType(String value) {
		if (!ACCEPTED_TYPES.contains(value)) {
			fail("Unknown type " + value);
		}
	}

	private static String getExpected(String filename) {
		URL url = SwaggerGeneratorTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
