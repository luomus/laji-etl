package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregateByCoordinateFields;

public class AggregateByValidationTests {

	@Test
	public void allowed_statisticsFields() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy(Base.GATHERING, true);
		aggregateBy.addField("gathering.conversions.year");
		try {
			aggregateBy.addField("foo");
			fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base GATHERING: foo", e.getMessage());
		}
		try {
			aggregateBy.addField("gathering.team");
			fail();
		} catch (NoSuchFieldException e) {
			assertEquals("Field not available for GATHERING statistics endpoint: gathering.team", e.getMessage());
		}
	}

	@Test
	public void test_geojson_empty_coordinates() {
		AggregateBy aggregateBy = new AggregateBy();
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		assertEquals(0, f.count());
		try {
			f.validate();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals(
					"To form GeoJSON response you must define crs and featureType parameters OR define two lat/lon or four latMin/latMax/lonMin/lonMax fields in aggregateBy",
					e.getMessage());
		}
	}

	@Test
	public void test_geojson_coordinate_pair() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy();
		aggregateBy.addField("gathering.conversions.wgs84CenterPoint.lat");
		aggregateBy.addField("gathering.conversions.year");
		aggregateBy.addField("gathering.conversions.wgs84CenterPoint.lon");
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		assertEquals(2, f.count());
		f.validate();
		assertTrue(f.contains("gathering.conversions.wgs84CenterPoint.lat"));
		assertTrue(f.contains("gathering.conversions.wgs84CenterPoint.lon"));
		assertFalse(f.contains("gathering.conversions.foobar"));
		assertEquals("gathering.conversions.wgs84CenterPoint.lat", f.getLat().getFieldName());
		assertEquals(0, f.getLat().getIndex());
		assertEquals("gathering.conversions.wgs84CenterPoint.lon", f.getLon().getFieldName());
		assertEquals(2, f.getLon().getIndex());
	}

	@Test
	public void test_geojson_missing_coordinate_pair() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy();
		aggregateBy.addField("gathering.conversions.wgs84CenterPoint.lat");
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		assertEquals(1, f.count());
		try {
			f.validate();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Must contain two or four coordinate fields (lat,lon) or (latMin,latMax,lonMin,lonMax), contains [lat]", e.getMessage());
		}
	}

	@Test
	public void test_geojson_minmax_pair() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy();
		aggregateBy.addField("gathering.conversions.euref.latMin");
		aggregateBy.addField("gathering.conversions.euref.latMax");
		aggregateBy.addField("gathering.conversions.year");
		aggregateBy.addField("gathering.conversions.euref.lonMin");
		aggregateBy.addField("gathering.conversions.euref.lonMax");
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		assertEquals(4, f.count());
		f.validate();
		assertEquals("gathering.conversions.euref.latMax", f.getLatMax().getFieldName());
		assertEquals(1, f.getLatMax().getIndex());
		assertEquals("gathering.conversions.euref.lonMax", f.getLonMax().getFieldName());
		assertEquals(4, f.getLonMax().getIndex());
	}

	@Test
	public void test_geojson_mixed_prefix() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy();
		aggregateBy.addField("gathering.conversions.euref.latMin");
		aggregateBy.addField("gathering.conversions.ykj.latMax");
		aggregateBy.addField("gathering.conversions.euref.lonMin");
		aggregateBy.addField("gathering.conversions.euref.lonMax");
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		try {
			f.validate();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Can not mix different coordinate types [EUREF, YKJ]", e.getMessage());
		}
	}

	@Test
	public void test_geojson_missing_minmax_pair() throws NoSuchFieldException {
		AggregateBy aggregateBy = new AggregateBy();
		aggregateBy.addField("gathering.conversions.euref.latMin");
		aggregateBy.addField("gathering.conversions.euref.lonMin");
		aggregateBy.addField("gathering.conversions.euref.lonMax");
		AggregateByCoordinateFields f = aggregateBy.getCoordinateFields();
		try {
			f.validate();
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Must contain two or four coordinate fields (lat,lon) or (latMin,latMax,lonMin,lonMax), contains [latMin, lonMin, lonMax]", e.getMessage());
		}
	}

}
