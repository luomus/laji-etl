package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.InvasiveControl;
import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.Filters.QualityIssues;
import fi.laji.datawarehouse.query.model.Filters.UnsupportedFilterException;
import fi.laji.datawarehouse.query.model.Filters.Wild;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class FilterTests {

	@Test
	public void test_unit_defaults() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false);
		String expected = "[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, individualCountMin:1, wild:[WILD,WILD_UNKNOWN], qualityIssues:NO_ISSUES]";
		assertEquals(0, filters.getSetFiltersExcludingDefaults().size());
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_unit_defaults_2() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).setIndividualCountMin(0);

		String expected = "[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, individualCountMin:0, wild:[WILD,WILD_UNKNOWN], qualityIssues:NO_ISSUES]";
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());

		expected = "[individualCountMin:0]";
		assertEquals(expected, filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_unit_defaults_with_id_filter() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).setIndividualCountMin(0).setKeyword("foo");

		String expected = "[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, keyword:[foo], individualCountMin:0]";
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());

		expected = "[keyword:[foo], individualCountMin:0]";
		assertEquals(expected, filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_unit_defaults_with_id_filter_2() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).setKeyword("foo");

		String expected = "[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, keyword:[foo]]";
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());

		expected = "[keyword:[foo]]";
		assertEquals(expected, filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_unit_defaults_with_id_filter_3() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false)
				.setKeyword("foo")
				.setIndividualCountMin(5)
				.setQualityIssues(QualityIssues.ONLY_ISSUES)
				.setWild(Wild.NON_WILD)
				.setWild(Wild.WILD_UNKNOWN)
				.setUseIdentificationAnnotations(false);

		String expected = "[useIdentificationAnnotations:false, includeSubTaxa:true, includeNonValidTaxa:true, keyword:[foo], individualCountMin:5, wild:[NON_WILD,WILD_UNKNOWN], qualityIssues:ONLY_ISSUES]";
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());

		expected = "[useIdentificationAnnotations:false, keyword:[foo], individualCountMin:5, wild:[NON_WILD,WILD_UNKNOWN], qualityIssues:ONLY_ISSUES]";
		assertEquals(expected, filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_document_defaults() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.DOCUMENT, false);
		String expected = "[qualityIssues:NO_ISSUES]";
		assertEquals(0, filters.getSetFiltersExcludingDefaults().size());
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_gathering_defaults() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.GATHERING, false);
		String expected = "[qualityIssues:NO_ISSUES]";
		assertEquals(0, filters.getSetFiltersExcludingDefaults().size());
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_annotation_defaults() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.ANNOTATION, false);
		String expected = "[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, includeSystemAnnotations:false]";
		assertEquals(0, filters.getSetFiltersExcludingDefaults().size());
		assertEquals(expected, filters.getSetFiltersIncludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_parse_from_null() throws Exception {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(((Map<String, String[]>)null));
		assertFalse(filters.hasSetFiltersExludingDefaults());
		filters.validate();
	}

	@Test
	public void test_parse_empty() throws Exception {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(new HashMap<String, String[]>());
		assertFalse(filters.hasSetFiltersExludingDefaults());
		filters.validate();
	}

	@Test
	public void test_parse_single() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("target", new String[] {"Susi"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals(1, filters.getTarget().size());
		assertEquals("[Susi]", filters.getTarget().toString());
		assertEquals("[target:[Susi]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_parse_two() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("target", new String[] {"Susi", "Karhu"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals(2, filters.getTarget().size());
		assertEquals("[Susi, Karhu]", filters.getTarget().toString());
		assertEquals("[target:[Susi,Karhu]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_parse_two_with_different_separator() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("teamMember", new String[] {"Meikäläinen, Matti", "Maija *"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals(2, filters.getTeamMember().size());
		assertEquals("[Meikäläinen, Matti, Maija *]", filters.getTeamMember().toString());
		assertEquals("[teamMember:[Meikäläinen, Matti;Maija *]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_parse_two_with_different_separator_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("teamMember", new String[] {"Meikäläinen, Matti; Maija *"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals(2, filters.getTeamMember().size());
		assertEquals("[Meikäläinen, Matti, Maija *]", filters.getTeamMember().toString());
		assertEquals("[teamMember:[Meikäläinen, Matti;Maija *]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_parse_boolean() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("typeSpecimen", new String[] {"true", "false"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertFalse(filters.isTypeSpecimen());
		filters.validate();
	}

	@Test
	public void test_parse_boolean_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("typeSpecimen", new String[] {"true", "false", "true"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertTrue(filters.isTypeSpecimen());
		filters.validate();
	}

	@Test
	public void test_parse_boolean_3() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("typeSpecimen", new String[] {"TRUE", "FALSE", "TRUE"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertTrue(filters.isTypeSpecimen());
		filters.validate();
	}

	@Test
	public void test_parse_boolean_4() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("typeSpecimen", new String[] {"A"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid boolean: a", e.getMessage());
		}
	}

	@Test
	public void test_parse_range() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("dayOfYear", new String[] {"1/5", "300/350"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("[1/5, 300/350]", filters.getDayOfYear().toString());
		filters.validate();
	}

	@Test
	public void test_parse_date() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("loadedSameOrAfter", new String[] {"2000-05-23"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("23.05.2000", DateUtils.format(filters.getLoadedSameOrAfter(), "dd.MM.yyyy"));
		filters.validate();
	}

	@Test
	public void test_parse_timestamp() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("loadedSameOrAfter", new String[] {"1091157732"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("30.07.2004", DateUtils.format(filters.getLoadedSameOrAfter(), "dd.MM.yyyy"));
		filters.validate();
	}

	@Test
	public void test_parse_date_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("loadedSameOrAfter", new String[] {"2000"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("01.01.1970", DateUtils.format(filters.getLoadedSameOrAfter(), "dd.MM.yyyy"));
		filters.validate();
	}

	@Test
	public void test_parse_date_3() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("loadedSameOrAfter", new String[] {"1.1.2000"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Unparseable timestamp: 1.1.2000", e.getMessage());
		}
	}

	@Test
	public void test_parse_qname() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("taxonId", new String[] {"MX.1", "A", "223", "dwc:MX.1"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("[MX.1, A, 223, dwc:MX.1]", filters.getTaxonId().toString());
		filters.validate();
	}

	@Test
	public void test_parse_coordinates() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {"666:333:YKJ"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		CoordinatesWithOverlapRatio c = filters.getCoordinates().iterator().next();
		assertEquals(666.0, c.getLatMin().doubleValue(), 0);
		assertEquals(667.0, c.getLatMax().doubleValue(), 0);
		assertEquals(333.0, c.getLonMin().doubleValue(), 0);
		assertEquals(334.0, c.getLonMax().doubleValue(), 0);
		assertEquals(Type.YKJ, c.getType());
		filters.validate();
	}

	@Test
	public void test_parse_coordinates_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {"60.3:61.1:-31.6:-30.0:WGS84"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());

		CoordinatesWithOverlapRatio c = filters.getCoordinates().iterator().next();

		assertEquals(60.3, c.getLatMin().doubleValue(), 0);
		assertEquals(61.1, c.getLatMax().doubleValue(), 0);
		assertEquals(-31.6, c.getLonMin().doubleValue(), 0);
		assertEquals(-30.0, c.getLonMax().doubleValue(), 0);
		assertEquals(Type.WGS84, c.getType());
		filters.validate();
	}

	@Test
	public void test_parse_coordinates_3() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {"a60.3:61.1:-30.0:-31.6:WGS84"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Non-numeric coordinate values: a60.3:61.1:-30.0:-31.6:WGS84 (For input string: \"a60.3\")", e.getMessage());
		}
	}

	@Test
	public void test_parse_coordinates_4() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {"60.3:-31.6:WGS_84"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid CRS: WGS_84", e.getMessage());
		}
	}

	@Test
	public void test_parse_coordinates_5() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {"60.3:-31.6"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid coordinate format: 60.3:-31.6", e.getMessage());
		}
	}

	@Test
	public void test_additional_coordinate_filters() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("coordinates", new String[] {
				"666:333:YKJ",
				"666:333:YKJ:0.5",
				"60.2342131232:30.132453353535:WGS84:0.50000012343535",
				"60.1:30.1:WGS84:",
				"60.2:30.2:WGS84"
		});
		input.put("wgs84CenterPoint", new String[] {
				"60.2342131232:30.132453353535:WGS84",
				"60.1:60.2:30.1:30.2:WGS84:"
		});
		input.put("ykj10km", new String[] {"666:333"});
		input.put("ykj1km", new String[] {"6666:3333"});
		input.put("ykj1kmCenter", new String[] {"6666:3333"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		String expectedCoordinates = "[" +
				"666.0:667.0:333.0:334.0:YKJ:null, " +
				"666.0:667.0:333.0:334.0:YKJ:0.5, " +
				"60.234213:60.234213:30.132453:30.132453:WGS84:0.5, " +
				"60.1:60.1:30.1:30.1:WGS84:null, " +
				"60.2:60.2:30.2:30.2:WGS84:null]";
		assertEquals(expectedCoordinates, filters.getCoordinates().toString());
		String expectedWgs84Center = "[60.234213:60.234213:30.132453:30.132453:WGS84:null, 60.1:60.2:30.1:30.2:WGS84:null]";
		assertEquals(expectedWgs84Center, filters.getWgs84CenterPoint().toString());
		assertEquals("[666.0:333.0]", filters.getYkj10km().toString());
		assertEquals("[6666.0:3333.0]", filters.getYkj1km().toString());
		assertEquals("[6666.0:3333.0]", filters.getYkj1kmCenter().toString());
		filters.validate();
	}

	@Test
	public void wgs84CenterpointGivenNonWgs84() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("wgs84CenterPoint", new String[] {
				"666:333:YKJ"
		});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("Invalid wgs84CenterPoint value: Filter by wgs84 centerpoint only supports coordinates in WGS84 format", e.getMessage());
		}
	}

	@Test
	public void polygonId_1() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygonId", new String[] {"1"});

		Filters f = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("1", f.getPolygonId().toString());
	}

	@Test
	public void polygonId_3_unknownid() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygonId", new String[] {"-1"});

		Filters f = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("-1", f.getPolygonId().toString());
		// When trying to use unknown polygon id error is thrown by vertica dao filters imple
	}

	@Test
	public void invalid_polygonIds() {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygonId", new String[] {"foo"});
		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail();
		} catch (Exception e) {
			assertEquals("Invalid polygon id: foo", e.getMessage());
		}
		input.put("polygonId", new String[] {"1:a"});
		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail();
		} catch (Exception e) {
			assertEquals("Invalid polygon id: 1:a", e.getMessage());
		}
	}

	@Test
	public void polygon_1() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygon", new String[] {"POLYGON((3556514 7171000,3557793 7169987,3558080 7172573,3556514 7171000)):YKJ"});

		Filters f = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("POLYGON ((556320 7167999, 557885 7169572, 557598 7166987, 556320 7167999)) : EUREF", f.getPolygon().toString());
	}

	@Test
	public void polygon_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygon", new String[] {"POLYGON((3556514 7171000,3557793 7169987,3558080 7172573,3556514 7171000)):YKJ"});

		Filters f = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("POLYGON ((556320 7167999, 557885 7169572, 557598 7166987, 556320 7167999)) : EUREF", f.getPolygon().toString());
	}

	@Test
	public void invalidPolygons() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("polygon", new String[] {"POLYGON((3556514 7171000,3557793 7169987,3558080 7172573,3556514 7171000)):YKJ"});
		Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		input.put("polygon", new String[] {"POLYGON((4 70,3557793 7169987,3558080 7172573,4 70))"});
		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail();
		} catch (Exception e) {
			assertEquals("Invalid polygon: Invalid Euref coordinates: 70 : 7172573 : 4 : 3558080 : EUREF", e.getMessage());
		}

		input.put("polygon", new String[] {"POLYGON((4 70,3557793 7169987,3558080 7172573,4 0))"});
		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail();
		} catch (Exception e) {
			assertEquals("Invalid polygon: Invalid WKT: Points of LinearRing do not form a closed linestring", e.getMessage());
		}

		input = new HashMap<>();
		input.put("polygon", new String[] {"POLYGON((3556514 7171000,3557793 7169987,3558080 7172573,3556514 7171000)):wgs"});
		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail();
		} catch (Exception e) {
			assertEquals("Invalid API parameter: Invalid crs: wgs", e.getMessage());
		}
	}

	@Test
	public void partition() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("partition", new String[] {
				"1/5"
		});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("1/5", filters.getPartition().toString());

		try {
			filters.addFilter("partition:a");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Use syntax 1/5 where 1 is index (1 based) and 5 the number of partitions", e.getMessage());
		}

		try {
			filters.addFilter("partition:5");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Use syntax 1/5 where 1 is index (1 based) and 5 the number of partitions", e.getMessage());
		}

		try {
			filters.addFilter("partition:5/2");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Index must be between 1 and number of partitions", e.getMessage());
		}

		try {
			filters.addFilter("partition:0/5");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Index must be between 1 and number of partitions", e.getMessage());
		}

		try {
			filters.addFilter("partition:1/a");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid number a", e.getMessage());
		}

		filters.addFilter("partition:5/5");
		filters.validate();
	}

	@Test
	public void test_unknown_parameter() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("foo", new String[] {"bar"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("Unknown parameter: foo", e.getMessage());
		}
	}

	@Test
	public void test_enum() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("sex", new String[] {"Male"});
		input.put("invasiveControl", new String[] {"full"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals("["+Sex.MALE+"]", filters.getSex().toString());
		assertEquals("["+InvasiveControl.FULL+"]", filters.getInvasiveControl().toString());
		filters.validate();
	}

	@Test
	public void test_enum_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("sex", new String[] {"Ukko"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid enumeration value: Ukko", e.getMessage());
		}
	}

	@Test
	public void test_enum_3() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("taxonAdminFiltersOperator", new String[] {"JAA"});

		try {
			Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid enumeration value: JAA", e.getMessage());
		}
	}

	@Test
	public void test_collections() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("collectionId", new String[] {"parent1"});

		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasSetFiltersExludingDefaults());
		assertEquals(new Qname("parent1").toURI(), filters.getCollectionId().iterator().next().toURI());
		filters.validate();
	}

	@Test
	public void test_hasTaxa() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("includeNonValidTaxa", new String[] {"true"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertFalse(filters.hasTaxonFilter());
		assertTrue(filters.getIncludeNonValidTaxa());
		filters.validate();
	}

	@Test
	public void test_hasTaxa_2() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("includeNonValidTaxa", new String[] {"false"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasTaxonFilter());
		assertFalse(filters.getIncludeNonValidTaxa());
		filters.validate();
	}

	@Test
	public void test_hasTaxa_3() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		filters.setIncludeNonValidTaxa(null);

		assertFalse(filters.hasTaxonFilter());
		assertNull(filters.getIncludeNonValidTaxa());
		filters.validate();
	}

	@Test
	public void test_hasTaxa_4() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("informalTaxonGroupId", new String[] {"MVL.1"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertTrue(filters.hasTaxonFilter());
		filters.validate();
	}

	@Test
	public void test_hasTaxa_5() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("informalTaxonGroupIdIncludingReported", new String[] {"MVL.1"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertFalse(filters.hasTaxonFilter());
		filters.validate();
	}

	@Test
	public void habitatFilters() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("primaryHabitat", new String[] {"X[1,2];Y[3]", "Z"});
		input.put("anyHabitat", new String[] {"X;", "Z[1]"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);

		assertEquals("[primaryHabitat:[X[1,2];Y[3];Z], anyHabitat:[X;Z[1]]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void unitFactFilter() throws Exception {
		Map<String, String[]> input = new HashMap<>();
		input.put("unitFact", new String[] {"fact1", "fact2=A;fact3=B"});
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false).addParameters(input);
		assertEquals("[unitFact:[fact1;fact2=A;fact3=B]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void test_filters_from_string() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false);
		filters.addFilter("target: [aves, susi]");
		filters.addFilter("time: [1800/2015]");
		filters.addFilter("loadedSameOrBefore: 2005-02-21");
		filters.addFilter("taxonRankId: [MX.species, http://tun.fi/MX.domain]");
		filters.addFilter("higherTaxon: false");
		filters.addFilter("dayOfYear: 10");
		filters.addFilter("recordBasis: [FOSSIL_SPECIMEN]");
		filters.addFilter("hasMedia: true");
		filters.addFilter("hasUnitMedia: false");
		filters.addFilter("ykj10km: [666:333]");
		filters.addFilter("ykj1km: [6666:3333, 6668:3333]");
		filters.addFilter("coordinates: [63.0131:64:32.0000000011:32.1313131313:WGS84]");
		filters.addFilter("coordinates: [60:30:WGS84,6666:6667:3333:3334:YKJ:0.02]");
		filters.addFilter("coordinates:[63.850291 : 63.863433 : 23.116263 : 23.124561 : WGS84 : null]");
		filters.addFilter("ykj1kmCenter: [6666:3333]");
		filters.addFilter("taxonCensus: [MX.1]");
		filters.addFilter("occurrenceCountMax: 123");
		filters.addFilter("occurrenceCountFinlandMax: 456");
		filters.addFilter("collectionAndRecordQuality: [PROFESSIONAL:NEUTRAL,UNCERTAIN;HOBBYIST:NEUTRAL]");
		filters.addFilter("collectionAndRecordQuality: [AMATEUR:NEUTRAL]");
		filters.addFilter("primaryHabitat:[X[1,2];Y[3];Z]");
		filters.addFilter("anyHabitat:[X;Z[1]]");
		filters.addFilter("unitFact:[fact1;fact2=A]");
		filters.addFilter("unitFact:fact3=B");
		filters.addFilter("partition:1/5");
		filters.addFilter("taxonAdminFiltersOperator:OR");
		filters.addFilter("namedPlaceTag: [MNP.tagHabitatFarmland, http://tun.fi/MNP.tagCensusRare]");
		assertEquals(23, filters.getSetFiltersExcludingDefaults().size());
		assertEquals("[aves, susi]", filters.getTarget().toString());
		assertEquals("[1800/2015]", filters.getTime().toString());
		assertEquals("21.2.2005", DateUtils.format(filters.getLoadedSameOrBefore(), "dd.M.yyyy"));
		assertEquals("[MX.species, MX.domain]", filters.getTaxonRankId().toString());
		assertEquals("false", filters.getHigherTaxon().toString());
		assertEquals("[10]", filters.getDayOfYear().toString());
		assertEquals("[FOSSIL_SPECIMEN]", filters.getRecordBasis().toString());
		assertEquals("true", filters.getHasMedia().toString());
		assertEquals("false", filters.getHasUnitMedia().toString());
		assertEquals("[666.0:333.0]", filters.getYkj10km().toString());
		assertEquals("[6666.0:3333.0, 6668.0:3333.0]", filters.getYkj1km().toString());
		String expectedCoordinates = "[" +
				"63.0131:64.0:32.0:32.131313:WGS84:null, " +
				"60.0:60.0:30.0:30.0:WGS84:null, " +
				"6666.0:6667.0:3333.0:3334.0:YKJ:0.02, " +
				"63.850291:63.863433:23.116263:23.124561:WGS84:null]";
		assertEquals(expectedCoordinates, filters.getCoordinates().toString());
		assertEquals("[6666.0:3333.0]", filters.getYkj1kmCenter().toString());
		assertEquals(null, filters.getBiogeographicalProvinceId());
		assertEquals("[MX.1]", filters.getTaxonCensus().toString());
		assertEquals(123, filters.getOccurrenceCountMax().intValue());
		assertEquals(456, filters.getOccurrenceCountFinlandMax().intValue());
		assertEquals("[PROFESSIONAL:NEUTRAL,UNCERTAIN, HOBBYIST:NEUTRAL, AMATEUR:NEUTRAL]", filters.getCollectionAndRecordQuality().toString());
		assertEquals("[X[1,2], Y[3], Z]", filters.getPrimaryHabitat().toString());
		assertEquals("[X, Z[1]]", filters.getAnyHabitat().toString());
		assertEquals("[fact1, fact2=A, fact3=B]", filters.getUnitFact().toString());
		assertEquals("1/5", filters.getPartition().toString());
		assertEquals("OR", filters.getTaxonAdminFiltersOperator().toString());
		assertEquals("[MNP.tagHabitatFarmland, MNP.tagCensusRare]", filters.getNamedPlaceTag().toString());

		filters.validate();
	}

	@Test
	public void donotAllowFiltersFromTooLowBase() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.GATHERING, false);
		try {
			filters.addFilter("target: [aves, susi]");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("target requires base UNIT but was tried to use with base GATHERING", e.getMessage());
		}
		filters.addFilter("dayOfYear: 10");
		filters.addFilter("hasMedia: true");
		filters.addFilter("loadedSameOrBefore: 2005-02-21");
		filters.addFilter("taxonCensus: [MX.1]");
		filters.validate();
	}

	@Test
	public void donotAllowFiltersFromTooLowBase_2() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.DOCUMENT, false);
		try {
			filters.addFilter("target: [aves, susi]");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("target requires base UNIT but was tried to use with base DOCUMENT", e.getMessage());
		}

		try {
			filters.addFilter("taxonCensus: [MX.1]");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("taxonCensus requires base GATHERING but was tried to use with base DOCUMENT", e.getMessage());
		}

		try {
			filters.addFilter("dayOfYear: 10");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("dayOfYear requires base GATHERING but was tried to use with base DOCUMENT", e.getMessage());
		}

		filters.addFilter("hasMedia: true");
		filters.addFilter("loadedSameOrBefore: 2005-02-21");
		filters.validate();
	}

	@Test
	public void statisticsFiltersOnly() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, true);
		assertEquals("[useIdentificationAnnotations:true, includeSubTaxa:true, includeNonValidTaxa:true, individualCountMin:1, wild:[WILD,WILD_UNKNOWN], qualityIssues:NO_ISSUES]", filters.getSetFiltersIncludingDefaults().toString());
		assertEquals("[]", filters.getSetFiltersExcludingDefaults().toString());

		filters.addFilter("yearMonth: 2000-01");
		filters.addFilter("time: 2000-01-01");
		try {
			filters.addFilter("documentId: JX.1");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals("documentId can not be used with statistics queries, onlyStatistics=true", e.getMessage());
		}

		assertEquals("[time:[2000-01-01], yearMonth:[2000-01]]", filters.getSetFiltersExcludingDefaults().toString());
		filters.validate();
	}

	@Test
	public void privateWarehouseFiltersOnly() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false);
		filters.addFilter(Const.EDITOR_ID_FILTER+": MA.5");

		filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false);
		try {
			filters.addFilter(Const.EDITOR_ID_FILTER+": MA.5");
			fail("Should throw exception");
		} catch (UnsupportedFilterException e) {
			assertEquals(Const.EDITOR_ID_FILTER +" can not be used with PUBLIC warehouse query", e.getMessage());
		}
		filters.validate();
	}

	@Test
	public void dateFilterBeginingEndDayTests() throws ParseException {
		Date begining = DateUtils.convertToDate("2021-08-16", "yyyy-MM-dd");
		assertEquals("Mon Aug 16 00:00:00 EEST 2021", begining.toString());
		Date end = org.apache.commons.lang3.time.DateUtils.addMilliseconds(org.apache.commons.lang3.time.DateUtils.ceiling(begining, Calendar.DATE), -1);
		assertEquals("Mon Aug 16 23:59:59 EEST 2021", end.toString());
	}

	@Test
	public void filterToStringLengthValidation() throws NoSuchFieldException {
		Filters filters = Filters.createEmptyFilters(Concealment.PRIVATE, Base.UNIT, false);
		for (int i = 0; i<200; i++) {
			filters.setCollectionId(new Qname("HR."+i));
		}
		filters.validate();
		for (int i = 0; i<600; i++) {
			filters.setCollectionId(new Qname("HR."+i));
		}
		try {
			filters.validate();
			fail("Should fail");
		} catch (IllegalArgumentException e) {
			assertEquals("Too long value given for filter 'collectionId'. Maximum is 4000 chars.", e.getMessage());
		}
	}

	@Test
	public void hasValueAcceptsFields() throws NoSuchFieldException {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false); // unit level
		filters.setHasValue("unit.unitId");
		filters.setHasValue("gathering.conversions.year");
		filters.setHasValue("gathering.gatheringSection");
		filters.setHasValue("document.namedPlaceId");
		filters.setHasValue("document.namedPlace.alternativeId");
		filters.validate();

		filters.setHasValue("document.keywords"); // array can't be accepted
		try {
			filters.validate();
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("document.keywords is not allowed for hasValue filter: it does not support array fields.", e.getMessage());
		}

		filters.getHasValue().clear();

		filters.setHasValue("foobar"); // unknown field
		try {
			filters.validate();
			fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: foobar", e.getMessage());
		}

		filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.GATHERING, false); // gathering level
		filters.setHasValue("unit.unitId");
		try {
			filters.validate();
			fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base GATHERING: unit.unitId", e.getMessage());
		}
	}

	@Test
	public void hasNamedPlaceFilter() {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false);
		assertFalse(filters.hasNamedPlaceFilter());

		filters.setNamedPlaceId(new Qname("foobar"));
		assertFalse(filters.hasNamedPlaceFilter());

		filters.setBirdAssociationAreaId(new Qname("foobar"));
		assertFalse(filters.hasNamedPlaceFilter());

		filters.setNamedPlaceTag(new Qname("foobar"));
		assertTrue(filters.hasNamedPlaceFilter());
	}

	@Test
	public void hasNamedPlaceFilter_2() {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false);
		assertFalse(filters.hasNamedPlaceFilter());

		filters.setHasValue("document.namedPlaceId");
		assertFalse(filters.hasNamedPlaceFilter());

		filters.setHasValue("document.namedPlace.birdAssociationAreaDisplayName");
		assertTrue(filters.hasNamedPlaceFilter());
	}
	
	@Test
	public void editorOrObserverIdIsNotErrorWithoutPrivateOrSystemPermission() {
		Filters filters = Filters.createDefaultFilters(Concealment.PUBLIC, Base.UNIT, false);
		
		try {
			filters.addFilter("editorOrObserverIdIsNot: MA.1");
			fail();
		} catch (UnsupportedFilterException e) {
			assertEquals("editorOrObserverIdIsNot can not be used with PUBLIC warehouse query", e.getMessage());
		}
	}
	
	@Test
	public void editorOrObserverIdIsNotIsAddedWithPrivate() {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false);
		
		filters.addFilter("editorOrObserverIdIsNot: MA.1");
		assertEquals("[MA.1]", filters.getEditorOrObserverIdIsNot().toString());
	}
	
	@Test
	public void editorOrObserverIdIsNotIsAddedWithPublicAndSystemPermission() {
		Filters filters = Filters.createDefaultFilters(Concealment.PRIVATE, Base.UNIT, false, true);
		
		filters.addFilter("editorOrObserverIdIsNot: MA.1");
		assertEquals("[MA.1]", filters.getEditorOrObserverIdIsNot().toString());
	}
}
