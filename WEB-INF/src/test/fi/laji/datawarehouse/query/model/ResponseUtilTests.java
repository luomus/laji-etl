package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.text.ParseException;

import org.junit.Test;

import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.vertica.VerticaDimensionsDAOStub;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.Const.FeatureType;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.laji.datawarehouse.query.model.queries.AggregatedQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.laji.datawarehouse.query.model.responses.AggregateResponse;
import fi.laji.datawarehouse.query.model.responses.ListResponse;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class ResponseUtilTests {

	private VerticaDimensionsDAO dao = new VerticaDimensionsDAOStub();

	@Test
	public void test1_aggregate() throws Exception {
		AggregatedQuery query = new AggregatedQuery(getBase(), new AggregateBy(), 10, 1);
		query.getAggregateBy().addField("document.documentId"); //  row.addAggregateValue("x");
		query.getAggregateBy().addField("gathering.conversions.month"); // row.addAggregateValue(1);
		query.getAggregateBy().addField("unit.annotations.annotationByPerson"); // row.addAggregateValue(3L);
		query.getAggregateBy().addField("gathering.conversions.wgs84Grid05.lon"); // row.addAggregateValue(2.2);
		query.getAggregateBy().addField("unit.breedingSite"); // row.addAggregateValue(true);
		AggregateResponse response = new AggregateResponse(1, 1, 10, Utils.list(generateAggregate()));
		JSONObject json = new ResponseUtil(dao).buildAggregateResponse(response, query);
		assertEquals(getExpected("expected_res_1.json").beautify(), json.beautify());
	}

	@Test
	public void test2_aggregateGeoJsonGrid() throws Exception {
		AggregatedQuery query = new AggregatedQuery(getBase(), new AggregateBy(), 10, 1);
		query.getAggregateBy().addField("document.documentId");
		query.getAggregateBy().addField("gathering.conversions.wgs84Grid01.lon");
		query.getAggregateBy().addField("gathering.conversions.wgs84Grid05.lat");
		AggregateResponse response = new AggregateResponse(1, 1, 10, Utils.list(generateGeoAggregate(23.0, -59.5)));
		JSONObject json = new ResponseUtil(dao).buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_2.json").beautify(), json.beautify());
	}

	@Test
	public void test9_aggregateGeoJsonCenterPoint() throws Exception {
		AggregatedQuery query = new AggregatedQuery(getBase(), new AggregateBy(), 10, 1);
		query.getAggregateBy().addField("document.documentId");
		query.getAggregateBy().addField("gathering.conversions.wgs84CenterPoint.lon");
		query.getAggregateBy().addField("gathering.conversions.wgs84CenterPoint.lat");
		AggregateResponse response = new AggregateResponse(1, 1, 10, Utils.list(generateGeoAggregate(53.2324252, 0.0222)));
		JSONObject json = new ResponseUtil(dao).buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_9.json").beautify(), json.beautify());
	}

	@Test
	public void test3_aggregateGeoJsonEnvelope() throws Exception {
		AggregatedQuery query = new AggregatedQuery(getBase(), new AggregateBy(), 10, 1);
		query.getAggregateBy().addField("document.documentId");
		query.getAggregateBy().addField("gathering.conversions.ykj.latMax");
		query.getAggregateBy().addField("gathering.conversions.ykj.latMin");
		query.getAggregateBy().addField("gathering.conversions.ykj.lonMax");
		query.getAggregateBy().addField("gathering.conversions.ykj.lonMin");

		AggregateResponse response = new AggregateResponse(1, 1, 10, Utils.list(generateGeoAggregateEnvelope()));
		JSONObject json = new ResponseUtil(dao).buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_3.json").beautify(), json.beautify());
	}

	@Test
	public void test4_list() throws Exception {
		ListQuery query = new ListQuery(getBase(), 1, 10);
		ListResponse response = new ListResponse(1, 1, 10, Utils.list(generateList()));
		query.setSelected(new Selected("unit", "document", "gathering.conversions.wgs84WKT"));
		JSONObject json = new ResponseUtil().buildListResponse(response, query);
		assertEquals(getExpected("expected_res_4.json").beautify(), json.beautify());
	}

	@Test
	public void test5_listGeoJson_centerp() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PUBLIC).setCaller(this.getClass()).setApiSourceId("foo")
				.setCrs(Coordinates.Type.WGS84).setFeatureType(FeatureType.CENTER_POINT)
				.build(), 1, 1);
		query.setSelected(new Selected().add("unit").add("document.collectionId").add("gathering.gatheringId"));
		ListResponse response = new ListResponse(1, 1, 1, Utils.list(generateList()));
		JSONObject json = new ResponseUtil().buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_5.json").beautify(), json.beautify());
	}

	@Test
	public void test6_listGeoJson_envelope() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PUBLIC).setCaller(this.getClass()).setApiSourceId("foo")
				.setCrs(Coordinates.Type.YKJ).setFeatureType(FeatureType.ENVELOPE)
				.build(), 1, 1);
		query.setSelected(new Selected().add("unit").add("document.collectionId").add("gathering.gatheringId"));
		ListResponse response = new ListResponse(1, 1, 1, Utils.list(generateList()));
		JSONObject json = new ResponseUtil().buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_6.json").beautify(), json.beautify());
	}

	@Test
	public void test7_listGeoJson_orig_geo_simple() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PUBLIC).setCaller(this.getClass()).setApiSourceId("foo")
				.setCrs(Coordinates.Type.WGS84).setFeatureType(FeatureType.ORIGINAL_FEATURE)
				.build(), 1, 1);
		query.setSelected(new Selected().add("unit").add("document.collectionId").add("gathering.gatheringId"));
		ListResponse response = new ListResponse(1, 1, 1, Utils.list(generateList()));
		JSONObject json = new ResponseUtil().buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_7.json").beautify(), json.beautify());
	}

	@Test
	public void test8_listGeoJson_orig_geo_multi() throws Exception {
		ListQuery query = new ListQuery(
				new BaseQueryBuilder(Concealment.PUBLIC).setCaller(this.getClass()).setApiSourceId("foo")
				.setCrs(Coordinates.Type.EUREF).setFeatureType(FeatureType.ORIGINAL_FEATURE)
				.build(), 1, 1);
		query.setSelected(new Selected().add("unit").add("document.collectionId").add("gathering.gatheringId"));
		ListResponse response = new ListResponse(1, 1, 1, Utils.list(generateList()));
		JSONObject json = new ResponseUtil().buildGeoJsonResponse(response, query);
		assertEquals(getExpected("expected_res_8.json").beautify(), json.beautify());
	}

	private JoinedRow generateList() throws CriticalParseFailure, DataValidationException {
		JoinedRow row = new JoinedRow(new Unit(new Qname("u")), new Gathering(new Qname("g")), new Document(Concealment.PUBLIC, new Qname("s"), new Qname("d"), new Qname("c")));
		row.getGathering().createConversions();
		row.getGathering().getConversions().setWgs84CenterPoint(new SingleCoordinates(4.2323, 1.2323, Coordinates.Type.WGS84));
		row.getGathering().getConversions().setYkj(new Coordinates(666, 667, 333, 334, Type.YKJ).toFullYkjLength());
		row.getGathering().getConversions().setEurefGeo(
				Geo.fromWKT("GEOMETRYCOLLECTION (LINESTRING(321235 6612345,328888 6637777), POINT(323434.0 6634343.1)))", Coordinates.Type.EUREF)
				.validate());
		row.getGathering().getConversions().setWgs84Geo(Geo.fromWKT("LINESTRING(25.078564975491 60.228716112882, 24.52 62.25)", Coordinates.Type.WGS84));
		row.getUnit().addMedia(new MediaObject(MediaType.IMAGE, "https://picurl.example/1"));
		row.getUnit().addMedia(new MediaObject(MediaType.IMAGE, "https://picurl.example/2"));
		row.getUnit().addKeyword("foo");
		row.getUnit().addKeyword("bar");
		return row;
	}

	private AggregateRow generateAggregate() throws Exception {
		AggregateRow row = new AggregateRow();
		row.setCount(13L);
		row.setFirstLoadDateMax(DateUtils.convertToDate("1.1.2000"));
		row.setFirstLoadDateMin(DateUtils.convertToDate("1.1.2000"));
		row.setIndividualCountMax(5);
		row.setIndividualCountSum(20L);
		row.setLineLengthSum(33333L);
		row.setNewestRecord(20000101L);
		row.setOldestRecord(20000101L);
		row.setPairCountMax(5);
		row.setPairCountSum(20L);
		row.setSpeciesCount(2L);
		row.setTaxonCount(3L);
		row.addAggregateValue("x");
		row.addAggregateValue(1);
		row.addAggregateValue(3L);
		row.addAggregateValue(2.2);
		row.addAggregateValue(true);
		return row;
	}

	private AggregateRow generateGeoAggregate(double lon, double lat) throws ParseException {
		AggregateRow row = new AggregateRow();
		row.setCount(13L);
		row.setFirstLoadDateMax(DateUtils.convertToDate("1.1.2000"));
		row.setFirstLoadDateMin(DateUtils.convertToDate("1.1.2000"));
		row.setIndividualCountMax(5);
		row.setIndividualCountSum(20L);
		row.setLineLengthSum(33333L);
		row.setNewestRecord(20000101L);
		row.setOldestRecord(20000101L);
		row.setPairCountMax(5);
		row.setPairCountSum(20L);
		row.setSpeciesCount(2L);
		row.setTaxonCount(3L);
		row.setAtlasCodeMax("atcode1");
		row.setAtlasClassMax("atlclass1");
		row.setRecordQualityMax(1L);
		row.setRedListStatusMax("redListStatus1");
		row.addAggregateValue("docid");
		row.addAggregateValue(lon);
		row.addAggregateValue(lat);
		return row;
	}

	private AggregateRow generateGeoAggregateEnvelope() {
		AggregateRow row = new AggregateRow();
		row.setCount(13L);
		row.addAggregateValue("docid");
		row.addAggregateValue(12.232);
		row.addAggregateValue(-55.24242);
		row.addAggregateValue(0.0);
		row.addAggregateValue(35.3222232232);
		return row;
	}

	private BaseQuery getBase() {
		return new BaseQueryBuilder(Concealment.PUBLIC).setCaller(this.getClass()).setApiSourceId("foo")
				.build();
	}

	private JSONObject getExpected(String filename) {
		URL url = ResponseUtilTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return new JSONObject(data);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
