package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.query.model.queries.Selected;

public class SelectedFieldsSelectionTests {

	@Test
	public void test() throws NoSuchFieldException {
		Selected selected = new Selected(Base.UNIT);
		selected.add("document.documentId");
		selected.add("document.facts.fact");
		selected.add("document.facts.value");
		selected.add("unit.facts"); // unit.facts.*

		try {
			selected.add("document.blaa");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document.blaa", e.getMessage());
		}

		selected.add("document.keywords"); // document.keywords
		try {
			selected.add("document.keyword");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: document.keyword", e.getMessage());
		}

		System.out.println(selected.getSelectedFields().toString());
		assertEquals(8, selected.getSelectedFields().size());

		assertTrue(selected.getSelectedFields().contains("document.documentId"));
		assertTrue(selected.getSelectedFields().contains("document.keywords"));

		assertTrue(selected.getSelectedFields().contains("document.facts.fact"));
		assertTrue(selected.getSelectedFields().contains("document.facts.value"));

		assertTrue(selected.getSelectedFields().contains("unit.facts.fact"));
		assertTrue(selected.getSelectedFields().contains("unit.facts.value"));
		assertTrue(selected.getSelectedFields().contains("unit.facts.integerValue"));
		assertTrue(selected.getSelectedFields().contains("unit.facts.decimalValue"));
	}

	@Test
	public void test_2() throws NoSuchFieldException {
		Selected selected = new Selected(Base.UNIT);
		selected.add("unit"); // unit.*

		System.out.println(selected.getSelectedFields().toString());
		assertTrue(selected.getSelectedFields().contains("unit.notes"));
		assertTrue(selected.getSelectedFields().contains("unit.linkings.taxon.id"));
	}

	@Test
	public void test_3() throws NoSuchFieldException {
		Selected selected = new Selected(Base.UNIT);
		selected.add("document.linkings");
		assertEquals(
				"[document.linkings.collectionQuality, document.linkings.editors.fullName, document.linkings.editors.id, document.linkings.editors.userId]",
				selected.getSelectedFields().toString());
	}

	@Test
	public void test_default_base_is_unit() throws NoSuchFieldException {
		Selected selected = new Selected();
		selected.add("unit.annotations.id");

		try {
			selected.add("annotation.id");
			Assert.fail();
		} catch (NoSuchFieldException e) {
			assertEquals("No such field for base UNIT: annotation.id", e.getMessage());
		}


		selected = new Selected(Base.ANNOTATION);
		selected.add("unit.annotations.id");
		selected.add("annotation.id");

		assertEquals("[annotation.id, unit.annotations.id]", selected.getSelectedFields().toString());
	}

	@Test
	public void annotation() throws NoSuchFieldException {
		Selected selected = new Selected(Base.ANNOTATION);
		selected.add("annotation.identification").add("unit.unitId");
		String expected = "[annotation.identification.author, annotation.identification.facts.decimalValue, annotation.identification.facts.fact, annotation.identification.facts.integerValue, annotation.identification.facts.value, annotation.identification.id, annotation.identification.linkings.taxon.administrativeStatuses, annotation.identification.linkings.taxon.checklist, annotation.identification.linkings.taxon.cursiveName, annotation.identification.linkings.taxon.finnish, annotation.identification.linkings.taxon.id, annotation.identification.linkings.taxon.informalTaxonGroups, annotation.identification.linkings.taxon.kingdomScientificName, annotation.identification.linkings.taxon.latestRedListStatusFinland.status, annotation.identification.linkings.taxon.latestRedListStatusFinland.year, annotation.identification.linkings.taxon.nameEnglish, annotation.identification.linkings.taxon.nameFinnish, annotation.identification.linkings.taxon.nameSwedish, annotation.identification.linkings.taxon.occurrenceCountFinland, annotation.identification.linkings.taxon.primaryHabitat.habitat, annotation.identification.linkings.taxon.primaryHabitat.habitatSpecificTypes, annotation.identification.linkings.taxon.primaryHabitat.id, annotation.identification.linkings.taxon.primaryHabitat.order, annotation.identification.linkings.taxon.scientificName, annotation.identification.linkings.taxon.scientificNameAuthorship, annotation.identification.linkings.taxon.scientificNameDisplayName, annotation.identification.linkings.taxon.sensitive, annotation.identification.linkings.taxon.taxonConceptIds, annotation.identification.linkings.taxon.taxonRank, annotation.identification.linkings.taxon.taxonomicOrder, annotation.identification.linkings.taxon.threatenedStatus, annotation.identification.linkings.taxon.vernacularName, annotation.identification.notes, annotation.identification.taxon, annotation.identification.taxonID, annotation.identification.taxonSpecifier, annotation.identification.taxonSpecifierAuthor, unit.unitId]";
		assertEquals(expected, selected.toString());
		assertEquals(expected, selected.getSelectedFields().toString());
	}

	@Test
	public void unitMedia() throws NoSuchFieldException {
		Selected selected = new Selected(Base.UNIT_MEDIA);
		selected.add("unit.media").add("unit.unitId");
		String expected = "[unit.media.author, unit.media.caption, unit.media.copyrightOwner, unit.media.fullResolutionMediaAvailable, unit.media.fullURL, unit.media.highDetailModelURL, unit.media.licenseId, unit.media.lowDetailModelURL, unit.media.mediaType, unit.media.mp3URL, unit.media.squareThumbnailURL, unit.media.thumbnailURL, unit.media.type, unit.media.videoURL, unit.media.wavURL, unit.unitId]";
		assertEquals(expected, selected.toString());
		assertEquals(expected, selected.getSelectedFields().toString());
	}

	@Test
	public void taxonId() throws Exception {
		Selected selected = new Selected(Base.UNIT_MEDIA).add("unit.linkings.taxon.id");
		assertEquals("[unit.linkings.taxon.id]", selected.getSelectedFields().toString());
	}
}
