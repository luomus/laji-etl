package fi.laji.datawarehouse.query.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.utils.TeamTextSearchUtil;

public class TeamTextSearchUtilTests {

	//	Pekka Kajalo
	//	P. Helle
	//	A.Sandsman												->	A. Sandsman
	//	Ranta, P.
	//	Pesola, Keijo K.
	//	Fraser-Pierre, R. F.
	//	MENONEN JUSSI (123)
	//	Pekka Palonen & Meeri Hildén							->	Pekka Palonen; Meeri Hildén
	//	T. Liikanen, F. Korpela, B. Oksanen ja O. Kirtanen		-> 	T. Liikanen; F. Korpela; B. Oksanen; O. Kirtanen
	//	Liikkonen Mauri & Salo V-M.							-	>	Liikkonen Mauri; Salo V-M.
	//	Pikkarainen,Finnewom,Hiirto								->	Pikkarainen; Finnewom; Hiirto
	//	F. ja G. Pikarinen										->	F. Pikarinen; G. Pikarinen
	//	J-P. & P. Turila										->	J-P. Turila; P. Turila
	//	F.Pakkanen & S.Pitkäranta								->	F. Pakkanen; S. Pitkäranta
	//	Jussi Jokinen,R. Aho ja J. Kiikanen						->	Jussi Jokinen; R. Aho; J. Kiikanen
	//	Jaakko L. & Matilda Haasonen							->	Jaakko L. Haasonen; Matilda Haasonen
	//	A. Ahola, B. & C. Tykkö									->	A Ahola; B. Tykkö; C. Tykkö
	//	von bonsdorff, R.
	//	Bonsdorff von Robert
	//	Bonsdorff, Tea von
	//	Vuori, Pekka; Bonsdorff, Tea von

	@Test
	public void test() {
		assertEquals("[]", TeamTextSearchUtil.combinations(null).toString());
		assertEquals("[]", TeamTextSearchUtil.combinations("").toString());
		assertEquals("[a]", TeamTextSearchUtil.combinations("a").toString());

		String abExpected = "['a b', 'a, b', 'b a', 'b, a']";
		assertEquals(abExpected, s(TeamTextSearchUtil.combinations("a b")));
		assertEquals(abExpected, s(TeamTextSearchUtil.combinations("a b b")));
		assertEquals(abExpected, s(TeamTextSearchUtil.combinations(" a  b ")));
		assertEquals("a, b", TeamTextSearchUtil.combinations(" a  b ").get(1));

		String abcExpected = "['a b c', 'a, b c', 'a b, c', 'a c b', 'a, c b', 'a c, b', 'b a c', 'b, a c', 'b a, c', 'b c a', 'b, c a', 'b c, a', 'c a b', 'c, a b', 'c a, b', 'c b a', 'c, b a', 'c b, a']";
		assertEquals(abcExpected, s(TeamTextSearchUtil.combinations("a b c")));

		assertEquals("[a b c d]", TeamTextSearchUtil.combinations("a b c d").toString());
		assertEquals("[a b c d e]", TeamTextSearchUtil.combinations("a b c d e").toString());
		assertEquals("[a b c d e]", TeamTextSearchUtil.combinations("a b c d e").toString());

		assertEquals("['lahti kari', 'lahti, kari', 'kari lahti', 'kari, lahti']", s(TeamTextSearchUtil.combinations("Lahti Kari")));

		assertEquals("['lahti kari', 'lahti, kari', 'kari lahti', 'kari, lahti']", s(TeamTextSearchUtil.combinations("Lahti, Kari")));

		String expected = "[" + 
				"'tea von bonsdorff', 'tea, von bonsdorff', 'tea von, bonsdorff', 'tea bonsdorff von', 'tea, bonsdorff von', 'tea bonsdorff, von', "+
				"'von tea bonsdorff', 'von, tea bonsdorff', 'von tea, bonsdorff', 'von bonsdorff tea', 'von, bonsdorff tea', 'von bonsdorff, tea', "+
				"'bonsdorff tea von', 'bonsdorff, tea von', 'bonsdorff tea, von', 'bonsdorff von tea', 'bonsdorff, von tea', 'bonsdorff von, tea']";

		assertEquals(expected, s(TeamTextSearchUtil.combinations("Tea von Bonsdorff")));
	}

	private String s(List<String> combinations) {
		List<String> debug = new ArrayList<>();
		for (String s : combinations) {
			debug.add("'" + s + "'");
		}
		return debug.toString();
	}

}
