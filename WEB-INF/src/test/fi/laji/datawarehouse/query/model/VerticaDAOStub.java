package fi.laji.datawarehouse.query.model;

import java.util.List;

import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.dao.VerticaQueryDAO;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;

public class VerticaDAOStub implements VerticaDAO {

	@Override
	public int save(List<Document> docs, ThreadStatusReporter statusReporter) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Document document, ThreadStatusReporter statusReporter) {
		// Auto-generated method stub
		
	}

	@Override
	public VerticaQueryDAO getQueryDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void performCleanUp(ThreadStatusReporter statusReporter) {
		// Auto-generated method stub
		
	}

	@Override
	public void close() {
		// Auto-generated method stub
		
	}

	@Override
	public void callGeoUpdate() {
		// Auto-generated method stub
		
	}

}
