package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.Distribution;
import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.Pass;
import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.Period;
import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.ValidationRuleException;
import fi.laji.datawarehouse.etl.models.AutomatedTaxonValidator.ValidationRules;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class AutomatedTaxonValidatorTests {

	private static final Qname TAXON_ID = new Qname("MX.123");

	@Test
	public void ruleValidations_duplicates() {
		ValidationRules rules = new ValidationRules();

		rules.rule(TAXON_ID, Distribution.allFinland());
		try {
			rules.rule(TAXON_ID, Distribution.allFinland());
			fail("Should throw exception because duplicate rules");
		} catch (ValidationRuleException e) {
			assertEquals("Rule already exists for " + TAXON_ID, e.getMessage());
		}

		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.6.", "31.8."));

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.10", "31.11."));
			fail("Should throw exception because duplicate rules");
		} catch (ValidationRuleException e) {
			assertEquals("Rule already exists for life stage 'ADULT' for " + TAXON_ID, e.getMessage());
		}

		rules.rule(TAXON_ID, LifeStage.LARVA, new Period("1.5.", "31.6."));
	}

	@Test
	public void ruleValidations_period() {
		ValidationRules rules = new ValidationRules();

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT);
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("No rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period[0]);
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("No rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("-1", "9999"));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid season for MX.123", e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("a.b.", "c.d."));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid season for MX.123", e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("123", "abc"));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid season for MX.123", e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("12.3", "abc"));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid season for MX.123", e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, LifeStage.ADULT, new Period("5.10.", "9999"));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid season for MX.123", e.getMessage());
		}

		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.6.", "31.8."), new Period("1.9", "31.10."), new Period("31.12.", "1.1."));
		rules.rule(TAXON_ID, null, new Period("1.6.", "31.8."));
		rules.rule(TAXON_ID, LifeStage.LARVA, new Period("1.4.", "31.5."));
	}

	@Test
	public void ruleValidations_distribution() {
		ValidationRules rules = new ValidationRules();

		try {
			rules.rule(TAXON_ID);
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("No rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, new Distribution[0]);
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("No rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, Distribution.allFinland(-1, 9999));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid year -1 for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, Distribution.allFinland(1700, 9999));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid year 9999 for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID, Distribution.allFinland(2000, 1999));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("End before begin for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2000),
					Distribution.allFinland(null, 2005));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Unbounded begin year in middle of rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2000),
					Distribution.allFinland(2001, 2005),
					Distribution.allFinland(null, 2010));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Unbounded begin year in middle of rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(2000, 2005),
					Distribution.allFinland(2006, null),
					Distribution.allFinland(2010, null));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Unbounded end year in middle of rules for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2005),
					Distribution.allFinland(2005, 2006));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges overlap for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2005),
					Distribution.allFinland(2006, 2010),
					Distribution.allFinland(2010, null));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges overlap for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2005),
					Distribution.allFinland(2006, 2010),
					Distribution.allFinland(2009, null));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges overlap for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2005),
					Distribution.allFinland(2000, 2010));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges overlap for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(null, 2005),
					Distribution.allFinland(2000, 2004));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges overlap for " + TAXON_ID, e.getMessage());
		}

		try {
			rules.rule(TAXON_ID,
					Distribution.allFinland(2000, 2005),
					Distribution.allFinland(1990, 1999));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Ranges not in ascending order for " + TAXON_ID, e.getMessage());
		}

		int taxonId = 1;
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(null, null));
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(null, 2000));
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(2000, null));
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(2000, 2000));
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(null, 2000), Distribution.allFinland(2001, null));
		rules.rule(new Qname("MX"+(taxonId++)), Distribution.allFinland(null, 2000), Distribution.allFinland(2010, null));

		try {
			rules.rule(TAXON_ID, new Distribution(null, 2000, null));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Allowed area not defined for " + TAXON_ID, e.getMessage());
		}

		testInvalidWkt("Invalid wkt", "foobarwkt",  rules);
		testInvalidWkt("Only polygons allowed", "LINESTRING(21.513443 65.263246,31.956533 65.02583)", rules);
		testInvalidWkt("Only polygons allowed", "POINT(27.118516 64.155046)", rules);
		testInvalidWkt("Invalid wkt", "POLYGON((25.919979 67.017948,27.707695 67.017948,27.707695 67.706003,25.919979 67.706003,25.919979 67.017948", rules); // missing ))
		testInvalidWkt("Allowed area outside Finland", "POLYGON((13.770514 69.332478,12.782343 69.260683,13.114941 69.038721,13.770514 69.332478))", rules);

		rules.rule(new Qname("MX"+(taxonId++)), new Distribution(1990, null,
				"POLYGON((25.462656 70.309169,24.340137 68.542395,31.033679 67.244144,30.326908 69.769887,27.790845 70.406993,25.462656 70.309169))\r\n" +
				"POLYGON((29.952735 65.252603,28.580766 65.042928,28.248168 64.40389,29.204388 63.58364,31.449427 63.639074,31.449427 64.511447,29.952735 65.252603))"));

		rules.rule(new Qname("MX"+(taxonId++)), new Distribution(1990, null,
				"MULTIPOLYGON " +
						"(((25.462656 70.309169,24.340137 68.542395,31.033679 67.244144,30.326908 69.769887,27.790845 70.406993,25.462656 70.309169)), "
						+ "((29.952735 65.252603,28.580766 65.042928,28.248168 64.40389,29.204388 63.58364,31.449427 63.639074,31.449427 64.511447,29.952735 65.252603)))"));
	}

	private void testInvalidWkt(String expectedError, String wkt, ValidationRules rules) {
		try {
			rules.rule(TAXON_ID, new Distribution(null, 2000, wkt));
			fail("Should not pass validation");
		} catch (ValidationRuleException e) {
			assertEquals("Invalid geometry: " + expectedError + " for " + TAXON_ID, e.getMessage());
		}
	}

	@Test
	public void noRules() throws Exception {
		ValidationRules rules = new ValidationRules();
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);
		assertEquals(Pass.NOT_VALIDATED, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(61, 24, Type.WGS84)));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT,  date("2.1.2000")));
	}

	private DateRange date(String date) throws Exception {
		return new DateRange(DateUtils.convertToDate(date));
	}

	private DateRange date(String begin, String end) throws Exception {
		return new DateRange(DateUtils.convertToDate(begin), DateUtils.convertToDate(end));
	}

	@Test
	public void distributionAllFinland() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, Distribution.allFinland());
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(61, 24, Type.WGS84)));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("2.1.2000")));
	}

	@Test
	public void distributionSingleSameAllYears() throws Exception {
		ValidationRules rules = new ValidationRules();
		// Validation area: Polygon around Joensuu
		rules.rule(TAXON_ID, new Distribution("POLYGON((29.400651 62.686603,29.650031 62.341824,30.188938 62.385704,30.272306 62.604568,29.794123 62.826473,29.400651 62.686603))"));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(61, 24, Type.WGS84))); // somewhere way of
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(62.614271, 29.772966, Type.WGS84))); // Joensuu centre

		// This area slightly touches validation area
		Coordinates c = Geo.fromWKT("POLYGON((30.14532 62.319711,30.289192 62.319711,30.289192 62.396684,30.14532 62.396684,30.14532 62.319711))", Type.WGS84).getBoundingBox();
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), c));

		// This area completely overlaps the validation area
		c = Geo.fromWKT("POLYGON((28.871162 61.921066,30.969653 61.921066,30.969653 63.042904,28.871162 63.042904,28.871162 61.921066))", Type.WGS84).getBoundingBox();
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), c));

		// This area almost touched validation area
		c = Geo.fromWKT("POLYGON((29.932434 62.354377,29.951718 62.353969,29.952599 62.362929,29.93331 62.363337,29.932434 62.354377))", Type.WGS84).getBoundingBox();
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), c));
	}

	@Test
	public void distributionMultiSameAllYears() throws Exception {
		ValidationRules rules = new ValidationRules();
		// Validation areas: Square in Kauniainen and polygon in northernmost area of Finland
		rules.rule(TAXON_ID,
				new Distribution(""+
						"MULTIPOLYGON("+
						"((27.218354 70.024098,28.31608 70.00257,28.27829 70.05911,27.912981 70.122841,27.360518 70.073835,27.218354 70.024098)),"+
						"((24.713121 60.201829,24.748525 60.201829,24.748525 60.216637,24.713121 60.216637,24.713121 60.201829)))"));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(61, 24, Type.WGS84))); // somewhere way of

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(60.211075, 24.735187, Type.WGS84))); // Kauniainen
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), new Coordinates(70.047357, 27.889903, Type.WGS84))); // Northernmost corner

		// This area is far from validation areas
		Coordinates c = Geo.fromWKT("POLYGON((29.932434 62.354377,29.951718 62.353969,29.952599 62.362929,29.93331 62.363337,29.932434 62.354377))", Type.WGS84).getBoundingBox();
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("2.1.2000"), c));
	}

	@Test
	public void distributionChangesOverYearsSingleOccurrenceDate() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID,
				Distribution.allFinland(null, 2000),
				new Distribution(2001, 2009, "POLYGON((28.871162 61.921066,30.969653 61.921066,30.969653 63.042904,28.871162 63.042904,28.871162 61.921066))"),
				// note 2010-2011 not defined: not allowed anywhere in Finland
				Distribution.allFinland(2012, 2015),
				new Distribution(2016, null, "POLYGON((28.871162 61.921066,30.969653 61.921066,30.969653 63.042904,28.871162 63.042904,28.871162 61.921066))")
				);
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		Coordinates areaMatch = new Coordinates(62.614271, 29.772966, Type.WGS84);
		Coordinates somewhereElse = new Coordinates(61, 29, Type.WGS84);

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.1980"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2000"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2000"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2001"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2001"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("31.12.2009"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("31.12.2009"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2010"), somewhereElse));
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2010"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2011"), somewhereElse));
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2011"), areaMatch));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2012"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2012"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2016"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2016"), areaMatch));

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2022"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("6.6.2022"), areaMatch));
	}

	@Test
	public void distributionChangesOverYearsDateRange() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID,
				Distribution.allFinland(1980, 2000),
				new Distribution(2001, 2009, "POLYGON((28.871162 61.921066,30.969653 61.921066,30.969653 63.042904,28.871162 63.042904,28.871162 61.921066))")
				);
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		Coordinates areaMatch = new Coordinates(62.614271, 29.772966, Type.WGS84);
		Coordinates somewhereElse = new Coordinates(61, 29, Type.WGS84);

		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("1.1.1975", "31.12.1979"), somewhereElse));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.1975", "31.12.1980"), somewhereElse));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.1985", "31.12.1986"), somewhereElse));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.1995", "31.12.2005"), areaMatch));
		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.1995", "31.12.2005"), somewhereElse));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.2001", "31.12.2005"), areaMatch));
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("1.1.2001", "31.12.2005"), somewhereElse));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(TAXON_ID, date("1.1.2009", "31.12.2020"), areaMatch));
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(TAXON_ID, date("1.1.2010", "31.12.2020"), areaMatch));
	}

	@Test
	public void period() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.6.", "31.8."));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.7.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.4.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.7.2000", "30.7.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.4.2000", "1.6.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.4.2000", "31.12.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.4.2000", "1.6.2001")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.12.2000", "1.5.2001")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.12.2000", "5.6.2001")));

		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, null, date("1.1.2000", "31.12.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, null, date("5.1.2000", "31.12.2005")));
	}

	@Test
	public void periodValidationTimeCrossingYears() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.12.", "31.1."));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.7.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.7.2000", "31.7.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("28.12.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("5.1.2001")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("5.1.2000", "30.4.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, null, date("10.6.2000", "5.12.2020")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, null, date("10.6.2000", "5.1.2021")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, null, date("10.6.2000", "12.12.2020")));
	}

	@Test
	public void periodValidationLifeStages() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.6.", "31.6."));
		rules.rule(TAXON_ID, LifeStage.LARVA, new Period("1.5.", "31.5."));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.1.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.1.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.1.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.1.2000")));

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.5.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.5.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.5.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.5.2000")));

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.6.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.6.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.6.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.6.2000")));

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.12.2000")));
		assertEquals(Pass.NOT_VALIDATED, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.12.2000")));
	}

	@Test
	public void periodValidationLifeStages_has_rule_for_unknown_lifestage() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, LifeStage.ADULT, new Period("1.6.", "31.6."));
		rules.rule(TAXON_ID, LifeStage.LARVA, new Period("1.5.", "31.5."));
		rules.rule(TAXON_ID, null, new Period("1.1.", "31.6."));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.1.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.1.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.1.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.1.2000")));

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.5.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.5.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.5.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.5.2000")));

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.6.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.6.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.6.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.6.2000")));

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.LARVA, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.12.2000")));
	}

	@Test
	public void periodValidationLifeStages_has_rule_only_for_unknown_lifestage() throws Exception {
		ValidationRules rules = new ValidationRules();
		rules.rule(TAXON_ID, null, new Period("1.1.", "31.6."));
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(rules);

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, null, date("1.1.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.1.2000")));
		assertEquals(Pass.TRUE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.1.2000")));

		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, null, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.ADULT, date("1.12.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(TAXON_ID, LifeStage.EGG, date("1.12.2000")));
	}

	@Test
	public void hardcoded_rules() throws Exception {
		AutomatedTaxonValidator validator = new AutomatedTaxonValidator(AutomatedTaxonValidatorHardCodedRules.RULES);

		assertEquals(Pass.TRUE, validator.passesPeriodCheck(new Qname("MX.61819"), null, date("5.7.2000")));
		assertEquals(Pass.FALSE, validator.passesPeriodCheck(new Qname("MX.61819"), null, date("5.4.2000")));

		assertEquals(Pass.TRUE, validator.passesDistributionCheck(new Qname("MX.61819"), date("5.5.1980"), new Coordinates(62.18, 26.60, Type.WGS84)));
		assertEquals(Pass.FALSE, validator.passesDistributionCheck(new Qname("MX.61819"), date("5.5.2015"), new Coordinates(62.18, 26.60, Type.WGS84)));
	}

}
