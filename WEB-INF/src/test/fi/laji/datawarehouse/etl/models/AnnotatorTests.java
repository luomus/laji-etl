package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Annotator.PersonIdResolver;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification.NotificationReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;

public class AnnotatorTests {
	private PersonIdResolver personIdResolver;
	private Annotator annotator;

	@Before
	public void before() {
		personIdResolver = new PersonIdResolver() {
			@Override
			public Qname resolve(String userId) {
				if (userId == null) return null;
				if (userId.startsWith("http://")) return Qname.fromURI(userId);
				if (userId.equals("hatikka:testi@hatikka.fi")) return new Qname("MA.123");
				return null;
			}
		};
		annotator = new Annotator(personIdResolver);
	}

	@Test
	public void test() throws CriticalParseFailure, DataValidationException {
		Annotation a = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		a.setAnnotationByPerson(new Qname("MA.2"));

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.addAnnotation(a);

		root.setCollectionId(new Qname("HR.1"));
		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		g.addUnit(u);
		g.addObserverUserId("MA.5");
		g.addObserverUserId("MA.2"); // same as person making annotation, should not receive notification
		Document privateDoc = publicDoc.copy(Concealment.PRIVATE);
		privateDoc.addEditorUserId("MA.1");
		privateDoc.addEditorUserId("hatikka:testi@hatikka.fi"); // resolves to MA.123
		privateDoc.addEditorUserId("blaa");
		root.setPrivateDocument(privateDoc);

		List<AnnotationNotification> notifications = annotator.annotate(root);

		assertEquals(1, root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());
		assertEquals(1, root.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());

		assertEquals(3, notifications.size());
		Set<Qname> notifiedPersons = new TreeSet<>();
		for (AnnotationNotification notification : notifications) {
			notifiedPersons.add(notification.getPersonId());
		}
		assertEquals("[MA.1, MA.5, MA.123]", notifiedPersons.toString());

		Annotation annotation = root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().get(0);
		assertEquals("MAN.1", annotation.getId().toString());
		assertEquals(null, annotation.getRootID()); // Do not show original annotation root and target because it might be a source of a split document:
		assertEquals(null, annotation.getTargetID()); // we do not want to reveal connection between a split document and the original document

		assertEquals("MAN.1", notifications.get(0).getAnnotation().getId().toString());
		assertEquals("JA.123", notifications.get(0).getAnnotation().getRootID().toString());
		assertEquals("uid", notifications.get(0).getAnnotation().getTargetID().toString());
	}

	@Test
	public void test_adminAnnotation_public_and_private() throws CriticalParseFailure, DataValidationException {
		Annotation a = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		a.addTag(Tag.FORMADMIN_UNCERTAIN);

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.addAnnotation(a);

		root.setCollectionId(new Qname("HR.1"));
		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		g.addUnit(u);

		Document privateDoc = publicDoc.copy(Concealment.PRIVATE);
		root.setPrivateDocument(privateDoc);

		List<AnnotationNotification> notifications = annotator.annotate(root);

		assertEquals(0, root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());
		assertEquals(1, root.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());

		assertEquals(0, notifications.size());
	}

	@Test
	public void test_adminAnnotation_public_without_private() throws CriticalParseFailure, DataValidationException {
		Annotation a = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		a.addTag(Tag.FORMADMIN_UNCERTAIN);

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));
		root.addAnnotation(a);

		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		g.addUnit(u);

		assertNull(root.getPrivateDocument());

		List<AnnotationNotification> notifications = annotator.annotate(root);

		assertNotNull(root.getPrivateDocument());

		assertEquals(0, root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());
		assertEquals(1, root.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());

		assertEquals(0, notifications.size());
	}

	@Test
	public void test_documentLevelAnnotation() throws Exception {
		Annotation adminAnnotation = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("JA.123"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		adminAnnotation.addTag(Tag.FORMADMIN_CENSUS_INNER_COUNT_ERROR);
		Annotation publicAnnotation = new Annotation(new Qname("MAN.2"), new Qname("JA.123"), new Qname("JA.123"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.setCollectionId(new Qname("HR.1"));
		root.addAnnotation(adminAnnotation);
		root.addAnnotation(publicAnnotation);

		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		publicDoc.addEditorUserId("MA.1");
		g.addUnit(u);
		g.addObserverUserId("MA.2");

		assertNull(root.getPrivateDocument());

		List<AnnotationNotification> notifications = annotator.annotate(root);

		assertNotNull(root.getPrivateDocument());

		assertEquals(0, root.getPublicDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());
		assertEquals(0, root.getPrivateDocument().getGatherings().get(0).getUnits().get(0).getAnnotations().size());

		assertEquals(2, notifications.size());

		assertEquals(2, root.getPrivateDocument().getAnnotations().size());
		assertEquals(1, root.getPublicDocument().getAnnotations().size());

		assertEquals("MAN.1", root.getPrivateDocument().getAnnotations().get(0).getId().toString());
		assertEquals("MAN.2", root.getPrivateDocument().getAnnotations().get(1).getId().toString());
		assertEquals("MAN.2", root.getPublicDocument().getAnnotations().get(0).getId().toString());

		assertEquals(null, root.getPublicDocument().getAnnotations().get(0).getRootID());
	}

	@Test
	public void notificationToOtherAnnotators() throws Exception {
		Annotation a1 = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2016-07-26T18:51:53+00:00"));
		a1.setAnnotationByPerson(new Qname("MA.1"));

		Annotation a2 = new Annotation(new Qname("MAN.2"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		a2.setAnnotationByPerson(new Qname("MA.2"));

		Annotation a3 = new Annotation(new Qname("MAN.3"), new Qname("JA.123"), new Qname("uid"), Util.toTimestamp("2018-07-26T18:51:53+00:00"));
		a3.setAnnotationByPerson(new Qname("MA.3"));

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.addAnnotation(a1);
		root.addAnnotation(a2);
		root.addAnnotation(a3);

		root.setCollectionId(new Qname("HR.1"));
		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		g.addUnit(u);

		List<AnnotationNotification> notifications = annotator.annotate(root);
		// MA.1 is notified about annotations by MA.2 and MA.3
		// MA.2 is notified about annotation by MA.3
		// MA.3 is not notified
		assertEquals(3, notifications.size());

		Map<Qname, List<Qname>> notifMap = new HashMap<>();
		for (AnnotationNotification n : notifications) {
			assertEquals(NotificationReason.ANNOTATED_DOCUMENT_ANNOTATED, n.getNotificationReason());
			if (!notifMap.containsKey(n.getPersonId())) {
				notifMap.put(n.getPersonId(), new ArrayList<Qname>());
			}
			notifMap.get(n.getPersonId()).add(n.getAnnotation().getAnnotationByPerson());
		}

		assertEquals(2, notifMap.keySet().size());
		assertEquals(2, notifMap.get(new Qname("MA.1")).size());
		assertEquals(1, notifMap.get(new Qname("MA.2")).size());
		assertTrue(notifMap.get(new Qname("MA.1")).contains(new Qname("MA.2")));
		assertTrue(notifMap.get(new Qname("MA.1")).contains(new Qname("MA.3")));
		assertTrue(notifMap.get(new Qname("MA.2")).contains(new Qname("MA.3")));
	}

	@Test
	public void hard_deleted_annotation() throws Exception {
		Annotation a = Annotation.deletedAnnotation(new Qname("MAN.1"), new Qname("JA.123"));

		DwRoot root = new DwRoot(new Qname("JA.123"), new Qname("KE.1"));
		root.addAnnotation(a);

		root.setCollectionId(new Qname("HR.1"));
		Document publicDoc = root.createPublicDocument();
		Gathering g = new Gathering(new Qname("gid"));
		Unit u = new Unit(new Qname("uid"));
		publicDoc.addGathering(g);
		g.addUnit(u);

		List<AnnotationNotification> notifications = annotator.annotate(root);
		assertEquals(0, notifications.size());

		assertEquals(0, publicDoc.getAnnotations().size());
		assertEquals(0, publicDoc.getGatherings().get(0).getUnits().get(0).getAnnotations().size());
	}

}