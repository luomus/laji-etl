package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.harmonizers.MunicipalityGISHarmonizer.GISLayerData;
import fi.laji.datawarehouse.etl.threads.custom.HelsinkiPullReader;
import fi.laji.datawarehouse.etl.threads.custom.TamperePullReader;
import fi.laji.datawarehouse.etl.threads.custom.TamperePullTests;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.FileUtils;

public class MunicipalityGISHarmonizerTests {

	private MunicipalityGISHarmonizer harmonizer;

	@Before
	public void setUp() {
		harmonizer = new MunicipalityGISHarmonizer();
	}

	@Test
	public void helsinki_vieras() throws Exception {
		GISLayerData data = new GISLayerData(HelsinkiPullReader.HELSINKI_INVASIVE_COLL_ID, null, getTestData("helsinki_vieras.json", 0));
		List<DwRoot> roots = harmonizer.harmonize(data, HelsinkiPullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		assertEquals(getExpected("expected_helsinki_vieras.json"), roots.get(0).clearProcesstime().toJSON().beautify());
	}

	@Test
	public void tampere_vieras() throws Exception {
		GISLayerData data = new GISLayerData(TamperePullReader.TAMPERE_INVASIVE_COLL_ID, null, getTestData("tampere_vieras.json", 0));
		List<DwRoot> roots = harmonizer.harmonize(data, TamperePullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		assertEquals(getExpected("expected_tampere_vieras.json"), roots.get(0).clearProcesstime().toJSON().beautify());
	}

	@Test
	public void tampere_laji_piste() throws Exception {
		GISLayerData data = new GISLayerData(TamperePullReader.TAMPERE_SPECIES_COLL_ID, "P.", getTestData("tampere_laji_piste.json", 0));
		List<DwRoot> roots = harmonizer.harmonize(data, TamperePullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		assertEquals(getExpected("expected_tampere_laji_piste.json"), roots.get(0).clearProcesstime().toJSON().beautify());
	}

	@Test
	public void tampere_sammal() throws Exception {
		GISLayerData data = new GISLayerData(TamperePullReader.TAMPERE_MOSS_COLL_ID, null, getTestData("tampere_sammal.json", 0));
		List<DwRoot> roots = harmonizer.harmonize(data, TamperePullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		assertEquals(getExpected("expected_tampere_sammal.json"), roots.get(0).clearProcesstime().toJSON().beautify());
	}

	private String getExpected(String filename) {
		URL url = MunicipalityGISHarmonizer.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private JSONObject getTestData(String filename, int index) throws Exception {
		JSONObject json = new JSONObject(TamperePullTests.getTestData(filename));
		return json.getArray("features").iterateAsObject().get(index);
	}

}
