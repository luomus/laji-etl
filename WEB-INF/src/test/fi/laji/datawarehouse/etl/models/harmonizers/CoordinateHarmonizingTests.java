package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;

public class CoordinateHarmonizingTests {

	@Test
	public void parseYkj() throws DataValidationException {
		// Point
		assertEquals("673.0 : 674.0 : 334.0 : 335.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673:334").toString());
		assertEquals("6690754.0 : 6690755.0 : 3393963.0 : 3393964.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("6690754:3393963/").toString());

		// Slide
		assertEquals("673.0 : 675.0 : 334.0 : 339.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673-674:334-338").toString());
		assertEquals("673.0 : 680.0 : 334.0 : 340.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673-679:334-339").toString());
		assertEquals("673.0 : 675.0 : 334.0 : 335.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673-674:334").toString());
		assertEquals("673.0 : 675.0 : 334.0 : 339.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673-4:334-8").toString());
		assertEquals("673000.0 : 673001.0 : 336746.0 : 336766.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("673000:336746-336765").toString());
		assertEquals("6973.0 : 6986.0 : 3380.0 : 3382.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("6973-6985:3380-81").toString());
		assertEquals("7000.0 : 7001.0 : 3311.0 : 3320.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("7000:3311-9").toString());
		assertEquals("7000.0 : 7001.0 : 3311.0 : 3342.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("7000:3311-41").toString());
		assertEquals("7000.0 : 7001.0 : 3311.0 : 3350.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("7000:3311-49").toString());
		assertEquals("7000.0 : 7001.0 : 3311.0 : 3400.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("7000:3311-99").toString());

		try {
			Coordinates c = BaseHarmonizer.parseYKJCoordinates("676746:336746-336744");
			Assert.fail("Should throw exception, returned " + c);
		} catch (DataValidationException e) {
			assertEquals("Lon max is smaller than min: 676746:336746-336744", e.getMessage());
		}

		try {
			Coordinates c = BaseHarmonizer.parseYKJCoordinates("676746:336746-336745");
			Assert.fail("Should throw exception, returned " + c);
		} catch (DataValidationException e) {
			assertEquals("Lon max is smaller than min: 676746:336746-336745", e.getMessage());
		}

		try {
			BaseHarmonizer.parseYKJCoordinates("673-685:33-3881").toString();
			Assert.fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid slide: 33-3881", e.getMessage());
		}

		try {
			BaseHarmonizer.parseYKJCoordinates("6793063-3451618");
			Assert.fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid YKJ grid 6793063-3451618", e.getMessage());
		}

		assertEquals("7363916.0 : 7363917.0 : 3610622.0 : 3610623.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("7363916:3610622/").toString());

		// Many
		try {
			BaseHarmonizer.parseYKJCoordinates("/667:333/668:335/");
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("When reporting YKJ Grid (separated by /), length of coordinates must be 7: 667", e.getMessage());
		}

		assertEquals("6670000.0 : 6680000.0 : 3330000.0 : 3350000.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("/6670000:3330000/6680000:3350000/").toString());
		assertEquals("6666667.0 : 6667001.0 : 3332001.0 : 3333333.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("/6666667:3333333/6667001:3332001/").toString());

		assertEquals("6765472.0 : 6765472.0 : 3565316.0 : 3565320.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("6765472:3565316/6765472:3565320/").toString());

		// Adds missing front 3
		assertEquals("6666.0 : 6667.0 : 3444.0 : 3445.0 : YKJ : null", BaseHarmonizer.parseYKJCoordinates("6666:444").toString());

		// Does not add front 3 if already starts with 3
		try {
			BaseHarmonizer.parseYKJCoordinates("6666:333");
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid YKJ coordinates (YKJ coordinates must be reported using same precision): 6666 : 6667 : 333 : 334 : YKJ", e.getMessage());
		}
	}

}
