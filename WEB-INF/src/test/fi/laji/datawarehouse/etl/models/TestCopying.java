package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Interpreter.GeoSource;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringInterpretations;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;

public class TestCopying {

	@Test
	public void test() throws Exception {
		DwRoot root = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		root.setCollectionId(new Qname("gbif-dataset:AAAA"));
		Document document = root.createPublicDocument();
		document.addEditorUserId("e1");
		document.addEditorUserId("e2");
		document.addFact("f1", "v1");
		Gathering g1 = new Gathering(new Qname("g1id"));
		Gathering g2 = new Gathering(new Qname("g2id"));
		document.addGathering(g1);
		document.addGathering(g2);
		g1.addTeamMember("g1a1");
		g1.addObserverUserId("MA.5");
		g1.setCoordinates(new Coordinates(60.0, 30.0, Type.WGS84));
		g1.setBiogeographicalProvince("bioprov");
		g1.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2013"), DateUtils.convertToDate("15.1.2013")));
		g1.addFact("f1", "v1");
		GatheringInterpretations interpretations = new GatheringInterpretations();
		interpretations.setCoordinates(new Coordinates(g1.getCoordinates()));
		interpretations.getCoordinates().setAccuracyInMeters(100);
		interpretations.setSourceOfCoordinates(GeoSource.REPORTED_VALUE);
		interpretations.setCountry(Const.FINLAND);
		interpretations.setSourceOfCountry(GeoSource.COORDINATES);
		g1.setInterpretations(interpretations);
		g2.setLocality("locality value");
		Unit u1 = new Unit(new Qname("u1id"));
		u1.addFact("f1", "v1");
		u1.setAbundanceString("1");
		u1.setLifeStage(LifeStage.ADULT);
		u1.setNotes("u notes");
		u1.setTypeSpecimen(true);
		g2.addUnit(u1);
		assertEquals(Concealment.PUBLIC, document.getConcealment());

		Document copy = document.copy(Concealment.PRIVATE);
		assertEquals(Concealment.PRIVATE, copy.getConcealment());
		assertEquals("https://www.gbif.org/dataset/AAAA", copy.getCollectionId().toURI());
		assertEquals(new Qname("12345"), copy.getDocumentId());
		assertEquals("http://tun.fi/KE.3", copy.getSourceId().toURI());
		assertEquals("[e1, e2]", copy.getEditorUserIds().toString());
		assertEquals("f1", document.getFacts().get(0).getFact());
		assertEquals("v1", document.getFacts().get(0).getValue());
		assertEquals(2, copy.getGatherings().size());
		assertEquals("http://tun.fi/g1id", copy.getGatherings().get(0).getGatheringId().toURI());
		assertEquals("http://tun.fi/g2id", copy.getGatherings().get(1).getGatheringId().toURI());
		assertEquals("[g1a1]", copy.getGatherings().get(0).getTeam().toString());
		assertEquals("[http://tun.fi/MA.5]", copy.getGatherings().get(0).getObserverUserIds().toString());
		assertEquals(Type.WGS84, copy.getGatherings().get(0).getCoordinates().getType());
		assertEquals(60.0, copy.getGatherings().get(0).getCoordinates().getLatMin(), 0);
		assertEquals("bioprov", copy.getGatherings().get(0).getBiogeographicalProvince());
		assertEquals("15.01.2013", DateUtils.format(copy.getGatherings().get(0).getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals("f1", copy.getGatherings().get(0).getFacts().get(0).getFact());
		assertEquals("v1", copy.getGatherings().get(0).getFacts().get(0).getValue());
		assertEquals(60.0, copy.getGatherings().get(0).getInterpretations().getCoordinates().getLatMin(), 0);
		assertEquals(100, copy.getGatherings().get(0).getInterpretations().getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(GeoSource.REPORTED_VALUE, copy.getGatherings().get(0).getInterpretations().getSourceOfCoordinates());
		assertEquals(Const.FINLAND.toURI(), copy.getGatherings().get(0).getInterpretations().getCountry().toURI());
		assertEquals(GeoSource.COORDINATES, copy.getGatherings().get(0).getInterpretations().getSourceOfCountry());

		assertEquals("locality value", copy.getGatherings().get(1).getLocality());
		assertEquals(0, copy.getGatherings().get(1).getFacts().size());
		assertEquals(0, copy.getGatherings().get(1).getTeam().size());
		assertEquals(null, copy.getGatherings().get(1).getBiogeographicalProvince());

		Unit u1Copy = copy.getGatherings().get(1).getUnits().get(0);
		assertEquals("http://tun.fi/u1id", u1Copy.getUnitId().toURI());
		assertEquals("v1", u1Copy.getFacts().get(0).getValue());
		assertEquals("1", u1Copy.getAbundanceString());
		assertEquals(LifeStage.ADULT, u1Copy.getLifeStage());
		assertEquals(null, u1Copy.getSex());
		assertEquals("u notes", u1Copy.getNotes());
		assertEquals(true, u1.isTypeSpecimen());

		assertNull(root.getPrivateDocument());

		// Test two copies are different objects
		document.setNotes("blaablaa");
		assertEquals("blaablaa", document.getNotes());
		assertEquals(null, copy.getNotes());

		copy.setNotes("foofuu");
		assertEquals("blaablaa", document.getNotes());
		assertEquals("foofuu", copy.getNotes());
	}

}
