package fi.laji.datawarehouse.etl.models;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.ApiUser;
import fi.laji.datawarehouse.etl.models.containers.LogEntry;
import fi.laji.datawarehouse.etl.models.containers.PersonInfo;
import fi.laji.datawarehouse.etl.models.containers.Source;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedTargetNameData;
import fi.laji.datawarehouse.etl.models.containers.UnlinkedUserIdsData;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;

public class DAOStub implements DAO {

	@Override
	public Map<String, Source> getSources() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Area> resolveMunicipalitiesByName(String reportedMunicipality) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Area> resolveOldMunicipalitiesByName(String reportedMunicipality) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Area> resolveBiogeographicalProvincesByName(String reportedProvince) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Area> resolveCountiesByName(String reportedCountry) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getFinnishMunicipalities(Coordinates wgs84Coordinates) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getFinnishMunicipalities(Geo wgs84Geo) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getNaturaAreas(Coordinates wgs84Coordinates) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getBiogeographicalProvinces(Coordinates wgs84Coordinates) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getBiogeographicalProvinces(Geo wgs84Geo) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getNaturaAreas(Geo wgs84geo) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Coordinates getCoordinatesByFinnishMunicipality(Qname finnishMunicipality) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public VerticaDAO getPrivateVerticaDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public VerticaDAO getPublicVerticaDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public VerticaDimensionsDAO getVerticaDimensionsDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ETLDAO getETLDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void logError(Qname source, Class<?> phase, String identifier, Throwable exception) {
		// Auto-generated method stub

	}

	@Override
	public void logError(Qname source, Class<?> phase, String identifier, String message, Throwable exception) {
		// Auto-generated method stub

	}

	@Override
	public void logMessage(Qname source, Class<?> phase, String message) {
		// Auto-generated method stub

	}

	@Override
	public void logMessage(Qname source, Class<?> phase, String identifier, String message) {
		// Auto-generated method stub

	}

	@Override
	public List<LogEntry> getLogEntries() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Taxon getTaxon(Qname qname) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasTaxon(Qname qname) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public List<UnlinkedTargetNameData> getUnlinkedTargetNames() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<UnlinkedUserIdsData> getUnlinkedUserIds() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void clearCachesStartReload() {
		// Auto-generated method stub

	}

	@Override
	public Map<String, CollectionMetadata> getCollections() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<Qname, Area> getAreas() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<Qname, Area> getAreasForceReload() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <T extends BaseQuery> void logQuery(T query) {
		// Auto-generated method stub

	}

	@Override
	public Map<String, String> getReferenceDescriptions() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ApiUser getApiUser(String accessToken) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TaxonomyDAO getTaxonomyDAO() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public LocalizedText getLabels(Qname propertyQname) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void storeDownloadRequest(DownloadRequest request) {
		// Auto-generated method stub

	}

	@Override
	public List<DownloadRequest> getUncompletedDownloadRequests() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public DownloadRequest getDownloadRequest(Qname id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void sendDownloadRequestForApproval(JSONObject json) {
		// Auto-generated method stub

	}

	@Override
	public void notifyApprovalServiceOfCompletion(Qname id) {
		// Auto-generated method stub

	}

	@Override
	public List<DownloadRequest> getDownloadRequestsFromToday() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<DownloadRequest> getFailedDownloadRequests() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public MediaObject getMediaObject(Qname id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<MediaObject> getMediaObjects(Qname documentId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void sendNotification(AnnotationNotification notification) {
		// Auto-generated method stub

	}

	@Override
	public Qname getSeqNextVal(String qnamePrefix) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Qname> getPersonLookupStructure() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ContextDefinitions getContextDefinitions() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlaces() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlacesForceReload() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getFormNames() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ErrorReporter getErrorReporter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Pair<Qname, Collection<URI>>> getGBIFDatasetEndpoints() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public RuntimeException exceptionAndReport(String identifier, String message, Exception e) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public boolean exceedsDownloadLimit(Qname personId) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public void clearPersonDailyLimits() {
		// Auto-generated method stub

	}

	@Override
	public PersonInfo getPerson(Qname personId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public PersonInfo getPerson(String personToken) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Collection<PersonInfo> getPersons() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Qname getPersonTargetSystem(String personToken) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void logOccurrenceCounts() {
		// Auto-generated method stub

	}

	@Override
	public void persist(String key, String data) {
		// Auto-generated method stub

	}

	@Override
	public String getPersisted(String key) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void close() {
		// Auto-generated method stub

	}

	@Override
	public TaxonLinkingService getTaxonLinkingService() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getAllTaxonMatches(String targetName) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ExecutorService getSharedThreadPool() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Taxon> getTaxaWithSecureLevels() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getMunicipalitiesPartOfELY(Qname elyId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getMunicipalitiesPartOfProvince(Qname provinceId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String generateAndStoreApiKey(DownloadRequest request) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public DownloadRequest getRequestForApiKey(String apiKey) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<DownloadRequest> getApiRequests() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<Pair<String, DownloadRequest>> getPersonsApiRequests(Qname personId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<DownloadRequest> getAuthoritiesDownloadRequests() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<DownloadRequest> getPersonsAuthoritiesDownloadRequests(Qname personId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getStatisticsAllowedCollections() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public boolean exceedsPolygonSearchLimit(Qname personId) {
		//  Auto-generated method stub
		return false;
	}

	@Override
	public long getPolygonSearchId(String wkt) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public String getPolygonSearch(long id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public long persistQueryLogToLogTable() {
		// Auto-generated method stub
		return -1;
	}

	@Override
	public boolean isValidEnumeration(Qname enumerationValue) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public Qname getBirdAssociationArea(String ykjGridString) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getInvasiveSpeciesEarlyWarningReportedUnits() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setInvasiveSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<Qname> getPestSpeciesEarlyWarningReportedUnits() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPestSpeciesEarlyWarningReportedUnits(Set<Qname> unitIds) {
		// TODO Auto-generated method stub

	}

}
