package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.GatheringQuality;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer.EventOccurrences;
import fi.laji.datawarehouse.etl.models.harmonizers.LajiGISHarmonizer.OccurrenceData;
import fi.laji.datawarehouse.etl.threads.custom.LajiGISPullReader;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class LajiGISHarmonizerTests {

	private static final Qname SOURCE = new Qname("KE.921");

	private LajiGISHarmonizer harmonizer;

	@Before
	public void setUp() {
		harmonizer = new LajiGISHarmonizer();
	}

	@Test
	public void singleOccurrenceEvent() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.projectNames = Utils.list("project1", "project2");
		data.occurrenceWKT = Utils.list(
				"POINT(607182 7361411)",
				"POLYGON((607328 7361552,607167 7361312,607696 7361103,607903 7361359,607328 7361552))",
				"POINT(607183 7361412)"
				);
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		assertNotNull(root.getPrivateDocument());
		assertNotNull(root.getPublicDocument());
		Document d = root.getPrivateDocument();
		assertEquals("KE.921/LGE.123", d.getDocumentId().toString());
		assertEquals("HR.3491", d.getCollectionId().toString());
		assertEquals("[project1, project2, 266]", d.getKeywords().toString());

		String expectedFacts = "" +
				"[Kartoituksen tarkoitus : Lajistokartoitus tai seuranta vesistössä, erittelemättä, Kartoitusmenetelmä : Drop-videokuvaus, Kartoituksen tila : Havaintotiedot tallennettu, Seurattava : ei]";
		assertEquals(expectedFacts, d.getFacts().toString());

		assertEquals("http://tun.fi/KE.921/LGNMP.789", d.getNamedPlaceId());
		assertEquals(null, d.getQuality());
		assertEquals(SecureLevel.NONE, d.getSecureLevel());
		assertEquals("["+SecureReason.CUSTOM+"]", d.getSecureReasons().toString());
		assertEquals("KE.921", d.getSourceId().toString());

		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		expectedFacts = "[PL_ARVIO_EPAVARMA : ei, Vesistöalue : Poutajoen vesistöalue]";
		assertEquals(expectedFacts, g.getFacts().toString());
		assertEquals(null, g.getCoordinates());
		assertEquals(
				"[POINT (607182 7361411), POLYGON ((607328 7361552, 607903 7361359, 607696 7361103, 607167 7361312, 607328 7361552)), POINT (607183 7361412)]",
				g.getGeo().getWKTList().toString());
		assertEquals("DateRange [begin=2016-07-26, end=2016-07-28]", g.getEventDate().toString());
		assertEquals("2016-07-26 - 2016-07-28", g.getDisplayDateTime());
		assertEquals(null, g.getQuality());
		assertEquals("Foofaakär, Poutajoen vesistöalue", g.getLocality());
		assertEquals("Kuusamo", g.getMunicipality());
		assertEquals("Koillismaa", g.getBiogeographicalProvince());
		assertEquals("KE.921/LGE.123G1", g.getGatheringId().toString());

		assertEquals(1, g.getUnits().size());
		Unit u1 = g.getUnits().get(0);

		assertEquals("MX.123", u1.getReportedTaxonId().toString());
		assertEquals("Erruburri loratus", u1.getTaxonVerbatim());
		assertEquals("88", u1.getAbundanceString());
		assertEquals("[Havainnon laatu : Näköhavainto lajista, EPIFYYTTI : ei, Havainnon määrän yksikkö : yksilö, Potentiaalinen kasvuala : 100.0, Paljas potentiaalinen kasvuala : 23.5, Eliölajit lajikoodi : 1234]", u1.getFacts().toString());
		assertEquals("KE.921/LGE.123/456", u1.getUnitId().toString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_SEEN, u1.getRecordBasis());
		assertEquals("[CHECK_TAXON]", u1.getSourceTags().toString());
		assertEquals("[456, el_1234]", u1.getKeywords().toString());
		assertEquals("MY.atlasCodeEnum71", Qname.fromURI(u1.getAtlasCode()).toString());
	}

	@Test
	public void multiOccurrenceEvent() throws Exception {
		// d1 and d2 are the same
		String d1json = "{\"eventId\":\"1\",\"occurrenceId\":\"11\",\"taxonVerbatim\":\"Achillea m1\"," +
				"\"team\":\"Peltola Allu\",\"dateBegin\":\"2015-08-22\",\"municipality\":\"Hailuoto\"}";
		String d2json = d1json;
		// d3 has different team
		String d3json = "{\"eventId\":\"1\",\"occurrenceId\":\"13\",\"taxonVerbatim\":\"Achillea m3\"," +
				"\"team\":\"Kuusi Jaakko\",\"dateBegin\":\"2015-08-22\",\"municipality\":\"Hailuoto\"}";
		// d4 has different date
		String d4json = "{\"eventId\":\"1\",\"occurrenceId\":\"14\",\"taxonVerbatim\":\"Achillea m4\"," +
				"\"team\":\"Peltola Allu\",\"dateBegin\":\"2015-08-23\",\"municipality\":\"Hailuoto\"}";
		// d5 has different facts
		String d5json = "{\"eventId\":\"1\",\"occurrenceId\":\"15\",\"taxonVerbatim\":\"Achillea m5\"," +
				"\"team\":\"Peltola Allu\",\"dateBegin\":\"2015-08-22\",\"municipality\":\"Hailuoto\", \"facts\": [{\"fact\":\"LG_KERTA_VESI.PL_ARVIO_EPAVARMA\",\"value\":\"ei\",\"columnName\":\"LG_KERTA_VESI.PL_ARVIO_EPAVARMA\"}]}";
		// d6 has different geo
		String d6json = "{\"eventId\":\"1\",\"occurrenceId\":\"16\",\"taxonVerbatim\":\"Achillea m1\"," +
				"\"team\":\"Peltola Allu\",\"dateBegin\":\"2015-08-22\",\"municipality\":\"Hailuoto\"}";

		OccurrenceData d1 = new OccurrenceData(1, 1, d1json);
		OccurrenceData d2 = new OccurrenceData(1, 2, d2json);
		OccurrenceData d3 = new OccurrenceData(1, 3, d3json);
		OccurrenceData d4 = new OccurrenceData(1, 4, d4json);
		OccurrenceData d5 = new OccurrenceData(1, 5, d5json);
		OccurrenceData d6 = new OccurrenceData(1, 6, d6json);

		List<String> geo = Utils.list("POINT(30 60)");
		d1.occurrenceWKT = geo; d2.occurrenceWKT = geo; d3.occurrenceWKT = geo; d4.occurrenceWKT = geo; d5.occurrenceWKT = geo;
		d6.occurrenceWKT = Utils.list("POINT(40 60)");

		DwRoot root = harmonizer.harmonize(new EventOccurrences(1, Utils.list(d1, d2, d3, d4, d5, d6)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		System.out.println(d.toJSON().beautify());
		assertEquals("HR.2029", d.getCollectionId().toString());
		assertEquals("KE.921/LGE.1", d.getDocumentId().toString());
		assertEquals(5, d.getGatherings().size());
		Gathering g1 = d.getGatherings().get(0);
		assertEquals(2, g1.getUnits().size());
		assertEquals("KE.921/LGE.1G1",g1.getGatheringId().toString());
		assertEquals("KE.921/LGE.1/1", g1.getUnits().get(0).getUnitId().toString());

		Gathering g4 = d.getGatherings().get(3);
		assertEquals("KE.921/LGE.1G4",g4.getGatheringId().toString());
		assertEquals("KE.921/LGE.1/5", g4.getUnits().get(0).getUnitId().toString());
	}

	@Test
	public void privateAndPublicFacts() throws CriticalParseFailure {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-2.json"));
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		assertNotNull(root.getPrivateDocument());
		assertNotNull(root.getPublicDocument());
		Document pri = root.getPrivateDocument();
		Document pub = root.getPublicDocument();
		assertEquals("[Kartoitusmenetelmä : Linjasukellus, Kartoituksen tila : Havaintotiedot tallennettu, Arviointiruudun syvyys (m) : 123.51, Kohteen taso : Eliölajit havaintopaikka, " +
				"Lajinseurantakohteen tila : Olemassa (+), Aineistolähde : LajiGIS, Tietolähteen kuvaus : Maastomuistiinpanot, Kartoituksen tarkoitus : Lajin kartoitus tai seuranta, Seurattava : ei]",
				pri.getFacts().toString());
		assertEquals("[Kartoitusmenetelmä : Linjasukellus, Kartoituksen tila : Havaintotiedot tallennettu, Kohteen taso : Eliölajit havaintopaikka, " +
				"Lajinseurantakohteen tila : Olemassa (+), Kartoituksen tarkoitus : Lajin kartoitus tai seuranta, Seurattava : ei]",
				pub.getFacts().toString());
		assertEquals("[Merialueen tunniste : 188898, Peittävyysarviointi : Kokonaispeittävyys, Pohjanlaatuarvio epävarma : ei, Pohjanlaatu: Muta <0,002 mm (%) : 100.0]",
				pri.getGatherings().get(0).getFacts().toString());
		assertEquals("[Merialueen tunniste : 188898]",
				pub.getGatherings().get(0).getFacts().toString());
		assertEquals("[Havainnon laatu : Näköhavainto lajista, Peittävyysprosentti : 50.0, Peittävyysprosentti : 50.0, Laji kasvaa epifyyttinä : ei, Peittävyysprosentti : 50.0]",
				pri.getGatherings().get(0).getUnits().get(0).getFacts().toString());
		assertEquals("[Havainnon laatu : Näköhavainto lajista, Peittävyysprosentti : 50.0, Peittävyysprosentti : 50.0, Laji kasvaa epifyyttinä : ei, Peittävyysprosentti : 50.0]",
				pub.getGatherings().get(0).getUnits().get(0).getFacts().toString());

		assertEquals(true, pri.getGatherings().get(0).getStateLand());
		assertEquals("LajiGIS: Maastomuistiinpanot", pri.getDataSource());
		assertEquals("Lajin kartoitus tai seuranta: Eliölajit havaintopaikka", pri.getSiteType());
		assertEquals("Olemassa (+)", pri.getSiteStatus());

		assertEquals(true, pub.getGatherings().get(0).getStateLand());
		assertEquals(null, pub.getDataSource());
		assertEquals("Lajin kartoitus tai seuranta: Eliölajit havaintopaikka", pub.getSiteType());
		assertEquals("Olemassa (+)", pub.getSiteStatus());
	}

	@Test
	public void abundanceString() throws CriticalParseFailure {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));

		// amount = 88
		// amountUnit = 28 <- yksilö
		checkAbundance(data, "88");

		// overide amount with 0
		data.json.setString("amount", "0");
		checkAbundance(data, "0");

		data.json.setString("amount", "0.0");
		checkAbundance(data, "0");

		data.json.setString("amount", "0.00");
		checkAbundance(data, "0");

		data.json.setString("amount", "1.0");
		checkAbundance(data, "1");

		data.json.setString("amount", "1.1");
		checkAbundance(data, "1.1");

		data.json.setString("amount", "1.123");
		checkAbundance(data, "1.123");

		data.json.setString("amount", "0.001");
		checkAbundance(data, "0.001");

		// clear amountUnit
		data.json.setString("amountUnit", "");

		data.json.setString("amount", "0");
		checkAbundance(data, "0");

		data.json.setString("amount", "0.0");
		checkAbundance(data, "0");

		data.json.setString("amount", "0.00");
		checkAbundance(data, "0");

		data.json.setString("amount", "1.0");
		checkAbundance(data, null);

		data.json.setString("amount", "0.001");
		checkAbundance(data, null);

		data.json.setString("amount", "88");
		checkAbundance(data, null);
	}

	private void checkAbundance(OccurrenceData data, String expectedAbundanceString) throws CriticalParseFailure {
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		Gathering g = d.getGatherings().get(0);
		Unit u1 = g.getUnits().get(0);
		assertEquals(expectedAbundanceString, u1.getAbundanceString());
	}

	@Test
	public void factValueCleaning() {
		assertEquals("20", LajiGISPullReader.clean("20"));
		assertEquals("50.0", LajiGISPullReader.clean("50.0000000000"));
		assertEquals("123.123", LajiGISPullReader.clean("123.123000000"));
		assertEquals("a", LajiGISPullReader.clean("a"));
		assertEquals(null, LajiGISPullReader.clean(null));
		assertEquals("", LajiGISPullReader.clean(""));
		assertEquals("asema 1.2345000", LajiGISPullReader.clean("asema 1.2345000"));
	}

	@Test
	@Ignore
	public void bigdata() throws CriticalParseFailure {
		List<OccurrenceData> data = readBigData();
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		assertEquals(1446, Util.countOfUnits(root.getPublicDocument()));
		assertEquals(1446, Util.countOfUnits(root.getPrivateDocument()));
		assertEquals(46, root.getPublicDocument().getGatherings().size());
		assertEquals(46, root.getPrivateDocument().getGatherings().size());
	}

	private List<OccurrenceData> readBigData() {
		List<OccurrenceData> data = new ArrayList<>();
		Map<Integer, List<String>> geos = readBigGeos();
		String lines = RdfXmlHarmonizerTests.getTestData("lajigis-bigdata.txt");
		for (String line : lines.split(Pattern.quote("\n"))) {
			String[] parts = line.split(Pattern.quote("\t"));
			OccurrenceData d = new OccurrenceData(Integer.valueOf(parts[0]), Integer.valueOf(parts[1]), parts[2]);
			d.occurrenceWKT = geos.get(d.occurrenceId);
			data.add(d);
		}
		return data;
	}

	private Map<Integer, List<String>> readBigGeos() {
		Map<Integer, List<String>> map = new HashMap<>();
		String data = RdfXmlHarmonizerTests.getTestData("lajigis-bigdata-geos.txt");
		for (String line : data.split(Pattern.quote("\n"))) {
			String[] parts = line.split(Pattern.quote("\t"));
			int occurenceId = Integer.valueOf(parts[0]);
			String wkt = parts[1];
			if (!map.containsKey(occurenceId)) map.put(occurenceId, new ArrayList<>());
			map.get(occurenceId).add(wkt);
		}
		return map;
	}

	@Test
	public void gatheringEqualsComparesAllFieldsButNotSomeOfTheNotWanted() throws Exception {
		Gathering g = new Gathering(new Qname(randString()));
		setAllValues(g);

		Gathering copy = g.copy();
		assertTrue(g.equals(copy));

		// change values one by one and make sure equals starts to fail
		List<Method> setters = new ArrayList<>(ReflectionUtil.getSetters(Gathering.class));
		for (Method setter : setters) {
			String name = ReflectionUtil.cleanSetterFieldName(setter);
			if (GATHERING_IGNORE.contains(name)) continue;

			// only geo or coordinates is set (randomly)
			if ("geo".equals(name)) {
				if (g.getCoordinates() != null)
					continue;
			}
			if ("coordinates".equals(name)) {
				if (g.getGeo() != null)
					continue;
			}

			Object originalValue = ReflectionUtil.getGetter(ReflectionUtil.cleanSetterFieldName(setter), Gathering.class).invoke(g);
			copy = g.copy();
			setValue(copy, setter, originalValue);

			assertFalse(name + " is not part of equals - it prob should be?", g.equals(copy));
		}
	}

	private static final Set<String> GATHERING_IGNORE = Utils.set("conversions", "displayDateTime", "gatheringId", "gatheringOrder", "interpretations", "linkings", "mediaCount", "unitOrderSeq", "units");

	private void setAllValues(Gathering g) throws Exception {
		List<Method> setters = new ArrayList<>(ReflectionUtil.getSetters(Gathering.class));
		Collections.shuffle(setters);
		for (Method setter : setters) {
			setValue(g, setter, null);
		}
	}

	private void setValue(Gathering g, Method setter, Object originalValue) throws Exception {
		String name = ReflectionUtil.cleanSetterFieldName(setter);
		if (GATHERING_IGNORE.contains(name))
			return;

		// only geo or coordinates is set (randomly)
		if ("geo".equals(name)) {
			if (g.getCoordinates() != null)
				return;
		}
		if ("coordinates".equals(name)) {
			if (g.getGeo() != null)
				return;
		}

		if (setter.getParameterCount() != 1) throw new IllegalStateException();
		Class<?> paramClass = setter.getParameterTypes()[0];
		if (paramClass == String.class) {
			setter.invoke(g, randString());
			return;
		}
		if (paramClass == Coordinates.class) {
			Coordinates c = new Coordinates(randDouble(), randDouble(), Type.WGS84);
			setter.invoke(g, c);
			return;
		}
		if (paramClass == DateRange.class) {
			DateRange d = new DateRange(randDate(), randDate());
			setter.invoke(g, d);
			return;
		}
		if (List.class.isAssignableFrom(paramClass)) {
			if (name.equals("facts")) {
				List<Fact> l = new ArrayList<>();
				for (int i = 0; i<Utils.randomNumber(1, 3); i++) {
					l.add(randFact());
				}
				setter.invoke(g, l);
				return;
			}
			if (name.equals("media")) {
				List<MediaObject> l = new ArrayList<>();
				for (int i = 0; i<Utils.randomNumber(1, 3); i++) {
					l.add(randMedia());
				}
				setter.invoke(g, l);
				return;
			}
			if (name.equals("taxonCensus")) {
				List<TaxonCensus> l = new ArrayList<>();
				l.add(new TaxonCensus(new Qname(randString()), new Qname(randString())));
				setter.invoke(g, l);
				return;
			}
			List<String> l = new ArrayList<>();
			for (int i = 0; i<Utils.randomNumber(1, 3); i++) {
				l.add(randString());
			}
			setter.invoke(g, l);
			return;
		}
		if (paramClass == Integer.class) {
			if (name.toLowerCase().contains("minute")) {
				int newNumber = Utils.randomNumber(0, 59);
				if (originalValue != null && newNumber == (int) originalValue) {
					newNumber = (int) originalValue == 0 ? 1 : 0;
				}
				setter.invoke(g, newNumber);
				return;
			}
			if (name.toLowerCase().contains("hour")) {
				int newNumber = Utils.randomNumber(0, 23);
				if (originalValue != null && newNumber == (int) originalValue) {
					newNumber = (int) originalValue == 0 ? 1 : 0;
				}
				setter.invoke(g, newNumber);
				return;
			}
			setter.invoke(g, randInt());
			return;
		}
		if (paramClass == Boolean.class) {
			setter.invoke(g, opposite(originalValue));
			return;
		}
		if (paramClass == Geo.class) {
			Geo geo = new Geo(Type.WGS84);
			for (int i = 0; i<Utils.randomNumber(1, 3); i++) {
				List<Point> coords = new ArrayList<>();
				for (int k = 0; k<Utils.randomNumber(3, 5); k++) {
					coords.add(Point.from(randDouble(), randDouble()));
				}
				geo.addFeature(Line.from(coords));
			}
			setter.invoke(g, geo);
			return;
		}
		if (paramClass == GatheringQuality.class) {
			GatheringQuality q = new GatheringQuality();
			q.setIssue(randIssue());
			q.setLocationIssue(randIssue());
			q.setTimeIssue(randIssue());
			setter.invoke(g, q);
			return;
		}
		throw new IllegalStateException("Not yet implemented for " + paramClass);
	}

	private Object opposite(Object originalValue) {
		if (originalValue == null) return true;
		if (Boolean.TRUE.equals(originalValue)) return false;
		return true;
	}

	private MediaObject randMedia() throws DataValidationException {
		return new MediaObject(MediaType.AUDIO, "https://"+randString());
	}

	private Quality randIssue() {
		return new Quality(Issue.COORDINATES_COUNTRY_MISMATCH, Source.AUTOMATED_FINBIF_VALIDATION, randString());
	}

	private Object randInt() {
		return Utils.randomNumber(0, Integer.MAX_VALUE);
	}

	private Fact randFact() {
		return new Fact(randString(), randString());
	}

	private Date randDate() {
		DateValue d = new DateValue(Utils.randomNumber(1, 25), Utils.randomNumber(1, 12), Utils.randomNumber(1900, DateUtils.getCurrentYear()));
		return DateUtils.convertToDate(d);
	}

	private String randString() {
		return Utils.generateGUID();
	}

	private double randDouble() {
		return Math.random();
	}

	@Test
	public void complexPolygon() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.occurrenceWKT = Utils.list(
				"POINT(607182 7361411)",
				"POLYGON((607328 7361552,607167 7361312,607696 7361103,607903 7361359,607328 7361552))",
				"POLYGON ((507633.23071289063 6682703.8850708008, 507656.38189697266 6682715.1298828125, 507660.35052490234 6682734.9736938477, 507672.25689697266 6682741.5883178711, 507674.90270996094 6682752.1716918945, 507686.14752197266 6682760.1091308594, 507700.0380859375 6682785.2446899414, 507711.28289794922 6682788.5518798828, 507719.88189697266 6682795.828125, 507737.74127197266 6682805.74987793, 507758.24652099609 6682811.703125, 507769.49151611328 6682816.3333129883, 507773.46008300781 6682836.8385009766, 507760.23089599609 6682849.4063110352, 507742.37152099609 6682869.9114990234, 507743.69451904297 6682889.09387207, 507733.11108398438 6682892.4011230469, 507719.22052001953 6682895.046875, 507712.60589599609 6682912.9063110352, 507698.71527099609 6682916.87512207, 507695.40789794922 6682937.3803100586, 507684.82452392578 6682949.2866821289, 507674.24127197266 6682953.9168701172, 507670.93389892578 6682957.8856811523, 507670.93389892578 6682968.46887207, 507666.96508789063 6682977.7294921875, 507670.27252197266 6682987.6513061523, 507678.20989990234 6683002.8649291992, 507683.50170898438 6683023.3701171875, 507689.45471191406 6683040.5681152344, 507687.47052001953 6683051.8129272461, 507672.91827392578 6683072.979675293, 507662.99652099609 6683084.885925293, 507653.07452392578 6683108.6984863281, 507644.47552490234 6683121.2661132813, 507634.5537109375 6683135.1569213867, 507634.5537109375 6683143.7556762695, 507649.10571289063 6683166.2454833984, 507649.76727294922 6683180.7974853516, 507645.13690185547 6683196.6724853516, 507633.89208984375 6683211.22467041, 507628.60052490234 6683235.0372924805, 507620.66290283203 6683250.9122924805, 507627.27752685547 6683267.4487304688, 507620.66290283203 6683284.6467285156, 507627.93908691406 6683300.5217285156, 507637.86090087891 6683312.4281005859, 507648.44427490234 6683319.0427246094, 507653.73590087891 6683319.0427246094, 507664.98071289063 6683305.8134765625, 507672.25689697266 6683308.4592895508, 507682.84008789063 6683307.7977294922, 507695.40789794922 6683301.1831054688, 507708.63708496094 6683297.8759155273, 507721.20489501953 6683302.5061035156, 507726.49652099609 6683311.1051025391, 507731.78833007813 6683328.3031005859, 507756.26232910156 6683354.7614746094, 507774.78308105469 6683361.3760986328, 507782.05908203125 6683385.1884765625, 507797.27270507813 6683397.7562866211, 507817.11651611328 6683405.6939086914, 507825.05407714844 6683405.6939086914, 507834.97589111328 6683393.1260986328, 507849.52807617188 6683385.1884765625, 507860.77288818359 6683378.5739135742, 507871.35607910156 6683377.9124755859, 507884.58532714844 6683383.8657226563, 507891.86151123047 6683396.4332885742, 507889.87707519531 6683406.3552856445, 507899.79888916016 6683412.969909668, 507919.64270019531 6683412.30847168, 507940.80950927734 6683409.6624755859, 507942.13232421875 6683401.7250976563, 507942.79388427734 6683394.44909668, 507959.33032226563 6683397.7562866211, 507980.4970703125 6683399.7407226563, 508002.32531738281 6683403.0479125977, 508020.84613037109 6683400.4020996094, 508025.47631835938 6683393.7874755859, 508020.84613037109 6683382.5427246094, 508008.2783203125 6683378.5739135742, 508007.61688232422 6683369.3134765625, 508019.52307128906 6683348.8082885742, 508016.87731933594 6683306.4749145508, 508021.50750732422 6683299.8602905273, 508034.73669433594 6683294.5687255859, 508054.58050537109 6683295.8914794922, 508065.16387939453 6683286.6311035156, 508071.77850341797 6683284.6467285156, 508082.36187744141 6683289.2769165039, 508094.92950439453 6683280.0164794922, 508108.8203125 6683282.6622924805, 508122.71087646484 6683288.6154785156, 508131.97131347656 6683280.0164794922, 508137.26287841797 6683274.7249145508, 508145.86187744141 6683270.7561035156, 508157.76831054688 6683262.1571044922, 508170.99749755859 6683262.1571044922, 508206.0546875 6683280.6779174805, 508243.75787353516 6683306.4749145508, 508268.89349365234 6683319.7041015625, 508286.7529296875 6683334.9177246094, 508290.06011962891 6683347.4852905273, 508290.06011962891 6683362.0374755859, 508293.36749267578 6683373.9437255859, 508304.6123046875 6683381.219909668, 508319.16430664063 6683381.219909668, 508356.20611572266 6683367.3291015625, 508358.85192871094 6683375.2667236328, 508352.89892578125 6683386.5114746094, 508354.2216796875 6683400.4020996094, 508362.82067871094 6683427.5219116211, 508364.80511474609 6683453.3189086914, 508368.77392578125 6683461.9179077148, 508383.32592773438 6683468.5324707031, 508384.64892578125 6683484.4074707031, 508401.18530273438 6683502.9282836914, 508417.06048583984 6683532.6940917969, 508430.28967285156 6683563.7827148438, 508440.60827636719 6683575.1597290039, 508468.12512207031 6683591.0349121094, 508508.87109375 6683591.0349121094, 508520.5126953125 6683582.5681152344, 508526.86267089844 6683586.272277832, 508525.80432128906 6683595.2681274414, 508530.03771972656 6683602.6765136719, 508537.44610595703 6683605.8515014648, 508545.38348388672 6683598.9722900391, 508554.37951660156 6683592.6223144531, 508562.31689453125 6683587.3306884766, 508572.37109375 6683591.5639038086, 508585.60028076172 6683604.79309082, 508596.18371582031 6683614.3181152344, 508613.64630126953 6683614.8472900391, 508632.6962890625 6683610.0848999023, 508642.75048828125 6683603.7349243164, 508654.39208984375 6683588.91809082, 508676.61712646484 6683571.4556884766, 508687.20050048828 6683561.4014892578, 508713.65887451172 6683552.4055175781, 508742.76312255859 6683557.69732666, 508763.40069580078 6683559.2847290039, 508776.10070800781 6683563.5181274414, 508787.74249267578 6683562.9888916016, 508814.20068359375 6683581.5098876953, 508817.37567138672 6683584.6848754883, 508832.19250488281 6683585.7431030273, 508844.36328125 6683595.7973022461, 508859.70928955078 6683600.0307006836, 508868.705078125 6683609.5557250977, 508862.88427734375 6683636.5432739258, 508857.06329345703 6683648.7141113281, 508844.36328125 6683663.5307006836, 508843.30511474609 6683685.22668457, 508825.84252929688 6683701.63092041, 508809.96752929688 6683706.3933105469, 508790.38830566406 6683719.6224975586, 508751.75891113281 6683750.3142700195, 508714.18811035156 6683761.955871582, 508696.1962890625 6683764.6019287109, 508681.90887451172 6683763.5435180664, 508662.32971191406 6683769.8934936523, 508632.16711425781 6683768.8350830078, 508619.99627685547 6683772.5393066406, 508604.65032958984 6683779.9476928711, 508583.48370361328 6683776.7727050781, 508571.84191894531 6683781.0061035156, 508563.90447998047 6683784.7103271484, 508555.96691894531 6683781.0061035156, 508543.79608154297 6683767.7769165039, 508541.67950439453 6683739.2017211914, 508537.97528076172 6683725.9724731445, 508528.97930908203 6683720.6809082031, 508516.279296875 6683717.50592041, 508491.93768310547 6683719.0935058594, 508480.2958984375 6683722.2684936523, 508472.8876953125 6683731.2642822266, 508469.71252441406 6683742.3767089844, 508467.59588623047 6683749.25592041, 508459.12927246094 6683750.8435058594, 508452.779296875 6683748.1976928711, 508449.60430908203 6683735.4976806641, 508442.19592285156 6683725.9724731445, 508439.55010986328 6683703.2183227539, 508434.25830078125 6683703.7474975586, 508425.26251220703 6683709.0393066406, 508403.56671142578 6683734.4392700195, 508391.39569091797 6683741.3184814453, 508384.51647949219 6683751.3726806641, 508371.81652832031 6683747.1392822266, 508363.87908935547 6683749.25592041, 508359.64569091797 6683751.3726806641, 508345.35809326172 6683753.4893188477, 508339.53729248047 6683752.4309082031, 508332.12890625 6683743.9642944336, 508324.19152832031 6683741.8477172852, 508312.54968261719 6683741.3184814453, 508308.845703125 6683731.7935180664, 508310.83532714844 6683706.9224853516, 508303.95611572266 6683691.5767211914, 508293.37268066406 6683693.69329834, 508280.67272949219 6683701.63092041, 508263.21008300781 6683698.9851074219, 508253.15588378906 6683686.2850952148, 508240.98510742188 6683675.7017211914, 508236.22271728516 6683663.5307006836, 508228.28509521484 6683663.0017089844, 508213.99749755859 6683674.114074707, 508199.18090820313 6683677.81829834, 508193.88928222656 6683683.1101074219, 508191.24328613281 6683678.8767089844, 508193.88928222656 6683669.88092041, 508181.71832275391 6683668.2932739258, 508176.95587158203 6683676.2308959961, 508165.31408691406 6683676.7598876953, 508157.90588378906 6683690.5183105469, 508157.90588378906 6683701.63092041, 508164.78491210938 6683709.56829834, 508160.02252197266 6683719.6224975586, 508146.79327392578 6683718.0350952148, 508135.15148925781 6683722.7974853516, 508118.74731445313 6683740.2600708008, 508105.51831054688 6683755.6058959961, 508097.58068847656 6683763.0142822266, 508095.99310302734 6683767.7769165039, 508101.28491210938 6683778.3601074219, 508097.05151367188 6683783.1226806641, 508104.45989990234 6683787.8853149414, 508117.15991210938 6683791.5892944336, 508123.50988769531 6683801.1145019531, 508132.50567626953 6683810.1102905273, 508136.73907470703 6683828.1019287109, 508141.50170898438 6683835.5103149414, 508147.32250976563 6683843.4478759766, 508147.32250976563 6683864.6145019531, 508163.19750976563 6683874.1395263672, 508180.66009521484 6683891.0728759766, 508198.65167236328 6683900.0687255859, 508217.17248535156 6683900.5979003906, 508249.45190429688 6683894.7770996094, 508278.02691650391 6683893.7186889648, 508296.01849365234 6683899.5396728516, 508305.54370117188 6683913.2979125977, 508309.77691650391 6683923.3521118164, 508326.18109130859 6683934.9938964844, 508342.05609130859 6683937.1104736328, 508360.57708740234 6683937.1104736328, 508374.86450195313 6683943.4605102539, 508375.92291259766 6683952.9854736328, 508382.80212402344 6683956.6896972656, 508394.44372558594 6683954.0438842773, 508417.19787597656 6683954.0438842773, 508434.13128662109 6683945.0480957031, 508446.83129882813 6683930.2313232422, 508460.06048583984 6683929.7020874023, 508467.46887207031 6683924.4105224609, 508476.99389648438 6683921.7647094727, 508496.57312011719 6683928.6439208984, 508511.38989257813 6683928.6439208984, 508527.26489257813 6683936.5812988281, 508539.96490478516 6683947.6939086914, 508548.43151855469 6683957.74810791, 508573.83172607422 6683958.8062744141, 508590.76507568359 6683967.2730712891, 508595.52752685547 6683974.1522827148, 508596.05670166016 6683984.2064819336, 508606.64007568359 6683995.3189086914, 508621.45672607422 6684004.84387207, 508650.03167724609 6684022.3065185547, 508650.56091308594 6684028.1273193359, 508632.56927490234 6684035.5357055664, 508629.92352294922 6684041.3565063477, 508628.86511230469 6684048.2357177734, 508615.63592529297 6684065.6983032227, 508605.05249023438 6684070.9899291992, 508550.54827880859 6684063.5814819336, 508524.61907958984 6684051.4107055664, 508510.86071777344 6684050.8815307617, 508498.16070556641 6684057.2315063477, 508491.28149414063 6684067.2857055664, 508493.39807128906 6684077.86907959, 508476.46472167969 6684079.4567260742, 508454.23968505859 6684093.2149047852, 508449.47729492188 6684098.5067138672, 508400.26470947266 6684099.5651245117, 508378.56872558594 6684086.3358764648, 508367.45629882813 6684079.9857177734, 508366.92712402344 6684064.6398925781, 508357.40209960938 6684047.7064819336, 508339.93951416016 6684037.1232910156, 508323.53527832031 6684039.2399291992, 508316.12689208984 6684041.3565063477, 508298.13531494141 6684039.2399291992, 508292.31451416016 6684041.3565063477, 508278.02691650391 6684057.2315063477, 508264.26849365234 6684065.169128418, 508240.98510742188 6684069.93170166, 508217.17248535156 6684065.169128418, 508197.59332275391 6684054.0565185547, 508185.95172119141 6684044.0023193359, 508177.48510742188 6684047.1773071289, 508169.54748535156 6684051.4107055664, 508157.90588378906 6684050.8815307617, 508147.85168457031 6684044.5314941406, 508140.44329833984 6684022.8356933594, 508129.85992431641 6683996.9064941406, 508116.10150146484 6683992.1439208984, 508107.10571289063 6683985.2647094727, 508100.22650146484 6683982.08972168, 508100.75567626953 6683967.8023071289, 508085.40991210938 6683949.81048584, 508056.83471679688 6683947.6939086914, 508038.84307861328 6683961.9813232422, 508028.25970458984 6683967.2730712891, 508018.20550537109 6683967.2730712891, 507998.09710693359 6683955.6312866211, 507980.63470458984 6683940.2855224609, 507967.93450927734 6683930.2313232422, 507968.46368408203 6683906.9478759766, 507956.29290771484 6683907.4771118164, 507933.53869628906 6683887.3687133789, 507902.84692382813 6683864.6145019531, 507872.68450927734 6683837.0979003906, 507853.10510253906 6683814.8726806641, 507845.69689941406 6683805.87689209, 507812.35931396484 6683791.5892944336, 507783.78430175781 6683761.4268798828, 507782.19671630859 6683747.6685180664, 507782.19671630859 6683728.6185302734, 507757.32592773438 6683708.5100708008, 507758.91333007813 6683694.7517089844, 507748.85913085938 6683681.5225219727, 507738.80487060547 6683656.1224975586, 507727.69250488281 6683642.364074707, 507728.75067138672 6683634.9556884766, 507721.34252929688 6683625.4307250977, 507715.521484375 6683618.022277832, 507695.41333007813 6683612.2014770508, 507683.771484375 6683601.0891113281, 507674.77569580078 6683573.57232666, 507646.20068359375 6683529.6513061523, 507613.39227294922 6683479.9094848633, 507599.10467529297 6683466.1513061523, 507567.88372802734 6683449.7470703125, 507523.43371582031 6683431.2260742188, 507492.74188232422 6683425.4052734375, 507457.28771972656 6683423.8178710938, 507437.17932128906 6683430.6968994141, 507431.35852050781 6683430.6968994141, 507446.70428466797 6683419.5844726563, 507458.87530517578 6683408.4719238281, 507467.87109375 6683393.1260986328, 507477.39611816406 6683359.7885131836, 507482.15869140625 6683334.9177246094, 507477.92529296875 6683310.0466918945, 507463.10852050781 6683288.8801269531, 507448.29187011719 6683272.4758911133, 507442.47113037109 6683261.89251709, 507435.59191894531 6683241.7841186523, 507428.71270751953 6683243.3717041016, 507420.24591064453 6683247.0756835938, 507410.19189453125 6683241.2548828125, 507401.19592285156 6683225.3798828125, 507389.55432128906 6683215.3256835938, 507367.85827636719 6683210.563293457, 507358.86248779297 6683215.3256835938, 507350.92510986328 6683226.438293457, 507339.8125 6683228.5548706055, 507333.99169921875 6683234.3756713867, 507318.64569091797 6683226.438293457, 507305.94567871094 6683222.7341308594, 507297.47912597656 6683211.0922851563, 507293.24572753906 6683199.4506835938, 507291.65832519531 6683181.45892334, 507281.07489013672 6683164.5256958008, 507277.89990234375 6683150.2380981445, 507280.01647949219 6683134.3630981445, 507273.66650390625 6683119.0172729492, 507275.25408935547 6683105.7880859375, 507276.31231689453 6683102.6130981445, 507287.9541015625 6683106.8463134766, 507293.77490234375 6683103.1420898438, 507300.12487792969 6683094.1463012695, 507301.71252441406 6683078.2713012695, 507307.00408935547 6683070.8629150391, 507301.71252441406 6683067.1586914063, 507311.23748779297 6683053.9295043945, 507314.41247558594 6683045.9921264648, 507321.82092285156 6683032.2337036133, 507329.75830078125 6683026.412902832, 507343.51672363281 6683032.2337036133, 507359.39172363281 6683042.8170776367, 507372.62091064453 6683058.162902832, 507375.26672363281 6683073.5087280273, 507374.20849609375 6683089.3839111328, 507382.14587402344 6683099.4381103516, 507396.43347167969 6683133.83392334, 507399.60852050781 6683141.771484375, 507410.72088623047 6683173.521484375, 507420.24591064453 6683185.6923217773, 507422.89190673828 6683196.8048706055, 507432.94610595703 6683195.2172851563, 507440.35430908203 6683178.8131103516, 507445.11688232422 6683167.7006835938, 507459.40447998047 6683155.0006713867, 507459.40447998047 6683140.7130737305, 507450.93768310547 6683132.2465209961, 507435.59191894531 6683124.8380737305, 507425.00848388672 6683113.7255249023, 507413.89587402344 6683093.6171264648, 507399.07928466797 6683070.3336791992, 507397.49169921875 6683055.5170898438, 507393.78771972656 6683033.8212890625, 507386.90850830078 6683009.4794921875, 507384.26251220703 6682993.6044921875, 507386.90850830078 6682980.9044799805, 507399.60852050781 6682985.6668701172, 507407.5458984375 6682984.6087036133, 507408.07507324219 6682970.8502807617, 507405.95849609375 6682956.5626831055, 507411.779296875 6682952.8585205078, 507424.47930908203 6682959.7376708984, 507430.82928466797 6682957.0919189453, 507433.47509765625 6682941.2169189453, 507436.12109375 6682931.1627197266, 507432.94610595703 6682911.0543212891, 507431.35852050781 6682900.4708862305, 507436.65008544922 6682885.654296875, 507440.88348388672 6682870.8375244141, 507449.87927246094 6682851.7874755859, 507449.35028076172 6682827.9749145508, 507443.529296875 6682818.9791259766, 507431.8876953125 6682812.6290893555, 507429.77111816406 6682799.9290771484, 507444.58770751953 6682779.2915039063, 507449.35028076172 6682778.2330932617, 507454.11267089844 6682784.0540771484, 507463.10852050781 6682785.1123046875, 507478.45452880859 6682780.8790893555, 507494.32952880859 6682777.1749267578, 507516.02532958984 6682762.8873291016, 507535.6044921875 6682748.5999145508, 507555.712890625 6682750.7164916992, 507567.88372802734 6682747.0123291016, 507572.11712646484 6682737.4873046875, 507574.76287841797 6682729.0205078125, 507591.6962890625 6682720.553894043, 507609.15887451172 6682709.44128418, 507618.15472412109 6682705.2081298828, 507633.23071289063 6682703.8850708008))"
				);
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(null, g.getCoordinates());
		assertEquals("" +
				"[POINT (607182 7361411), " +
				"POLYGON ((607328 7361552, 607903 7361359, 607696 7361103, 607167 7361312, 607328 7361552)), " +
				"POLYGON ((507633.2307128906 6682703.885070801, 507444.58770751953 6682779.291503906, 507275.25408935547 6683105.7880859375, 507297.47912597656 6683211.092285156, 507431.3585205078 6683430.696899414, 507967.93450927734 6683930.231323242, 508157.90588378906 6684050.881530762, 508400.26470947266 6684099.565124512, 508615.63592529297 6684065.698303223, 508650.56091308594 6684028.127319336, 508843.3051147461 6683685.22668457, 508868.705078125 6683609.555725098, 507633.2307128906 6682703.885070801))]",
				g.getGeo().getWKTList().toString());
	}

	@Test
	public void complexPolygon_2() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.occurrenceWKT = Utils.list(
				"MULTIPOLYGON (((666228.46588134766 7040711.2554931641, 666229.59387207031 7040715.4649047852, 666229.97369384766 7040719.8060913086, 666229.59387207031 7040724.147277832, 666228.46588134766 7040728.3565063477, 666226.62432861328 7040732.3060913086, 666225.58209228516 7040733.7943115234, 666228.46911621094 7040735.1406860352, 666232.03887939453 7040737.6400756836, 666235.120300293 7040740.721496582, 666237.61968994141 7040744.2913208008, 666239.46148681641 7040748.2407226563, 666240.58929443359 7040752.4500732422, 666240.96911621094 7040756.7913208008, 666240.58929443359 7040761.1325073242, 666239.46148681641 7040765.3416748047, 666237.61968994141 7040769.2913208008, 666235.120300293 7040772.8609008789, 666232.03887939453 7040775.9423217773, 666228.46911621094 7040778.4418945313, 666224.51971435547 7040780.2835083008, 666220.31030273438 7040781.4114990234, 666215.96911621094 7040781.7913208008, 666211.6279296875 7040781.4114990234, 666207.41870117188 7040780.2835083008, 666203.46911621094 7040778.4418945313, 666199.89947509766 7040775.9423217773, 666196.81811523438 7040772.8609008789, 666194.31848144531 7040769.2913208008, 666192.47692871094 7040765.3416748047, 666191.34887695313 7040761.1325073242, 666190.96911621094 7040756.7913208008, 666191.34887695313 7040752.4500732422, 666192.47692871094 7040748.2407226563, 666194.31848144531 7040744.2913208008, 666195.36071777344 7040742.8029174805, 666192.47369384766 7040741.4567260742, 666188.90393066406 7040738.9570922852, 666185.82250976563 7040735.8756713867, 666183.32312011719 7040732.3060913086, 666181.48132324219 7040728.3565063477, 666180.353515625 7040724.147277832, 666179.97369384766 7040719.8060913086, 666180.32769775391 7040715.7603149414, 666176.64227294922 7040715.4379272461, 666172.43292236328 7040714.3098754883, 666168.48352050781 7040712.4683227539, 666164.91369628906 7040709.9686889648, 666164.162902832 7040709.2178955078, 666163.33349609375 7040709.4403076172, 666158.99230957031 7040709.8201293945, 666154.65112304688 7040709.4403076172, 666150.44189453125 7040708.3123168945, 666146.49230957031 7040706.470703125, 666142.92272949219 7040703.9711303711, 666139.84130859375 7040700.8897094727, 666137.34167480469 7040697.3201293945, 666135.50012207031 7040693.3704833984, 666134.3720703125 7040689.161315918, 666133.99230957031 7040684.8201293945, 666134.3720703125 7040680.4788818359, 666135.50012207031 7040676.2694702148, 666137.34167480469 7040672.3201293945, 666139.84130859375 7040668.7503051758, 666142.92272949219 7040665.6688842773, 666146.49230957031 7040663.1694946289, 666150.44189453125 7040661.3276977539, 666154.65112304688 7040660.1998901367, 666158.99230957031 7040659.8201293945, 666159.32189941406 7040659.8488769531, 666160.43792724609 7040659.3284912109, 666164.64709472656 7040658.2006835938, 666168.98828125 7040657.8209228516, 666173.32952880859 7040658.2006835938, 666177.53887939453 7040659.3284912109, 666180.55609130859 7040660.7354736328, 666181.42932128906 7040660.3283081055, 666185.638671875 7040659.2003173828, 666189.97991943359 7040658.8204956055, 666194.321105957 7040659.2003173828, 666198.5302734375 7040660.3283081055, 666202.47991943359 7040662.169921875, 666206.04949951172 7040664.6694946289, 666209.13092041016 7040667.7509155273, 666211.63049316406 7040671.3204956055, 666213.47210693359 7040675.2700805664, 666214.60009765625 7040679.479309082, 666214.97991943359 7040683.8204956055, 666214.60009765625 7040688.1616821289, 666213.47210693359 7040692.37109375, 666211.84368896484 7040695.8635253906, 666213.52410888672 7040696.3137207031, 666217.47369384766 7040698.1555175781, 666221.04327392578 7040700.6549072266, 666224.12469482422 7040703.736328125, 666226.62432861328 7040707.3060913086, 666228.46588134766 7040711.2554931641)), "+
						"((666296.56628417969 7040863.4055175781, 666296.946105957 7040867.7467041016, 666296.56628417969 7040872.087890625, 666295.4384765625 7040876.2973022461, 666293.5966796875 7040880.2467041016, 666291.09729003906 7040883.81652832, 666288.01593017578 7040886.8978881836, 666284.446105957 7040889.397277832, 666280.49670410156 7040891.239074707, 666276.28729248047 7040892.3668823242, 666271.946105957 7040892.7467041016, 666267.60491943359 7040892.3668823242, 666263.395690918 7040891.239074707, 666259.446105957 7040889.397277832, 666255.87652587891 7040886.8978881836, 666252.79510498047 7040883.81652832, 666250.29547119141 7040880.2467041016, 666248.453918457 7040876.2973022461, 666247.32592773438 7040872.087890625, 666246.946105957 7040867.7467041016, 666247.32592773438 7040863.4055175781, 666248.453918457 7040859.1962890625, 666250.29547119141 7040855.2467041016, 666252.79510498047 7040851.6771240234, 666255.87652587891 7040848.595703125, 666259.446105957 7040846.0961303711, 666263.395690918 7040844.2545166016, 666267.60491943359 7040843.1265258789, 666271.946105957 7040842.7467041016, 666276.28729248047 7040843.1265258789, 666276.32427978516 7040843.1364746094, 666276.31427001953 7040843.09967041, 666275.93450927734 7040838.7584838867, 666276.22448730469 7040835.444519043, 666275.60192871094 7040835.3898925781, 666271.39270019531 7040834.2620849609, 666267.44311523438 7040832.4202880859, 666263.87347412109 7040829.9208984375, 666260.79211425781 7040826.8394775391, 666258.29248046875 7040823.2697143555, 666256.45092773438 7040819.3203125, 666255.32287597656 7040815.1109008789, 666255.02508544922 7040811.7067260742, 666253.39990234375 7040811.2713012695, 666249.45050048828 7040809.4295043945, 666245.88067626953 7040806.9301147461, 666242.79931640625 7040803.8486938477, 666240.29992675781 7040800.2789306641, 666238.45812988281 7040796.3295288086, 666237.33032226563 7040792.1201171875, 666236.95050048828 7040787.7789306641, 666237.33032226563 7040783.4376831055, 666238.45812988281 7040779.228515625, 666240.29992675781 7040775.2789306641, 666242.79931640625 7040771.7092895508, 666245.88067626953 7040768.6279296875, 666249.45050048828 7040766.1282958984, 666253.39990234375 7040764.2866821289, 666257.60931396484 7040763.1586914063, 666261.95050048828 7040762.7789306641, 666266.29168701172 7040763.1586914063, 666270.50091552734 7040764.2866821289, 666274.45050048828 7040766.1282958984, 666278.02008056641 7040768.6279296875, 666281.10150146484 7040771.7092895508, 666283.60107421875 7040775.2789306641, 666285.44268798828 7040779.228515625, 666286.57067871094 7040783.4376831055, 666286.86853027344 7040786.8419189453, 666288.49371337891 7040787.2775268555, 666292.44311523438 7040789.11907959, 666296.012878418 7040791.6187133789, 666299.09429931641 7040794.7000732422, 666301.59368896484 7040798.2697143555, 666303.43548583984 7040802.2192993164, 666304.563293457 7040806.428527832, 666304.94311523438 7040810.7697143555, 666304.6533203125 7040814.0836791992, 666305.27569580078 7040814.1383056641, 666309.48510742188 7040815.2661132813, 666313.43450927734 7040817.1079101563, 666317.00427246094 7040819.6072998047, 666320.08569335938 7040822.6887207031, 666322.58508300781 7040826.2584838867, 666324.42687988281 7040830.2078857422, 666325.5546875 7040834.4172973633, 666325.93450927734 7040838.7584838867, 666325.5546875 7040843.09967041, 666324.42687988281 7040847.3088989258, 666322.58508300781 7040851.2584838867, 666320.08569335938 7040854.828125, 666317.00427246094 7040857.9094848633, 666313.43450927734 7040860.4091186523, 666309.48510742188 7040862.2506713867, 666305.27569580078 7040863.3787231445, 666300.93450927734 7040863.7584838867, 666296.59332275391 7040863.3787231445, 666296.55651855469 7040863.3687133789, 666296.56628417969 7040863.4055175781)), "+
						"((664706.91827392578 7041259.9528808594, 664711.12750244141 7041261.080871582, 664715.07708740234 7041262.9224853516, 664718.64672851563 7041265.4221191406, 664721.72808837891 7041268.5034790039, 664724.227722168 7041272.0731201172, 664726.06927490234 7041276.0227050781, 664727.19732666016 7041280.2318725586, 664727.57708740234 7041284.5731201172, 664727.19732666016 7041288.9143066406, 664726.06927490234 7041293.1237182617, 664724.227722168 7041297.0731201172, 664721.72808837891 7041300.6428833008, 664721.51708984375 7041300.8538818359, 664721.579284668 7041301.56628418, 664721.19952392578 7041305.9074707031, 664720.07171630859 7041310.1168823242, 664718.22991943359 7041314.06628418, 664715.73052978516 7041317.6361083984, 664712.64910888672 7041320.7175292969, 664709.95611572266 7041322.6030883789, 664711.07507324219 7041325.0029296875, 664712.203125 7041329.212097168, 664712.36767578125 7041331.0941162109, 664713.20269775391 7041334.2100830078, 664713.58251953125 7041338.5513305664, 664713.20269775391 7041342.89251709, 664712.07470703125 7041347.1019287109, 664710.23309326172 7041351.0513305664, 664707.73352050781 7041354.62109375, 664704.65209960938 7041357.7025146484, 664701.08251953125 7041360.2019042969, 664697.13287353516 7041362.0437011719, 664692.92370605469 7041363.1715087891, 664688.58251953125 7041363.5513305664, 664684.24127197266 7041363.1715087891, 664680.03192138672 7041362.0437011719, 664676.08251953125 7041360.2019042969, 664672.5126953125 7041357.7025146484, 664669.43127441406 7041354.62109375, 664666.93188476563 7041351.0513305664, 664665.09008789063 7041347.1019287109, 664663.96228027344 7041342.89251709, 664663.79772949219 7041341.0104980469, 664662.96270751953 7041337.8944702148, 664662.58288574219 7041333.5532836914, 664662.96270751953 7041329.212097168, 664664.09051513672 7041325.0029296875, 664665.93231201172 7041321.0532836914, 664668.43170166016 7041317.4837036133, 664671.51312255859 7041314.4022827148, 664674.20611572266 7041312.5167236328, 664673.087097168 7041310.1168823242, 664671.95910644531 7041305.9074707031, 664671.579284668 7041301.56628418, 664671.95910644531 7041297.2250976563, 664673.087097168 7041293.0159301758, 664674.9287109375 7041289.06628418, 664677.42828369141 7041285.4967041016, 664677.63928222656 7041285.2855224609, 664677.57708740234 7041284.5731201172, 664677.95690917969 7041280.2318725586, 664679.08471679688 7041276.0227050781, 664680.92651367188 7041272.0731201172, 664683.42590332031 7041268.5034790039, 664686.50732421875 7041265.4221191406, 664690.07708740234 7041262.9224853516, 664694.02648925781 7041261.080871582, 664698.23590087891 7041259.9528808594, 664702.57708740234 7041259.5731201172, 664706.91827392578 7041259.9528808594)), "+
						"((664644.10510253906 7041450.1655273438, 664640.15570068359 7041452.0073242188, 664635.9462890625 7041453.1350708008, 664631.60510253906 7041453.5148925781, 664627.26391601563 7041453.1350708008, 664623.0546875 7041452.0073242188, 664619.10510253906 7041450.1655273438, 664617.33752441406 7041448.9279174805, 664617.16491699219 7041449.0083007813, 664612.95550537109 7041450.1362915039, 664608.61431884766 7041450.5161132813, 664604.27307128906 7041450.1362915039, 664600.06390380859 7041449.0083007813, 664596.11431884766 7041447.1666870117, 664592.54467773438 7041444.6671142578, 664589.46331787109 7041441.5856933594, 664586.963684082 7041438.0161132813, 664585.1220703125 7041434.06652832, 664583.99407958984 7041429.8572998047, 664583.61431884766 7041425.5161132813, 664583.99407958984 7041421.1749267578, 664585.1220703125 7041416.9655151367, 664586.963684082 7041413.0161132813, 664589.46331787109 7041409.4462890625, 664592.54467773438 7041406.3649291992, 664596.11431884766 7041403.8654785156, 664600.06390380859 7041402.0236816406, 664604.27307128906 7041400.8958740234, 664608.61431884766 7041400.5161132813, 664612.95550537109 7041400.8958740234, 664617.16491699219 7041402.0236816406, 664621.11431884766 7041403.8654785156, 664622.88208007813 7041405.1030883789, 664623.0546875 7041405.0227050781, 664627.26391601563 7041403.8947143555, 664631.60510253906 7041403.5148925781, 664635.9462890625 7041403.8947143555, 664640.15570068359 7041405.0227050781, 664642.686706543 7041406.2028808594, 664645.52331542969 7041403.3662719727, 664649.09307861328 7041400.8668823242, 664653.04248046875 7041399.0250854492, 664657.25189208984 7041397.897277832, 664661.59307861328 7041397.51751709, 664665.93432617188 7041397.897277832, 664670.14349365234 7041399.0250854492, 664674.09307861328 7041400.8668823242, 664677.66271972656 7041403.3662719727, 664680.74407958984 7041406.4476928711, 664683.24371337891 7041410.01751709, 664685.08532714844 7041413.9669189453, 664686.21331787109 7041418.1763305664, 664686.59307861328 7041422.51751709, 664686.21331787109 7041426.8587036133, 664685.08532714844 7041431.0678710938, 664683.24371337891 7041435.01751709, 664680.74407958984 7041438.587097168, 664677.66271972656 7041441.6685180664, 664674.09307861328 7041444.16809082, 664670.14349365234 7041446.00970459, 664665.93432617188 7041447.1376953125, 664661.59307861328 7041447.51751709, 664657.25189208984 7041447.1376953125, 664653.04248046875 7041446.00970459, 664650.51147460938 7041444.8295288086, 664647.67492675781 7041447.66607666, 664644.10510253906 7041450.1655273438)), "+
						"((666111.169921875 7040868.8218994141, 666108.08850097656 7040871.9033203125, 666104.51867675781 7040874.4027099609, 666100.56927490234 7040876.2445068359, 666096.35992431641 7040877.3723144531, 666092.01867675781 7040877.7520751953, 666087.67749023438 7040877.3723144531, 666083.46832275391 7040876.2445068359, 666079.51867675781 7040874.4027099609, 666075.94909667969 7040871.9033203125, 666072.86767578125 7040868.8218994141, 666070.36810302734 7040865.2520751953, 666068.52648925781 7040861.30267334, 666067.39849853516 7040857.0933227539, 666067.01867675781 7040852.7520751953, 666067.39849853516 7040848.4108886719, 666068.52648925781 7040844.2017211914, 666070.36810302734 7040840.2520751953, 666072.86767578125 7040836.6824951172, 666075.94909667969 7040833.6010742188, 666079.51867675781 7040831.1015014648, 666083.46832275391 7040829.2598876953, 666087.67749023438 7040828.1318969727, 666092.01867675781 7040827.7520751953, 666095.49847412109 7040828.0565185547, 666097.85748291016 7040824.6873168945, 666100.93890380859 7040821.6058959961, 666104.50872802734 7040819.1065063477, 666108.45812988281 7040817.2647094727, 666112.66748046875 7040816.1369018555, 666117.00872802734 7040815.7570800781, 666121.34991455078 7040816.1369018555, 666124.321472168 7040816.9331054688, 666125.50030517578 7040816.1077270508, 666129.44970703125 7040814.2661132813, 666133.65911865234 7040813.1381225586, 666138.00030517578 7040812.7583007813, 666142.34149169922 7040813.1381225586, 666146.55072021484 7040814.2661132813, 666150.50030517578 7040816.1077270508, 666154.06988525391 7040818.6072998047, 666157.15130615234 7040821.6887207031, 666159.65087890625 7040825.2583007813, 666161.49249267578 7040829.2078857422, 666162.62048339844 7040833.4171142578, 666163.00030517578 7040837.7583007813, 666162.62048339844 7040842.0994873047, 666161.49249267578 7040846.3088989258, 666159.65087890625 7040850.2583007813, 666157.15130615234 7040853.828125, 666154.06988525391 7040856.9094848633, 666150.50030517578 7040859.4088745117, 666146.55072021484 7040861.2506713867, 666142.34149169922 7040862.3784790039, 666138.00030517578 7040862.7583007813, 666133.65911865234 7040862.3784790039, 666130.6875 7040861.5822753906, 666129.50872802734 7040862.4077148438, 666125.55908203125 7040864.2493286133, 666121.34991455078 7040865.3773193359, 666117.00872802734 7040865.7570800781, 666113.52893066406 7040865.4526977539, 666111.169921875 7040868.8218994141)), "+
						"((666349.25830078125 7040824.3944702148, 666344.91711425781 7040824.7742919922, 666340.57592773438 7040824.3944702148, 666338.91949462891 7040823.9506835938, 666337.26312255859 7040824.3944702148, 666332.921875 7040824.7742919922, 666328.58068847656 7040824.3944702148, 666324.37127685547 7040823.2667236328, 666320.421875 7040821.4249267578, 666316.85211181641 7040818.9254760742, 666313.770690918 7040815.8441162109, 666311.27130126953 7040812.2742919922, 666309.42950439453 7040808.3248901367, 666308.30169677734 7040804.1154785156, 666307.921875 7040799.7742919922, 666308.30169677734 7040795.4331054688, 666309.42950439453 7040791.2238769531, 666311.27130126953 7040787.2742919922, 666313.770690918 7040783.7047119141, 666316.85211181641 7040780.6232910156, 666320.421875 7040778.1237182617, 666324.37127685547 7040776.2821044922, 666328.58068847656 7040775.1541137695, 666332.921875 7040774.7742919922, 666337.26312255859 7040775.1541137695, 666338.91949462891 7040775.5979003906, 666340.57592773438 7040775.1541137695, 666344.91711425781 7040774.7742919922, 666349.25830078125 7040775.1541137695, 666351.35052490234 7040775.71472168, 666352.75512695313 7040773.7086791992, 666355.83648681641 7040770.6273193359, 666359.40612792969 7040768.1279296875, 666363.35571289063 7040766.2860717773, 666367.56488037109 7040765.1583251953, 666369.44750976563 7040764.9935302734, 666372.56292724609 7040764.1586914063, 666376.90411376953 7040763.7789306641, 666381.245300293 7040764.1586914063, 666385.45471191406 7040765.2866821289, 666389.40411376953 7040767.1282958984, 666392.97387695313 7040769.6279296875, 666396.05529785156 7040772.7092895508, 666398.5546875 7040776.2789306641, 666400.396484375 7040780.228515625, 666401.52429199219 7040784.4376831055, 666401.90411376953 7040788.7789306641, 666401.52429199219 7040793.1201171875, 666400.396484375 7040797.3295288086, 666398.5546875 7040801.2789306641, 666396.05529785156 7040804.8486938477, 666392.97387695313 7040807.9301147461, 666389.40411376953 7040810.4295043945, 666385.45471191406 7040812.2713012695, 666381.245300293 7040813.3991088867, 666379.36291503906 7040813.5639038086, 666376.24731445313 7040814.3986816406, 666371.90612792969 7040814.778503418, 666367.56488037109 7040814.3986816406, 666365.47271728516 7040813.8380737305, 666364.06811523438 7040815.8441162109, 666360.98669433594 7040818.9254760742, 666357.41711425781 7040821.4249267578, 666353.46752929688 7040823.2667236328, 666349.25830078125 7040824.3944702148)), "+
						"((666158.33471679688 7040917.0963134766, 666162.54388427734 7040918.2241210938, 666166.49353027344 7040920.0659179688, 666170.06311035156 7040922.5653076172, 666173.14447021484 7040925.6467285156, 666175.64410400391 7040929.2164916992, 666177.48571777344 7040933.1658935547, 666178.61370849609 7040937.3753051758, 666178.99353027344 7040941.7164916992, 666178.61370849609 7040946.0576782227, 666177.48571777344 7040950.2669067383, 666175.64410400391 7040954.2164916992, 666173.14447021484 7040957.7860717773, 666170.06311035156 7040960.8674926758, 666166.49353027344 7040963.3671264648, 666162.54388427734 7040965.2086791992, 666159.97448730469 7040965.897277832, 666161.49212646484 7040969.1514892578, 666162.61987304688 7040973.3607177734, 666162.99969482422 7040977.7019042969, 666162.61987304688 7040982.04309082, 666161.49212646484 7040986.2525024414, 666159.65032958984 7040990.2019042969, 666157.15087890625 7040993.7717285156, 666154.069519043 7040996.8530883789, 666150.49969482422 7040999.3524780273, 666146.55029296875 7041001.1942749023, 666142.34088134766 7041002.3220825195, 666137.99969482422 7041002.7019042969, 666133.65850830078 7041002.3220825195, 666129.44927978516 7041001.1942749023, 666125.49969482422 7040999.3524780273, 666121.93011474609 7040996.8530883789, 666118.84869384766 7040993.7717285156, 666116.34912109375 7040990.2019042969, 666114.50750732422 7040986.2525024414, 666113.37951660156 7040982.04309082, 666112.99969482422 7040977.7019042969, 666113.37951660156 7040973.3607177734, 666114.50750732422 7040969.1514892578, 666116.34912109375 7040965.2019042969, 666118.84869384766 7040961.6323242188, 666121.93011474609 7040958.55090332, 666125.49969482422 7040956.0513305664, 666129.44927978516 7040954.2097167969, 666132.01867675781 7040953.5211181641, 666130.50109863281 7040950.2669067383, 666129.37329101563 7040946.0576782227, 666128.99353027344 7040941.7164916992, 666129.37329101563 7040937.3753051758, 666130.50109863281 7040933.1658935547, 666132.34289550781 7040929.2164916992, 666134.84228515625 7040925.6467285156, 666137.92370605469 7040922.5653076172, 666141.49353027344 7040920.0659179688, 666145.44287109375 7040918.2241210938, 666149.65228271484 7040917.0963134766, 666153.99353027344 7040916.7164916992, 666158.33471679688 7040917.0963134766)), "+
						"((665920.43029785156 7040965.07611084, 665924.63970947266 7040966.2041015625, 665928.58911132813 7040968.045715332, 665932.15887451172 7040970.5452880859, 665935.24029541016 7040973.6267089844, 665937.73968505859 7040977.1962890625, 665939.58148193359 7040981.1458740234, 665940.70928955078 7040985.3551025391, 665941.08911132813 7040989.6962890625, 665940.82208251953 7040992.7479248047, 665943.58288574219 7040994.03527832, 665947.15270996094 7040996.5347290039, 665950.23413085938 7040999.6160888672, 665952.73352050781 7041003.1859130859, 665954.57531738281 7041007.1353149414, 665955.703125 7041011.3447265625, 665956.08288574219 7041015.6859130859, 665955.703125 7041020.0270996094, 665954.57531738281 7041024.236328125, 665952.73352050781 7041028.1859130859, 665950.23413085938 7041031.7554931641, 665947.15270996094 7041034.8369140625, 665943.58288574219 7041037.3364868164, 665939.63348388672 7041039.1781005859, 665935.42407226563 7041040.3060913086, 665931.08288574219 7041040.6859130859, 665926.74169921875 7041040.3060913086, 665922.53247070313 7041039.1781005859, 665918.58288574219 7041037.3364868164, 665915.01330566406 7041034.8369140625, 665911.93188476563 7041031.7554931641, 665909.43231201172 7041028.1859130859, 665907.59069824219 7041024.236328125, 665906.46270751953 7041020.0270996094, 665906.08288574219 7041015.6859130859, 665906.34991455078 7041012.6342773438, 665903.58911132813 7041011.3469238281, 665900.01947021484 7041008.8474731445, 665896.93811035156 7041005.7661132813, 665894.4384765625 7041002.1962890625, 665892.59692382813 7040998.246887207, 665891.46887207031 7040994.0374755859, 665891.08911132813 7040989.6962890625, 665891.46887207031 7040985.3551025391, 665892.59692382813 7040981.1458740234, 665894.4384765625 7040977.1962890625, 665896.93811035156 7040973.6267089844, 665900.01947021484 7040970.5452880859, 665903.58911132813 7040968.045715332, 665907.53869628906 7040966.2041015625, 665911.74792480469 7040965.07611084, 665916.08911132813 7040964.6962890625, 665920.43029785156 7040965.07611084)), "+
						"((665996.05651855469 7041105.6599121094, 665992.05810546875 7041105.3101196289, 665988.05969238281 7041105.6599121094, 665983.71850585938 7041105.280090332, 665979.50927734375 7041104.1522827148, 665975.55969238281 7041102.31048584, 665971.99011230469 7041099.8110961914, 665968.90869140625 7041096.729675293, 665966.40911865234 7041093.1599121094, 665964.56750488281 7041089.2105102539, 665963.43951416016 7041085.0010986328, 665963.05969238281 7041080.6599121094, 665963.43951416016 7041076.3187255859, 665964.56750488281 7041072.10949707, 665966.40911865234 7041068.1599121094, 665968.90869140625 7041064.5902709961, 665970.36608886719 7041063.13269043, 665970.40692138672 7041062.6672973633, 665970.05712890625 7041058.6688842773, 665970.43688964844 7041054.3276977539, 665971.56469726563 7041050.1182861328, 665973.40649414063 7041046.1688842773, 665975.90588378906 7041042.5991210938, 665978.9873046875 7041039.5177001953, 665982.55712890625 7041037.0183105469, 665986.50653076172 7041035.1765136719, 665990.71588134766 7041034.0487060547, 665995.05712890625 7041033.6688842773, 665999.39831542969 7041034.0487060547, 666003.60748291016 7041035.1765136719, 666007.55712890625 7041037.0183105469, 666011.12670898438 7041039.5177001953, 666014.20812988281 7041042.5991210938, 666016.70770263672 7041046.1688842773, 666018.54931640625 7041050.1182861328, 666019.67730712891 7041054.3276977539, 666020.05712890625 7041058.6688842773, 666019.70727539063 7041062.6672973633, 666020.05712890625 7041066.6655273438, 666019.67730712891 7041071.0067138672, 666019.44268798828 7041071.8818969727, 666019.54888916016 7041072.10949707, 666020.67669677734 7041076.3187255859, 666021.05651855469 7041080.6599121094, 666020.67669677734 7041085.0010986328, 666019.54888916016 7041089.2105102539, 666017.70709228516 7041093.1599121094, 666015.20770263672 7041096.729675293, 666012.12628173828 7041099.8110961914, 666008.55651855469 7041102.31048584, 666004.60711669922 7041104.1522827148, 666000.39770507813 7041105.280090332, 665996.05651855469 7041105.6599121094)), "+
						"((664778.88891601563 7041357.9136962891, 664783.09808349609 7041359.0416870117, 664787.04772949219 7041360.8833007813, 664790.61730957031 7041363.3828735352, 664793.69873046875 7041366.4642944336, 664796.19830322266 7041370.0338745117, 664798.03991699219 7041373.9835205078, 664799.16790771484 7041378.1926879883, 664799.54772949219 7041382.5338745117, 664799.16790771484 7041386.87512207, 664798.03991699219 7041391.0844726563, 664796.19830322266 7041395.0338745117, 664793.69873046875 7041398.6036987305, 664790.61730957031 7041401.6851196289, 664787.04772949219 7041404.1845092773, 664783.09808349609 7041406.0263061523, 664782.52429199219 7041406.1801147461, 664782.55450439453 7041406.5242919922, 664782.17468261719 7041410.8654785156, 664781.04669189453 7041415.0747070313, 664779.205078125 7041419.0242919922, 664776.70550537109 7041422.59387207, 664773.62408447266 7041425.6752929688, 664770.05450439453 7041428.1749267578, 664766.10491943359 7041430.0164794922, 664761.895690918 7041431.1444702148, 664757.55450439453 7041431.5242919922, 664753.21331787109 7041431.1444702148, 664749.00390625 7041430.0164794922, 664745.05450439453 7041428.1749267578, 664741.48468017578 7041425.6752929688, 664738.4033203125 7041422.59387207, 664735.90393066406 7041419.0242919922, 664734.06207275391 7041415.0747070313, 664732.93432617188 7041410.8654785156, 664732.55450439453 7041406.5242919922, 664732.93432617188 7041402.1831054688, 664734.06207275391 7041397.9736938477, 664735.90393066406 7041394.0242919922, 664738.4033203125 7041390.4545288086, 664741.48468017578 7041387.37310791, 664745.05450439453 7041384.8737182617, 664749.00390625 7041383.0319213867, 664749.57769775391 7041382.878112793, 664749.54772949219 7041382.5338745117, 664749.92749023438 7041378.1926879883, 664751.05529785156 7041373.9835205078, 664752.89709472656 7041370.0338745117, 664755.396484375 7041366.4642944336, 664758.47790527344 7041363.3828735352, 664762.04772949219 7041360.8833007813, 664765.9970703125 7041359.0416870117, 664770.20648193359 7041357.9136962891, 664774.54772949219 7041357.5338745117, 664778.88891601563 7041357.9136962891)), "+
						"((666442.22113037109 7040696.1862792969, 666446.43029785156 7040697.3142700195, 666450.3798828125 7040699.1558837891, 666453.94952392578 7040701.6555175781, 666457.03088378906 7040704.7368774414, 666459.53051757813 7040708.3065185547, 666461.3720703125 7040712.2561035156, 666462.50012207031 7040716.4652709961, 666462.8798828125 7040720.8065185547, 666462.50012207031 7040725.1477050781, 666461.3720703125 7040729.3571166992, 666459.53051757813 7040733.3065185547, 666457.03088378906 7040736.8762817383, 666453.94952392578 7040739.9577026367, 666452.55950927734 7040740.9309082031, 666453.50347900391 7040744.4541015625, 666453.88330078125 7040748.7952880859, 666453.50347900391 7040753.1364746094, 666452.37567138672 7040757.345703125, 666450.53387451172 7040761.2952880859, 666448.03448486328 7040764.8649291992, 666444.953125 7040767.9462890625, 666441.38330078125 7040770.4459228516, 666437.43389892578 7040772.2874755859, 666433.22448730469 7040773.4155273438, 666428.88330078125 7040773.7952880859, 666424.54211425781 7040773.4155273438, 666420.33288574219 7040772.2874755859, 666416.38330078125 7040770.4459228516, 666412.81372070313 7040767.9462890625, 666409.73229980469 7040764.8649291992, 666407.23272705078 7040761.2952880859, 666405.39111328125 7040757.345703125, 666404.26312255859 7040753.1364746094, 666403.88330078125 7040748.7952880859, 666404.26312255859 7040744.4541015625, 666405.39111328125 7040740.2446899414, 666407.23272705078 7040736.2952880859, 666409.73229980469 7040732.7255249023, 666412.81372070313 7040729.6441040039, 666414.20367431641 7040728.6708984375, 666413.25970458984 7040725.1477050781, 666412.8798828125 7040720.8065185547, 666413.25970458984 7040716.4652709961, 666414.387512207 7040712.2561035156, 666416.229309082 7040708.3065185547, 666418.72869873047 7040704.7368774414, 666421.81011962891 7040701.6555175781, 666425.3798828125 7040699.1558837891, 666429.329284668 7040697.3142700195, 666433.53869628906 7040696.1862792969, 666437.8798828125 7040695.8065185547, 666442.22113037109 7040696.1862792969)),"+
						"((666078.36828613281 7040535.24987793, 666082.57751464844 7040536.3776855469, 666086.52709960938 7040538.2194824219, 666090.0966796875 7040540.71887207, 666093.17810058594 7040543.8002929688, 666095.67767333984 7040547.3701171875, 666097.51928710938 7040551.319519043, 666098.647277832 7040555.5289306641, 666099.02709960938 7040559.8701171875, 666098.647277832 7040564.2113037109, 666097.51928710938 7040568.4204711914, 666095.67767333984 7040572.3701171875, 666095.47607421875 7040572.6580810547, 666094.67810058594 7040574.3693237305, 666092.178527832 7040577.9389038086, 666089.09710693359 7040581.020324707, 666085.52752685547 7040583.5198974609, 666081.57788085938 7040585.3615112305, 666080.93707275391 7040585.5333251953, 666079.52990722656 7040586.5186767578, 666075.58032226563 7040588.3602905273, 666071.37109375 7040589.48828125, 666067.02990722656 7040589.8681030273, 666062.68872070313 7040589.48828125, 666058.479309082 7040588.3602905273, 666054.52990722656 7040586.5186767578, 666050.96008300781 7040584.0191040039, 666047.87872314453 7040580.9376831055, 666045.37927246094 7040577.3681030273, 666043.53747558594 7040573.4185180664, 666042.40972900391 7040569.2092895508, 666042.02990722656 7040564.8681030273, 666042.40972900391 7040560.5269165039, 666043.53747558594 7040556.3175048828, 666045.37927246094 7040552.3681030273, 666047.87872314453 7040548.7982788086, 666050.96008300781 7040545.7169189453, 666054.52990722656 7040543.2175292969, 666056.2705078125 7040542.4057006836, 666057.95727539063 7040540.71887207, 666061.52709960938 7040538.2194824219, 666065.47650146484 7040536.3776855469, 666069.68591308594 7040535.24987793, 666074.02709960938 7040534.8701171875, 666078.36828613281 7040535.24987793)), "+
						"((665869.45111083984 7040910.0980834961, 665873.66027832031 7040911.2258911133, 665877.60992431641 7040913.0676879883, 665881.17950439453 7040915.5670776367, 665884.260925293 7040918.6484985352, 665886.76049804688 7040922.2183227539, 665888.60211181641 7040926.1677246094, 665889.73010253906 7040930.3770751953, 665890.10992431641 7040934.7183227539, 665889.73010253906 7040939.0595092773, 665888.60211181641 7040943.2686767578, 665886.76049804688 7040947.2183227539, 665884.260925293 7040950.787902832, 665881.17950439453 7040953.8693237305, 665877.60992431641 7040956.3688964844, 665873.66027832031 7040958.2105102539, 665869.45111083984 7040959.3385009766, 665865.10992431641 7040959.7183227539, 665860.76867675781 7040959.3385009766, 665856.55932617188 7040958.2105102539, 665852.60992431641 7040956.3688964844, 665849.04010009766 7040953.8693237305, 665845.95867919922 7040950.787902832, 665843.45928955078 7040947.2183227539, 665841.61749267578 7040943.2686767578, 665840.48968505859 7040939.0595092773, 665840.10992431641 7040934.7183227539, 665840.48968505859 7040930.3770751953, 665841.61749267578 7040926.1677246094, 665843.45928955078 7040922.2183227539, 665845.95867919922 7040918.6484985352, 665849.04010009766 7040915.5670776367, 665852.60992431641 7040913.0676879883, 665856.55932617188 7040911.2258911133, 665860.76867675781 7040910.0980834961, 665865.10992431641 7040909.7183227539, 665869.45111083984 7040910.0980834961)) "+
						")"
				);
		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(null, g.getCoordinates());
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("" +
				"[POLYGON ((664584 7040535, 664584 7041454, 666463 7041454, 666463 7040535, 664584 7040535))]",
				g.getGeo().getWKTList().toString());
	}

	@Test
	public void accurateArea() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.occurrenceWKT = Utils.list(
				"POLYGON((607328.0 7361552.0,607903.0 7361359.0,607696.0 7361103.0,607167.0 7361312.0,607328.0 7361552.0))"
				);
		data.json.setString("coordinateAccuracy", "51"); // Set coordinate accuracy to 51 --> täsmällisesti määritelty havaintoalue

		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		Gathering g = d.getGatherings().get(0);

		assertEquals(
				"[POLYGON ((607328 7361552, 607903 7361359, 607696 7361103, 607167 7361312, 607328 7361552))]",
				g.getGeo().getWKTList().toString());
		assertEquals(true, g.getAccurateArea());
		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals(1, g.getGeo().getAccuracyInMeters().intValue());
	}
	
	@Test
	public void accurateLine() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 456, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.occurrenceWKT = Utils.list(
				"LINESTRING(333219 6659938, 332919 6660186, 332919 6660342, 332751 6660502, 332647 6660766, 332487 6660762, 332435 6660690, 332215 6660886, 332095 6660930, 331979 6661074, 331759 6661050)"
				);
		data.json.setString("coordinateAccuracy", "42"); // Set coordinate accuracy to 42 --> täsmällisesti määritelty linja/viiva

		DwRoot root = harmonizer.harmonize(new EventOccurrences(123, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		Gathering g = d.getGatherings().get(0);

		assertEquals(
				"[LINESTRING (333219 6659938, 332919 6660186, 332919 6660342, 332751 6660502, 332647 6660766, 332487 6660762, 332435 6660690, 332215 6660886, 332095 6660930, 331979 6661074, 331759 6661050)]",
				g.getGeo().getWKTList().toString());
		assertEquals(true, g.getAccurateArea());
		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals(1, g.getGeo().getAccuracyInMeters().intValue());
	}

	@Test
	public void multi_features_and_geo_collection() throws Exception {
		OccurrenceData data = new OccurrenceData(null, 1, RdfXmlHarmonizerTests.getTestData("lajigis-1.json"));
		data.occurrenceWKT = Utils.list(
				"GEOMETRYCOLLECTION(POLYGON ((420173.30670166016 7243294.205871582, 420254.8103027344 7243186.280883789, 420438.4514770508 7242983.933288574, 420514.18927001953 7242970.702087402, 420602.01751708984 7242935.114501953, 420644.4489135742 7242805.310913086, 420685.9678955078 7242714.516906738, 420693.72412109375 7242704.023071289, 420693.49591064453 7242703.7951049805, 420683.91467285156 7242705.6201171875, 420659.2770996094 7242717.938903809, 420660.64587402344 7242723.8701171875, 420660.1896972656 7242741.207702637, 420619.1270751953 7242756.263916016, 420615.4771118164 7242768.582702637, 420626.42712402344 7242778.164123535, 420628.7083129883 7242802.801513672, 420607.720703125 7242843.864318848, 420563.4642944336 7242921.426879883, 420527.4204711914 7242964.31451416, 420459.89532470703 7242970.2459106445, 420401.03887939453 7242983.020874023, 420372.29510498047 7243014.0458984375, 420349.02630615234 7243031.83972168, 420328.03869628906 7243054.652282715, 420315.263671875 7243077.464904785, 420291.31048583984 7243096.171325684, 420275.79791259766 7243129.477478027, 420242.0352783203 7243186.05267334, 420220.1353149414 7243219.815307617, 420186.37268066406 7243262.474914551, 420174.96630859375 7243291.218688965, 420173.30670166016 7243294.205871582)), POLYGON ((420134.57830810547 7243451.963928223, 420135.72869873047 7243454.100280762, 420110.6348876953 7243490.600524902, 420154.4351196289 7243559.038330078, 420174.96630859375 7243522.08190918, 420134.57830810547 7243451.963928223)), POLYGON ((420125.69470214844 7243436.059692383, 420126.0805053711 7243384.755310059, 420102.4223022461 7243386.575073242, 420101.0537109375 7243422.61907959, 420125.69470214844 7243436.059692383)), POLYGON ((420138.70111083984 7243353.9470825195, 420167.3668823242 7243303.925292969, 420133.90368652344 7243321.103088379, 420138.70111083984 7243353.9470825195)), POLYGON ((420126.0805053711 7243384.755310059, 420126.14752197266 7243384.75012207, 420139.83489990234 7243361.709472656, 420138.70111083984 7243353.9470825195, 420126.14752197266 7243375.853271484, 420126.0805053711 7243384.755310059)), POLYGON ((420125.69470214844 7243436.059692383, 420125.6911010742 7243436.534729004, 420134.57830810547 7243451.963928223, 420126.14752197266 7243436.306518555, 420125.69470214844 7243436.059692383)), POLYGON ((420167.3668823242 7243303.925292969, 420168.1224975586 7243303.537475586, 420173.30670166016 7243294.205871582, 420171.77252197266 7243296.237487793, 420167.3668823242 7243303.925292969)))",
				"POLYGON ((419731.30291748047 7244060.6829223633, 419690.5615234375 7243965.6196899414, 419696.11712646484 7243934.7548828125, 419689.32690429688 7243915.0015258789, 419665.86968994141 7243889.6925048828, 419661.54870605469 7243855.7412719727, 419663.40051269531 7243841.5435180664, 419676.98107910156 7243834.135925293, 419697.96911621094 7243813.7653198242, 419769.57531738281 7243788.4561157227, 419792.41510009766 7243747.40612793, 419794.88427734375 7243721.479675293, 419843.03332519531 7243633.2067260742, 419788.09411621094 7243528.8839111328, 419785.00750732422 7243499.25390625, 419825.13171386719 7243441.8455200195, 419887.478515625 7243451.1049194336, 419899.82427978516 7243448.0183105469, 419925.13330078125 7243465.919921875, 419954.76348876953 7243466.5372924805, 419976.36871337891 7243488.14251709, 420042.41931152344 7243477.6484985352, 420070.81469726563 7243464.0681152344, 420110.32171630859 7243491.2288818359, 420154.76690673828 7243559.7487182617, 420103.22271728516 7243655.4293212891, 419990.87512207031 7243826.4196777344, 419968.65252685547 7243896.7913208008, 419864.32989501953 7243975.4962768555, 419790.87188720703 7244045.8679199219, 419747.66131591797 7244069.9423217773, 419731.30291748047 7244060.6829223633))"
				);
		DwRoot root = harmonizer.harmonize(new EventOccurrences(1, Utils.list(data)), SOURCE).get(0);
		Document d = root.getPrivateDocument();
		Gathering g = d.getGatherings().get(0);

		assertEquals(null, g.getQuality());
		assertEquals(
				"[POLYGON ((420173.30670166016 7243294.205871582, 420254.8103027344 7243186.280883789, 420438.4514770508 7242983.933288574, 420514.18927001953 7242970.702087402, 420602.01751708984 7242935.114501953, 420644.4489135742 7242805.310913086, 420685.9678955078 7242714.516906738, 420693.72412109375 7242704.023071289, 420693.49591064453 7242703.7951049805, 420683.91467285156 7242705.6201171875, 420659.2770996094 7242717.938903809, 420660.64587402344 7242723.8701171875, 420660.1896972656 7242741.207702637, 420619.1270751953 7242756.263916016, 420615.4771118164 7242768.582702637, 420626.42712402344 7242778.164123535, 420628.7083129883 7242802.801513672, 420607.720703125 7242843.864318848, 420563.4642944336 7242921.426879883, 420527.4204711914 7242964.31451416, 420459.89532470703 7242970.2459106445, 420401.03887939453 7242983.020874023, 420372.29510498047 7243014.0458984375, 420349.02630615234 7243031.83972168, 420328.03869628906 7243054.652282715, 420315.263671875 7243077.464904785, 420291.31048583984 7243096.171325684, 420275.79791259766 7243129.477478027, 420242.0352783203 7243186.05267334, 420220.1353149414 7243219.815307617, 420186.37268066406 7243262.474914551, 420174.96630859375 7243291.218688965, 420173.30670166016 7243294.205871582)), POLYGON ((420134.57830810547 7243451.963928223, 420135.72869873047 7243454.100280762, 420110.6348876953 7243490.600524902, 420154.4351196289 7243559.038330078, 420174.96630859375 7243522.08190918, 420134.57830810547 7243451.963928223)), POLYGON ((420125.69470214844 7243436.059692383, 420126.0805053711 7243384.755310059, 420102.4223022461 7243386.575073242, 420101.0537109375 7243422.61907959, 420125.69470214844 7243436.059692383)), POLYGON ((420138.70111083984 7243353.9470825195, 420167.3668823242 7243303.925292969, 420133.90368652344 7243321.103088379, 420138.70111083984 7243353.9470825195)), POLYGON ((420126.0805053711 7243384.755310059, 420126.14752197266 7243384.75012207, 420139.83489990234 7243361.709472656, 420138.70111083984 7243353.9470825195, 420126.14752197266 7243375.853271484, 420126.0805053711 7243384.755310059)), POLYGON ((420125.69470214844 7243436.059692383, 420125.6911010742 7243436.534729004, 420134.57830810547 7243451.963928223, 420126.14752197266 7243436.306518555, 420125.69470214844 7243436.059692383)), POLYGON ((420167.3668823242 7243303.925292969, 420168.1224975586 7243303.537475586, 420173.30670166016 7243294.205871582, 420171.77252197266 7243296.237487793, 420167.3668823242 7243303.925292969)), POLYGON ((419731.30291748047 7244060.682922363, 419747.66131591797 7244069.942321777, 419790.87188720703 7244045.867919922, 419864.32989501953 7243975.4962768555, 419968.65252685547 7243896.791320801, 419990.8751220703 7243826.419677734, 420103.22271728516 7243655.429321289, 420154.7669067383 7243559.748718262, 420110.3217163086 7243491.228881836, 420070.8146972656 7243464.068115234, 420042.41931152344 7243477.648498535, 419976.3687133789 7243488.14251709, 419954.76348876953 7243466.5372924805, 419925.13330078125 7243465.919921875, 419899.82427978516 7243448.018310547, 419887.478515625 7243451.104919434, 419825.1317138672 7243441.8455200195, 419785.0075073242 7243499.25390625, 419788.09411621094 7243528.883911133, 419843.0333251953 7243633.206726074, 419794.88427734375 7243721.479675293, 419792.41510009766 7243747.40612793, 419769.5753173828 7243788.456115723, 419697.96911621094 7243813.765319824, 419676.98107910156 7243834.135925293, 419663.4005126953 7243841.543518066, 419661.5487060547 7243855.741271973, 419665.8696899414 7243889.692504883, 419689.3269042969 7243915.001525879, 419696.11712646484 7243934.7548828125, 419690.5615234375 7243965.619689941, 419731.30291748047 7244060.682922363))]",
				g.getGeo().getWKTList().toString());
	}

}
