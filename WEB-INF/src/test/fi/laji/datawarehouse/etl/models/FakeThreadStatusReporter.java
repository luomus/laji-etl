package fi.laji.datawarehouse.etl.models;

import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;

public class FakeThreadStatusReporter extends ThreadStatusReporter {
	@Override
	public void setStatus(String status) {
		System.out.println(status);
	}
}