package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class SykeZoobenthosHarmonizerTests {

	private SykeZoobenthosHarmonizer harmonizer;

	@Before
	public void setUp() {
		harmonizer = new SykeZoobenthosHarmonizer();
	}

	@Test
	public void test() throws CriticalParseFailure, UnknownHarmonizingFailure {
		JSONObject data = new JSONObject(RdfXmlHarmonizerTests.getTestData("syke-zoobenthos-1.json"));
		List<DwRoot> roots = harmonizer.harmonize(data, SykeZoobenthosPullReader.SOURCE);
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertNull(root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals("KE.941/Sampling_17179", d.getDocumentId().toString());
		assertEquals("HR.3391", d.getCollectionId().toString());
		assertEquals("2018-04-30", DateUtils.format(d.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals("2018-04-30", DateUtils.format(d.getModifiedDate(), "yyyy-MM-dd"));

		String expectedFacts = "" +
				"[type : stationVisit, Bottom type : hard bottom, habitat : River, minimumDepthInMeters : 0.1, maximumDepthInMeters : 0.2, "+
				"benthosStationID : 7639, observationAreaID : 896, " +
				"observationAreaName : Litojoki alapää, name : Litojoki alapää_h, municipality : Pudasjärvi, vegetationTypeName : water moss, " +
				"siteTypeName : Rapid with sandy/silty/clay bottom, close on shore, slow currents, usually with some silt/detritus, "+
				"environmentTypeName : River, waterBasinCode : 61.418, waterManagementAreaCode : VHA4, waterManagementAreaName : Oulujoen-Iijoen vesienhoitoalue, " +
				"waterBodyCode : 61.418_001, waterBodyName : Litojoki, surfaceWaterMonitoringStationID : SW_3871, surfaceWaterMonitoringStationName : Litojoki alapää]";
		assertEquals(expectedFacts, d.getFacts().toString());

		assertEquals("http://tun.fi/KE.941/BenthosStation/7639", d.getNamedPlaceId());
		assertEquals("Average of 2", d.getNotes());
		assertEquals(null, d.getQuality());
		assertEquals(SecureLevel.NONE, d.getSecureLevel());
		assertEquals(0, d.getSecureReasons().size());
		assertEquals(SykeZoobenthosPullReader.SOURCE, d.getSourceId());

		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		expectedFacts = "[Sampling instrument name : Handnet, Lower sieve mesh size (Millimetres) : 0.5]";
		assertEquals(expectedFacts, g.getFacts().toString());
		assertEquals("65.51785 : 65.51785 : 26.38779 : 26.38779 : WGS84 : null", g.getCoordinates().toString());
		assertEquals("DateRange [begin=2013-09-24, end=2013-09-24]", g.getEventDate().toString());
		assertEquals("2013-09-24 [8:30]", g.getDisplayDateTime());
		assertEquals(null, g.getQuality());
		assertEquals("Litojoki alapää", g.getLocality());
		assertEquals("Pudasjärvi", g.getMunicipality());
		assertEquals("KE.941/Sampling_17179-Sample_36712-Sieve_05mm", g.getGatheringId().toString());

		assertEquals(3, g.getUnits().size());
		Unit u1 = g.getUnits().get(0);
		Unit u2 = g.getUnits().get(1);
		Unit u3 = g.getUnits().get(2);

		assertEquals("MX.253681", u1.getReportedTaxonId().toString());
		assertEquals("Chironomidae", u1.getTaxonVerbatim());
		assertEquals("Macquart, 1838", u1.getAuthor());
		assertEquals("present", u1.getAbundanceString());
		assertEquals("[scientificNameID : urn:lsid:marinespecies.org:taxname:118100, Count (in assayed sample) of biological entity specified elsewhere. : 19]", u1.getFacts().toString());
		assertEquals("KE.941/Sampling_17179-Sample_36712-Sieve_05mm-Taxon_1030", u1.getUnitId().toString());
		assertEquals(LifeStage.LARVA, u1.getLifeStage());
		assertEquals(RecordBasis.MATERIAL_SAMPLE, u1.getRecordBasis());

		assertEquals("MX.231766", u2.getReportedTaxonId().toString());
		assertEquals("Oxyethira", u2.getTaxonVerbatim());
		assertEquals("Eaton, 1873", u2.getAuthor());
		assertEquals("present", u2.getAbundanceString());
		assertEquals("[scientificNameID : urn:lsid:marinespecies.org:taxname:989860, Count (in assayed sample) of biological entity specified elsewhere. : 1]", u2.getFacts().toString());
		assertEquals("KE.941/Sampling_17179-Sample_36712-Sieve_05mm-Taxon_97", u2.getUnitId().toString());

		assertEquals(null, u3.getReportedTaxonId());
		assertEquals("Animalia", u3.getTaxonVerbatim());
		assertEquals(null, u3.getAuthor());
		assertEquals("absent", u3.getAbundanceString());
		assertEquals("[scientificNameID : urn:lsid:marinespecies.org:taxname:2]", u3.getFacts().toString());
		assertEquals("KE.941/Sampling_17179-Sample_36712-Sieve_05mm-Taxon_0", u3.getUnitId().toString());
	}

}
