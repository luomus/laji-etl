package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.XMLReader;

public class SimpleDarwinRecordSetHarmonizerTests {

	@Test
	public void parse() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();

		String data = RdfXmlHarmonizerTests.getTestData("dwc-data1.xml");
		List<DwRoot> harmonized = harmonizer.harmonize(parse(data), new Qname("KE.1"));
		assertEquals(1, harmonized.size());

		DwRoot root = harmonized.get(0);
		System.out.println(root.toJSON());
		assertEquals("http://tun.fi/HR.200", root.getCollectionId().toURI());
		assertEquals(SecureLevel.KM100, root.getPublicDocument().getSecureLevel());
	}

	@Test
	public void parse_virtala_securelevel() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();

		String data = RdfXmlHarmonizerTests.getTestData("dwc-data1.xml");
		List<DwRoot> harmonized = harmonizer.harmonize(parse(data), SimpleDarwinRecordSetHarmonizer.INSECT_DATABASE_VIRTALA);
		assertEquals(1, harmonized.size());

		DwRoot root = harmonized.get(0);
		System.out.println(root.toJSON());
		assertEquals("http://tun.fi/HR.200", root.getCollectionId().toURI());
		assertEquals(SecureLevel.KM10, root.getPublicDocument().getSecureLevel());
	}

	private Document parse(String data) {
		return new XMLReader().parse(data);
	}

	@Test
	public void parse_weird_documentid() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();
		String data = RdfXmlHarmonizerTests.getTestData("dwc-data-virtala-occurrenceid.xml");
		List<DwRoot> harmonized = harmonizer.harmonize(parse(data), new Qname("KE.421"));
		assertEquals(1, harmonized.size());

		DwRoot root = harmonized.get(0);
		assertEquals("http://tun.fi/KE.421/DABUH:SEMF:130677", root.getDocumentId().toURI());
	}

	@Test
	public void parse_uknown_source_to_collection_mapping() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();
		String data = RdfXmlHarmonizerTests.getTestData("dwc-data-virtala-occurrenceid.xml");

		try {
			harmonizer.harmonize(parse(data), new Qname("KE.123456"));
			fail();
		} catch (CriticalParseFailure e) {
			assertEquals("Unknown dwc-source: KE.123456", e.getMessage());
		}
	}

	@Test
	public void parse_luke_dwc() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();
		String data = RdfXmlHarmonizerTests.getTestData("dwc-data-luke.xml");
		List<DwRoot> roots = harmonizer.harmonize(parse(data), new Qname("KE.621"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		assertEquals(null, root.getAnnotations());
		fi.laji.datawarehouse.etl.models.dw.Document doc = root.getPublicDocument();
		assertEquals("HR.2009", doc.getCollectionId().toString());
		assertEquals("http://tun.fi/KE.621/6103", doc.getDocumentId().toURI());
		assertEquals(null, doc.getCreatedDate());
		assertEquals(null, doc.getModifiedDate());

		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);

		assertEquals(null, g.getEventDate());
		assertEquals("Finland", g.getCountry());
		assertEquals("Porvoo", g.getMunicipality());
		assertEquals("Kalapaikka, josta blaa", g.getLocality());
		assertEquals(null, g.getNotes());
		assertEquals("60.21523880035999, 25.53692289713584 WGS84", g.getCoordinatesVerbatim());
		assertEquals("60.215239 : 60.215239 : 25.536923 : 25.536923 : WGS84 : null", g.getCoordinates().toString());

		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);

		assertEquals("Neogobius melanostomus", u.getTaxonVerbatim());
		assertEquals("2", u.getAbundanceString());
		assertEquals("Muu kommentti\nKala oli tuntematon mutta tuntomerkit sopivat mustatäplätokkoon (YLEn uutinen 16.6.)", u.getNotes());

	}

	@Test
	public void parse_luke_dwc_2() throws Exception {
		SimpleDarwinRecordSetHarmonizer harmonizer = new SimpleDarwinRecordSetHarmonizer();
		String data = RdfXmlHarmonizerTests.getTestData("dwc-data-luke2.xml");
		List<DwRoot> roots = harmonizer.harmonize(parse(data), new Qname("KE.621"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		assertEquals(null, root.getAnnotations());
		fi.laji.datawarehouse.etl.models.dw.Document doc = root.getPublicDocument();
		assertEquals("HR.2009", doc.getCollectionId().toString());
		assertEquals("http://tun.fi/KE.621/189385", doc.getDocumentId().toURI());
		assertEquals("2018-03-22", DateUtils.format(doc.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals("2018-03-22", DateUtils.format(doc.getModifiedDate(), "yyyy-MM-dd"));

		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);

		assertEquals("DateRange [begin=2018-03-22, end=2018-03-22]", g.getEventDate().toString());
		assertEquals("Finland", g.getCountry());
		assertEquals("Lohja", g.getMunicipality());
		assertEquals(null, g.getLocality());
		assertEquals(null, g.getNotes());
		assertEquals("60.25998410383153, 23.860537956546526 WGS84", g.getCoordinatesVerbatim());
		assertEquals("60.259984 : 60.259984 : 23.860538 : 23.860538 : WGS84 : null", g.getCoordinates().toString());

		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);

		assertEquals(null, u.getTaxonVerbatim());
		assertEquals(null, u.getAbundanceString());
		assertEquals(null, u.getNotes());
	}

}

