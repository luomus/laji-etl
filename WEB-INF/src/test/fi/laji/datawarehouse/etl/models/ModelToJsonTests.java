package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.DocumentDWLinkings.CollectionQuality;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Identification;
import fi.laji.datawarehouse.etl.models.dw.IdentificationDwLinkings;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.ObsCount;
import fi.laji.datawarehouse.etl.models.dw.OccurrenceAtTimeOfAnnotation;
import fi.laji.datawarehouse.etl.models.dw.Person;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.UnitDWLinkings;
import fi.laji.datawarehouse.etl.models.dw.UnitInterpretations.RecordQuality;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.dw.geo.Line;
import fi.laji.datawarehouse.etl.models.dw.geo.Point;
import fi.laji.datawarehouse.etl.models.dw.geo.Polygon;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.laji.datawarehouse.query.download.model.DownloadRequest;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadFormat;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadInclude;
import fi.laji.datawarehouse.query.download.model.DownloadRequest.DownloadType;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON;
import fi.laji.datawarehouse.query.download.util.DownloadRequestToJSON.CollectionAndReasons;
import fi.laji.datawarehouse.query.model.CollectionAndRecordQuality;
import fi.laji.datawarehouse.query.model.CoordinatesWithOverlapRatio;
import fi.laji.datawarehouse.query.model.PolygonSearch;
import fi.laji.datawarehouse.query.model.queries.BaseQuery;
import fi.laji.datawarehouse.query.model.queries.BaseQueryBuilder;
import fi.laji.datawarehouse.query.model.queries.Selected;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.java.tests.commons.taxonomy.TaxonContainerStub;

public class ModelToJsonTests {

	private static TestDAO dao;

	@BeforeClass
	public static void init() {
		dao = new TestDAO(ModelToJsonTests.class);
	}

	@AfterClass
	public static void close() {
		if (dao != null) dao.close();
	}

	@Test
	public void test() throws Exception {
		DwRoot root = generateTestData();
		JSONObject json = root.toJSON();

		System.out.println(json.beautify());

		assertEquals("laji-etl", json.getString("schema"));

		assertEquals(new Qname("12345").toURI(), json.getString("documentId"));
		assertEquals("http://tun.fi/KE.3", json.getString("sourceId"));
		JSONObject privateDocument = json.getObject("privateDocument");
		assertEquals(false, privateDocument.getBoolean("public"));
		JSONObject publicDocument = json.getObject("publicDocument");
		assertEquals(true, publicDocument.getBoolean("public"));

		assertEquals("KM100", publicDocument.getString("secureLevel"));
		assertEquals("[\"CUSTOM\"]", publicDocument.getArray("secureReasons").toString());
		assertEquals(true, publicDocument.getBoolean("secured"));
		assertEquals("NONE", privateDocument.getString("secureLevel"));
		assertEquals("[]", privateDocument.getArray("secureReasons").toString());
		assertEquals(false, privateDocument.getBoolean("secured"));

		assertEquals(2, privateDocument.getArray("editorUserIds").size());
		assertEquals("editor 1", privateDocument.getArray("editorUserIds").iterator().next());

		assertEquals(2, privateDocument.getArray("gatherings").size());
		JSONObject gathering1 = privateDocument.getArray("gatherings").iterateAsObject().get(0);
		JSONObject gathering2 = privateDocument.getArray("gatherings").iterateAsObject().get(1);

		assertEquals("gathering 2 notes", gathering2.getString("notes"));

		assertEquals(2, gathering1.getArray("team").size());
		assertEquals(0, gathering2.getArray("team").size());

		assertEquals(2, gathering1.getArray("facts").size());
		assertEquals("fact 2", gathering1.getArray("facts").iterateAsObject().get(1).getString("fact"));
		assertEquals("fact 2 value", gathering1.getArray("facts").iterateAsObject().get(1).getString("value"));

		assertEquals("locality value", gathering1.getString("locality"));

		assertEquals("2010-05-01", gathering1.getObject("eventDate").getString("begin"));
		assertEquals("2011-06-30", gathering1.getObject("eventDate").getString("end"));

		JSONObject unit = gathering1.getArray("units").iterateAsObject().get(0);
		assertEquals("http://tun.fi/u1id", unit.getString("unitId"));
		assertEquals("unit fact 1", unit.getArray("facts").iterateAsObject().get(0).getString("fact"));
		assertEquals("fact 1 value", unit.getArray("facts").iterateAsObject().get(0).getString("value"));
		assertEquals("1", unit.getString("abundanceString"));
		assertEquals("ADULT", unit.getString("lifeStage"));
		assertEquals(true, unit.getBoolean("typeSpecimen"));

		assertEquals(2, unit.getArray("media").iterateAsObject().size());
		assertEquals("{\"author\":\"auth\",\"fullURL\":\"https://image.com/1\",\"keywords\":[\"i1keyword\"],\"licenseId\":\"http://tun.fi/license\",\"mediaType\":\"IMAGE\",\"thumbnailURL\":\"https://image.com/1_thum\"}",
				unit.getArray("media").iterateAsObject().get(0).toString());

		assertEquals(1, unit.getArray("annotations").size());
		assertEquals(new Qname("MAN.1").toURI(), unit.getArray("annotations").iterateAsObject().get(0).getString("id"));

		assertEquals(0, gathering1.getInteger("gatheringOrder"));
		assertEquals(1, gathering2.getInteger("gatheringOrder"));
		assertEquals(0, unit.getInteger("unitOrder"));

		assertEquals("" +
				"{\"alternativeId\":\"altid\",\"birdAssociationAreaDisplayName\":\"Tringa\",\"birdAssociationAreaId\":\"http://tun.fi/ML.123\",\"collectionId\":\"http://tun.fi/HR.1\",\"id\":\"http://tun.fi/NMP.1\",\"municipalityDisplayName\":\"Helsinki\",\"municipalityId\":\"http://tun.fi/456\",\"name\":\"placename\",\"tags\":[\"http://tun.fi/nptag1\",\"http://tun.fi/nptag2\"],\"tagsString\":\"|nptag1|nptag2|\",\"wgs84WKT\":\"POINT(28.201646 64.678066)\",\"ykj10km\":{\"lat\":666,\"lon\":333,\"type\":\"YKJ\"}}",
				privateDocument.getObject("namedPlace").toString());

		assertEquals("https://inat.example/123", privateDocument.getString("referenceURL"));
		assertEquals(3, unit.getInteger("externalMediaCount"));

		assertEquals("http://tun.fi/MY.somemethod", unit.getString("samplingMethod"));
		assertEquals("[http://tun.fi/MY.otherbasis, http://tun.fi/MY.somebasis]", unit.getArray("identificationBasis").iterateAsString().toString());
	}

	private DwRoot generateTestData() throws Exception {
		DwRoot root = new DwRoot(new Qname("12345"), new Qname("KE.3"));
		root.setCollectionId(new Qname("HR.1"));
		Document privateData = root.createPrivateDocument();

		privateData.addEditorUserId("editor 1");
		privateData.addEditorUserId("editor 2");

		privateData.setReferenceURL("https://inat.example/123");

		Gathering gathering1 = new Gathering(new Qname("gid"));
		gathering1.setCoordinates(new Coordinates(60.123, 30.123, Type.WGS84));
		gathering1.addTeamMember("agent 1");
		gathering1.addTeamMember("agent 2");
		gathering1.addFact("fact 1", "fact 1 value");
		gathering1.addFact("fact 2", "fact 2 value");
		gathering1.setLocality("locality value");
		gathering1.setEventDate(new DateRange(DateUtils.convertToDate("01.05.2010"), DateUtils.convertToDate("30.06.2011")));
		gathering1.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_DATE, Quality.Source.ORIGINAL_DOCUMENT, "blaa"));
		privateData.addGathering(gathering1);

		Unit unit1 = new Unit(new Qname("u1id"));
		unit1.addFact("unit fact 1", "fact 1 value");
		unit1.setAbundanceString("1");
		unit1.setLifeStage(LifeStage.ADULT);
		unit1.setTypeSpecimen(true);
		unit1.setExternalMediaCount(3);
		unit1.addIdentificationBasis(new Qname("MY.somebasis")).addIdentificationBasis(new Qname("MY.otherbasis"));
		unit1.setSamplingMethodUsingQname(new Qname("MY.somemethod"));

		MediaObject m1 = new MediaObject(MediaType.IMAGE, "http://image.com/1");
		m1.setAuthor("auth");
		m1.setThumbnailURL("http://image.com/1_thum");
		m1.setLicenseIdUsingQname(new Qname("license"));
		m1.addKeyword("i1keyword");

		MediaObject m2 = new MediaObject(MediaType.AUDIO);
		m2.setWavURL("http://audio.com/1.wav");
		m2.setMp3URL("http://audio.com/1.mp3");
		unit1.addMedia(m1);
		unit1.addMedia(m2);

		gathering1.addUnit(unit1);

		Gathering gathering2 = new Gathering(new Qname("gid"));
		gathering2.setNotes("gathering 2 notes");
		privateData.addGathering(gathering2);

		Document publicData = privateData.concealPublicData(SecureLevel.KM100, Utils.set(SecureReason.CUSTOM));
		root.setPublicDocument(publicData);

		Document splittedDoc = new Document(Concealment.PUBLIC, new Qname("KE.3"), new Qname("A.ABC"), new Qname("HR.1"));
		Gathering gathering3 = new Gathering(new Qname("A.ABC#G"));
		Unit unit2 = new Unit(new Qname("A.ABC#U"));
		splittedDoc.setSecureLevel(SecureLevel.KM10);
		splittedDoc.addSecureReason(SecureReason.DEFAULT_TAXON_CONSERVATION);
		splittedDoc.addGathering(gathering3);
		gathering3.setLocality("splitted local");
		gathering3.addUnit(unit2);
		unit2.setTaxonVerbatim("splitted unit");
		root.addSplittedPublicDocument(splittedDoc);

		NamedPlaceEntity namedPlace = new NamedPlaceEntity(new Qname("NMP.1").toURI());
		namedPlace.addTag(new Qname("nptag1"));
		namedPlace.addTag(new Qname("nptag2"));
		namedPlace.setAlternativeId("altid");
		namedPlace.setBirdAssociationAreaDisplayName("Tringa");
		namedPlace.setBirdAssociationAreaId(new Qname("ML.123").toURI());
		namedPlace.setCollectionId(new Qname("HR.1").toURI());
		namedPlace.setMunicipalityDisplayName("Helsinki");
		namedPlace.setMunicipalityId(new Qname("456").toURI());
		namedPlace.setName("placename");
		namedPlace.setWgs84WKT("POINT(28.201646 64.678066)");
		namedPlace.setYkj10km(new SingleCoordinates(666, 333, Type.YKJ));
		privateData.setNamedPlace(namedPlace);

		unit1.addAnnotation(new Annotation(new Qname("MAN.1"), privateData.getDocumentId(), unit1.getUnitId(), Util.toTimestamp("2017-07-26T18:51:53+00:00")));
		privateData.addAnnotation(new Annotation(new Qname("MAN.2"), privateData.getDocumentId(), privateData.getDocumentId(), Util.toTimestamp("2018-07-26T18:51:53+00:00")));
		return root;
	}

	@Test
	public void test_trim() throws Exception {
		DwRoot root = new DwRoot(new Qname("1234"), new Qname("KE.3"));
		root.setCollectionId(new Qname("HR.1"));
		root.createPublicDocument().setNotes(" n o t e s ");
		root.getPublicDocument().addEditorUserId(" editor ");
		JSONObject json = root.toJSON();

		assertEquals("http://tun.fi/1234", json.getString("documentId"));
		assertEquals("n o t e s", json.getObject("publicDocument").getString("notes"));
		assertEquals("editor", json.getObject("publicDocument").getArray("editorUserIds").iterator().next());
	}

	@Test
	public void test_special_chars() throws Exception {
		DwRoot root = new DwRoot(new Qname("1234"), new Qname("KE.3"));
		root.setCollectionId(new Qname("HR.1"));
		String notes = "Test[]\"\"{}><\\\"/\"''";
		root.createPublicDocument().setNotes(notes);
		JSONObject json = root.toJSON();

		assertEquals(notes, json.getObject("publicDocument").getString("notes"));

		String jsonString = json.getObject("publicDocument").toString();
		System.out.println(jsonString);
		String escapedNotes = "Test[]\\\"\\\"{}><\\\\\\\"/\\\"''";
		assertTrue(jsonString.contains(escapedNotes));

		root = JsonToModel.rootFromJson(root.toJSON());
		assertEquals(notes, json.getObject("publicDocument").getString("notes"));
	}

	@Test
	public void joinedRowToJson() throws Exception {
		JoinedRow row = createTestDoc();
		JSONObject json = ModelToJson.rowToJson(row);

		assertEquals(3, json.getKeys().length);
		assertEquals(false, json.getObject("document").hasKey("gatherings"));
		assertEquals("locality", json.getObject("gathering").getString("locality"));
		assertEquals("Matti Meikäläinen", json.getObject("gathering").getArray("team").iterator().next());
		assertEquals("http://tun.fi/MVL.someinformal", json.getObject("unit").getObject("linkings").getObject("taxon").getArray("informalTaxonGroups").iterator().next());
		assertEquals("http://tun.fi/MVL.someinformal", json.getObject("unit").getObject("linkings").getObject("originalTaxon").getArray("informalTaxonGroups").iterator().next());
		assertEquals("[\"http://taxonid.org/ABCD\"]", json.getObject("unit").getObject("linkings").getObject("taxon").getArray("taxonConceptIds").toString());
		System.out.println(json.beautify());
	}

	@Test
	public void joinedRowToSelectedJson() throws Exception {
		JoinedRow row = createTestDoc();
		Selected selected = new Selected("gathering.locality", "gathering.team", "document.documentId", "gathering.eventDate.begin", "gathering.facts", "document.secureLevel", "unit", "document.linkings.editors");
		assertTrue(selected.getSelectedFields().contains("gathering.eventDate.begin"));
		assertFalse(selected.getSelectedFields().contains("gathering.eventDate.end"));
		JSONObject json = ModelToJson.toSelectedJSON(row, selected);
		System.out.println(json.beautify());

		assertFalse(json.getObject("document").hasKey("editorUserIds"));
		assertEquals("http://tun.fi/F.1", json.getObject("document").getString("documentId"));
		assertEquals("[{\"fact\":\"f1\",\"value\":\"v1\"}]", json.getObject("gathering").getArray("facts").toString());
		assertTrue(json.getObject("gathering").getObject("eventDate").hasKey("begin"));
		assertFalse(json.getObject("gathering").getObject("eventDate").hasKey("end"));
		assertEquals("extiirai", json.getObject("document").getObject("linkings").getArray("editors").iterateAsObject().get(0).getString("userId"));
		assertEquals("http://tun.fi/MX.1", json.getObject("unit").getObject("linkings").getObject("taxon").getString("id"));
		assertEquals("[\"http://tun.fi/MX.adminstatus\"]", json.getObject("unit").getObject("linkings").getObject("taxon").getArray("administrativeStatuses").toString());
	}

	@Test
	public void taxonId() throws Exception {
		JoinedRow row = createTestDoc();
		Selected selected = new Selected("unit.linkings.taxon.id");
		JSONObject json = ModelToJson.toSelectedJSON(row, selected);
		System.out.println(json.beautify());
		assertEquals("http://tun.fi/MX.1", json.getObject("unit").getObject("linkings").getObject("taxon").getString("id"));
		assertEquals("", json.getObject("unit").getObject("linkings").getObject("taxon").getString("qname"));
	}

	private JoinedRow createTestDoc() throws Exception {
		Document d = new Document(Concealment.PRIVATE, new Qname("KE.3"), new Qname("F.1"), new Qname("HR.1"));
		d.addEditorUserId("editor");
		d.setLinkings(new DocumentDWLinkings());
		d.getLinkings().getEditors().add(new Person("extiirai"));

		Gathering g = new Gathering(new Qname("F.1#1"));
		g.setLocality("locality");
		g.setBiogeographicalProvince("province");
		g.addFact("f1", "v1");
		g.addTeamMember("Matti Meikäläinen");
		g.createQuality().setTimeIssue(new Quality(Quality.Issue.INVALID_DATE, Quality.Source.ORIGINAL_DOCUMENT, "blaa"));
		g.setEventDate(new DateRange(DateUtils.convertToDate("1.1.2000"), DateUtils.convertToDate("5.1.2000")));

		Unit u = new Unit(new Qname("F.1#2"));
		u.setTaxonVerbatim("hiphip purpur");
		u.setLinkings(new UnitDWLinkings());
		Taxon taxon =  new Taxon(new Qname("MX.1"), new TaxonContainerImple());
		taxon.addTradeName("fi", "Kaunis kukkanen");
		taxon.setScientificName("Hiphip purpur");
		taxon.addVernacularName("fi", "hipsukkainen");
		taxon.setParentQname(new Qname("MX.2"));
		taxon.addOccurrenceInFinlandPublication(new Qname("MP.1"));
		taxon.setRedListStatus2015Finland(new Qname("MX.iucnLC"));
		taxon.addEditor(new Qname("MA.1"));
		taxon.addInformalTaxonGroup(new Qname("MVL.someinformal"));
		taxon.addAdministrativeStatus(new Qname("MX.adminstatus"));
		taxon.addTypeOfOccurrenceInFinland(new Qname("MX.occurs"));
		taxon.addTaxonConceptId(new Qname("taxonid:ABCD"));
		u.getLinkings().setTaxon(taxon);
		u.getLinkings().setOriginalTaxon(taxon);

		d.addGathering(g);
		g.addUnit(u);

		JoinedRow row = new JoinedRow(u, g, d);
		return row;
	}

	private static class TaxonContainerImple extends TaxonContainerStub {
		@Override
		public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups) {
			return informalTaxonGroups;
		}

		@Override
		public boolean hasTaxon(Qname id) {
			return true;
		}

		@Override
		public Taxon getTaxon(Qname id) {
			Taxon t = new Taxon(id, this);
			t.setScientificName("sciname " + id);
			if ("MX.2".equals(id.toString())) {
				t.setTaxonRank(new Qname("MX.kingdom"));
			}
			return t;
		}
		@Override
		public Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatuses) {
			return administrativeStatuses;
		}
	}

	@Test
	public void taxonData() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		doc.addGathering(new Gathering(new Qname("G")));
		Unit u = new Unit(new Qname("U"));
		doc.getGatherings().get(0).addUnit(u);
		u.setLinkings(new UnitDWLinkings());
		Taxon t = new Taxon(new Qname("MX.1"), new TaxonContainerImple());
		u.getLinkings().setTaxon(t);
		t.setPrimaryHabitat(new HabitatObject(new Qname("HB.1"), new Qname("H"), 1).addHabitatSpecificType(new Qname("spf")));
		t.setBirdlifeCode("asd");
		t.setBreedingSecureLevel(new Qname("MX.seclev"));
		t.setChecklist(new Qname("MR.1"));
		t.setCreatedAtTimestamp(123);
		t.setCustomReportFormLink(new URI("http://example.com"));
		t.setHiddenTaxon(true);
		t.setStopOccurrenceInFinlandPublicationInheritance(true);
		t.setScientificName("Parus major");
		t.addVernacularName("fi", "talitintti");
		t.setTaxonRank(new Qname("MX.species"));
		t.setTaxonomicOrder(123);
		t.setRedListStatus2019Finland(new Qname("MX.iucnLC"));
		t.setParentQname(new Qname("MX.2"));
		t.setExplicitObservationCountFinland(24);

		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		JSONObject taxonJson = json.getArray("gatherings").iterateAsObject().get(0).getArray("units").iterateAsObject().get(0).getObject("linkings").getObject("taxon");
		Map<String, String> asSortedMap = new TreeMap<>();
		for (String key : taxonJson.getKeys()) {
			if (taxonJson.isObject(key)) {
				asSortedMap.put(key, taxonJson.getObject(key).toString());
			} else {
				String v = taxonJson.getString(key);
				if (v.isEmpty()) {
					v = Boolean.toString(taxonJson.getBoolean(key));
				}
				asSortedMap.put(key, v);
			}
		}

		String expected = "" +
				"{checklist=http://tun.fi/MR.1, cursiveName=true, finnish=false, id=http://tun.fi/MX.1, kingdomScientificName=sciname MX.2, " +
				"latestRedListStatusFinland={\"status\":\"http://tun.fi/MX.iucnLC\",\"year\":2019}, nameFinnish=talitintti, occurrenceCountFinland=24, " +
				"primaryHabitat={\"habitat\":\"http://tun.fi/H\",\"habitatSpecificTypes\":[\"http://tun.fi/spf\"],\"id\":\"http://tun.fi/HB.1\",\"order\":1}, " +
				"scientificName=Parus major, scientificNameDisplayName=Parus major, " +
				"sensitive=true, taxonRank=http://tun.fi/MX.species, taxonomicOrder=123, vernacularName={\"fi\":\"talitintti\"}}";
		assertEquals(expected, asSortedMap.toString());
	}

	@Test
	public void geoToGeoJSON() throws Exception {
		Gathering g = new Gathering(new Qname("g1"));
		g.setGeo(new Geo(Type.WGS84)
				.addFeature(Line.from(new double[][] {{60.0, 30.0}, {61.0, 30.0}}))
				.addFeature(Point.from(58.123, 42.123))
				.addFeature(Polygon.from(new double[][] {{55.0, 70.0}, {55.1, 70.0}, {55.1, 70.1}, {55.0, 70.1}})).setAccuracyInMeters(1000));
		JSONObject json = ModelToJson.toJson(g);
		System.out.println(json.beautify());
		assertEquals("FeatureCollection", json.getObject("geo").getString("type"));
		assertEquals(Type.WGS84.name(), json.getObject("geo").getString("crs"));
		assertEquals(1000, json.getObject("geo").getInteger(Geo.ACCURACY_IN_METERS));
		JSONArray features = json.getObject("geo").getArray("features");
		assertEquals(3, features.size());
		for (JSONObject feature : features.iterateAsObject()) {
			assertEquals("Feature", feature.getString("type"));
		}
		JSONObject line = features.iterateAsObject().get(0).getObject("geometry");
		JSONObject point = features.iterateAsObject().get(1).getObject("geometry");
		JSONObject polygon = features.iterateAsObject().get(2).getObject("geometry");
		assertEquals("LineString", line.getString("type"));
		assertEquals("Point", point.getString("type"));
		assertEquals("Polygon", polygon.getString("type"));
	}

	@Test
	public void approvalRequest() throws Exception {
		BaseQuery baseQuery = new BaseQueryBuilder(Concealment.PRIVATE).setApiSourceId("user").setCaller(this.getClass()).setPersonEmail("email@some.com").setPersonId(new Qname("MA.5")).build();
		baseQuery.getFilters()
		.setTarget("a")
		.setTarget("b")
		.setCoordinates(new CoordinatesWithOverlapRatio(6666666, 3333333, Type.YKJ))
		.setCoordinates(new CoordinatesWithOverlapRatio(1.2345678901234, -1.0001, Type.WGS84).setOverlapRatio(0.50000000000000000001))
		.setPolygon(new PolygonSearch("POLYGON((556320 7167999,557598 7166987,557885 7169572,556320 7167999))", Coordinates.Type.EUREF.name()))
		.setYkj10km(new SingleCoordinates(666, 333, null))
		.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.AMATEUR).setRecordQuality(RecordQuality.EXPERT_VERIFIED).setRecordQuality(RecordQuality.COMMUNITY_VERIFIED))
		.setCollectionAndRecordQuality(new CollectionAndRecordQuality(CollectionQuality.HOBBYIST).setRecordQuality(RecordQuality.NEUTRAL).setRecordQuality(RecordQuality.COMMUNITY_VERIFIED));
		DownloadRequest request = new DownloadRequest(new Qname("HBF.123"), new Date(), baseQuery, DownloadType.DATA_REQUEST, DownloadFormat.CSV_FLAT, Utils.set(DownloadInclude.DOCUMENT_EDITORS));
		request.setApproximateResultSize(10);
		CollectionAndReasons c1 = new CollectionAndReasons(new Qname("col1"));
		CollectionAndReasons c2 = new CollectionAndReasons(new Qname("col2"));
		CollectionAndReasons c3 = new CollectionAndReasons(new Qname("col3"));
		c1.addCount(SecureReason.BREEDING_SITE_CONSERVATION, 3);
		c1.addCount(SecureReason.CUSTOM, 2);
		c1.addCount(SecureReason.DATA_QUARANTINE_PERIOD, 4);
		c2.addCount(SecureReason.USER_PERSON_NAMES_HIDDEN, 1);

		List<CollectionAndReasons> collectionReasons = Utils.list(c1, c2, c3);

		JSONObject json = DownloadRequestToJSON.toJSON(request, collectionReasons, dao);
		JSONObject expected = getTestData("approval-request.json");

		System.out.println(json.beautify());

		assertEquals("HBF.123", json.getString("id"));
		assertEquals(DateUtils.getCurrentDateTime("yyyy-MM-dd"), json.getString("requested"));

		json.remove("id");
		json.remove("requested");

		assertEquals(expected.beautify(), json.beautify());
	}

	@Test
	public void lightweightRequest() throws Exception {
		BaseQuery baseQuery = new BaseQueryBuilder(Concealment.PUBLIC).setApiSourceId("user").setCaller(this.getClass()).build();
		DownloadRequest request = new DownloadRequest(new Qname("HBF.123"), new Date(), baseQuery, DownloadType.LIGHTWEIGHT, DownloadFormat.CSV_FLAT, Utils.set());
		request.setApproximateResultSize(10);

		JSONObject json = DownloadRequestToJSON.toJSON(request, Collections.emptyList(), dao);
		JSONObject expected = getTestData("lightweight-request.json");

		System.out.println(json.beautify());
		json.remove("id");
		json.remove("requested");

		assertEquals(expected.toString(), json.toString());
	}

	private JSONObject getTestData(String filename) throws Exception {
		URL url = ModelToJsonTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return new JSONObject(data);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void annotation() throws CriticalParseFailure, DataValidationException, ParseException {
		Annotation annotation = new Annotation(new Qname("MAN.1"), new Qname("JA.123"), new Qname("JA.123#1"), Util.toTimestamp("2017-07-26T18:51:53+00:00"));
		annotation.setAnnotationByPerson(new Qname("MA.5"));
		annotation.setAnnotationBySystem(new Qname("KE.1"));
		annotation.setNotes("blaa");
		annotation.setIdentification(new Identification("susi", new Qname("MX.1")));
		annotation.getIdentification().setLinkings(new IdentificationDwLinkings());
		annotation.getIdentification().getLinkings().setTaxon(new Taxon(new Qname("MX.1"), new TaxonContainerStub()));

		OccurrenceAtTimeOfAnnotation o = new OccurrenceAtTimeOfAnnotation();
		o.setCountryVerbatim("fi");
		o.setDateBegin(DateUtils.convertToDate("1.3.2011"));
		o.setLocality("local");
		o.setMunicipalityVerbatim("munic");
		o.setTaxonId(new Qname("MX.1"));
		o.setTaxonVerbatim("päästäiset");
		o.setWgs84centerPointLat(60.123);
		o.setWgs84centerPointLon(30.0);
		o.setLinkings(new IdentificationDwLinkings());
		o.getLinkings().setTaxon(new Taxon(new Qname("MX.1"), new TaxonContainerStub()));
		annotation.setOccurrenceAtTimeOfAnnotation(o);

		annotation.addTag(Tag.ADMIN_MARKED_COARSE).addTag(Tag.CHECK_COORDINATES);
		annotation.removeTag(Tag.EXPERT_TAG_VERIFIED);

		JSONObject json = ModelToJson.toJson(annotation);
		System.out.println(json.beautify());

		assertEquals("blaa", json.getString("notes"));
		assertEquals(new Qname("MAN.1").toURI(), json.getString("id"));
		assertEquals(new Qname("MA.5").toURI(), json.getString("annotationByPerson"));
		assertEquals(new Qname("KE.1").toURI(), json.getString("annotationBySystem"));
		assertEquals("susi", json.getObject("identification").getString("taxon"));
		assertEquals(new Qname("JA.123").toURI(), json.getString("rootID"));
		assertEquals(new Qname("JA.123#1").toURI(), json.getString("targetID"));
		assertEquals("2017-07-26T18:51:53+00:00",  json.getString("created"));
		assertEquals("[ADMIN_MARKED_COARSE, CHECK_COORDINATES]", json.getArray("addedTags").iterateAsString().toString());
		assertEquals("[EXPERT_TAG_VERIFIED]", json.getArray("removedTags").iterateAsString().toString());
	}

	@Test
	public void obsCounts() {
		Collection<ObsCount> counts = new ArrayList<>();

		ObsCount c1 = new ObsCount(new Qname("MX.1"));
		c1.count = 10;
		c1.countFinland = 9;
		c1.biogeographicalProvinceCounts = new LinkedHashMap<>();
		c1.biogeographicalProvinceCounts.put(new Qname("ML.123"), 8);
		c1.biogeographicalProvinceCounts.put(new Qname("ML.456"), 1);

		ObsCount c2 = new ObsCount(new Qname("MX.2"));
		c2.count = 10;

		ObsCount c3 = new ObsCount(new Qname("MX.3"));
		c3.count = 1234;
		c3.countFinland = 890;
		c3.habitatOccurrenceCounts = new LinkedHashMap<>();
		c3.habitatOccurrenceCounts.put("http://tun.fi/MY.foobar", 5);
		c3.habitatOccurrenceCounts.put("metsä", 235);

		counts.add(c1);
		counts.add(c2);
		counts.add(c3);

		JSONObject json = ModelToJson.toJson(counts);
		System.out.println(json.beautify());

		assertEquals("http://tun.fi/MX.1", json.getArray("results").iterateAsObject().get(0).getString("taxonId"));
		assertEquals(10, json.getArray("results").iterateAsObject().get(0).getInteger("count"));
		assertEquals(8, json.getArray("results").iterateAsObject().get(0).getObject("biogeographicalProvinceCounts").getInteger("ML.123"));
		assertEquals(235, json.getArray("results").iterateAsObject().get(2).getObject("habitatOccurrenceCounts").getInteger("metsä"));
	}

}
