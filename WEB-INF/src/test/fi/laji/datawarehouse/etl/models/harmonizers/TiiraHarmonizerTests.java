package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.threads.custom.TiiraAtlasPullReader;
import fi.luomus.commons.containers.rdf.Qname;

public class TiiraHarmonizerTests {

	private Harmonizer<String> harmonizer = new TiiraHarmonizer();

	@Test
	public void tiira() throws CriticalParseFailure, UnknownHarmonizingFailure {
		String data = "61d0e3f94bcfb	ANAPLA	684	335			20220101		1	tiira.fi	Suomenselän Lintutieteellinen Yhdistys ry";
		List<DwRoot> roots = harmonizer.harmonize(data, TiiraAtlasPullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(TiiraHarmonizer.COLLECTION.toURI()+"/61d0e3f94bcfb", d.getDocumentId().toURI());
		assertEquals("[]", d.getSecureReasons().toString());
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(d.getDocumentId().toURI()+"_G", g.getGatheringId().toURI());
		assertEquals("684.0 : 685.0 : 335.0 : 336.0 : YKJ : null", g.getCoordinates().toString());
		assertEquals("684:335", g.getCoordinatesVerbatim());
		assertEquals("[Suomenselän Lintutieteellinen Yhdistys ry]", g.getTeam().toString());
		assertEquals("2022-01-01", g.getDisplayDateTime());
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);
		assertEquals(d.getDocumentId().toURI()+"_U", u.getUnitId().toURI());
		assertEquals("ANAPLA", u.getTaxonVerbatim());
		assertEquals("MY.atlasCodeEnum1", Qname.fromURI(u.getAtlasCode()).toString());
		assertEquals(null, u.getAtlasClass());
		assertEquals(null, d.getQuality());
		assertEquals(null, g.getQuality());
		assertEquals(null, u.getQuality());
	}

	@Test
	public void tiira_2() throws CriticalParseFailure, UnknownHarmonizingFailure {
		String data = "61d629fa45705	LOXCUR	675	342	0	50	20220105	20220106	2";
		List<DwRoot> roots = harmonizer.harmonize(data, TiiraAtlasPullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(TiiraHarmonizer.COLLECTION.toURI()+"/61d629fa45705", d.getDocumentId().toURI());
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(d.getDocumentId().toURI()+"_G", g.getGatheringId().toURI());
		assertEquals("675.0 : 676.0 : 342.0 : 343.0 : YKJ : null", g.getCoordinates().toString());
		assertEquals("675:342", g.getCoordinatesVerbatim());
		assertEquals("[]", g.getTeam().toString());
		assertEquals("2022-01-05 - 2022-01-06", g.getDisplayDateTime());
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);
		assertEquals(d.getDocumentId().toURI()+"_U", u.getUnitId().toURI());
		assertEquals("LOXCUR", u.getTaxonVerbatim());
		assertEquals("MY.atlasCodeEnum2", Qname.fromURI(u.getAtlasCode()).toString());
		assertEquals(null, u.getAtlasClass());
		assertEquals(null, d.getQuality());
		assertEquals(null, g.getQuality());
		assertEquals(null, u.getQuality());
		assertEquals("[tarkkuus_a : 0, tarkkuus_y : 50]", g.getFacts().toString());
	}

	@Test
	public void tiira_3() throws CriticalParseFailure, UnknownHarmonizingFailure {
		String data = "61d629fa45705";
		List<DwRoot> roots = harmonizer.harmonize(data, TiiraAtlasPullReader.STAGING_SOURCE);
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(TiiraHarmonizer.COLLECTION.toURI()+"/61d629fa45705", d.getDocumentId().toURI());
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(d.getDocumentId().toURI()+"_G", g.getGatheringId().toURI());
		assertEquals(null, g.getCoordinates());
		assertEquals(null, g.getCoordinatesVerbatim());
		assertEquals("[]", g.getTeam().toString());
		assertEquals(null, g.getDisplayDateTime());
		assertEquals(0, g.getUnits().size());
	}


}
