package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class LajistoreAnnotationHarmonizerTests {

	private final LajistoreAnnotationHarmonizer harmonizer = new LajistoreAnnotationHarmonizer();

	@Test
	public void test() throws CriticalParseFailure {
		String data = RdfXmlHarmonizerTests.getTestData("annotation-1.json");
		Annotation annotation = test_common(data);
		assertFalse(annotation.isDeleted());
		assertEquals(null, annotation.getDeletedByPerson());
		assertEquals(null, annotation.getDeletedTimestamp());
		assertEquals(null, annotation.getDeletedDateTime());
	}

	private Annotation test_common(String data) throws CriticalParseFailure {
		List<DwRoot> roots = harmonizer.harmonize(new JSONObject(data), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("http://tun.fi/KE.1", root.getSourceId().toURI());
		assertNull(root.getDocumentId());
		assertNull(root.getCollectionId());
		assertNull(root.getPrivateDocument());
		assertNull(root.getPublicDocument());
		assertNull(root.getSplittedPublicDocuments());
		assertEquals(1, root.getAnnotations().size());
		Annotation annotation = root.getAnnotations().get(0);

		assertEquals("http://tun.fi/MAN.2852", annotation.getId().toURI());
		assertEquals("http://tun.fi/JX.163821", annotation.getRootID().toURI());
		assertEquals("http://tun.fi/JX.163821#3", annotation.getTargetID().toURI());
		assertEquals("MA.5", annotation.getAnnotationByPerson().toString());
		assertEquals(null, annotation.getAnnotationBySystem());
		assertEquals("minusta tämä...", annotation.getNotes());
		assertEquals(1581495507, annotation.getCreatedTimestamp().intValue());
		assertEquals("2020-02-12T08:18:27+00:00", annotation.getCreated());
		assertEquals("sienet", annotation.getIdentification().getTaxon());
		assertEquals("["+Tag.EXPERT_TAG_ERRONEOUS+"]", annotation.getAddedTags().toString());
		assertEquals("["+Tag.EXPERT_TAG_VERIFIED+"]", annotation.getRemovedTags().toString());
		assertEquals(
				"Identification [id=null, taxon=sienet, author=null, taxonId=MX.53062, taxonSpecifier=foo, taxonSpecifierAuthor=null]",
				annotation.getIdentification().toString());
		assertEquals(
				"OccurrenceAtTimeOfAnnotation [taxonId=MX.72753, taxonVerbatim=kellovinokas, dateBegin=12.10.2019, dateEnd=12.10.2019, wgs84centerPointLat=60.377632, wgs84centerPointLon=24.419417, countryVerbatim=Suomi, locality=, municipalityVerbatim=]",
				annotation.getOccurrenceAtTimeOfAnnotation().toString());
		// TODO byRole
		return annotation;
	}

	@Test
	public void test_delete() throws CriticalParseFailure {
		String data = RdfXmlHarmonizerTests.getTestData("annotation-delete.json");
		Annotation annotation = test_common(data);
		assertTrue(annotation.isDeleted());
		assertEquals("MA.1", annotation.getDeletedByPerson().toString());
		assertEquals(1581507000, annotation.getDeletedTimestamp().intValue());
		assertEquals("2020-02-12T11:30:00+00:00", annotation.getDeletedDateTime());
	}

	@Test
	public void several_annotations() throws CriticalParseFailure {
		String data = RdfXmlHarmonizerTests.getTestData("annotation-2.json");
		List<DwRoot> roots = harmonizer.harmonize(new JSONObject(data), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("http://tun.fi/KE.1", root.getSourceId().toURI());
		assertNull(root.getDocumentId());
		assertNull(root.getPrivateDocument());
		assertNull(root.getPublicDocument());
		assertNull(root.getSplittedPublicDocuments());
		assertEquals(3, root.getAnnotations().size());

		Annotation annotation = root.getAnnotations().get(0);
		assertEquals("http://tun.fi/MAN.2851", annotation.getId().toURI());
		assertEquals("2020-02-12T08:17:58+00:00", annotation.getCreated());

		annotation = root.getAnnotations().get(1);
		assertEquals("http://tun.fi/MAN.2852", annotation.getId().toURI());
		assertEquals("2020-02-12T08:18:27+00:00", annotation.getCreated());

		annotation = root.getAnnotations().get(2);
		assertEquals("http://tun.fi/MAN.2853", annotation.getId().toURI());
		assertEquals("2020-02-12T08:19:12+00:00", annotation.getCreated());
	}

	@Test
	public void missingValues() throws CriticalParseFailure {
		String data = RdfXmlHarmonizerTests.getTestData("annotation-3.json");
		List<DwRoot> roots = harmonizer.harmonize(new JSONObject(data), new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(1, root.getAnnotations().size());
		Annotation annotation = root.getAnnotations().get(0);
		assertEquals(null, annotation.getIdentification());
		assertEquals("OccurrenceAtTimeOfAnnotation [taxonId=null, taxonVerbatim=kellovinokas, dateBegin=null, dateEnd=null, wgs84centerPointLat=null, wgs84centerPointLon=null, countryVerbatim=, locality=, municipalityVerbatim=]", annotation.getOccurrenceAtTimeOfAnnotation().toString());
	}

}
