package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.dao.MediaDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;

public class RdfXmlHarmonizerTests {

	private RdfXmlHarmonizer rdfXmlHarmonizer;

	public static String getTestData(String filename) {
		URL url = RdfXmlHarmonizerTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static class TestMediaDAO implements MediaDAO {
		@Override
		public MediaObject getMediaObject(Qname id) {
			throw new UnsupportedOperationException("No test mock yet for media id " + id);
		}

		@Override
		public List<MediaObject> getMediaObjects(Qname documentId) {
			try {
				if (documentId.toString().equals("luomus:GZ.1")) {
					List<MediaObject> media = new ArrayList<>();
					media.add(new MediaObject(MediaType.IMAGE, "http://img.com/1/full.jpg").addKeyword("label").addKeyword("skull"));
					//				<MM.image>
					//				<MM.fullURL>http://img.com/1/full.jpg</MM.fullURL>
					//				<MM.squareThumbnailURL>http://img.com/1/square.jpg</MM.squareThumbnailURL>
					//				<MM.largeURL>http://img.com/1/large.jpg</MM.largeURL>
					//				<MM.keyword>label</MM.keyword>
					//				<MM.keyword>skull</MM.keyword>
					//				<MZ.intellectualRights>Creative Commons Attribution Share-Alike 4.0</MZ.intellectualRights>
					//				<MZ.intellectualRightsID>MZ.intellectualRightsCC-BY-SA-4.0</MZ.intellectualRightsID>
					//				<MZ.intellectualOwner>Luomus</MZ.intellectualOwner>
					//				<MM.capturerVerbatim>Malinen, Pekka</MM.capturerVerbatim>
					//			</MM.image>

					media.add(new MediaObject(MediaType.IMAGE, "http://img.com/label_in_name_not_as_keyword/foobar.jpg"));
					media.add(new MediaObject(MediaType.IMAGE, "http://img.com/habitat_in_name_not_as_keyword/foobar.jpg"));
					media.add(new MediaObject(MediaType.IMAGE, "http://img.com/hab_as_keyword/foobar.jpg").addKeyword("habitat"));
					media.add(new MediaObject(MediaType.IMAGE, "http://img.com/nonprim_specimenimage/foobar.jpg"));

					MediaObject m = new MediaObject(MediaType.IMAGE, "http://img.com/prim_as_keyword/foobar.jpg").addKeyword("specimen").addKeyword("primary");
					m.setAuthor("null null");
					media.add(m);

					MediaObject m2 = new MediaObject(MediaType.IMAGE, "http://digi.fi/foobar.jpg");
					m2.setCopyrightOwner("Luomus, University of Eastern Finland");
					m2.setLicenseIdUsingQname(new Qname("MZ.intellectualRightsARR"));
					m2.setAuthor("Digitarium; Digi Digitoija");
					media.add(m2);

					return media;
				}
				if (documentId.toString().equals("luomus:GZ.2")) {
					MediaObject m = new MediaObject(MediaType.MODEL);
					m.setId(new Qname("MM.1"));
					m.setVideoURL("https://test.com/video.mp4");
					m.setThumbnailURL("https://test.com/video_thumb.jpg");
					m.setFullResolutionMediaAvailable(true);
					return Utils.list(m);
				}
			} catch (DataValidationException e) {
				throw new RuntimeException(e);
			}
			return Collections.emptyList();
		}
	}

	@Before
	public void setUp() {
		rdfXmlHarmonizer = new RdfXmlHarmonizer(new TestMediaDAO());
	}

	@Test
	public void when_harmonizing_invalid_xml_throws_critical_parse_failure() throws UnknownHarmonizingFailure {
		PrintStream original = System.err;
		try {
			System.setErr(new PrintStream(new OutputStream() { // Make xml parser error silent
				@Override
				public void write(int b) throws IOException {
					// NOP
				}
			}));
			rdfXmlHarmonizer.harmonize("foo", new Qname("KE.3"));
			Assert.fail("Should throw exception");
		} catch (CriticalParseFailure e) {
			assertEquals("Invalid application/rdf+xml", e.getMessage());
		} finally {
			System.setErr(original); // Restore System.err
		}
	}

	@Test
	public void when_harmonizing_documents_without_documentid_throws_critical_parse_failure() throws UnknownHarmonizingFailure {
		String xml = removeDocumentId();
		try {
			rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
			fail("Should fail");
		} catch (CriticalParseFailure e) {
			assertEquals("No document id", e.getMessage());
		}
	}

	private String removeDocumentId() {
		Node testData = getTestDataAsDocument("rdf-xml-testdata-1.xml");
		testData.getNode("MY.document").removeAttribute("rdf:about");
		String xml = new XMLWriter(testData).generateXML();
		return xml;
	}

	private Node getTestDataAsDocument(String filename) {
		Node testData = new XMLReader().parse(getTestData(filename)).getRootNode();
		return testData;
	}

	@Test
	public void when_harmonizing_documents_without_collectionid() throws Exception {
		String xml = removeCollectionId();
		try {
			rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
			fail();
		} catch (CriticalParseFailure e) {
			assertEquals("Id for collectionId is not set", e.getMessage());
		}
	}

	private String removeCollectionId() {
		Node testData = getTestDataAsDocument("rdf-xml-testdata-1.xml");
		testData.getNode("MY.document").removeNodes("MY.collectionID");
		String xml = new XMLWriter(testData).generateXML();
		return xml;
	}

	@Test
	public void when_harmonizing_documents_where_there_is_invalid_json() throws Exception {
		try {
			rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-4-invalidjson.xml"), new Qname("KE.3"));
			fail();
		} catch (CriticalParseFailure e) {
			assertEquals("Invalid JSON: INVALID_JSON", e.getMessage());
		}
	}

	@Test
	public void harmonize_single_document() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-1.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		assertEquals("http://tun.fi/EA4.0WB", root.getDocumentId().toURI());
		assertEquals("http://tun.fi/HR.407", root.getCollectionId().toURI());
		assertEquals("KE.3", root.getSourceId().toString());

		Document publicDocument = root.getPublicDocument();
		Document privateDocument = root.getPrivateDocument();
		assertTrue(publicDocument != null);
		assertTrue(privateDocument == null);

		assertEquals("KE.3", publicDocument.getSourceId().toString());
		assertEquals("http://tun.fi/EA4.0WB", publicDocument.getDocumentId().toURI());
		assertEquals("http://tun.fi/HR.407", publicDocument.getCollectionId().toURI());

		assertEquals("2013-11-01", DateUtils.format(publicDocument.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals(1, publicDocument.getEditorUserIds().size());
		assertEquals("http://tun.fi/MA.88", publicDocument.getEditorUserIds().get(0));

		assertEquals(2, publicDocument.getKeywords().size());
		assertEquals("http://tun.fi/GX.270", publicDocument.getKeywords().get(0));
		assertEquals("http://tun.fi/GX.643", publicDocument.getKeywords().get(1));

		assertEquals("previous collection owner; Lahden historiallinen museo", publicDocument.getNotes());

		String expectedDocumentFacts = "" +
				"http://tun.fi/MZ.owner : http://tun.fi/MOS.1007\n" +
				"http://tun.fi/MY.preservation : http://tun.fi/MY.preservationPinned\n" +
				"http://tun.fi/MY.documentLocation : In collection; Cabinet: Noctuidae; Box: 10";
		String actualDocumentFacts = debugFacts(publicDocument);
		assertEquals(expectedDocumentFacts, actualDocumentFacts);

		assertEquals(1, publicDocument.getGatherings().size());
		Gathering gathering = publicDocument.getGatherings().get(0);

		assertEquals("Europe", gathering.getHigherGeography());
		assertEquals("FI", gathering.getCountry());
		assertEquals("Turku", gathering.getMunicipality());
		assertEquals("Varsinais-Suomi", gathering.getBiogeographicalProvince());

		assertEquals(1, gathering.getTeam().size());
		assertEquals("Seppälä, J.", gathering.getTeam().get(0));
		assertNull(gathering.getNotes());

		String expectedGatheringFacts = "" +
				"http://tun.fi/MY.alt : 35m\n" +
				"biogeographicalProvince : N\n" + // MY.dynamicProperties
				"TaxonCensus : Rhopalocera"; // MY.dynamicProperties
		String actualGatheringFacts = debugFacts(gathering);
		assertEquals(expectedGatheringFacts, actualGatheringFacts);

		assertEquals(1, gathering.getTaxonCensus().size());
		assertEquals("MX.60720", gathering.getTaxonCensus().get(0).getTaxonId().toString());
		assertEquals("MY.taxonCensusTypeCounted", gathering.getTaxonCensus().get(0).getType().toString());

		assertEquals(1, gathering.getUnits().size());
		Unit unit = gathering.getUnits().get(0);
		assertEquals("1", unit.getAbundanceString());
		assertNull(unit.getNotes());

		String expectedUnitFacts = "" +
				"somefact : factvalue";
		String actualUnitFacts = debugFacts(unit);
		assertEquals(expectedUnitFacts, actualUnitFacts);

		assertEquals(Sex.FEMALE, unit.getSex());
		assertEquals(LifeStage.ADULT, unit.getLifeStage());
		assertEquals(RecordBasis.PRESERVED_SPECIMEN, unit.getRecordBasis());
		assertEquals(false, unit.isWild());
		assertEquals("http://tun.fi/MY.plantStatusCodeR", unit.getPlantStatusCode());

		assertEquals("von Bonsdorff, Robert", unit.getDet());
		assertEquals("Apamea crenata var. gymnandrus", unit.getTaxonVerbatim());

		assertEquals(true, unit.isAlive());

		// System.out.println(root.toJSON().beautify());
	}

	private String debugFacts(BaseModel model) {
		return model.getFacts().stream().map(f->f.toString()).collect(Collectors.joining("\n"));
	}

	@Test
	public void testHarmonize_with_multiple_units_and_identifications() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testadata-1-multiunit-identification-type.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		Document publicDocument = root.getPublicDocument();
		Gathering gathering = publicDocument.getGatherings().get(0);

		assertEquals(2, gathering.getUnits().size());
		Unit unit1 = gathering.getUnits().get(1); // units are sorted so that specimens are put first
		Unit unit2 = gathering.getUnits().get(0); //

		assertEquals(true, unit2.isPrimarySpecimen());
		assertEquals(null, unit1.isPrimarySpecimen());

		assertEquals("Subularia ququtata", unit1.getTaxonVerbatim()); // Single identification
		assertEquals("Subularia aquatica 2", unit2.getTaxonVerbatim()); // Three idenfificatons, none marked preferred: take smallest unit id numeric value
		assertEquals("Detteri Petteri", unit2.getDet());

		assertFalse(unit1.isTypeSpecimen());
		assertTrue(unit2.isTypeSpecimen());

		assertEquals(
				"[Identification [id=MY.99384, taxon=Subularia ququtata, author=null, taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
				unit1.getIdentifications().toString());
		assertEquals(
				"[Identification [id=MY.11, taxon=Subularia aquatica 2, author=L. s. str., taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null], "+
						"Identification [id=MY.99385, taxon=Subularia aquatica 1, author=L., taxonId=null, taxonSpecifier=f. elongata, taxonSpecifierAuthor=null], "+
						"Identification [id=MY.99387, taxon=Subularia aquatica 3, author=L. s. str., taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
						unit2.getIdentifications().toString());
		assertEquals(
				"[]",
				unit1.getTypes().toString());
		assertEquals(
				"[Identification [id=MY.1, taxon=Subularia aquatica, author=null, taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
				unit2.getTypes().toString());
	}

	@Test
	public void testHarmonize_with_multiple_units_and_identifications_sort_by_det_date() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testadata-1-multiunit-identification-type1.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		Document publicDocument = root.getPublicDocument();
		Gathering gathering = publicDocument.getGatherings().get(0);

		Unit unit2 = gathering.getUnits().get(0); // units sorted so that second unit in document is actually now first

		assertEquals(
				"[Identification [id=MY.99387, taxon=Subularia aquatica 3, author=L. s. str., taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null], " +
						"Identification [id=MY.99385, taxon=Subularia aquatica 1, author=L., taxonId=null, taxonSpecifier=f. elongata, taxonSpecifierAuthor=null], " +
						"Identification [id=MY.11, taxon=Subularia aquatica 2, author=L. s. str., taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
						unit2.getIdentifications().toString());
	}

	@Test
	public void testHarmonize_with_multiple_identifications_with_preferred() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testadata-1-multiunit-identification-type2.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		Document publicDocument = root.getPublicDocument();
		Gathering gathering = publicDocument.getGatherings().get(0);
		Unit unit1 = gathering.getUnits().get(0);
		Unit unit2 = gathering.getUnits().get(1);
		assertEquals("Subularia aquatica 1", unit1.getTaxonVerbatim()); // One idenfificaton
		assertEquals("Subularia ququtata", unit2.getTaxonVerbatim()); // Three idenfificatons, this one marked as preferred
	}

	@Test
	public void type_specimen_marked_not_a_type() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testadata-1-multiunit-identification-type3.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		Document publicDocument = root.getPublicDocument();
		Gathering gathering = publicDocument.getGatherings().get(0);

		Unit unit1 = gathering.getUnits().get(0);
		Unit unit2 = gathering.getUnits().get(1);

		assertFalse(unit1.isTypeSpecimen());
		assertFalse(unit2.isTypeSpecimen());
	}

	@Test
	public void harmonize_single_document_marked_private() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-1-private.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		assertEquals("http://tun.fi/EA4.0WB", root.getDocumentId().toURI());
		assertEquals("http://tun.fi/HR.407", root.getCollectionId().toURI());

		Document publicDocument = root.getPublicDocument();
		Document privateDocument = root.getPrivateDocument();
		assertTrue(publicDocument != null);
		assertTrue(privateDocument == null);

		assertEquals(SecureLevel.KM100, publicDocument.getSecureLevel());
		assertEquals("["+SecureReason.CUSTOM+"]", publicDocument.getSecureReasons().toString());

		assertEquals("2013-11-01", DateUtils.format(publicDocument.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals(1, publicDocument.getEditorUserIds().size());
		assertEquals(2, publicDocument.getKeywords().size());
		assertEquals("previous collection owner; Lahden historiallinen museo", publicDocument.getNotes());
		assertEquals(3, publicDocument.getFacts().size());
		assertEquals(1, publicDocument.getGatherings().size());

		Gathering gathering = publicDocument.getGatherings().get(0);
		assertEquals("Europe", gathering.getHigherGeography());
		assertEquals("FI", gathering.getCountry());
		assertEquals("Turku", gathering.getMunicipality());
		assertEquals("Varsinais-Suomi", gathering.getBiogeographicalProvince());
		assertEquals("Europe", gathering.getHigherGeography());
		assertEquals("FI", gathering.getCountry());

		//		<MY.dateBegin>01.10.2014</MY.dateBegin>
		//		<MY.dateEnd>23.10.2015</MY.dateEnd>

		assertEquals("2014-10-01", DateUtils.format(gathering.getEventDate().getBegin(), "yyyy-MM-dd"));
		assertEquals("2015-10-23", DateUtils.format(gathering.getEventDate().getEnd(), "yyyy-MM-dd"));

		//		<MY.wgs84Longitude>24.005514</MY.wgs84Longitude>
		//		<MY.wgs84Latitude></MY.wgs84Latitude>

		assertEquals(60.076463, gathering.getCoordinates().getLatMin(), 0);
		assertEquals(24.005514, gathering.getCoordinates().getLonMin(), 0);
		assertEquals(60.076463, gathering.getCoordinates().getLatMax(), 0);
		assertEquals(24.005514, gathering.getCoordinates().getLonMax(), 0);
		assertEquals(Type.WGS84, gathering.getCoordinates().getType());
		assertEquals(null, gathering.getCoordinates().getAccuracyInMeters());
		assertEquals("60.076463 24.005514 WGS84", gathering.getCoordinatesVerbatim());

		assertEquals(1, gathering.getTeam().size());

		assertEquals(1, gathering.getUnits().size());
		Unit unit = gathering.getUnits().get(0);
		assertFalse(unit.isTypeSpecimen());
		assertEquals("1", unit.getAbundanceString());
		assertEquals("Kuusankosken joella (paljastaa jotain paikasta)", unit.getNotes());

		assertEquals(Sex.FEMALE, unit.getSex());
		assertEquals(LifeStage.ADULT, unit.getLifeStage());
		assertEquals(RecordBasis.PRESERVED_SPECIMEN, unit.getRecordBasis());

		assertEquals("von Bonsdorff, Robert", unit.getDet());
		assertEquals("Apamea crenata", unit.getTaxonVerbatim());
		// System.out.println(root.toJSON().beautify());
	}

	@Test
	public void harmonize_single_document__more_fields__is_private__ykj_coordinates() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-2.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document publicDocument = root.getPublicDocument();
		Document privateDocument = root.getPrivateDocument();
		assertTrue(publicDocument != null);
		assertTrue(privateDocument == null);

		assertContains("sOriginal catalogue number", publicDocument.getKeywords());
		assertContains("sId4", publicDocument.getKeywords());
		assertContains("http://tun.fi/GX.10", publicDocument.getKeywords());
		assertEquals(6, publicDocument.getKeywords().size());

		Gathering gathering = publicDocument.getGatherings().get(0);

		assertEquals("sAdministrative province", gathering.getProvince());
		assertEquals("sBiogeographical province", gathering.getBiogeographicalProvince());
		assertEquals("sLocality name", gathering.getLocality());
		assertEquals("Inkoo", gathering.getMunicipality());

		//		<MY.coordinateSystem rdf:resource="http://tun.fi/MY.coordinateSystemYkj" />
		//		<MY.latitude>6666</MY.latitude>
		//		<MY.longitude>3333</MY.longitude>
		//		<MY.coordinateSource rdf:resource="http://tun.fi/MY.coordinateSourceOther" />

		//		<MY.wgs84Latitude>60.076463</MY.wgs84Latitude>
		//		<MY.wgs84Longitude>24.005514</MY.wgs84Longitude>
		//		<MY.coordinateRadius>100</MY.coordinateRadius>

		assertEquals(6666.0, gathering.getCoordinates().getLatMin(), 0);
		assertEquals(6667.0, gathering.getCoordinates().getLatMax(), 0);
		assertEquals(3333.0, gathering.getCoordinates().getLonMin(), 0);
		assertEquals(3334.0, gathering.getCoordinates().getLonMax(), 0);
		assertEquals(Type.YKJ, gathering.getCoordinates().getType());
		assertEquals(100, gathering.getCoordinates().getAccuracyInMeters().intValue()); // Note: contradictory with actual accuracy, but we pass on the original value; accuracy will be interpreted when reading out pipe
		assertEquals("6666:3333 YKJ (100m)", gathering.getCoordinatesVerbatim());

		Unit unit = gathering.getUnits().get(0);

		assertFalse(unit.isTypeSpecimen());

		assertEquals(null, unit.getRecordBasis());
		assertEquals("Parus major x eskolus", unit.getTaxonVerbatim());
		assertEquals("Piirainen, Esko", unit.getDet());
		assertEquals("EP", unit.getAuthor());

		assertEquals("[FOOBAR]", unit.getKeywords().toString());

		assertEquals("http://tun.fi/MY.samplingMethod : http://tun.fi/MY.samplingMethodSoilsample", gathering.getFacts().stream().filter(f->f.getFact().equals("http://tun.fi/MY.samplingMethod")).map(f->f.toString()).findFirst().get());
		assertEquals("http://tun.fi/MY.samplingMethodSoilsample", unit.getSamplingMethod());
		// System.out.println(root.toJSON().beautify());
	}

	private void assertContains(String string, List<String> list) {
		for (String s : list) {
			if (s.equals(string)) return;
		}
		Assert.fail(string + " not found from " + list);
	}

	@Test
	public void document_containing_many_documents_and_many_gatherings_in_document() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-3.xml"), new Qname("KE.3"));
		assertEquals(2, list.size());
		DwRoot root1 = list.get(0);
		DwRoot root2 = list.get(1);

		assertEquals("http://tun.fi/EA4.0WB", root1.getDocumentId().toURI());
		assertEquals("http://tun.fi/EA4.0XX", root2.getDocumentId().toURI());

		Document doc1 = root1.getPublicDocument();
		Document doc2 = root2.getPublicDocument();

		assertEquals(2, doc1.getGatherings().size());
		assertEquals(3, doc2.getGatherings().size());
	}

	@Test
	public void misc_char_testing() throws Exception {
		String s = "asdasdasdda";
		assertEquals(11, s.length());
		if (s.length() > 10) {
			s = s.substring(0, 10);
		}
		assertEquals(10, s.length());

		s = "öä Дёко попюльо пошжим шэа";
		byte[] utf8 = s.getBytes("UTF-8");
		assertEquals(26, s.length());
		assertEquals(48, utf8.length);

	}

	@Test
	public void trimming_fact_and_notes_length() throws UnsupportedEncodingException, CriticalParseFailure {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("1"), new Qname("HR.1"));

		String longString = "Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа "+
				"Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа Дёко попюльо пошжим шэа";

		doc.setNotes(longString);
		doc.addFact("f", longString);

		assertEquals(5000, doc.getNotes().getBytes("UTF-8").length);
		assertEquals(985, doc.getFacts().get(0).getValue().getBytes("UTF-8").length);
	}

	@Test
	public void parsing_images() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-image-1.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());

		Document doc = list.get(0).getPublicDocument();
		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);

		assertEquals(3, g.getUnits().size());

		assertEquals(2, doc.getMedia().size());
		assertEquals(2, g.getMedia().size());

		for (Unit u : g.getUnits()) {
			if (u.getUnitId().toString().equals("MY.487936") || u.getUnitId().toString().equals("MY.foobarSpecimen")) {
				assertEquals(3, u.getMedia().size());
			} else if (u.getUnitId().toString().equals("MY.foobarObservation")) {
				assertEquals(0, u.getMedia().size());
			} else {
				throw new IllegalStateException();
			}
		}

		MediaObject docMedia1 = doc.getMedia().get(0);
		MediaObject docMedia2 = doc.getMedia().get(1);
		MediaObject gatMedia1 = g.getMedia().get(0);
		MediaObject gatMedia2 = g.getMedia().get(1);
		List<MediaObject> u1Media = g.getUnits().get(0).getMedia();
		List<MediaObject> u2Media = g.getUnits().get(1).getMedia();

		assertEquals("https://img.com/1/full.jpg", docMedia1.getFullURL()); // marked as label using keyword
		assertEquals("https://img.com/label_in_name_not_as_keyword/foobar.jpg", docMedia2.getFullURL()); // marked as label in filename (not as keyword)

		assertEquals("https://img.com/habitat_in_name_not_as_keyword/foobar.jpg", gatMedia1.getFullURL()); // marked as habitat image in filename (not as keyword)
		assertEquals("https://img.com/hab_as_keyword/foobar.jpg", gatMedia2.getFullURL()); // marked as habitat image using keyword

		assertEquals("https://img.com/prim_as_keyword/foobar.jpg", u1Media.get(0).getFullURL()); // primary before other
		assertEquals("https://img.com/nonprim_specimenimage/foobar.jpg", u1Media.get(1).getFullURL());
		assertEquals("https://digi.fi/foobar.jpg", u1Media.get(2).getFullURL());

		assertEquals(u1Media.toString(), u2Media.toString());

		String expected = "MediaObject [id=null, mediaType=IMAGE, fullURL=https://digi.fi/foobar.jpg, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=Digitarium; Digi Digitoija, copyrightOwner=Luomus, University of Eastern Finland, licenseId=MZ.intellectualRightsARR, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null]";
		assertEquals(expected, u1Media.get(2).toString()); // license qname resolved from verbatim license "copyright"

		expected = "MediaObject [id=null, mediaType=IMAGE, fullURL=https://img.com/prim_as_keyword/foobar.jpg, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null null, copyrightOwner=null, licenseId=null, caption=null, keywords=[specimen, primary], type=null, fullResolutionMediaAvailable=null]";
		assertEquals(expected, u1Media.get(0).toString()); // license id left empty, verbatim license abbrevation given

		expected = "MediaObject [id=null, mediaType=IMAGE, fullURL=https://img.com/1/full.jpg, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=[label, skull], type=null, fullResolutionMediaAvailable=null]";
		assertEquals(expected, docMedia1.toString()); // license using license id (qname)
	}

	@Test
	public void parsing_media_type_3d_model() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-3dmodel-1.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());

		Document doc = list.get(0).getPublicDocument();
		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);

		assertEquals(1, g.getUnits().size());

		assertEquals(0, doc.getMedia().size());
		assertEquals(0, g.getMedia().size());

		Unit u = g.getUnits().get(0);
		assertEquals(1, u.getMedia().size());

		assertEquals("" +
				"MediaObject [id=MM.1, mediaType=MODEL, fullURL=null, thumbnailURL=https://test.com/video_thumb.jpg, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=https://test.com/video.mp4, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=true]",
				u.getMedia().get(0).toString());
	}

	@Test
	public void unitGathering() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-vanhahatikka.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());

		Document doc = list.get(0).getPublicDocument();
		assertEquals(2, doc.getGatherings().size());

		Gathering g1 = doc.getGatherings().get(0);
		assertEquals(1, g1.getUnits().size());
		assertEquals("DateRange [begin=1994-06-27, end=1994-06-27]", g1.getEventDate().toString());
		assertEquals("6707:3098 FI KKJ27", g1.getCoordinatesVerbatim());

		Gathering g2 = doc.getGatherings().get(1);
		assertEquals(5, g2.getUnits().size());
		assertEquals("DateRange [begin=1994-06-29, end=1994-06-29]", g2.getEventDate().toString());
		assertEquals("6708:3098 FI KKJ27", g2.getCoordinatesVerbatim());
	}

	@Test
	public void deleteRequest() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-delete-request.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
		assertEquals(true, root.isDeleteRequest());
	}

	@Test
	public void typeSpecimen() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf_xml-typespecimen.xml"), new Qname("KE.123"));
		Unit u = list.get(0).getPublicDocument().getGatherings().get(0).getUnits().get(0);
		assertEquals(1, u.getTypes().size());
		TypeSpecimen t = u.getTypes().get(0);
		assertEquals(
				"Identification [id=MY.2449502, taxon=Chrysis borealis, author=Paukkunen, Ødegaard & Soon, 2015, taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]",
				t.toString());
		assertEquals("MY.typeVerificationVerified", t.getVerification().toString());
		assertEquals("MY.typeStatusParatype", t.getStatus().toString());
		assertEquals(true, t.verified());
		assertEquals("http://zookeys.pensoft.net/articles.php?id=6164&display_type=list&element_type=9", t.getNotes());
		assertEquals(
				"Paukkunen J, Berg A, Soon V, Ødegaard F, Rosa P (2015) An illustrated key to the cuckoo wasps (Hymenoptera, Chrysididae) of the Nordic and Baltic countries, with description of a new species. ZooKeys 548: 1-116.",
				t.getBasionymePublication());
		assertEquals("Tyyppi Tyytti", t.getTypif());
		assertEquals("2005", t.getTypifDate());
		assertEquals("[http://tun.fi/MY.typeSeriesID : TP1]", t.getFacts().toString());
	}

	@Test
	public void issues() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-with-issues.xml"), new Qname("KE.123"));
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		Unit u = g.getUnits().get(0);

		assertEquals(Quality.Issue.INVALID_DATE, g.getQuality().getTimeIssue().getIssue());
		assertEquals(Quality.Source.ORIGINAL_DOCUMENT, g.getQuality().getTimeIssue().getSource());

		assertEquals(Quality.Issue.INVALID_GEO, g.getQuality().getLocationIssue().getIssue());
		assertEquals(Quality.Source.ORIGINAL_DOCUMENT, g.getQuality().getLocationIssue().getSource());

		assertEquals(TaxonConfidence.UNSURE, u.getReportedTaxonConfidence());
	}

	@Test
	public void timeparsing_issue() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-datebug.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.createQuality().getTimeIssue());
		assertEquals("DateRange [begin=1966-07-23, end=1966-07-23]", g.getEventDate().toString());
	}

	@Test
	public void timeparsing_issue_2() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-datebug2.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals(null, g.getEventDate());
	}

	@Test
	public void short_euref() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-short-euref.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("7120000.0 : 7130000.0 : 660000.0 : 670000.0 : EUREF : null", g.getCoordinates().toString());
		assertEquals("712:66 EUREF", g.getCoordinatesVerbatim());
	}

	@Test
	public void no_date_begin() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-date-begin-missing.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(null, g.getQuality());
		assertEquals("DateRange [begin=null, end=2012-06-23]", g.getEventDate().toString());
		assertEquals(" - 2012-06-23", g.getDisplayDateTime());
	}

	@Test
	public void samples() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-samples.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);
		assertEquals(5, u.getSamples().size());
		Sample s = u.getSamples().get(0);
		assertEquals("luomus:HLA.155049#P4", s.getSampleId().toString());
		assertEquals(0, s.getSampleOrder());
		assertEquals("HR.2831", s.getCollectionId().toString());
		assertEquals("[http://tun.fi/GX.8057, Labcode: Hia214 1:10]", s.getKeywords().toString());
		assertEquals("notes, Cond, Qnotes", s.getNotes());
		assertEquals(new Qname("MF.qualityHigh").toURI(), s.getQuality());
		assertEquals(new Qname("MY.statusOk").toURI(), s.getStatus());
		assertEquals(new Qname("MF.preparationTypeDNAExtract").toURI(), s.getType());
		assertEquals(new Qname("MF.materialGenomicDNA").toURI(), s.getMaterial());

		String expectedFacts = "" +
				"http://tun.fi/MF.datasetID : http://tun.fi/GX.8057\n" +
				"http://tun.fi/MF.preparationType : http://tun.fi/MF.preparationTypeDNAExtract\n" +
				"http://tun.fi/MF.quality : http://tun.fi/MF.qualityHigh\n" +
				"http://tun.fi/MF.sampleLocation : CryoCube2\n" +
				"http://tun.fi/MF.status : http://tun.fi/MY.statusOk\n" +
				"http://tun.fi/MF.preservation : http://tun.fi/MY.preservationFrozenMinus80C\n" +
				"http://tun.fi/MF.additionalIDs : Labcode: Hia214 1:10\n" +
				"http://tun.fi/MF.qualityCheckMethod : http://tun.fi/MF.qualityCheckMethodNanoDrop\n" +
				"http://tun.fi/MF.collectionID : http://tun.fi/HR.2831\n" +
				"http://tun.fi/MF.qualityCheckDate : 16.08.2017\n" +
				"http://tun.fi/MF.elutionMedium : http://tun.fi/MF.elutionMediumElutionBuffer\n" +
				"http://tun.fi/MF.material : http://tun.fi/MF.materialGenomicDNA\n" +
				"http://tun.fi/MF.preparationMaterials : http://tun.fi/MF.preparationMaterialsNucleospinTissueKit\n" +
				"http://tun.fi/MY.DNAVolumeMicroliters : 90\n" +
				"http://tun.fi/MY.DNARatioOfAbsorbance260And280 : 1.95\n" +
				"http://tun.fi/MY.DNAConcentrationNgPerMicroliter : 5.7";
		assertEquals(expectedFacts, debugFacts(s));
	}

	@Test
	public void type_no_identification() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-type-no-identification.xml"), new Qname("KE.123"));
		assertEquals(1, list.size());
		assertEquals(1, list.get(0).getPublicDocument().getGatherings().size());
		Gathering g = list.get(0).getPublicDocument().getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);
		assertEquals("Agriotes strigilatus var. moppius", u.getTaxonVerbatim());
		assertEquals("Schwarz", u.getAuthor());
		assertEquals(true, u.isTypeSpecimen());
	}

	@Test
	public void sortingIdentificationEvents() {
		IdentificationEvent e1 = new IdentificationEvent();
		IdentificationEvent e2 = new IdentificationEvent();

		e1.setId(new Qname("E1"));
		e2.setId(new Qname("E2"));

		List<IdentificationEvent> list = Utils.list(e1, e2);
		assertEquals("[E1, E2]", ids(list));

		Collections.sort(list);
		assertEquals("[E1, E2]", ids(list));

		e2.setDetDate("2001");
		assertEquals("2001", e2.formattedDetDate());
		Collections.sort(list);
		assertEquals("[E2, E1]", ids(list));

		e1.setDetDate("2020");
		e2.setDetDate("2001");
		assertEquals("2020", e1.formattedDetDate());
		Collections.sort(list);
		assertEquals("[E1, E2]", ids(list));

		e1.setDetDate("2020");
		e2.setDetDate("6.11.2021");
		assertEquals("20211106", e2.formattedDetDate());
		Collections.sort(list);
		assertEquals("[E2, E1]", ids(list));

		e1.setDetDate("6.6.2020");
		e2.setDetDate("2021");
		assertEquals("20200606", e1.formattedDetDate());
		assertEquals("2021", e2.formattedDetDate());
		Collections.sort(list);
		assertEquals("[E2, E1]", ids(list));

		e1.setDetDate("2000");
		e2.setDetDate("6.11.2021");
		Collections.sort(list);
		assertEquals("[E2, E1]", ids(list));

		e1.setDetDate("6.6.2020");
		e2.setDetDate("2000");
		Collections.sort(list);
		assertEquals("[E1, E2]", ids(list));

		e1.setDetDate("6.11.2021");
		e2.setDetDate("6.11.2021");
		Collections.sort(list);
		assertEquals("[E1, E2]", ids(list));
	}

	private String ids(List<IdentificationEvent> list) {
		return list.stream().map(i->i.getId().toString()).collect(Collectors.toList()).toString();
	}

	@Test(expected = IllegalStateException.class)
	public void test_unit_sorting_NullInput() {
		List<Unit> units = null;
		RdfXmlHarmonizer.sort(units);
	}

	@Test
	public void test_unit_sorting_EmptyList() {
		List<Unit> units = new ArrayList<>();
		ArrayList<Unit> expected = new ArrayList<>();
		ArrayList<Unit> actual = RdfXmlHarmonizer.sort(units);
		assertEquals(expected, actual);
	}

	@Test
	public void test_unit_sorting_Sort() throws CriticalParseFailure {
		Unit unit1 = unit(false, RecordBasis.PRESERVED_SPECIMEN);
		Unit unit2 = unit(true, RecordBasis.PRESERVED_SPECIMEN);
		Unit unit3 = unit(false, RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		List<Unit> units = Utils.list(unit1, unit2, unit3);
		List<Unit> expected = Utils.list(unit1, unit3, unit2);
		ArrayList<Unit> actual = RdfXmlHarmonizer.sort(units);
		assertEquals(expected, actual);

		Gathering g = new Gathering(new Qname("G"));
		g.setUnits(actual);
		assertEquals(0, actual.get(0).getUnitOrder());
		assertEquals(1, actual.get(1).getUnitOrder());
		assertEquals(2, actual.get(2).getUnitOrder());
	}

	private Unit unit(Boolean primary, RecordBasis recordBasis) throws CriticalParseFailure {
		Unit u = new Unit(new Qname(Utils.generateGUID()));
		u.setPrimarySpecimen(primary);
		u.setRecordBasis(recordBasis);
		return u;
	}

	private String addVerificationStatus(String status) {
		Node testData = getTestDataAsDocument("rdf-xml-testdata-1.xml");
		Node verificationStatus = new Node("MY.verificationStatus").addAttribute("rdf:resource", new Qname(status).toURI());
		testData.getNode("MY.document").addChildNode(verificationStatus);

		String xml = new XMLWriter(testData).generateXML();
		return xml;
	}

	@Test
	public void do_not_set_sourcetag_for_documents_without_verification_status() throws Exception {
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(getTestData("rdf-xml-testdata-1.xml"), new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();
		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertEquals(0, unit.getSourceTags().size());
	}

	@Test
	public void set_sourcetag_uncetain_for_documents_with_verification_status_set_verify() throws Exception {
		String xml = addVerificationStatus("MY.verificationStatusVerify");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();
		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertEquals(1, unit.getSourceTags().size());
		assertTrue(unit.getSourceTags().contains(Tag.EXPERT_TAG_UNCERTAIN));
	}

	@Test
	public void set_sourcetag_expert_verfied_for_documents_with_verification_status_set_ok() throws Exception {
		String xml = addVerificationStatus("MY.verificationStatusOk");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();
		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertEquals(1, unit.getSourceTags().size());
		assertTrue(unit.getSourceTags().contains(Tag.EXPERT_TAG_VERIFIED));
	}

	private String addUnreliableFields(String file, String fields) {
		Node testData = getTestDataAsDocument(file);
		Node unreliableFields = new Node("MY.unreliableFields").setContents(fields);
		testData.getNode("MY.document").addChildNode(unreliableFields);

		String xml = new XMLWriter(testData).generateXML();
		return xml;
	}

	@Test
	public void set_sourcetag_uncertain_for_documents_with_unreliable_unit_identification_field() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-1.xml", "MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon]");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();
		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertEquals(1, unit.getSourceTags().size());
		assertTrue(unit.getSourceTags().contains(Tag.EXPERT_TAG_UNCERTAIN));
	}

	@Test
	public void do_not_set_sourcetag_for_documents_with_unreliable_unit_identification_field_and_multiple_units() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-2.xml", "MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon] MYGathering[0][MYUnit][1][MYIdentification][0][MYTaxon]");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();
		Gathering gathering = document.getGatherings().get(0);

		assertEquals(2, gathering.getUnits().size());

		Unit unit1 = gathering.getUnits().get(0);
		Unit unit2 = gathering.getUnits().get(1);

		assertEquals(0, unit1.getSourceTags().size());
		assertEquals(0, unit2.getSourceTags().size());
	}

	@Test
	public void do_not_set_sourcetag_for_documents_with_unreliable_unit_identification_field_and_multiple_gatherings() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-5.xml", "MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon] MYGathering[1][MYUnit][0][MYIdentification][0][MYTaxon]");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();

		assertEquals(2, document.getGatherings().size());

		Gathering gathering1 = document.getGatherings().get(0);
		Gathering gathering2 = document.getGatherings().get(1);

		Unit unit1 = gathering1.getUnits().get(0);
		Unit unit2 = gathering2.getUnits().get(0);

		assertEquals(0, unit1.getSourceTags().size());
		assertEquals(0, unit2.getSourceTags().size());
	}

	@Test
	public void set_sourcetag_uncertain_for_documents_with_unreliable_unit_identification_field_and_multiple_identifications_counts_match() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-4.xml", "MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon] MYGathering[0][MYUnit][0][MYIdentification][1][MYTaxon]");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();

		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertTrue(unit.getSourceTags().contains(Tag.EXPERT_TAG_UNCERTAIN));
	}

	@Test
	public void do_not_set_sourcetag_for_documents_with_unreliable_unit_identification_field_and_multiple_identifications_counts_mismatch() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-4.xml", "MYGathering[0][MYUnit][0][MYIdentification][0][MYTaxon]");

		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);

		Document document = root.getPublicDocument();

		Gathering gathering = document.getGatherings().get(0);

		Unit unit = gathering.getUnits().get(0);

		assertEquals(0, unit.getSourceTags().size());
	}
	
	@Test
	public void do_not_set_sourcetag_for_documents_with_multiple_units_and_unrelated_unreliable_field() throws Exception {
		String xml = addUnreliableFields("rdf-xml-testdata-2.xml", "MYGathering[0][MYSubstrate]");
	
		List<DwRoot> list = rdfXmlHarmonizer.harmonize(xml, new Qname("KE.3"));
		assertEquals(1, list.size());
		DwRoot root = list.get(0);
	
		Document document = root.getPublicDocument();
	
		Gathering gathering = document.getGatherings().get(0);
	
		Unit unit = gathering.getUnits().get(0);
	
		assertEquals(0, unit.getSourceTags().size());
	}
}
