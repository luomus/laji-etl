package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.dao.vertica.TargetEntity;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;

public class TaxonLinkingServiceTests {

	private TaxonLinkingService service;

	@Before
	public void before() {
		Map<String, Qname> lookup = new HashMap<>();
		for (String s : TaxonLinkingService.toTargetLookupStrings("Pelecanus", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.1"), true)) {
			lookup.put(s, new Qname("MX.1"));
		}

		for (String s : TaxonLinkingService.toTargetLookupStrings("susi canis lupus", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.3"), true)) {
			lookup.put(s, new Qname("MX.3"));
		}
		for (String s : TaxonLinkingService.toTargetLookupStrings("susi", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.3"), true)) {
			lookup.put(s, new Qname("MX.3"));
		}
		for (String s : TaxonLinkingService.toTargetLookupStrings("canis lupus", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.3"), true)) {
			lookup.put(s, new Qname("MX.3"));
		}
		this.service = new TaxonLinkingService(lookup);
	}

	@Test
	public void lookup_strings() {
		assertEquals("sääski itikka", TaxonLinkingService.cleanTargetName("sääski (itikka)"));
		assertEquals("sääskiitikka:null", TaxonLinkingService.toTargetLookupString("sääski (itikka)"));
		assertEquals("[" +
				"sääskiitikka:null, sääskiitikka:null:MX.1:COUNTRY-FI, sääskiitikka:null:MX.1:null, sääskiitikka:null:null:COUNTRY-FI, sääskiitikka:null:null:null, " +
				"sääskiitikka:MR.1, sääskiitikka:MR.1:MX.1:COUNTRY-FI, sääskiitikka:MR.1:MX.1:null, sääskiitikka:MR.1:null:COUNTRY-FI, sääskiitikka:MR.1:null:null]",
				TaxonLinkingService.toTargetLookupStrings("sääski (itikka)", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.1"), true).toString());

		assertEquals("[" +
				"sääskiitikka:null, sääskiitikka:null:MX.1:null, sääskiitikka:null:null:null, " +
				"sääskiitikka:MR.1, sääskiitikka:MR.1:MX.1:null, sääskiitikka:MR.1:null:null]",
				TaxonLinkingService.toTargetLookupStrings("sääski (itikka)", Const.MASTER_CHECKLIST_QNAME, new Qname("MX.1"), false).toString());

		assertEquals("[koivunlehti1cm:MR.84, koivunlehti1cm:MR.84:MX.1:null, koivunlehti1cm:MR.84:null:null]",
				TaxonLinkingService.toTargetLookupStrings("koivunlehti 1cm", new Qname("MR.84"), new Qname("MX.1"), false).toString());

		assertEquals("[" +
				"mx1:null, mx1:null:MX.1:null, mx1:null:null:null, " +
				"mx1:MR.1, mx1:MR.1:MX.1:null, mx1:MR.1:null:null, " +
				"http://tunfi/mx1:null, http://tunfi/mx1:null:MX.1:null, http://tunfi/mx1:null:null:null, "+
				"http://tunfi/mx1:MR.1, http://tunfi/mx1:MR.1:MX.1:null, http://tunfi/mx1:MR.1:null:null]",
				TaxonLinkingService.toTaxonIdLookupStrings(new Qname("MX.1"), Const.MASTER_CHECKLIST_QNAME).toString());

		assertEquals("haigrosaibi", TaxonLinkingService.cleanTargetName("Haigrosáibi"));
		assertEquals("hygrokybe", TaxonLinkingService.cleanTargetName("Hýgrokybe"));
		assertEquals("mycena lohjaensis", TaxonLinkingService.cleanTargetName("Mycena lohjaënsis"));
		assertEquals("epichloe typhina", TaxonLinkingService.cleanTargetName("Epichloë typhina"));
	}

	@Test
	public void get_by_taxonid() {
		// TODO service.getTaxonId(taxonId)
	}

	@Test
	public void get_by_targetname() {
		// TODO service.getTaxonId(targetName)
	}

	@Test
	public void taxon_linking_resolving() throws CriticalParseFailure {
		Unit unit = new Unit(new Qname("U"));
		unit.setTaxonVerbatim("susi (canis lupus)");
		Gathering gathering = new Gathering(new Qname("G"));
		gathering.createInterpretations().setCountry(Const.FINLAND);

		assertEquals("MX.3", service.getTaxonId(unit, gathering, true).toString());

		unit.setTaxonVerbatim("susi");
		unit.setAutocompleteSelectedTaxonId(new Qname("MX.3"));

		assertEquals("MX.3", service.getTaxonId(unit, gathering, true).toString());

		unit.setTaxonVerbatim("canis lupus lupsus");
		unit.setAutocompleteSelectedTaxonId(null);

		assertEquals(null, service.getTaxonId(unit, gathering, true)); // hot linking should not make non-exact matches


		unit.setTaxonVerbatim("canis lupus");
		gathering.getInterpretations().setCountry(new Qname("ML.123456foo"));

		assertEquals("MX.3", service.getTaxonId(unit, gathering, true).toString());

		unit.setTaxonVerbatim("Pelecanus");

		assertEquals("MX.1", service.getTaxonId(unit, gathering, true).toString());

		gathering.getInterpretations().setCountry(Const.FINLAND);

		assertEquals("MX.1", service.getTaxonId(unit, gathering, true).toString());

		// TODO test annotated taxon id
		// TODO test reported taxon id
	}

	@Test
	public void target_entity_building_and_taxon_resolving() throws CriticalParseFailure {
		Unit unit = new Unit(new Qname("U"));
		unit.setTaxonVerbatim("susi (canis lupus)");
		Gathering gathering = new Gathering(new Qname("G"));
		gathering.createInterpretations().setCountry(Const.FINLAND);

		TargetEntity e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("susi canis lupus:null:null:COUNTRY-FI", e.getId());
		assertEquals("susi canis lupus", e.getTargetLowercase());
		assertEquals("susicanislupus:null:null:COUNTRY-FI", e.getTargetlookup());
		assertEquals(3, e.getTaxonKey().intValue());
		assertEquals(null, e.getNotExactTaxonMatch());
		assertEquals(null, e.getReferenceKey());

		unit.setTaxonVerbatim("susi");
		unit.setAutocompleteSelectedTaxonId(new Qname("MX.3"));

		e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("susi:null:MX.3:COUNTRY-FI", e.getId());
		assertEquals("susi", e.getTargetLowercase());
		assertEquals("susi:null:MX.3:COUNTRY-FI", e.getTargetlookup());
		assertEquals(3, e.getTaxonKey().intValue());

		unit.setTaxonVerbatim("canis lupus lupsus");
		unit.setAutocompleteSelectedTaxonId(null);

		e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("canis lupus lupsus:null:null:COUNTRY-FI", e.getId());
		assertEquals("canis lupus lupsus", e.getTargetLowercase());
		assertEquals("canislupuslupsus:null:null:COUNTRY-FI", e.getTargetlookup());
		assertEquals(3, e.getTaxonKey().intValue());
		assertEquals(true, e.getNotExactTaxonMatch());

		gathering.getInterpretations().setCountry(new Qname("ML.123456foo"));

		e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("canis lupus lupsus:null:null:null", e.getId());
		assertEquals("canis lupus lupsus", e.getTargetLowercase());
		assertEquals("canislupuslupsus:null:null:null", e.getTargetlookup());
		assertEquals(3, e.getTaxonKey().intValue());
		assertEquals(true, e.getNotExactTaxonMatch());

		unit.setTaxonVerbatim("Pelecanus");
		e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("pelecanus:null:null:null", e.getId());
		assertEquals("pelecanus", e.getTargetLowercase());
		assertEquals("pelecanus:null:null:null", e.getTargetlookup());
		assertEquals(1, e.getTaxonKey().intValue());
		assertEquals(null, e.getNotExactTaxonMatch());

		gathering.getInterpretations().setCountry(Const.FINLAND);
		e = service.getTargetEntity(unit, gathering, true, null);
		assertEquals("pelecanus:null:null:COUNTRY-FI", e.getId());
		assertEquals("pelecanus", e.getTargetLowercase());
		assertEquals("pelecanus:null:null:COUNTRY-FI", e.getTargetlookup());
		assertEquals(1, e.getTaxonKey().intValue());
		assertEquals(null, e.getNotExactTaxonMatch());

		// TODO test annotated taxon id
		// TODO test reported taxon id
	}

	@Test
	public void reprocessing_target_entity_with_new_linkings() {
		// TODO service.getTargetEntity(target)
	}

	@Test
	public void target_checklist_entity_from_target() {
		// TODO service.getTargetChecklistEntity(checklist, target)
	}

}
