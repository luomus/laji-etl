package fi.laji.datawarehouse.etl.models;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.PersonBaseEntity;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.Utils;

public class TestDAO extends DAOImple {

	private static final Config config = TestConfig.getConfig();

	public static enum Mode { REAL_COLLECTIONS }

	public static final Qname HIGH_QUALITY_COLLECTION = new Qname("HR.1");
	public static final Qname LOW_QUALITY_COLLECTION = new Qname("HR.2");
	public static final Qname NAMED_PLACE_ID = new Qname("NMP.1");
	public static final String NAMEDPLACE_MUNICIPALITY = "Inkoo";
	public static final String NAMED_PLACE_NAME = "123, Paikkainen paikka";
	public static final SingleCoordinates NAMED_PLACE_YKJ10 = new SingleCoordinates(666, 333, Type.YKJ);

	private final Mode mode;

	public TestDAO(Mode mode, Class<?> createdBy) {
		super(config, "FOO", new ErrorReportingToSystemErr(), new ThreadStatuses(), createdBy);
		this.mode = mode;
	}

	public TestDAO(Class<?> createdBy) {
		this(null, createdBy);
	}

	@Override
	protected QueryLogStorer initQueryLogger() {
		return null;
	}

	@Override
	public Map<String, CollectionMetadata> getCollections() {
		if (Mode.REAL_COLLECTIONS.equals(mode)) return super.getCollections();
		return getFakeCollections();
	}

	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlaces() {
		return getNamedPlacesForceReload();
	}

	@Override
	public Map<Qname, NamedPlaceEntity> getNamedPlacesForceReload() {
		Map<Qname, NamedPlaceEntity> places = new HashMap<>();
		NamedPlaceEntity e = new NamedPlaceEntity(NAMED_PLACE_ID.toURI());
		e.setMunicipalityDisplayName(NAMEDPLACE_MUNICIPALITY);
		e.setYkj10km(NAMED_PLACE_YKJ10);
		e.setName(NAMED_PLACE_NAME);
		places.put(NAMED_PLACE_ID, e);
		return places;
	}

	public static Map<String, CollectionMetadata> getFakeCollections() {
		Map<String, CollectionMetadata> collections = new HashMap<>();
		CollectionMetadata highQ = new CollectionMetadata(HIGH_QUALITY_COLLECTION, new LocalizedText().set("en", "High quality"), null);
		CollectionMetadata lowQ = new CollectionMetadata(LOW_QUALITY_COLLECTION, new LocalizedText().set("en", "Low quality"), null);
		CollectionMetadata invasiveC = new CollectionMetadata(Const.INVASIVE_ALIEN_SPECIES_CONTROL_COLLECTION_ID, new LocalizedText().set("en", "invasive"), null);

		highQ.setCollectionQuality(new Qname("MY.collectionQualityEnum3"));
		lowQ.setCollectionQuality(new Qname("MY.collectionQualityEnum1"));

		highQ.setIntellectualRights(new Qname("MY.licenseHighQ"));
		lowQ.setIntellectualRights(new Qname("MY.licenseLowQ"));
		invasiveC.setCollectionQuality(new Qname("MY.collectionQualityEnum2"));

		collections.put(HIGH_QUALITY_COLLECTION.toURI(), highQ);
		collections.put(LOW_QUALITY_COLLECTION.toURI(), lowQ);
		collections.put(new Qname("HR.48").toURI(), lowQ);
		collections.put(new Qname("HR.60").toURI(), lowQ);
		collections.put(new Qname("HR.39").toURI(), lowQ);
		collections.put(invasiveC.getQname().toURI(), invasiveC);
		collections.put(Interpreter.SPRING_MONITORING.toURI(), lowQ);

		return collections;
	}

	@Override
	protected void log(Qname source, Class<?> phase, String type, String identifier, String message) {
		System.out.println(Utils.debugS("LOGGING", source, phase.getSimpleName(), type, identifier, message));
	}

	@Override
	public VerticaDimensionsDAO getVerticaDimensionsDAO() {
		return new VerticaDimensionsDAO() {

			@Override
			public void updateNameableEntityNames() {
				// Auto-generated method stub

			}

			@Override
			public void updateEntities(List<BaseEntity> entityList) {
				// Auto-generated method stub

			}

			@Override
			public void startTaxonAndPersonReprosessing() {
				// Auto-generated method stub

			}

			@Override
			public void performCleanUp() {
				// Auto-generated method stub

			}

			@Override
			public void insertEntities(List<BaseEntity> entityList) {
				// Auto-generated method stub

			}

			@Override
			public VerticaDimensionsTaxonService getTaxonService() {
				// Auto-generated method stub
				return new VerticaDimensionsTaxonService() {

					@Override
					public void switchTaxonTempToActual() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void insertToTaxonTempTable(Taxon taxon) throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void generateOccurrenceTypeLinkings() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void generateInformalGroupLinkings() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void generateHabitatLinkings() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void generateAdminStatusLinkings() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void emptyTaxonTempTable() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void close() {
						// Auto-generated method stub

					}

					@Override
					public void generateTaxonSetLinkings() throws Exception {
						// Auto-generated method stub

					}
				};
			}

			@Override
			public VerticaDimensionsPersonService getPersonSevice() {
				// Auto-generated method stub
				return new VerticaDimensionsPersonService() {

					@Override
					public void switchPersonTempToActual() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void insertToPersonTempTable(PersonBaseEntity e) throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void emptyPersonTempTable() throws Exception {
						// Auto-generated method stub

					}

					@Override
					public void close() {
						// Auto-generated method stub

					}
				};
			}

			@Override
			public String getIdForKey(Long value, String field) {
				// Auto-generated method stub
				return null;
			}

			@Override
			public Long getEnumKey(Enum<?> enumeration) {
				// Auto-generated method stub
				return null;
			}

			@Override
			public List<NameableEntity> getEntities(Class<? extends NameableEntity> nameableEntityClass) {
				// Auto-generated method stub
				return null;
			}

			@Override
			public void storeDocumentIds(Iterator<String> iterator) {
				// Auto-generated method stub

			}

			@Override
			public void storeSplittedDocumentIds(Collection<SplittedDocumentIdEntity> values) {
				// Auto-generated method stub

			}
		};
	}

	@Override
	public String getPolygonSearch(long id) {
		if (id == 1) return "POLYGON((556320 7167999,557598 7166987,557885 7169572,556320 7167999))";
		return null;
	}

}