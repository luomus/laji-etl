package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.NamedPlaceEntity;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class DwModelTests {

	@Test
	public void test_end_date_before_start_date() throws Exception {
		DateRange r = new DateRange(DateUtils.convertToDate("1.1.2000"), DateUtils.convertToDate("31.12.1999"));
		assertEquals("DateRange [begin=1999-12-31, end=2000-01-01]", r.toString()); // turned around

		r = new DateRange(new Date(1000*1000), new Date(1000*1000));
		assertEquals("DateRange [begin=1970-01-01, end=1970-01-01]", r.toString());

		r = new DateRange(new Date(1000*1000), new Date(1001*1000));
		assertEquals("DateRange [begin=1970-01-01, end=1970-01-01]", r.toString());
	}

	@Test
	public void test_end_not_null_start_is_null() throws DataValidationException {
		DateRange date = new DateRange(null, new Date(1));
		assertEquals(null, date.getBegin());
		assertEquals("1970-01-01", DateUtils.format(date.getEnd(), "yyyy-MM-dd"));
	}

	@Test
	public void test_date_in_future() throws ParseException {
		try {
			new DateRange(DateUtils.convertToDate("01.01.2300"));
			Assert.fail("Should fail");
		} catch (DataValidationException e) {
			assertEquals("Begin is in the future: 2300-01-01", e.getMessage());
		}
	}

	@Test
	public void test_date_in_future_2() throws ParseException {
		try {
			new DateRange(DateUtils.convertToDate("01.01.1900"), DateUtils.convertToDate("01.01.2300"));
			Assert.fail("Should fail");
		} catch (DataValidationException e) {
			assertEquals("End is in the future: 2300-01-01", e.getMessage());
		}
	}

	@Test
	public void test_begin_date_in_future() throws DateValidationException {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		Date tomorrow = c.getTime();
		new DateRange(tomorrow);
		c.add(Calendar.DATE, 1);
		Date dayAfterTomorrow = c.getTime();
		try {
			new DateRange(dayAfterTomorrow);
			Assert.fail("Should fail");
		} catch (DataValidationException e) {
			assertEquals("Begin is in the future: " + DateUtils.format(dayAfterTomorrow, "yyyy-MM-dd"), e.getMessage());
		}
	}

	@Test
	public void test_end_date_in_future() throws ParseException, DateValidationException {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		Date tomorrow = c.getTime();
		new DateRange(DateUtils.convertToDate("1.1.2000"), tomorrow);
	}

	@Test
	public void test_end_date_in_future_next_year() throws ParseException {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, 1);
		Date nextYear = c.getTime();
		try {
			new DateRange(DateUtils.convertToDate("1.1.2000"), nextYear);
			Assert.fail("Should fail");
		} catch (DataValidationException e) {
			assertEquals("End is in the future: " + DateUtils.format(nextYear, "yyyy-MM-dd"), e.getMessage());
		}
	}

	@Test
	public void test_date_not_future() throws DataValidationException {
		new DateRange(new Date());
	}

	@Test
	public void factIntegerDouble() {
		Fact fact = new Fact("fact", "1,5");
		assertEquals("fact", fact.getFact());
		assertEquals("1,5", fact.getValue());
		assertEquals(1.5, fact.getDecimalValue().doubleValue(), 0);
		assertEquals(null, fact.getIntegerValue());

		fact = new Fact("fact", "1.0");
		assertEquals("1.0", fact.getValue());
		assertEquals(1.0, fact.getDecimalValue().doubleValue(), 0);
		assertEquals(null, fact.getIntegerValue());

		fact = new Fact("fact", "1");
		assertEquals("1", fact.getValue());
		assertEquals(1.0, fact.getDecimalValue().doubleValue(), 0);
		assertEquals(1, fact.getIntegerValue().intValue());
	}

	@Test
	public void coordinates_and_geometry() throws DataValidationException {
		Gathering g = Gathering.emptyGathering();
		g.setCoordinates(new Coordinates(50, 20, Type.WGS84));
		try {
			g.setGeo(new Geo(Type.WGS84));
			fail("Should throw exception");
		} catch (IllegalStateException e) {
			assertEquals("Should not have both coordinates and geography!", e.getMessage());
		}

		g = Gathering.emptyGathering();
		g.setGeo(new Geo(Type.WGS84));
		try {
			g.setCoordinates(new Coordinates(50, 20, Type.WGS84));
			fail("Should throw exception");
		} catch (IllegalStateException e) {
			assertEquals("Should not have both coordinates and geography!", e.getMessage());
		}
	}

	@Test
	public void euref_coordinates() throws DataValidationException {
		try {
			new Coordinates(123456789, 3, Type.EUREF);
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid Euref coordinates: 123456789 : 123456790 : 3 : 4 : EUREF", e.getMessage());
			assertEquals("Too long: 123456789, must not be longer than 7", e.getCause().getMessage());
		}

		try {
			new Coordinates(1234567, 123456, Type.EUREF);
			fail("Should throw exception");
		} catch (DataValidationException e) {
			assertEquals("Invalid Euref coordinates: 1234567 : 1234568 : 123456 : 123457 : EUREF", e.getMessage());
			assertEquals("Too small value: 1234567", e.getCause().getMessage());
		}

		new Coordinates(7234567, 323456, Type.EUREF);
	}

	@Test
	public void coordinatesIsPoint() throws DataValidationException {
		assertTrue(new Coordinates(60, 30, Type.WGS84).checkIfIsPoint());
		assertTrue(new Coordinates(60.3, 30.000001, Type.WGS84).checkIfIsPoint());
		assertFalse(new Coordinates(694, 364, Type.YKJ).checkIfIsPoint());
		assertTrue(new Coordinates(6948582, 3642503, Type.YKJ).checkIfIsPoint());
		assertTrue(new Coordinates(6948500, 6948501, 3642500, 3642501, Type.YKJ).checkIfIsPoint());
	}

	@Test
	public void documentIdUniqueness() throws CriticalParseFailure {
		Document d = new Document(Concealment.PRIVATE, new Qname("KE.1"), new Qname("D.1"), new Qname("HR.1"));
		d.validateIncomingIds();
		d.addGathering(new Gathering(new Qname("G.1")));
		d.validateIncomingIds();
		d.addGathering(new Gathering(new Qname("D.1")));
		try {
			d.validateIncomingIds();
			fail("Should throw");
		} catch (CriticalParseFailure e) {
			assertEquals("Id exists in same document multiple times: D.1", e.getMessage());
		}
		d.getGatherings().remove(1);
		d.validateIncomingIds();
		d.addGathering(new Gathering(new Qname("G.1")));
		try {
			d.validateIncomingIds();
			fail("Should throw");
		} catch (CriticalParseFailure e) {
			assertEquals("Id exists in same document multiple times: G.1", e.getMessage());
		}
		d.getGatherings().remove(1);
		d.validateIncomingIds();
		Gathering g = d.getGatherings().get(0);
		g.addUnit(new Unit(new Qname("U.1")));
		d.validateIncomingIds();
		g.addUnit(new Unit(new Qname("U.2")));
		d.validateIncomingIds();
		g.addUnit(new Unit(new Qname("U.1")));
		try {
			d.validateIncomingIds();
			fail("Should throw");
		} catch (CriticalParseFailure e) {
			assertEquals("Id exists in same document multiple times: U.1", e.getMessage());
		}
	}

	@Test
	public void idValidation() throws CriticalParseFailure {
		DwRoot root = new DwRoot(new Qname("A"), new Qname("foo"));
		root.setCollectionId(new Qname("foo"));
		Document d = root.createPublicDocument();

		try {
			root = new DwRoot(new Qname("<script>alert(\"xss\");</script>"), new Qname("foo"));
			root.setCollectionId(new Qname("foo"));
			d = root.createPublicDocument();
			d.validateIncomingIds();
			fail();
		} catch (CriticalParseFailure f) {
			assertEquals("Invalid characters http://tun.fi/<script>alert(\"xss\");</script>", f.getMessage());
		}

		try {
			root = new DwRoot(new Qname("A#1"), new Qname("foo"));
			root.setCollectionId(new Qname("foo"));
			d = root.createPublicDocument();
			d.validateIncomingIds();
			fail();
		} catch (CriticalParseFailure f) {
			assertEquals("Document IDs must not contain # : A#1", f.getMessage());
		}
	}

	@Test
	public void documentIdPrefix() throws CriticalParseFailure {
		Document d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("JX.1"), new Qname("HR.1"));
		assertEquals("JX", d.getPrefix());

		d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("luomus:JA.1"), new Qname("HR.1"));
		assertEquals("luomus:JA", d.getPrefix());

		d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("luomus:123"), new Qname("HR.1"));
		assertEquals("luomus:123", d.getPrefix());

		d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("luomus:123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"), new Qname("HR.1"));
		assertEquals("luomus:1234567890123456789012345678901234567890", d.getPrefix());
	}

	@Test
	public void namedPlaceTagParsing() {
		NamedPlaceEntity e = new NamedPlaceEntity();

		assertEquals(null, e.getTags());
		assertEquals(null, e.getTagsString());

		e.addTag(new Qname("tag1"));
		assertEquals("[tag1]", e.getTags().toString());
		assertEquals("|tag1|", e.getTagsString());

		e.addTag(new Qname("tag2"));
		assertEquals("[tag1, tag2]", e.getTags().toString());
		assertEquals("|tag1|tag2|", e.getTagsString());

		e.setTagsString("|foo|bar|zzz");
		assertEquals("[bar, foo, zzz]", e.getTags().toString());
		assertEquals("|bar|foo|zzz|", e.getTagsString());
	}


	@Test
	public void wgs84_secure_logic() {
		// 100km <-> 1 deg
		assertEquals(67.0, Math.floor(67.32134), 0);
		assertEquals(68.0, Math.ceil(67.32134), 0);

		// 50km <-> 0.5 deg
		assertEquals(67.0, Math.floor(67.32134 * 2) / 2, 0);
		assertEquals(67.5, Math.ceil(67.32134 * 2) / 2, 0);

		// 25km <-> 0.25 deg
		assertEquals(67.25, Math.floor(67.32134 * 4) / 4, 0);
		assertEquals(67.5, Math.ceil(67.32134 * 4) / 4, 0);

		// 10km <-> 0.1 deg
		assertEquals(67.3, Math.floor(67.32134 * 10) / 10, 0);
		assertEquals(67.4, Math.ceil(67.32134 * 10) / 10, 0);

		// 5km <-> 0.05 deg
		assertEquals(67.3, Math.floor(67.32134 * 20) / 20, 0);
		assertEquals(67.35, Math.ceil(67.32134 * 20) / 20, 0);

		// 1km <-> 0.01 deg
		assertEquals(67.32, Math.floor(67.32134 * 100) / 100, 0);
		assertEquals(67.33, Math.ceil(67.32134 * 100) / 100, 0);
	}

	@Test
	public void concealWgs84_minmax() throws DataValidationException {
		Coordinates coordinates = new Coordinates(60.725, 27.221, Type.WGS84);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(60.0, km100.getLatMin(), 0);
		assertEquals(61.0, km100.getLatMax(), 0);
		assertEquals(27.0, km100.getLonMin(), 0);
		assertEquals(28.0, km100.getLonMax(), 0);
		assertEquals(100000, km100.getAccuracyInMeters().intValue());

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(60.5, km50.getLatMin(), 0);
		assertEquals(61.0, km50.getLatMax(), 0);
		assertEquals(27.0, km50.getLonMin(), 0);
		assertEquals(27.5, km50.getLonMax(), 0);
		assertEquals(50000, km50.getAccuracyInMeters().intValue());

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(60.5, km25.getLatMin(), 0);
		assertEquals(60.75, km25.getLatMax(), 0);
		assertEquals(27.0, km25.getLonMin(), 0);
		assertEquals(27.25, km25.getLonMax(), 0);
		assertEquals(25000, km25.getAccuracyInMeters().intValue());

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(60.7, km10.getLatMin(), 0);
		assertEquals(60.8, km10.getLatMax(), 0);
		assertEquals(27.2, km10.getLonMin(), 0);
		assertEquals(27.3, km10.getLonMax(), 0);
		assertEquals(10000, km10.getAccuracyInMeters().intValue());

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(60.7, km5.getLatMin(), 0);
		assertEquals(60.75, km5.getLatMax(), 0);
		assertEquals(27.2, km5.getLonMin(), 0);
		assertEquals(27.25, km5.getLonMax(), 0);
		assertEquals(5000, km5.getAccuracyInMeters().intValue());

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(60.72, km1.getLatMin(), 0);
		assertEquals(60.73, km1.getLatMax(), 0);
		assertEquals(27.22, km1.getLonMin(), 0);
		assertEquals(27.23, km1.getLonMax(), 0);
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealWgs84_minmax_2() throws DataValidationException {
		Coordinates coordinates = new Coordinates(60.545, 28.7, Type.WGS84);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(60.0, km100.getLatMin(), 0);
		assertEquals(61.0, km100.getLatMax(), 0);
		assertEquals(28.0, km100.getLonMin(), 0);
		assertEquals(29.0, km100.getLonMax(), 0);
		assertEquals(100000, km100.getAccuracyInMeters().intValue());

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(60.5, km50.getLatMin(), 0);
		assertEquals(61.0, km50.getLatMax(), 0);
		assertEquals(28.5, km50.getLonMin(), 0);
		assertEquals(29.0, km50.getLonMax(), 0);
		assertEquals(50000, km50.getAccuracyInMeters().intValue());

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(60.5, km25.getLatMin(), 0);
		assertEquals(60.75, km25.getLatMax(), 0);
		assertEquals(28.5, km25.getLonMin(), 0);
		assertEquals(28.75, km25.getLonMax(), 0);
		assertEquals(25000, km25.getAccuracyInMeters().intValue());

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(60.5, km10.getLatMin(), 0);
		assertEquals(60.6, km10.getLatMax(), 0);
		assertEquals(28.7, km10.getLonMin(), 0);
		assertEquals(28.8, km10.getLonMax(), 0);
		assertEquals(10000, km10.getAccuracyInMeters().intValue());

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(60.5, km5.getLatMin(), 0);
		assertEquals(60.55, km5.getLatMax(), 0);
		assertEquals(28.7, km5.getLonMin(), 0);
		assertEquals(28.75, km5.getLonMax(), 0);
		assertEquals(5000, km5.getAccuracyInMeters().intValue());

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(60.54, km1.getLatMin(), 0);
		assertEquals(60.55, km1.getLatMax(), 0);
		assertEquals(28.7, km1.getLonMin(), 0);
		assertEquals(28.71, km1.getLonMax(), 0);
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealWgs84_negative_coords() throws DataValidationException {
		Coordinates coordinates = new Coordinates(-60.52222, -60.51111, 0, 0, Type.WGS84);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(-61.0, km100.getLatMin(), 0);
		assertEquals(-60.0, km100.getLatMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(-60.6, km10.getLatMin(), 0);
		assertEquals(-60.5, km10.getLatMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(-60.53, km1.getLatMin(), 0);
		assertEquals(-60.51, km1.getLatMax(), 0);

		coordinates = new Coordinates(-0.00011, 0.00011, 0, 0, Type.WGS84);
		km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(-1, km100.getLatMin(), 0);
		assertEquals(1, km100.getLatMax(), 0);

		km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(-0.1, km10.getLatMin(), 0);
		assertEquals(0.1, km10.getLatMax(), 0);

		km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(-0.01, km1.getLatMin(), 0);
		assertEquals(0.01, km1.getLatMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealWgs84_lat_range() throws DataValidationException {
		Coordinates coordinates = new Coordinates(60.311111, 60.511111, 0, 0, Type.WGS84);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(60.0, km100.getLatMin(), 0);
		assertEquals(61.0, km100.getLatMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(60.0, km50.getLatMin(), 0);
		assertEquals(61.0, km50.getLatMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(60.25, km25.getLatMin(), 0);
		assertEquals(60.75, km25.getLatMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(60.3, km10.getLatMin(), 0);
		assertEquals(60.6, km10.getLatMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(60.3, km10.getLatMin(), 0);
		assertEquals(60.6, km10.getLatMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(60.3, km10.getLatMin(), 0);
		assertEquals(60.6, km10.getLatMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(10000, km5.getAccuracyInMeters().intValue());
		assertEquals(10000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ() throws DataValidationException {
		Coordinates coordinates = new Coordinates(6666666, 3333333, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(66.0, km100.getLatMin(), 0);
		assertEquals(67.0, km100.getLatMax(), 0);
		assertEquals(33.0, km100.getLonMin(), 0);
		assertEquals(34.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(665.0, km50.getLatMin(), 0);
		assertEquals(670.0, km50.getLatMax(), 0);
		assertEquals(330.0, km50.getLonMin(), 0);
		assertEquals(335.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(6650.0, km25.getLatMin(), 0);
		assertEquals(6675.0, km25.getLatMax(), 0);
		assertEquals(3325.0, km25.getLonMin(), 0);
		assertEquals(3350.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(666.0, km10.getLatMin(), 0);
		assertEquals(667.0, km10.getLatMax(), 0);
		assertEquals(333.0, km10.getLonMin(), 0);
		assertEquals(334.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(6665.0, km5.getLatMin(), 0);
		assertEquals(6670.0, km5.getLatMax(), 0);
		assertEquals(3330.0, km5.getLonMin(), 0);
		assertEquals(3335.0, km5.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(6666.0, km1.getLatMin(), 0);
		assertEquals(6667.0, km1.getLatMax(), 0);
		assertEquals(3333.0, km1.getLonMin(), 0);
		assertEquals(3334.0, km1.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(5000, km5.getAccuracyInMeters().intValue());
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_2() throws DataValidationException {
		Coordinates coordinates = new Coordinates(734, 359, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(73.0, km100.getLatMin(), 0);
		assertEquals(74.0, km100.getLatMax(), 0);
		assertEquals(35.0, km100.getLonMin(), 0);
		assertEquals(36.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(730.0, km50.getLatMin(), 0);
		assertEquals(735.0, km50.getLatMax(), 0);
		assertEquals(355.0, km50.getLonMin(), 0);
		assertEquals(360.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(7325.0, km25.getLatMin(), 0);
		assertEquals(7350.0, km25.getLatMax(), 0);
		assertEquals(3575.0, km25.getLonMin(), 0);
		assertEquals(3600.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(734.0, km10.getLatMin(), 0);
		assertEquals(735.0, km10.getLatMax(), 0);
		assertEquals(359.0, km10.getLonMin(), 0);
		assertEquals(360.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(734.0, km10.getLatMin(), 0);
		assertEquals(735.0, km10.getLatMax(), 0);
		assertEquals(359.0, km10.getLonMin(), 0);
		assertEquals(360.0, km10.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(734.0, km1.getLatMin(), 0);
		assertEquals(735.0, km1.getLatMax(), 0);
		assertEquals(359.0, km1.getLonMin(), 0);
		assertEquals(360.0, km1.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(10000, km5.getAccuracyInMeters().intValue());
		assertEquals(10000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_3() throws DataValidationException {
		Coordinates coordinates = new Coordinates(739, 359, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(73.0, km100.getLatMin(), 0);
		assertEquals(74.0, km100.getLatMax(), 0);
		assertEquals(35.0, km100.getLonMin(), 0);
		assertEquals(36.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(735.0, km50.getLatMin(), 0);
		assertEquals(740.0, km50.getLatMax(), 0);
		assertEquals(355.0, km50.getLonMin(), 0);
		assertEquals(360.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(7375.0, km25.getLatMin(), 0);
		assertEquals(7400.0, km25.getLatMax(), 0);
		assertEquals(3575.0, km25.getLonMin(), 0);
		assertEquals(3600.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(739.0, km10.getLatMin(), 0);
		assertEquals(740.0, km10.getLatMax(), 0);
		assertEquals(359.0, km10.getLonMin(), 0);
		assertEquals(360.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(739.0, km5.getLatMin(), 0);
		assertEquals(740.0, km5.getLatMax(), 0);
		assertEquals(359.0, km5.getLonMin(), 0);
		assertEquals(360.0, km5.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(739.0, km1.getLatMin(), 0);
		assertEquals(740.0, km1.getLatMax(), 0);
		assertEquals(359.0, km1.getLonMin(), 0);
		assertEquals(360.0, km1.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(10000, km5.getAccuracyInMeters().intValue());
		assertEquals(10000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_4() throws DataValidationException {
		Coordinates coordinates = new Coordinates(730, 350, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(73.0, km100.getLatMin(), 0);
		assertEquals(74.0, km100.getLatMax(), 0);
		assertEquals(35.0, km100.getLonMin(), 0);
		assertEquals(36.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(730.0, km50.getLatMin(), 0);
		assertEquals(735.0, km50.getLatMax(), 0);
		assertEquals(350.0, km50.getLonMin(), 0);
		assertEquals(355.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(7300.0, km25.getLatMin(), 0);
		assertEquals(7325.0, km25.getLatMax(), 0);
		assertEquals(3500.0, km25.getLonMin(), 0);
		assertEquals(3525.0, km25.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_5() throws DataValidationException {
		Coordinates coordinates = new Coordinates(6790280, 3390259, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(67.0, km100.getLatMin(), 0);
		assertEquals(68.0, km100.getLatMax(), 0);
		assertEquals(33.0, km100.getLonMin(), 0);
		assertEquals(34.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(675.0, km50.getLatMin(), 0);
		assertEquals(680.0, km50.getLatMax(), 0);
		assertEquals(335.0, km50.getLonMin(), 0);
		assertEquals(340.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(6775.0, km25.getLatMin(), 0);
		assertEquals(6800.0, km25.getLatMax(), 0);
		assertEquals(3375.0, km25.getLonMin(), 0);
		assertEquals(3400.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(679.0, km10.getLatMin(), 0);
		assertEquals(680.0, km10.getLatMax(), 0);
		assertEquals(339.0, km10.getLonMin(), 0);
		assertEquals(340.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(6790.0, km5.getLatMin(), 0);
		assertEquals(6795.0, km5.getLatMax(), 0);
		assertEquals(3390.0, km5.getLonMin(), 0);
		assertEquals(3395.0, km5.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(6790.0, km1.getLatMin(), 0);
		assertEquals(6791.0, km1.getLatMax(), 0);
		assertEquals(3390.0, km1.getLonMin(), 0);
		assertEquals(3391.0, km1.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(5000, km5.getAccuracyInMeters().intValue());
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_6() throws DataValidationException {
		Coordinates coordinates = new Coordinates(6701001, 3300001, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(67.0, km100.getLatMin(), 0);
		assertEquals(68.0, km100.getLatMax(), 0);
		assertEquals(33.0, km100.getLonMin(), 0);
		assertEquals(34.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(670.0, km50.getLatMin(), 0);
		assertEquals(675.0, km50.getLatMax(), 0);
		assertEquals(330.0, km50.getLonMin(), 0);
		assertEquals(335.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(6700.0, km25.getLatMin(), 0);
		assertEquals(6725.0, km25.getLatMax(), 0);
		assertEquals(3300.0, km25.getLonMin(), 0);
		assertEquals(3325.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(670.0, km10.getLatMin(), 0);
		assertEquals(671.0, km10.getLatMax(), 0);
		assertEquals(330.0, km10.getLonMin(), 0);
		assertEquals(331.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(6700.0, km5.getLatMin(), 0);
		assertEquals(6705.0, km5.getLatMax(), 0);
		assertEquals(3300.0, km5.getLonMin(), 0);
		assertEquals(3305.0, km5.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(6701.0, km1.getLatMin(), 0);
		assertEquals(6702.0, km1.getLatMax(), 0);
		assertEquals(3300.0, km1.getLonMin(), 0);
		assertEquals(3301.0, km1.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(5000, km5.getAccuracyInMeters().intValue());
		assertEquals(1000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_larger_area_than_km10() throws DataValidationException {
		Coordinates coordinates = new Coordinates(6641579, 6642001, 3271763, 3283699, Type.YKJ);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(66.0, km100.getLatMin(), 0);
		assertEquals(67.0, km100.getLatMax(), 0);
		assertEquals(32.0, km100.getLonMin(), 0);
		assertEquals(33.0, km100.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(660.0, km50.getLatMin(), 0);
		assertEquals(665.0, km50.getLatMax(), 0);
		assertEquals(325.0, km50.getLonMin(), 0);
		assertEquals(330.0, km50.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(6625.0, km25.getLatMin(), 0);
		assertEquals(6650.0, km25.getLatMax(), 0);
		assertEquals(3275.0, km25.getLonMin(), 0);
		assertEquals(3300.0, km25.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType()); // Bigger than KM10 so concealed with KM25 secure level
		assertEquals(6625.0, km10.getLatMin(), 0);
		assertEquals(6650.0, km10.getLatMax(), 0);
		assertEquals(3275.0, km10.getLonMin(), 0);
		assertEquals(3300.0, km10.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(6625.0, km10.getLatMin(), 0);
		assertEquals(6650.0, km10.getLatMax(), 0);
		assertEquals(3275.0, km10.getLonMin(), 0);
		assertEquals(3300.0, km10.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(6625.0, km10.getLatMin(), 0);
		assertEquals(6650.0, km10.getLatMax(), 0);
		assertEquals(3275.0, km10.getLonMin(), 0);
		assertEquals(3300.0, km10.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(25000, km10.getAccuracyInMeters().intValue());
		assertEquals(25000, km5.getAccuracyInMeters().intValue());
		assertEquals(25000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void conceal_keeps_original_accuracy_if_larger() throws DataValidationException {
		Coordinates coordinates = new Coordinates(666, 333, Type.YKJ).setAccuracyInMeters(200000);
		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(200000, km100.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_larger_area_than_conceal_level_1() throws DataValidationException {
		Coordinates coordinates = new Coordinates(666, 333, Type.YKJ);

		assertEquals(666.0, coordinates.getLatMin(), 0);
		assertEquals(667.0, coordinates.getLatMax(), 0);
		assertEquals(333.0, coordinates.getLonMin(), 0);
		assertEquals(334.0, coordinates.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(666.0, km1.getLatMin(), 0);
		assertEquals(667.0, km1.getLatMax(), 0);
		assertEquals(333.0, km1.getLonMin(), 0);
		assertEquals(334.0, km1.getLonMax(), 0);

		Coordinates km5 = coordinates.conceal(SecureLevel.KM5, coordinates.getType());
		assertEquals(666.0, km5.getLatMin(), 0);
		assertEquals(667.0, km5.getLatMax(), 0);
		assertEquals(333.0, km5.getLonMin(), 0);
		assertEquals(334.0, km5.getLonMax(), 0);

		Coordinates km10 = coordinates.conceal(SecureLevel.KM10, coordinates.getType());
		assertEquals(666.0, km10.getLatMin(), 0);
		assertEquals(667.0, km10.getLatMax(), 0);
		assertEquals(333.0, km10.getLonMin(), 0);
		assertEquals(334.0, km10.getLonMax(), 0);

		Coordinates km25 = coordinates.conceal(SecureLevel.KM25, coordinates.getType());
		assertEquals(6650.0, km25.getLatMin(), 0);
		assertEquals(6675.0, km25.getLatMax(), 0);
		assertEquals(3325.0, km25.getLonMin(), 0);
		assertEquals(3350.0, km25.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(665.0, km50.getLatMin(), 0);
		assertEquals(670.0, km50.getLatMax(), 0);
		assertEquals(330.0, km50.getLonMin(), 0);
		assertEquals(335.0, km50.getLonMax(), 0);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(66.0, km100.getLatMin(), 0);
		assertEquals(67.0, km100.getLatMax(), 0);
		assertEquals(33.0, km100.getLonMin(), 0);
		assertEquals(34.0, km100.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(50000, km50.getAccuracyInMeters().intValue());
		assertEquals(25000, km25.getAccuracyInMeters().intValue());
		assertEquals(10000, km10.getAccuracyInMeters().intValue());
		assertEquals(10000, km5.getAccuracyInMeters().intValue());
		assertEquals(10000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void concealYKJ_larger_area_than_conceal_level_2() throws DataValidationException {
		Coordinates coordinates = new Coordinates(66, 33, Type.YKJ);

		assertEquals(66.0, coordinates.getLatMin(), 0);
		assertEquals(67.0, coordinates.getLatMax(), 0);
		assertEquals(33.0, coordinates.getLonMin(), 0);
		assertEquals(34.0, coordinates.getLonMax(), 0);

		Coordinates km1 = coordinates.conceal(SecureLevel.KM1, coordinates.getType());
		assertEquals(66.0, km1.getLatMin(), 0);
		assertEquals(67.0, km1.getLatMax(), 0);
		assertEquals(33.0, km1.getLonMin(), 0);
		assertEquals(34.0, km1.getLonMax(), 0);

		Coordinates km50 = coordinates.conceal(SecureLevel.KM50, coordinates.getType());
		assertEquals(66.0, km50.getLatMin(), 0);
		assertEquals(67.0, km50.getLatMax(), 0);
		assertEquals(33.0, km50.getLonMin(), 0);
		assertEquals(34.0, km50.getLonMax(), 0);

		Coordinates km100 = coordinates.conceal(SecureLevel.KM100, coordinates.getType());
		assertEquals(66.0, km100.getLatMin(), 0);
		assertEquals(67.0, km100.getLatMax(), 0);
		assertEquals(33.0, km100.getLonMin(), 0);
		assertEquals(34.0, km100.getLonMax(), 0);

		assertEquals(100000, km100.getAccuracyInMeters().intValue());
		assertEquals(100000, km50.getAccuracyInMeters().intValue());
		assertEquals(100000, km1.getAccuracyInMeters().intValue());
	}

	@Test
	public void wgs84_accuracy_in_meters() throws DataValidationException {
		assertEquals(100000, new Coordinates(60.0, 60.5, 30.0, 32.0, Type.WGS84).calculateBoundingBoxAccuracy());
		assertEquals(25000, new Coordinates(60.0, 60.5, 32.1, 32.5, Type.WGS84).calculateBoundingBoxAccuracy());
		assertEquals(25000, new Coordinates(60.0, 60.1, 32.1, 32.5, Type.WGS84).calculateBoundingBoxAccuracy());
		assertEquals(5000, new Coordinates(60.0, 60.07, 32.08, 32.1, Type.WGS84).calculateBoundingBoxAccuracy());
		assertEquals(1000, new Coordinates(60.011, 60.012, 32.085, 32.09, Type.WGS84).calculateBoundingBoxAccuracy());
	}

	@Test
	public void conceal_municipality() throws Exception {
		Gathering gathering = new Gathering(new Qname("gid"));
		gathering.setMunicipality("Helsinki");
		gathering.createInterpretations().setFinnishMunicipality(new Qname("XX.1"));
		gathering.createInterpretations().addToFinnishMunicipalities(new Qname("XX.1"));

		assertEquals("Helsinki", gathering.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getMunicipality());
		assertEquals("XX.1", gathering.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipality().toString());
		assertEquals("[XX.1]", gathering.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipalities().toString());

		assertEquals("Helsinki", gathering.concealPublicData(SecureLevel.KM5, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getMunicipality());
		assertEquals("XX.1", gathering.concealPublicData(SecureLevel.KM5, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipality().toString());
		assertEquals("[XX.1]", gathering.concealPublicData(SecureLevel.KM5, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipalities().toString());

		assertEquals(null, gathering.concealPublicData(SecureLevel.KM10, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getMunicipality());
		assertEquals(null, gathering.concealPublicData(SecureLevel.KM10, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipality());
		assertEquals(null, gathering.concealPublicData(SecureLevel.KM10, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipalities());

		assertEquals(null, gathering.concealPublicData(SecureLevel.KM100, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getMunicipality());
		assertEquals(null, gathering.concealPublicData(SecureLevel.KM100, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipalities());
		assertEquals(null, gathering.concealPublicData(SecureLevel.HIGHEST, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getMunicipality());
		assertEquals(null, gathering.concealPublicData(SecureLevel.HIGHEST, Utils.set(SecureReason.CUSTOM), new Qname("KE.1")).getInterpretations().getFinnishMunicipalities());
	}

	@Test
	public void conceal_event_date() throws Exception {
		DateRange d1 = new DateRange(DateUtils.convertToDate("3.7.2013"));
		DateRange d2 = new DateRange(DateUtils.convertToDate("1.1.2013"), DateUtils.convertToDate("5.4.2016"));

		DateRange cd1 = d1.conceal(SecureLevel.KM1);
		assertEquals("original remains unchanged", "DateRange [begin=2013-07-03, end=2013-07-03]", d1.toString());

		assertEquals("DateRange [begin=2013-01-01, end=2013-12-31]", cd1.toString());

		assertEquals("DateRange [begin=2013-01-01, end=2016-12-31]", d2.conceal(SecureLevel.KM1).toString());
		assertEquals("DateRange [begin=2013-01-01, end=2013-12-31]", d1.conceal(SecureLevel.HIGHEST).toString());
		assertEquals("DateRange [begin=2013-01-01, end=2013-12-31]", d1.conceal(SecureLevel.HIGHEST).toString());
	}

	@Test
	public void concealing_unit_facts() {
		Unit unit = Unit.emptyUnit();
		unit.addFact("f0", "1");
		unit.addFact("f1", "1.0");
		unit.addFact("f2", "1,0");
		unit.addFact("f3", "1.0mm");
		unit.addFact("f4", "A");
		unit.addFact("f5", "AB");
		unit.addFact("f6", "Leivinkenkäänkatu 2");
		unit.addFact("f7", "http://foobar.com/female");
		unit.addFact("f8", "http://foobar.com löytyy kivoja juttuja ja tämä unit sijaitsee osoitteessa leivinkenkäänkatu 2");
		unit.addFact("f9", "http://foobar.com/löytyy/kivoja/juttuja/leivinkenkäänkatu 2");
		List<Fact> concealed = Unit.concealFacts(unit.getFacts());
		assertEquals("f0", concealed.get(0).getFact());
		assertEquals("f1", concealed.get(1).getFact());
		assertEquals("f2", concealed.get(2).getFact());
		assertEquals("f3", concealed.get(3).getFact());
		assertEquals("f4", concealed.get(4).getFact());
		assertEquals("f5", concealed.get(5).getFact());
		assertEquals("f7", concealed.get(6).getFact());
		assertEquals(7, concealed.size());
	}

	@Test
	public void concealing_media() throws Exception {
		Unit unit = new Unit(new Qname("u"));
		unit.addMedia(new MediaObject(MediaType.IMAGE, "http://img.com/1.jpg"));
		Unit concealed = unit.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1"));
		assertEquals(1, concealed.getMedia().size());

		Gathering g = new Gathering(new Qname("g"));
		g.addMedia(new MediaObject(MediaType.IMAGE, "http://img.com/1.jpg"));
		Gathering concealedG = g.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1"));
		assertEquals(0, concealedG.getMedia().size()); // Location images more likely reveal locations (for example screenshot of map)

		Document d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("JX.123"), new Qname("HR.128"));
		d.addMedia(new MediaObject(MediaType.IMAGE, "http://img.com/1.jpg"));
		Document concealedD = d.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM));
		assertEquals(0, concealedD.getMedia().size()); // Document images very likely to reveal locations (images of labels)
	}


	@Test
	public void concealing_samples_1() throws Exception {
		Unit unit = new Unit(new Qname("U"));
		Sample s = new Sample(new Qname("S"));
		s.setNotes("shouldhide");
		unit.addSample(s);
		Unit concealed = unit.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1"));
		assertEquals(1, concealed.getSamples().size());
		assertEquals(null, concealed.getSamples().get(0).getNotes());
		assertEquals("shouldhide", s.getNotes()); // did not touch the original object
	}

	@Test
	public void concealing_samples_2() throws CriticalParseFailure {
		Sample sample = new Sample(new Qname("S"));
		sample.setNotes("notes");
		sample.addFact(new Qname("MF.qualityCheckDate").toURI(), "val");
		sample.addFact("some", "other");

		Sample secured = sample.concealPublicData(SecureLevel.KM1);

		assertEquals(null, secured.getNotes());
		assertEquals("notes", sample.getNotes());

		assertEquals("[some : other]", secured.getFacts().toString());
		assertEquals("[http://tun.fi/MF.qualityCheckDate : val, some : other]", sample.getFacts().toString());
	}

	@Test
	public void concealing_identifications() throws Exception {
		Unit unit = new Unit(new Qname("U"));
		IdentificationEvent e = new IdentificationEvent();
		e.setDet("dettaaja");
		e.setDetDate("somedate");
		e.setNotes("noottia");
		e.setId(new Qname("JA.123"));
		e.setTaxon("whatisit");

		unit.addIdentification(e);

		Unit concealed = unit.concealPublicData(SecureLevel.KM1, Utils.set(SecureReason.CUSTOM), new Qname("KE.1"));
		assertEquals(1, concealed.getIdentifications().size());

		IdentificationEvent conE = concealed.getIdentifications().get(0);

		String originalData = ModelToJson.toJson(e).toString();
		String concealedData = ModelToJson.toJson(conE).toString();

		assertEquals("{\"det\":\"dettaaja\",\"detDate\":\"somedate\",\"id\":\"http://tun.fi/JA.123\",\"notes\":\"noottia\",\"taxon\":\"whatisit\"}", originalData);
		assertEquals("{\"id\":\"http://tun.fi/JA.123\",\"taxon\":\"whatisit\"}", concealedData);
	}

	@Test
	public void parsingNumbersCoordinateConceal() {
		try {
			assertEquals(30, Coordinates.getNumbers(4, 1, "123"));
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
		}

		try {
			assertEquals(30, Coordinates.getNumbers(3, 2, "123"));
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
		}

		assertEquals(1, Coordinates.getNumbers(1, 1, "1"));
		assertEquals(1, Coordinates.getNumbers(1, 1, "12"));
		assertEquals(2, Coordinates.getNumbers(2, 1, "12"));
		assertEquals(3, Coordinates.getNumbers(3, 1, "123"));
		assertEquals(23, Coordinates.getNumbers(2, 2, "123"));
		assertEquals(71, Coordinates.getNumbers(3, 2, "3271763"));
	}

}
