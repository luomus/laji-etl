package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.SingleCoordinates;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.service.console.reports.ReportGenerator;
import fi.laji.datawarehouse.etl.utils.Util;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;

public class UtilTests {

	private static final Date D_2_1_2000;
	private static final Date D_1_1_2000;
	static {
		try {
			D_2_1_2000 = DateUtils.convertToDate("2.1.2000");
			D_1_1_2000 = DateUtils.convertToDate("1.1.2000");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getMonths() throws Exception {
		assertEquals("[1]", Util.getMonths(new DateRange(D_1_1_2000)).toString());
		assertEquals("[12]", Util.getMonths(new DateRange(DateUtils.convertToDate("13.12.2000"))).toString());
		assertEquals("[1, 2]", Util.getMonths(new DateRange(D_1_1_2000, DateUtils.convertToDate("1.2.2000"))).toString());
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]", Util.getMonths(new DateRange(D_1_1_2000, DateUtils.convertToDate("1.2.2001"))).toString());
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]", Util.getMonths(new DateRange(DateUtils.convertToDate("11.6.2000"), DateUtils.convertToDate("10.5.2001"))).toString());
		assertEquals("[1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12]", Util.getMonths(new DateRange(DateUtils.convertToDate("11.6.2000"), DateUtils.convertToDate("10.4.2001"))).toString());
		assertEquals("[1, 11, 12]", Util.getMonths(new DateRange(DateUtils.convertToDate("1.11.2000"), DateUtils.convertToDate("10.1.2001"))).toString());
		assertEquals("[4, 5, 6]", Util.getMonths(new DateRange(DateUtils.convertToDate("1.4.2000"), DateUtils.convertToDate("10.6.2000"))).toString());
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]", Util.getMonths(new DateRange(null, DateUtils.convertToDate("10.4.2001"))).toString());
	}

	@Test
	public void hash() {
		assertEquals("OFj2IjCsPJFfMAxmQxLGPw==", Util.getHash("foobar"));
		assertEquals("zq68T7hcFosfBiCWZ4xw9Q==", Util.getHash("longtext алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext longtext"));
	}

	@Test
	public void randomExpire() {
		int c = 0;
		while (true) {
			long timestamp = Util.randomExpire(1, 2);
			long min = DateUtils.getCurrentEpoch() + 1*60;
			long max = DateUtils.getCurrentEpoch() + 2*60;
			assertTrue(timestamp >= min);
			assertTrue(timestamp <= max);
			if (c++ > 100) break;
		}
	}

	@Test
	public void coordinateRounding() throws DataValidationException {
		assertEquals("666.0:333.0:YKJ", new SingleCoordinates(666, 333, Type.YKJ).toString());
		assertEquals("6666666.321:333333.22:EUREF", new SingleCoordinates(6666666.321, 333333.22, Type.EUREF).toString());
		assertEquals("60.2311:32.25336:WGS84", new SingleCoordinates(60.2311, 32.25336, Type.WGS84).toString());
		assertEquals("60.231132:32.253362:WGS84", new SingleCoordinates(60.231132424234234234, 32.25336234234234234234234, Type.WGS84).toString());

		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : null", new Coordinates(666, 333, Type.YKJ).toString());
		assertEquals("6666666.0 : 6666667.0 : 333333.0 : 333334.0 : EUREF : null", new Coordinates(6666666.321, 333333.22, Type.EUREF).toString());
		assertEquals("60.2311 : 60.2311 : 32.25336 : 32.25336 : WGS84 : null", new Coordinates(60.2311, 32.25336, Type.WGS84).toString());
		assertEquals("60.231132 : 60.231132 : 32.253362 : 32.253362 : WGS84 : null", new Coordinates(60.231132424234234234, 32.25336234234234234234234, Type.WGS84).toString());

		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : null", new Coordinates(666, 667, 333, 334, Type.YKJ).toString());
		assertEquals("60.2311 : 61.223232 : 32.25336 : 32.253361 : WGS84 : null", new Coordinates(60.2311, 61.223232323, 32.25336, 32.253361, Type.WGS84).toString());
	}

	@Test
	public void displayDateTime() throws Exception {
		assertEquals("2000-01-01", Util.getDisplayDateTime(new DateRange(D_1_1_2000), null, null, null, null));
		assertEquals("2000-01-01 [8]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, null, null, null));
		assertEquals("2000-01-01 [8-11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, null, 11, null));
		assertEquals("2000-01-01 [8:15-11:35]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, 15, 11, 35));
		assertEquals("2000-01-01 [8:00-11:00]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, 0, 11, 0));
		assertEquals("2000-01-01 [8:00-11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, 0, 11, null));
		assertEquals("2000-01-01 [8-11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, null, 11, null));

		assertEquals("2000-01-01 - 2000-01-02", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), null, null, null, null));
		assertEquals("2000-01-01 [8] - 2000-01-02", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, null, null, null));
		assertEquals("2000-01-01 [8] - 2000-01-02 [11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, null, 11, null));
		assertEquals("2000-01-01 [8:15] - 2000-01-02 [11:35]", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, 15, 11, 35));
		assertEquals("2000-01-01 [8:00] - 2000-01-02 [11:00]", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, 0, 11, 0));
		assertEquals("2000-01-01 [8:00] - 2000-01-02 [11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, 0, 11, null));
		assertEquals("2000-01-01 [8] - 2000-01-02 [11]", Util.getDisplayDateTime(new DateRange(D_1_1_2000, D_2_1_2000), 8, null, 11, null));

		assertEquals("2000-01-01 [8:01-11:09]", Util.getDisplayDateTime(new DateRange(D_1_1_2000), 8, 1, 11, 9));

		assertEquals("2000-01-01", Util.getDisplayDateTime(new DateRange(D_1_1_2000,  null), null, null, null, null));
	}

	@Test
	public void reportGenerator_cleanFactOrValue() {
		assertEquals(null, ReportGenerator.cleanFactOrValue(null));
		assertEquals("", ReportGenerator.cleanFactOrValue(""));
		assertEquals("a", ReportGenerator.cleanFactOrValue("a"));
		assertEquals("a.a", ReportGenerator.cleanFactOrValue("a.a"));
		assertEquals("a", ReportGenerator.cleanFactOrValue(new Qname("a").toURI()));
		assertEquals("a", ReportGenerator.cleanFactOrValue(new Qname("luomus:a").toURI()));
		assertEquals("prop", ReportGenerator.cleanFactOrValue(new Qname("WBC.prop").toURI()));
		assertEquals("prop", ReportGenerator.cleanFactOrValue(new Qname("luomus:WBC.prop").toURI()));
		assertEquals("WBC.", ReportGenerator.cleanFactOrValue(new Qname("WBC.").toURI()));
		assertEquals("luomus:", ReportGenerator.cleanFactOrValue(new Qname("luomus:").toURI()));
	}

	@Test
	public void numericRange() {
		assertEquals(null, Util.parseNumericRange(null));
		assertEquals(null, Util.parseNumericRange(""));
		assertEquals(null, Util.parseNumericRange("a"));
		assertEquals(null, Util.parseNumericRange("a/a"));
		assertEquals(null, Util.parseNumericRange("a/5"));
		assertEquals(null, Util.parseNumericRange("1"));
		assertEquals("[1.0, 2.0]", toString(Util.parseNumericRange("1/2")));
		assertEquals("[-0.5, 2.345]", toString(Util.parseNumericRange("-0.5/2.345")));
		assertEquals("[1.0, "+Double.MAX_VALUE+"]", toString(Util.parseNumericRange("1/")));
		assertEquals("["+(-1*Double.MAX_VALUE)+", 2.0]", toString(Util.parseNumericRange("/2")));
	}

	private Object toString(double[] minMax) {
		return Arrays.toString(minMax);
	}

	@Test
	public void hashSetEquals() {
		Set<Long> s1 = new HashSet<>();
		Set<Long> s2 = new HashSet<>();
		assertTrue(s1.equals(s2));
		s1.add(1L);
		assertFalse(s1.equals(s2));
		s1.add(2L);
		assertFalse(s1.equals(s2));
		s2.add(2L);
		assertFalse(s1.equals(s2));
		s2.add(1L);
		assertTrue(s1.equals(s2));
	}

	@Test
	public void jsonNull() {
		String data = "{" +
				" \"a\": null, " +
				" \"b\": \"null\", " +
				" \"c\": \"\", " +
				"}";
		JSONObject o = new JSONObject(data);
		assertEquals("", o.getString("a"));
		assertEquals("null", o.getString("b"));
		assertEquals("", o.getString("c"));
	}

	@Test
	public void batchDocuments() {
		List<Document> docs = new ArrayList<>();
		assertEquals(0, Util.batch(docs, 10).size());

		docs.add(doc(0));
		assertEquals("[1:0]", res(Util.batch(docs, 10)));

		docs.add(doc(1, 4));
		docs.add(doc(4));
		assertEquals("[3:9]", res(Util.batch(docs, 10)));

		docs.add(doc(1));
		assertEquals("[4:10]", res(Util.batch(docs, 10)));

		docs.add(doc(1000));
		assertEquals("[4:10, 1:1000]", res(Util.batch(docs, 10)));

		docs.add(doc(1));
		docs.add(doc(1));
		assertEquals("[4:10, 1:1000, 2:2]", res(Util.batch(docs, 10)));

		docs.add(doc(9));
		assertEquals("[4:10, 1:1000, 2:2, 1:9]", res(Util.batch(docs, 10)));

		docs.add(doc(2));
		assertEquals("[4:10, 1:1000, 3:4, 1:9]", res(Util.batch(docs, 10)));

		docs.add(doc(0));
		assertEquals("[5:10, 1:1000, 3:4, 1:9]", res(Util.batch(docs, 10)));

		docs.add(doc(12, 1));
		assertEquals("[5:10, 1:1000, 3:4, 1:9, 1:13]", res(Util.batch(docs, 10)));

		docs.add(doc(2,4));
		assertEquals("[5:10, 1:1000, 4:10, 1:9, 1:13]", res(Util.batch(docs, 10)));
	}

	private String res(List<List<Document>> batches) {
		return new BatchResults(batches).batchSizes();
	}

	private static class BatchResults {
		private final List<List<Document>> batches;
		public BatchResults(List<List<Document>> batches) {
			this.batches = batches;
		}
		public String batchSizes() {
			List<String> sizes = new ArrayList<>();
			for (List<Document> batch : batches) {
				sizes.add(batch.size()+":"+Util.countOfUnits(batch));
			}
			return sizes.toString();
		}

	}

	private Document doc(int ... unitsPerGathering) {
		try {
			Document d = new Document(Concealment.PRIVATE, new Qname("sourceid"), new Qname("docid"), new Qname("collectionid"));
			int gi = 0;
			for (int units : unitsPerGathering) {
				Gathering g = new Gathering(new Qname("g"+(gi++)));
				for (int i=0; i<units; i++) {
					g.addUnit(new Unit(new Qname(g.getGatheringId().toString()+"#"+i)));
				}
				d.addGathering(g);
			}
			return d;
		} catch (CriticalParseFailure e) {
			throw new IllegalStateException(e);
		}

	}
}
