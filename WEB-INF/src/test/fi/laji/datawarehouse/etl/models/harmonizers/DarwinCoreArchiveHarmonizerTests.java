package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.luomus.commons.containers.rdf.Qname;

public class DarwinCoreArchiveHarmonizerTests {

	@Test
	public void dwca() throws CriticalParseFailure, UnknownHarmonizingFailure {
		DarwinCoreArchiveHarmonizer harmonizer = new DarwinCoreArchiveHarmonizer(CSVFormat.RFC4180);

		String data = ""+
				"collectionId,occurrenceID,catalogNumber,datasetName,locality,verbatimEventDate,otherCatalogNumbers,occurrenceRemarks,individualCount,scientificName,foobar\r\n" +
				"\n" + // empty line
				"a,nounit,,Phenology_Russia_Prioksko-Terrasnyj_Arachnids_v3,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n" +
				"a,o1,,Phenology_Russia_Prioksko-Terrasnyj_Arachnids_v3,,,,unotes,,target1,x\r\n\r" +
				"a,o2,cn,Phenology_Russia_Prioksko-Terrasnyj_Arachnids_v3,Loc,20/08/1991,\"ocn1,ocn2\",unotes2,4,target2\n";

		List<DwRoot> roots = harmonizer.harmonize(data, new Qname("KE.1"));
		assertEquals(2, roots.size());

		DwRoot root1 = roots.get(0);
		DwRoot root2 = roots.get(1);

		assertEquals("KE.1/o1", root1.getDocumentId().toString());
		assertEquals("KE.1/o2", root2.getDocumentId().toString());
		assertEquals("a", root1.getCollectionId().toString());
		assertEquals("a", root2.getCollectionId().toString());

		assertNotNull(root1.getPublicDocument());
		assertNotNull(root2.getPublicDocument());

		assertNull(root1.getPrivateDocument());
		assertNull(root2.getPrivateDocument());

		Document d1 = root1.getPublicDocument();
		Document d2 = root2.getPublicDocument();

		assertEquals(1, d1.getGatherings().size());
		assertEquals(1, d2.getGatherings().size());

		Gathering g1 = d1.getGatherings().get(0);
		Gathering g2 = d2.getGatherings().get(0);

		assertEquals(1, g1.getUnits().size());
		assertEquals(1, g2.getUnits().size());

		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g2.getUnits().get(0);

		//"locality,verbatimEventDate,otherCatalogNumbers,occurrenceRemarks,individualCount,scientificName,foobar\r\n" +
		//"o1,,Phenology_Russia_Prioksko-Terrasnyj_Arachnids_v3,,,,unotes,,target1,x\r\n\r" +
		//"o2,cn,Phenology_Russia_Prioksko-Terrasnyj_Arachnids_v3,Loc,20/08/1991,\"ocn1,ocn2\",unotes2,4,target2\n";

		assertEquals("KE.1/o1_Gathering", g1.getGatheringId().toString());
		assertEquals("KE.1/o1_Unit", u1.getUnitId().toString());

		assertEquals("[]", d1.getKeywords().toString());
		assertEquals("[cn, ocn1, ocn2]", d2.getKeywords().toString());

		assertEquals(null, g1.getLocality());
		assertEquals("Loc", g2.getLocality());

		assertEquals(null, g1.getEventDate());
		assertEquals("DateRange [begin=1991-08-20, end=1991-08-20]", g2.getEventDate().toString());

		assertEquals("unotes", u1.getNotes());
		assertEquals("unotes2", u2.getNotes());

		assertEquals("4", u2.getAbundanceString());

		assertEquals("target1", u1.getTaxonVerbatim());
		assertEquals("target2", u2.getTaxonVerbatim());
	}

	@Test
	public void expanded() throws CriticalParseFailure, UnknownHarmonizingFailure {
		DarwinCoreArchiveHarmonizer harmonizer = new DarwinCoreArchiveHarmonizer(CSVFormat.RFC4180);

		String data = "" +
				"collectionId,id,occurrenceID,catalogNumber,datasetName,recordNumber,otherCatalogNumbers,basisOfRecord,eventDate,locality,countryCode,decimalLatitude,decimalLongitude,geodeticDatum,coordinateUncertaintyInMeters,year,sex,lifeStage,recordedBy,occurrenceRemarks,individualCount,taxonId,scientificName,scientificNameAuthorship,taxonRank,genus,family,order,class,kingdom,dynamicProperties\n" +
				"gbif-dataset:asd,bf48d89c-3fbf-4c84-a2e3-c0e21c6f159f,bf48d89c-3fbf-4c84-a2e3-c0e21c6f159f,ECNUAKA3666,Chronicle of Nature - Phenology of Plants of Kaniv Nature Reserve,,ECNUAKA   ,HumanObservation,2015-11-11,Kanivsky,UA,49.744444,31.455833,4326,10000,2015,,,,,1,2875818,Carpinus betulus,,Species,Carpinus,Betulaceae,Fagales,Magnoliopsida,Plantae,\"{\"\"phenologicalEventType\"\":\"\"leaf fall end\"\",\"\"phenologicalEventTypeOriginal\"\":\"\"leaf fall end\"\"}\"\n";

		List<DwRoot> roots = harmonizer.harmonize(data, new Qname("KE.1"));
		assertEquals(1, roots.size());

		DwRoot root = roots.get(0);
		assertEquals("KE.1/bf48d89c-3fbf-4c84-a2e3-c0e21c6f159f", root.getDocumentId().toString());
		assertEquals("gbif-dataset:asd", root.getCollectionId().toString());
		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);

		assertEquals("KE.1/bf48d89c-3fbf-4c84-a2e3-c0e21c6f159f_Gathering", g.getGatheringId().toString());
		assertEquals("KE.1/bf48d89c-3fbf-4c84-a2e3-c0e21c6f159f_Unit", u.getUnitId().toString());

		assertEquals("[ECNUAKA3666, ECNUAKA]", d.getKeywords().toString());

		assertEquals("Kanivsky", g.getLocality());
		assertEquals("UA", g.getCountry());
		assertEquals("49.744444 : 49.744444 : 31.455833 : 31.455833 : WGS84 : 10000", g.getCoordinates().toString());

		assertEquals("DateRange [begin=2015-11-11, end=2015-11-11]", g.getEventDate().toString());
		assertEquals("Carpinus betulus", u.getTaxonVerbatim());
		assertEquals("[phenologicalEventType : leaf fall end, phenologicalEventTypeOriginal : leaf fall end]", u.getFacts().toString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
	}

	@Test
	public void tsv() throws CriticalParseFailure, UnknownHarmonizingFailure {
		DarwinCoreArchiveHarmonizer harmonizer = new DarwinCoreArchiveHarmonizer(CSVFormat.TDF);

		String data = "" +
				"collectionId	occurrenceID	catalogNumber	otherCatalogNumbers	basisOfRecord	eventDate	locality	stateProvince	country	decimalLatitude	decimalLongitude	recordedBy	individualCount	scientificName	taxonRank	dynamicProperties	\n"+
				"http://tun.fi/HR.3691	http://tun.fi/HR.3691/OBS123	OBS123	URN:catalog:CLO:EBIRD:OBS123	HumanObservation	2017-10-03	FI-SK Merikarvia -- Kuutie 123 (home yard)	Western Finland	FI	61.1234	21.1234	obsr123	6	Chloris chloris	SPECIES	{'lastCrawled': '2020-06-30T08:20:54.124Z', 'lastParsed': '2020-09-08T21:08:34.008Z', 'hasGeospatialIssues': False, 'lastInterpreted': '2020-09-08T21:08:34.008Z', 'verbatimScientificName': 'Chloris chloris', 'stateProvince': 'Satakunta', 'acceptedScientificName': 'Chloris chloris (Linnaeus, 1758)', 'repatriated': False}	\n";

		List<DwRoot> roots = harmonizer.harmonize(data, new Qname("KE.1"));
		assertEquals(1, roots.size());

		DwRoot root = roots.get(0);
		assertEquals("HR.3691/OBS123", root.getDocumentId().toString());
		assertEquals("HR.3691", root.getCollectionId().toString());
		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);

		assertEquals("HR.3691/OBS123_Gathering", g.getGatheringId().toString());
		assertEquals("HR.3691/OBS123_Unit", u.getUnitId().toString());

		assertEquals("[OBS123, URN:catalog:CLO:EBIRD:OBS123]", d.getKeywords().toString());

		assertEquals("FI-SK Merikarvia -- Kuutie 123 (home yard)", g.getLocality());
		assertEquals(null, g.getMunicipality());
		assertEquals("Western Finland", g.getProvince());
		assertEquals("FI", g.getCountry());
		assertEquals("61.1234 : 61.1234 : 21.1234 : 21.1234 : WGS84 : null", g.getCoordinates().toString());
		assertEquals("DateRange [begin=2017-10-03, end=2017-10-03]", g.getEventDate().toString());
		assertEquals("[obsr123]", g.getTeam().toString());

		assertEquals("6", u.getAbundanceString());
		assertEquals("Chloris chloris", u.getTaxonVerbatim());
		assertEquals(
				"[lastCrawled : 2020-06-30T08:20:54.124Z, lastParsed : 2020-09-08T21:08:34.008Z, hasGeospatialIssues : false, lastInterpreted : 2020-09-08T21:08:34.008Z, verbatimScientificName : Chloris chloris, stateProvince : Satakunta, acceptedScientificName : Chloris chloris (Linnaeus, 1758), repatriated : false]",
				u.getFacts().toString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
	}

}
