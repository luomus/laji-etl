package fi.laji.datawarehouse.etl.models;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.StatelessSession;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.Streams.ResultStream;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.containers.AnnotationNotification;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.containers.OriginalIds;
import fi.laji.datawarehouse.etl.models.containers.OutPipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeData;
import fi.laji.datawarehouse.etl.models.containers.PipeStats;
import fi.laji.datawarehouse.etl.models.containers.QueueData;
import fi.laji.datawarehouse.etl.models.containers.SplittedDocumentIds;
import fi.laji.datawarehouse.etl.models.dw.Annotation;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.luomus.commons.containers.rdf.Qname;

public class ETLDAOStub implements ETLDAO {

	@Override
	public void clearReprocessDocumentIds() {
		// Auto-generated method stub

	}

	@Override
	public void storeReprocessDocumentIds(Iterable<String> documentIds) {
		// Auto-generated method stub
	}

	@Override
	public void storeToInPipe(Qname source, String data, String contentType) {
		// Auto-generated method stub

	}

	@Override
	public boolean storeToOutPipe(List<OutPipeData> data) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public Set<Qname> getUnprosessedSources() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<InPipeData> getUnprocessedNotErroneousInOrderFromInPipe(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<OutPipeData> getUnprocessedNotErroneousInOrderFromOutPipe(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void reportAttempted(List<? extends PipeData> data, String errorMessage) {
		// Auto-generated method stub

	}

	@Override
	public void reportPermantentyFailed(List<InPipeData> data, String errorMessage) {
		// Auto-generated method stub

	}

	@Override
	public void reportProcessed(List<? extends PipeData> data) {
		// Auto-generated method stub

	}

	@Override
	public InPipeData getFromInPipe(long id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public OutPipeData getFromOutPipe(long id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public OutPipeData getFromOutPipe(Qname documentId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<InPipeData> getFromInPipeForSource(Qname source, PipeSearchParams searchParams) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<OutPipeData> getFromOutPipeForSource(Qname source, PipeSearchParams searchParams) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void markReprocessInPipe(long id) {
		// Auto-generated method stub

	}

	@Override
	public void markReprocessOutPipe(long id) {
		// Auto-generated method stub

	}

	@Override
	public void removeInPipe(long id) {
		// Auto-generated method stub

	}

	@Override
	public void removeOutPipe(long id) {
		// Auto-generated method stub

	}

	@Override
	public List<InPipeData> getFailedAttemptDataFromInPipe(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<OutPipeData> getFailedAttemptDataFromOutPipe(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public int markReprocessOutPipe(Qname documentId) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public void callKastikkaSync() {
		// Auto-generated method stub

	}

	@Override
	public void storeSplittedDocumentToQueue(Qname source, Qname originalDocumentId, String data) {
		// Auto-generated method stub

	}

	@Override
	public SplittedDocumentIds getSplittedDocumentIds(Set<Qname> originalDocumentIds) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void removeSplittedDocumentsFromQueue(Set<Qname> originalDocumentIds) {
		// Auto-generated method stub

	}

	@Override
	public OriginalIds getOriginalIdsOfSplittedDocument(Qname splittedDocumentId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public SplittedDocumentIds getOrGenerateSplittedDocumentIds(Qname documentId, List<Qname> unitIds) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Qname secureIndividualId(Qname individualId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void removeSplittedQueue(List<Long> ids) {
		// Auto-generated method stub

	}

	@Override
	public List<QueueData> getExpiredSplittedDocumentsFromQueue(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void markOutPipeErroneousForReattempt() {
		// Auto-generated method stub

	}

	@Override
	public Date getLatestTimestampFromOutPipe() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void storeFirstLoadedTimes(Map<Qname, Long> createdTimes) {
		// Auto-generated method stub

	}

	@Override
	public Map<Qname, Long> getFirstLoadTimes(List<Document> documents) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public StatelessSession getETLEntityConnection() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Connection getETLConnection() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Integer getLastReadPullApiEntrySequenceFor(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void setLastReadPullApiEntrySequence(Qname source, int sequence) {
		// Auto-generated method stub

	}

	@Override
	public Map<Qname, List<Annotation>> getAnnotations(Set<Qname> documentIds) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getAnnotatedDocumenIds() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void storeAnnotation(Annotation annotation, long inPipeId) {
		// Auto-generated method stub

	}

	@Override
	public Long getExistingAnnotationInPipeId(Qname annotationId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void storeNotification(AnnotationNotification notification) {
		// Auto-generated method stub

	}

	@Override
	public void markNotificationSent(long id) {
		// Auto-generated method stub

	}

	@Override
	public List<AnnotationNotification> getUnsentNotifications() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void removeUnsentNotifications(Qname annotationId) {
		// Auto-generated method stub

	}

	@Override
	public void storeToInPipe(Qname source, List<String> data, String contentType) {
		// Auto-generated method stub

	}

	@Override
	public PipeStats getPipeStats(boolean forceReload) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public int removeUnlinkedInPipeData(Qname source) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public List<Annotation> getTopAnnotations(int limit) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<AnnotationNotification> getTopSentNotifications(int limit) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<QueueData> getTopSplittedDocumentsFromQueue(int limit) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void storeToInPipeOnHold(Qname source, List<String> data, String contentType) {
		// Auto-generated method stub

	}

	@Override
	public void releaseInPipeHold(Qname source) {
		// Auto-generated method stub

	}

	@Override
	public int markReprocessOutPipeByNamedPlaceIds(List<Qname> ids) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public OutPipeData getSingleMultiAttemptedDataFromOutPipe(Qname source) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, SplittedDocumentIdEntity> getAllSplittedIdToOriginalId() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public ResultStream<String> getAllDocumentIds() {
		// Auto-generated method stub
		return null;
	}

}
