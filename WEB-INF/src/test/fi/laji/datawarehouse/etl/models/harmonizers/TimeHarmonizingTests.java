package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.luomus.commons.utils.DateUtils;

public class TimeHarmonizingTests {

	@Test
	public void parsing_datetime_text() {
		Gathering g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2007-03-24", g);
		assertEquals("24.03.2007", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("24.03.2007", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());
		assertEquals(null, g.getMinutesBegin());
		assertEquals(null, g.getMinutesEnd());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2007-03-24/2007-03-25", g);
		assertEquals("24.03.2007", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("25.03.2007", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());
		assertEquals(null, g.getMinutesBegin());
		assertEquals(null, g.getMinutesEnd());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1995-02-25T09:00", g);
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals(0, g.getMinutesBegin().intValue());
		assertEquals(null, g.getMinutesEnd());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2014-04-30T17:00/2014-05-01T03:00", g);
		assertEquals("30.04.2014", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("01.05.2014", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(17, g.getHourBegin().intValue());
		assertEquals(3, g.getHourEnd().intValue());
		assertEquals(0, g.getMinutesBegin().intValue());
		assertEquals(0, g.getMinutesEnd().intValue());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1995-02-25T09:10/14:40", g);
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(10, g.getMinutesBegin().intValue());
		assertEquals(14, g.getHourEnd().intValue());
		assertEquals(40, g.getMinutesEnd().intValue());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1995-02-25T09:59/14:00", g);
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(59, g.getMinutesBegin().intValue());
		assertEquals(14, g.getHourEnd().intValue());
		assertEquals(0, g.getMinutesEnd().intValue());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1995-02-25T09:01/14:01", g);
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(14, g.getHourEnd().intValue());
		assertEquals(1, g.getMinutesBegin().intValue());
		assertEquals(1, g.getMinutesEnd().intValue());

		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1995-02-25T09:01", g);
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("25.02.1995", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals(1, g.getMinutesBegin().intValue());
		assertEquals(null, g.getMinutesEnd());

		// 25.7.2012T15.00
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("25.7.2012T15.00", g);
		assertEquals("25.07.2012", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("25.07.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(15, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals(0, g.getMinutesBegin().intValue());
		assertEquals(null, g.getMinutesEnd());

		// /25.07.2012
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("/25.07.2012", g);
		assertEquals(null, g.getEventDate().getBegin());
		assertEquals("25.07.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		// /2012-07-25
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("/2012-07-25", g);
		assertEquals(null, g.getEventDate().getBegin());
		assertEquals("25.07.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		// /2012
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("/2012", g);
		assertEquals(null, g.getEventDate().getBegin());
		assertEquals("31.12.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		// /
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime(" / ", g);
		assertEquals(null, g.getQuality());
		assertEquals(null, g.getEventDate());

		// 2012-11-12T10:35/2012-11-17
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2012-11-12T10:35/2012-11-17", g);
		assertEquals("12.11.2012", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("17.11.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(10, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals(35, g.getMinutesBegin().intValue());
		assertEquals(null, g.getMinutesEnd());

		// 1940/1955
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1940/1955", g);
		assertEquals("01.01.1940", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("31.12.1955", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		// 1940
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1940", g);
		assertEquals("01.01.1940", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("31.12.1940", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1934-06-25/1934
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1934-06-25/1934", g);
		assertEquals("25.06.1934", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("31.12.1934", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1969-06
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1969-06", g);
		assertEquals("01.06.1969", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("30.06.1969", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1993-04/1993-10-20
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1993-04/1993-10-20", g);
		assertEquals("01.04.1993", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("20.10.1993", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1938-07/1938
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1938-07/1938", g);
		assertEquals("01.07.1938", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("31.12.1938", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1991-04/1991-09
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1991-04/1991-09", g);
		assertEquals("01.04.1991", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("30.09.1991", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(null, g.getHourBegin());
		assertEquals(null, g.getHourEnd());

		//1991-04/09-11
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1991-04/09-11", g);
		assertEquals("01.04.1991", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("11.09.1991", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		//1991-04/11
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1991-04/11", g);
		assertEquals("01.04.1991", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("11.04.1991", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		// 2011-04-04T15.25/16.00
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2011-04-04T15.25/16.00", g);
		assertEquals("04.04.2011", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("04.04.2011", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(15, g.getHourBegin().intValue());
		assertEquals(16, g.getHourEnd().intValue());

		// 2011-06-03 T10:06
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2011-06-03 T10:06", g);
		assertEquals("03.06.2011", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("03.06.2011", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(10, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals(6, g.getMinutesBegin().intValue());
		assertEquals(null, g.getMinutesEnd());

		// 2016.06.10
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2016.06.10", g);
		assertEquals("10.06.2016", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("10.06.2016", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		// 2012-05-10T16,00/2012-05-10T16,30
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2012-05-10T16,00/2012-05-10T16,30", g);
		assertEquals("10.05.2012", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("10.05.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(16, g.getHourBegin().intValue());
		assertEquals(00, g.getMinutesBegin().intValue());
		assertEquals(16, g.getHourEnd().intValue());
		assertEquals(30, g.getMinutesEnd().intValue());

		// 29.VIII.1983
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("29.VIII.1983", g);
		assertEquals("29.08.1983", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("29.08.1983", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));

		// 2011-01-07T1100/2011-01-07T1200
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2011-01-07T1100/2011-01-07T1205", g);
		assertEquals("07.01.2011", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("07.01.2011", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(11, g.getHourBegin().intValue());
		assertEquals(00, g.getMinutesBegin().intValue());
		assertEquals(12, g.getHourEnd().intValue());
		assertEquals(05, g.getMinutesEnd().intValue());

		// 2011-03-17T0930/2011-03-17T1130
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2011-03-17T0930/2011-03-17T1130", g);
		assertEquals("17.03.2011", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("17.03.2011", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(30, g.getMinutesBegin().intValue());
		assertEquals(11, g.getHourEnd().intValue());
		assertEquals(30, g.getMinutesEnd().intValue());

		// 2011-01-16T11.00-12.00
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2011-01-16T11.04-12.00", g);
		assertEquals("16.01.2011", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("16.01.2011", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(11, g.getHourBegin().intValue());
		assertEquals(04, g.getMinutesBegin().intValue());
		assertEquals(12, g.getHourEnd().intValue());
		assertEquals(00, g.getMinutesEnd().intValue());

		// 1985-27-24
		assertEquals("D", BaseHarmonizer.generateDateMask("1985-27-24"));
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("1985-27-24", g);
		assertEquals(null, g.getEventDate());
		assertEquals("Unparsable date \"1985-27-24\". Cause: Unparseable date: \"1985-27-24\" Interpreted date format: D", g.getQuality().getTimeIssue().getMessage());
		
		// 2003-06-38
		assertEquals("D", BaseHarmonizer.generateDateMask("2003-06-38"));
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2003-06-38", g);
		assertEquals(null, g.getEventDate());
		assertEquals("Unparsable date \"2003-06-38\". Cause: Unparseable date: \"2003-06-38\" Interpreted date format: D", g.getQuality().getTimeIssue().getMessage());
		
		// 2003-06-38/2003-07-02
		assertEquals("D/D", BaseHarmonizer.generateDateMask("2003-06-38/2003-07-02"));
		g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2003-06-38/2003-07-02", g);
		assertEquals(null, g.getEventDate());
		assertEquals("Unparsable date \"2003-06-38/2003-07-02\". Cause: Unparseable date: \"2003-06-38\" Interpreted date format: D/D", g.getQuality().getTimeIssue().getMessage());
	}

	@Test
	public void datemask_for_dateparsing() {
		assertEquals("D", BaseHarmonizer.generateDateMask("2007-03-24"));
		assertEquals("D/D", BaseHarmonizer.generateDateMask("2007-03-24/2007-03-25"));
		assertEquals("DT", BaseHarmonizer.generateDateMask("1995-02-25T09:01"));
		assertEquals("DT/DT", BaseHarmonizer.generateDateMask("2014-04-30T17:00/2014-05-01T03:00"));
		assertEquals("DT/T", BaseHarmonizer.generateDateMask("1995-02-25T09:01/14:01"));
		assertEquals("DT/T", BaseHarmonizer.generateDateMask("2.1995T09:01/14:01"));
		assertEquals("DT", BaseHarmonizer.generateDateMask("25.7.2012T15.00"));
		assertEquals("DT/D", BaseHarmonizer.generateDateMask("2012-11-12T10:35/2012-11-17"));
		assertEquals("DT/D", BaseHarmonizer.generateDateMask("12.11.2012T10:35/17.11.2012"));
		assertEquals("D/DT", BaseHarmonizer.generateDateMask("2012-11-12/2012-11-17T10:35"));
		assertEquals("D/DT", BaseHarmonizer.generateDateMask("12.11.2012/1711.2012T10:35"));

		assertEquals("D/DT", BaseHarmonizer.generateDateMask("12.11.2012/17.11.2012T10"));
		assertEquals("DT/D", BaseHarmonizer.generateDateMask("12.11.2012T10/17.11.2012"));
		assertEquals("DT/DT", BaseHarmonizer.generateDateMask("12.11.2012T10/17.11.2012T11"));

		assertEquals("D/D", BaseHarmonizer.generateDateMask("1940/1955"));
		assertEquals("D", BaseHarmonizer.generateDateMask("1940"));

		assertEquals("D/D", BaseHarmonizer.generateDateMask("1934-06-25/1934"));
		assertEquals("D", BaseHarmonizer.generateDateMask("1969-06"));

		assertEquals("D/D", BaseHarmonizer.generateDateMask("1993-04/1993-10-20"));
		assertEquals("D/D", BaseHarmonizer.generateDateMask("1938-07/1938"));
		assertEquals("D/D", BaseHarmonizer.generateDateMask("1991-04-25/1991-09"));

		assertEquals("D/D", BaseHarmonizer.generateDateMask("1991-04-25/09-11"));

		assertEquals("DT/T", BaseHarmonizer.generateDateMask("2011-04-04T15.25/16.00"));

		assertEquals("DT", BaseHarmonizer.generateDateMask("2011-06-03 T10:06"));

		assertEquals("/D", BaseHarmonizer.generateDateMask("/25.07.2012"));
		assertEquals("/DT", BaseHarmonizer.generateDateMask("/25.07.2012T10:35"));

		assertEquals("DT/DT", BaseHarmonizer.generateDateMask("2011-01-07T1100/2011-01-07T1200"));
	}

}
