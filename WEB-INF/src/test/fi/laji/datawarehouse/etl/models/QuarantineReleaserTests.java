package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.dao.ETLDAO;
import fi.laji.datawarehouse.dao.VerticaDAO;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.containers.ThreadStatusData;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.ETLException;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.laji.datawarehouse.query.model.VerticaDAOStub;
import fi.laji.datawarehouse.query.model.VerticaQueryDAOStub;
import fi.laji.datawarehouse.query.model.queries.ListQuery;
import fi.luomus.commons.containers.CollectionMetadata;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class QuarantineReleaserTests {

	private static final Qname SOURCE_2 = new Qname("KE.2");
	private static final Qname SOURCE_1 = new Qname("KE.1");
	private static final Qname DOC_ID_2 = new Qname("2");
	private static final Qname DOC_ID_1 = new Qname("1");
	private static final Qname COLLECTION_ID_2 = new Qname("HR.2");
	private static final Qname COLLECTION_ID_1 = new Qname("HR.1");

	/**
	 * Fake dao for this test to keep track of which documents the app asks to reprocess.
	 * Also defines quarantine periods for these fake test collections.
	 */
	private class FakeDAO extends DAOStub {

		@Override
		public Map<String, CollectionMetadata> getCollections() {
			Map<String, CollectionMetadata> map = new LinkedHashMap<>();
			CollectionMetadata c1 = new CollectionMetadata(COLLECTION_ID_1, null, null);
			CollectionMetadata c2 = new CollectionMetadata(COLLECTION_ID_2, null, null);
			c1.setDataQuarantinePeriod(4.0);  // 4 years
			c2.setDataQuarantinePeriod(1.0); // 1 year
			map.put(c1.getQname().toURI(), c1);
			map.put(c2.getQname().toURI(), c2);
			return map;
		}

		List<String> markedForReprocess = new ArrayList<>();

		@Override
		public ETLDAO getETLDAO() {
			return new ETLDAOStub() {
				@Override
				public int markReprocessOutPipe(Qname documentId) {
					markedForReprocess.add(documentId.toURI());
					if (documentId.equals(DOC_ID_1) || documentId.equals(DOC_ID_2)) {
						return 1; // marked 1 document
					}
					return 0; // did nothing
				}
			};
		}

		private final FakeVerticaDAO fakeVerticaDao = new FakeVerticaDAO();
		@Override
		public VerticaDAO getPrivateVerticaDAO() {
			return fakeVerticaDao;
		}
	}

	/**
	 * Fake vertica dao for this test to keep track of which queries to app performs when marking data for quarantine reprocess
	 */
	private class FakeVerticaDAO extends VerticaDAOStub {

		private final List<ListQuery> executedQueries = new ArrayList<>();
		private final FakeQueryDAO fakeQueryDAO = new FakeQueryDAO();
		int queryIndex = 0;

		@Override
		public FakeQueryDAO getQueryDAO() {
			return fakeQueryDAO;
		}

		private class FakeQueryDAO extends VerticaQueryDAOStub {

			@Override
			public List<JoinedRow> getRawList(ListQuery query) {
				try {
					return getListWithFail(query);
				} catch (CriticalParseFailure e) {
					throw new ETLException(e);
				}
			}

			private List<JoinedRow> getListWithFail(ListQuery query) throws CriticalParseFailure {
				executedQueries.add(query);
				if (query.getCurrentPage() != 1) return Collections.emptyList();
				if (definededReturn.isEmpty()) {
					List<JoinedRow> results = new ArrayList<>();
					if (query.getFilters().getCollectionId().iterator().next().equals(COLLECTION_ID_1)) {
						results.add(new JoinedRow(null, null, new Document(Concealment.PRIVATE, SOURCE_1, DOC_ID_1, COLLECTION_ID_1)));
					}
					if (query.getFilters().getCollectionId().iterator().next().equals(COLLECTION_ID_2)) {
						results.add(new JoinedRow(null, null, new Document(Concealment.PRIVATE, SOURCE_2, DOC_ID_2, COLLECTION_ID_2)));
					}
					return results;
				}
				try {
					return Utils.list(definededReturn.get(queryIndex++));
				} catch (IndexOutOfBoundsException notdefinedindex) {
					return Collections.emptyList();
				}
			}
		}

		List<JoinedRow> definededReturn = new ArrayList<>();
		public void setQueryToReturn(JoinedRow joinedRow) {
			definededReturn.add(joinedRow);
		}
	}

	private class OurErrorReporter implements ErrorReporter {
		List<String> reportedMessages = new ArrayList<>();
		@Override
		public void report(String message, Throwable condition) {
			reportedMessages.add(message + " : " + condition.getMessage());
		}
		@Override
		public void report(Throwable condition) {
			reportedMessages.add(condition.getMessage());
		}
		@Override
		public void report(String message) {
			reportedMessages.add(message);
		}
	}

	private FakeDAO fakeDao;
	private OurErrorReporter errorReporter;
	private ThreadHandler threadHandler;
	private ThreadStatuses threadStatuses;

	@Before
	public void init() {
		fakeDao = new FakeDAO();
		threadStatuses = new ThreadStatuses();
		threadHandler = new ThreadHandler(fakeDao, null, threadStatuses);
		errorReporter = new OurErrorReporter();
	}

	@Test
	public void successfulCase() throws Exception {
		QuarantineReleaser releaser = new QuarantineReleaser(fakeDao, errorReporter, threadHandler);
		releaser.release(DateUtils.convertToDate("1.1.2000"));

		System.out.println(errorReporter.reportedMessages.toString());
		assertTrue(errorReporter.reportedMessages.isEmpty());

		assertEquals(4, fakeDao.fakeVerticaDao.executedQueries.size());
		ListQuery q1 =  fakeDao.fakeVerticaDao.executedQueries.get(0);
		ListQuery q2 =  fakeDao.fakeVerticaDao.executedQueries.get(1);
		ListQuery q3 =  fakeDao.fakeVerticaDao.executedQueries.get(2);
		ListQuery q4 =  fakeDao.fakeVerticaDao.executedQueries.get(3);

		// First looking for collection 1, first page --- 4 years time limit from 1.1.2000
		assertEquals(1, q1.getCurrentPage());
		assertEquals("[" + COLLECTION_ID_1 + "]", q1.getFilters().getCollectionId().toString());
		assertEquals("[1700-01-01/1996-01-01]", q1.getFilters().getTime().toString());
		assertEquals("[" + SecureReason.DATA_QUARANTINE_PERIOD + "]", q1.getFilters().getSecureReason().toString());

		// Then for next page (but finds nothing)
		assertEquals(2, q2.getCurrentPage());
		assertEquals("[" + COLLECTION_ID_1 + "]", q2.getFilters().getCollectionId().toString());
		assertEquals("[1700-01-01/1996-01-01]", q2.getFilters().getTime().toString());
		assertEquals("[" + SecureReason.DATA_QUARANTINE_PERIOD + "]", q2.getFilters().getSecureReason().toString());

		// First looking for collection 2, first page --- 1 year time limit from 1.1.2000
		assertEquals(1, q3.getCurrentPage());
		assertEquals("[" + COLLECTION_ID_2 + "]", q3.getFilters().getCollectionId().toString());
		assertEquals("[1700-01-01/1999-01-01]", q3.getFilters().getTime().toString());
		assertEquals("[" + SecureReason.DATA_QUARANTINE_PERIOD + "]", q3.getFilters().getSecureReason().toString());

		// Then for next page (but finds nothing)
		assertEquals(2, q4.getCurrentPage());
		assertEquals("[" + COLLECTION_ID_2 + "]", q4.getFilters().getCollectionId().toString());
		assertEquals("[1700-01-01/1999-01-01]", q4.getFilters().getTime().toString());
		assertEquals("[" + SecureReason.DATA_QUARANTINE_PERIOD + "]", q4.getFilters().getSecureReason().toString());

		assertEquals("[http://tun.fi/1, http://tun.fi/2]", fakeDao.markedForReprocess.toString());

		List<String> startedReaders = new ArrayList<>();
		for (ThreadStatusData threadStatusData : threadStatuses.getThreadStatuses()) {
			startedReaders.add(threadStatusData.getSource() + " - " + threadStatusData.getThreadName());
		}
		assertTrue(startedReaders.contains(SOURCE_1 + " - OutPipeReader"));
		assertTrue(startedReaders.contains(SOURCE_2 + " - OutPipeReader"));
	}

	@Test
	public void queryReturnsDocumentNotFoundFromOutPipe() throws Exception {

		fakeDao.fakeVerticaDao.setQueryToReturn(new JoinedRow(null, null, new Document(Concealment.PRIVATE, SOURCE_1, new Qname("notfound"), COLLECTION_ID_1)));

		QuarantineReleaser releaser = new QuarantineReleaser(fakeDao, errorReporter, threadHandler);
		releaser.release(DateUtils.convertToDate("1.1.2000"));

		assertEquals("[Entry set for quarantine reprocess was not found from out pipe: http://tun.fi/notfound : KE.1]", errorReporter.reportedMessages.toString());

		assertEquals("[http://tun.fi/notfound]", fakeDao.markedForReprocess.toString());

		assertEquals(0, threadStatuses.getThreadStatuses().size());
	}

}
