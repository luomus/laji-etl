package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class LajiETLHarmonizerTests {

	private static final Qname SOURCE = new Qname("KE.901");
	LajiETLJSONHarmonizer harmonizer = new LajiETLJSONHarmonizer();

	@Test
	public void garbage() {
		try {
			harmonizer.harmonize(new JSONObject().setString("foo", "bar"), SOURCE);
		} catch (CriticalParseFailure e) {
			assertEquals("fi.laji.datawarehouse.etl.utils.JsonToModel failed: No document id", e.getMessage());
		}
	}

	@Test
	public void test_different_source() {
		JSONObject testData = getTestData("laji-etl-batch.json");
		try {
			harmonizer.harmonize(testData, new Qname("foobar"));
		} catch (CriticalParseFailure e) {
			assertEquals("fi.laji.datawarehouse.etl.utils.JsonToModel failed: Json source = KE.901 but harmonized for source foobar", e.getMessage());
		}
	}

	@Test
	public void test_batch() throws CriticalParseFailure {
		JSONObject testData = getTestData("laji-etl-batch.json");
		List<DwRoot> roots = harmonizer.harmonize(testData, SOURCE);
		assertEquals(2, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("HR.3211", root.getCollectionId().toString());
		assertEquals("HR.3211/123", root.getDocumentId().toString());

		assertEquals(null, root.getPrivateDocument());

		Document d1 = root.getPublicDocument();
		Document d2 = roots.get(0).getPublicDocument();

		assertEquals(root.getCollectionId(), d1.getCollectionId());
		assertEquals(root.getDocumentId(), d1.getDocumentId());

		assertEquals("PUBLIC", d1.getConcealment().toString());
		assertEquals("PUBLIC", d2.getConcealment().toString()); // missing from json

		assertEquals("[foo:123]", d1.getEditorUserIds().toString());
		assertEquals("[catalogueNumber : 123, observedOrCreatedAt : 2011-08-17T18:40:00+02:00]", d1.getFacts().toString());
		assertEquals(null, d1.getFirstLoadDate());
		assertEquals(null, d1.getFirstLoadTime());
		assertEquals("[123, has_images, research_grade, identifications_most_agree]", d1.getKeywords().toString());
		assertEquals("http://tun.fi/MZ.intellectualRightsCC-BY-NC-4.0", d1.getLicenseId());
		assertEquals(null, d1.getQuality());
		assertEquals(null, d1.getLoadDate());
		assertEquals(null, d1.getLoadTime());
		assertEquals(null, d1.getLinkings());
	}

	@Test
	public void minimal_doc() throws CriticalParseFailure {
		JSONObject testData = getTestData("laji-etl-minimal.json");
		List<DwRoot> roots = harmonizer.harmonize(testData, SOURCE);
		assertEquals(1, roots.size());
		
		JSONObject expected = getTestData("laji-etl-minimal-expected.json");
		assertEquals(expected.beautify(), roots.get(0).clearProcesstime().toJSON().beautify());
	}
	
	private JSONObject getTestData(String filename) {
		return new JSONObject(RdfXmlHarmonizerTests.getTestData(filename));
	}
}
