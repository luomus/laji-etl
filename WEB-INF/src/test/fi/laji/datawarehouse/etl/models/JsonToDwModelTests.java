package fi.laji.datawarehouse.etl.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.dao.MediaDAOStub;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.IdentificationEvent;
import fi.laji.datawarehouse.etl.models.dw.JoinedRow;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Sample;
import fi.laji.datawarehouse.etl.models.dw.TaxonCensus;
import fi.laji.datawarehouse.etl.models.dw.TypeSpecimen;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizer;
import fi.laji.datawarehouse.etl.models.harmonizers.RdfXmlHarmonizerTests;
import fi.laji.datawarehouse.etl.utils.JsonToModel;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.laji.datawarehouse.query.model.Base;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

public class JsonToDwModelTests {

	@Test
	public void root() throws CriticalParseFailure {
		String json = "" +
				"{ " +
				"    \"deleteRequest\": false, " +
				"    \"processTime\": 1521188286, " +
				"    \"sourceId\": \"http://tun.fi/KE.481\", " +
				"    \"documentId\": \"http://tun.fi/KE.481/10799\", " +
				"    \"collectionId\": \"http://tun.fi/HR.175\", " +
				"    \"schema\": \"laji-etl\" " +
				"}";
		DwRoot root = DwRoot.fromJson(new JSONObject(json));
		assertEquals(false, root.isDeleteRequest());
		assertEquals("http://tun.fi/HR.175", root.getCollectionId().toURI());
		assertEquals("http://tun.fi/KE.481/10799", root.getDocumentId().toURI());
		assertEquals("http://tun.fi/KE.481", root.getSourceId().toURI());
	}

	@Test
	public void test() throws Exception {
		JSONObject json = getTestData("rdf-xml-testdata-2.xml");

		DwRoot root = DwRoot.fromJson(json);

		assertEquals("http://tun.fi/HR.65", root.getCollectionId().toURI());
		assertEquals("http://tun.fi/HT.17068", root.getDocumentId().toURI());
		assertTrue(root.getProcessTime() <= DateUtils.getCurrentEpoch());

		Document privateDocument = root.getPrivateDocument();
		assertTrue(privateDocument == null);
		Document publicDocument = root.getPublicDocument();

		assertTrue(publicDocument.isPublic());

		assertEquals(SecureLevel.KM100, publicDocument.getSecureLevel());
		assertEquals("["+SecureReason.CUSTOM+"]", publicDocument.getSecureReasons().toString());

		assertEquals(1, publicDocument.getEditorUserIds().size());
		assertEquals("http://tun.fi/MA.5", publicDocument.getEditorUserIds().get(0));
		assertEquals("2015-10-29", toIsoDate(publicDocument.getCreatedDate()));
		assertEquals(7, publicDocument.getFacts().size());
		assertEquals("http://tun.fi/MY.labelsVerbatim", publicDocument.getFacts().get(0).getFact());
		assertEquals("sVerbatim labels", publicDocument.getFacts().get(0).getValue());
		assertEquals(6, publicDocument.getKeywords().size());
		assertEquals("sNotes", publicDocument.getNotes());
		assertEquals("http://inat.example/ABC", publicDocument.getReferenceURL());

		assertEquals(1, publicDocument.getGatherings().size());
		Gathering gathering = publicDocument.getGatherings().get(0);

		assertEquals(2, gathering.getTeam().size());
		assertEquals("s1", gathering.getTeam().get(0));
		assertEquals("s2", gathering.getTeam().get(1));

		assertEquals("sBiogeographical province", gathering.getBiogeographicalProvince());
		assertEquals("sCountry", gathering.getCountry());
		assertEquals(16, gathering.getFacts().size());
		assertEquals("sHigher geography", gathering.getHigherGeography());
		assertEquals("sLocality name", gathering.getLocality());
		assertEquals("Inkoo", gathering.getMunicipality());
		assertEquals("sNotes", gathering.getNotes());
		assertEquals("sAdministrative province", gathering.getProvince());

		assertEquals("2015-10-01", toIsoDate(gathering.getEventDate().getBegin()));
		assertEquals("2015-10-23", toIsoDate(gathering.getEventDate().getEnd()));

		assertEquals(100, gathering.getCoordinates().getAccuracyInMeters().intValue());
		assertEquals(6667.0, gathering.getCoordinates().getLatMax(), 0);
		assertEquals(6666.0, gathering.getCoordinates().getLatMin(), 0);
		assertEquals(3334.0, gathering.getCoordinates().getLonMax(), 0);
		assertEquals(3333.0, gathering.getCoordinates().getLonMin(), 0);
		assertEquals(Type.YKJ, gathering.getCoordinates().getType());

		assertEquals(2, gathering.getUnits().size());
		Unit unit = gathering.getUnits().get(0);
		Unit unit2 = gathering.getUnits().get(1);

		assertEquals("sAssociated observation taxa", unit2.getTaxonVerbatim());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, unit2.getRecordBasis());

		assertEquals(false, unit.isTypeSpecimen());
		assertEquals("3", unit.getAbundanceString());
		assertEquals("EP", unit.getAuthor());
		assertEquals("Piirainen, Esko", unit.getDet());
		assertEquals(12, unit.getFacts().size());
		assertEquals(LifeStage.FERTILE, unit.getLifeStage());
		assertEquals("sNotes", unit.getNotes());
		assertEquals(null, unit.getRecordBasis());
		assertEquals(Sex.UNKNOWN, unit.getSex());
		assertEquals("Parus major x eskolus", unit.getTaxonVerbatim());
		assertEquals(1, unit.getExternalMediaCount().intValue());

		assertEquals("http://tun.fi/somemethod", unit.getSamplingMethod());
		assertEquals("[somebasis]", unit.getIdentificationBasis().toString());
		assertEquals(json.setInteger("processTime", 1).beautify(), ModelToJson.rootToJson(root).setInteger("processTime", 1).beautify());
	}

	@Test
	public void test_boolean() throws Exception {
		JSONObject json = getTestData("rdf-xml-testadata-1-multiunit-identification-type2.xml");
		DwRoot root = DwRoot.fromJson(json);
		Document publicDocument = root.getPublicDocument();
		Gathering gathering = publicDocument.getGatherings().get(0);
		Unit unit1 = gathering.getUnits().get(0);
		Unit unit2 = gathering.getUnits().get(1);

		assertTrue(unit1.isTypeSpecimen());
		assertFalse(unit2.isTypeSpecimen());
	}

	private String toIsoDate(Date date) {
		return DateUtils.format(date, "yyyy-MM-dd");
	}

	private JSONObject getTestData(String filename) throws Exception {
		String rdfXml = RdfXmlHarmonizerTests.getTestData(filename);
		List<DwRoot> roots = new RdfXmlHarmonizer(new MediaDAOStub()).harmonize(rdfXml, new Qname("KE.3"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		root.getPublicDocument().setReferenceURL("http://inat.example/ABC");
		root.getPublicDocument().getGatherings().get(0).getUnits().get(0).setExternalMediaCount(1);

		root.getPublicDocument().getGatherings().get(0).getUnits().get(0).setSamplingMethodUsingQname(new Qname("somemethod"));
		root.getPublicDocument().getGatherings().get(0).getUnits().get(0).addIdentificationBasis(new Qname("somebasis"));
		JSONObject json = root.toJSON();
		return json;
	}

	@Test
	public void test_trim() throws Exception {
		JSONObject json = getTestData("rdf-xml-testdata-2.xml");
		json.setString("documentId", "http://id.luomus.fi/12345");
		json.setString("sourceId", "http://tun.fi/KE.3");
		json.getObject("publicDocument").setString("notes", " n o t e s ");
		json.getObject("publicDocument").getArray("editorUserIds").appendString(" editor ");
		DwRoot root = DwRoot.fromJson(json);
		assertEquals("http://id.luomus.fi/12345", root.getDocumentId().toURI());
		assertEquals("KE.3", root.getSourceId().toString());
		assertEquals("n o t e s", root.getPublicDocument().getNotes());
		assertEquals("editor", root.getPublicDocument().getEditorUserIds().get(1));
	}

	@Test
	public void json_to_joinedrow_unit_base() throws Exception {
		String json = getTestDataString("joinedrow.json");
		JoinedRow row = JsonToModel.joinedRowFromJson(new JSONObject(json), "http://tun.fi/F.1#2");
		assertEquals("locality", row.getGathering().getLocality());
		assertEquals(0, row.getGathering().getUnits().size());
		assertEquals(1, row.getUnit().getAnnotations().size());
		assertEquals(new Qname("MAN.1"), row.getUnit().getAnnotations().get(0).getId());
		assertEquals("http://tun.fi/F.1#2", row.getUnit().getUnitId().toURI());
		assertEquals(null, row.getAnnotation());
	}

	@Test
	public void json_to_joinedrow_annotation_base() throws Exception {
		String json = getTestDataString("joinedrow.json");
		JoinedRow row = JsonToModel.joinedRowFromJson(new JSONObject(json), "http://tun.fi/F.1#2", Base.ANNOTATION, "http://tun.fi/MAN.1");
		assertEquals("http://tun.fi/MAN.1", row.getAnnotation().getId().toURI());
	}

	@Test
	public void json_with_legacy_values_to_document() throws Exception {
		String json = getTestDataString("document.json");
		JsonToModel.documentFromJson(new JSONObject(json));
	}

	private static String getTestDataString(String filename) {
		URL url = JsonToDwModelTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void secureasons() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		doc.addSecureReason(SecureReason.BREEDING_SEASON_TAXON_CONSERVATION);
		doc.addSecureReason(SecureReason.USER_HIDDEN);
		String expected = "["+SecureReason.BREEDING_SEASON_TAXON_CONSERVATION+", "+SecureReason.USER_HIDDEN+"]";
		assertEquals(expected, doc.getSecureReasons().toString());

		String expectedJson = "[\""+SecureReason.BREEDING_SEASON_TAXON_CONSERVATION+"\",\""+SecureReason.USER_HIDDEN+"\"]";
		JSONObject json = ModelToJson.toJson(doc);
		assertEquals(expectedJson, json.getArray("secureReasons").toString());

		doc = JsonToModel.documentFromJson(json);
		assertEquals(expected, doc.getSecureReasons().toString());
	}

	@Test
	public void geo() throws Exception {
		String json = getTestDataString("etl-schema-gathering-GeoJSON.json");
		System.out.println(json);
		Gathering g = JsonToModel.gatheringFromJson(new JSONObject(json));
		assertEquals("GEOMETRYCOLLECTION(LINESTRING (30 60, 30 61), POINT (42.123 58.123), POLYGON ((70 55, 70 55.1, 70.1 55.1, 70.1 55, 70 55)))", g.getGeo().getWKT());
	}

	@Test
	public void qualities() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		doc.createQuality().setIssue(new Quality(Issue.REPORTED_UNRELIABLE, Quality.Source.ORIGINAL_DOCUMENT));
		doc.addGathering(new Gathering(new Qname("G")));
		Unit unit = new Unit(new Qname("U"));
		doc.getGatherings().get(0).addUnit(unit);
		unit.setReportedTaxonConfidence(TaxonConfidence.UNSURE);
		unit.createQuality().setIssue(new Quality(Quality.Issue.RECORD_BASIS_MISSING, Quality.Source.AUTOMATED_FINBIF_VALIDATION));
		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		assertEquals(Issue.REPORTED_UNRELIABLE, doc.getQuality().getIssue().getIssue());
		assertEquals(Quality.Source.ORIGINAL_DOCUMENT, doc.getQuality().getIssue().getSource());
		unit = doc.getGatherings().get(0).getUnits().get(0);
		assertEquals(TaxonConfidence.UNSURE, unit.getReportedTaxonConfidence());
		assertEquals(Quality.Issue.RECORD_BASIS_MISSING, unit.getQuality().getIssue().getIssue());
	}

	@Test
	public void dateparsing() {
		assertEquals("2012-02-13", DateUtils.format(JsonToModel.dateFromIso("2012-02-13"), "yyyy-MM-dd"));
		assertEquals("1602-02-13", DateUtils.format(JsonToModel.dateFromIso("1602-02-13"), "yyyy-MM-dd"));
		assertEquals("0666-12-01", DateUtils.format(JsonToModel.dateFromIso("666-12-01"), "yyyy-MM-dd"));

		long start = System.currentTimeMillis();
		for (int i = 0; i<1000000; i++) {
			JsonToModel.dateFromIso("2012-02-13");
		}
		long end = System.currentTimeMillis();
		System.out.println((end-start));
		// was 2094 .. 2105  now 473 .. 481
	}

	@Test
	public void loadDates() throws Exception {
		Document document = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("JA.123"), new Qname("HR.1"));
		document.setFirstLoadTime(DateUtils.convertToDate("1.1.2000").getTime() / 1000);
		document.setLoadTimeNow();

		String now = date(new Date());

		assertEquals(now, date(document.getLoadDate()));
		assertEquals("2000-01-01", date(document.getFirstLoadDate()));

		JSONObject json = ModelToJson.toJson(document);
		System.out.println(json.beautify());

		assertEquals(now, json.getString("loadDate"));
		assertEquals("2000-01-01", json.getString("firstLoadDate"));

		document = JsonToModel.documentFromJson(json);
		assertEquals(now, date(document.getLoadDate()));
		assertEquals("2000-01-01", date(document.getFirstLoadDate()));
	}

	private String date(Date date) {
		return DateUtils.format(date, "yyyy-MM-dd");
	}

	@Test
	public void taxonCensus() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		doc.addGathering(new Gathering(new Qname("G")));
		doc.getGatherings().get(0).addTaxonCensus(new TaxonCensus(new Qname("MX.1"), new Qname("MX.typeSomething")));
		doc.getGatherings().get(0).addTaxonCensus(new TaxonCensus(new Qname("MX.2"), new Qname("MX.typeSomethingElse")));
		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		Gathering g = doc.getGatherings().get(0);
		assertEquals("MX.1", g.getTaxonCensus().get(0).getTaxonId().toString());
		assertEquals("MX.typeSomething", g.getTaxonCensus().get(0).getType().toString());
		assertEquals("MX.2", g.getTaxonCensus().get(1).getTaxonId().toString());
		assertEquals("MX.typeSomethingElse", g.getTaxonCensus().get(1).getType().toString());
	}

	@Test
	public void samples() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		Unit u = new Unit(new Qname("U"));
		doc.addGathering(new Gathering(new Qname("G")));
		doc.getGatherings().get(0).addUnit(u);
		Sample sample = new Sample(new Qname("S"));
		u.addSample(sample);
		sample.setCollectionId(new Qname("HR.123"));
		sample.addFact("f", "v");
		sample.addKeyword("a");
		sample.addKeyword("b");
		sample.setMultiple(true);
		sample.setNotes("notes");
		sample.setQualityUsingQname(new Qname("q"));
		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		Sample revSample = doc.getGatherings().get(0).getUnits().get(0).getSamples().get(0);
		assertEquals(sample.getCollectionId(), revSample.getCollectionId());
		assertEquals(sample.getFacts().toString(), revSample.getFacts().toString());
		assertEquals(sample.getKeywords().toString(), revSample.getKeywords().toString());
		assertEquals(sample.isMultiple(), revSample.isMultiple());
		assertEquals(sample.getNotes(), revSample.getNotes());
		assertEquals(sample.getQuality(), revSample.getQuality());
	}

	@Test
	public void emptyBeginDate() throws Exception {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		doc.addGathering(new Gathering(new Qname("G")));
		doc.getGatherings().get(0).setEventDate(new DateRange(null, DateUtils.convertToDate("1.1.2000")));
		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		Gathering g = doc.getGatherings().get(0);
		assertEquals("DateRange [begin=null, end=2000-01-01]", g.getEventDate().toString());
	}

	@Test
	public void media() throws CriticalParseFailure, DataValidationException {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		Unit u = new Unit(new Qname("U"));
		doc.addGathering(new Gathering(new Qname("G")));
		doc.getGatherings().get(0).addUnit(u);

		MediaObject m1 = new MediaObject(MediaType.IMAGE, "http://image.com/1");
		m1.setAuthor("auth");
		m1.setThumbnailURL("http://image.com/1_thum");
		m1.setLicenseIdUsingQname(new Qname("license"));
		m1.addKeyword("m1keyword");

		MediaObject m2 = new MediaObject(MediaType.AUDIO);
		m2.setWavURL("http://audio.com/1.wav");
		m2.setMp3URL("http://audio.com/1.mp3");
		u.addMedia(m1);
		u.addMedia(m2);

		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		u = doc.getGatherings().get(0).getUnits().get(0);

		assertEquals(MediaType.IMAGE, u.getMedia().get(0).getMediaType());
		assertEquals(MediaType.AUDIO, u.getMedia().get(1).getMediaType());
		assertEquals("https://image.com/1", u.getMedia().get(0).getFullURL());
		assertEquals("https://audio.com/1.mp3", u.getMedia().get(1).getFullURL());
		assertEquals("https://audio.com/1.wav", u.getMedia().get(1).getWavURL());
		assertEquals("https://audio.com/1.mp3", u.getMedia().get(1).getMp3URL());
		assertEquals("[m1keyword]", u.getMedia().get(0).getKeywords().toString());
	}

	@Test
	public void identifications_and_types() throws CriticalParseFailure {
		Document doc = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("ZZ.1"), new Qname("HR.1"));
		Unit u = new Unit(new Qname("U"));
		doc.addGathering(new Gathering(new Qname("G")));
		doc.getGatherings().get(0).addUnit(u);

		IdentificationEvent e1 = new IdentificationEvent();
		IdentificationEvent e2 = new IdentificationEvent();
		e1.setId(new Qname("E1"));
		e2.setId(null);
		e1.setDetDate("2005");
		e1.setTaxonID(new Qname("MX.1"));
		e2.addFact("f1", "v1");
		e2.setNotes("note");

		TypeSpecimen t = new TypeSpecimen();
		t.setId(new Qname("T1"));
		t.addFact("f2", "v2");
		t.setNotes("note");
		t.setStatus(new Qname("typeoftype"));

		u.addIdentification(e1);
		u.addIdentification(e2);
		u.addType(t);

		JSONObject json = ModelToJson.toJson(doc);
		System.out.println(json.beautify());

		doc = JsonToModel.documentFromJson(json);
		u = doc.getGatherings().get(0).getUnits().get(0);

		assertEquals("" +
				"[Identification [id=E1, taxon=null, author=null, taxonId=MX.1, taxonSpecifier=null, taxonSpecifierAuthor=null], " +
				"Identification [id=null, taxon=null, author=null, taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
				u.getIdentifications().toString());
		assertEquals("[Identification [id=T1, taxon=null, author=null, taxonId=null, taxonSpecifier=null, taxonSpecifierAuthor=null]]",
				u.getTypes().toString());

		assertEquals("[f1 : v1]", u.getIdentifications().get(1).getFacts().toString());
		assertEquals("[f2 : v2]", u.getTypes().get(0).getFacts().toString());
	}

	@Test
	public void exceptionMessage() throws CriticalParseFailure {
		Document d = new Document(Concealment.PUBLIC, new Qname("KE.1"), new Qname("d"), new Qname("col"));
		Gathering g = new Gathering(new Qname("g"));
		d.addGathering(g);
		Unit u = new Unit(new Qname("u"));
		g.addUnit(u);
		IdentificationEvent e = new IdentificationEvent();
		e.setPreferred(true);
		u.addIdentification(e);

		JSONObject docJson = ModelToJson.toJson(d);
		System.out.println(docJson.beautify());

		JsonToModel.documentFromJson(docJson);

		docJson.getArray("gatherings").iterateAsObject().get(0).getArray("units").iterateAsObject().get(0).getArray("identifications").iterateAsObject().get(0).setString("preferred", "invalidbooleanval");

		try {
			JsonToModel.documentFromJson(docJson);
			fail("should throw exception");
		} catch (CriticalParseFailure ex) {
			assertEquals("gatherings.units.identifications.preferred: org.json.JSONException: JSONObject[\"preferred\"] is not a Boolean.", ex.getMessage());
		}
	}

}
