package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.UnknownHarmonizingFailure;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.XMLReader;

public class Fmnh2008HarmonizerTests {

	public static fi.luomus.commons.xml.Document getTestData(String filename) {
		String data = RdfXmlHarmonizerTests.getTestData(filename);
		fi.luomus.commons.xml.Document xmlDocument = new XMLReader().parse(data);
		return xmlDocument;
	}

	@Test
	public void hatikka_ykj_coordinatesets() throws Exception {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-hatikka-ykj-coordinatesets.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);
		System.out.println(ModelToJson.rootToJson(root));
		assertEquals("http://tun.fi/HR.447", root.getCollectionId().toURI());
		assertEquals("http://tun.fi/KE.1111/1234", root.getDocumentId().toURI());
		assertEquals(null, root.getPrivateDocument());
		assertTrue(root.getPublicDocument() != null);
		assertEquals("KE.1111", root.getSourceId().toString());

		Document doc = root.getPublicDocument();
		assertEquals("http://tun.fi/HR.447", doc.getCollectionId().toURI());
		assertEquals(Concealment.PUBLIC, doc.getConcealment());
		assertEquals(null, doc.getCreatedDate());
		assertEquals("http://tun.fi/KE.1111/1234", doc.getDocumentId().toURI());
		assertEquals(1, doc.getEditorUserIds().size());
		assertEquals(0, doc.getFacts().size());
		assertEquals(1, doc.getGatherings().size());
		assertEquals(0, doc.getKeywords().size());
		assertEquals(null, doc.getModifiedDate());
		assertEquals(null, doc.getNotes());
		assertEquals(SecureLevel.NONE, doc.getSecureLevel());
		assertEquals("[]", doc.getSecureReasons().toString());
		assertEquals("KE.1111", doc.getSourceId().toString());

		Gathering g = doc.getGatherings().get(0);
		assertEquals(2, g.getTeam().size());
		assertEquals("[Lahti, Tapani, Saarela, Anne]", g.getTeam().toString());
		assertEquals(null, g.getCoordinates());
		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("" +
				"[POLYGON ((3397130 6676342, 3397518 6676614, 3398022 6676510, 3398226 6676146, 3398030 6675818, 3397578 6675746, 3397242 6675950, 3397130 6676342))]",
				g.getGeo().getWKTList().toString());
		assertEquals(Type.YKJ, g.getGeo().getCRS());
		assertEquals("/6676342:3397130/6676614:3397518/6676510:3398022/6676146:3398226/6675818:3398030/6675746:3397578/6675950:3397242/ YKJ", g.getCoordinatesVerbatim());
		assertEquals("04.05.2013", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("04.05.2013", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(1, g.getFacts().size());
		assertEquals("[Weather : 0/8, kohtalainen SW, +14C]", g.getFacts().toString());
		assertEquals("Uusimaa", g.getBiogeographicalProvince());
		assertEquals("Suomi", g.getCountry());
		assertEquals(null, g.getHigherGeography());
		assertEquals("Vuosaari, Kallvik", g.getLocality());
		assertEquals("Helsinki", g.getMunicipality());
		assertEquals("Helsinki Kallvik 4.5.2013", g.getNotes());
		assertEquals(null, g.getProvince());
		assertEquals(14, g.getHourBegin().intValue());
		assertEquals(45, g.getMinutesBegin().intValue());
		assertEquals(15, g.getHourEnd().intValue());
		assertEquals(45, g.getMinutesEnd().intValue());
		assertEquals(18, g.getUnits().size());

		Unit u = g.getUnits().get(0);
		assertEquals("[]", u.getFacts().toString());
		assertEquals("1", u.getAbundanceString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
		assertEquals("Inachis io", u.getTaxonVerbatim());
		assertEquals("http://tun.fi/KE.1111/1234#2", u.getUnitId().toURI());
		assertEquals(null, u.getNotes());
	}

	@Test
	public void hatikka_wgs84_coordinatesets() throws Exception {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-hatikka-wgs84-coordinatesets.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);
		assertEquals("http://tun.fi/HR.447", root.getCollectionId().toURI());
		assertEquals("http://tun.fi/KE.1111/1234", root.getDocumentId().toURI());
		assertEquals(null, root.getPrivateDocument());
		assertTrue(root.getPublicDocument() != null);
		assertEquals("KE.1111", root.getSourceId().toString());

		Document doc = root.getPublicDocument();
		assertEquals("http://tun.fi/HR.447", doc.getCollectionId().toURI());
		assertEquals(Concealment.PUBLIC, doc.getConcealment());
		assertEquals(null, doc.getCreatedDate());
		assertEquals("http://tun.fi/KE.1111/1234", doc.getDocumentId().toURI());
		assertEquals(1, doc.getEditorUserIds().size());
		assertEquals(0, doc.getFacts().size());
		assertEquals(1, doc.getGatherings().size());
		assertEquals(0, doc.getKeywords().size());
		assertEquals(null, doc.getModifiedDate());
		assertEquals(null, doc.getNotes());
		assertEquals(SecureLevel.NONE, doc.getSecureLevel());
		assertEquals("[]", doc.getSecureReasons().toString());
		assertEquals("KE.1111", doc.getSourceId().toString());

		Gathering g = doc.getGatherings().get(0);
		assertEquals(2, g.getTeam().size());
		assertEquals("[Lahti, Tapani, Saarela, Anne]", g.getTeam().toString());
		assertEquals(null, g.getCoordinates());
		assertEquals("[POLYGON ((-4.454441 36.664701, -4.464397 36.656439, -4.471779 36.660433, -4.462681 36.671999, -4.454441 36.664701))]", g.getGeo().getWKTList().toString());
		assertEquals(Type.WGS84, g.getGeo().getCRS());
		assertEquals("/+36.664701-004.454441/+36.671999-004.462681/+36.660433-004.471779/+36.656439-004.464397/ WGS84", g.getCoordinatesVerbatim());
		assertEquals("09.04.2012", DateUtils.format(g.getEventDate().getBegin(), "dd.MM.yyyy"));
		assertEquals("09.04.2012", DateUtils.format(g.getEventDate().getEnd(), "dd.MM.yyyy"));
		assertEquals(0, g.getFacts().size());
		assertEquals(null, g.getBiogeographicalProvince());
		assertEquals("Spain", g.getCountry());
		assertEquals(null, g.getHigherGeography());
		assertEquals("Guadalmar", g.getLocality());
		assertEquals("Malaga", g.getMunicipality());
		assertEquals("Malaga 9.4.2012", g.getNotes());
		assertEquals(null, g.getProvince());
		assertEquals(10, g.getHourBegin().intValue());
		assertEquals(21, g.getHourEnd().intValue());
		assertEquals(20, g.getUnits().size());

		Unit u = g.getUnits().get(0);
		assertEquals("[]", u.getFacts().toString());
		assertEquals("50", u.getAbundanceString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
		assertEquals("Myiopsitta monachus", u.getTaxonVerbatim());
		assertEquals("http://tun.fi/KE.1111/1234#2", u.getUnitId().toURI());
		assertEquals("Several nests in the Eucalyptus trees alongside the streets", u.getNotes());
	}

	@Test
	public void hatikka_talvilintulaskenta() throws Exception {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-talvilintu-hatikkadata.xml"), new Qname("KE.8"));
		assertEquals(1, harmonized.size());
		DwRoot root = harmonized.get(0);
		assertEquals("http://tun.fi/HR.39", root.getCollectionId().toURI());
		assertEquals("http://tun.fi/KE.8/1234", root.getDocumentId().toURI());
		assertEquals(null, root.getPrivateDocument());
		assertTrue(root.getPublicDocument() != null);
		assertEquals("KE.8", root.getSourceId().toString());

		Document doc = root.getPublicDocument();
		assertEquals("[hatikka:matti.meikalainen@hatikka.fi]", root.getPublicDocument().getEditorUserIds().toString());
		assertEquals("http://tun.fi/HR.39", doc.getCollectionId().toURI());
		assertEquals(null, doc.getNamedPlaceId());
		assertEquals(Concealment.PUBLIC, doc.getConcealment());
		assertEquals(null, doc.getCreatedDate());
		assertEquals("http://tun.fi/KE.8/1234", doc.getDocumentId().toURI());
		assertEquals(1, doc.getEditorUserIds().size());
		assertEquals(0, doc.getFacts().size());
		assertEquals(1, doc.getGatherings().size());
		assertEquals(0, doc.getKeywords().size());
		assertEquals(null, doc.getModifiedDate());
		assertEquals(null, doc.getNotes());
		assertEquals(SecureLevel.NONE, doc.getSecureLevel());
		assertEquals("[]", doc.getSecureReasons().toString());
		assertEquals("KE.8", doc.getSourceId().toString());

		assertEquals(1, doc.getGatherings().size());

		Gathering g = doc.getGatherings().get(0);
		assertEquals(1, g.getTeam().size());
		assertEquals("[Silaste Risto]", g.getTeam().toString());
		assertEquals(28, g.getFacts().size());
		assertEquals("TaxonCensus : Aves", g.getFacts().get(0).toString());
		assertEquals("ReitinNumero : 3910", g.getFacts().get(1).toString());
		assertEquals(null, g.getBiogeographicalProvince());
		assertEquals("Suomi", g.getCountry());
		assertEquals(null, g.getHigherGeography());
		assertEquals("PALLAS", g.getLocality());
		assertEquals("Lahti", g.getMunicipality());
		assertEquals(null, g.getNotes());
		assertEquals(null, g.getProvince());
		assertEquals(9, g.getHourBegin().intValue());
		assertEquals(12, g.getHourEnd().intValue());
		assertEquals(20, g.getUnits().size());
		assertEquals(1, g.getTaxonCensus().size());
		assertEquals("MX.37580", g.getTaxonCensus().get(0).getTaxonId().toString());
		assertEquals("MY.taxonCensusTypeCounted", g.getTaxonCensus().get(0).getType().toString());

		Unit u = g.getUnits().get(0);
		assertEquals("[YksilömääräBiotooppiB : 216, YksilömääräSukupuolet : 153/63]", u.getFacts().toString());
		assertEquals("216", u.getAbundanceString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
		assertEquals("Anas platyrhynchos", u.getTaxonVerbatim());
		assertEquals(null, u.getNotes());

		assertEquals("valkoposkihanhi", g.getUnits().get(19).getTaxonVerbatim());
	}

	@Test
	public void parsing_geo_features() throws DataValidationException {
		// 6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/  -> LINE
		// /6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/ -> POLYGON
		// /+36.664701-004.454441/+36.671999-004.462681/+36.660433-004.471779/+36.656439-004.464397/ -> POLYGON (wgs84)
		Geo geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("666:333", Type.YKJ);
		assertEquals("[POINT (333 666)]", geo.getWKTList().toString());

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/", Type.YKJ);
		assertEquals("[LINESTRING (3636592 6833008, 3637000 6832976, 3637160 6829976, 3635736 6829880)]", geo.getWKTList().toString());

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("/6833008:3636592/6832976:3637000/6829976:3637160/6829880:3635736/", Type.YKJ);
		assertEquals("[POLYGON ((3636592 6833008, 3637000 6832976, 3637160 6829976, 3635736 6829880, 3636592 6833008))]", geo.getWKTList().toString());

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("/6833008:3636592/", Type.YKJ);
		assertEquals("[POINT (3636592 6833008)]", geo.getWKTList().toString());

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("/6833008:3636592/6832976:3637000/", Type.YKJ);
		assertEquals("[LINESTRING (3636592 6833008, 3637000 6832976)]", geo.getWKTList().toString());

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("/+36.664701-004.454441/+36.671999-004.462681/+36.660433-004.471779/+36.656439-004.464397/", Type.WGS84); // This is counter-clockwise
		assertEquals("[POLYGON ((-4.454441 36.664701, -4.464397 36.656439, -4.471779 36.660433, -4.462681 36.671999, -4.454441 36.664701))]", geo.getWKTList().toString()); // It is reversed

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("/+36.660433-004.471779/+36.664701-004.454441/+36.671999-004.462681/+36.656439-004.464397/", Type.WGS84); // This polygon intercuts itself
		assertEquals("[POLYGON ((-4.464397 36.656439, -4.471779 36.660433, -4.462681 36.671999, -4.454441 36.664701, -4.464397 36.656439))]", geo.getWKTList().toString()); // it is stored as convex hull

		geo = Fmnh2008Harmonizer.parseGeoFromFmnh2008CoordinateText("67000/33900", Type.YKJ); // This is invalid data
		assertEquals(null, geo);
	}

	@Test
	public void hatikka_wgs84_coordinatesets_2() throws Exception {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh288-coordinateset2.xml"), new Qname("KE.1111"));
		Gathering g = harmonized.get(0).getPublicDocument().getGatherings().get(0);
		String expectedVerbatim = "/-34.604742-058.361692/-34.601987-058.356972/-34.604460-058.352251/-34.610182-058.350878/-34.616893-058.350105/-34.619294-058.352938/-34.619294-058.357487/ WGS84";
		assertEquals(expectedVerbatim, g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		String expectedPolygon = "POLYGON ((-58.361692 -34.604742, -58.356972 -34.601987, -58.352251 -34.60446, -58.350878 -34.610182, -58.350105 -34.616893, -58.352938 -34.619294, -58.357487 -34.619294, -58.361692 -34.604742))";
		assertEquals(expectedPolygon, g.getGeo().getWKT());
	}

	@Test
	public void gird_and_line() throws Exception {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-grid-and-line.xml"), new Qname("KE.1111"));
		Gathering g = harmonized.get(0).getPublicDocument().getGatherings().get(0);
		String expected = "[POLYGON ((3400000 6820000, 3400000 6830000, 3410000 6830000, 3410000 6820000, 3400000 6820000)), LINESTRING (3404108 6820022, 3404042 6819915, 3404271 6819806, 3404362 6819791, 3404446 6819741, 3404480 6819739, 3404557 6819630, 3404568 6819520, 3404556 6819628, 3404479 6819741, 3404562 6819771, 3404638 6819722, 3404886 6819721, 3404927 6819696, 3404973 6819611, 3405046 6819548, 3405101 6819544, 3405218 6819577, 3405254 6819571, 3405438 6819649, 3405560 6819713, 3405617 6819720, 3405679 6819761, 3405788 6819785, 3405964 6819829, 3406070 6819897, 3406242 6819850, 3406276 6819829, 3406331 6819814, 3406376 6819815, 3406406 6819728, 3406445 6819678, 3406488 6819630, 3406548 6819598, 3406559 6819585, 3406609 6819553, 3406641 6819547, 3406669 6819555, 3406713 6819547, 3406761 6819547, 3406787 6819549, 3406813 6819568, 3406882 6819567, 3406914 6819547, 3406897 6819499, 3406913 6819545, 3407006 6819593, 3407225 6819553, 3407215 6819627, 3407173 6819708, 3407144 6819736, 3407112 6819738, 3407084 6819747, 3407007 6819832, 3406983 6819925, 3406975 6820033, 3406932 6820125, 3406900 6820205, 3406863 6820293, 3406822 6820361, 3406697 6820383, 3406663 6820446, 3406628 6820469, 3406574 6820476, 3406545 6820496, 3406483 6820479, 3406443 6820477, 3406418 6820499, 3406417 6820555, 3406396 6820587, 3406391 6820647, 3406399 6820691, 3406363 6820754, 3406339 6820801, 3406305 6820819, 3406257 6820835, 3406234 6820860, 3406191 6820886, 3406157 6820913, 3406099 6820916, 3406046 6820908, 3406034 6820911, 3405981 6820800, 3405979 6820722, 3405977 6820692, 3405946 6820645, 3405927 6820634, 3405837 6820642, 3405759 6820659, 3405682 6820661, 3405644 6820651, 3405621 6820669, 3405599 6820699, 3405523 6820849, 3405531 6820871, 3405548 6820924, 3405513 6820987, 3405509 6821004, 3405515 6821070, 3405516 6821098, 3405551 6821129, 3405559 6821131, 3405551 6821128, 3405515 6821100, 3405495 6821177, 3405462 6821229, 3405428 6821255, 3405410 6821282, 3405383 6821303, 3405371 6821331, 3405352 6821338, 3405258 6821345, 3405222 6821353, 3405186 6821403, 3405149 6821419, 3405060 6821440, 3404980 6821441, 3404941 6821446, 3404944 6821474, 3404925 6821501, 3404903 6821527, 3404914 6821560, 3404901 6821525, 3404941 6821480, 3404941 6821447, 3404903 6821440, 3404864 6821430, 3404816 6821440, 3404715 6821386, 3404699 6821360, 3404679 6821343, 3404640 6821337, 3404615 6821326, 3404581 6821312, 3404525 6821279, 3404447 6821232, 3404423 6821218, 3404402 6821192, 3404372 6821145, 3404352 6821136, 3404294 6821147, 3404224 6821120, 3404164 6821106, 3404120 6821076, 3404104 6821055, 3404085 6821005, 3404021 6820948, 3403976 6820901, 3403926 6820876, 3403905 6820855, 3403890 6820885, 3403857 6820916, 3403891 6820884, 3403907 6820852, 3403891 6820783, 3403880 6820739, 3403835 6820695, 3403801 6820671, 3403771 6820605, 3403747 6820557, 3403693 6820509, 3403669 6820447, 3403620 6820402, 3403580 6820373, 3403567 6820343, 3403581 6820303, 3403605 6820246, 3403594 6820216, 3403559 6820186, 3403525 6820155, 3403452 6820033, 3403409 6819979, 3403385 6819966, 3403352 6819963, 3403308 6819967, 3403230 6820004, 3403163 6820012, 3403115 6819998, 3403074 6819965, 3403026 6819916, 3403054 6819909, 3403085 6819878, 3403103 6819855, 3403080 6819883, 3403051 6819910, 3403024 6819917, 3403083 6819975, 3403113 6820003, 3403160 6820017, 3403227 6820003, 3403308 6819966, 3403357 6819965, 3403381 6819967, 3403442 6819939, 3403512 6819915, 3403543 6819900, 3403586 6819891, 3403642 6819856, 3403674 6819850, 3403731 6819868, 3403759 6819876, 3403816 6819881, 3403867 6819908, 3403911 6819928, 3403974 6819935, 3404021 6819934, 3404053 6819938)]";
		assertEquals(expected, g.getGeo().getWKTList().toString());
		assertEquals(null, g.getCoordinates());
	}

	@Test
	public void gathering_under_units() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-multigathering.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		Document doc = harmonized.get(0).getPublicDocument();
		assertEquals(41, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(40);
		assertEquals(2, g.getUnits().size());
		assertEquals("[Aaltonen, Matti]", g.getTeam().toString()); // in base gathering not in unit gathering
		assertEquals("6756976:3285716/6756000:3285068/ YKJ", g.getCoordinatesVerbatim()); // from unit gathering
	}

	@Test
	public void gathering_under_units_2_no_base_gathering() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-multigathering_2.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		Document doc = harmonized.get(0).getPublicDocument();
		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		assertEquals("Kamppi", g.getLocality());
		assertEquals("DateRange [begin=2016-11-25, end=2016-11-25]", g.getEventDate().toString());
	}

	@Test
	public void targetname() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-targetname.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		Document doc = harmonized.get(0).getPublicDocument();
		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);
		assertEquals(2, g.getUnits().size());
		assertEquals("Loxodonta africana", g.getUnits().get(0).getTaxonVerbatim());
		assertEquals(null, g.getUnits().get(0).getReportedTaxonConfidence());
		assertEquals("Pompiloma africana?", g.getUnits().get(1).getTaxonVerbatim());
		assertEquals(TaxonConfidence.UNSURE, g.getUnits().get(1).getReportedTaxonConfidence());
	}

	@Test
	public void bugtest() throws CriticalParseFailure, UnknownHarmonizingFailure {
		List<DwRoot> harmonized = new Fmnh2008Harmonizer().harmonize(getTestData("fmnh2008-bugtest.xml"), new Qname("KE.1111"));
		assertEquals(1, harmonized.size());
		Document doc = harmonized.get(0).getPublicDocument();
		assertEquals(1, doc.getGatherings().size());
		Gathering g = doc.getGatherings().get(0);
		assertEquals("7363916:3610622/ FI KKJ27", g.getCoordinatesVerbatim());
		assertEquals(null, g.getCoordinates());
		assertEquals("[POINT (3610622 7363916)]", g.getGeo().getWKTList().toString());
		assertEquals(null, g.createQuality().getLocationIssue());
	}

}
