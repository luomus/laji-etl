package fi.laji.datawarehouse.etl.models.harmonizers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fi.laji.datawarehouse.dao.DAOImple;
import fi.laji.datawarehouse.dao.MediaDAO;
import fi.laji.datawarehouse.etl.models.ContextDefinitions;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.Document.Concealment;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Fact;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.Unit.TaxonConfidence;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class LajistoreHarmonizerTests {

	private static final ContextDefinitions CONTEXT_DEFINITIONS;
	static {
		try {
			CONTEXT_DEFINITIONS = DAOImple.getLocalContextDefinition(TestConfig.getConfig());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static class TestMediaDAO implements MediaDAO {
		@Override
		public MediaObject getMediaObject(Qname id) {
			try {
				if (id.toString().equals("MM.1")) {
					MediaObject m = new MediaObject(MediaType.AUDIO, "https://some.domain/"+id+"/sound.wav");
					m.setId(id);
					return m;
				}
				if (id.toString().equals("MM.2")) {
					MediaObject m = new MediaObject(MediaType.IMAGE, "https://some.domain/"+id+"/image.jpg");
					m.setId(id);
					return m;
				}
				if (id.toString().equals("MM.3")) {
					MediaObject m = new MediaObject(MediaType.IMAGE, "https://some.domain/"+id+"/image.jpg").addKeyword("habitat");
					m.setId(id);
					return m;
				}
			} catch (DataValidationException e) {
				throw new RuntimeException(e);
			}
			throw new UnsupportedOperationException("No test mock yet for media id " + id);
		}

		@Override
		public List<MediaObject> getMediaObjects(Qname documentId) {
			throw new UnsupportedOperationException();
		}
	}

	private static final LajistoreHarmonizer harmonizer = new LajistoreHarmonizer(CONTEXT_DEFINITIONS, new TestMediaDAO());

	private JSONObject getTestData(String filename) {
		return new JSONObject(RdfXmlHarmonizerTests.getTestData(filename));
	}

	@Test
	public void test_basic() throws Exception {
		JSONObject json = getTestData("lajistore-1-basic.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals("http://tun.fi/JX.110761", root.getPublicDocument().getDocumentId().toURI());
		assertEquals(null, root.getPrivateDocument());

		assertEquals("HR.1747", root.getCollectionId().toString());
		assertEquals("HR.1747", root.getPublicDocument().getCollectionId().toString());
		assertEquals("KE.1", root.getSourceId().toString());
		assertEquals("KE.1", root.getPublicDocument().getSourceId().toString());

		Document doc = root.getPublicDocument();
		assertEquals("[http://tun.fi/MA.5]", doc.getEditorUserIds().toString());
		assertEquals("2016-12-15", DateUtils.format(doc.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals("2016-12-16", DateUtils.format(doc.getModifiedDate(), "yyyy-MM-dd"));
		assertEquals("[]", doc.getFacts().toString());
		assertEquals("http://tun.fi/JX.519", doc.getFormId());
		assertEquals(SecureLevel.NONE, doc.getSecureLevel());
		assertEquals(0, doc.getSecureReasons().size());

		assertEquals(1, doc.getGatherings().size());

		Gathering g1 = doc.getGatherings().get(0);
		assertEquals("JX.110761#2", g1.getGatheringId().toString());
		assertEquals("[MA.5, MA.97, Tuntti Tuntematon]", g1.getTeam().toString()); // from gathering event
		assertEquals("DateRange [begin=2016-12-15, end=2016-12-16]", g1.getEventDate().toString()); // from gathering
		assertEquals(14, g1.getHourBegin().intValue()); // from gathering
		assertEquals(1, g1.getHourEnd().intValue()); // from gathering
		assertEquals("[]", g1.getFacts().toString());
		assertEquals(null, g1.getLocality());
		assertEquals(null, g1.getNotes());

		assertEquals(1, g1.getUnits().size());

		Unit u = g1.getUnits().get(0);

		assertEquals("susi", u.getTaxonVerbatim());
		assertEquals(TaxonConfidence.SURE, u.getReportedTaxonConfidence());
		assertEquals("MX.46549", u.getReportedTaxonId().toString());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, u.getRecordBasis());
		assertEquals("[]", u.getFacts().toString());
		assertEquals(null, u.getNotes());
		assertEquals("1", u.getAbundanceString());
		assertNull(u.isBreedingSite());
		assertNull(u.isWild());
		assertNull(u.isAlive());
		assertNull(u.isLocal());
	}

	@Test
	public void test_alive_lifestage() throws Exception {
		JSONObject json = getTestData("lajistore-alivelifestage-1.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u = g1.getUnits().get(0);

		assertNull(u.getLifeStage());
		assertTrue(u.isAlive());
	}

	@Test
	public void test_alive_lifestage_2() throws Exception {
		JSONObject json = getTestData("lajistore-alivelifestage-2.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u = g1.getUnits().get(0);

		assertNull(u.getLifeStage());
		assertFalse(u.isAlive());
	}

	@Test
	public void test_local_valid_moving_status() throws Exception {
		JSONObject json = getTestData("lajistore-local-validmovingstatus.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g1.getUnits().get(1);

		assertTrue(u1.isLocal());
		assertTrue(u2.isLocal());
	}

	@Test
	public void test_local_invalid_moving_status() throws Exception {
		JSONObject json = getTestData("lajistore-local-invalidmovingstatus.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g1.getUnits().get(1);

		assertNull(u1.isLocal());
		assertNull(u2.isLocal());
	}

	@Test
	public void test_local_migrating_moving_status() throws Exception {
		JSONObject json = getTestData("lajistore-local-migratingmovingstatus.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g1.getUnits().get(1);

		assertTrue(u1.isLocal());
		assertFalse(u2.isLocal());
	}

	@Test
	public void test_local_breeding() throws Exception {
		JSONObject json = getTestData("lajistore-local-migratingmovingstatus.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g1.getUnits().get(1);

		assertTrue(u1.isLocal());
		assertFalse(u2.isLocal());
	}

	@Test
	public void test_basic_no_end_time() throws Exception {
		JSONObject json = getTestData("lajistore-1-basic-no_end_time.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("[]", root.getPublicDocument().getFacts().toString());
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		assertEquals("DateRange [begin=2016-12-16, end=2016-12-16]", g.getEventDate().toString());
		assertEquals(15, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
		assertEquals("[]", g.getFacts().toString());
	}

	@Test
	public void test_basic_no_time_of_day() throws Exception {
		JSONObject json = getTestData("lajistore-1-basic-no_end_time.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("DateRange [begin=2016-12-16, end=2016-12-16]", root.getPublicDocument().getGatherings().get(0).getEventDate().toString());
	}

	@Test
	public void time_format_to_be_removed_from_use() {
		Gathering g = Gathering.emptyGathering();
		BaseHarmonizer.parseEventDateTime("2016-12-15T16:30:00.000Z", g);
		assertEquals("DateRange [begin=2016-12-15, end=2016-12-15]", g.getEventDate().toString());
		assertEquals(16, g.getHourBegin().intValue());
		assertEquals(null, g.getHourEnd());
	}

	@Test
	public void test_multieditors() throws Exception {
		JSONObject json = getTestData("lajistore-1-multieditors.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("[http://tun.fi/MA.97, http://tun.fi/MA.5]", root.getPublicDocument().getEditorUserIds().toString());
	}

	@Test
	public void test_nonpublic_leg() throws Exception {
		JSONObject json = getTestData("lajistore-1-nonpublic_leg.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals("[]", root.getPublicDocument().getEditorUserIds().toString());
		assertEquals("[]", root.getPublicDocument().getGatherings().get(0).getTeam().toString());

		assertEquals("[http://tun.fi/MA.5]", root.getPrivateDocument().getEditorUserIds().toString());
		assertEquals("[MA.5, MA.97, Tuntti Tuntematon]", root.getPrivateDocument().getGatherings().get(0).getTeam().toString());
	}

	@Test
	public void test_10km_secure() throws Exception {
		JSONObject json = getTestData("lajistore-1-10km_secure.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("["+SecureReason.USER_HIDDEN_LOCATION+"]", root.getPublicDocument().getSecureReasons().toString());
		assertEquals("[http://tun.fi/MA.5]", root.getPublicDocument().getEditorUserIds().toString());
		assertEquals("[MA.5, MA.97, Tuntti Tuntematon]", root.getPublicDocument().getGatherings().get(0).getTeam().toString());

		assertEquals("["+SecureReason.USER_HIDDEN_LOCATION+"]", root.getPrivateDocument().getSecureReasons().toString());

		assertEquals("752.0 : 753.0 : 349.0 : 350.0 : YKJ : 10000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getGeo());

		assertEquals(null, root.getPrivateDocument().getGatherings().get(0).getCoordinates());
		assertEquals("[POINT (26.902831 67.806678)]", root.getPrivateDocument().getGatherings().get(0).getGeo().getWKTList().toString());

		assertEquals("Jossain", root.getPrivateDocument().getGatherings().get(0).getLocality());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getLocality());

		assertEquals("DateRange [begin=2016-12-15, end=2016-12-15]", root.getPrivateDocument().getGatherings().get(0).getEventDate().toString());
		assertEquals("DateRange [begin=2016-12-15, end=2016-12-15]", root.getPublicDocument().getGatherings().get(0).getEventDate().toString());

		assertEquals(""+
				"something that reveals location\n"+
				"under Pasila bridge\n"+
				"sunny", root.getPrivateDocument().getGatherings().get(0).getNotes());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getNotes());
	}

	@Test
	public void test_10km_secure_insectdb() throws Exception {
		JSONObject json = getTestData("lajistore-1-10km_secure.json");

		json.getArray("roots").iterateAsObject().get(0).getObject("document").setString("collectionID", LajistoreHarmonizer.INSECT_DB_COLLECTION_ID.toString());

		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("["+SecureReason.USER_HIDDEN_LOCATION+", "+SecureReason.USER_HIDDEN_TIME+"]", root.getPrivateDocument().getSecureReasons().toString());
		assertEquals("752.0 : 753.0 : 349.0 : 350.0 : YKJ : 10000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getGeo());
		assertEquals(null, root.getPrivateDocument().getGatherings().get(0).getCoordinates());
		assertEquals("[POINT (26.902831 67.806678)]", root.getPrivateDocument().getGatherings().get(0).getGeo().getWKTList().toString());

		assertEquals("DateRange [begin=2016-12-15, end=2016-12-15]", root.getPrivateDocument().getGatherings().get(0).getEventDate().toString());
		assertEquals("DateRange [begin=2016-01-01, end=2016-12-31]", root.getPublicDocument().getGatherings().get(0).getEventDate().toString());
	}

	@Test
	public void test_10km_secure_foreign() throws Exception {
		JSONObject json = getTestData("lajistore-1-10km_secure_foreign.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("["+SecureReason.USER_HIDDEN_LOCATION+"]", root.getPublicDocument().getSecureReasons().toString());
		assertEquals("["+SecureReason.USER_HIDDEN_LOCATION+"]", root.getPrivateDocument().getSecureReasons().toString());

		assertEquals("67.8 : 67.9 : -27.0 : -26.9 : WGS84 : 10000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		assertEquals(null, root.getPublicDocument().getGatherings().get(0).getGeo());

		assertEquals(null, root.getPrivateDocument().getGatherings().get(0).getCoordinates());
		assertEquals("[POINT (-26.902831 67.806678)]", root.getPrivateDocument().getGatherings().get(0).getGeo().getWKTList().toString());
	}

	@Test
	public void test_multigathering_unitgathering() throws Exception {
		JSONObject json = getTestData("lajistore-2-multigathering-unitgathering.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(3, root.getPublicDocument().getGatherings().size());
		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		Gathering g3 = root.getPublicDocument().getGatherings().get(2);

		assertEquals("http://tun.fi/JX.110766#2", g1.getGatheringId().toURI()); // first gathering
		assertEquals("1", g1.getLocality());
		assertEquals(1, g1.getUnits().size());
		assertEquals("POINT (26.822489 65.570093)", g1.getGeo().getWKT());
		assertEquals("DateRange [begin=2016-12-16, end=2016-12-16]", g1.getEventDate().toString());
		assertEquals(10, g1.getHourBegin().intValue());
		assertEquals(null, g1.getHourEnd());

		assertEquals("http://tun.fi/JX.110766#6", g2.getGatheringId().toURI()); // second gathering with only one unit where unit has it's own gathering -> this gathering has no units
		assertEquals("2", g2.getLocality());
		assertEquals(0, g2.getUnits().size());
		assertEquals("POLYGON ((27.416192 66.450188, 27.416192 66.616941, 27.780906 66.616941, 27.780906 66.450188, 27.416192 66.450188))", g2.getGeo().getWKT());
		assertEquals("DateRange [begin=2016-12-16, end=2016-12-16]", g2.getEventDate().toString());
		assertEquals(11, g2.getHourBegin().intValue());
		assertEquals(null, g2.getHourEnd());

		assertEquals("http://tun.fi/JX.110766#7", g3.getGatheringId().toURI()); // unit taken out of second gathering
		assertEquals("2", g3.getLocality());
		assertEquals(1, g3.getUnits().size());
		assertEquals("POINT (27.597616 66.46937)", g3.getGeo().getWKT());
		assertEquals(g2.getEventDate().toString(), g3.getEventDate().toString());
		assertEquals(g2.getHourBegin(), g3.getHourBegin());
		assertEquals(g2.getHourEnd(), g3.getHourEnd());

	}

	@Test
	public void test_multigathering_unitgathering_2() throws Exception {
		JSONObject json = getTestData("lajistore-5-multigathering-bug.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(6, root.getPublicDocument().getGatherings().size());
		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		Gathering g3 = root.getPublicDocument().getGatherings().get(2);
		Gathering g4 = root.getPublicDocument().getGatherings().get(3);
		Gathering g5 = root.getPublicDocument().getGatherings().get(4);
		Gathering g6 = root.getPublicDocument().getGatherings().get(5);

		assertEquals("http://tun.fi/JX.5845#2", g1.getGatheringId().toURI());
		assertEquals(6, g1.getUnits().size());
		assertEquals(
				"LINESTRING (24.610165 60.19321, 24.605882 60.19157, 24.608924 60.189433, 24.610376 60.189315, 24.612087 60.189598, 24.612672 60.189501, 24.613622 60.188332, 24.613685 60.186968, 24.614597 60.186337, 24.616149 60.185826, 24.617404 60.184933, 24.617209 60.184642, 24.617531 60.18418, 24.621121 60.184424, 24.621919 60.184367, 24.623366 60.184303, 24.622248 60.184804, 24.622937 60.185248, 24.620839 60.186324, 24.618855 60.185821)",
				g1.getGeo().getWKT());

		assertEquals("http://tun.fi/JX.5845#15", g2.getGatheringId().toURI());
		assertEquals(1, g2.getUnits().size());
		assertEquals("POINT (24.60949 60.190331)", g2.getGeo().getWKT());

		assertEquals("http://tun.fi/JX.5845#18", g3.getGatheringId().toURI());
		assertEquals(1, g3.getUnits().size());

		assertEquals("http://tun.fi/JX.5845#21", g4.getGatheringId().toURI());
		assertEquals(1, g4.getUnits().size());

		assertEquals("http://tun.fi/JX.5845#27", g5.getGatheringId().toURI());
		assertEquals(1, g5.getUnits().size());

		assertEquals("http://tun.fi/JX.5845#30", g6.getGatheringId().toURI());
		assertEquals(1, g6.getUnits().size());
	}

	@Test
	public void test_images() throws Exception {
		JSONObject json = getTestData("lajistore-3-images.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals("[]", root.getPublicDocument().getMedia().toString());

		assertEquals(2, root.getPublicDocument().getGatherings().size());

		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);

		// original media attached to gathering (mm.1) is in g1
		assertEquals("" +
				"[MediaObject [id=MM.1, mediaType=AUDIO, fullURL=https://some.domain/MM.1/sound.wav, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null]]",
				g1.getMedia().toString());

		// g1 has unit that has a habitat media -> that unit is splitted to it's own gathering (g2) and that gathering has the original g1 media (mm.1) and the unit media (mm.3)
		assertEquals("[" +
				"MediaObject [id=MM.1, mediaType=AUDIO, fullURL=https://some.domain/MM.1/sound.wav, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null], " +
				"MediaObject [id=MM.3, mediaType=IMAGE, fullURL=https://some.domain/MM.3/image.jpg, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=[habitat], type=null, fullResolutionMediaAvailable=null]]",
				g2.getMedia().toString());

		assertEquals(0, g1.getUnits().size());
		assertEquals(1, g2.getUnits().size());

		// unit that is splitted to g2 has the other media (mm.2) that was attached to that unit originally
		assertEquals("[MediaObject [id=MM.2, mediaType=IMAGE, fullURL=https://some.domain/MM.2/image.jpg, thumbnailURL=null, squareThumbnailURL=null, mp3URL=null, wavURL=null, videoURL=null, lowDetailModelURL=null, highDetailModelURL=null, author=null, copyrightOwner=null, licenseId=null, caption=null, keywords=null, type=null, fullResolutionMediaAvailable=null]]",
				g2.getUnits().get(0).getMedia().toString());
	}

	@Test
	public void test_geoshapes() throws Exception {
		JSONObject json = getTestData("lajistore-4-geoshapes.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertEquals(1, root.getPublicDocument().getGatherings().size());

		assertEquals("[" +
				// line
				"LINESTRING (25.43339 65.051463, 25.726497 65.293169, 26.249731 65.425317, 26.604446 65.316406, 26.73848 65.096188, 26.568599 64.820144), "+
				// polygon (triangle)
				"POLYGON ((26.362026 64.433464, 27.214245 64.636845, 26.831334 64.195837, 26.362026 64.433464)), "+
				// square
				"POLYGON ((26.45438 63.717868, 26.45438 64.066262, 27.248722 64.066262, 27.248722 63.717868, 26.45438 63.717868)), "+
				// point with radius
				"POLYGON ((27 63.214262121057764, 27.24636759230252 63.168022447360244, 27.348416390363425 63.05639, 27.24636759230252 62.94475755263976, 27 62.898517878942236, 26.75363240769748 62.94475755263976, 26.651583609636575 63.05639, 26.75363240769748 63.168022447360244, 27 63.214262121057764)), "+
				// point
				"POINT (27 62.339485)]",
				root.getPublicDocument().getGatherings().get(0).getGeo().getWKTList().toString());
	}

	@Test
	public void test_taxon_confidence() throws Exception {
		JSONObject json = getTestData("lajistore-1-basic-uncertain-units.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		Document doc = root.getPublicDocument();
		Gathering g1 = doc.getGatherings().get(0);
		Unit u1 = g1.getUnits().get(0);
		Unit u2 = g1.getUnits().get(1);
		Unit u3 = g1.getUnits().get(2);

		assertEquals("susi", u1.getTaxonVerbatim());
		assertEquals(TaxonConfidence.UNSURE, u1.getReportedTaxonConfidence());

		assertEquals("mäyrä", u2.getTaxonVerbatim());
		assertEquals(TaxonConfidence.SUBSPECIES_UNSURE, u2.getReportedTaxonConfidence());

		assertEquals("koira", u3.getTaxonVerbatim());
		assertEquals(null, u3.getReportedTaxonConfidence());
	}

	@Test
	public void test_all_units_have_unitgathering() throws Exception {
		JSONObject json = getTestData("lajistore-6-unitgatherings.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		Document doc = root.getPublicDocument();
		assertEquals(3, doc.getGatherings().size());
		assertEquals(0, doc.getGatherings().get(0).getUnits().size());
		assertEquals(1, doc.getGatherings().get(1).getUnits().size());
		assertEquals(1, doc.getGatherings().get(02).getUnits().size());
	}

	@Test
	public void test_unit_gathering_facts() throws Exception {
		JSONObject json = getTestData("lajistore-6-unitgatherings-facts.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		Document doc = root.getPublicDocument();
		assertEquals(1, doc.getGatherings().size());

		Gathering g = doc.getGatherings().get(0);
		assertEquals(1, g.getUnits().size());

		Unit u = g.getUnits().get(0);
		assertEquals(
				"[http://tun.fi/MY.identificationBasis : http://tun.fi/MY.identificationBasisMicroscope, http://tun.fi/MY.habitatDescription : Kostealla maalla blaa, http://tun.fi/MY.habitatIUCN : iucn-hab-val, http://tun.fi/MY.substrate : substrate-val]",
				u.getFacts().toString());
	}

	@Test
	public void test_ykj_verbatim() throws Exception {
		JSONObject json = getTestData("lajistore-7-ykj-verbatim.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		assertEquals(3, root.getPublicDocument().getGatherings().size());

		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		Gathering g3 = root.getPublicDocument().getGatherings().get(2);

		assertEquals(null, g1.getGeo());
		assertEquals("6666:3333", g1.getCoordinatesVerbatim());
		assertEquals("6666.0 : 6667.0 : 3333.0 : 3334.0 : YKJ : null", g1.getCoordinates().toString());

		assertEquals("POINT (37.845466 67.8)", g2.getGeo().getWKT());
		assertEquals("67.8:37.8454657", g2.getCoordinatesVerbatim());
		assertEquals(null, g2.getCoordinates());

		assertEquals("POINT (26.67551 63.092781)", g3.getGeo().getWKT());
		assertEquals(null, g3.getCoordinatesVerbatim());
		assertEquals(null, g3.getCoordinates());
	}

	@Test
	public void test_valid_geom_with_invalid_ykj_verbatim() throws Exception {
		JSONObject json = getTestData("lajistore-7-2-invalid-ykj-verbatim.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);

		// first gathering has  "coordinateVerbatim":"xxx:yyy"
		// second gathering has "coordinateVerbatim":"67.8:37.8454657"
		// third is a point without verbatim

		assertEquals(3, root.getPublicDocument().getGatherings().size());

		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		assertEquals("POLYGON ((23.996941 60.071776, 23.996125 60.080739, 24.014066 60.081146, 24.014877 60.072183, 23.996941 60.071776))",
				g1.getGeo().getWKT());
		assertEquals("xxx:yyy", g1.getCoordinatesVerbatim());
		assertEquals(null, g1.getCoordinates());

		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		assertEquals("POINT (37.845466 67.8)",
				g2.getGeo().getWKT());
		assertEquals("67.8xx:37.8454657", g2.getCoordinatesVerbatim());
		assertEquals(null, g2.getCoordinates());

		Gathering g3 = root.getPublicDocument().getGatherings().get(2);
		assertEquals("POINT (26.67551 63.092781)",
				g3.getGeo().getWKT());
		assertEquals(null, g3.getCoordinatesVerbatim());
		assertEquals(null, g3.getCoordinates());
	}

	@Test
	public void test_unit_gathering_dates() throws Exception {
		JSONObject json = getTestData("lajistore-8-dates-in-unitgathering.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		assertEquals(3, root.getPublicDocument().getGatherings().size());

		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		Gathering g3 = root.getPublicDocument().getGatherings().get(2);

		assertEquals(1, g1.getUnits().size());
		assertEquals(1, g2.getUnits().size());
		assertEquals(1, g3.getUnits().size());

		assertEquals("DateRange [begin=2017-06-14, end=2017-06-14]", g1.getEventDate().toString());
		assertEquals(3, g1.getHourBegin().intValue());
		assertEquals(21, g1.getHourEnd().intValue());
		assertEquals(0, g1.getMinutesBegin().intValue());
		assertEquals(30, g1.getMinutesEnd().intValue());

		assertEquals("DateRange [begin=2017-06-14, end=2017-06-14]", g2.getEventDate().toString());
		assertEquals(9, g2.getHourBegin().intValue());
		assertEquals(null, g2.getHourEnd());
		assertEquals(30, g2.getMinutesBegin().intValue());
		assertEquals(null, g2.getMinutesEnd());

		assertEquals("DateRange [begin=2017-06-14, end=2017-06-15]", g3.getEventDate().toString());
		assertEquals(11, g3.getHourBegin().intValue());
		assertEquals(12, g3.getHourEnd().intValue());
		assertEquals(30, g3.getMinutesBegin().intValue());
		assertEquals(0, g3.getMinutesEnd().intValue());
	}

	@Test
	public void test_gathering_fields() throws Exception {
		JSONObject json = getTestData("lajistore-9-gathering-fields.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals("Locality", g.getLocality());
		assertEquals("Notes\nLocality desc\nWeather", g.getNotes());
		assertEquals("[]", g.getFacts().toString());
	}

	@Test
	public void polygon_bug() throws Exception {
		JSONObject json = getTestData("lajistore-polygon-bugtext.json"); // first geometry is polygon with all four points being the same point
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);

		assertEquals(null, g.createQuality().getLocationIssue());
		assertEquals("[" +
				"POINT (26.873754 60.757934), POLYGON ((26.87346 60.753774, 26.87346 60.758005, 26.880963 60.758005, 26.880963 60.753774, 26.87346 60.753774))]",
				g.getGeo().getWKTList().toString());
	}

	@Test
	public void winterbird() throws Exception {
		JSONObject json = getTestData("lajistore-winterbird.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		Unit u = g.getUnits().get(0);

		System.out.println(root.getPublicDocument().toJSON().beautify());

		assertEquals("MX.25860", u.getReportedTaxonId().toString());
		assertEquals("1", u.getAbundanceString());
		assertEquals("[http://tun.fi/WBC.individualCountBiotopeA : 1]", u.getFacts().toString());
		assertEquals("[TaxonCensus [taxonId=MX.37580, type=MY.taxonCensusTypeCounted], TaxonCensus [taxonId=MX.37612, type=MY.taxonCensusTypeNotCounted]]", g.getTaxonCensus().toString());
		assertEquals(28, g.getFacts().size());
		assertEquals("http://tun.fi/WBC.abundanceBOMGAR : http://tun.fi/WBC.speciesAbundanceEnum2", g.getFacts().get(0).toString());

	}

	@Test
	public void informalgroupAsIdentification() throws Exception {
		JSONObject json = getTestData("lajistore-informalgroup.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		DwRoot root = roots.get(0);
		assertEquals(1, root.getPublicDocument().getGatherings().size());
		Gathering g = root.getPublicDocument().getGatherings().get(0);
		Unit u = g.getUnits().get(0);

		System.out.println(root.getPublicDocument().toJSON().beautify());

		assertEquals(null, u.getReportedTaxonId());
		assertEquals(null, u.getTaxonVerbatim());
		assertEquals(new Qname("MVL.140"), u.getReportedInformalTaxonGroup());
	}

	@Test
	public void fieldExpansion() throws Exception {
		JSONObject json = getTestData("lajistore-field-expansion.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertNotNull(root.getPublicDocument());
		assertNotNull(root.getPrivateDocument());

		Document privateDoc = root.getPrivateDocument();

		System.out.println(privateDoc.toJSON().beautify());

		assertEquals(2, privateDoc.getGatherings().size());
		Gathering baseG = privateDoc.getGatherings().get(0);
		Gathering unitG = privateDoc.getGatherings().get(1);
		assertEquals(0, baseG.getUnits().size());
		assertEquals(1, unitG.getUnits().size());

		Unit unit = unitG.getUnits().get(0);

		assertEquals(Concealment.PRIVATE, privateDoc.getConcealment());
		assertEquals(SecureLevel.NONE, privateDoc.getSecureLevel());
		assertEquals(SecureLevel.NONE, root.getPublicDocument().getSecureLevel());
		assertEquals("["+SecureReason.USER_PERSON_NAMES_HIDDEN+", "+SecureReason.USER_HIDDEN_LOCATION+"]", privateDoc.getSecureReasons().toString());
		assertEquals("HR.1747", privateDoc.getCollectionId().toString());
		assertEquals("KE.1", privateDoc.getSourceId().toString());
		assertEquals("JX.129560", privateDoc.getDocumentId().toString());
		assertEquals("["+new Qname("MA.5").toURI()+"]", privateDoc.getEditorUserIds().toString());
		assertEquals("2018-03-01", DateUtils.format(privateDoc.getCreatedDate(), "yyyy-MM-dd"));
		assertEquals("2018-03-01", DateUtils.format(privateDoc.getModifiedDate(), "yyyy-MM-dd"));
		assertEquals("[http://tun.fi/MY.keywords : keyword]", privateDoc.getFacts().toString());
		assertEquals("[keyword]", privateDoc.getKeywords().toString());
		assertEquals(null, privateDoc.getFirstLoadDate());
		assertEquals(null, privateDoc.getLoadDate());
		assertEquals(null, privateDoc.getNotes());
		assertEquals(null, privateDoc.getQuality());

		assertEquals("JX.129560#2", baseG.getGatheringId().toString());
		assertEquals("6666.0 : 6667.0 : 3333.0 : 3334.0 : YKJ : null", baseG.getCoordinates().toString());
		assertEquals("6666:3333", baseG.getCoordinatesVerbatim());

		assertEquals(null, baseG.getGeo());
		assertEquals("[POINT (24.0097 60.077008)]", unitG.getGeo().getWKTList().toString());
		assertEquals(Type.WGS84, unitG.getGeo().getCRS());
		assertEquals(null, unitG.getCoordinates());
		assertEquals(null, unitG.getCoordinatesVerbatim());


		assertEquals("2018-02-28 [9:30-15:30]", baseG.getDisplayDateTime());
		assertEquals("DateRange [begin=2018-02-28, end=2018-02-28]", baseG.getEventDate().toString());
		assertEquals(9, baseG.getHourBegin().intValue());
		assertEquals(15, baseG.getHourEnd().intValue());
		assertEquals(30, baseG.getMinutesBegin().intValue());
		assertEquals(30, baseG.getMinutesEnd().intValue());

		assertEquals("2018-02-27 [2:30] - 2018-02-28 [20:30]", unitG.getDisplayDateTime());

		testBase(baseG);
		testBase(unitG);

		assertEquals("JX.129560#4", unit.getUnitId().toString());

		assertEquals("1", unit.getAbundanceString());
		assertEquals(2, unit.getIndividualCountMale().intValue());
		assertEquals(3, unit.getIndividualCountFemale().intValue());

		assertEquals(Sex.FEMALE, unit.getSex()); // this test data does not make sense: count, male count and female count reported but sex is female

		assertEquals("kettu", unit.getTaxonVerbatim());
		assertEquals("MR.1", unit.getReferencePublication().toString());
		assertEquals("MVL.2", unit.getReportedInformalTaxonGroup().toString());
		assertEquals(null, unit.getReportedTaxonId());
		assertEquals("MX.46587", unit.getAutocompleteSelectedTaxonId().toString());
		assertEquals(TaxonConfidence.SUBSPECIES_UNSURE, unit.getReportedTaxonConfidence());
		assertEquals(null, unit.getAuthor());
		assertEquals("Det", unit.getDet());
		assertEquals(null, unit.getIndividualId());
		assertEquals(LifeStage.SUBTERRANEAN, unit.getLifeStage()); // plant lifestage beats lifestage
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, unit.getRecordBasis());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED, unit.getSuperRecordBasis());

		List<Fact> expected = new ArrayList<>();
		expected.add(new Fact("http://tun.fi/MY.detDate", "2018-03-01"));
		expected.add(new Fact("http://tun.fi/MY.identificationBasis", "http://tun.fi/MY.identificationBasisChemical"));
		expected.add(new Fact("http://tun.fi/MY.identificationBasis", "http://tun.fi/MY.identificationBasisHandled"));
		expected.add(new Fact("http://tun.fi/MY.hostInformalNameString", "susi"));
		expected.add(new Fact("http://tun.fi/MY.substrateSpecies", "MX.47180"));
		expected.add(new Fact("http://tun.fi/MY.taste", "http://tun.fi/MY.tasteWeak"));
		expected.add(new Fact("http://tun.fi/MY.tasteNotes", "maku notes"));
		expected.add(new Fact("http://tun.fi/MY.smell", "http://tun.fi/MY.smellNoSmelled"));
		expected.add(new Fact("http://tun.fi/MY.smellNotes", "haju notes"));
		expected.add(new Fact("http://tun.fi/MY.preservation", "http://tun.fi/MY.preservationPinned"));
		expected.add(new Fact("http://tun.fi/MY.movingStatus", "Än (nähty laulava lintu)"));
		expected.add(new Fact("http://tun.fi/MY.movingStatus", "P (paikallinen)"));
		expected.add(new Fact("http://tun.fi/MY.twitched", "true"));
		expected.add(new Fact("http://tun.fi/MY.wild", "http://tun.fi/MY.wildNonWild"));
		expected.add(new Fact("http://tun.fi/MY.samplingMethod", "http://tun.fi/MY.samplingMethodLight")); // Note that we are using harmonizer with a hard coded context definitions file that only has this one fact as a property -> only this is transformed into an URL
		expected.add(new Fact("http://tun.fi/MY.habitatIUCN", "biotooppi"));
		expected.add(new Fact("http://tun.fi/MY.habitatDescription", "biot kuvaus"));
		expected.add(new Fact("http://tun.fi/MY.substrate", "kasvumuistiinpanot"));

		assertEquals(Utils.debugS(expected), Utils.debugS(unit.getFacts()));
		assertEquals("unit lisätiedot", unit.getNotes());
		assertEquals(null, unit.getQuality());

		assertEquals("http://tun.fi/MY.plantStatusCodeTNV", unit.getPlantStatusCode());
		assertTrue(unit.isBreedingSite());
		assertFalse(unit.isWild());
		assertFalse(unit.isTypeSpecimen());
		assertFalse(unit.isAlive());

		Gathering pubBasG = root.getPublicDocument().getGatherings().get(1);
		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : 10000", pubBasG.getCoordinates().toString()); // coordinates coarsed to 10km in public document
		assertEquals(null, pubBasG.getCoordinatesVerbatim());
		assertEquals(null, baseG.getGeo());

		assertEquals("http://tun.fi/MY.samplingMethodLight", unit.getSamplingMethod());
		assertEquals("[MY.identificationBasisChemical, MY.identificationBasisHandled]", unit.getIdentificationBasis().toString());
	}

	private void testBase(Gathering g) {
		assertEquals("Suomi", g.getCountry());
		assertEquals(null, g.getHigherGeography());
		assertEquals("hall maakunta", g.getProvince());
		assertEquals("Uusimaa", g.getBiogeographicalProvince());
		assertEquals("Inkoo, Jamuu", g.getMunicipality());
		assertEquals("locality", g.getLocality());
		assertEquals("[MA.5]", g.getTeam().toString()); // lajistore hack, will be transformed into name in Interpreter
		assertEquals("[]", g.getObserverUserIds().toString()); // resolved from team in Interpreter

		assertEquals("[TaxonCensus [taxonId=MX.37580, type=MY.taxonCensusTypeCounted], TaxonCensus [taxonId=jokinmuutuntematontaksoni, type=MY.taxonCensusTypeUHEXCounted]]",
				g.getTaxonCensus().toString());

		List<Fact> expected = new ArrayList<>();
		expected.add(new Fact(new Qname("MY.samplingMethod").toURI(), new Qname("MY.samplingMethodLightTrap").toURI()));
		expected.add(new Fact(new Qname("MY.samplingMethodNotes").toURI(), "ker menetelmä lisätiedot"));
		expected.add(new Fact(new Qname("MY.habitat").toURI(), new Qname("MY.habitatEnumValue3").toURI()));
		expected.add(new Fact(new Qname("MY.habitat").toURI(), new Qname("MY.habitatEnumValue16").toURI()));
		expected.add(new Fact(new Qname("MY.habitatDescription").toURI(), "biot kuvaus"));
		expected.add(new Fact(new Qname("MY.observationDays").toURI(), "1"));
		expected.add(new Fact(new Qname("MY.observationMinutes").toURI(), "2"));
		expected.add(new Fact(new Qname("MY.trapCount").toURI(), "3"));

		assertEquals(expected.toString(), g.getFacts().toString());
		assertEquals("lisätiedot\nplace desc\nsää", g.getNotes());
		assertEquals(null, g.getQuality());
	}

	@Test
	public void linetransect() throws Exception {
		JSONObject json = getTestData("lajistore-linetransect.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);

		assertNotNull(root.getPublicDocument());
		assertNull(root.getPrivateDocument());

		Document d = root.getPublicDocument();

		System.out.println(d.toJSON().beautify());

		assertEquals("http://tun.fi/MNP.27456", d.getNamedPlaceId());
		assertEquals("Lisätiedot laskennasta (laskentakohtainen)", d.getNotes());
		assertEquals("[http://tun.fi/MY.timeStart : 04:05, http://tun.fi/MY.timeEnd : 05:32]", d.getFacts().toString());

		assertEquals(2, d.getGatherings().size());
		Gathering g1 = d.getGatherings().get(0);
		Gathering g2 = d.getGatherings().get(1);

		assertEquals(
				"[http://tun.fi/MY.habitatClassification : HhA5, http://tun.fi/MY.timeStart : 04:00, http://tun.fi/MY.lineTransectSegmentCounted : true, http://tun.fi/MY.lineTransectSegmentMetersStart : 0, http://tun.fi/MY.lineTransectSegmentMetersEnd : 25]",
				g1.getFacts().toString());
		assertEquals(
				"[http://tun.fi/MY.habitatClassification : RoK12, http://tun.fi/MY.lineTransectSegmentCounted : true, http://tun.fi/MY.lineTransectSegmentMetersStart : 25, http://tun.fi/MY.lineTransectSegmentMetersEnd : 300]",
				g2.getFacts().toString());

		assertEquals("2017-06-29 [4:00]", g1.getDisplayDateTime());
		assertEquals("2017-06-29 [4:05-5:32]", g2.getDisplayDateTime());

		assertEquals(3, g1.getUnits().size());
		assertEquals(1, g2.getUnits().size());

		Unit u1 = g1.getUnits().get(0);
		assertEquals("Parus major", u1.getTaxonVerbatim());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_HEARD, u1.getRecordBasis());
		assertEquals("1", u1.getAbundanceString());
		assertEquals(
				"[http://tun.fi/MY.shortHandText : parmaj x, http://tun.fi/MY.pairCount : 1, http://tun.fi/MY.lineTransectObsType : http://tun.fi/MY.lineTransectObsTypeSong, http://tun.fi/MY.lineTransectRouteFieldType : http://tun.fi/MY.lineTransectRouteFieldTypeOuter]",
				u1.getFacts().toString());
		assertEquals(null, u1.getLifeStage());
		assertEquals(null, u1.getSex());
		assertEquals(null, u1.isBreedingSite());

		Unit u2 = g1.getUnits().get(1);
		assertEquals("Parus major", u2.getTaxonVerbatim());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_SEEN, u2.getRecordBasis());
		assertEquals("5", u2.getAbundanceString());
		assertEquals(
				"[http://tun.fi/MY.shortHandText : parmaj a 5 o p, http://tun.fi/MY.pairCount : 3, http://tun.fi/MY.lineTransectObsType : http://tun.fi/MY.lineTransectObsTypeSeenNest, http://tun.fi/MY.lineTransectRouteFieldType : http://tun.fi/MY.lineTransectRouteFieldTypeInner]",
				u2.getFacts().toString());
		assertEquals(null, u2.getLifeStage());
		assertEquals(null, u2.getSex());
		assertEquals(true, u2.isBreedingSite());

		Unit u3 = g1.getUnits().get(2);
		assertEquals("Parus major", u3.getTaxonVerbatim());
		assertEquals(RecordBasis.HUMAN_OBSERVATION_SEEN, u3.getRecordBasis());
		assertEquals("5", u3.getAbundanceString());
		assertEquals(
				"[http://tun.fi/MY.shortHandText : parmaj a5y, http://tun.fi/MY.pairCount : 3, http://tun.fi/MY.lineTransectObsType : http://tun.fi/MY.lineTransectObsTypeSeenFemale, http://tun.fi/MY.lineTransectRouteFieldType : http://tun.fi/MY.lineTransectRouteFieldTypeOuter]",
				u3.getFacts().toString());
		assertEquals(LifeStage.ADULT, u3.getLifeStage());
		assertEquals(Sex.FEMALE, u3.getSex());
		assertEquals(null, u3.isBreedingSite());
	}

	@Test
	public void legUserId() throws Exception {
		JSONObject json = getTestData("lajistore-leguserid.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals("[Lahti, Tapani]", g.getTeam().toString());
		assertEquals("[vanhahatikka:tapani]", g.getObserverUserIds().toString());
	}

	@Test
	public void testGatheringEventTaxonCensus() throws Exception {
		JSONObject json = getTestData("lajistore-taxon-census.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals(5, d.getGatherings().size());

		String expected = "[TaxonCensus [taxonId=MX.37580, type=MY.taxonCensusTypeCounted], TaxonCensus [taxonId=MX.37612, type=MY.taxonCensusTypeCounted]]";
		for (Gathering g : d.getGatherings()) {
			assertEquals(expected, g.getTaxonCensus().toString());
		}
	}

	@Test
	public void gridCoordinateParsing() throws DataValidationException {
		assertEquals("666.0 : 667.0 : 333.0 : 334.0 : YKJ : null", LajistoreHarmonizer.parseYkjOrEurefGrid("666:333").toString());
		assertEquals("6840699.0 : 6840700.0 : 208546.0 : 208547.0 : EUREF : null", LajistoreHarmonizer.parseYkjOrEurefGrid("6840699:208546").toString());
		assertEquals("7047000.0 : 7048000.0 : 511000.0 : 512000.0 : EUREF : null", LajistoreHarmonizer.parseYkjOrEurefGrid("7047:8511").toString());
		assertEquals("6667000.0 : 6668000.0 : 333000.0 : 334000.0 : EUREF : null", LajistoreHarmonizer.parseYkjOrEurefGrid("6667:333").toString());
		assertEquals("6932.0 : 6934.0 : 3412.0 : 3413.0 : YKJ : null", LajistoreHarmonizer.parseYkjOrEurefGrid("6932-3:3412").toString());
	}

	@Test
	public void invasiveControlForm() throws Exception {
		JSONObject json = new JSONObject(RdfXmlHarmonizerTests.getTestData("invasive-control.json"));
		List<DwRoot> roots = new LajistoreHarmonizer(CONTEXT_DEFINITIONS, null).harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document doc = root.getPrivateDocument();

		assertEquals(Const.INVASIVE_ALIEN_SPECIES_CONTROL_COLLECTION_ID, doc.getCollectionId());
		assertEquals("http://tun.fi/MNP.28836", doc.getNamedPlaceId());

		assertEquals(2, doc.getGatherings().size());
		Gathering unitGathering = doc.getGatherings().get(1);
		Gathering topGathering = doc.getGatherings().get(0);
		assertEquals(1, unitGathering.getUnits().size());
		assertEquals(0, topGathering.getUnits().size());

		assertEquals("2016-10-01", unitGathering.getDisplayDateTime());
		assertEquals(topGathering.getDisplayDateTime(), unitGathering.getDisplayDateTime());
		assertEquals("[POLYGON ((24.954371 60.199384, 24.958556 60.199092, 24.958313 60.198606, 24.956422 60.198556, 24.954192 60.198543, 24.954371 60.199384)), POLYGON ((24.960492 60.199176, 24.960492 60.200117, 24.961981 60.200117, 24.961981 60.199176, 24.960492 60.199176))]",
				unitGathering.getGeo().getWKTList().toString()); // location of controlled areas
		assertEquals("[POLYGON ((24.96052 60.197593, 24.958377 60.198605, 24.955058 60.1985, 24.954004 60.19856, 24.950706 60.198835, 24.949058 60.199692, 24.952169 60.200665, 24.954872 60.201175, 24.959042 60.201316, 24.961619 60.20104, 24.961785 60.200487, 24.962052 60.200415, 24.961638 60.19932, 24.961696 60.197644, 24.96052 60.197593))]",
				topGathering.getGeo().getWKTList().toString()); // the named place area (not used for anything..?)

		Unit u = unitGathering.getUnits().get(0);
		assertEquals("jättipalsami", u.getTaxonVerbatim());
		assertEquals("["+Tag.INVASIVE_PARTIAL+"]", u.getSourceTags().toString());

		assertEquals("http://tun.fi/MY.invasiveControlEffectivenessPartial", topGathering.factValue("http://tun.fi/MY.invasiveControlEffectiveness").toString());
		assertEquals("true", topGathering.factValue("http://tun.fi/MY.invasiveControlOpen").toString());
	}

	@Test
	public void testInvalidDate() throws Exception {
		JSONObject json = getTestData("lajistore-10-invalid-date.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);

		assertEquals(null, g.getEventDate());
		assertEquals("Quality [issue=INVALID_DATE, source=AUTOMATED_FINBIF_VALIDATION, message=Unparsable date \"2003-06-38/2003-07-02\". Cause: Unparseable date: \"2003-06-38\" Interpreted date format: D/D]", g.getQuality().getTimeIssue().toString());
	}

	@Test
	public void testSecondaryDataUpload() throws Exception {
		JSONObject json = getTestData("lajistore-secondary-data.json");
		List<DwRoot> roots = new LajistoreSecondaryDataHarmonizer(CONTEXT_DEFINITIONS).harmonize(json, new Qname("KE.981"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertFalse(root.isDeleteRequest());
		assertEquals(null, root.getPrivateDocument());
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(1, g.getUnits().size());
		Unit u = g.getUnits().get(0);

		assertEquals("HR.128/T.123", d.getDocumentId().toString());
		assertEquals("HR.128/T.123#G", g.getGatheringId().toString());
		assertEquals("HR.128/T.123#U", u.getUnitId().toString());
	}

	@Test
	public void testSecondaryDataUpload_deletions() throws Exception {
		JSONObject json = getTestData("lajistore-secondary-data-deletions.json");
		List<DwRoot> roots = new LajistoreSecondaryDataHarmonizer(CONTEXT_DEFINITIONS).harmonize(json, new Qname("KE.981"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(null, root.getPublicDocument());
		assertEquals(null, root.getPrivateDocument());

		assertEquals("HR.128/T.123", root.getDocumentId().toString());
		assertTrue(root.isDeleteRequest());
	}

	@Test
	public void testComplexUnitGatheringGeometry() throws Exception {
		JSONObject json = getTestData("lajistore-11-unitgathering-complex-geometry.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals(2, root.getPublicDocument().getGatherings().size());
		Gathering g1 = root.getPublicDocument().getGatherings().get(0);
		Gathering g2 = root.getPublicDocument().getGatherings().get(1);
		assertEquals("[POINT (28.608398 64.143842)]", g1.getGeo().getWKTList().toString());
		assertEquals("" +
				"[LINESTRING (28.60828 64.143973, 28.608661 64.144081, 28.609182 64.14401), "+
				"POLYGON ((28.608651 64.143936, 28.608651 64.144034, 28.608795 64.144034, 28.608795 64.143936, 28.608651 64.143936))]",
				g2.getGeo().getWKTList().toString());
	}

	@Test
	public void testCompleteLists() throws Exception {
		JSONObject json = getTestData("lajistore-complete-lists.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals("MX.37580", Qname.fromURI(d.getCompleteListTaxonId()).toString());
		assertEquals("MY.completeListTypeComplete", Qname.fromURI(d.getCompleteListType()).toString());
	}

	@Test
	public void concealWgs84() throws Exception {
		JSONObject json = getTestData("lajistore-conceal-wgs84.json");
		// Two points
		// 59.830037 23.138948
		// 59.827405 22.926201
		// secureLevel = KM10
		// But area is bigger than KM10 accuracy so concealed in KM25 accuracy

		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		Gathering g = d.getGatherings().get(0);
		assertEquals(1, d.getGatherings().size());
		assertEquals("6625.0 : 6650.0 : 3275.0 : 3300.0 : YKJ : 25000", g.getCoordinates().toString());
		assertEquals(null, g.getGeo());
	}

	@Test
	public void lolife() throws Exception {
		JSONObject json = getTestData("lajistore-lolife.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		Document d = roots.get(0).getPublicDocument();
		assertEquals(9, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(11, g.getUnits().size());

		int i = 0;
		assertLolife(g, i++, "Havainnot 1", RecordBasis.HUMAN_OBSERVATION_SEEN, "1", null, true, "[http://tun.fi/MY.lolifeNestTree : http://tun.fi/MX.38590]");
		assertLolife(g, i++, "Havainnot 2", RecordBasis.PRESERVED_SPECIMEN, "3", null, false, "[]");
		assertLolife(g, i++, "Jälkihavainnot 1", RecordBasis.HUMAN_OBSERVATION_INDIRECT, null, AbundanceUnit.INDIRECT_MARKS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeSnowTracks]");
		assertLolife(g, i++, "Jälkihavainnot ei", RecordBasis.HUMAN_OBSERVATION_INDIRECT, "0", AbundanceUnit.INDIRECT_MARKS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeNone]");
		assertLolife(g, i++, "Jälkihavainnot 2", RecordBasis.HUMAN_OBSERVATION_INDIRECT, null, AbundanceUnit.INDIRECT_MARKS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeUrine]");
		assertLolife(g, i++, "Jälkihavainnot 3", RecordBasis.HUMAN_OBSERVATION_INDIRECT, null, AbundanceUnit.INDIRECT_MARKS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeFeasting]");
		assertLolife(g, i++, "Jälkihavainnot 4", RecordBasis.HUMAN_OBSERVATION_INDIRECT, null, AbundanceUnit.INDIRECT_MARKS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeFoodStock]");
		assertLolife(g, i++, "Pesähavainnot 1", RecordBasis.HUMAN_OBSERVATION_INDIRECT, "22424", AbundanceUnit.NESTS, null, "[http://tun.fi/MY.nestType : http://tun.fi/MY.nestTypeTreeCavity, http://tun.fi/MY.nestNotes : a, http://tun.fi/MY.nestCount : 22424]");
		assertLolife(g, i++, "Pesähavainnot 2", RecordBasis.HUMAN_OBSERVATION_INDIRECT, "3", AbundanceUnit.NESTS, null, "[http://tun.fi/MY.nestType : http://tun.fi/MY.nestTypeBuilding, http://tun.fi/MY.nestNotes : b, http://tun.fi/MY.nestCount : 3]");
		assertLolife(g, i++, "Papanahavainnot 1", RecordBasis.HUMAN_OBSERVATION_INDIRECT, "1-10", AbundanceUnit.DROPPINGS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeFeces, http://tun.fi/MY.lolifeDroppingsCount : http://tun.fi/MY.lolifeDroppingsCount1, http://tun.fi/MY.lolifeDroppingsQuality : http://tun.fi/MY.lolifeDroppingsQuality1, http://tun.fi/MY.lolifeDroppingsType : http://tun.fi/MY.lolifeDroppingsTypeRock]");
		assertLolife(g, i++, "Papanahavainnot 2", RecordBasis.HUMAN_OBSERVATION_INDIRECT, "0", AbundanceUnit.DROPPINGS, null, "[http://tun.fi/MY.indirectObservationType : http://tun.fi/MY.indirectObservationTypeFeces, http://tun.fi/MY.lolifeDroppingsCount : http://tun.fi/MY.lolifeDroppingsCount0, http://tun.fi/MY.lolifeDroppingsQuality : http://tun.fi/MY.lolifeDroppingsQuality2, http://tun.fi/MY.lolifeDroppingsType : http://tun.fi/MY.lolifeDroppingsTypeOther]");
	}

	private void assertLolife(Gathering g, int i, String notes, RecordBasis recordBasis, String abunance, AbundanceUnit abundanceUnit, Boolean alive, String facts) {
		Unit u = g.getUnits().get(i);
		assertEquals(notes, u.getNotes());
		assertEquals(facts, u.getFacts().toString());
		assertEquals(recordBasis, u.getRecordBasis());
		assertEquals(abunance, u.getAbundanceString());
		assertEquals(abundanceUnit, u.getAbundanceUnit());
		assertEquals(alive, u.isAlive());
		assertEquals(new Qname("MX.48243"), u.getReportedTaxonId());
	}

	@Test
	public void ykj_verbatim_with_multigeo() throws Exception {
		JSONObject json = getTestData("lajistore-multigeo_ykj_verbatim.json");
		// Three geometries
		// Smaller polygon (inside 10KM grid)
		// "polygon" that is actually a point (inside 10KM grid)
		// polygon of 10KM ykj grid with YKJ verbatim 671:342

		// We want to use ykj verbatim instead of the geometry when converting coordinates, etc

		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		Gathering g = d.getGatherings().get(0);
		assertEquals(1, d.getGatherings().size());

		assertEquals(null, g.getQuality());
		assertEquals(null, g.getCoordinates());
		assertEquals("671:342", g.getCoordinatesVerbatim());
		assertEquals("["+
				"POLYGON ((3427670 6714906, 3427735 6716358, 3429971 6716316, 3429988 6716021, 3429972 6715699, 3429977 6715393, 3429993 6715059, 3428988 6714770, 3427670 6714906)), " +
				"POINT (3429298 6715715), " +
				"POLYGON ((3420000 6710000, 3420000 6720000, 3430000 6720000, 3430000 6710000, 3420000 6710000))]",
				g.getGeo().getWKTList().toString());
	}

	@Test
	public void geom_coll_inside_geom_coll() throws Exception {
		JSONObject json = getTestData("lajistore-geometrycol.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals(2, d.getGatherings().size());

		Gathering g1 = d.getGatherings().get(0);
		Gathering g2 = d.getGatherings().get(1);

		assertEquals(null, g1.getQuality());
		assertEquals(null, g2.getQuality());

		assertEquals(""+
				"[POLYGON ((23.072711 60.849176, 23.072824 60.849119, 23.072723 60.849076, 23.07282 60.849036, 23.072638 60.848945, 23.072429 60.849038, 23.072711 60.849176))]",
				g1.getGeo().getWKTList().toString());

		assertEquals(""+
				"[POLYGON ((23.072687 60.849072, 23.07262 60.849092, 23.072653 60.84912, 23.072716 60.849093, 23.072687 60.849072))]",
				g2.getGeo().getWKTList().toString());
	}

	@Test
	public void document_identifications() throws Exception {
		JSONObject json = getTestData("lajistore-12-document-identifications.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		Document d = root.getPublicDocument();
		assertEquals(1, d.getGatherings().size());
		Gathering g = d.getGatherings().get(0);
		assertEquals(4, g.getUnits().size());

		assertEquals("[http://tun.fi/MY.dataOrigin : http://tun.fi/MY.dataOriginSpreadsheetFile]", d.getFacts().toString());

		assertEquals("Sauli Sitruuna",
				g.getUnits().get(0).getDet());
		assertEquals("[http://tun.fi/MY.detDate : 2024]",
				g.getUnits().get(0).getFacts().toString());

		assertEquals("Daniel Dokumentti",
				g.getUnits().get(1).getDet());
		assertEquals("[http://tun.fi/MY.detDate : 2024-12-05]",
				g.getUnits().get(1).getFacts().toString());

		assertEquals("Daniel Dokumentti",
				g.getUnits().get(2).getDet());
		assertEquals("[http://tun.fi/MY.detDate : 2024-12-05]",
				g.getUnits().get(2).getFacts().toString());

		assertEquals("Testi Määrittäjä",
				g.getUnits().get(3).getDet());
		assertEquals("[http://tun.fi/MY.detDate : 2024-12-05]",
				g.getUnits().get(3).getFacts().toString());
	}

	@Test
	public void contacts() throws Exception {
		JSONObject json = getTestData("lajistore-contacts.json");
		List<DwRoot> roots = harmonizer.harmonize(json, new Qname("KE.1"));
		assertEquals(1, roots.size());
		DwRoot root = roots.get(0);
		assertEquals("[]", root.getPublicDocument().getFacts().toString());
		assertEquals("[http://tun.fi/MA.emailAddress : testi@testiposti.fi, http://tun.fi/MA.inheritedName : Jokunen, http://tun.fi/MA.preferredName : Joku, http://tun.fi/MZ.city : Helsinki, http://tun.fi/MZ.phoneNumber : 044 4444444, http://tun.fi/MZ.postalCode : 00100, http://tun.fi/MZ.streetAddress : Jokutie 13]",
				root.getPrivateDocument().getFacts().toString());
	}

}