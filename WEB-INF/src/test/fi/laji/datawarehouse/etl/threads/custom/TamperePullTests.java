package fi.laji.datawarehouse.etl.threads.custom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.StatelessSession;
import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.DAOImpleTests;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.models.containers.InPipeData;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;

public class TamperePullTests {

	// NOTE: For this test to work set TamperePull_URL to https://bitbucket.org/luomus/laji-etl/raw/HEAD/WEB-INF/src/test/fi/laji/datawarehouse/etl/threads/custom
	// Username/password/driver can be set to "foo" or something else

	private List<String> errors;
	private ErrorReporter errorReporter;
	private DAO dao = new DAOImpleTests.AlmostRealDao(TestConfig.getConfig(), TamperePullTests.class);
	private List<String> statuses;
	private ThreadStatusReporter reporter;

	private class OurThreadStatusReporter extends ThreadStatusReporter {
		@Override
		public void setStatus(String status) {
			super.setStatus(status);
			statuses.add(status);
		}
	}

	@Before
	public void before() {
		errors = new ArrayList<>();
		errorReporter = new ErrorReporter() {

			@Override
			public void report(String message, Throwable condition) {
				errors.add(message + ": " + LogUtils.buildStackTrace(condition));
			}

			@Override
			public void report(String message) {
				errors.add(message);
			}

			@Override
			public void report(Throwable condition) {
				errors.add(LogUtils.buildStackTrace(condition));
			}
		};

		statuses = new ArrayList<>();
		reporter = new OurThreadStatusReporter();

		try (StatelessSession session = dao.getETLDAO().getETLEntityConnection()) {
			session.getTransaction().begin();
			session.createSQLQuery("DELETE FROM in_pipe_data WHERE source = '" + TamperePullReader.STAGING_SOURCE + "'").executeUpdate();
			session.createSQLQuery("DELETE FROM in_pipe WHERE source = '" + TamperePullReader.STAGING_SOURCE + "'").executeUpdate();
			session.createSQLQuery("DELETE FROM tampere_etl_previous").executeUpdate();
			session.getTransaction().commit();
		}

	}

	@Test
	public void reading_tampere_json_streams_from_test_repo() throws Exception {
		try (TamperePullReader reader = init()) {
			reader.start();
			try {
				reader.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				fail();
			}
		}
		assertEquals("[]", errors.toString());

		String expected = getTestData("tampere_expected.txt");
		assertEquals(expected, statuses.stream().collect(Collectors.joining("\n")));

		List<InPipeData> data = dao.getETLDAO().getUnprocessedNotErroneousInOrderFromInPipe(TamperePullReader.STAGING_SOURCE);
		assertEquals(6, data.size());
		DwRoot root = DwRoot.fromJson(new JSONObject(data.get(0).getData()));
		assertEquals("HR.4391/3599", root.getDocumentId().toString());
		assertEquals("HR.4391", root.getCollectionId().toString());
	}

	private TamperePullReader init() {
		return new TamperePullReader(TestConfig.getConfig(), dao, errorReporter, new ThreadHandler(dao, null, new ThreadStatuses()), reporter);
	}

	public static String getTestData(String filename) throws Exception {
		URL url = TamperePullTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
