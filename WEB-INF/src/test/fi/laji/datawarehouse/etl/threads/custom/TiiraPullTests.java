package fi.laji.datawarehouse.etl.threads.custom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.dao.DAO;
import fi.laji.datawarehouse.dao.DAOImpleTests;
import fi.laji.datawarehouse.etl.models.TestConfig;
import fi.laji.datawarehouse.etl.threads.ThreadHandler;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.laji.datawarehouse.etl.utils.ThreadStatuses;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;

public class TiiraPullTests {

	// NOTE: For this test to work set TiiraPull_URL to https://bitbucket.org/luomus/laji-etl/raw/HEAD/WEB-INF/src/test/fi/laji/datawarehouse/etl/threads/custom/tiira_atlas_testdata.csv
	// Username/password/driver can be set to "foo" or something else

	private List<String> errors;
	private ErrorReporter errorReporter;
	private DAO dao = new DAOImpleTests.AlmostRealDao(TestConfig.getConfig(), TiiraPullTests.class);
	private List<String> statuses;
	private ThreadStatusReporter reporter;

	private class OurThreadStatusReporter extends ThreadStatusReporter {
		@Override
		public void setStatus(String status) {
			super.setStatus(status);
			statuses.add(status);
		}
	}

	@Before
	public void before() {
		errors = new ArrayList<>();
		errorReporter = new ErrorReporter() {

			@Override
			public void report(String message, Throwable condition) {
				errors.add(message + ": " + LogUtils.buildStackTrace(condition));
			}

			@Override
			public void report(String message) {
				errors.add(message);
			}

			@Override
			public void report(Throwable condition) {
				errors.add(LogUtils.buildStackTrace(condition));
			}
		};

		statuses = new ArrayList<>();
		reporter = new OurThreadStatusReporter();
	}

	@Test
	public void reading_bird_atlas_from_test_data_repo() throws Exception {
		try (TiiraAtlasPullReader reader = init()) {
			reader.start();
			try {
				reader.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				fail();
			}
		}
		assertEquals("[]", errors.toString());

		String expected = getTestData("tiira_expected.txt");
		assertEquals(expected, statuses.stream().collect(Collectors.joining("\n")));
		// Note that this test assumes there are no changes.. If parsing logic is changed, then there will be one update and test will pass on second run
		// Not perfect by any means but at least there is some kind of test
	}

	private TiiraAtlasPullReader init() {
		return new TiiraAtlasPullReader(TestConfig.getConfig(), dao, errorReporter, new ThreadHandler(dao, null, new ThreadStatuses()), reporter);
	}

	public static String getTestData(String filename) throws Exception {
		URL url = TiiraPullTests.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
