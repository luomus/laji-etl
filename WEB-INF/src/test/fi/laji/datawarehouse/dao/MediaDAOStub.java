package fi.laji.datawarehouse.dao;

import java.util.Collections;
import java.util.List;

import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.luomus.commons.containers.rdf.Qname;

public class MediaDAOStub implements MediaDAO {

	@Override
	public MediaObject getMediaObject(Qname id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<MediaObject> getMediaObjects(Qname documentId) {
		// Auto-generated method stub
		return Collections.emptyList();
	}

}
