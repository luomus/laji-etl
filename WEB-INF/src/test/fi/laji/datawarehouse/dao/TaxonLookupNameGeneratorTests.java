package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.Test;

import fi.laji.datawarehouse.dao.LookupNameProvider.LookupName;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.java.tests.commons.taxonomy.TaxonContainerStub;

public class TaxonLookupNameGeneratorTests {

	private static final Qname SUBGENUS = new Qname("MX.subgenus");
	private static final Qname GENUS = new Qname("MX.genus");
	private static final Qname SPECIES = new Qname("MX.species");
	private static final Qname SUBSPECIES = new Qname("MX.subspecies");
	private final TaxonLookupNameGenerator generator = new TaxonLookupNameGenerator();
	private final TaxonContainer taxonContainer = new TaxonContainerStub() {
		@Override
		public boolean hasTaxon(Qname taxonId) {
			return true;
		}
		@Override
		public Taxon getTaxon(Qname taxonId) {
			if (subgenusTaxon.getQname().equals(taxonId)) return subgenusTaxon;
			if (subspeciesTaxon.getQname().equals(taxonId)) return subspeciesTaxon;
			Taxon t = new Taxon(taxonId, this);
			t.setScientificName("Synonymious synomius");
			t.setScientificNameAuthorship("Author");
			return t;
		}
	};

	@Test
	public void test_just_id() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setChecklist(Const.MASTER_CHECKLIST_QNAME);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:MR.1, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:MR.1:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:MR.1:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:MR.1, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:MR.1:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:MR.1:null:null, id=MX.1, type=PRIMARY, isOverriding=true]";
		assertEquals(expected, actual(lookupNames));
	}

	private String actual(List<LookupName> lookupNames) {
		return lookupNames.stream().map(n->n.toString()).collect(Collectors.joining("\n"));
	}

	@Test
	public void trashChars() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.addVernacularName("fi", "-"); // this kind of name does not cause any more matches than lookupnames with just id
		generator.generateLookupNames(taxon, lookupNames);
		assertEquals(6, lookupNames.size());

	}

	@Test
	public void test_scientificname_author() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Psallus laticeps");
		taxon.setScientificNameAuthorship("Linnea");
		taxon.setTaxonRank(SPECIES);
		taxon.setFinnish(true);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +

				"LookupName [name=psalluslaticeps:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // Psallus laticeps
				"LookupName [name=psalluslaticeps:null:MX.1:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticeps:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticeps:null:null:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticeps:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=psalluslaticepslinnea:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // Psallus laticeps (Linnea)
				"LookupName [name=psalluslaticepslinnea:null:MX.1:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnea:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnea:null:null:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnea:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]";
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void test_species_scientificname_vernacularname_author() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Psallus laticeps");
		taxon.setScientificNameAuthorship("Linnea");
		taxon.getVernacularName().set("fi", "lude");
		taxon.setTaxonRank(SPECIES);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +

				"LookupName [name=psalluslaticeps:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticeps:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticeps:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=psalluslaticepslinnea:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnea:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnea:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=lude:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=lude:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=lude:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=ludepsalluslaticeps:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // lude - Psallus laticeps
				"LookupName [name=ludepsalluslaticeps:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=ludepsalluslaticeps:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=psalluslaticepslude:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // Psallus laticeps (lude)
				"LookupName [name=psalluslaticepslude:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslude:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=ludepsalluslaticepslinnea:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // lude (Psallus laticeps, Linnea)
				"LookupName [name=ludepsalluslaticepslinnea:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=ludepsalluslaticepslinnea:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=psalluslaticepslinnealude:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" + // Psallus laticeps, Linnea (lude)
				"LookupName [name=psalluslaticepslinnealude:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psalluslaticepslinnealude:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]";
		// Note these do not match:
		// Psallus laticeps L.
		// Psallus laticeps, Linnea 1770
		// If such names exist, AKA names can be added case by case
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void test_species_alternative_name() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.getAlternativeVernacularNames().add("fi", "lude");
		taxon.setTaxonRank(SPECIES);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +

				"LookupName [name=lude:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=lude:null:MX.1:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=lude:null:null:null, id=MX.1, type=SECONDARY, isOverriding=false]";
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void test_missapplied() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setTaxonRank(SPECIES);
		taxon.setScientificName("Actualis actualis");
		taxon.setChecklist(Const.MASTER_CHECKLIST_QNAME);

		taxon.addSynonyms().addMisappliedName(new Qname("MX.666"));

		generator.generateLookupNames(taxon, lookupNames);

		Set<String> names = lookupNames.stream().map(l->l.toString()).collect(Collectors.toSet());
		assertTrue(names.contains("LookupName [name=synonymioussynomius:MR.1:null:COUNTRY-FI, id=MX.1, type=SECONDARY, isOverriding=false]"));
		assertTrue(names.contains("LookupName [name=synonymioussynomius:null:null:COUNTRY-FI, id=MX.1, type=SECONDARY, isOverriding=false]"));
	}

	@Test
	public void test_higher_taxa_scientificname() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Psallus");
		taxon.setTaxonRank(GENUS);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"http://tunfi/mx1->MX.1\n" +
				"mx1->MX.1\n" +
				"psallus->MX.1\n" +
				"psallussp->MX.1\n" +
				"psallusspp->MX.1";
		assertEquals(expected, names(lookupNames));
	}

	@Test
	public void test_higher_taxa_vernacular() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Psallus");
		taxon.getVernacularName().set("fi", "suomuluteet");
		taxon.getAlternativeVernacularNames().add("fi", "suomuiset luteet");
		taxon.setTaxonRank(GENUS);
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=psallus:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallus:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallus:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallusspp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallusspp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallusspp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteet:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteet:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteet:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteetpsallus:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteetpsallus:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuluteetpsallus:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuluteet:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuluteet:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuluteet:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteet:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteet:null:MX.1:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteet:null:null:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteetpsallus:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteetpsallus:null:MX.1:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=suomuisetluteetpsallus:null:null:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuisetluteet:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuisetluteet:null:MX.1:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=psallussuomuisetluteet:null:null:null, id=MX.1, type=SECONDARY, isOverriding=false]";
		assertEquals(expected, actual(lookupNames));
	}

	private Taxon subgenusTaxon = createSubgenusTaxon();
	private Taxon subspeciesTaxon = createSubSpeciesTaxon();

	private Taxon createSubgenusTaxon() {
		Taxon t = new Taxon(new Qname("MX.12345"), taxonContainer);
		t.setScientificName("Psallus (Pityopsallus)");
		t.setTaxonRank(SUBGENUS);
		return t;
	}

	private Taxon createSubSpeciesTaxon() {
		Taxon t = new Taxon(new Qname("MX.67890"), taxonContainer);
		t.setScientificName("Ovis orientalis musimon");
		t.setTaxonRank(SUBSPECIES);
		return t;
	}

	@Test
	public void test_subgenus() {
		List<LookupName> lookupNames = new ArrayList<>();

		Taxon taxon1 = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon1.setScientificName("Psallus");
		taxon1.setTaxonRank(GENUS);

		Taxon taxon3 = new Taxon(new Qname("MX.3"), taxonContainer);
		taxon3.setScientificName("Psallus (Pityopsallus) laticeps");
		taxon3.setTaxonRank(SPECIES);
		taxon3.setParentQname(subgenusTaxon.getQname());

		Taxon taxon4 = new Taxon(new Qname("MX.4"), taxonContainer);
		taxon4.setScientificName("Psallus effirent");
		taxon4.setTaxonRank(SPECIES);
		taxon4.setParentQname(subgenusTaxon.getQname());

		Taxon taxon5 = new Taxon(new Qname("MX.5"), taxonContainer);
		taxon5.setScientificName("Psallus effirent (grey form)");
		taxon5.setTaxonRank(new Qname("MX.form"));
		taxon5.setParentQname(taxon4.getQname());

		generator.generateLookupNames(taxon1, lookupNames);
		generator.generateLookupNames(subgenusTaxon, lookupNames);
		generator.generateLookupNames(taxon3, lookupNames);
		generator.generateLookupNames(taxon4, lookupNames);
		generator.generateLookupNames(taxon5, lookupNames);

		Map<String, List<String>> taxonNames = namesById(lookupNames);

		assertEquals(66, lookupNames.size());

		List<String> n1 = taxonNames.get("MX.1");
		List<String> n2 = taxonNames.get(subgenusTaxon.getQname().toString());
		List<String> n3 = taxonNames.get("MX.3");
		List<String> n4 = taxonNames.get("MX.4");
		assertEquals(15, n1.size());
		assertEquals(15, n2.size());
		assertEquals(12, n3.size());
		assertEquals(15, n4.size());

		String expected = "" +
				"http://tunfi/mx1->MX.1\n" +
				"http://tunfi/mx12345->MX.12345\n" +
				"http://tunfi/mx3->MX.3\n" +
				"http://tunfi/mx4->MX.4\n" +
				"http://tunfi/mx5->MX.5\n" +
				"mx1->MX.1\n" +
				"mx12345->MX.12345\n" +
				"mx3->MX.3\n" +
				"mx4->MX.4\n" +
				"mx5->MX.5\n" +
				"psallus->MX.1\n" +
				"psalluseffirent->MX.4\n" +
				"psalluseffirentgreyform->MX.5\n" +
				"psalluslaticeps->MX.3\n" +
				"psalluspityopsallus->MX.12345\n" +
				"psalluspityopsalluseffirent->MX.4\n" +
				"psalluspityopsalluslaticeps->MX.3\n" +
				"psalluspityopsallussp->MX.12345\n" +
				"psalluspityopsallusspp->MX.12345\n" +
				"psallussp->MX.1\n" +
				"psallusspp->MX.1";
		assertEquals(expected, names(lookupNames));
	}

	@Test
	public void test_subspecies() {
		List<LookupName> lookupNames = new ArrayList<>();

		generator.generateLookupNames(subspeciesTaxon, lookupNames);

		assertEquals(12, lookupNames.size());

		String expected = "" +
				"http://tunfi/mx67890->MX.67890\n" +
				"mx67890->MX.67890\n" +
				"ovisorientalismusimon->MX.67890\n" +
				"ovisorientalissubspmusimon->MX.67890"; // added "subsp" to the name "ovis orientalis musimon"
		assertEquals(expected, names(lookupNames));
	}

	private Map<String, List<String>> namesById(List<LookupName> lookupNames) {
		Map<String, List<String>> taxonnames = new LinkedHashMap<>();
		for (LookupName name : lookupNames) {
			if (!taxonnames.containsKey(name.getId().toString())) {
				taxonnames.put(name.getId().toString(), new ArrayList<String>());
			}
			taxonnames.get(name.getId().toString()).add(name.toString());
		}
		return taxonnames;
	}

	@Test
	public void formatting() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.getVernacularName().set("fi", "käki (harmaa värimuoto)");
		taxon.setTaxonRank(new Qname("MX.form"));
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"http://tunfi/mx1->MX.1\n" +
				"käkiharmaavärimuoto->MX.1\n" +
				"mx1->MX.1";
		assertEquals(expected, names(lookupNames));
	}

	@Test
	public void synonyms() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Aaa bbb");
		taxon.setTaxonRank(new Qname("MX.species"));
		taxon.setScientificNameAuthorship("Linnea");
		taxon.addSynonyms().addBasionym(new Qname("MX.666"));
		taxon.getVernacularName().set("fi", "lude");
		generator.generateLookupNames(taxon, lookupNames);
		assertEquals(63, lookupNames.size());
		String expected = "" +
				"aaabbb->MX.1\n" +
				"aaabbblinnea->MX.1\n" +
				"aaabbblinnealude->MX.1\n" +
				"aaabbblude->MX.1\n" +
				"http://tunfi/mx1->MX.1\n" +
				"http://tunfi/mx666->MX.1\n" +
				"lude->MX.1\n" +
				"ludeaaabbb->MX.1\n" +
				"ludeaaabbblinnea->MX.1\n" +
				"ludesynonymioussynomius->MX.1\n" +
				"ludesynonymioussynomiusauthor->MX.1\n" +
				"mx1->MX.1\n" +
				"mx666->MX.1\n" +
				"synonymioussynomius->MX.1\n" +
				"synonymioussynomiusauthor->MX.1\n" +
				"synonymioussynomiusauthorlude->MX.1\n" +
				"synonymioussynomiuslude->MX.1\n" +
				"synonymioussynomiussp->MX.1\n" +
				"synonymioussynomiusspauthor->MX.1\n" +
				"synonymioussynomiusspp->MX.1\n" +
				"synonymioussynomiussppauthor->MX.1";
		assertEquals(expected, names(lookupNames));
	}

	private String names(List<LookupName> lookupNames) {
		Set<String> names = lookupNames.stream().map(n->n.getName().replace("http:", "http").split(Pattern.quote(":"))[0]+"->"+n.getId()).collect(Collectors.toSet());
		List<String> sorted = new ArrayList<>(names);
		Collections.sort(sorted);
		return sorted.stream().map(s->s.replace("http//", "http://")).collect(Collectors.joining("\n"));
	}

	@Test
	public void some_stange_case() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Lampetra fluviatilis, landlocked populations");
		taxon.setTaxonRank(new Qname("MX.form"));
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"http://tunfi/mx1->MX.1\n" +
				"lampetrafluviatilislandlockedpopulations->MX.1\n" +
				"mx1->MX.1";
		assertEquals(expected, names(lookupNames));

	}

	@Test
	public void test_combinations() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Aaa bbb ssp. ccc ×ddd var. eee");
		taxon.setTaxonRank(new Qname("MX.hybrid"));
		taxon.setScientificNameAuthorship("Linnea");
		generator.generateLookupNames(taxon, lookupNames);

		assertEquals(30, lookupNames.size());

		taxon.setChecklist(new Qname("MR.1"));
		lookupNames.clear();
		generator.generateLookupNames(taxon, lookupNames);
		assertEquals(60, lookupNames.size());

		taxon.getVernacularName().set("fi", "lude");
		lookupNames.clear();
		generator.generateLookupNames(taxon, lookupNames);
		assertEquals(90, lookupNames.size());

		taxon.getAlternativeVernacularNames().add("sv", "låde");
		lookupNames.clear();
		generator.generateLookupNames(taxon, lookupNames);
		assertEquals(120, lookupNames.size());
	}

	@Test
	public void overridingNames() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Putte");
		taxon.setTaxonRank(new Qname("MX.genus"));
		taxon.addOverridingTargetName("Putte possuius");

		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=putte:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=putte:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=putte:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttesp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttesp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttesp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttespp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttespp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttespp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttepossuius:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=puttepossuius:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=puttepossuius:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]";
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void overridingNames_2() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Putte possu");
		taxon.setTaxonRank(new Qname("MX.species"));
		taxon.setBirdlifeCode("PUTPOS");

		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +

				"LookupName [name=puttepossu:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttepossu:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=puttepossu:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +

				"LookupName [name=putpos:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=putpos:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=putpos:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]";
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void akaname() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxon = new Taxon(new Qname("MX.1"), taxonContainer);
		taxon.setScientificName("Muridae");
		taxon.setTaxonRank(new Qname("MX.family"));
		taxon.addAlsoKnownAsName("hiiri, myyrä");
		generator.generateLookupNames(taxon, lookupNames);
		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=muridae:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridae:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridae:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaesp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaesp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaesp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaespp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaespp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=muridaespp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=hiirimyyrä:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=hiirimyyrä:null:MX.1:null, id=MX.1, type=SECONDARY, isOverriding=false]\n" +
				"LookupName [name=hiirimyyrä:null:null:null, id=MX.1, type=SECONDARY, isOverriding=false]";
		assertEquals(expected, actual(lookupNames));
	}

	@Test
	public void conflicting_names() {
		List<LookupName> lookupNames = new ArrayList<>();
		Taxon taxonFi = new Taxon(new Qname("MX.1"), taxonContainer);
		taxonFi.setScientificName("Parus");  // conflicting name
		taxonFi.setTaxonRank(new Qname("MX.genus"));
		taxonFi.setFinnish(true);

		Taxon taxonForeign = new Taxon(new Qname("MX.2"), taxonContainer);
		taxonForeign.setScientificName("Parus");  // conflicting name
		taxonForeign.setTaxonRank(new Qname("MX.genus"));
		taxonForeign.setFinnish(false);

		generator.generateLookupNames(taxonFi, lookupNames);
		generator.generateLookupNames(taxonForeign, lookupNames);

		String expected = "" +
				"LookupName [name=mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx1:null:null:null, id=MX.1, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=parus:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parus:null:MX.1:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +  // only MX.1 has lookup from country=fi so if the observation is from Finland, MX.1 will not confict with MX.2
				"LookupName [name=parus:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parus:null:null:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parus:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:MX.1:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:null:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:MX.1:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:MX.1:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:null:COUNTRY-FI, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:null:null, id=MX.1, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=mx2:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx2:null:MX.2:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=mx2:null:null:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx2:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx2:null:MX.2:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=http://tunfi/mx2:null:null:null, id=MX.2, type=PRIMARY, isOverriding=true]\n" +
				"LookupName [name=parus:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parus:null:MX.2:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parus:null:null:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:MX.2:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parussp:null:null:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:MX.2:null, id=MX.2, type=PRIMARY, isOverriding=false]\n" +
				"LookupName [name=parusspp:null:null:null, id=MX.2, type=PRIMARY, isOverriding=false]";
		assertEquals(expected, actual(lookupNames));
	}

}
