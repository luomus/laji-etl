package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader;
import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader.SykeZoobenthosAPI;
import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader.SykeZoobenthosService;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;

public class SykeZoobethosServiceTests {

	private static class SykeZoobenthosAPIMock implements SykeZoobenthosAPI {

		@Override
		public void close() {}

		@Override
		public JSONObject getEvent(String eventId) throws SykeZoobenthosAPIException {
			if (eventId.equals("deleted")) return null;
			return new JSONObject().setString("eventID", eventId);
		}

		@Override
		public List<JSONObject> getChildEvents(String eventId) throws SykeZoobenthosAPIException {
			List<JSONObject> ret = new ArrayList<>();
			if (eventId.equals("1")) {
				ret.add(new JSONObject().setString("eventID", "2"));
				ret.add(new JSONObject().setString("eventID", "3"));
			}
			if (eventId.equals("2")) {
				ret.add(new JSONObject().setString("eventID", "4"));
			}
			return ret;
		}

		@Override
		public String getParentEventId(String eventId) throws SykeZoobenthosAPIException {
			if (eventId.equals("6")) return "7";
			if (eventId.equals("7")) return "8";
			return null;
		}

		@Override
		public List<List<String>> get(String endpoint, int timestamp, String... select) throws SykeZoobenthosAPIException {
			if (timestamp == 0 && !endpoint.endsWith("Event")) Assert.fail("Initially need to load only events");
			List<List<String>> ret = new ArrayList<>();
			if (endpoint.equals("Occurrence")) {
				ret.add(Utils.list("4"));
				ret.add(Utils.list("6"));
			} 
			if (endpoint.equals("Event")) {
				ret.add(Utils.list("1", null));
				ret.add(Utils.list("2", "1"));
				ret.add(Utils.list("3", "1"));
				ret.add(Utils.list("4", "2"));
				ret.add(Utils.list("5", null));
			}
			return ret;
		}

	}

	SykeZoobenthosService service;

	@Before
	public void before() {
		service = new SykeZoobenthosService(new SykeZoobenthosAPIMock());	
	}

	@Test
	public void testModifiedRootEventIds() {
		assertEquals("[1, 5]", new TreeSet<>( service.getModifiedRootEventIds(0) ).toString());
		assertEquals("[1, 5, 8]", new TreeSet<>( service.getModifiedRootEventIds(1) ).toString());
	}

	@Test
	public void getDeletedEventTree() {
		JSONObject o = service.getEventTree("deleted");
		assertEquals("laji-etl", o.getString("schema"));
		assertEquals(new Qname(SykeZoobenthosPullReader.SOURCE.toString()+"/deleted").toURI(), o.getString("documentId"));
		assertEquals(true, o.getBoolean("deleteRequest"));
	}

	@Test
	public void getEventTree() {
		JSONObject o = service.getEventTree("1");
		String expected = "{\"eventID\":\"1\",\"Events\":[{\"eventID\":\"2\",\"Events\":[{\"eventID\":\"4\"}]},{\"eventID\":\"3\"}],\"schema\":\"syke-zoobenthos\"}";
		assertEquals(expected, o.toString());
	}

}
