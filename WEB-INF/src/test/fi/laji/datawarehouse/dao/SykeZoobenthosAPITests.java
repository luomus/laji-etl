package fi.laji.datawarehouse.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.etl.threads.custom.SykeZoobenthosPullReader.SykeZoobenthosAPIImple;
import fi.laji.datawarehouse.etl.utils.ThreadStatusReporter;
import fi.luomus.commons.json.JSONObject;

public class SykeZoobenthosAPITests {

	SykeZoobenthosAPIImple api;

	@Before
	public void before() {
		api = new SykeZoobenthosAPIImple(statusToSysout());
	}

	@After
	public void after() {
		api.close();
	}

	@Test
	public void testGetParentEventId() {
		assertEquals(null, api.getParentEventId("foo"));
		assertEquals(null, api.getParentEventId("Sampling_10051"));
		assertEquals("Sampling_10051", api.getParentEventId("Sampling_10051-Sample_15848"));
	}

	@Test
	public void getEvent() {
		assertEquals(null, api.getEvent("foo"));

		JSONObject o = api.getEvent("Sampling_10051");
		assertEquals("Sampling_10051", o.getString("eventID"));
		System.out.println(o.beautify());
		assertEquals(4855, o.getObject("BenthosStation").getInteger("benthosStationID"));
		assertEquals(0, o.getArray("Occurrences").size());
		assertEquals(1, o.getArray("Measurements").size());
	}

	@Test
	public void getChildEvents() {
		assertEquals(0, api.getChildEvents("foo").size());

		assertEquals(6,  api.getChildEvents("Sampling_10051").size());

		List<JSONObject> res = api.getChildEvents("Sampling_10051-Sample_15848");
		assertEquals(1, res.size());

		JSONObject o = res.get(0);
		System.out.println(o.beautify());
		assertEquals(7, o.getArray("Occurrences").size());
		assertEquals("Sampling_10051-Sample_15848-Sieve_05mm", o.getArray("Occurrences").iterateAsObject().get(0).getString("eventID"));
		// Sometimes this test fails: order from API is not always the same? Should order them in our imple?
		assertTrue(
				o.getArray("Occurrences").iterateAsObject().get(0).getString("occurrenceID"),
				o.getArray("Occurrences").iterateAsObject().get(0).getString("occurrenceID").startsWith("Sampling_10051-Sample_15848-Sieve_05mm-Taxon_"));
		assertEquals("tun.fi/MX.273359", o.getArray("Occurrences").iterateAsObject().get(0).getString("scientificNameMXID"));
	}

	@Test
	public void getModifiedEventIds() {
		assertEquals(0, api.get("Event", Integer.MAX_VALUE, "eventID", "parentEventID").size());

		List<List<String>> weekAgo = api.get("Event", weekAgo(), "eventID", "parentEventID");
		assertTrue(weekAgo.size() > 0); // Note this test uses real life production api and assumes that every week something is done in the db

		for (List<String> l : weekAgo) {
			assertTrue(l.get(0).length() > 0);
			assertTrue(l.get(1) == null || l.get(1).length() > 0);
		}
	}

	private int weekAgo() {
		return (int) LocalDate.now().minusDays(7).atStartOfDay(ZoneId.of("UTC")).toEpochSecond();
	}

	private ThreadStatusReporter statusToSysout() {
		return new ThreadStatusReporter() {
			@Override
			public void setStatus(String status) {
				System.out.println(status);
			}
		};
	}

}
