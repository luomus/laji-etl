package fi.laji.datawarehouse.dao.vertica;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import fi.laji.datawarehouse.query.model.Base;

public class VerticaDAOImpleSharedMappingsTests {

	VerticaDAOImpleSharedMappings mappings;

	@Before
	public void init() {
		mappings = new VerticaDAOImpleSharedMappings();
	}

	@Test
	public void test_dimesion_class() {
		assertNull(mappings.getDimensionClass("foo"));
		assertEquals(SecureLevelEntity.class, mappings.getDimensionClass("document.secureLevel"));
	}

	@Test
	public void test_mapping_documentid() {
		assertEquals("id", mappings.getEntityReference(Base.DOCUMENT, "document.documentId"));
		assertEquals("foreignKeys.documentId", mappings.getEntityReference(Base.GATHERING, "document.documentId"));
		assertEquals("gatheringForeignKeys.documentId", mappings.getEntityReference(Base.UNIT, "document.documentId"));

		assertEquals("gatheringForeignKeys.documentId", mappings.getEntityReference(Base.ANNOTATION, "document.documentId"));
		assertEquals("gatheringForeignKeys.documentId", mappings.getEntityReference(Base.UNIT_MEDIA, "document.documentId"));

		assertEquals("sample.id", mappings.getEntityReference(Base.SAMPLE, "unit.samples.sampleId"));
	}

	@Test
	public void test_mapping_namedplace() {
		assertEquals("namedPlace.municipalityId", mappings.getEntityReference(Base.UNIT, "document.namedPlace.municipalityId"));
		assertEquals("namedPlace.id", mappings.getEntityReference(Base.UNIT, "document.namedPlace.id"));

		try {
			mappings.getEntityReference(Base.UNIT, "document.namedPlace.key");
			fail("Should throw exception");
		} catch (IllegalStateException e) {
			assertEquals("Unmapped field document.namedPlace.key for base UNIT", e.getMessage());
		}

		assertEquals("namedPlace.municipalityId", mappings.getEntityReference(Base.GATHERING, "document.namedPlace.municipalityId"));
		assertEquals("namedPlace.municipalityId", mappings.getEntityReference(Base.DOCUMENT, "document.namedPlace.municipalityId"));
		assertEquals("namedPlace.alternativeId", mappings.getEntityReference(Base.DOCUMENT, "document.namedPlace.alternativeId"));

		assertEquals("parentDocument.namedPlaceId", mappings.getEntityReference(Base.UNIT, "document.namedPlaceId"));
	}

	@Test
	public void test_mappings_gathering_level_field() {
		assertEquals("parentGathering.conversions.ykj10km.lat", mappings.getEntityReference(Base.UNIT, "gathering.conversions.ykj10km.lat"));
		assertEquals("gathering.conversions.ykj10km.lat", mappings.getEntityReference(Base.GATHERING, "gathering.conversions.ykj10km.lat"));

		try {
			mappings.getEntityReference(Base.DOCUMENT, "gathering.conversions.ykj10km.lat");
			fail("Should throw exception");
		} catch (IllegalStateException e) {
			assertEquals("Unmapped field gathering.conversions.ykj10km.lat for base DOCUMENT", e.getMessage());
		}
	}

	@Test
	public void mapping_date() {
		assertEquals("gatheringForeignKeys.eventDateEndKey", mappings.getEntityReference(Base.UNIT, "gathering.eventDate.end"));
	}

	@Test
	public void mapping_enum() {
		assertEquals("foreignKeys.sexKey", mappings.getEntityReference(Base.UNIT, "unit.sex"));
	}

	@Test
	public void mapping_joins() {
		assertEquals("documentKeyword.keyword", mappings.getEntityReference(Base.UNIT, "document.keywords"));
		assertEquals("team.teamText", mappings.getEntityReference(Base.UNIT, "gathering.team"));
		assertEquals("teamAgent.agentKey", mappings.getEntityReference(Base.UNIT, "gathering.team.memberId"));
		assertEquals("teamAgentAgent.id", mappings.getEntityReference(Base.UNIT, "gathering.team.memberName"));
		assertEquals("unitAnnotation.annotationByPersonName", mappings.getEntityReference(Base.ANNOTATION, "unit.annotations.annotationByPersonName"));
		assertEquals("documentFact.propertyKey", mappings.getEntityReference(Base.UNIT, "document.facts.fact"));
		assertEquals("gatheringFact.integerValue", mappings.getEntityReference(Base.UNIT, "gathering.facts.integerValue"));
		assertEquals("taxon.scientificName", mappings.getEntityReference(Base.UNIT, "unit.linkings.taxon.scientificName"));
		assertEquals("taxonToGroup.groupKey", mappings.getEntityReference(Base.UNIT, "unit.linkings.taxon.informalTaxonGroups"));
		assertEquals("taxonToHabitat.habitat", mappings.getEntityReference(Base.UNIT, "unit.linkings.taxon.habitats"));
		assertEquals("mediaObject.fullURL", mappings.getEntityReference(Base.UNIT_MEDIA, "unit.media.fullURL"));
		assertEquals("gatheringMedia.mediaObject.author", mappings.getEntityReference(Base.UNIT, "gathering.media.author"));
		assertEquals("id", mappings.getEntityReference(Base.SAMPLE, "unit.unitId"));
		assertEquals("sample.id", mappings.getEntityReference(Base.SAMPLE, "unit.samples.sampleId"));
		assertEquals("mediaForeignKeys.unitId", mappings.getEntityReference(Base.UNIT_MEDIA, "unit.unitId"));
		assertEquals("parentUnit.mediaCount", mappings.getEntityReference(Base.UNIT_MEDIA, "unit.mediaCount"));
		assertEquals("mediaForeignKeys.mediaTypeKey", mappings.getEntityReference(Base.UNIT_MEDIA, "unit.media.mediaType"));
		assertEquals("unitMedia.mediaForeignKeys.mediaTypeKey", mappings.getEntityReference(Base.UNIT, "unit.media.mediaType"));
		try {
			mappings.getEntityReference(Base.UNIT, "unit.media.fullURL");
			fail("Should throw exception");
		} catch (IllegalStateException e) {
			assertEquals("Unmapped field unit.media.fullURL for base UNIT", e.getMessage());
		}
	}

}
