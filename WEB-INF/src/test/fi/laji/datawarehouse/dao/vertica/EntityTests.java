package fi.laji.datawarehouse.dao.vertica;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class EntityTests {

	private static final int KEY_COUNT = 1000000;

	@Test
	public void test_generate_key() {
		long key = NonAutogeneratingBaseEntity.generateKey();
		System.out.println(key + " - " +  ( Long.toString(key).length()));

		Set<Long> keys = new HashSet<>();
		for (int i = 0; i<KEY_COUNT; i++) {
			key = NonAutogeneratingBaseEntity.generateKey();
			if (keys.contains(key)) Assert.fail("Too baad");
			keys.add(key);
			if (i%50000 == 0) System.out.print(".");
		}
		System.out.print(".\n");
		assertEquals(KEY_COUNT, keys.size());
	}

}
