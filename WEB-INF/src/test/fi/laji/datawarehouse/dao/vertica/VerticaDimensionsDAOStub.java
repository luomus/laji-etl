package fi.laji.datawarehouse.dao.vertica;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import fi.laji.datawarehouse.dao.VerticaDimensionsDAO;
import fi.laji.datawarehouse.dao.oracle.SplittedDocumentIdEntity;
import fi.laji.datawarehouse.etl.models.dw.BaseEntity;
import fi.laji.datawarehouse.etl.models.dw.NameableEntity;

public class VerticaDimensionsDAOStub implements VerticaDimensionsDAO {

	@Override
	public void startTaxonAndPersonReprosessing() {
		// Auto-generated method stub

	}

	@Override
	public void updateNameableEntityNames() {
		// Auto-generated method stub

	}

	@Override
	public String getIdForKey(Long value, String field) {
		return "id-for-field-"+field+"-key-"+value;
	}

	@Override
	public Long getEnumKey(Enum<?> enumeration) {
		return 1L;
	}

	@Override
	public void performCleanUp() {
		// Auto-generated method stub

	}

	@Override
	public VerticaDimensionsPersonService getPersonSevice() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public VerticaDimensionsTaxonService getTaxonService() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<NameableEntity> getEntities(Class<? extends NameableEntity> nameableEntityClass) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void updateEntities(List<BaseEntity> entityList) {
		// Auto-generated method stub

	}

	@Override
	public void insertEntities(List<BaseEntity> entityList) {
		// Auto-generated method stub

	}

	@Override
	public void storeDocumentIds(Iterator<String> iterator) {
		// Auto-generated method stub

	}

	@Override
	public void storeSplittedDocumentIds(Collection<SplittedDocumentIdEntity> values) {
		// Auto-generated method stub

	}

}
