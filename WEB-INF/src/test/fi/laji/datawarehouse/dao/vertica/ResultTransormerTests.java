package fi.laji.datawarehouse.dao.vertica;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.exceptions.NoSuchFieldException;
import fi.laji.datawarehouse.etl.utils.Const;
import fi.laji.datawarehouse.query.model.AggregateRow;
import fi.laji.datawarehouse.query.model.queries.AggregateBy;
import fi.luomus.commons.containers.rdf.Qname;

public class ResultTransormerTests {

	@Test
	public void test_aggregated() throws NoSuchFieldException {
		AggregatedResultTransformer transformer = new AggregatedResultTransformer(new AggregateBy("document.collectionId", "document.loadDate"));
		Date date = new Date();
		Object[] values = new Object[] {"foo", "bar", 35L, 4, date, new Qname("atlascode").toURI()};
		String[] fields = new String[] {"querykeyforcollection", "querykeyforloaddate", Const.COUNT, Const.INDIVIDUAL_COUNT_MAX, Const.FIRST_LOAD_DATE_MAX, Const.ATLAS_CODE_MAX};
		AggregateRow row = (AggregateRow) transformer.transformTuple(values, fields);

		assertEquals("foo", row.getAggregateByValues().get(0));
		assertEquals("bar", row.getAggregateByValues().get(1));
		assertEquals(35, row.getCount().intValue());
		assertEquals(4, row.getIndividualCountMax().intValue());
		assertEquals(2, row.getAggregateByValues().size());
		assertEquals(null, row.getFirstLoadDateMin());
		assertEquals(date.getTime(), row.getFirstLoadDateMax().getTime());
		assertEquals(new Qname("atlascode").toURI(), row.getAtlasCodeMax());
		assertEquals(null, row.getAtlasClassMax());
	}

}
